﻿#if USE_FUMB_ZEBEDEE
namespace ZBDFumb {
    public enum UserFeedbackType {
        RequiresVerification,
        NetworkRequestFailed,
        ConfirmLogout
    }
}
#endif