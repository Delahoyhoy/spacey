#if USE_FUMB_ZEBEDEE
using System;
using System.Linq;
using Fumb.NativeDialog;
using UnityEngine;

namespace ZBDFumb {
    public class UserFeedbackManager : MonoBehaviour {
        public static UserFeedbackComponent[] UserFeedbackComponents;

        private void Awake() {
            UserFeedbackComponents = GetComponentsInChildren<UserFeedbackComponent>();
        }

        public static void DisplayUserFeedback(UserFeedbackType userFeedbackType, Action onOkPressed = null,
            Action onCancelPressed = null) {
            UserFeedbackComponent userFeedbackComponent =
                UserFeedbackComponents.FirstOrDefault(item => item.FeedbackType == userFeedbackType);
            userFeedbackComponent.Display(onOkPressed, onCancelPressed);
        }
        
        public static void DisplayNetworkError(long errorCode) {
            Dialog.DisplayGenericSomethingWentWrongCheckInternetDialog(errorCode.ToString());
        }
    }
}
#endif