#if USE_FUMB_ZEBEDEE
using System;
using Fumb.Attribute;
using Fumb.NativeDialog;
using UnityEngine;

namespace ZBDFumb {
    public class UserFeedbackComponent : MonoBehaviour {
        [SerializeField]
        private UserFeedbackType feedbackType;

        public UserFeedbackType FeedbackType => feedbackType;

        [SerializeField]
        private bool displayGenericNetworkError;

        [SerializeField]
        [ConditionalField("displayGenericNetworkError", true)]
        private string displayTitle;

        [SerializeField]
        [ConditionalField("displayGenericNetworkError", true)]
        private string displayMessage;

        [SerializeField]
        private string okText = "OK";

        public void Display(Action onOK = null, Action onCancel = null) {
            if (displayGenericNetworkError) {
                Dialog.DisplayGenericSomethingWentWrongCheckInternetDialog();
            }
            else {
                Dialog.OpenDialog(displayTitle, displayMessage, okText, onOK, "Cancel", onCancel);
            }
        }
    }
}
#endif