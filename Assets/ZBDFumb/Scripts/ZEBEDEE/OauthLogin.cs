#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using Fumb.Debugging;
using Fumb.General;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using Zebedee;

namespace ZBDFumb {
    class AccessTokenRequest {
        public string code;
        public string client_id;
        public string code_verifier;
        public string grant_type;
        public string redirect_uri;
        public string refresh_token;
    }

    public class ActionResponse {
        public bool error;
        public string response;
        public string type;
        public string data;
        public long responseCode;
    }

    public class OauthLogin : MonoBehaviour {
        public Action<string> gotUserInfoPayloadAction;
        public Action<long> failedToGetUserInfoAction;
        public Action detectedUserCancelLogin;
        public Action<AccessTokenInfo> gotAccessTokenInfoAction;

        public string baseUrl;
        public bool useLocal;
        public string clientId;
        public string redirectUrl;
        string lastConnectionString;

        private bool didLeaveAppForAuth;
        private bool deepLinkWasActivated;

        private ZebedeeSettings settings;
        private ZebedeeSettings Settings => settings ??= Fumb.FumbResourcesHelper.LoadObject<ZebedeeSettings>();

        private void Awake() {
            if (useLocal) {
                baseUrl = "http://localhost:8000";
            }
            else {
                baseUrl = Settings.AuthenticationBaseURL;
            }

            clientId = Settings.AuthenticationClientId;
            redirectUrl = Settings.AuthenticationRedirectUrl;

            // We can use this function to get uri parameters
            Application.deepLinkActivated += onDeepLinkActivated;
            if (!string.IsNullOrEmpty(Application.absoluteURL)) {
                // Cold start and Application.absoluteURL not null so process Deep Link.
                onDeepLinkActivated(Application.absoluteURL);
            }

        }

        // Loing via the native browser, advantages it supports all login function and shares cookies allowing faster login to socials as passwords/usernames are likley remembered, disadvantage is it leaves the app to open the flow in the OS browser.
        public void LoginViaBrowser() {
            StartLogin();
        }

        public void StartLogin() {
            deepLinkWasActivated = false;

            //Generate the PKCE
            string[] pkce = GeneratePKCE();
            string verifier = pkce[0];
            string challenge = pkce[1];

            // state is used to track users on callback, just use a ranomd string for now, but ideally this would be a user id or unique user reference
            string state = Guid.NewGuid().ToString();

            //save to player prefs as OS may close the app duing reidrect so the app could be reopened with the deep link uri data hence we need to be able to get the last verifier and/or state
            PlayerPrefs.SetString("verifier", verifier);
            PlayerPrefs.SetString("state", state);

            // Generate the oauth url and open in webview or browser
            string zbdOauthUrl = "https://api.zebedee.io/v0/oauth2/authorize?redirect_uri=";
            zbdOauthUrl += UnityWebRequest.EscapeURL(redirectUrl);
            zbdOauthUrl += "&scope=user&client_id=" + clientId;
            zbdOauthUrl += "&response_type=code&code_challenge=" + challenge + "&code_challenge_method=S256&state=" +
                           state;

            didLeaveAppForAuth = true;
            Application.OpenURL(zbdOauthUrl);
        }

        // ZEBEDEE will redirect to the app with the code and state to continue login
        void ContinueLogin(string url) {
            DebugLogger.Log<ZBDFumbLogs>("Continue login", GetType());
            if (!url.Contains(redirectUrl)) {
                DebugLogger.Log<ZBDFumbLogs>("Deeplink returned url doesn't contain the redirectUrl. Ending login", GetType());
                return;
            }
            
            String code = getParam(url, "code");
            string state = getParam(url, "state");
            string verifier = PlayerPrefs.GetString("verifier");

            //Get access token using the code and verifier, this SHOULD BE DONE on the server!
            GetOauthAccessToken(code, verifier, state);
        }

        private AccessTokenInfo GetAccessTokenFromSuccessActionResponse(ActionResponse actionResponse) {
            JSONNode data = JSON.Parse(actionResponse.response);
            string accessToken = data["data"]["access_token"].Value;
            string refreshToken = data["data"]["refresh_token"].Value;
            AccessTokenInfo accessTokenInfo = new AccessTokenInfo(accessToken, refreshToken);
            gotAccessTokenInfoAction?.Invoke(accessTokenInfo);
            return accessTokenInfo;
        }
        
        void GetOauthAccessToken(string code, string verifier, string state) {
            GetAccessToken(code, verifier, state, (callback) => {
                if (callback.error) {
                    failedToGetUserInfoAction?.Invoke(callback.responseCode);
                } else {
                    AccessTokenInfo accessTokenInfo = GetAccessTokenFromSuccessActionResponse(callback);
                    //Now we have the accessToken use it get the user data!
                    GetUserData(accessTokenInfo, (callbackUserData) => {
                        if (callbackUserData.error == false) {
                            gotUserInfoPayloadAction?.Invoke(callbackUserData.response);
                        }
                        else {
                            failedToGetUserInfoAction?.Invoke(callbackUserData.responseCode);
                        }
                    });
                }
            });
        }

        private void OnApplicationPause(bool pause) {
            if (!pause) {
                if (didLeaveAppForAuth) {
                    didLeaveAppForAuth = false;
                    if (!deepLinkWasActivated) {
                        SetTimer.StartTimer(1.0f, () => {
                            if (!deepLinkWasActivated) {
                                detectedUserCancelLogin?.Invoke();
                            }
                        });
                    }
                }
            }
        }

        //On ios we can get the redirect info here
        private void onDeepLinkActivated(string url) {
            deepLinkWasActivated = true;
            DebugLogger.Log<ZBDFumbLogs>("ZBD OAuth Deep link activated. Url returned: " + url, GetType());
            ContinueLogin(url);
        }

        //THIS SHOULD BE DONE ON THE SERVER AS IT EXPOSES THE CLIENT SECRET, HERE FOR EDUCATIONAL PURPOSES ONLY!
        public void GetAccessToken(string code, string verifier, string state, Action<ActionResponse> callback) {
            StartCoroutine(ContinueGetAccessToken(code, verifier, state, callback));
            return;
        }
        
        IEnumerator ContinueGetAccessToken(string code, string verifier, string state, Action<ActionResponse> callback) {
            AccessTokenRequest myObject = new AccessTokenRequest();
            myObject.code = code;
            myObject.client_id = clientId; //THIS SHOULD BE ON THE SERVER!
            myObject.code_verifier = verifier;
            myObject.grant_type = "authorization_code";
            myObject.redirect_uri = redirectUrl;
            string jsonString = JsonUtility.ToJson(myObject);
            byte[] postData = System.Text.Encoding.UTF8.GetBytes(jsonString);

            if (baseUrl[baseUrl.Length - 1] != '/') {
                baseUrl += "/";
            }
            string webRequestUrl = baseUrl + "api/v1/zbd/users/get-access-token";
            
            DebugLogger.Log<ZBDFumbLogs>("Begin get access token request", GetType());
            DebugLogger.Log<ZBDFumbLogs>("Access token request url: " + webRequestUrl, GetType());
            DebugLogger.Log<ZBDFumbLogs>("Request json: " + jsonString, GetType());
            
            var request = new UnityWebRequest(webRequestUrl, "POST");
            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(postData);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();
            DebugLogger.Log<ZBDFumbLogs>("ACCESS TOKEN REQUEST HANDLER TEXT: " + request.downloadHandler.text, GetType());
            ActionResponse aR = new ActionResponse();
            aR.responseCode = request.responseCode;

            if (request.isNetworkError || request.isHttpError) {
                DebugLogger.LogError<ZBDFumbLogs>("Failed to get access token", GetType());
                aR.error = true;
                aR.response = "get access token error";
            }
            else {
                aR.response = request.downloadHandler.text;
                AccessTokenInfo newAccessTokenInfo = GetAccessTokenFromSuccessActionResponse(aR);
                gotAccessTokenInfoAction?.Invoke(newAccessTokenInfo);
                DebugLogger.Log<ZBDFumbLogs>("Access token request complete: " + request.downloadHandler.text, GetType());
            }

            callback(aR);
        }

        public void RefreshAccessToken(AccessTokenInfo accessTokenInfo, Action<ActionResponse> callback) {
            StartCoroutine(ContinueRefreshAccessToken(accessTokenInfo, callback));
        }

        IEnumerator ContinueRefreshAccessToken(AccessTokenInfo accessTokenInfo, Action<ActionResponse> callback) {
            AccessTokenRequest myObject = new AccessTokenRequest();
            myObject.refresh_token = accessTokenInfo.RefreshToken;
            myObject.client_id = clientId;
            myObject.grant_type = "refresh_token";
            myObject.redirect_uri = redirectUrl;
            
            string jsonString = JsonUtility.ToJson(myObject);
            byte[] postData = System.Text.Encoding.UTF8.GetBytes(jsonString);

            if (baseUrl[baseUrl.Length - 1] != '/') {
                baseUrl += "/";
            }
            string webRequestUrl = baseUrl + "api/v1/zbd/users/get-access-token";
            
            DebugLogger.Log<ZBDFumbLogs>("Begin refresh access token request", GetType());
            DebugLogger.Log<ZBDFumbLogs>("Access token refresh request url: " + webRequestUrl, GetType());
            DebugLogger.Log<ZBDFumbLogs>("Refresh request json: " + jsonString, GetType());
            
            var request = new UnityWebRequest(webRequestUrl, "POST");
            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(postData);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();
            DebugLogger.Log<ZBDFumbLogs>("ACCESS TOKEN REFRESH REQUEST HANDLER TEXT: " + request.downloadHandler.text, GetType());
            ActionResponse aR = new ActionResponse();
            aR.responseCode = request.responseCode;

            if (request.isNetworkError || request.isHttpError) {
                DebugLogger.LogError<ZBDFumbLogs>("Failed to refresh access token", GetType());
                aR.error = true;
                aR.response = "get access token error";
            }
            else {
                aR.response = request.downloadHandler.text;
                AccessTokenInfo newAccessTokenInfo = GetAccessTokenFromSuccessActionResponse(aR);
                gotAccessTokenInfoAction?.Invoke(newAccessTokenInfo);
                DebugLogger.Log<ZBDFumbLogs>("Access token request complete: " + request.downloadHandler.text, GetType());
            }

            callback(aR);
        }

        public void GetUserData(AccessTokenInfo accessTokenInfo, Action<ActionResponse> callback) {
            DebugLogger.Log<ZBDFumbLogs>("Get user data called", GetType());
            StartCoroutine(ContinueGetUserData(accessTokenInfo, callback));
        }

        IEnumerator ContinueGetUserData(AccessTokenInfo accessTokenInfo, Action<ActionResponse> callback) {
            DebugLogger.Log<ZBDFumbLogs>("Beginning request to get user data", GetType());
            var request = new UnityWebRequest("https://api.zebedee.io/v0/oauth2/user", "GET");
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + accessTokenInfo.AccessToken);
            yield return request.SendWebRequest();
            ActionResponse aR = new ActionResponse();
            aR.responseCode = request.responseCode;

            if (request.isNetworkError || request.isHttpError) {
                DebugLogger.Log<ZBDFumbLogs>("Get user data network error", GetType());
                aR.error = true;
                try {
                    JSONNode data = JSON.Parse(request.downloadHandler.text);

                    if (data["message"].Value != null) {
                        aR.response = data["message"].Value;
                    }
                    else {
                        aR.response = request.downloadHandler.text;
                    }
                    
                    DebugLogger.Log<ZBDFumbLogs>("Fail request response: " + aR.response, GetType());
                }
                catch (Exception e) {
                    aR.response = request.error;
                }

                callback(aR);
            }
            else {
                DebugLogger.Log<ZBDFumbLogs>("Get user data success: " + request.downloadHandler.text, GetType());
                JSONNode data = JSON.Parse(request.downloadHandler.text);
                aR.error = false;
                if (data["error"].AsBool == true) {
                    aR.error = true;
                    aR.response = data["message"].Value;
                    aR.data = data["data"].Value;
                }
                else {
                    aR.response = request.downloadHandler.text;
                }

                callback(aR);
            }
        }

        string getParam(string url, string param) {
            String res = "";

            String substr = param + "=";
            int index = url.IndexOf(substr, StringComparison.OrdinalIgnoreCase);

            // -1 if not found
            if (index >= 0) {
                int indexEnd = index + substr.Length;
                int stringLength = url.Length - indexEnd;
                res = url.Substring(indexEnd, stringLength);
                string[] arr = res.Split(char.Parse("&"));
                if (arr.Length > 0) {
                    res = arr[0];
                }

                return res;

            }

            return null;
        }

        string[] GeneratePKCE() {

            byte[] randomBytes = new byte[32];
            System.Random random = new System.Random();
            random.NextBytes(randomBytes);

            // encode
            string verifier = URLEncode(Convert.ToBase64String(randomBytes));

            // hash and encode to get the challenge
            byte[] sha256 = Sha256(verifier);
            string challenge = URLEncode(Convert.ToBase64String(sha256));
            return new string[2] {verifier, challenge};


        }

        public static byte[] Sha256(string s) {
            // Form hash
            System.Security.Cryptography.SHA256 h = System.Security.Cryptography.SHA256.Create();
            byte[] data = h.ComputeHash(System.Text.Encoding.Default.GetBytes(s));
            return data;
        }


        public static string URLEncode(string s) {
            s = s.Replace('+', '-').Replace('/', '_').Replace("=", ""); // no padding
            return s;
        }
    }
}
#endif