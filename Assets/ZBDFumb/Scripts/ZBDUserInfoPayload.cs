#if USE_FUMB_ZEBEDEE
using System;
using UnityEngine;

namespace ZBDFumb {
    [Serializable]
    public class ZBDUserInfoPayload {
        [SerializeField]
        private bool success;
        public bool Success => success;

        [SerializeField]
        private UserInfo data;
        public UserInfo Data => data;

        public ZBDUserInfoPayload(bool success, UserInfo data) {
            this.success = success;
            this.data = data;
        }
    }
}
#endif