#if USE_FUMB_ZEBEDEE
using System;
using System.Collections.Generic;
using System.Linq;
using Fumb.General;
using Fumb.Save;
using Fumb.UI;
using UnityEngine;

namespace ZBDFumb {
    public class ZBDAuthWithdrawalManager : MonoBehaviour {
        
        public static ZBDAuthWithdrawalManager Instance { get; private set; }

        public static string CurrentAuthToken {
            get {
                if (GetUserInfoHandler.AccessTokenInfo.IsPopulated) {
                    return GetUserInfoHandler.AccessTokenInfo.Value.AccessToken;
                }

                return null;
            }
        }

        public Action<VerificationRequiredInfo> gotVerificationRequiredInfoAction;
        
        [SerializeField]
        private GetVerificationRequiredHandler getVerificationRequiredHandler;
        private VerificationRequiredInfo cachedVerificationRequiredInfo;
        private UserInfo CachedUserInfo => ZBDFumbAuthLogin.CachedUserInfo;

        [SerializeField]
        private OpenApp openApp;

        public Action<WithdrawalAuthState> authStateUpdatedAction;
        public WithdrawalAuthState CurrentWithdrawalAuthState { get; private set; }
        private string GamerTag => ZBDFumbAuthLogin.GamerTag;

        private void Awake() {
            Instance = this;
        }

        public bool CanWithdraw {
            get { return CurrentWithdrawalAuthState == WithdrawalAuthState.HasWithdrawalPermission; }
        }

        public void RequestCanWithdraw(Action<bool> onGetCanWithdrawState) {
            Action startGetVerificationRequired = null;
            Action<VerificationRequiredInfo> onGetVerificationRequiredInfo = null;
            Action onGetVerificationRequiredInfoFailed = null;
            Action startGetUserInfo = null;
            Action onProcessEnd = null;

            startGetVerificationRequired = () => {
                getVerificationRequiredHandler.GetIsVerificationRequired(onGetVerificationRequiredInfo,
                    onGetVerificationRequiredInfoFailed);
            };
            onGetVerificationRequiredInfo = info => {
                cachedVerificationRequiredInfo = info;
                gotVerificationRequiredInfoAction?.Invoke(info);
                startGetUserInfo();
            };
            onGetVerificationRequiredInfoFailed = () => {
                UserFeedbackManager.DisplayUserFeedback(UserFeedbackType.NetworkRequestFailed);
                onProcessEnd();
            };
            
            startGetUserInfo = () => {
                Action<UserInfo> onGetUserInfo = null;
                Action onGetUserInfoFail = null;
                Action onUserCancel = null;
                
                onGetUserInfo = info => { onProcessEnd(); };
                onGetUserInfoFail = () => { onProcessEnd(); };

                ZBDFumbAuthLogin.Instance.Login(false, onGetUserInfo, onGetUserInfoFail);
            };

            onProcessEnd = () => {
                LoadingScreenController.Display(false);
                RefreshAuthState();
                bool canWithdraw = CanWithdraw;
                if (CurrentWithdrawalAuthState == WithdrawalAuthState.NeedsVerification) {
                    Action onUserPressVerify = () => { openApp.OpenTheApp(); };
                    Action onUserPressCancel = () => { onGetCanWithdrawState?.Invoke(canWithdraw); };

                    UserFeedbackManager.DisplayUserFeedback(UserFeedbackType.RequiresVerification, onUserPressVerify,
                        onUserPressCancel);
                }
                else {
                    onGetCanWithdrawState?.Invoke(canWithdraw);
                }
            };

            LoadingScreenController.Display(true);
            startGetVerificationRequired();
        }

        private void RefreshAuthState() {
            WithdrawalAuthState refreshedWithdrawalAuthState;
            if (string.IsNullOrEmpty(GamerTag)) {
                refreshedWithdrawalAuthState = WithdrawalAuthState.NotLoggedIn;
            }
            else {
                if (CachedUserInfo != null && CachedUserInfo.IsVerified) {
                    refreshedWithdrawalAuthState = WithdrawalAuthState.HasWithdrawalPermission;
                }
                else {
                    if (cachedVerificationRequiredInfo == null) {
                        refreshedWithdrawalAuthState = WithdrawalAuthState.FailedToGetVerificationRequiredInfo;
                    }
                    else {
                        if (cachedVerificationRequiredInfo.RequiresZBDVerification) {
                            refreshedWithdrawalAuthState = WithdrawalAuthState.NeedsVerification;
                        }
                        else {
                            refreshedWithdrawalAuthState = WithdrawalAuthState.HasWithdrawalPermission;
                        }
                    }
                }
            }

            CurrentWithdrawalAuthState = refreshedWithdrawalAuthState;
            authStateUpdatedAction?.Invoke(CurrentWithdrawalAuthState);
        }
    }
}
#endif