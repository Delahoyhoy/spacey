﻿#if USE_FUMB_ZEBEDEE
namespace ZBDFumb {
    public enum WithdrawalAuthState {
        FailedToGetVerificationRequiredInfo,
        NotLoggedIn,
        HasWithdrawalPermission,
        NeedsVerification
    }
}
#endif