#if USE_FUMB_ZEBEDEE
using System;
using Fumb;
using Fumb.Save;
using Fumb.UI;
using UnityEngine;

namespace ZBDFumb {
    public class ZBDFumbAuthLogin : MonoBehaviour
    {

        private static ZBDFumbAuthLogin instance;
        public static ZBDFumbAuthLogin Instance
        {
            get
            {
                if (setup)
                    return instance;
                Setup();
                return instance;
            }
            private set => instance = value;
        }

        public Action authCompleteAction;
        public Action logoutCompleteAction;
        
        public static UserInfo CachedUserInfo { get; private set; }
        [SerializeField]
        private GetUserInfoHandler getUserInfoHandler;

        [ManagedSaveValue("SavedZBDGamerTag")]
        private static SaveValueString SavedGamerTag;
        public static string GamerTag {
            get => SavedGamerTag;
            private set => SavedGamerTag.Value = value;
        }

        [ManagedSaveValue("SavedDidLogoutZBD")]
        private static SaveValueBool SavedDidLogout;
        private static bool DidLogout {
            get => SavedDidLogout.Value;
            set => SavedDidLogout.Value = value;
        }

        [ManagedSaveValue("SaveHasLoggedIn")]
        private static SaveValueBool SaveHasLoggedIn;

        public Action newLoginAction;

        private static bool setup;
        
        public static bool IsLoggedIn => !string.IsNullOrEmpty(GamerTag);

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        private static void RuntimeInitializeOnLoad() {
            Initialize();
        }

        private static void Initialize() {
            SceneLoader.LoadedFirstValidSceneAction += OnLoadedFirstValidScene;
        }

        private static void OnLoadedFirstValidScene() {
            Setup();
        }

        private static void Setup()
        {
            if (setup) 
                return;
            GameObject zbdAuthBehavioursPrefab = Resources.Load<GameObject>("ZBDAuthBehaviours");
            GameObject zbdAuthBehavioursGameObject = GameObject.Instantiate(zbdAuthBehavioursPrefab);
            DontDestroyOnLoad(zbdAuthBehavioursGameObject);
            setup = true;
        }

        private void Awake() {
            Instance = this;
        }

        public void Login(bool displayLoadingScreen, Action<UserInfo> onSuccess, Action onFail) {
            Action startLogin = null;
            Action<UserInfo> onLoginSuccess = null;
            Action<long> onLoginFail = null;
            Action onLoginUserCancel = null;
            Action onProcessEnd = null;

            startLogin = () => { getUserInfoHandler.GetUserInfo(onLoginSuccess, onLoginFail, onLoginUserCancel); };
            onLoginSuccess = info => {
                CachedUserInfo = info;
                GamerTag = info.GamerTag;
                authCompleteAction?.Invoke();

                onProcessEnd();
                onSuccess?.Invoke(info);
                if (!SaveHasLoggedIn.Value)
                {
                    newLoginAction?.Invoke();
                    SaveHasLoggedIn.Value = true;
                }
            };
            onLoginFail = errorCode => {
                UserFeedbackManager.DisplayNetworkError(errorCode);
                onProcessEnd();
                onFail?.Invoke();
            };
            onLoginUserCancel = () => {
                onProcessEnd();
                onFail?.Invoke();
            };
            onProcessEnd = () => {
                if (displayLoadingScreen) {
                    LoadingScreenController.Display(false);
                }
            };

            if (displayLoadingScreen) {
                LoadingScreenController.Display(true);
            }
            startLogin();
        }

        public void Logout(Action onComplete = null, Action onCancel = null) {
            Action onLogoutConfirm = () => {
                GamerTag = null;
                getUserInfoHandler.Logout();
                onComplete?.Invoke();
                logoutCompleteAction?.Invoke();
            };
            Action onLogoutCancel = () => {
                onCancel?.Invoke();
            };
            
            UserFeedbackManager.DisplayUserFeedback(UserFeedbackType.ConfirmLogout, onLogoutConfirm, onLogoutCancel);
        }
    }
}
#endif