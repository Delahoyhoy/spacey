#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using Fumb.Debugging;
using Fumb.Serialization;
using UnityEngine;
using UnityEngine.Networking;

namespace ZBDFumb {
    public class GetVerificationRequiredHandler : MonoBehaviour {

        [SerializeField]
        private string uri;

        public void GetIsVerificationRequired(Action<VerificationRequiredInfo> onComplete, Action onFail) {
            StartCoroutine(ProcessGetIsAuthRequired(onComplete, onFail));
        }

        private IEnumerator ProcessGetIsAuthRequired(Action<VerificationRequiredInfo> onComplete, Action onFail) {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri)) {
                yield return webRequest.SendWebRequest();
                switch (webRequest.result) {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                    case UnityWebRequest.Result.ProtocolError:
                        onFail?.Invoke();
                        break;
                    case UnityWebRequest.Result.Success:
                        VerificationRequiredInfoPayload payload = Serializer.Deserialize<VerificationRequiredInfoPayload>(webRequest.downloadHandler.text);
                        DebugLogger.Log<ZBDFumbLogs>("Completed get verification required. Verification Required Info:\n" + payload.VerificationRequiredInfo, GetType());
                        onComplete?.Invoke(payload.VerificationRequiredInfo);
                        break;
                }
            }
        }
    }
}
#endif