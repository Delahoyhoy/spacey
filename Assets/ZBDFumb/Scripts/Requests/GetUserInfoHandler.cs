#if USE_FUMB_ZEBEDEE
using System;
using Fumb.Debugging;
using Fumb.NativeDialog;
using Fumb.Save;
using Fumb.Serialization;
using UnityEngine;

namespace ZBDFumb {
    public class GetUserInfoHandler : MonoBehaviour {

        [SerializeField]
        private OauthLogin zbdGetUserInfo;
        private const long InvalidCredentialsCode = 401;

        [ManagedSaveValue("SavedZBDAccessTokenInfo")]
        private static SaveValueAccessTokenInfo SavedZBDAccessToken;
        public static SaveValueAccessTokenInfo AccessTokenInfo {
            get => SavedZBDAccessToken;
            private set => SavedZBDAccessToken.Value = value;
        }

        private void Awake() {
            zbdGetUserInfo.gotAccessTokenInfoAction += OnGotAccessToken;
        }

        private void OnGotAccessToken(AccessTokenInfo accessTokenInfo) {
            AccessTokenInfo.Value = accessTokenInfo;
        }

        public void GetUserInfo(Action<UserInfo> onGetUserInfo, Action<long> onFail, Action onUserCancel) {
            DebugLogger.Log<ZBDFumbLogs>("Start get user info", GetType());
            if (Application.isEditor) {
                DoEditorAuthProcess(onGetUserInfo, onFail);
            }
            else {
                if (AccessTokenInfo.IsPopulated) {
                    DebugLogger.Log<ZBDFumbLogs>("Have a saved access token. Try silent auth process", GetType());
                    DoAlreadyHaveAccessTokenAuthProcess(onGetUserInfo, onFail, onUserCancel);
                }
                else {
                    DebugLogger.Log<ZBDFumbLogs>("No access token stored. Begin full auth process", GetType());
                    DoFullAuthProcess(onGetUserInfo, onFail, onUserCancel);
                }
            }
        }

        private void DoFullAuthProcess(Action<UserInfo> onGetUserInfo, Action<long> onFail, Action onUserCancel) {
            Action<string> onGetUserPayload = null;
            Action onUserCancelLogin = null;
            Action<long> onGetUserPayloadFail = null;
            Action endProcess = null;

            onGetUserPayload = s => {
                DebugLogger.Log<ZBDFumbLogs>("Got user payload: " + s, GetType());
                UserInfoPayload userInfoPayload = Serializer.Deserialize<UserInfoPayload>(s);
                onGetUserInfo?.Invoke(userInfoPayload.UserInfo);
                endProcess();
            };

            onUserCancelLogin = () => {
                DebugLogger.Log<ZBDFumbLogs>("User cancelled login", GetType());
                onUserCancel?.Invoke();
                endProcess();
            };

            onGetUserPayloadFail = errorCode => {
                DebugLogger.Log<ZBDFumbLogs>("Failed to get user payload", GetType());
                onFail?.Invoke(errorCode);
                endProcess();
            };

            endProcess = () => {
                zbdGetUserInfo.gotUserInfoPayloadAction -= onGetUserPayload;
                zbdGetUserInfo.detectedUserCancelLogin -= onUserCancelLogin;
                zbdGetUserInfo.failedToGetUserInfoAction -= onGetUserPayloadFail;
            };

            zbdGetUserInfo.gotUserInfoPayloadAction += onGetUserPayload;
            zbdGetUserInfo.detectedUserCancelLogin += onUserCancelLogin;
            zbdGetUserInfo.failedToGetUserInfoAction += onGetUserPayloadFail;

            zbdGetUserInfo.StartLogin();
        }

        private void TryRefreshAccessToken(Action onSuccess, Action<long> onFail) {
            zbdGetUserInfo.RefreshAccessToken(AccessTokenInfo, response => {
                if (response.error) {
                    onFail?.Invoke(response.responseCode);
                } else {
                    onSuccess?.Invoke();
                }
            });
        }

        private void DoAlreadyHaveAccessTokenAuthProcess(Action<UserInfo> onGetUserInfo, Action<long> onFail, Action onUserCancel) {
            DebugLogger.Log<ZBDFumbLogs>("Already has auth token so try and get user data called", GetType());
            
            Action<UserInfo> onGetUserDataSuccess = null;
            Action<long> onFailGetUserData = null;
            Action<long> onFailRefreshAccessToken = null;
            Action<long> onFailAndTerminateProcess = null;
            
            onGetUserDataSuccess = info => {
                onGetUserInfo?.Invoke(info);
            };
            onFailGetUserData = responseCode => {
                DebugLogger.Log<ZBDFumbLogs>("Failed to get user data. Response code: " + responseCode, GetType());
                if (responseCode == InvalidCredentialsCode) {
                    DebugLogger.Log<ZBDFumbLogs>("Access token was invalid. Try and refresh", GetType());
                    Action onRefreshSuccess = () => {
                        DebugLogger.Log<ZBDFumbLogs>("Did successfully refresh access token. Now try and get user data", GetType());
                        //Don't check for invalid credentials on this one. We've just refreshed the token so it should work.
                        GetUserData(onGetUserDataSuccess, onFailAndTerminateProcess);
                    };
                    RefreshAccessToken(onRefreshSuccess, onFailRefreshAccessToken);
                    return;
                }
                
                onFail?.Invoke(responseCode);
            };
            onFailRefreshAccessToken = responseCode => {
                DebugLogger.Log<ZBDFumbLogs>("Failed to refresh access token", GetType());
                if (responseCode == InvalidCredentialsCode) {
                    DebugLogger.Log<ZBDFumbLogs>("The refresh token has expired. Do full auth process", GetType());
                    DoFullAuthProcess(onGetUserInfo, onFail, onUserCancel);
                    return;
                }
                
                AccessTokenInfo.ResetValue();
                onFail?.Invoke(responseCode);
            };
            onFailAndTerminateProcess = responseCode => {
                DebugLogger.Log<ZBDFumbLogs>("Failed to get user data from already having access token. Ending process as failed here", GetType());
                onFail?.Invoke(responseCode);
            };
            
            if (AccessTokenInfo.Value.GetElapsedTimeSinceCreation().TotalDays >= 1) {
                DebugLogger.Log<ZBDFumbLogs>("Access token is likely to have expired. Try and refresh it", GetType());
                Action onRefreshSuccess = () => {
                    DebugLogger.Log<ZBDFumbLogs>("Refreshed access token. Try and get user data using it", GetType());
                    GetUserData(onGetUserDataSuccess, onFailGetUserData);
                };
                RefreshAccessToken(onRefreshSuccess, onFailRefreshAccessToken);
            } else {
                DebugLogger.Log<ZBDFumbLogs>("Access token should still be valid. Call get user data", GetType());
                GetUserData(onGetUserDataSuccess, onFailGetUserData);
            }
        }

        private void RefreshAccessToken(Action onSuccess, Action<long> onFail) {
            Action<ActionResponse> onRefreshProcessConcluded = response => {
                if (response.error) {
                    if (response.responseCode == InvalidCredentialsCode) {
                        //Refresh token has expired. Reset access token info.
                        AccessTokenInfo.ResetValue();
                    }
                    //Failed process of refreshing token. Likely internet issue. Failed.
                    onFail?.Invoke(response.responseCode);
                } else {
                    onSuccess?.Invoke();
                }
            };

            zbdGetUserInfo.RefreshAccessToken(AccessTokenInfo, onRefreshProcessConcluded);
        }

        private void GetUserData(Action<UserInfo> onSuccess, Action<long> onFail) {
            zbdGetUserInfo.GetUserData(AccessTokenInfo, response => {
                if (response.error) {
                    onFail?.Invoke(response.responseCode);
                    return;
                }
                
                UserInfoPayload userInfoPayload = Serializer.Deserialize<UserInfoPayload>(response.response);
                onSuccess?.Invoke(userInfoPayload.UserInfo);
            });
        }

        private void DoEditorAuthProcess(Action<UserInfo> onGetUserInfo, Action<long> onFail) {
            Action onChooseSuccess = () => {
                const string GamerTag = "DebugGamerTag";
                Action onChooseIsVerified = () => {
                    onGetUserInfo?.Invoke(new UserInfo("debugID", GamerTag, true, GamerTag + "@lightning.gg", ""));
                };
                Action onChooseIsNotVerified = () => {
                    onGetUserInfo?.Invoke(new UserInfo("debugID", GamerTag, false, GamerTag + "@lightning.gg", ""));
                };

                Dialog.OpenDialog("Debug Set Verification State", "Choose ZBD verification state", "Verified",
                    onChooseIsVerified, "Unverified", onChooseIsNotVerified);
            };
            Action onChooseFail = () => { onFail?.Invoke(0); };

            Dialog.OpenDialog("Debug Editor ZBD Auth", "Choose whether auth will succeed", "Succeed", onChooseSuccess,
                "Fail", onChooseFail);
        }

        public void Logout() {
            AccessTokenInfo.ResetValue();
        }
    }
}
#endif