#if USE_FUMB_ZEBEDEE
using System;
using UnityEngine;

namespace ZBDFumb {
    [Serializable]
    public class UserInfoPayload {
        [SerializeField]
        private bool success;

        public bool Success => success;

        [SerializeField]
        private UserInfo data;

        public UserInfo UserInfo => data;
    }
}
#endif