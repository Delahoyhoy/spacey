#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBDFumb {
    [Serializable]
    public class UserInfo {
        [SerializeField]
        private string id;

        public string Id => id;

        [SerializeField]
        private string gamertag;

        public string GamerTag => gamertag;

        [SerializeField]
        private bool isVerified;

        public bool IsVerified => isVerified;

        [SerializeField]
        private string lightningAddress;

        public string LightningAddress => lightningAddress;

        [SerializeField]
        private string publicStaticCharge;

        public string PublicStaticCharge => publicStaticCharge;

        public UserInfo(string id, string gamertag, bool isVerified, string lightningAddress,
            string publicStaticCharge) {
            this.id = id;
            this.gamertag = gamertag;
            this.isVerified = isVerified;
            this.lightningAddress = lightningAddress;
            this.publicStaticCharge = publicStaticCharge;
        }
    }
}
#endif