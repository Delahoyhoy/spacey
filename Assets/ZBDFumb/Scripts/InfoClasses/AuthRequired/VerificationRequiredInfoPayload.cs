#if USE_FUMB_ZEBEDEE
using System;
using UnityEngine;

namespace ZBDFumb {
    [Serializable]
    public class VerificationRequiredInfoPayload {
        [SerializeField]
        private string message;
        public string Message => message;

        [SerializeField]
        private VerificationRequiredInfo data;
        public VerificationRequiredInfo VerificationRequiredInfo => data;
    }
}
#endif