#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBDFumb {
    [Serializable]
    public class VerificationRequiredInfo {
        [SerializeField]
        private bool zbdLogin;
        public bool RequiresZBDVerification => zbdLogin || Vpn;

        [SerializeField]
        private bool vpn;

        public bool Vpn => vpn;

        [SerializeField]
        private string countryCode;

        public string CountryCode => countryCode;

        public VerificationRequiredInfo(bool zbdLogin, bool vpn, string countryCode) {
            this.zbdLogin = zbdLogin;
            this.vpn = vpn;
            this.countryCode = countryCode;
        }

        public override string ToString() {
            string returnString = "Requires Verification: " + RequiresZBDVerification +
                                  "\nVPN: " + Vpn +
                                  "\nCounty Code: " + CountryCode;
            return returnString;
        }
    }
}
#endif