#if USE_FUMB_ZEBEDEE
using System;
using Fumb.Save;

namespace ZBDFumb {
    [Serializable]
    public class SaveValueAccessTokenInfo : SaveValue<AccessTokenInfo> {
        
    }
}
#endif