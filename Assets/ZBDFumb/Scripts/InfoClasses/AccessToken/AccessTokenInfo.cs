#if USE_FUMB_ZEBEDEE
using System;
using Fumb.Serialization;
using UnityEngine;

namespace ZBDFumb {
    [Serializable]
    public class AccessTokenInfo {
        [SerializeField]
        private string accessToken;
        public string AccessToken => accessToken;

        [SerializeField]
        private string refreshToken;
        public string RefreshToken => refreshToken;

        [SerializeField]
        private SerializableDateTime creationTime;
        public DateTime CreationTime => creationTime.DateTime;

        public AccessTokenInfo(string accessToken, string refreshToken) {
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
            creationTime = new SerializableDateTime(DateTime.Now);
        }

        public TimeSpan GetElapsedTimeSinceCreation() {
            return DateTime.Now - CreationTime;
        }
    }
}
#endif