#if USE_FUMB_ZEBEDEE
using System;
using Fumb.NativeDialog;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace ZBDFumb {
    public class WithdrawalUiController : MonoBehaviour {
        [SerializeField]
        private UnityEvent displayNotLoggedInEvent;
        [SerializeField]
        private UnityEvent displayLoggedInEvent;
        [SerializeField]
        private UnityEvent<string> setGamerTagEvent;

        private void Start() {
            RefreshDisplay();
            ZBDFumbAuthLogin.Instance.authCompleteAction += OnAuthComplete;
        }

        private void OnAuthComplete() {
            RefreshGamerTag();
        }

        public void OnLoginPressed() {
            Action<UserInfo> onSuccess = info => {
                RefreshDisplay();
            };
            Action onFail = () => {
                RefreshDisplay();
            };
            
            ZBDFumbAuthLogin.Instance.Login(true, onSuccess, onFail);
        }

        public void LogoutPressed() {
            Action onLogoutComplete = () => {
                RefreshDisplay();
            };
            
            ZBDFumbAuthLogin.Instance.Logout(onLogoutComplete);
        }

        public void OnCanWithdrawPressed() {
            Action<bool> onGetResult = b => {
                RefreshDisplay();
            };
            
            ZBDAuthWithdrawalManager.Instance.RequestCanWithdraw(onGetResult);
        }

        private void RefreshDisplay() {
            if (ZBDFumbAuthLogin.IsLoggedIn) {
                displayLoggedInEvent.Invoke();
            } else {
                displayNotLoggedInEvent.Invoke();
            }
            
            RefreshGamerTag();
        }

        private void RefreshGamerTag() {
            string gamerTag = ZBDFumbAuthLogin.GamerTag;
            if (gamerTag == null) {
                gamerTag = string.Empty;
            }
            
            setGamerTagEvent.Invoke(gamerTag);
        }
    }
}
#endif