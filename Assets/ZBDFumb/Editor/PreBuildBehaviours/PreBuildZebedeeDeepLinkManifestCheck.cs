#if USE_FUMB_ZEBEDEE
using Fumb.Adverts;
using Fumb.Helper.Editor;
using Zebedee;

namespace Fumb.Build.PreCheck {
    public class PreBuildZebedeeDeepLinkManifestCheck : PreBuildCheck {

        private string GetRequiredManifestEntry(string redirectAppName) {
            string requiredManifestEntry = "<data android:scheme=\"" + redirectAppName + "\"";
            return requiredManifestEntry;
        }
        
        public override PreBuildCheckResult CheckIfCanRunBuild() {
            #if !UNITY_ANDROID
            return new PreBuildCheckResult(true);
            #endif

            ZebedeeSettings zebedeeSettings = FumbResourcesHelper.LoadObject<ZebedeeSettings>();
            if (zebedeeSettings == null) {
                return new PreBuildCheckResult(true);
            }

            string redirectUrl = zebedeeSettings.AuthenticationRedirectUrl;
            string redirectAppName = redirectUrl.Replace("://authorize", "");
            
            string manifestText = ManifestHelper.GetMainManifestText();
            string requiredManifestEntry = GetRequiredManifestEntry(redirectAppName);
            if (!manifestText.Contains(requiredManifestEntry)) {
                return new PreBuildCheckResult(false, "Android Manifest is missing redirect entry which allows external " +
                                                      "ZBD auth to redirect back to the app. Look at asset 'AndroidManifestZBDRedirectSample'" +
                                                      "for guidance");
            }
            
            return new PreBuildCheckResult(true);
        }
    }
}
#endif