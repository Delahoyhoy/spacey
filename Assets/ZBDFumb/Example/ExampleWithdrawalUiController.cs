#if USE_FUMB_ZEBEDEE
using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace ZBDFumb.Example {
    public class ExampleWithdrawalUiController : MonoBehaviour {
        [SerializeField]
        private TextMeshProUGUI textVerificationRequiredInfo;
        [SerializeField]
        private TextMeshProUGUI textWithdrawlAuthState;
        [SerializeField]
        private TextMeshProUGUI textGamerTag;

        [SerializeField]
        private UnityEvent displayNotLoggedInEvent;
        [SerializeField]
        private UnityEvent displayLoggedInEvent;

        private void Start() {
            ZBDAuthWithdrawalManager.Instance.authStateUpdatedAction += OnAuthStateUpdated;
            ZBDAuthWithdrawalManager.Instance.gotVerificationRequiredInfoAction += OnGotVerificationRequired;
            
            RefreshDisplay();
        }

        private void OnDestroy() {
            ZBDAuthWithdrawalManager.Instance.authStateUpdatedAction -= OnAuthStateUpdated;
            ZBDAuthWithdrawalManager.Instance.gotVerificationRequiredInfoAction -= OnGotVerificationRequired;
        }

        private void OnAuthStateUpdated(WithdrawalAuthState withdrawalAuthState) {
            textWithdrawlAuthState.text = withdrawalAuthState.ToString();
        }
        
        private void OnGotVerificationRequired(VerificationRequiredInfo verificationRequiredInfo) {
            textVerificationRequiredInfo.text = verificationRequiredInfo.ToString();
        }

        public void OnLoginPressed() {
            Action<UserInfo> onSuccess = info => {
                RefreshDisplay();
            };
            Action onFail = () => {
                RefreshDisplay();
            };
            
            ZBDFumbAuthLogin.Instance.Login(true, onSuccess, onFail);
        }

        public void LogoutPressed() {
            ZBDFumbAuthLogin.Instance.Logout();
            RefreshDisplay();
        }

        public void OnCanWithdrawPressed() {
            Action<bool> onGetResult = b => {
                RefreshDisplay();
            };
            
            ZBDAuthWithdrawalManager.Instance.RequestCanWithdraw(onGetResult);
        }

        private void RefreshDisplay() {
            if (ZBDFumbAuthLogin.IsLoggedIn) {
                displayLoggedInEvent.Invoke();
            } else {
                displayNotLoggedInEvent.Invoke();
            }
            
            textGamerTag.text = string.IsNullOrEmpty(ZBDFumbAuthLogin.GamerTag) ? "null" : ZBDFumbAuthLogin.GamerTag;
        }
    }
}
#endif