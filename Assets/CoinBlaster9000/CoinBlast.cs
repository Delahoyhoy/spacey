﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CurrencyState
{
    Blasting,
    Stopped,
    Collecting,
    Collected,
}

public class CoinBlast : MonoBehaviour {

    private Vector3 direction;
    [SerializeField]
    private float maxSpeed = 10f; // The top speed the coin can travel at. Coin will reach destination in 1/speed seconds if left at a constant rate
    [SerializeField]
    private float acceleration = 1f; // How quickly the coin reaches its top speed when going to the targetPos
    private float speed;

    private float speedMultiplier = 1f, drag = 1f;

    [SerializeField]
    private Vector3 startScale = Vector3.one; // Redundant now really, but keep in case you need it!

    private CurrencyState currentState = CurrencyState.Blasting;

    [SerializeField]
    Image actualImage;
    [SerializeField]
    Sprite coinSprite;
    [SerializeField]
    Sprite gemSprite;

    // Attribute to adjust the current stat
    public CurrencyState CurrentState
    {
        get { return currentState; }
        set
        {
            currentState = value;
            // When we switch to the collecting state, reset some values for the journey!
            if (value == CurrencyState.Collecting)
            {
                totalDistance = Vector3.Distance(transform.position, targetPos);
                startTraversalPos = transform.position;
                distSoFar = 0;
                speed = 0;
            }
        }
    }
    // Where the coin should go
    private Vector3 targetPos;

    // Attribute to publicly set the target position of the coin
    public Vector3 TargetPos { set { targetPos = value; } }

    /// <summary>
    /// Setup the initial state of the coin
    /// </summary>
    /// <param name="position">The position to start at</param>
    /// <param name="dir">The direction to travel in</param>
    /// <param name="spd">The starting speed of the coin</param>
    /// <param name="drg">The rate at which the coin decelerates from the initial blast</param>
    /// <param name="speedMult">The overall multiplier to speed</param>
    public void Initialise(Vector3 position, Vector3 dir, float spd, float drg, float speedMult = 1f, bool isCoins = true)
    {
        gameObject.SetActive(true);
        transform.position = position;
        direction = dir;
        speed = spd;
        speedMultiplier = speedMult;
        drag = drg;
        transform.localScale = startScale * Random.Range(1.5f, 1.75f);
        //acceleration = drag * 4f;
        if (isCoins)
        {
            actualImage.sprite = coinSprite;
        }
        else {
            actualImage.sprite = gemSprite;
        }
        currentState = CurrencyState.Blasting;
    }

    Vector3 startTraversalPos;
    float totalDistance;
    float distSoFar = 0;

	// Update is called once per frame
	void Update () {
		switch(currentState)
        {
            case CurrencyState.Blasting:
                transform.Translate(direction * speed * Time.deltaTime); // Move outwards
                speed = Mathf.Lerp(speed, 0.1f * speedMultiplier, Time.deltaTime * drag);
                if (speed < 0.2f * speedMultiplier)
                {
                    currentState = CurrencyState.Stopped;
                    //direction = GetDirectionToTarget();
                    totalDistance = Vector3.Distance(transform.localPosition, targetPos);
                    startTraversalPos = transform.position;
                    distSoFar = 0;
                }
                break;
            case CurrencyState.Stopped:
                transform.Translate(direction * speed * Time.deltaTime); // Keep slowly drifting
                break;
            case CurrencyState.Collecting:
                //transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPos, speed);
                speed = Mathf.Lerp(speed, maxSpeed, acceleration * Time.deltaTime);
                distSoFar += (speed * Time.deltaTime);// / totalDistance;
                transform.position = Vector3.Lerp(startTraversalPos, targetPos, distSoFar);
                //transform.Translate(direction * speed * Time.deltaTime);
                if (distSoFar >= 1f)
                {
                    currentState = CurrencyState.Collected;
                    gameObject.SetActive(false);
                }
                break;
        }
	}
}
