﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Currency explosion controller
/// Wow, I did comments for once!
/// Reworked to function on "Screen Space - Overlay" canvas
/// </summary>
public class CurrencyExplosion : MonoBehaviour {

    [SerializeField]
    private CoinBlast coinPrefab; // The coin prefab to spawn
    [SerializeField]
    private CoinBlast secondCoinPrefab; // The coin prefab to spawn

    [Header("Select target destination for your coins!")]
    [SerializeField]
    public Transform target; // Transform of where to send them...If you need, you could set up an empty GameObject on your UI

    private Queue<CoinBlast> pool = new Queue<CoinBlast>();

    [Header("Customise the blast here!")]
    [Tooltip("Change this value to make the coins go faster or slower")]
    [SerializeField]
    private float startSpeedMult = 100f; // Handle how fast you want your coins to go...if they're flying off the screen, try lowering this value!

    // Debug
    float nextExplosion;

    public static CurrencyExplosion Instance;
    public static bool InstanceExists { get { return Instance != null; } }

    /// <summary>
    /// Coin pooling and initialisation
    /// </summary>
    /// <param name="pos">The start position of the coin</param>
    /// <param name="dir">The start direction for the coin to travel in</param>
    /// <param name="speed">The start speed of this coin</param>
    /// <param name="drag">How quickly the coin will slow down</param>
    /// <returns></returns>
    private CoinBlast NewCoin(Vector3 pos, Vector3 dir, float speed, float drag, bool isCoins = true)
    {
        CoinBlast newCoin = null;
        if (pool.Count > 0)
        {
            newCoin = pool.Dequeue();
        }
        else
        {
            newCoin = Instantiate(coinPrefab, transform);
            newCoin.TargetPos = target.position;
        }
        newCoin.Initialise(pos, dir, speed, drag, startSpeedMult, isCoins);
        return newCoin;
    }

    /// <summary>
    /// Activate a coin blast from a given position
    /// </summary>
    /// <param name="position">The origin of the blast</param>
    public void CreateNewBlast(Vector3 position, bool _isCoins = true)
    {
        StartCoroutine(NewBlast(position, _isCoins));
    }

    /// <summary>
    /// Create an explosion of coins all at once!
    /// </summary>
    /// <param name="position">The origin of the blast</param>
    /// <returns></returns>
    IEnumerator NewBlast(Vector3 position, bool isCoins = true)
    {
        List<CoinBlast> coinsInBlast = new List<CoinBlast>();
        for (float a = 0; a < 360f; a++)
        {
            Vector3 direction = new Vector3(Mathf.Sin(a), Mathf.Cos(a), 0);
            coinsInBlast.Add(NewCoin(position, direction, Random.Range(1f, 2f) * startSpeedMult, Random.Range(3f, 6f), isCoins)); // <-- This is where the coin is set up
            a += Random.Range(20f, 40f);
        }
        bool waitForStop = true;
        while (waitForStop)
        {
            // Check all of the coins have "stopped" before going to the next stage
            for (int i = 0; i < coinsInBlast.Count; i++)
            {
                if (coinsInBlast[i].CurrentState == CurrencyState.Stopped)
                    waitForStop = false;
                else
                {
                    waitForStop = true;
                    break;
                }
            }
            yield return null;
        }
        yield return new WaitForSeconds(.25f);
        // Wait before launching them at the destination
        for (int i = 0; i < coinsInBlast.Count; i++)
            coinsInBlast[i].CurrentState = CurrencyState.Collecting;
        while (coinsInBlast.Count > 0)
        {
            // Systematically remove each coin as it reaches the destination!
            for (int i = 0; i < coinsInBlast.Count;)
            {
                if (coinsInBlast[i].CurrentState == CurrencyState.Collected)
                {
                    pool.Enqueue(coinsInBlast[i]);
                    coinsInBlast.RemoveAt(i);
                }
                else
                    i++;
            }
            yield return null;
        }
        // All done!
        //Debug.Log("Coroutine finished");
    }

	// Use this for initialization
	void Awake () {
        // Debug
        //nextExplosion = Time.time + 5f;
        Instance = this;
    }

    /// <summary>
    /// Just used for testing on Screen Space UI
    /// </summary>
    public void BlastAtRandom()
    {
        Vector3 randomPos = new Vector3(Random.Range(0, Screen.width * transform.root.localScale.x), Random.Range(0, Screen.height * transform.root.localScale.y), 0);
        CreateNewBlast(randomPos);
    }

    // Update is called once per frame
    void Update () {
        // Debug
        //if (Time.time > nextExplosion)
        //{
        //    StartCoroutine(NewBlast(Vector3.zero));
        //    nextExplosion = Time.time + 5f;
        //}
        if (Input.GetKeyDown(KeyCode.I))
        {
            BlastAtRandom();
        }
    }
}
