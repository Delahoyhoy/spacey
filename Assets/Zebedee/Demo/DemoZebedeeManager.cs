using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Zebedee;

public class DemoZebedeeManager : MonoBehaviour
{
    [SerializeField]
    private InputField gamertagInput;

    [SerializeField]
    private Text currentSats, buttonText;
    
#if USE_FUMB_ZEBEDEE
    private void Awake()
    {
        ZebedeeManager.Instance.OnSatsValueChange += SetCurrentSats;

    }

    private void Start()
    {
        if (ZebedeeManager.Instance.HasStoredGamertag)
            gamertagInput.text = ZebedeeManager.Instance.Gamertag;
        StartCoroutine(CheckWithdrawalCoolDowns());
    }

    private void SetCurrentSats(int sats)
    {
        currentSats.text = sats.ToString();
        int maxWithdrawAmount = ZebedeeManager.Instance.MaxWithdrawAmount;
        buttonText.text = maxWithdrawAmount > 0 && sats > maxWithdrawAmount ? $"Withdraw {maxWithdrawAmount}" : "Withdraw";
    }

    public void WithdrawAll()
    {
        ZebedeeManager.Instance.CashOut(gamertagInput.text, ZebedeeManager.Instance.Satoshis);
    }

    public void OpenDiscord()
    {
        ZebedeeManager.Instance.OpenDiscord();
    }

    public void DownloadZebedee()
    {
        ZebedeeManager.Instance.DownloadZebedee();
    }

    private IEnumerator CheckWithdrawalCoolDowns()
    {
        while (true)
        {
            yield return new WaitForSeconds(10f);
            ZebedeeManager.Instance.CheckTimeOuts();
            int sats = ZebedeeManager.Instance.Satoshis;
            int maxWithdrawAmount = ZebedeeManager.Instance.MaxWithdrawAmount;
            buttonText.text = sats > maxWithdrawAmount ? $"Withdraw {maxWithdrawAmount}" : "Withdraw";
        }
    }
    
#endif
}
