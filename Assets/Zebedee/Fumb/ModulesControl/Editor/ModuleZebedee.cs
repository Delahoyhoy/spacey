using System.Collections.Generic;
#if USE_FUMB_ZEBEDEE

#endif
using Fumb.Editor;
using Zebedee;
using Fumb.ExternalPackages;

namespace Fumb.Modules
{

    public class ModuleZebedee : FumbModule
    {
        public override string ScriptingDefine
        {
            get { return "USE_FUMB_ZEBEDEE"; }
        }
        
        public override ExternalPackageType[] ExternalPackageTypes {
            get {
                return new ExternalPackageType[] { ExternalPackageType.UnbiasedTime };
            }
        }

        protected override List<FumbModuleIncompleteStep> IncompleteSetupSteps
        {
            get
            {
                List<FumbModuleIncompleteStep> incompleteSetupSteps = new List<FumbModuleIncompleteStep>();
#if USE_FUMB_ZEBEDEE
                ZebedeeSettings settings = EditorFumbResourcesHelper.GetResourcesObject<ZebedeeSettings>(false);

                if (settings == null)
                {
                    incompleteSetupSteps.Add(new FumbModuleIncompleteStep("No settings object found, create one now.", EditorZebedee.GoToSettings));
                }
#endif
                return incompleteSetupSteps;
            }
        }

        public override string ExampleScenePath => "Assets/Zebedee/Demo/ZebedeeDemo";

        protected override FumbModule[] RequiredSupportingModules => new[] { new ModuleSave() };
    }
}
