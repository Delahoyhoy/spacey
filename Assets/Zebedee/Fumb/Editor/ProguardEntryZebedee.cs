#if UNITY_ANDROID
using System.Collections.Generic;

namespace Fumb.Build {
    public class ProguardEntryZebedee : ProguardEntry {
        public override List<string> Entries {
            get {
                return new List<string>() {
                    "-keep class com.nekolaboratory.** { *; }"
                };
            }
        }

        public override bool RequiresEntries {
            get {
#if USE_FUMB_ZEBEDEE
                return true;
#else
                return false;
#endif
            }
        }
    }
}
#endif