#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Zebedee.Security
{
    public static class QuickHexHash
    {
        // Fixed values, do not change these once you have set them
        private static HashBaseValue[] hashBaseValues =
        {
            new HashBaseValue(7, 72, 1023),
            new HashBaseValue(13, 129, 1137),
            new HashBaseValue(15, 142, 1321),
        };

        // Scrambled digits, do not change these once set up
        private static char[] digits =
        {
            'f', '6', '7', '0', '2', 'c', 'e', '8', '9', '3',
            '5', 'd', 'b', 'a', '1', '4', 'g', 'h', 'i', 'j'
        };

        private const int TargetLength = 24;

        private static string Encode(int value, int baseNum)
        {
            string encoded = "";

            int remaining = value;

            int iterations = 0;

            while (remaining > 0)
            {
                encoded += digits[remaining % baseNum];
                remaining /= baseNum;

                iterations++;
                if (iterations > 1000)
                {
                    Debug.LogError("Too many iterations whilst encoding!");
                    break;
                }
            }

            string finalValue = "";
            for (int i = 0; i < encoded.Length; i++)
                finalValue += encoded[encoded.Length - 1 - i];

            return finalValue;
        }

        private static int Decode(string encoded, int baseNum)
        {
            List<char> encodingTable = new List<char>(digits);
            int total = 0;
            for (int i = encoded.Length - 1; i >= 0; i--)
            {
                if (!encodingTable.Contains(encoded[i]))
                    continue;
                int digit = encodingTable.IndexOf(encoded[i]);
                int level = encoded.Length - 1 - i;
                total += digit * (int) Mathf.Pow(baseNum, level);
            }

            return total;
        }

        public static string MultipliedEncode(int value, int baseNum, int addition, int mult)
        {
            return Encode((value + addition) * mult, baseNum);
        }

        public static int MultipliedDecode(string encoded, int baseNum, int addition, int mult)
        {
            return (Decode(encoded, baseNum) / mult) - addition;
        }

        public static string PadAndJumble(int value, int baseNum, int addition, int mult)
        {
            string finalValue = "";
            string core = MultipliedEncode(value, baseNum, addition, mult);
            int lengthToAdd = TargetLength - core.Length;
            char separatorChar = digits[baseNum];
            int padBefore = Random.Range(4, lengthToAdd - 4);
            for (int i = 0; i < padBefore; i++)
                finalValue += digits[Random.Range(0, baseNum)];

            finalValue += separatorChar;
            finalValue += core;
            finalValue += separatorChar;

            while (finalValue.Length < TargetLength)
                finalValue += digits[Random.Range(0, baseNum)];

            return finalValue;
        }

        public static bool ValidateAndDecode(string jumbled, int baseNumber, int addition, int mult, out int result)
        {
            char separatorChar = digits[baseNumber];
            result = 0;

            if (!jumbled.Contains(separatorChar.ToString()))
                return false;

            string[] separated = jumbled.Split(separatorChar);

            if (separated.Length != 3)
                return false;

            result = MultipliedDecode(separated[1], baseNumber, addition, mult);
            return true;
        }

        public static List<string> ObtainSaveValues(int value)
        {
            List<string> output = new List<string>();
            for (int i = 0; i < hashBaseValues.Length; i++)
            {
                HashBaseValue hashBaseValue = hashBaseValues[i];
                output.Add(PadAndJumble(value, hashBaseValue.baseNumber, hashBaseValue.addition,
                    hashBaseValue.multiplier));
            }

            return output;
        }

        public static int CheckSaveValues(List<string> savedValues, int expectedValue, out bool matchSuccess)
        {
            matchSuccess = true;
            if (savedValues == null)
                return 0;
            int minValueObtained = expectedValue;
            for (int i = 0; i < hashBaseValues.Length; i++)
            {
                HashBaseValue hashBaseValue = hashBaseValues[i];
                if (savedValues.Count > i && ValidateAndDecode(savedValues[i], hashBaseValue.baseNumber,
                    hashBaseValue.addition, hashBaseValue.multiplier, out int result))
                {
                    if (savedValues[i].Length != TargetLength)
                        matchSuccess = false;
                    if (result != expectedValue)
                        matchSuccess = false;
                    minValueObtained = Mathf.Min(minValueObtained, result);
                }
                else
                    minValueObtained = 0;
            }

            return minValueObtained;
        }
    }

}
#endif