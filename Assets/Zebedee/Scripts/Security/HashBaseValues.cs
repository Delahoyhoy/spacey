#if USE_FUMB_ZEBEDEE
using System;

namespace Zebedee.Security
{
    [Serializable]
    public struct HashBaseValue
    {
        public int baseNumber;
        public int addition;
        public int multiplier;

        public HashBaseValue(int baseNum, int add, int mult)
        {
            baseNumber = baseNum;
            addition = add;
            multiplier = mult;
        }
    }
}
#endif
