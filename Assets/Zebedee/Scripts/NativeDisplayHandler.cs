using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fumb.NativeDialog;

namespace Zebedee.Display
{
    public class NativeDisplayHandler : MonoBehaviour
    {
        public void ShowError(string error)
        {
            Dialog.OpenDialog("ERROR", error);
        }

        public void ShowSuccess(int amount)
        {
            Dialog.OpenDialog("SUCCESS!", $"Congratulations, {amount} Sats have been sent to your Zebedee Wallet!");
        }
    }
}
