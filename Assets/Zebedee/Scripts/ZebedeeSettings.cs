using System.Collections;
using System.Collections.Generic;
using Fumb.Attribute;
using UnityEngine;

namespace Zebedee
{
    public class ZebedeeSettings : ScriptableObject
    {
        [SerializeField]
        private string middlewareUrl;

        public string MiddlewareURL => middlewareUrl;

        [Header("Social and Attribution")]
        [SerializeField]
        private string attributionUrl;

        public string AttributionUrl => attributionUrl;

        [SerializeField]
        private string discordUrl;

        public string DiscordUrl => discordUrl;

        [Header("Local Withdrawal Limit Options")]
        [SerializeField]
        private bool useLocalWithdrawalLimit;

        public bool UseLocalWithdrawalLimit => useLocalWithdrawalLimit;

        [SerializeField]
        private int satsWithdrawalLimit;

        public int SatsWithdrawLimit => useLocalWithdrawalLimit ? satsWithdrawalLimit : 0;
        public int SatsDailyLimit => satsWithdrawalLimit;

        [SerializeField]
        [ConditionalField("useLocalWithdrawalLimit")]
        private int satsWithdrawalLimitPeriod;

        public int SatsWithdrawalLimitPeriod => useLocalWithdrawalLimit ? satsWithdrawalLimitPeriod : 0;

        [Header("Authentication Settings")]
        [SerializeField]
        private string authenticationBaseUrl;

        public string AuthenticationBaseURL => authenticationBaseUrl;

        [SerializeField]
        private string authenticationClientId;

        public string AuthenticationClientId => authenticationClientId;

        [SerializeField]
        private string authenticationRedirectUrl;

        public string AuthenticationRedirectUrl => authenticationRedirectUrl;

        public ZebedeeSettings() // Default settings
        {
            authenticationBaseUrl = middlewareUrl = "https://game-middleware.herokuapp.com";
            attributionUrl = "https://zeb.gg/fumb-bitcoinminer";
            discordUrl = "https://discord.gg/WMawgaZzDB";

            authenticationClientId = "ca97c7b4-aa88-4ee7-abe7-5e92d27bd75c";
            authenticationRedirectUrl = "bitcoinminer://authorize";
            
            useLocalWithdrawalLimit = false;
            satsWithdrawalLimit = 500;
            satsWithdrawalLimitPeriod = 86400;
        }
    }
}
