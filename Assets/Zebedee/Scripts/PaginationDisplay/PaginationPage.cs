using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PaginationPage : MonoBehaviour
{
    [SerializeField]
    private UnityEvent<float> updatePosValue;

    private float position;

    public void Setup(float pos)
    {
        position = pos;
    }

    public void UpdatePos(float pos, float offset)
    {
        float value = 1 - Mathf.Clamp01(Mathf.Abs(-position - pos) / offset);
        updatePosValue?.Invoke(value);
    }
}
