using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

public class PaginationScreen : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private int totalIndices = 3;
    private int currentIndex = 0;

    [SerializeField]
    private Image selectionPrefab;
    [SerializeField]
    private Transform selectionHolder;
    private List<Image> selections = new List<Image>();
    [SerializeField]
    private Button nextButton;

    [SerializeField]
    private UnityEvent<float> setScrollPercent;

    private float dragStartTime;
    private Vector2 dragStartPos;

    public void Next()
    {
        if (dragging)
            return;
        currentIndex = Mathf.Clamp(currentIndex + 1, 0, totalIndices - 1);
        //AudioManager.Play("Click", 0.25f);
        SetupForCurrent();
    }

    public void Prev()
    {
        if (dragging)
            return;
        currentIndex = Mathf.Clamp(currentIndex - 1, 0, totalIndices - 1);
        //AudioManager.Play("Click", 0.25f);
        SetupForCurrent();
    }
    
    [SerializeField]
    private List<PaginationPage> allPages = new List<PaginationPage>();

    [SerializeField]
    private float offset;

    [SerializeField]
    private GameObject screenObject;

    [SerializeField]
    private Transform holder;

    private bool setup = false, dragging = false;

    private int currentNearestIndex = 0;
    private int CurrentIndex => currentIndex;

    public void Setup()
    {
        if (!setup)
        {
            for (int i = 0; i < allPages.Count; i++)
            {
                //newCard.Setup(i);
                PaginationPage page = allPages[i];
                page.Setup(offset * i);
                Image newSelection = Instantiate(selectionPrefab, selectionHolder);
                selections.Add(newSelection);
                newSelection.gameObject.SetActive(true);
                page.gameObject.SetActive(true);
                int index = i;
                page.transform.localPosition = new Vector3(offset * index, 0, 0);
                page.transform.localScale = Vector3.one;
            }
            totalIndices = allPages.Count;
            currentIndex = 0;
            SetupForCurrent();
            setup = true;
        }
        else
        {
            // for (int i = 0; i < allPages.Count; i++)
            // {
            //     allPages[i].Setup(i);
            // }
            SetupForCurrent();
        }
    }

    private void SetupForCurrent()
    {
        int current = CurrentIndex;
        nextButton.gameObject.SetActive(currentIndex < totalIndices - 1);
        for (int i = 0; i < selections.Count; i++)
        {
            Color color = selections[i].color;
            color.a = i == current ? 1f : 0.25f;
            selections[i].color = color;
        }
        //levelUpButton.color = levelUpAvailable ? activeColour : inactiveColour;
        //levelUpText.text = GetButtonText();
    }

    public void OpenOnIndex(int index)
    {
        Setup();
        if (index < 0)
        {
            OpenScreen();
            return;
        }
        if (gameObject.activeSelf)
        {
            // if (CurrentIndex != index)
            //     BasicSoundManager.Instance.PlayLittleWoosh();
            //     AudioManager.Play("Swipe", 0.33f);
        }
        else
            OpenScreen();
        currentIndex = index;
        SetupForCurrent();
        Vector3 currentPos = holder.localPosition;
        holder.transform.localPosition = new Vector3(-currentIndex * offset,
            currentPos.y, currentPos.z);
    }

    public void NavigateToIndex(int index)
    {
        currentIndex = index;
        SetupForCurrent();
    }

    public void CloseScreen()
    {
        screenObject.SetActive(false);
        //AudioManager.Play("Click", 0.5f);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        dragging = true;
        currentNearestIndex = currentIndex;
        dragStartTime = Time.unscaledTime;
        dragStartPos = eventData.position;
        //AudioManager.Play("Click", 0.25f);
        //startPos = holder.localPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!dragging)
            return;
        holder.Translate(Vector3.right * eventData.delta.x);
        int nearest = GetNearestIndex();
        if (nearest != currentNearestIndex)
        {
            //AudioManager.Play("Swipe", 0.33f);
            //BasicSoundManager.Instance.PlayLittleWoosh();
            currentNearestIndex = nearest;
            //SortRequests(availableIndices[currentNearestIndex]);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!dragging)
            return;
        dragging = false;
        //Debug.Log(eventData.delta.x);
        if (Vector2.Distance(eventData.position, eventData.pressPosition) > (Screen.width / 20f) && Time.unscaledTime < dragStartTime + 0.3f)
        {
            currentIndex = GetNearestIndex(eventData.position.x > eventData.pressPosition.x ? -1 : 1);
        }
        else
            currentIndex = GetNearestIndex();
        if (currentNearestIndex != currentIndex)
        {
            //AudioManager.Play("Swipe", 0.33f);
            //BasicSoundManager.Instance.PlayLittleWoosh();
        }
        SetupForCurrent();
        //BasicSoundManager.Instance.PlayCloseSound();
        //AudioManager.Play("Click", 0.25f);
    }

    public void OpenScreen()
    {
        // run any screen open actions
    }

    private int GetNearestIndex(int indexOffset = 0)
    {
        int nearest = Mathf.RoundToInt(-holder.localPosition.x / offset);
        if (indexOffset > 0)// && ((-nearest - (indexOffset * 0.33f)) * offset) < -holder.localPosition.x)
            return Mathf.Clamp(nearest + indexOffset, 0, Mathf.Min(totalIndices - 1, currentIndex + 1));
        if (indexOffset < 0)// && ((-nearest + (indexOffset * 0.33f)) * offset) > -holder.localPosition.x)
            return Mathf.Clamp(nearest + indexOffset, Mathf.Max(currentIndex - 1, 0), totalIndices - 1);
        return Mathf.Clamp(nearest, 0, totalIndices - 1);
    }

    // Update is called once per frame
    private void Update()
    {
        if (!dragging)
            holder.localPosition = Vector3.Lerp(holder.localPosition, new Vector3(-currentIndex * offset, 
                holder.localPosition.y, holder.localPosition.z), Time.unscaledDeltaTime * 12f);
        for (int i = 0; i < allPages.Count; i++)
        {
            allPages[i].UpdatePos(holder.localPosition.x, offset);
        }
        setScrollPercent?.Invoke(1f - (holder.localPosition.x / offset));
    }
}
