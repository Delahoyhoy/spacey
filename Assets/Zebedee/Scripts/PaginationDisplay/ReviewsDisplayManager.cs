using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Zebedee.Display
{
    [Serializable]
    public class ReviewDisplayData
    {
        [Range(0, 5)]
        public int stars = 5;
        [Multiline]
        public string reviewText;
    }
    
    public class ReviewsDisplayManager : MonoBehaviour
    {
        [SerializeField]
        private ReviewDisplay reviewPrefab;

        [SerializeField]
        private RectTransform reviewHolder;
        
        [SerializeField]
        private ReviewDisplayData[] displayData;
        
        private bool setup;

        private void Start()
        {
            if (setup)
                return;

            for (int i = 0; i < displayData.Length; i++)
            {
                ReviewDisplay newDisplay = Instantiate(reviewPrefab, reviewHolder);
                newDisplay.Setup(displayData[i].stars, displayData[i].reviewText);
            }
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(reviewHolder);

            setup = true;
        }
    }
}
