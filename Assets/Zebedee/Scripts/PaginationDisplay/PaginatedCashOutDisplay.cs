#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Fumb.Save;
using Fumb.UI;

namespace Zebedee.Display
{
    public class PaginatedCashOutDisplay : UiScreen
    {
        [SerializeField]
        private UnityEvent<string> onSetCurrentSats;

        [SerializeField]
        private UnityEvent<int> onSetWithdrawLimit;

        [SerializeField]
        private PaginationScreen screen;
        
        [ManagedSaveValue("PaginatedCashOutDisplayHasOpened")]
        private SaveValueBool saveHasOpened;

        [SerializeField]
        private int cashOutPage = 2;

        private bool setup = false;

        protected override void StartTransitionIn(bool instant = false, Action onComplete = null)
        {
            screen.OpenOnIndex(saveHasOpened.Value ? cashOutPage : 0);
            saveHasOpened.Value = true;
            base.StartTransitionIn(instant, onComplete);
        }

        public void PressRedeem()
        {
            ZebedeeManager.Instance.CashOut();
        }

        private void Start()
        {
            if (setup)
                return;
            onSetWithdrawLimit?.Invoke(ZebedeeManager.Instance.DailyLimit);
            ZebedeeManager.Instance.OnSatsValueChange += UpdateCurrentSats;
            UpdateCurrentSats(ZebedeeManager.Instance.Satoshis);
            setup = true;
        }

        public void UpdateCurrentSats(int sats)
        {
            onSetCurrentSats?.Invoke(sats.ToString());
        }

        public void DownloadZebedee()
        {
            ZebedeeManager.Instance.DownloadZebedee();
        }

        public void OpenDiscord()
        {
            ZebedeeManager.Instance.OpenDiscord();
        }
    }
}

#endif