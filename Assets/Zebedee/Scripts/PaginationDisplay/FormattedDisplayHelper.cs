using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Zebedee.Display
{
    public class FormattedDisplayHelper : MonoBehaviour
    {
        [SerializeField]
        private string format;
        
        [SerializeField]
        public UnityEvent<string> onSetDisplayText;

        public void SetValue(int value)
        {
            onSetDisplayText?.Invoke(string.Format(format, value));
        }
    }
}
