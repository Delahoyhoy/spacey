using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Zebedee.Display
{
    public class ReviewDisplay : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] starDisplays;

        [SerializeField]
        private UnityEvent<string> onSetReviewText;

        public void Setup(int stars, string reviewText)
        {
            onSetReviewText?.Invoke(reviewText);
            for (int i = 0; i < starDisplays.Length; i++)
                starDisplays[i].SetActive(i < stars);
        }
    }    
}

