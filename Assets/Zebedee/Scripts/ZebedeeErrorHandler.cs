#if USE_FUMB_ZEBEDEE
using System;
using System.Collections.Generic;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;

namespace Zebedee.Error
{
    public enum ZebedeeErrorFormat
    {
        NoFormat,
        Milliseconds,
    }
    
    [Serializable]
    public class ZebedeeError
    {
        public string errorId;
        [Multiline]
        public string errorMessage;

        public bool formatted;
        [ConditionalField("formatted")]
        public ZebedeeErrorFormat format;
        
        public bool IsError(string id, string metaData, out string message)
        {
            message = "";
            if (string.IsNullOrEmpty(id))
                return false;
            if (!id.Equals(errorId))
                return false;
            if (formatted)
                message = string.Format(errorMessage, GetFormat(metaData, format));
            else
                message = errorMessage;
            return true;
        }

        private static string GetFormat(string data, ZebedeeErrorFormat format)
        {
            switch (format)
            {
                case ZebedeeErrorFormat.NoFormat:
                    return data;
                case ZebedeeErrorFormat.Milliseconds:
                    if (!string.IsNullOrEmpty(data) &&
                        double.TryParse(data, out double totalMilliseconds))
                    {
                        double seconds = totalMilliseconds / 1000d;
                        if (seconds < 120)
                            return $"{Math.Ceiling(seconds)} seconds";
                        double minutes = seconds / 60d;
                        if (minutes < 60)
                            return $"{Math.Ceiling(minutes)} minutes";
                        double hours = minutes / 60d;
                        return hours < 1.5d ? "1 hour" : $"{Math.Round(hours)} hours";
                    }
                    return "24 hours";
            }

            return data;
        }
    }

    [Serializable]
    public class ZebedeeErrorEvent : UnityEvent<string> { }

    [Serializable]
    public class CustomZebedeeError : ZebedeeError
    {
        public ZebedeeErrorEvent onError;

        public bool CheckError(string id, string metaData)
        {
            if (!IsError(id, metaData, out string message))
                return false;
            onError?.Invoke(message);
            return true;
        }
    }
    
    public class ZebedeeErrorHandler : MonoBehaviour
    {
        [SerializeField]
        private ZebedeeError[] errors;

        [SerializeField]
        private CustomZebedeeError[] customErrors;

        [SerializeField]
        private ZebedeeErrorEvent defaultErrorEvent;
        
        public static ZebedeeErrorHandler Instance;

        private void Awake()
        {
            Instance = this;
        }

        public void HandleError(string error, string defaultMessage, string data)
        {
            for (int i = 0; i < customErrors.Length; i++)
            {
                if (customErrors[i].CheckError(error, data))
                    return;
            }

            for (int i = 0; i < errors.Length; i++)
            {
                if (!errors[i].IsError(error, data, out string message)) 
                    continue;
                defaultErrorEvent?.Invoke(message);
                return;
            }
            
            defaultErrorEvent?.Invoke(defaultMessage);
        }
        
        public bool GetErrorMessage(string error, string data, out string message)
        {
            message = "";
            for (int i = 0; i < errors.Length; i++)
            {
                if (errors[i].IsError(error, data, out message))
                    return true;
            }
            return false;
        }
    }
}
#endif
