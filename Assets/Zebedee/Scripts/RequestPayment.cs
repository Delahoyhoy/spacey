#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using System.Collections.Generic;
using Fumb;
using Fumb.PlayerStats;
using Fumb.Authentication;
using Fumb.CheatDetection;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using ZBDFumb;

namespace Zebedee
{
    public class JsonPacket
    {
        public string data;
        public string iv;
        public string hash;
    }

    public class ActionResponse
    {
        public bool error;
        public string response;
        public string type;
        public string data;
        public long responseCode;
        public bool verify;
        public string amount;
    }

    public class WithdrawRequest
    {
        public int amount;
        public int withdrawCount;
        public int timePlayed;
        public string gamerTag;
        public string description;
        public string deviceId;
        public string nonce;
        public string isDevice;
        public string isGoogelePlay;
        public string isGooglePlay;
        public string isDebuggable;
        public string signature;
        public string appId;
        public string versionNumber;
        public string deviceInfo;
        public string authToken;

        // Add any additional info to be sent here:
        // public string someData;
    }


    public class RequestPayment : MonoBehaviour
    {
        public static RequestPayment Instance;
        public Texture2D ceaserImage;

        [HideInInspector]
        public SimpleAES encryptor;

        [HideInInspector]
        public string url;
        public bool useLocal;
        Action<ActionResponse> sendPaymentCallback;

        private ZebedeeSettings settings;
        private ZebedeeSettings Settings => settings ??= FumbResourcesHelper.LoadObject<ZebedeeSettings>();
        
        private void Awake()
        {
            encryptor = new SimpleAES();
            Instance = this;
            url = useLocal ? "http://localhost:8000" : Settings.MiddlewareURL;
        }

        private void OnDestroy()
        {
            Destroy(Instance);
        }

        string encryptStuff(System.Object data)
        {
            string myjson = JsonUtility.ToJson(data);
            byte[] ivBytes = SimpleAES.GenerateEncryptionVector();
            var encrypted = encryptor.Encrypt(myjson, ivBytes);

            JsonPacket myData = new JsonPacket();

            myData.data = encrypted;

            string ivString = SimpleAES.ConvertByteArrayToString(ivBytes);
            myData.iv = ivString;
            myData.hash = encryptor.hash();


            return JsonUtility.ToJson(myData);
        }

        string decryptStuff(JSONNode data)
        {
            string encryptedData = data["data"].Value;
            string ivString = data["iv"].Value;
            byte[] ivBytes = SimpleAES.ConvertStringToByteArray(ivString);

            string dec = encryptor.Decrypt(encryptedData, ivBytes);

            return dec;
        }

        public void SendPayment(int amount, string gamerTag, string description, Action<ActionResponse> callback)
        {
            sendPaymentCallback = callback;
            StartCoroutine(ContinueSendPayment(amount, gamerTag, description));
            return;
        }

        private IEnumerator ContinueSendPayment(int amount, string gamerTag, string description)
        {
            Guid myuuid = Guid.NewGuid();
            WithdrawRequest myObject = new WithdrawRequest();
            myObject.amount = amount;
            double totalSecondsPlayed = PlayerStatsManager.GetPlayerStat<PlayerStatTimePlayed>().GetTimePlayed().TotalSeconds;
            if (totalSecondsPlayed > int.MaxValue)
                myObject.timePlayed = int.MaxValue;
            else
                myObject.timePlayed = (int) Math.Floor(totalSecondsPlayed);

            myObject.gamerTag = gamerTag;
            myObject.description = description;
            myObject.nonce = myuuid.ToString();
            myObject.deviceId = SystemInfo.deviceUniqueIdentifier;
            myObject.isDevice = CheatDetection.Instance.IsDevice();
            myObject.isDebuggable = CheatDetection.Instance.IsDebuggable();
            string isGooglePlay = CheatDetection.Instance.IsGooglePlay();
            myObject.isGooglePlay = isGooglePlay;
            myObject.isGoogelePlay = isGooglePlay;
            myObject.signature = CheatDetection.Instance.GetSignature();
            myObject.appId = Application.identifier;
            myObject.versionNumber = Application.version;
            myObject.deviceInfo = DeviceInfo.GetDeviceInfo();
            myObject.authToken = ZBDAuthWithdrawalManager.CurrentAuthToken;

            // Configure any additional data here:
            // myObject.someData = DataHandler.GetSomeData();

            byte[] postData = System.Text.Encoding.UTF8.GetBytes(encryptStuff(myObject));
            Debug.Log(url + "/api/v1/zbd/create-transaction");

            var request = new UnityWebRequest(url + "/api/v1/zbd/create-transaction", "POST");

            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(postData);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();
            ActionResponse aR = new ActionResponse();
            aR.responseCode = request.responseCode;

            if (request.isNetworkError || request.isHttpError)
            {
                aR.error = true;
                try
                {
                    JSONNode data = JSON.Parse(request.downloadHandler.text);

                    if (data["message"].Value != null)
                    {
                        aR.response = data["message"].Value;
                    }
                    else
                    {
                        aR.response = request.downloadHandler.text;
                    }
                }
                catch (Exception e)
                {
                    aR.response = request.error;
                }

                sendPaymentCallback(aR);
            }
            else
            {
                JSONNode data = JSON.Parse(request.downloadHandler.text);
                string dec = decryptStuff(data);
                data = JSON.Parse(dec);
                aR.error = false;
                Debug.Log(dec);
                if (data["error"].AsBool == true)
                {
                    aR.error = true;
                    aR.type = data["type"].Value;
                    aR.response = data["message"].Value;
                    aR.data = data["data"].Value;
                }
                else
                {
                    aR.type = data["type"].Value;
                    aR.amount = data["data"]["amount"].Value;
                    aR.response = dec;
                }

                sendPaymentCallback(aR);
            }
        }
    }
}
#endif
