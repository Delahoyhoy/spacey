#if USE_FUMB_ZEBEDEE
using System;
using System.Collections.Generic;
using Fumb.Serialization;
using Fumb.Save;

namespace Zebedee
{
    [Serializable]
    public class SaveValueCashOutTimeOuts : SaveValue<List<ZebedeeCashOutDelay>>
    {
    }

    [Serializable]
    public class ZebedeeCashOutDelay
    {
        public DateTime CashOutDateTime
        {
            get => DeserializeDateTime(cashOutDateTime, UnbiasedTime.Instance.Now());
            set => cashOutDateTime = SerializeDateTime(value);
        }

        public string cashOutDateTime;
        public int satsClaimed;

        public ZebedeeCashOutDelay(DateTime claimTime, int sats)
        {
            CashOutDateTime = claimTime;
            satsClaimed = sats;
        }

        public static DateTime DeserializeDateTime(string serialized, DateTime defaultValue)
        {
            long tmp = 0;
            if (string.IsNullOrEmpty(serialized))
                return defaultValue;
            tmp = Convert.ToInt64(serialized);
            if (tmp == 0)
                return defaultValue;
            return DateTime.FromBinary(tmp);
        }

        public static string SerializeDateTime(DateTime time)
        {
            return time.ToBinary().ToString();
        }
    }
}
#endif