﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatDetection : MonoBehaviour
{
    public static CheatDetection Instance;
    // Start is called before the first frame update
    void Start()
    {

    }

    public string GetSignature()
    {
#if UNITY_ANDROID
        string signature = string.Empty;
        return FSG.AndroidSecurity.IsAppSignatureValid(out signature);
#endif

        return "na";  //not android so cant check
    }

    public string IsDebuggable()
    {
#if UNITY_ANDROID
#if RELEASE
if (FSG.AndroidSecurity.IsDebuggable())
{
    return "true";
}else{
return "false";
}
#endif
#endif

        return "na";  //not android so cant check

    }

    public string IsGooglePlay()
    {

#if UNITY_ANDROID
        if (FSG.AndroidSecurity.VerifyGooglePlayInstaller())
        {
            return "true";
        }
        else
        {
            return "false";
        }


#endif

        return "na";  //not android so cant check

    }




    private void Awake()
    {
        Instance = this;
    }


    public string IsDevice()
    {

#if UNITY_EDITOR

        return "true";
#endif
#if UNITY_ANDROID
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject context = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaClass cls = new AndroidJavaClass("com.nekolaboratory.EmulatorDetector");
        bool result = cls.CallStatic<bool>("isEmulator", context);
        if(result){ // isEmulator
            return "false";
        }
        return "true";
#endif

        return "na"; //not android so cant check
    }

}
