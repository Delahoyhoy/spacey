using UnityEngine;

[System.Serializable]
public class DeviceInfo
{
    public float batteryLevel;
    public int batteryStatus;
    public string deviceModel;
    public string deviceName;
    public int deviceType;
    public string deviceUniqueIdentifier;
    public string graphicsDeviceName;
    public int graphicsMemorySize;
    public int systemMemorySize;
    public string operatingSystem;
    public int processorCount;
    public string processorType;

    public static string GetDeviceInfo()
    {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.batteryStatus = (int) SystemInfo.batteryStatus;
        deviceInfo.batteryLevel = SystemInfo.batteryLevel;
        deviceInfo.deviceModel = SystemInfo.deviceModel;
        deviceInfo.deviceName = SystemInfo.deviceName;
        deviceInfo.deviceType = (int) SystemInfo.deviceType;
        deviceInfo.deviceUniqueIdentifier = SystemInfo.deviceUniqueIdentifier;
        deviceInfo.graphicsDeviceName = SystemInfo.graphicsDeviceName;
        deviceInfo.graphicsMemorySize = SystemInfo.graphicsMemorySize;
        deviceInfo.systemMemorySize = SystemInfo.systemMemorySize;
        deviceInfo.operatingSystem = SystemInfo.operatingSystem;
        deviceInfo.processorCount = SystemInfo.processorCount;
        deviceInfo.processorType = SystemInfo.processorType;
        return JsonUtility.ToJson(deviceInfo);
    }
}
