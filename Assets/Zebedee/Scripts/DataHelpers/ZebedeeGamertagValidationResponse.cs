namespace Zebedee.Validation
{
    [System.Serializable]
    public class ZebedeeGamertagValidationData
    {
        public string id;
    }

    [System.Serializable]
    public class ZebedeeGamertagValidationResponse
    {
        public bool success;
        public ZebedeeGamertagValidationData data;
        public string message;
    }
}
