#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Fumb;
using UnityEngine;
using UnityEngine.Events;

using Fumb.General;
using Fumb.Save;
using Fumb.UI;
using ZBDFumb;
using Zebedee.Validation;
using Zebedee.Error;
using Zebedee.Security;

namespace Zebedee
{
    [SaveCategory("Zebedee")]
    public class ZebedeeManager : MonoBehaviour
    {
        [ManagedSaveValue("ZebedeeManagerCurrentSatoshis")]
        private SaveValueInt saveCurrentSatoshis;
        
        [ManagedSaveValue("ZebedeeManagerUniqueData")]
        private SaveValueListString saveUniqueData;

        [ManagedSaveValue("ZebedeeManagerGamertag")]
        private SaveValueString saveGamertag;
        
        [ManagedSaveValue("ZebedeeManagerNextWithdrawal")]
        private SaveValueDateTime saveNextWithdrawal;
        
        [ManagedSaveValue("ZebedeeManagerLockOutTimers")]
        private SaveValueCashOutTimeOuts saveCashOutTimeOuts;
        
        public Action onSaveValueMismatch; // Called when values do not successfully line up in the unique data

        private List<ZebedeeCashOutDelay> CashOutTimeOuts
        {
            get
            {
                if (!saveCashOutTimeOuts.IsPopulated)
                    saveCashOutTimeOuts.Value = new List<ZebedeeCashOutDelay>();
                return saveCashOutTimeOuts.Value;
            }
            set => saveCashOutTimeOuts.Value = value;
        }
        
        private int SatsSaveValue
        {
            get
            {
                // Hashing the code to get some of that Bitcoin
                int value = saveCurrentSatoshis.Value;
                value = QuickHexHash.CheckSaveValues(saveUniqueData.Value, value, out bool matchSuccess);
                if (!matchSuccess)
                {
                    Debug.Log("FAILED TO MATCH VALUES");
                    onSaveValueMismatch?.Invoke(); // This still may not be 100% accurate at detecting alterations
                    saveCurrentSatoshis.Value = value;
                }

                return value;
            }

            set
            {
                saveCurrentSatoshis.Value = value;
                saveUniqueData.Value = QuickHexHash.ObtainSaveValues(value);
            }
        }
        
        public void CheckTimeOuts()
        {
            DateTime now = UnbiasedTime.Instance.Now();
            List<ZebedeeCashOutDelay> timeOuts = CashOutTimeOuts;
            for (int i = 0; i < timeOuts.Count; i++)
            {
                if (timeOuts[i] == null)
                {
                    timeOuts.RemoveAt(i);
                    i--;
                    continue;
                }
                if (timeOuts[i].CashOutDateTime > now)
                {
                    if ((timeOuts[i].CashOutDateTime - now).TotalSeconds > limitPeriod)
                        timeOuts[i].CashOutDateTime = now.AddSeconds(limitPeriod); // time out should never be longer than full period
                    continue;
                }
                timeOuts.RemoveAt(i);
                i--;
            }

            CashOutTimeOuts = timeOuts;
        }
        
        private DateTime EarliestTimeOut => CashOutTimeOuts != null && CashOutTimeOuts.Count > 0 ?
            CashOutTimeOuts[0].CashOutDateTime : UnbiasedTime.Instance.Now(); 
        
        public bool HasStoredGamertag => saveGamertag.IsPopulated;
        public string Gamertag => saveGamertag.Value;

        public static ZebedeeManager Instance => instance ??= SceneHelper.GetComponentInScene<ZebedeeManager>();
        private static ZebedeeManager instance;

        private ZebedeeSettings settings;
        private ZebedeeSettings Settings => settings ??= FumbResourcesHelper.LoadObject<ZebedeeSettings>();

        public int DailyLimit => Settings.SatsDailyLimit;
        
        // These are not hard coded, they are default values
        private string zebedeeAttributionLink = "https://zeb.gg/fumb-bitcoinminer";
        private string discordLink = "https://discord.gg/WMawgaZzDB";
        // vvv They are assigned in Awake below vvv
        
        private void Awake()
        {
            instance = this;
            // Here
            limitAmount = Settings.SatsDailyLimit;
            limitPeriod = Settings.SatsWithdrawalLimitPeriod;
            zebedeeAttributionLink = Settings.AttributionUrl;
            discordLink = Settings.DiscordUrl;
        }

        public delegate void SatsValueChange(int newValue);

        public event SatsValueChange OnSatsValueChange;
        public event SatsValueChange OnSatsWithdrawn; // Subscribe to event in code
        
        public int Satoshis 
        {
            get => SatsSaveValue;
            set
            {
                int newValue = value;
                if (newValue < 0)
                    newValue = 0;

                SatsSaveValue = newValue; // Hash the value
                OnSatsValueChange?.Invoke(newValue);
            }
        }

        private int limitAmount, limitPeriod;

        [Serializable]
        private class ZebedeeSatsEvent : UnityEvent<int> {}
        
        [SerializeField]
        private ZebedeeSatsEvent onSatsWithdrawn; // Set up event in inspector

        private int TotalClaimedInPeriod
        {
            get
            {
                CheckTimeOuts();
                if (CashOutTimeOuts.Count <= 0)
                    return 0;
                int total = 0;
                for (int i = 0; i < CashOutTimeOuts.Count; i++)
                    total += CashOutTimeOuts[i].satsClaimed;
                return total;
            }
        }

        public void SetGamertag(string gamertag)
        {
            saveGamertag.Value = gamertag;
        }

        public int MaxWithdrawAmount => Mathf.Min(Satoshis, limitAmount - TotalClaimedInPeriod);

        private void Start()
        {
            OnSatsValueChange?.Invoke(Satoshis);
        }

        public void AddSats(int sats)
        {
            Satoshis += sats;
        }

        public void DownloadZebedee()
        {
            Application.OpenURL(zebedeeAttributionLink);
        }

        public void OpenDiscord()
        {
            Application.OpenURL(discordLink);
        }

        private void ProcessCashout(string gamertag, int amount)
        {
            int startSats = Satoshis;
            Satoshis -= amount;
#if UNITY_EDITOR
            // Start fake withdrawal - use this for testing local functionality
            LoadingScreenController.Display(true, "Faking Withdrawal...");
            amount = limitAmount > 0 ? Math.Min(Satoshis, limitAmount - TotalClaimedInPeriod) : Satoshis;
            SetTimer.StartTimer(4f, () =>
            {
                LoadingScreenController.Display(false);

                CashOutTimeOuts.Add(new ZebedeeCashOutDelay(UnbiasedTime.Instance.Now().AddSeconds(limitPeriod), amount));
                Satoshis -= amount;
                
                OnSatsWithdrawn?.Invoke(amount);
                onSatsWithdrawn?.Invoke(amount);
            });
#else
            // Send withdrawal request to Zebedee
            LoadingScreenController.Display(true, "Requesting Withdrawal...");
            amount = Math.Min(amount, limitAmount - TotalClaimedInPeriod);
            RequestPayment.Instance.SendPayment(amount * 1000, gamertag, "Withdrawal", response =>
            {
                LoadingScreenController.Display(false);
                if (response.error)
                {
                    ZebedeeErrorHandler.Instance.HandleError(response.type, response.response, response.data);
                    Satoshis = startSats;
                    return;
                }

                if (int.TryParse(response.amount, out int millisats))
                    amount = millisats / 1000;
                
                if (limitAmount >= 0)
                    CashOutTimeOuts.Add(new ZebedeeCashOutDelay(UnbiasedTime.Instance.Now().AddSeconds(limitPeriod), amount));
                Satoshis = startSats - amount;
                OnSatsWithdrawn?.Invoke(amount);
                onSatsWithdrawn?.Invoke(amount);
            });
#endif
        }

        public void CashOut()
        {
            CashOut("", Satoshis);
        }

        public void CashOut(string gamertag, int amount)
        {
            if (Satoshis <= 0)
            {
                ZebedeeErrorHandler.Instance.HandleError("noSats", "You do not currently have any Sats to withdraw!", "");
                return;
            }

            if (limitAmount > 0 && TotalClaimedInPeriod >= limitAmount)
            {
                ZebedeeErrorHandler.Instance.HandleError("localLimit", "You have reached your withdrawal limit! Please wait {0}.", 
                    (EarliestTimeOut - UnbiasedTime.Instance.Now()).TotalMilliseconds.ToString(CultureInfo.InvariantCulture));
                return;
            }
            // Disabled old flow for now, may re-add it as separate module later
            // if (requireValidation && (!validated || gamertag != Gamertag))
            // {
            //     LoadingScreenController.Display(true, "Checking Gamertag...");
            //     ZebedeeValidationRequest.Instance.Validate(gamertag, (success, message) =>
            //     {
            //         if (!success)
            //         {
            //             LoadingScreenController.Display(false);
            //             ZebedeeErrorHandler.Instance.HandleError(message, "Failed to validate Gamertag.", "");
            //             return;
            //         }
            //
            //         validated = true;
            //         saveGamertag.Value = gamertag;
            //         CashOut(gamertag, amount);
            //     });
            //     return;
            // }
            
            // New Auth flow
            ZBDAuthWithdrawalManager.Instance.RequestCanWithdraw(canWithdraw =>
            {
                if (canWithdraw)
                {
                    ProcessCashout(ZBDFumbAuthLogin.GamerTag, amount);
                }
            });
        }
    }
}
#endif
