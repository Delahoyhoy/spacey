#if USE_FUMB_ZEBEDEE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace Zebedee.Validation
{
    public class ZebedeeValidationRequest : MonoBehaviour
    {
        private int connectionAttempts;
        private const int MaxAttempts = 6;

        private Coroutine validationRoutine;

        [SerializeField]
        private UnityEvent onBeginValidate;
        
        [SerializeField]
        private UnityEvent<bool> onValidated;

        public static ZebedeeValidationRequest Instance;

        private void Awake()
        {
            Instance = this;
        }

        public void Validate(string gamertag, Action<bool, string> responseCallback)
        {
            connectionAttempts = 0;
            if (string.IsNullOrEmpty(gamertag))
            {
                responseCallback?.Invoke(false, "noTag");
                return;
            }

            void HandleResponse(bool response, string message)
            {
                responseCallback?.Invoke(response, message);
                onValidated?.Invoke(response);
            }
            
            onBeginValidate?.Invoke();
            validationRoutine = StartCoroutine(RequestValidation(gamertag, HandleResponse));
        }

        public void Validate(string gamertag)
        {
            Validate(gamertag, null);
        }
        
        private string TrimGamertag(string gamertag)
        {
            if (gamertag.EndsWith("@zbd.gg"))
                return gamertag.Remove(gamertag.Length - 7);
            if (gamertag.StartsWith("https://zbd.gg/"))
                return gamertag.Remove(0, 15);    
            if (gamertag.StartsWith("zbd.gg/"))
                return gamertag.Remove(0, 7);    
            return gamertag;
        }

        private IEnumerator RequestValidation(string gamertag, Action<bool, string> responseCallback)
        {
            gamertag = TrimGamertag(gamertag);
            UnityWebRequest request = UnityWebRequest.Get($"https://jlcotterill.com/_functions/userId/{gamertag}");

            yield return request.SendWebRequest();
        
            if (request.result != UnityWebRequest.Result.Success)
            {
                connectionAttempts++;
                if (connectionAttempts < MaxAttempts && !request.error.Contains("400"))
                {
                    yield return new WaitForSeconds(1f);
                    validationRoutine = StartCoroutine(RequestValidation(gamertag, responseCallback));
                }
                else
                {
                    responseCallback?.Invoke(false, connectionAttempts >= MaxAttempts ? "timeout" : "unreachable");
                }
                yield break;
            }
            
            string resultJson = request.downloadHandler.text;
            ZebedeeGamertagValidationResponse result = JsonUtility.FromJson<ZebedeeGamertagValidationResponse>(resultJson);

            if (result == null || !result.success)
            {
                responseCallback?.Invoke(false, "invalid");
                yield break;
            }
            
            responseCallback?.Invoke(true, gamertag);
        }
    }   
}
#endif
