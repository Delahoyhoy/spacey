using Fumb.Editor;
using UnityEditor;

#if USE_FUMB_ZEBEDEE
namespace Zebedee
{
    public class EditorZebedee
    {
        [MenuItem("Fumb/Zebedee/Settings")]
        public static void GoToSettings() {
            EditorFumbResourcesHelper.SelectResourcesObject<ZebedeeSettings>(true);
        }
    }
}
#endif
