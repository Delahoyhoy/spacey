using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace FSG
{
	[CustomEditor(typeof(AndroidSecuritySettings))]
	public class AndroidSecuritySettingsEditor : Editor
	{
		private static Texture2D logoImage;
		[InitializeOnLoadMethod]
		private static void EnsureSettingsExist()
		{
			if (UnityEditorInternal.InternalEditorUtility.inBatchMode || UnityEditorInternal.InternalEditorUtility.isHumanControllingUs == false)
				return;
			var instance = Resources.Load<AndroidSecuritySettings>(AndroidSecuritySettings.SETTINGS_NAME);
			if (instance == null)
			{
				instance = ScriptableObject.CreateInstance<AndroidSecuritySettings>();
				var pluginPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(instance)));
				string resourcesFolder = pluginPath.Replace("/Scripts", "/Resources");
				if (Directory.Exists(resourcesFolder) == false)
					AssetDatabase.CreateFolder(pluginPath.Replace("/Scripts", string.Empty), "Resources");
				string folder = pluginPath + "/Resources/";
				folder = folder.Replace("/Scripts", string.Empty);
				AssetDatabase.CreateAsset(instance, folder + AndroidSecuritySettings.SETTINGS_NAME + ".asset");
			}
			string logoPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(instance)));
			logoPath = logoPath.Replace("/Scripts", "/Editor");
			logoImage = AssetDatabase.LoadAssetAtPath(logoPath + "/androidsecurity_logo.png", typeof(Texture2D)) as Texture2D;
			EditorApplication.delayCall += () => StoreKeystoreSignature(instance);
		}
		private static void StoreKeystoreSignature(AndroidSecuritySettings settings, bool force = false)
		{
			if (settings && !settings.lockAsset && settings.autoImportSignature)
			{
				try
				{
					string ndkroot = string.Format("\"{0}/bin/keytool.exe\"", EditorPrefs.GetString("JdkPath"));
					string arguments = string.Format("-v -list -keystore \"{0}\" -alias {1} -storepass {2} -keypass {3}",
						PlayerSettings.Android.keystoreName, PlayerSettings.Android.keyaliasName, PlayerSettings.keystorePass, PlayerSettings.keyaliasPass);

					StringBuilder errorBuilder = new StringBuilder();
					if (string.IsNullOrEmpty(ndkroot))
					{
						errorBuilder.AppendLine("JDK Root path not found, is it installed? Check in Edit -> Preferences -> External Tools.");
						errorBuilder.AppendLine();
					}
					if (string.IsNullOrEmpty(PlayerSettings.Android.keystoreName))
					{
						errorBuilder.AppendLine("Empty string in setting keystoreName. Specify one in Edit -> Project Settings -> Player.");
						errorBuilder.AppendLine();
					}
					if (string.IsNullOrEmpty(PlayerSettings.Android.keyaliasName))
					{
						errorBuilder.AppendLine("Empty string in setting keyaliasName. Specify one in Edit -> Project Settings -> Player.");
						errorBuilder.AppendLine();
					}
					if (string.IsNullOrEmpty(PlayerSettings.Android.keystorePass))
					{
						errorBuilder.AppendLine("Empty string in setting keystorePass. Enter your keystore password in Edit -> Project Settings -> Player.");
						errorBuilder.AppendLine();
					}
					if (string.IsNullOrEmpty(PlayerSettings.Android.keyaliasPass))
					{
						errorBuilder.AppendLine("Empty string in setting keyaliasPass. Enter your alias password in Edit -> Project Settings -> Player.");
						errorBuilder.AppendLine();
					}
					if (errorBuilder.Length > 0)
					{
						settings.error = errorBuilder.ToString();
						return;
					}
					var process = new System.Diagnostics.Process();
					process.StartInfo.FileName = ndkroot;
					process.StartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
					process.StartInfo.Arguments = arguments;

					// Hide window
					process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
					process.StartInfo.RedirectStandardOutput = true;
					process.StartInfo.RedirectStandardError = true;
					process.StartInfo.UseShellExecute = false;
					process.StartInfo.CreateNoWindow = true;

					process.Start();
					var output = process.StandardOutput.ReadToEnd();

					process.WaitForExit();
					if (process.ExitCode == 0)
					{
						var lines = output.Split('\n');
						for (int i = 0; i < lines.Length; i++)
						{
							if (lines[i].Contains("MD5"))
							{
								string[] md5split = lines[i].Split(' ');
								string signature = md5split[md5split.Length - 1].ToLower().Trim();
								signature = signature.Replace(":", string.Empty);
								if (settings.keystoreSignature != signature || force)
								{
									settings.keystoreSignature = signature;
									settings.signature = Encoding.UTF8.GetBytes(signature);
									EditorUtility.SetDirty(settings);
									Debug.LogFormat("Stored keystore signature {0}", signature);
								}
								break;
							}
						}
					}
					else
					{
						throw new System.Exception("Unable to execute keytool correctly. Check your JDK path in Edit -> Preferences -> External Tools.");
					}
					settings.error = null;
				}
				catch (System.Exception e)
				{
					settings.error = e.ToString();
				}
			}
		}
		public override void OnInspectorGUI()
		{
			int logoSize = 100;
			if (logoImage != null)
			{
				GUILayout.BeginHorizontal();
                GUILayout.Space(logoSize);
				var pos = GUILayoutUtility.GetLastRect();
				Debug.Log(pos);
				pos.width = logoSize;
				pos.height = logoSize;
                GUI.DrawTexture(pos, logoImage);
			}
			AndroidSecuritySettings settings = target as AndroidSecuritySettings;
			GUILayout.BeginVertical();
			{
				settings.lockAsset = EditorGUILayout.Toggle("Lock Asset", settings.lockAsset);
				settings.autoImportSignature = EditorGUILayout.Toggle("Automatically Import", settings.autoImportSignature);
				GUI.enabled = false;
				EditorGUILayout.TextField("Stored Signature", settings.keystoreSignature);
				GUI.enabled = settings.lockAsset == false;
				if (GUILayout.Button("Reimport Signature"))
				{
					StoreKeystoreSignature(settings, true);
				}
			}
			GUILayout.EndVertical();
			if (logoImage != null)
			{
				GUILayout.EndHorizontal();
			}
			GUI.enabled = true;
			if (logoImage != null)
			{
				GUILayout.Space(30);
			}
			string error = settings.error;
			if (string.IsNullOrEmpty(error) == false)
			{
				GUIStyle errorStyle = new GUIStyle(GUI.skin.FindStyle("CN EntryError"));
				errorStyle.wordWrap = true;
				GUILayout.Box(string.Empty, errorStyle);
				errorStyle.normal.background = null;
				GUILayout.Space(-errorStyle.fixedHeight * 1.5f);
				errorStyle.fixedHeight = 0;
				errorStyle.fixedWidth = 0;
				EditorGUILayout.LabelField(error, errorStyle);
			}
			else if (settings.signature != null && settings.signature.Length > 0)
			{
				GUIStyle infoStyle = new GUIStyle(GUI.skin.FindStyle("CN EntryInfo"));
				infoStyle.alignment = TextAnchor.MiddleLeft;
				GUILayout.Box("Signature import was successful.", infoStyle);
			}
		}
	}
}