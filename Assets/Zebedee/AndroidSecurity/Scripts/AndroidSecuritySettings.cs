using UnityEngine;

namespace FSG
{
	/// <summary>
	/// This ScriptableObject stores the authentic app signature
	/// </summary>
	public class AndroidSecuritySettings : ScriptableObject
	{
		/// <summary>
		/// Resource location for settings
		/// </summary>
		public const string SETTINGS_NAME = "AndroidSecuritySettings";

		/// <summary>
		/// This information is removed in builds for security
		/// </summary>
#if UNITY_EDITOR
		public string keystoreSignature;
#else
		public string keystoreSignature { get { return string.Empty; } }
#endif
		/// <summary>
		/// Error information. This information is removed in builds for security
		/// </summary>
#if UNITY_EDITOR
		public string error;
#else
		public string error { get { return string.Empty; } }
#endif
		/// <summary>
		/// Signature of the app stored as a byte[]
		/// </summary>
		[HideInInspector]
		public byte[] signature;
		public bool lockAsset;
		public bool autoImportSignature = true;
	}
}