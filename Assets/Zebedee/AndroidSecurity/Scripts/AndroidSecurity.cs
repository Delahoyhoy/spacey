using UnityEngine;
using System.Text;
using System.Security.Cryptography;

namespace FSG
{
    public static class AndroidSecurity
    {
        public static string IsAppSignatureValid(out string signature)
        {
            signature = string.Empty;
#if UNITY_ANDROID
            try
            {
                var asset = Resources.Load<AndroidSecuritySettings>(AndroidSecuritySettings.SETTINGS_NAME);
                if (asset)
                {
                    signature = Encoding.UTF8.GetString(asset.signature);
#if !RELEASE
                    Debug.LogFormat("Loaded signature from resources: {0}", signature);
                    if (string.IsNullOrEmpty(signature))
                    {
                        Debug.LogWarning("Loaded empty signature");
                    }
#endif
                }
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                string packageName = activity.Call<string>("getPackageName");
                AndroidJavaObject packageManager = activity.Call<AndroidJavaObject>("getPackageManager");
                int GET_SIGNATURES = 0x00000040;
                AndroidJavaObject packageInfo = packageManager.Call<AndroidJavaObject>("getPackageInfo", packageName, GET_SIGNATURES);
                AndroidJavaObject[] signatures = packageInfo.Get<AndroidJavaObject[]>("signatures");
                if (signatures.Length == 0)
                {
                    throw new System.Exception("Application is not signed");
                }
                for (int i = 0; i < signatures.Length; i++)
                {
                    AndroidJavaObject currentObject = signatures[i];
                    byte[] bytes = currentObject.Call<byte[]>("toByteArray");
                    MD5 md5 = new MD5CryptoServiceProvider();
                    byte[] result = md5.ComputeHash(bytes);
                    StringBuilder sb = new StringBuilder();
                    for (int j = 0; j < result.Length; j++)
                    {
                        sb.Append(result[j].ToString("x2"));
                    }
                    string sig = sb.ToString();
#if !RELEASE
                    Debug.LogFormat("Current signature: {0}", sig);
#endif

                    return sig;
                }
                return "";

            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
            }
            return "";
#else
			return "";
#endif
        }
        public static bool IsDebuggable()
        {
#if UNITY_ANDROID
            try
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject info = activity.Call<AndroidJavaObject>("getApplicationInfo");
                int flags = info.Get<int>("flags");
                return (flags & 0x00000002) != 0;
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
            }
            return true;
#else
			return false;
#endif
        }
        public static bool VerifyGooglePlayInstaller()
        {
#if UNITY_ANDROID
            try
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject packageManager = activity.Call<AndroidJavaObject>("getPackageManager");
                string packageName = activity.Call<string>("getPackageName");
                string installer = packageManager.Call<string>("getInstallerPackageName", packageName);
#if !RELEASE
                Debug.LogFormat("Current installer: {0}", installer);
#endif
                return installer != null && installer.StartsWith("com.android.vending");
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
            }
            return false;
#else
			return false;
#endif
        }
    }
}