﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityThing : MonoBehaviour {
    [SerializeField]
    GameObject attractedObject;
    [SerializeField]
    GameObject attractingBody;
    [SerializeField]
    float gravityStrength;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

//        Debug.Log("Distance: " + Vector3.Distance(attractedObject.transform.position, attractingBody.transform.position));
        if(Vector3.Distance(attractedObject.transform.position, attractingBody.transform.position) < 7){
            float force = gravityStrength / Vector3.Distance(attractedObject.transform.position, attractingBody.transform.position);

            // establish direction towards the attracting body
            Vector3 direction = attractingBody.transform.position - attractedObject.transform.position;

            // apply gravitational force of attraction to attracted body
            attractedObject.GetComponent<Rigidbody>().AddForce(direction.x * force, direction.y * force, direction.z * force, ForceMode.Impulse);        
        }
	}
}
