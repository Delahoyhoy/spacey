﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using UnityEngine;

namespace Legacy
{
    public class OptionsScreenController : MonoBehaviour
    {
        [SerializeField]
        Animator screenAnimator;

        [SerializeField]
        GameObject soundOnObject;

        [SerializeField]
        GameObject soundOffObject;

        [SerializeField]
        GameObject quickbuyOnObject;

        [SerializeField]
        GameObject quickbuyOffObject;

        private void Start()
        {
            screenAnimator.Play("Maximise");
            SortOutSoundIcons();
            SortOutQuickBuyIcons();
        }

        void SortOutSoundIcons()
        {
            if (!DontDestroyDataHolder.Instance.muteAudio)
            {
                soundOnObject.SetActive(true);
                soundOffObject.SetActive(false);
            }
            else
            {
                soundOnObject.SetActive(false);
                soundOffObject.SetActive(true);
            }
        }

        void SortOutQuickBuyIcons()
        {
            if (DontDestroyDataHolder.Instance.quickBuy)
            {
                quickbuyOnObject.SetActive(true);
                quickbuyOffObject.SetActive(false);
            }
            else
            {
                quickbuyOnObject.SetActive(false);
                quickbuyOffObject.SetActive(true);
            }
        }

        public void PressToggleSound()
        {
            DontDestroyDataHolder.Instance.ToggleAudio();
            SortOutSoundIcons();
        }

        public void PressQuickBuy()
        {
            DontDestroyDataHolder.Instance.ToggleQuickBuy();
            SortOutQuickBuyIcons();
        }

        public void PressDiscord()
        {
            Application.OpenURL("https://discord.gg/dyYXVt2");
        }

        public void PressMessenger()
        {
            Application.OpenURL("https://m.me/1867904263535842");
        }

        private void Update()
        {

        }

        public void DismissPopup()
        {
            GameController.Instance.IsPopupOpenNow = false;
            screenAnimator.Play("Minimise");
            StartCoroutine(WaitThenDestroyThisBud());
        }

        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }
    }

}