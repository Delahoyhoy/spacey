﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Legacy
{
    public class ChestScreenController : MonoBehaviour
    {
        [SerializeField]
        GameObject[] allChests;

        [SerializeField]
        Text currentCardsLeft;

        [SerializeField]
        GameObject nextButton;

        [SerializeField]
        Text nextButtonText;

        [SerializeField]
        Sprite[] allShipSprites;

        [SerializeField]
        Text[] allShipTexts;

        int[] allAmountsOfShips;

        GameObject currentChest;

        // Use this for initialization
        void Start()
        {
            allAmountsOfShips = new int[allShipSprites.Length];
            currentCardsLeft.gameObject.SetActive(false);
            StartCoroutine(WaitSecThenSpawnChest(1.0f));
            nextButton.SetActive(false);
        }


        IEnumerator WaitSecThenSpawnChest(float timer)
        {
            yield return new WaitForSeconds(timer);
            // DontDestroyDataHolder.Instance.chestTypesToOpen =
            //     new List<ChestData.ChestType>(new ChestData.ChestType[] {ChestData.ChestType.Epic});
            // switch (DontDestroyDataHolder.Instance.chestTypesToOpen[0])
            // {
            //     case ChestData.ChestType.Normal:
            //         currentChest = Instantiate(allChests[0], transform);
            //         currentChest.GetComponent<ChestAnimator>().theScreenController = this;
            //         cardsToOpenInThisChest = 5;
            //         break;
            //     case ChestData.ChestType.Epic:
            //         currentChest = Instantiate(allChests[1], transform);
            //         currentChest.GetComponent<ChestAnimator>().theScreenController = this;
            //         cardsToOpenInThisChest = 10;
            //         break;
            //     case ChestData.ChestType.Rare:
            //         currentChest = Instantiate(allChests[2], transform);
            //         currentChest.GetComponent<ChestAnimator>().theScreenController = this;
            //         cardsToOpenInThisChest = 15;
            //         break;
            //     case ChestData.ChestType.Legendary:
            //         currentChest = Instantiate(allChests[3], transform);
            //         currentChest.GetComponent<ChestAnimator>().theScreenController = this;
            //         cardsToOpenInThisChest = 20;
            //         break;
            //     default:
            //         break;
            // }

            currentCardsLeft.gameObject.SetActive(true);
            nextButton.SetActive(false);
            currentCardsLeft.text = cardsToOpenInThisChest.ToString() + " Cards Left";
        }

        public int cardsToOpenInThisChest = 0;

        public void IncrementCardsOpened()
        {
            currentCardsLeft.gameObject.SetActive(true);
            cardsToOpenInThisChest--;
            currentCardsLeft.text = cardsToOpenInThisChest.ToString() + " Cards Left";
            if (cardsToOpenInThisChest <= 0)
            {
                nextButton.SetActive(true);
                // if (DontDestroyDataHolder.Instance.chestTypesToOpen.Count > 1)
                // {
                //     nextButtonText.text = "Next Chest!";
                // }
                // else
                // {
                //     nextButtonText.text = "Collect!";
                // }
            }
        }

        public Sprite GetShipSprite()
        {
            int rando = Random.Range(0, allShipSprites.Length);
            allAmountsOfShips[rando]++;
            allShipTexts[rando].text = "x" + allAmountsOfShips[rando].ToString();
            return allShipSprites[rando];
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void PressNext()
        {
            // if (DontDestroyDataHolder.Instance.chestTypesToOpen.Count > 1)
            // {
            //     currentCardsLeft.gameObject.SetActive(false);
            //     DontDestroyDataHolder.Instance.chestTypesToOpen.RemoveAt(0);
            //     Destroy(GameObject.FindWithTag("card"));
            //     Destroy(currentChest);
            //     StartCoroutine(WaitSecThenSpawnChest(0.5f));
            // }
            // else
            // {
            //     currentCardsLeft.gameObject.SetActive(false);
            //     PressComplete();
            // }
        }

        public void PressComplete()
        {
            DontDestroyDataHolder.Instance.allChestOpenedItemsInts = new int[allAmountsOfShips.Length];
            DontDestroyDataHolder.Instance.allChestOpenedItemsInts = allAmountsOfShips;
            DontDestroyDataHolder.Instance.hasHadNewCardsAdded = true;
            SceneManager.LoadScene("SystemScene");
        }
    }

}