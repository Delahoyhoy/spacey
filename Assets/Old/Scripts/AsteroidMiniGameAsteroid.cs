﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
	public class AsteroidMiniGameAsteroid : MonoBehaviour
	{

		// Use this for initialization
		void Start()
		{

		}

		Vector3 start;
		Vector3 end;
		float speed;

		public void Setup(Vector3 startPos, Vector3 endPos, float _speed)
		{
			transform.position = startPos;
			start = startPos;
			end = endPos;
			speed = _speed;
			StartCoroutine(MoveTowardsGoal());
		}

		IEnumerator MoveTowardsGoal()
		{
			float time = 0;
			while (time < 1)
			{
				this.transform.position = Vector3.Lerp(start, end, time);
				time += Time.deltaTime * speed;
				yield return 0;
			}
		}

		// Update is called once per frame
		void Update()
		{

		}
	}
}
