﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class BoostsController : MonoBehaviour
    {

        public int currentVIPLevel = 0;
        public int currentVIPExperience = 0;
        public int VIPExperienceToNextLevel = 0;
        public string nextBoostUnlocksText;

        public int extraOfflineHours = 0;
        public int extraAdMinutes = 0;
        public double extraDiamondsMultiplier = 1.0d;
        public double extraAdMultipliers = 1.0d;
        public double extraGoldMultipliers = 1.0d;

        public string extraOfflineHoursText;
        public string extraAdMinuteText;
        public string extraDiamondsMultiplierText;
        public string extraAdMultipliersText;
        public string extraGoldMultipliersText;

        public double adBoostAmount = 1.5d;

        public GameObject notificationIcon;

        // Use this for initialization
        void Start()
        {
            Load(0, 0);
        }

        public void Load(int _vipLevel, int _currentExp)
        {
            currentVIPLevel = _vipLevel;
            currentVIPExperience = _currentExp;
            VIPExperienceToNextLevel = WorkOutVIPExperienceToNextLevel();
            SetNextBoostText();
            SetAllActualBoostAmounts();
        }

        public void AddVIPPoints(int howManyBro)
        {
            currentVIPExperience += howManyBro;
            if (currentVIPExperience >= VIPExperienceToNextLevel)
            {
                notificationIcon.SetActive(true);
            }
        }

        public bool IsVIPUpgradeAvailable()
        {
            if (currentVIPExperience >= VIPExperienceToNextLevel)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public double GetAdBoostAmount()
        {
            if (GameController.Instance.basicAdController.isAdModeActive)
            {
                return adBoostAmount;
            }
            else
            {
                return 1;
            }
        }

        public void UpgradeVIPLevel()
        {
            if (IsVIPUpgradeAvailable())
            {
                currentVIPLevel++;
                currentVIPExperience -= VIPExperienceToNextLevel;
                Load(currentVIPLevel, currentVIPExperience);
            }
        }

        public float GetVIPUpgradeProgress()
        {
            return (float) ((float) currentVIPExperience / (float) VIPExperienceToNextLevel);
        }

        // Update is called once per frame
        void Update()
        {

        }


        private int WorkOutVIPExperienceToNextLevel()
        {
            switch (currentVIPLevel)
            {
                case 0:
                    return 20;
                case 1:
                    return 200;
                case 2:
                    return 400;
                case 3:
                    return 500;
                case 4:
                    return 600;
                case 5:
                    return 700;
                case 6:
                    return 900;
                case 7:
                    return 1200;
                case 8:
                    return 1600;
                case 9:
                    return 2000;
                default:
                    return 99999999;
            }
        }

        public void SetNextBoostText()
        {
            switch (currentVIPLevel)
            {
                case 0:
                    nextBoostUnlocksText = "Unlock extra Offline Time & Ad Time";
                    break;
                case 1:
                    nextBoostUnlocksText = "Unlock extra Diamonds from purchases";
                    break;
                case 2:
                    nextBoostUnlocksText = "Unlock extra Ad & Gold multipliers";
                    break;
                case 3:
                    nextBoostUnlocksText = "Unlock extra Offline Time & Ad Time";
                    break;
                case 4:
                    nextBoostUnlocksText = "Unlock extra Diamonds from purchases";
                    break;
                case 5:
                    nextBoostUnlocksText = "Unlock extra Ad & Gold multipliers";
                    break;
                case 6:
                    nextBoostUnlocksText = "Unlock extra Offline Time & Ad Time";
                    break;
                case 7:
                    nextBoostUnlocksText = "Unlock extra Diamonds from purchases";
                    break;
                case 8:
                    nextBoostUnlocksText = "Unlock extra Ad & Gold multipliers";
                    break;
                case 9:
                    nextBoostUnlocksText = "Unlock extra everything!";
                    break;
                default:
                    nextBoostUnlocksText = "All boosts unlocked!";
                    break;
            }
        }

        public void SetAllActualBoostAmounts()
        {
            switch (currentVIPLevel)
            {
                case 0:
                    extraOfflineHours = 0;
                    extraAdMinutes = 0;
                    extraDiamondsMultiplier = 1.0d;
                    extraAdMultipliers = 1.0d;
                    extraGoldMultipliers = 1.0d;
                    break;
                case 1:
                    extraOfflineHours = 1;
                    extraAdMinutes = 1;
                    extraDiamondsMultiplier = 1.0d;
                    extraAdMultipliers = 1.0d;
                    extraGoldMultipliers = 1.0d;
                    break;
                case 2:
                    extraOfflineHours = 1;
                    extraAdMinutes = 1;
                    extraDiamondsMultiplier = 1.3d;
                    extraAdMultipliers = 1.0d;
                    extraGoldMultipliers = 1.0d;
                    break;
                case 3:
                    extraOfflineHours = 1;
                    extraAdMinutes = 1;
                    extraDiamondsMultiplier = 1.3d;
                    extraAdMultipliers = 1.5d;
                    extraGoldMultipliers = 1.3d;
                    break;
                case 4:
                    extraOfflineHours = 2;
                    extraAdMinutes = 2;
                    extraDiamondsMultiplier = 1.3d;
                    extraAdMultipliers = 1.5d;
                    extraGoldMultipliers = 1.3d;
                    break;
                case 5:
                    extraOfflineHours = 2;
                    extraAdMinutes = 2;
                    extraDiamondsMultiplier = 1.6d;
                    extraAdMultipliers = 1.5d;
                    extraGoldMultipliers = 1.3d;
                    break;
                case 6:
                    extraOfflineHours = 2;
                    extraAdMinutes = 2;
                    extraDiamondsMultiplier = 1.6d;
                    extraAdMultipliers = 1.75d;
                    extraGoldMultipliers = 1.6d;
                    break;
                case 7:
                    extraOfflineHours = 3;
                    extraAdMinutes = 3;
                    extraDiamondsMultiplier = 1.6d;
                    extraAdMultipliers = 1.75d;
                    extraGoldMultipliers = 1.6d;
                    break;
                case 8:
                    extraOfflineHours = 3;
                    extraAdMinutes = 3;
                    extraDiamondsMultiplier = 1.9d;
                    extraAdMultipliers = 1.75d;
                    extraGoldMultipliers = 1.6d;
                    break;
                case 9:
                    extraOfflineHours = 3;
                    extraAdMinutes = 3;
                    extraDiamondsMultiplier = 1.9d;
                    extraAdMultipliers = 2.0d;
                    extraGoldMultipliers = 2.0d;
                    break;
                case 10:
                    extraOfflineHours = 4;
                    extraAdMinutes = 5;
                    extraDiamondsMultiplier = 2.5d;
                    extraAdMultipliers = 2.5d;
                    extraGoldMultipliers = 2.5d;
                    break;
                default:
                    break;
            }

            extraOfflineHoursText = "+" + extraOfflineHours.ToString() + " hours";
            extraAdMinuteText = "+" + extraAdMinutes.ToString() + " mins";
            extraDiamondsMultiplierText = "x" + extraDiamondsMultiplier.ToString();
            extraAdMultipliersText = "x" + extraAdMultipliers.ToString();
            extraGoldMultipliersText = "x" + extraGoldMultipliers.ToString();
            adBoostAmount = 1.5d + extraAdMultipliers;
        }
    }

}