﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class BasicShipEngineController : MonoBehaviour
    {
        Vector3 previousEulerRotation;
        Vector3 previousLocalPosition;

        [SerializeField]
        GameObject leftEngine;

        [SerializeField]
        GameObject rightEngine;

        [SerializeField]
        Animator theAnimator;

        [SerializeField]
        BasicSystemController theSystem;

        [SerializeField]
        bool isContainerShip = false;

        double beingHeldOnContainerShip = 0;

        public float extraDistance = 0.01f;
        Color midAlpha = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        Color fullAlpha = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        Color noAlpha = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        bool isCurrentlySpedUp = false;

        float baseSpeed = 1;

        // Use this for initialization
        void Start()
        {

        }

        public void DroppedOff()
        {
            theSystem.ShipDroppedOff();
        }

        public void PickupForContainerShip()
        {
            beingHeldOnContainerShip = theSystem.ContainerShipPickup();
        }

        public void DropOffForContainerShip()
        {

            double amt = beingHeldOnContainerShip * GameController.Instance.boostsController.GetAdBoostAmount() *
                         GameController.Instance.boostsController.extraGoldMultipliers *
                         GameController.Instance.researchController.extraGoldEarned;
            GameController.Instance.currencyController.AddCurrency(amt, CurrencyType.SoftCurrency);
            GameController.Instance.tasksController.AddProgress(amt, TasksController.TaskType.coinCollect);

            beingHeldOnContainerShip = 0;
        }

        public void BigShipTutorialFinished()
        {
            //GameController.Instance.tutorialController.IncrementTutorialState();
            //TutorialManager.OnTrigger("intro");
        }

        public void ActivateAnimation()
        {
            Debug.Log("Trigger anim");
            theAnimator.enabled = true;
        }

        // Update is called once per frame
        void Update()
        {

            if (leftEngine != null)
            {
                if (previousEulerRotation.x > this.gameObject.transform.localEulerAngles.x + extraDistance)
                {
                    leftEngine.SetActive(true);
                    rightEngine.SetActive(false);
                }
                else if (previousEulerRotation.x < this.gameObject.transform.localEulerAngles.x - extraDistance)
                {
                    leftEngine.SetActive(false);
                    rightEngine.SetActive(true);
                }
                else
                {
                    leftEngine.SetActive(true);
                    rightEngine.SetActive(true);
                }

                float distance = Vector3.Distance(this.gameObject.transform.localPosition, previousLocalPosition);

                if (distance > 0.001f && distance < 0.015f)
                {
                    leftEngine.GetComponent<SpriteRenderer>().color = midAlpha;
                    rightEngine.GetComponent<SpriteRenderer>().color = midAlpha;
                }
                else if (distance >= 0.015f)
                {
                    leftEngine.GetComponent<SpriteRenderer>().color = fullAlpha;
                    rightEngine.GetComponent<SpriteRenderer>().color = fullAlpha;
                }
                else
                {
                    leftEngine.GetComponent<SpriteRenderer>().color = noAlpha;
                    rightEngine.GetComponent<SpriteRenderer>().color = noAlpha;
                }
            }

            if (!isCurrentlySpedUp)
            {
                if (theAnimator != null)
                {
                    theAnimator.speed = baseSpeed * GameController.Instance.researchController.miningShipSpeedBoost;
                }
            }

            previousEulerRotation = this.gameObject.transform.localEulerAngles;
            previousLocalPosition = this.gameObject.transform.localPosition;
        }

        public void SetToLeave()
        {
            if (this.GetComponent<Animator>() != null)
            {
                this.GetComponent<Animator>().enabled = false;
            }

            StartCoroutine(TurnToRight());
        }

        IEnumerator TurnToRight()
        {
            float turnAmount = 0.2f;

            while (this.gameObject.transform.localEulerAngles.z < 0)
            {
                this.gameObject.transform.localEulerAngles = new Vector3(this.gameObject.transform.localEulerAngles.x,
                    this.gameObject.transform.localEulerAngles.y,
                    this.gameObject.transform.localEulerAngles.z + 360);
            }

            if (this.gameObject.transform.localEulerAngles.z > 270 || this.gameObject.transform.localEulerAngles.z < 90)
            {
                turnAmount *= -1;
            }

            while (this.gameObject.transform.localEulerAngles.z < 270 ||
                   this.gameObject.transform.localEulerAngles.z > 273)
            {
                this.gameObject.transform.localEulerAngles = new Vector3(this.gameObject.transform.localEulerAngles.x,
                    this.gameObject.transform.localEulerAngles.y,
                    this.gameObject.transform.localEulerAngles.z + turnAmount);
                yield return 0;
            }

            while (this.gameObject.transform.position.x < 30)
            {
                this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x + 0.01f,
                    this.gameObject.transform.position.y,
                    this.gameObject.transform.position.z);
                yield return 0;
            }
        }

        public bool SpeedUpShip()
        {
            if (!isCurrentlySpedUp)
            {
                StartCoroutine(SpeedItUpYo());
                return true;
            }
            else
            {
                return false;
            }
        }

        IEnumerator SpeedItUpYo()
        {
            isCurrentlySpedUp = true;
            theAnimator.speed = (baseSpeed * GameController.Instance.researchController.miningShipSpeedBoost) * 2;
            yield return new WaitForSecondsRealtime(2.0f);
            isCurrentlySpedUp = false;
            theAnimator.speed = baseSpeed * GameController.Instance.researchController.miningShipSpeedBoost;
        }
    }

}