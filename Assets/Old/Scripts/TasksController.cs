﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class TasksController : MonoBehaviour
    {

        public enum TaskType
        {
            coinCollect,
            systemUpgrade,
            newSystem,
            research
        }

        public GameObject notificationIcon;

        public class SingleTask
        {
            public string taskShortID;
            public string taskName;
            public TaskType taskType;
            public double taskCurrentProgress;
            public double taskAmount;
            public bool isActive = false;
            public bool isCompleted = false;
            public double rewardAmount;

            public string GetProgress()
            {
                return CurrencyController.CurrencyToString(taskCurrentProgress) + "/" +
                       CurrencyController.CurrencyToString(taskAmount);
            }

            public double GetPercentage()
            {
                return taskCurrentProgress / taskAmount;
            }

            public bool IsReadyToCollect()
            {
                if (taskCurrentProgress >= taskAmount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<SingleTask> GetActiveTasks()
        {
            List<SingleTask> bleh = new List<SingleTask>();

            if (singleTasks.Count < 2)
            {
                Start();
            }

            for (int i = 0; i < singleTasks.Count; i++)
            {
                if (singleTasks[i].isActive)
                {
                    bleh.Add(singleTasks[i]);
                }
            }

            if (bleh.Count < 3)
            {
                for (int i = 0; i < singleTasks.Count; i++)
                {
                    bool isSameTask = false;
                    for (int j = 0; j < bleh.Count; j++)
                    {
                        if (singleTasks[i].isActive || singleTasks[i].taskType == bleh[j].taskType ||
                            singleTasks[i].isCompleted)
                        {
                            isSameTask = true;
                        }
                    }

                    if (!isSameTask)
                    {
                        singleTasks[i].isActive = true;
                        bleh.Add(singleTasks[i]);
                        if (bleh.Count >= 3)
                        {
                            break;
                        }
                    }
                }
            }

            return bleh;
        }

        public List<SingleTask> singleTasks = new List<SingleTask>();

        // Use this for initialization
        void Start()
        {
            SingleTask tempTask = new SingleTask();
            tempTask.taskShortID = "1";
            tempTask.taskName = "Collect coins";
            tempTask.taskType = TaskType.coinCollect;
            tempTask.taskCurrentProgress = 0;
            tempTask.taskAmount = 100;
            tempTask.rewardAmount = 5;
            singleTasks.Add(tempTask);

            tempTask = new SingleTask();
            tempTask.taskShortID = "2";
            tempTask.taskName = "Upgrade System";
            tempTask.taskType = TaskType.systemUpgrade;
            tempTask.taskCurrentProgress = 0;
            tempTask.taskAmount = 4;
            tempTask.rewardAmount = 5;
            singleTasks.Add(tempTask);

            tempTask = new SingleTask();
            tempTask.taskShortID = "3";
            tempTask.taskName = "Unlock New System";
            tempTask.taskType = TaskType.newSystem;
            tempTask.taskCurrentProgress = 0;
            tempTask.taskAmount = 1;
            tempTask.rewardAmount = 5;
            singleTasks.Add(tempTask);

            tempTask = new SingleTask();
            tempTask.taskShortID = "4";
            tempTask.taskName = "Collect coins";
            tempTask.taskType = TaskType.coinCollect;
            tempTask.taskCurrentProgress = 0;
            tempTask.taskAmount = 10000;
            tempTask.rewardAmount = 5;
            singleTasks.Add(tempTask);

            tempTask = new SingleTask();
            tempTask.taskShortID = "5";
            tempTask.taskName = "Perform Research";
            tempTask.taskType = TaskType.research;
            tempTask.taskCurrentProgress = 0;
            tempTask.taskAmount = 1;
            tempTask.rewardAmount = 5;
            singleTasks.Add(tempTask);

            tempTask = new SingleTask();
            tempTask.taskShortID = "6";
            tempTask.taskName = "Perform Research";
            tempTask.taskType = TaskType.research;
            tempTask.taskCurrentProgress = 0;
            tempTask.taskAmount = 5;
            tempTask.rewardAmount = 10;
            singleTasks.Add(tempTask);

            GetActiveTasks();
        }

        public void AddProgress(double amount, TaskType theType)
        {
            for (int i = 0; i < singleTasks.Count; i++)
            {
                if (singleTasks[i].isActive && singleTasks[i].taskType == theType)
                {
                    singleTasks[i].taskCurrentProgress += amount;
                    if (singleTasks[i].taskCurrentProgress >= singleTasks[i].taskAmount && !singleTasks[i].isCompleted)
                    {
                        notificationIcon.SetActive(true);
                    }
                }
            }
        }

        public void CompleteTaskPressed(string taskID, Transform bleh)
        {
            for (int i = 0; i < singleTasks.Count; i++)
            {
                if (singleTasks[i].taskShortID == taskID)
                {
                    singleTasks[i].isCompleted = true;
                    singleTasks[i].isActive = false;
                    GameController.Instance.currencyController.AddCurrency(singleTasks[i].rewardAmount,
                        CurrencyType.HardCurrency);
                    GameController.Instance.PlayGemsCoinBlast(bleh);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}