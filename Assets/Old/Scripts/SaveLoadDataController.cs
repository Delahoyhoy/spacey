﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using Core.Gameplay;

namespace Legacy
{
    public class SaveLoadDataController : MonoBehaviour
    {
        //TODO: PRERELEASE private static string filename = "Saving17/saveData.txt";
        private static string filename = "Saving2/saveData.txt";

        private static string gameDataName = "gameData1";

        //"gameData1"
        //iOS: Potentially Saving2
        //Android: Potentially Saving2
        //Max got to: 13
        public bool turnOffGPSaving = false;

        private string currentGold = "crGl";
        private string currentDiamonds = "crDi";
        private string systemLevel = "syLv";
        private string currentTutorialInt = "crTu";
        private string currentVIPLevel = "crVI";
        private string currentVIPExperience = "crVEx";
        private string timeSinceClose = "tsc";
        private string taskProgress = "tp";
        private string taskComplete = "tc";
        private string taskActive = "ta";
        private string researchSaving = "rs";
        private string currentActiveResearch = "car";
        private string muteAudio = "ma";
        private string quickBuy = "qb";
        private string numberOfUnlockedShips = "nus";


        //bool usingIcloudManager = false;
        //bool waitingForIcloudResponse = false;

        public bool leavingForAd = false;

        bool loadSequenceStarted = false;

        //public double brainsAtLoad = 0;
        //public double skullsAtLoad = 0;
        DateTime timeGameOpened;

        bool cameFromFullRestart = true;

        // Use this for initialization
        void Start()
        {
            cameFromFullRestart = true;

            /*if (PlayerPrefs.GetFloat("GPsavesoff", 1) == 0)
        {
           turnOffGPSaving = false;
        } else if (PlayerPrefs.GetFloat("GPsavesoff",1) == 1)
        {
            turnOffGPSaving = true;
        }*/
            StartLoadSequence();
        }

        /// <summary>
        /// Google Play stoof
        /// </summary>
        int loadCounter = 2;
        // IEnumerator WaitSecThenLoadGP()
        //{
        /*yield return new WaitForSecondsRealtime(0.3f);
        Debug.Log("Waited 0.3f");

        if (!DontDestroyOnLoadSaveLoad.Instance.justSwitchingScenes)
        {
            if (GooglePlayConnection.Instance.IsConnected)
            {
                Debug.Log("Loading snapshot");

                GooglePlaySavedGamesManager.Instance.LoadSpanshotByName(gameDataName);
            }
            else
            {
                //try X times before giving up
                if (GooglePlayConnection.Instance.IsConnecting)
                {
                    StartCoroutine(WaitSecThenLoadGP());
                    Debug.Log("Connecting to GPGS");
                }
                else
                {
                    Debug.Log("No longer connecting");

                    if (loadCounter > 0)
                    {
                        GooglePlayConnection.Instance.Connect();
                        StartCoroutine(WaitSecThenLoadGP());
                        loadCounter--;
                        Debug.Log("Load counter minus");
                    }
                    else
                    {
                        waitingForIcloudResponse = false;
                        Load();
                    }
                }
            }
        } else
        {
            waitingForIcloudResponse = false;
            Load();
        }*/
        // }

        /*private void LoadFromSnapShot(GP_Snapshot theSnapshot)
    {
        Debug.Log("THE RESULTS ARE IN: " + theSnapshot.stringData);
        Debug.Log("Snapshot.Title: " + theSnapshot.meta.Title);
        Debug.Log("Snapshot.Description: " + theSnapshot.meta.Description);
        Debug.Log("Snapshot.CoverImageUrl): " + theSnapshot.meta.CoverImageUrl);
        Debug.Log("Snapshot.stringData: " + theSnapshot.stringData);
        Debug.Log("Snapshot.LastModifiedTimestamp: " + theSnapshot.meta.LastModifiedTimestamp);
        Debug.Log("Snapshot.bytes.Length: " + theSnapshot.bytes.Length);
        Debug.Log("Fresh open status: " + DontDestroyOnLoadSaveLoad.Instance.isFreshOpen);
        if (theSnapshot.stringData == null || theSnapshot.stringData.Length == 0 ||
            DontDestroyOnLoadSaveLoad.Instance.isFreshOpen == false)
        {
            Debug.Log("NOT Fresh open");
            Load();
        }
        else
        {
            //Debug.Log ("BYTES: " + data.bytesValue [0] + data.bytesValue [1] + data.bytesValue [2] + data.bytesValue [3] + data.bytesValue [4] + data.bytesValue [5] + data.bytesValue [6] + data.bytesValue [7] + data.bytesValue [8] + data.bytesValue [9] + data.bytesValue [10] + data.bytesValue [11] + data.bytesValue [12] + data.bytesValue [13] + data.bytesValue [14] + data.bytesValue [15] + data.bytesValue [16] + data.bytesValue [17] + data.bytesValue [18] + data.bytesValue [19] + data.bytesValue [20] + data.bytesValue [21] + data.bytesValue [22] + data.bytesValue [23] + data.bytesValue [24] + data.bytesValue [25] + data.bytesValue [26] + data.bytesValue [27] + data.bytesValue [28] + data.bytesValue [29] + data.bytesValue [30] + data.bytesValue [31] + data.bytesValue [32] + data.bytesValue [33] + data.bytesValue [34] + data.bytesValue [35] + data.bytesValue [36] + data.bytesValue [37] + data.bytesValue [38] + data.bytesValue [39] + data.bytesValue [40]);
            if (ES2.Exists(filename))
            {
                Debug.Log("Comparing clloud to local");
                compareICloudToLocal = true;
            }
            if (compareICloudToLocal)
            {
                ES2.SaveRaw(theSnapshot.bytes, filename + "test");
            }
            else
            {
                ES2.SaveRaw(theSnapshot.bytes, filename);
            }
            Debug.Log("4aaa");
            //File.WriteAllText (Application.persistentDataPath + "/" + filename, data.stringValue);
            StartCoroutine(WaitFrameThenLoad());
        }
    }

    public void RemoveListenersBeforeSwitchingScenes()
    {
        GooglePlayConnection.ActionConnectionResultReceived -= ActionConnectionRecieved;
        GooglePlaySavedGamesManager.ActionNewGameSaveRequest -= ActionNewGameSaveRequest;
        GooglePlaySavedGamesManager.ActionGameSaveLoaded -= ActionGameSaveLoaded;
        GooglePlaySavedGamesManager.ActionConflict -= ActionConflict;
    }

    private void ActionGameSaveLoaded(GP_SpanshotLoadResult result)
    {
        waitingForIcloudResponse = false;
        Debug.Log("ActionGameSaveLoaded: " + result.Message);
        GooglePlaySavedGamesManager.ActionGameSaveLoaded -= ActionGameSaveLoaded;
        if (result.IsSucceeded)
        {
            LoadFromSnapShot(result.Snapshot);
        } else
        {
            Debug.Log("5aaa");
            Load();
        }
    }*/
        /*
    private void ActionConflict(GP_SnapshotConflict result)
    {
        Debug.Log("Conflict Detected: ");
        GP_Snapshot snapshot = result.Snapshot;
        GP_Snapshot conflictSnapshot = result.ConflictingSnapshot;
        // Resolve between conflicts by selecting the newest of the conflicting snapshots.
        GP_Snapshot mResolvedSnapshot = snapshot;
        GooglePlaySavedGamesManager.ActionGameSaveLoaded += ActionGameSaveLoaded;
        GooglePlaySavedGamesManager.ActionConflict -= ActionConflict;
        /*
        if (snapshot.meta.LastModifiedTimestamp < conflictSnapshot.meta.LastModifiedTimestamp)
        {
            mResolvedSnapshot = conflictSnapshot;
        }*/
        /*
        ES2.SaveRaw(snapshot.bytes, filename + "GPConflict1");
        ES2.SaveRaw(conflictSnapshot.bytes, filename + "GPConflict2");


        double conflict1 = StringToDouble(ES2.Load<string>(filename + "GPConflict1" + "?tag=" + totalBrainsEverName));
        double conflict2 = StringToDouble(ES2.Load<string>(filename + "GPConflict2" + "?tag=" + totalBrainsEverName));


        Debug.Log("Tutorial state 1 is: " + ES2.Load<int>(filename + "GPConflict1" + "?tag=" + tutorialState));
        Debug.Log("Tutorial state 2 is: " + ES2.Load<int>(filename + "GPConflict2" + "?tag=" + tutorialState));

        if(conflict1 <= conflict2)
        {
            mResolvedSnapshot = conflictSnapshot;
        }

        result.Resolve(mResolvedSnapshot);
        Debug.Log("6aaa");
        //Load();
        LoadFromSnapShot(mResolvedSnapshot);
    }*/
        /*
    public void SignOutOfGP()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        usingIcloudManager = false;
        waitingForIcloudResponse = false;
        GooglePlayConnection.ActionConnectionResultReceived -= ActionConnectionRecieved;
        GooglePlaySavedGamesManager.ActionNewGameSaveRequest -= ActionNewGameSaveRequest;
        GooglePlaySavedGamesManager.ActionGameSaveLoaded -= ActionGameSaveLoaded;
        GooglePlaySavedGamesManager.ActionConflict -= ActionConflict;
        GooglePlayConnection.Instance.Disconnect();
#endif
    }

    private void ActionNewGameSaveRequest()
    {
        Debug.Log("Making and pushing a saved game to GP");
        if (!turnOffGPSaving)
        {
            if (GooglePlayConnection.Instance.IsConnected)
            {
                GooglePlaySavedGamesManager.Instance.CreateNewSnapshot(gameDataName,
                                                                       "MainSaveData",
                                                                       null,
                                                                       ES2.LoadRaw(filename),
                                                                       0);
            }
        }
    }
    private void ActionConnectionRecieved(GooglePlayConnectionResult result)
    {
        Debug.Log("Connection recieved as " + result.IsSuccess);
    }
    /// <summary>
    /// End google play stoof
    /// </summary>

    public void PickABTestGroup()
    {
        aBTestGroup = UnityEngine.Random.Range(2, 4);
        Debug.Log("ab test group: " + aBTestGroup);
    }*/

        public void StartLoadSequence()
        {
            /*if (PlayerPrefs.GetFloat("GPsavesoff", 1) == 0)
        {
            turnOffGPSaving = false;
        }
        else if (PlayerPrefs.GetFloat("GPsavesoff",1) == 1)
        {
            turnOffGPSaving = true;
        }*/
            if (!loadSequenceStarted)
            {
                loadSequenceStarted = true;
#if UNITY_EDITOR

                Load();
#elif UNITY_ANDROID
            //TODO: PRERELEASE - remove this line - it turns off GP seervices
            turnOffGPSaving = true;
            /*if(!turnOffGPSaving){
                usingIcloudManager = true;
                waitingForIcloudResponse = true;
                Debug.Log("Came into start load sequence");
                GooglePlayConnection.ActionConnectionResultReceived += ActionConnectionRecieved;
                if(!GooglePlayConnection.Instance.IsConnected){
                    GooglePlayConnection.Instance.Connect();
                }
                GooglePlaySavedGamesManager.ActionNewGameSaveRequest += ActionNewGameSaveRequest;
                GooglePlaySavedGamesManager.ActionGameSaveLoaded += ActionGameSaveLoaded;
                GooglePlaySavedGamesManager.ActionConflict += ActionConflict;
                StartCoroutine(WaitSecThenLoadGP());
            } else {*/
                Load();
            //}
#elif UNITY_IOS
        /*iCloudManager.OnCloudInitAction += CloudInitAction;
        //      //iCloudManager.Instance.init();
        //iCloudManager.Instance.setString ("TestStringKey", "Hello World");
        usingIcloudManager = true;
        iCloudManager.OnCloudDataReceivedAction += OnCloudDataReceivedAction;
        waitingForIcloudResponse = true;
        iCloudManager.Instance.requestDataForKey (gameDataName);
            */
#endif
            }
        }
        /*bool compareICloudToLocal = false;
    private void OnCloudDataReceivedAction (iCloudData data) {
        iCloudManager.OnCloudDataReceivedAction -= OnCloudDataReceivedAction;
        
        waitingForIcloudResponse = false;
        if(data == null || data.IsEmpty)
        {
            Load();
        } else {
            //Debug.Log ("BYTES: " + data.bytesValue [0] + data.bytesValue [1] + data.bytesValue [2] + data.bytesValue [3] + data.bytesValue [4] + data.bytesValue [5] + data.bytesValue [6] + data.bytesValue [7] + data.bytesValue [8] + data.bytesValue [9] + data.bytesValue [10] + data.bytesValue [11] + data.bytesValue [12] + data.bytesValue [13] + data.bytesValue [14] + data.bytesValue [15] + data.bytesValue [16] + data.bytesValue [17] + data.bytesValue [18] + data.bytesValue [19] + data.bytesValue [20] + data.bytesValue [21] + data.bytesValue [22] + data.bytesValue [23] + data.bytesValue [24] + data.bytesValue [25] + data.bytesValue [26] + data.bytesValue [27] + data.bytesValue [28] + data.bytesValue [29] + data.bytesValue [30] + data.bytesValue [31] + data.bytesValue [32] + data.bytesValue [33] + data.bytesValue [34] + data.bytesValue [35] + data.bytesValue [36] + data.bytesValue [37] + data.bytesValue [38] + data.bytesValue [39] + data.bytesValue [40]);
            if (ES2.Exists(filename))
            {
                compareICloudToLocal = true;
            }
            if (compareICloudToLocal)
            {
                ES2.SaveRaw(data.bytesValue, filename + "test");
            } else
            {
                ES2.SaveRaw(data.bytesValue, filename);
            }

            //File.WriteAllText (Application.persistentDataPath + "/" + filename, data.stringValue);
            StartCoroutine (WaitFrameThenLoad ());
        }
    }*/

        // IEnumerator WaitFrameThenLoad()
        //{
        /*Debug.Log("bbb");
        yield return new WaitForSecondsRealtime (0.2f);
        Debug.Log("ccc");
        if (compareICloudToLocal)
        {
            Debug.Log("ddd");
            compareICloudToLocal = false;
            double icloudBrains = StringToDouble(ES2.Load<string>(filename + "?tag=" + totalBrainsEverName));
            double localBrains = StringToDouble(ES2.Load<string>(filename + "test" + "?tag=" + totalBrainsEverName));

            if (icloudBrains >= localBrains)
            {
                waitingForIcloudResponse = false;
                Load();
                Debug.Log("eee");
            }
            else
            {
                waitingForIcloudResponse = false;
                ES2.SaveRaw(ES2.LoadRaw(filename + "test"), filename);
                StartCoroutine(WaitFrameThenLoad());
                Debug.Log("fff");
            }
        }
        else
        {
            waitingForIcloudResponse = false;
            Load();
            Debug.Log("ggg");
        }*/
        //  }

        /*public void CloudInitAction(SA.Common.Models.Result result) {
        iCloudManager.OnCloudInitAction -= CloudInitAction;
        if(result.IsSucceeded){
            IOSNativePopUpManager.showMessage("Cloud Init", "Succeeded!");
            iCloudManager.OnCloudDataReceivedAction += OnCloudDataReceivedActionGameManager;
            //iCloudManager.Instance.requestDataForKey ("blah");
        } else {
        //Maybe pop message here?
        }
    }
    private void OnCloudDataReceivedActionGameManager (iCloudData data = null) {
        iCloudManager.OnCloudDataReceivedAction -= OnCloudDataReceivedActionGameManager;
        if (data != null) {
            if (data.IsEmpty) {
                //Initialise with local / default data...
                
            } 
            else 
            {
                if (data.stringValue == "") {
                    //Initialise with local / default data...
                    //GameManager.Instance.InitFromSaveData ();
                } else {
                    //Initialise with saved data...
                    //GameManager.Instance.InitFromSaveData (data.stringValue);
                }
            }
//          iCloudManager.OnCloudDataReceivedAction += OnCloudDataReceivedActionInventoryManager;
//          iCloudManager.Instance.requestDataForKey ("InventoryManagerSettingsiCloud");
        } else {
//          GameManager.Instance.InitFromSaveData ();   
        }
    }*/

        /*public void SaveSingleThing(){
        SetDouble(totalSkulls, GameController.Instance.currencyController.GetCurrency(CurrencyType.Skulls));
    }*/

        int saveInt = 0;
        public bool isLoadComplete = false;

        public void Save(bool resetTierData = false)
        {
            if (isLoadComplete)
            {
                //if (!waitingForIcloudResponse)
                //{
                Debug.Log("saaaaaaaaving...");
                using (ES2Writer writer = ES2Writer.Create(filename))
                {
                    writer.Write(
                        DoubleToString(GameController.Instance.currencyController.GetCurrency(CurrencyType.SoftCurrency)),
                        currentGold);
                    writer.Write(
                        DoubleToString(GameController.Instance.currencyController.GetCurrency(CurrencyType.HardCurrency)),
                        currentDiamonds);

                    for (int i = 0; i < GameController.Instance.allSystems.Length; i++)
                    {
                        writer.Write(GameController.Instance.allSystems[i].systemLevel, systemLevel + i.ToString());
                    }

                    writer.Write(GameController.Instance.tutorialController.currentTutorialState, currentTutorialInt);
                    writer.Write(GameController.Instance.boostsController.currentVIPLevel, currentVIPLevel);
                    writer.Write(GameController.Instance.boostsController.currentVIPExperience, currentVIPExperience);
                    writer.Write(ReturnTimeStamp(UnbiasedTime.Instance.Now()), timeSinceClose);

                    for (int i = 0; i < GameController.Instance.researchController.allResearchItems.Length; i++)
                    {
                        writer.Write(GameController.Instance.researchController.allResearchItems[i].isOwned,
                            GameController.Instance.researchController.allResearchItems[i].researchID + researchSaving);
                    }

                    if (GameController.Instance.researchController.isActivelyResearching)
                    {
                        writer.Write(GameController.Instance.researchController.currentActiveResearch.researchID,
                            currentActiveResearch);
                        writer.Write(ReturnTimeStamp(GameController.Instance.researchController.currentResearchTimer),
                            currentActiveResearch + "timer");
                    }
                    else
                    {
                        writer.Write("LolNo", currentActiveResearch);
                    }


                    for (int i = 0; i < GameController.Instance.tasksController.singleTasks.Count; i++)
                    {
                        writer.Write(GameController.Instance.tasksController.singleTasks[i].isActive,
                            GameController.Instance.tasksController.singleTasks[i].taskShortID + taskActive);
                        writer.Write(
                            DoubleToString(GameController.Instance.tasksController.singleTasks[i].taskCurrentProgress),
                            GameController.Instance.tasksController.singleTasks[i].taskShortID + taskProgress);
                        writer.Write(GameController.Instance.tasksController.singleTasks[i].isCompleted,
                            GameController.Instance.tasksController.singleTasks[i].taskShortID + taskComplete);
                    }

                    for (int i = 0;
                        i < GameController.Instance.allShipCardsController.allShipCardsCurrentlyHeld.Length;
                        i++)
                    {
                        writer.Write(GameController.Instance.allShipCardsController.allShipCardsCurrentlyHeld[i],
                            numberOfUnlockedShips + i.ToString());
                    }

                    writer.Write(DontDestroyDataHolder.Instance.muteAudio, muteAudio);
                    writer.Write(DontDestroyDataHolder.Instance.quickBuy, quickBuy);

                    writer.Save();
                }
                /*if (iCloudManager.Instance != null)
                {
                    iCloudManager.Instance.setData(gameDataName, ES2.LoadRaw(filename));
                }*/

                /*#if UNITY_ANDROID && !UNITY_EDITOR
                if(!turnOffGPSaving){
                    if (GooglePlayConnection.Instance.IsConnected)
                    {
                        Debug.Log(ES2.LoadRaw(filename));
                        GooglePlaySavedGamesManager.Instance.CreateNewSnapshot(gameDataName,
                                                                   "MainSaveData",
                                                                   saveimage,
                                                                   ES2.LoadRaw(filename),
                                                                   0);
                    }
                }
            #endif  */
                //}
            }
        }

        [SerializeField]
        Texture2D saveimage;

        public GameObject testObjectToRemove;
        public bool shouldWeCrash = true;

        public void Load()
        {
            //if (!waitingForIcloudResponse)
            //{
            if (ES2.Exists(filename))
            {
                using (ES2Reader reader = ES2Reader.Create(filename))
                {
                    if (reader.TagExists(currentGold))
                    {
                        double diamonds = 0;
                        if (reader.TagExists(currentDiamonds))
                        {
                            diamonds = StringToDouble(reader.Read<string>(currentDiamonds));
                        }

                        GameController.Instance.currencyController.LoadCurrency(
                            StringToDouble(reader.Read<string>(currentGold)), 0, 0, diamonds, 0);
                    }

                    if (reader.TagExists(systemLevel + 0.ToString()))
                    {
                        for (int i = 0; i < GameController.Instance.allSystems.Length; i++)
                        {
                            if (reader.TagExists(systemLevel + i.ToString()))
                            {
                                GameController.Instance.allSystems[i]
                                    .LoadSystem(reader.Read<int>(systemLevel + i.ToString()));
                            }
                        }
                    }

                    if (reader.TagExists(currentTutorialInt))
                    {
                        GameController.Instance.tutorialController.LoadTutorial(reader.Read<int>(currentTutorialInt));
                    }

                    if (reader.TagExists(currentVIPLevel))
                    {
                        GameController.Instance.boostsController.Load(
                            reader.Read<int>(currentVIPLevel),
                            reader.Read<int>(currentVIPExperience));
                    }

                    for (int i = 0; i < GameController.Instance.tasksController.singleTasks.Count; i++)
                    {
                        if (reader.TagExists(GameController.Instance.tasksController.singleTasks[i].taskShortID +
                                             taskActive))
                        {
                            GameController.Instance.tasksController.singleTasks[i].isActive =
                                reader.Read<bool>(GameController.Instance.tasksController.singleTasks[i].taskShortID +
                                                  taskActive);
                            GameController.Instance.tasksController.singleTasks[i].taskCurrentProgress =
                                StringToDouble(reader.Read<string>(
                                    GameController.Instance.tasksController.singleTasks[i].taskShortID + taskProgress));
                            GameController.Instance.tasksController.singleTasks[i].isCompleted =
                                reader.Read<bool>(GameController.Instance.tasksController.singleTasks[i].taskShortID +
                                                  taskComplete);
                        }
                    }

                    GameController.Instance.tasksController.GetActiveTasks();

                    if (reader.TagExists(muteAudio))
                    {
                        DontDestroyDataHolder.Instance.muteAudio = reader.Read<bool>(muteAudio);
                        DontDestroyDataHolder.Instance.AudioLoaded();
                    }

                    if (reader.TagExists(quickBuy))
                    {
                        DontDestroyDataHolder.Instance.quickBuy = reader.Read<bool>(quickBuy);
                    }

                    DateTime timeLeft = this.ReadTimestamp(timeSinceClose, UnbiasedTime.Instance.Now());
                    TimeSpan timeDiff = UnbiasedTime.Instance.Now() - timeLeft;
                    timeGameOpened = UnbiasedTime.Instance.Now();
                    if (timeDiff.TotalSeconds > 120) //TODO: Prerelease: Check if 2 min timer makes sense. 120
                    {
                        secondsSince = timeDiff.TotalSeconds;
                        if (!alreadyWaiting)
                        {
                            StartCoroutine(WaitThenSpawnPopup());
                        }
                    }
                    else
                    {
                        secondsSince = timeDiff.TotalSeconds;
                        GameController.Instance.currencyController.AddCurrency(
                            secondsSince * GameController.Instance.GetAllPerSecAmounts(), CurrencyType.SoftCurrency, false);
                    }

                    if (reader.TagExists("01" + researchSaving))
                    {
                        for (int i = 0; i < GameController.Instance.researchController.allResearchItems.Length; i++)
                        {
                            GameController.Instance.researchController.allResearchItems[i].isOwned =
                                reader.Read<bool>(GameController.Instance.researchController.allResearchItems[i]
                                    .researchID + researchSaving);
                        }

                        if (reader.Read<string>(currentActiveResearch) != "LolNo")
                        {
                            GameController.Instance.researchController.LoadResearchTimer(
                                this.ReadTimestamp(currentActiveResearch + "timer", UnbiasedTime.Instance.Now()),
                                reader.Read<string>(currentActiveResearch), timeDiff.TotalSeconds);
                        }
                    }

                    if (reader.TagExists(numberOfUnlockedShips + 0.ToString()))
                    {
                        for (int i = 0;
                            i < GameController.Instance.allShipCardsController.allShipCardsCurrentlyHeld.Length;
                            i++)
                        {
                            GameController.Instance.allShipCardsController.allShipCardsCurrentlyHeld[i] =
                                reader.Read<int>(numberOfUnlockedShips + i.ToString());
                        }
                    }

                    GameController.Instance.researchController.LoadAllResearchBoosts();
                }
            }

            //GameController.Instance.LoadFinished ();
            isLoadComplete = true;
            if (DontDestroyDataHolder.Instance.amountEarnedFromAsteroid > 0)
            {
                GameController.Instance.currencyController.AddCurrency(
                    DontDestroyDataHolder.Instance.amountEarnedFromAsteroid, CurrencyType.SoftCurrency);
                DontDestroyDataHolder.Instance.amountEarnedFromAsteroid = 0;
            }

            GameController.Instance.LoadFinished();
            //GameController.Instance.isGPServicesSetup = true;
            // }
        }

        IEnumerator WaitThenSpawnPopup()
        {
            alreadyWaiting = true;
            if (cameFromFullRestart)
            {
                yield return new WaitForSecondsRealtime(4.5f);
                cameFromFullRestart = false;
            }
            else
            {
                yield return new WaitForSecondsRealtime(2f);
            }

            if (GameController.Instance.tutorialController.tutorialCompleted)
            {
                GameController.Instance.OpenReturnToGamePopup(secondsSince *
                                                              GameController.Instance.GetAllPerSecAmounts());
            }

            alreadyWaiting = false;
        }

        bool alreadyWaiting = false;
        double secondsSince = 0;


        void LoadSeveralTimersLol()
        {
            DateTime timeLeft = this.ReadTimestamp(timeSinceClose, UnbiasedTime.Instance.Now());
            TimeSpan timeDiff = UnbiasedTime.Instance.Now() - timeLeft;
            timeGameOpened = UnbiasedTime.Instance.Now();
            if (timeDiff.TotalSeconds > 120) //TODO: Prerelease: Check if 2 min timer makes sense. 120
            {
                secondsSince = timeDiff.TotalSeconds;
                if (!alreadyWaiting)
                {
                    StartCoroutine(WaitThenSpawnPopup());
                }
            }
            else
            {
                secondsSince = timeDiff.TotalSeconds;
                GameController.Instance.currencyController.AddCurrency(
                    secondsSince * GameController.Instance.GetAllPerSecAmounts(), CurrencyType.SoftCurrency, false);
            }
        }

        void OnApplicationPause(bool paused)
        {
            if (paused)
            {
                Save();
            }
            else
            {
                //if (!GameController.Instance.iapController.isWaitingOnIAPToReturn)
                //{
                // if (!waitingForIcloudResponse)
                //{
                if (!leavingForAd)
                {
                    LoadSeveralTimersLol();
                }
                else
                {
                    leavingForAd = false;
                }

                //}
                //}
            }
        }

        public double GetTimeSinceOpening()
        {
            TimeSpan timeDiff = UnbiasedTime.Instance.Now() - timeGameOpened;
            return timeDiff.TotalSeconds;
        }

        void OnApplicationQuit()
        {
            if (!dontsaveonquit)
            {
                Save();
            }
        }

        bool dontsaveonquit = false;

        public void ClearPlayerPrefs()
        {
//        PlayerPrefs.DeleteAll();
//        PlayerPrefs.Save();
            ES2.Delete(filename);
            dontsaveonquit = true;
            Application.Quit();
        }

        ///////////////////////Timer helper functions:
        private DateTime ReadTimestamp(string key, DateTime defaultValue)
        {
            long tmp = 0;
            if (ES2.Exists(filename))
            {
                if (ES2.Exists(filename + "?tag=" + key))
                {
                    tmp = Convert.ToInt64(ES2.Load<string>(filename + "?tag=" + key));
                }

                if (tmp == 0)
                {
                    return defaultValue;
                }
            }
            else
            {
                return defaultValue;
            }

            return DateTime.FromBinary(tmp);
        }

        private void WriteTimestamp(string key, DateTime time)
        {
            ES2.Save(time.ToBinary().ToString(), filename + "?tag=" + key);
        }

        private string ReturnTimeStamp(DateTime time)
        {
            return time.ToBinary().ToString();
        }


        ///////////////////////GETTING AND SETTING DOUBLES:
        /// <summary>
        /// Uses SetString()
        /// </summary>
        public static void SetDouble(string key, double value)
        {
            ES2.Save(DoubleToString(value), filename + "?tag=" + key);
        }

        public static double GetDouble(string key, double defaultValue)
        {
            double returnValue = 0;
            if (ES2.Exists(filename))
            {
                if (ES2.Exists(filename + "?tag=" + key))
                {
                    returnValue = StringToDouble(ES2.Load<string>(filename + "?tag=" + key));
                    if (returnValue == 0)
                    {
                        returnValue = defaultValue;
                    }
                }
                else
                {
                    returnValue = defaultValue;
                }
            }
            else
            {
                return defaultValue;
            }

            return returnValue;
        }

        public static double GetDouble(string key)
        {
            return GetDouble(key, 0d);
        }


        private static string DoubleToString(double target)
        {
            return target.ToString("R");
        }

        private static double StringToDouble(string target)
        {
            if (string.IsNullOrEmpty(target))
                return 0d;

            return double.Parse(target);
        }
    }

}