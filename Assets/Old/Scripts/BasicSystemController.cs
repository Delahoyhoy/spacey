﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class BasicSystemController : MonoBehaviour
    {

        public class LevelUpDataHolder
        {
            public int level;
            public int totalShips;
            public double levelCost;
            public double miningAmount;
            public double amountInCargoShips;

            public enum LevelUpType
            {
                ships,
                miningAmount,
                cargoAmount
            }

            public LevelUpType levelUpType;

            public void SetupData(int _level, int _totalShips, double _levelCost, double _miningAmount,
                double _amountInCargoShips, LevelUpType _levelUpType)
            {
                level = _level;
                totalShips = _totalShips;
                levelCost = _levelCost;
                miningAmount = _miningAmount;
                amountInCargoShips = _amountInCargoShips;
                levelUpType = _levelUpType;
            }
        }

        LevelUpDataHolder[] allLevelUpHolders = new LevelUpDataHolder[37];

        [SerializeField]
        GameObject[] allShipObjects;

        [SerializeField]
        Text systemLevelText;

        [SerializeField]
        Image fastForwardFillObject;

        [SerializeField]
        Text currentHeldAmountText;

        [SerializeField]
        Text shipNameText;

        [SerializeField]
        GameObject upgradeArrow;

        [SerializeField]
        RectTransform systemInfoRectTransform;

        [SerializeField]
        Text nextUpgradeCostText;

        public Image shipImage;
        public SpriteRenderer rockImage;
        public SpriteRenderer cargoBigShipImage;
        public double systemMultiplier;

        double currentHeldAmount = 0;

        [SerializeField]
        GameObject actualCargoShip;

        [SerializeField]
        Animator uiAnimator;

        public double nextLevelCost = 0;
        bool isOpeningUI = false;
        public int systemLevel = 0;
        public string shipName;
        public bool isTier2System = false;

        public double perSecAmount = 0;

        // Use this for initialization
        void Start()
        {
            SetupAllLevelUpHolders();

            shipNameText.text = shipName;
            for (int i = 0; i < allShipObjects.Length; i++)
            {
                if (i < systemLevel)
                {
                    allShipObjects[i].SetActive(true);
                }
                else
                {
                    allShipObjects[i].SetActive(false);
                }
            }

            systemLevelText.text = systemLevel.ToString();
            UpdateLevel0Visuals();
            nextLevelCost = allLevelUpHolders[systemLevel + 1].levelCost;
            nextUpgradeCostText.text = CurrencyController.CurrencyToString(nextLevelCost);
            SetupUsability();
        }

        [SerializeField]
        GameObject systemUIHolder;

        public void SetupUsability()
        {
            if (isTier2System)
            {
                if (!GameController.Instance.researchController.firstScanComplete)
                {
                    systemUIHolder.SetActive(false);
                    shipNameText.text = "Research to Unlock!";
                }
                else
                {
                    systemUIHolder.SetActive(true);
                    if (systemLevel < 1)
                    {
                        shipNameText.text = "Unlock";
                    }
                }
            }
        }

        void UpdateLevel0Visuals()
        {
            if (systemLevel == 0)
            {
                shipNameText.text = "Unlock";
                rockImage.color = new Color(0.5f, 0.5f, 0.5f);
                cargoBigShipImage.color = new Color(0.5f, 0.5f, 0.5f);
                actualCargoShip.SetActive(false);
            }
            else
            {
                shipNameText.text = shipName;
                rockImage.color = new Color(1f, 1f, 1f);
                cargoBigShipImage.color = new Color(1f, 1f, 1f);
                actualCargoShip.SetActive(true);
            }
        }

        void Update()
        {
            if (systemInfoRectTransform.rect.height > 60f)
            {
                if (GameController.Instance.currencyController.GetCurrency(CurrencyType.SoftCurrency) >= nextLevelCost)
                {
                    if (systemLevel + 1 < allLevelUpHolders.Length)
                    {
                        upgradeArrow.SetActive(true);
                    }
                    else
                    {
                        upgradeArrow.SetActive(false);
                    }
                }
                else
                {
                    upgradeArrow.SetActive(false);
                }
            }
            else
            {
                upgradeArrow.SetActive(false);
            }
        }

        bool isAlreadyShowingSpeedup = false;

        public void SetAllShipsToFasterMovement()
        {
            bool didItSpeedUp = false;
            for (int i = 0; i < allShipObjects.Length; i++)
            {
                if (allShipObjects[i].activeSelf)
                {
                    didItSpeedUp = allShipObjects[i].GetComponent<BasicShipEngineController>().SpeedUpShip();
                }
            }

            if (didItSpeedUp)
            {
                actualCargoShip.GetComponent<BasicShipEngineController>().SpeedUpShip();
            }

            if (didItSpeedUp)
            {
                StartCoroutine(MakeTheSpeedupThingShowForXSeconds());
            }
        }

        public void SetToLeave()
        {
            for (int i = 0; i < allShipObjects.Length; i++)
            {
                if (allShipObjects[i].activeSelf)
                {
                    allShipObjects[i].GetComponent<BasicShipEngineController>().SetToLeave();
                }
            }
        }

        public void ShipDroppedOff()
        {
            currentHeldAmount += allLevelUpHolders[systemLevel].miningAmount;
            currentHeldAmountText.text = CurrencyController.CurrencyToString(currentHeldAmount);
        }

        public double ContainerShipPickup()
        {
            if (currentHeldAmount > allLevelUpHolders[systemLevel].amountInCargoShips)
            {
                currentHeldAmount -= allLevelUpHolders[systemLevel].amountInCargoShips;
                currentHeldAmountText.text = CurrencyController.CurrencyToString(currentHeldAmount);
                return allLevelUpHolders[systemLevel].amountInCargoShips;
            }
            else
            {
                double returnNum = currentHeldAmount;
                currentHeldAmount = 0;
                currentHeldAmountText.text = CurrencyController.CurrencyToString(currentHeldAmount);
                return returnNum;
            }
        }

        IEnumerator MakeTheSpeedupThingShowForXSeconds()
        {
            if (!isAlreadyShowingSpeedup)
            {
                isAlreadyShowingSpeedup = true;
                fastForwardFillObject.gameObject.SetActive(true);
                fastForwardFillObject.fillAmount = 1;
                float twoSecondCounter = 2.0f;
                while (twoSecondCounter > 0)
                {
                    twoSecondCounter -= Time.deltaTime;
                    fastForwardFillObject.fillAmount = twoSecondCounter / 2.0f;
                    yield return 0;
                }

                fastForwardFillObject.gameObject.SetActive(false);
                isAlreadyShowingSpeedup = false;
            }
        }

        public void LevelUpSystem()
        {
            if (!DontDestroyDataHolder.Instance.quickBuy)
            {
                if (!isOpeningUI)
                {
                    uiAnimator.Play("Minimise");
                    isOpeningUI = true;
                    StartCoroutine(WaitForAnimFinishThenOpenPopup());
                    if (GameController.Instance.tutorialController.currentTutorialState == 1)
                    {
                        GameController.Instance.tutorialController.IncrementTutorialState();
                    }
                }
            }
            else
            {
                UpgradeSystemLevel();
                if (GameController.Instance.tutorialController.currentTutorialState == 1)
                {
                    GameController.Instance.tutorialController.IncrementTutorialState();
                }
            }
        }

        IEnumerator WaitForAnimFinishThenOpenPopup()
        {
            yield return new WaitForSecondsRealtime(0.4f);

            GameController.Instance.SetupUpgradePopup(this);
            isOpeningUI = false;
        }

        public void PopupDoneClosed()
        {
            StartCoroutine(WaitForAnimFinishThenOpenLocalUI());
        }

        IEnumerator WaitForAnimFinishThenOpenLocalUI()
        {
            yield return new WaitForSecondsRealtime(0.4f);
            uiAnimator.Play("Maximise");
        }

        public void LoadSystem(int whatLevel)
        {
            while (systemLevel < whatLevel)
            {
                UpgradeSystemLevel(true);
            }
        }

        public void UpgradeSystemLevel(bool ignoreCost = false)
        {
            if (systemLevel + 1 < allLevelUpHolders.Length)
            {
                if (ignoreCost || allLevelUpHolders[systemLevel + 1].levelCost <=
                    GameController.Instance.currencyController.GetCurrency(CurrencyType.SoftCurrency))
                {
                    if (!ignoreCost)
                    {
                        GameController.Instance.currencyController.AddCurrency(
                            -allLevelUpHolders[systemLevel + 1].levelCost, CurrencyType.SoftCurrency);
                    }

                    systemLevel++;
                    if (systemLevel == 1)
                    {
                        if (!ignoreCost)
                        {
                            GameController.Instance.tasksController.AddProgress(1, TasksController.TaskType.newSystem);
                            GameController.Instance.SpawnNewShipPopup(this);
                        }
                    }
                    else
                    {
                        if (!ignoreCost)
                        {
                            GameController.Instance.tasksController.AddProgress(1,
                                TasksController.TaskType.systemUpgrade);
                        }
                    }

                    if (systemLevel >= allLevelUpHolders.Length - 1)
                    {
                        systemLevel = allLevelUpHolders.Length - 1;
                    }

                    UpdateLevel0Visuals();

                    if (allLevelUpHolders[systemLevel].levelUpType == LevelUpDataHolder.LevelUpType.ships)
                    {
                        for (int i = 0; i < allShipObjects.Length; i++)
                        {
                            if (allLevelUpHolders[systemLevel].totalShips > i)
                            {
                                allShipObjects[i].SetActive(true);
                            }
                        }
                    }
                }

                //SetAllTextOnScreen();
                if (systemLevel + 1 < allLevelUpHolders.Length)
                {
                    nextLevelCost = allLevelUpHolders[systemLevel + 1].levelCost;
                    nextUpgradeCostText.text = CurrencyController.CurrencyToString(nextLevelCost);
                }
                else
                {
                    nextUpgradeCostText.text = "Max";
                    reachedMaxLevel = true;
                }

                perSecAmount =
                    ((allLevelUpHolders[systemLevel].miningAmount * allLevelUpHolders[systemLevel].totalShips) / 5.0d)
                    / 10.0d;

                systemLevelText.text = systemLevel.ToString();
            }
        }

        public bool reachedMaxLevel = false;

        public void SetAllTextOnScreen(Text _systemLevelText, Text numberOfShipsText, Text miningPerShipText,
            Text cargoShipAmountText,
            Text nextLevelCostText, Text nextLevelUnlockText)
        {
            _systemLevelText.text = systemLevel.ToString();
            numberOfShipsText.text = allLevelUpHolders[systemLevel].totalShips.ToString();
            miningPerShipText.text = CurrencyController.CurrencyToString(allLevelUpHolders[systemLevel].miningAmount);
            cargoShipAmountText.text =
                CurrencyController.CurrencyToString(allLevelUpHolders[systemLevel].amountInCargoShips);

            if (systemLevel >= allLevelUpHolders.Length - 1)
            {
                nextLevelCostText.text = "Max Level";
                nextLevelUnlockText.text = "";
            }
            else
            {
                nextLevelCostText.text =
                    CurrencyController.CurrencyToString(allLevelUpHolders[systemLevel + 1].levelCost);
                switch (allLevelUpHolders[systemLevel + 1].levelUpType)
                {
                    case LevelUpDataHolder.LevelUpType.ships:
                        nextLevelUnlockText.text = "Extra Ship";
                        break;
                    case LevelUpDataHolder.LevelUpType.cargoAmount:
                        nextLevelUnlockText.text = "Cargo Ship capacity";
                        break;
                    case LevelUpDataHolder.LevelUpType.miningAmount:
                        nextLevelUnlockText.text = "Amount mined per ship";
                        break;
                    default:
                        break;
                }
            }
        }

        void SetupAllLevelUpHolders()
        {
            double systemCostMultiplier = (systemMultiplier * systemMultiplier) * 400;

            double systemEarningsMultiplier = systemMultiplier * 60d;

            if (systemCostMultiplier < 1)
            {
                systemCostMultiplier = 1;
            }

            if (systemEarningsMultiplier < 1)
            {
                systemEarningsMultiplier = 1;
            }

            if (systemMultiplier < 1)
            {
                systemMultiplier = 1;
            }

            LevelUpDataHolder temp;
            temp = new LevelUpDataHolder();
            temp.SetupData(0, 0, 5, 0, 0, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[0] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(1, 1, 10 * (systemCostMultiplier * systemMultiplier), 10 * systemEarningsMultiplier,
                10 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[1] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(2, 1, 40 * (systemCostMultiplier * systemMultiplier), 20 * systemEarningsMultiplier,
                10 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[2] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(3, 1, 80 * (systemCostMultiplier * systemMultiplier), 20 * systemEarningsMultiplier,
                100 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[3] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(4, 2, 140 * (systemCostMultiplier * systemMultiplier), 20 * systemEarningsMultiplier,
                100 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[4] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(5, 2, 150 * (systemCostMultiplier * systemMultiplier), 40 * systemEarningsMultiplier,
                100 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[5] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(6, 2, 300 * (systemCostMultiplier * systemMultiplier), 40 * systemEarningsMultiplier,
                200 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[6] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(7, 3, 500 * (systemCostMultiplier * systemMultiplier), 40 * systemEarningsMultiplier,
                200 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[7] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(8, 3, 700 * (systemCostMultiplier * systemMultiplier), 60 * systemEarningsMultiplier,
                200 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[8] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(9, 3, 900 * (systemCostMultiplier * systemMultiplier), 60 * systemEarningsMultiplier,
                300 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[9] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(10, 4, 1100 * (systemCostMultiplier * systemMultiplier), 60 * systemEarningsMultiplier,
                300 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[10] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(11, 4, 1600 * (systemCostMultiplier * systemMultiplier), 80 * systemEarningsMultiplier,
                300 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[11] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(12, 4, 2500 * (systemCostMultiplier * systemMultiplier), 80 * systemEarningsMultiplier,
                400 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[12] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(13, 5, 3100 * (systemCostMultiplier * systemMultiplier), 80 * systemEarningsMultiplier,
                400 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[13] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(14, 5, 3800 * systemCostMultiplier, 100 * systemEarningsMultiplier,
                400 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[14] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(15, 5, 4500 * systemCostMultiplier, 100 * systemEarningsMultiplier,
                600 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[15] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(16, 6, 5500 * systemCostMultiplier, 100 * systemEarningsMultiplier,
                600 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[16] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(17, 6, 6700 * systemCostMultiplier, 120 * systemEarningsMultiplier,
                600 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[17] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(18, 6, 7700 * systemCostMultiplier, 120 * systemEarningsMultiplier,
                800 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[18] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(19, 7, 9000 * systemCostMultiplier, 120 * systemEarningsMultiplier,
                800 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[19] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(20, 7, 12000 * systemCostMultiplier, 140 * systemEarningsMultiplier,
                800 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[20] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(21, 7, 15000 * systemCostMultiplier, 140 * systemEarningsMultiplier,
                1000 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[21] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(22, 8, 20000 * systemCostMultiplier, 140 * systemEarningsMultiplier,
                1000 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[22] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(23, 8, 26000 * systemCostMultiplier, 160 * systemEarningsMultiplier,
                1000 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[23] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(24, 8, 33000 * systemCostMultiplier, 160 * systemEarningsMultiplier,
                1400 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[24] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(25, 9, 34500 * systemCostMultiplier, 160 * systemEarningsMultiplier,
                1400 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[25] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(26, 9, 36000 * systemCostMultiplier, 180 * systemEarningsMultiplier,
                1400 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[26] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(27, 9, 40000 * systemCostMultiplier, 180 * systemEarningsMultiplier,
                1800 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[27] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(28, 10, 42000 * systemCostMultiplier, 180 * systemEarningsMultiplier,
                1800 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[28] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(29, 10, 45000 * systemCostMultiplier, 200 * systemEarningsMultiplier,
                1800 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[29] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(30, 10, 51000 * systemCostMultiplier, 200 * systemEarningsMultiplier,
                2200 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[30] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(31, 11, 55000 * systemCostMultiplier, 200 * systemEarningsMultiplier,
                2200 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[31] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(32, 11, 62000 * systemCostMultiplier, 220 * systemEarningsMultiplier,
                2200 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[32] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(33, 11, 70000 * systemCostMultiplier, 220 * systemEarningsMultiplier,
                2500 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[33] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(34, 12, 80000 * systemCostMultiplier, 220 * systemEarningsMultiplier,
                2500 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.ships);
            allLevelUpHolders[34] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(35, 12, 95000 * systemCostMultiplier, 240 * systemEarningsMultiplier,
                2500 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.miningAmount);
            allLevelUpHolders[35] = temp;

            temp = new LevelUpDataHolder();
            temp.SetupData(36, 12, 111000 * systemCostMultiplier, 240 * systemEarningsMultiplier,
                3000 * systemEarningsMultiplier, LevelUpDataHolder.LevelUpType.cargoAmount);
            allLevelUpHolders[36] = temp;
        }
    }
}