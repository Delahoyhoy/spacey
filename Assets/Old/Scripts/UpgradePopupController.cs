﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class UpgradePopupController : MonoBehaviour
    {
        [SerializeField]
        Animator screenAnimator;

        [SerializeField]
        Image shipImage;

        [SerializeField]
        Text shipNameText;

        [SerializeField]
        Text levelText;

        [SerializeField]
        Text numberOfShipsText;

        [SerializeField]
        Text miningPerShipText;

        [SerializeField]
        Text cargoShipText;

        [SerializeField]
        Text nextLevelUnlocksText;

        [SerializeField]
        Text nextLevelCostText;

        [SerializeField]
        Button upgradeButton;

        [SerializeField]
        GameObject managersOverlay;

        [SerializeField]
        GameObject mainBacking;

        BasicSystemController basicSystemController;


        public void SetupPopup(BasicSystemController _wordUpBro)
        {
            shipImage.sprite = _wordUpBro.shipImage.sprite;
            basicSystemController = _wordUpBro;
            this.gameObject.SetActive(true);
            screenAnimator.Play("Maximise");
            shipNameText.text = basicSystemController.shipName;
            SetAllText();
        }

        public void PressUpgradeButton()
        {
            if (basicSystemController.systemLevel == 0)
            {
                DismissPopup();
            }

            basicSystemController.UpgradeSystemLevel();
            SetAllText();
        }

        public void SetAllText()
        {
            basicSystemController.SetAllTextOnScreen(levelText, numberOfShipsText, miningPerShipText, cargoShipText,
                nextLevelCostText, nextLevelUnlocksText);
        }

        public void DismissPopup()
        {
            GameController.Instance.IsPopupOpenNow = false;
            screenAnimator.Play("Minimise");
            basicSystemController.PopupDoneClosed();
            StartCoroutine(WaitThenDestroyThisBud());
        }

        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }

        private void Update()
        {
            if (GameController.Instance.currencyController.GetCurrency(CurrencyType.SoftCurrency) >=
                basicSystemController.nextLevelCost &&
                !basicSystemController.reachedMaxLevel)
            {
                upgradeButton.interactable = true;
            }
            else
            {
                upgradeButton.interactable = false;
            }
        }

        public void OpenManagersPopupThing()
        {
            managersOverlay.SetActive(true);
            mainBacking.SetActive(false);
        }
    }

}