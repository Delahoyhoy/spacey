﻿using System;
using Core.Gameplay.Tutorial;
using UnityEngine;
using UnityEngine.Events;

namespace Legacy
{
    [Serializable]

    public class TutorialStage
    {

        [SerializeField]
        private string ID;

        [SerializeField]
        private bool bannerActive = true;

        [SerializeField]
        private string bannerID;

        [SerializeField]
        private bool characterActive = false;

        [SerializeField]
        private string characterID;

        public string StageID
        {
            get { return ID; }
        }

        public bool BannerActive
        {
            get { return bannerActive; }
        }

        public string BannerID
        {
            get { return bannerID; }
        }

        public bool CharacterActive
        {
            get { return characterActive; }
        }

        public string CharacterID
        {
            get { return characterID; }
        }

        [SerializeField]
        private bool triggerOnValue;

        [SerializeField]
        private double targetValue = 0;

        private double currentValue = 0;

        [SerializeField]
        private FeatureMatrix featureMatrix;

        [SerializeField]
        private string sheenID;

        [SerializeField]
        private bool isSafeState = false;

        [SerializeField]
        private SafeState safety = SafeState.Next;

        public SafeState Safety
        {
            get { return safety; }
        }

        public bool IsSafeState
        {
            get { return isSafeState; }
        }

        [SerializeField]
        public UnityEvent onBeginEvent, onTriggerEvent, onLoadEvent;

        public string SheenID
        {
            get { return sheenID; }
        }

        public FeatureMatrix Features
        {
            get { return featureMatrix; }
        }

        public double CurrentValue
        {
            get { return currentValue; }
            set
            {
                currentValue = value;
                if (currentValue >= targetValue)
                    Trigger();
            }
        }

        [SerializeField]
        private bool cameraLocked = false;

        public bool CameraLocked
        {
            get { return cameraLocked; }
        }

        [SerializeField]
        private bool highlightDisplay;

        [SerializeField]
        private string highlightID;

        public bool HighlightActive
        {
            get { return highlightDisplay; }
        }

        public string HighlightID
        {
            get { return highlightID; }
        }

        //public void EnableSheen()
        //{
        //    List<GameObject> sheen = FeatureIdentifier.GetAllFeaturesByName(sheenID);
        //    for (int i = 0; i < sheen.Count; i++)
        //        sheen[i].SetActive(true);
        //}

        //public void DisableSheen()
        //{
        //    List<GameObject> sheen = FeatureIdentifier.GetAllFeaturesByName(sheenID);
        //    for (int i = 0; i < sheen.Count; i++)
        //        sheen[i].SetActive(false);
        //}

        public void BeginStage()
        {
            if (onBeginEvent != null)
                onBeginEvent.Invoke();
        }

        public void Trigger()
        {
            if (onTriggerEvent != null)
                onTriggerEvent.Invoke();
            //TutorialManager.OnCompleteCurrentStage();
        }

        public void Load()
        {
            if (onLoadEvent != null)
                onLoadEvent.Invoke();
        }
    }
}