﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class FeatureIdentifier : MonoBehaviour
    {

        [SerializeField]
        protected string ID;

        public string FeatureID
        {
            get { return ID; }
        }

        [SerializeField]
        private Feature type;

        public Feature FeatureType
        {
            get { return type; }
        }

        public delegate void FeatureByName(string check, ref List<GameObject> objects);

        public static event FeatureByName GetFeaturesByName;

        public delegate void FeatureByType(Feature check, ref List<FeatureIdentifier> objects);

        public static event FeatureByType GetFeaturesByType;

        public virtual void IsID(string check, ref List<GameObject> objects)
        {
            if (ID.Equals(check))
                objects.Add(gameObject);
        }

        public static List<GameObject> GetAllFeaturesByName(string check)
        {
            List<GameObject> objects = new List<GameObject>();
            if (GetFeaturesByName != null)
                GetFeaturesByName(check, ref objects);
            return objects;
        }

        public virtual void IsType(Feature check, ref List<FeatureIdentifier> objects)
        {
            if (check == Feature.All || (check == Feature.Any && type != Feature.None) || check == type)
                objects.Add(this);
            //Debug.Log(ID + " " + type);
        }

        public virtual void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public static List<FeatureIdentifier> GetAllFeaturesByType(Feature check)
        {
            List<FeatureIdentifier> objects = new List<FeatureIdentifier>();
            GetFeaturesByType?.Invoke(check, ref objects);
            return objects;
        }

        void OnEnable()
        {
            //if (GameController.Instance)
            //{
            //    if (GameController.Instance.oldTutorial && type == Feature.None)
            //        gameObject.SetActive(false);
            //}
            GetFeaturesByType += IsType;
            GetFeaturesByName += IsID;
        }

        void OnDisable()
        {
            GetFeaturesByType -= IsType;
            GetFeaturesByName -= IsID;
        }

        private void OnDestroy()
        {
            GetFeaturesByType -= IsType;
            GetFeaturesByName -= IsID;
        }
    }

}