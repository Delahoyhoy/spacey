﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Save;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Legacy
{
    public class TutorialManager : MonoBehaviour, ISaveLoad
    {

        [Header("Tutorial Stages")]
        [SerializeField]
        private TutorialStage[] allStages;

        [Header("Tutorial Objects")]
        [SerializeField]
        private Transform pointerTransform;

        [SerializeField]
        private GameObject tutorialCharacter, pointer, textBox;

        [SerializeField]
        private Text tutorialText;

        [SerializeField]
        private ParticleSystem[] fireworks;

        private int currentStage;

        public int CurrentStage
        {
            get { return currentStage >= allStages.Length ? 100 : currentStage; }
        }

        public string StageName
        {
            get { return currentStage < allStages.Length ? allStages[currentStage].StageID : "n/a"; }
        }

        public delegate void TriggerStage(string ID);

        public static event TriggerStage OnTriggerStage;

        public delegate void Complete();

        public static event Complete OnComplete;

        public delegate void Increment(string ID, double newValue);

        public static event Increment OnIncrement;

        private bool tutorialCompleted = false;

        public bool TutorialCompleted
        {
            get { return tutorialCompleted; }
            private set { tutorialCompleted = value; }
        }

        private static TutorialManager instance;

        public static TutorialManager Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<TutorialManager>();
                return instance;
            }
        }

        private FeatureMatrix currentMatrix = FeatureMatrix.GetEmptyMatrix();
        private List<GameObject> sheenObjects = new List<GameObject>();

        private Dictionary<Feature, List<FeatureIdentifier>> features =
            new Dictionary<Feature, List<FeatureIdentifier>>();

        private Dictionary<string, List<FeatureIdentifier>> toggleObjects =
            new Dictionary<string, List<FeatureIdentifier>>();

        private UIFogTest highlightScreen;

        public UIFogTest HighlightScreen
        {
            get { return highlightScreen; }
        }

        [SerializeField]
        private SplashScreen splashScreen;

        private bool featuresInitialised = false;

        void OnEnable()
        {
            OnTriggerStage += Trigger;
            OnComplete += CompleteCurrentStage;
            OnIncrement += IncrementStage;
        }

        void OnDisable()
        {
            OnTriggerStage -= Trigger;
            OnComplete -= CompleteCurrentStage;
            OnIncrement -= IncrementStage;
        }

        void OnDestroy()
        {
            OnTriggerStage -= Trigger;
            OnComplete -= CompleteCurrentStage;
            OnIncrement -= IncrementStage;
        }

        public static void OnTrigger(string ID)
        {
            if (OnTriggerStage != null)
                OnTriggerStage(ID);
        }

        bool IndexInBounds(int index)
        {
            return index >= 0 && index < allStages.Length;
        }

        public void Trigger(string ID)
        {
            if (tutorialCompleted || !IndexInBounds(currentStage))
                return;
            if (allStages[currentStage].StageID == ID)
            {
                allStages[currentStage].Trigger();
            }
        }

        public static void OnIncrementStage(string ID, double newValue)
        {
            if (OnIncrement != null)
                OnIncrement(ID, newValue);
        }

        public void TriggerCurrentStage()
        {
            if (tutorialCompleted || !IndexInBounds(currentStage))
                return;
            allStages[currentStage].Trigger();
        }

        public void IncrementStage(string ID, double newValue)
        {
            if (tutorialCompleted || !IndexInBounds(currentStage))
                return;
            if (allStages[currentStage].StageID.Equals(ID))
            {
                allStages[currentStage].CurrentValue = newValue;
                if (TutorialBanner.InstanceExists)
                {
                    TutorialBanner.Instance.SetValue(newValue);
                }
            }
        }

        private void SetCurrentStage(int stage)
        {
            if (stage >= allStages.Length)
            {
                //TutorialBanner.Instance.DisableBanner();
                TutorialCharacter.Instance.HideCharacter();
                TutorialCharacter.Instance.gameObject.SetActive(false);
                highlightScreen.Hide();
                //if (TownSceneController.Instance)
                //{
                //    TownSceneController.Instance.camControl = true;
                //    TownSceneController.Instance.UnlockAllShops();
                //}
                tutorialCompleted = true;
                //PlayerPrefs.SetInt("tutStage", 100);
                return;
            }

            UpdateFeatures(allStages[stage]);
            ToggleSheenObjects(true);
            if (TutorialBanner.InstanceExists)
            {
                if (allStages[stage].BannerActive)
                    TutorialBanner.Instance.SetDisplay(allStages[stage].BannerID, hideFlags > 0);
                else
                    TutorialBanner.Instance.DisableBanner();
            }

            if (TutorialCharacter.Instance)
            {
                if (allStages[stage].CharacterActive)
                    TutorialCharacter.Instance.ShowTutorialCharacterStage(allStages[stage].CharacterID);
                else
                {
                    if (TutorialCharacter.Instance.IsCharacterActive)
                        TutorialCharacter.Instance.HideCharacter();
                }
            }

            //if (TownSceneController.Instance)
            //{
            //    TownSceneController.Instance.camControl = !allStages[stage].CameraLocked;
            //    TownSceneController.Instance.SetShopsUnlocked(allStages[stage].ShopsUnlocked);
            //}
            if (allStages[stage].HighlightActive)
            {
                highlightScreen.ShowHighlight(allStages[stage].HighlightID);
            }
            else
            {
                highlightScreen.Hide();
            }

            //PlayerPrefs.SetInt("tutStage", stage);
            StartCoroutine(WaitFrameThenBegin(stage));
        }

        public void SetActiveByName(string tag)
        {
            List<GameObject> taggedObjects = FeatureIdentifier.GetAllFeaturesByName(tag);
            foreach (GameObject obj in taggedObjects)
            {
                obj.SetActive(true);
            }
        }

        public void SetInactiveByName(string tag)
        {
            List<GameObject> taggedObjects = FeatureIdentifier.GetAllFeaturesByName(tag);
            foreach (GameObject obj in taggedObjects)
            {
                obj.SetActive(false);
            }
        }

        IEnumerator WaitFrameThenBegin(int stage)
        {
            yield return null;
            allStages[stage].BeginStage();
        }

        public void Swap(int index1, int index2)
        {
            List<TutorialStage> tutorialStages = new List<TutorialStage>(allStages);
            TutorialStage temp = tutorialStages[index1];
            tutorialStages[index1] = tutorialStages[index2];
            tutorialStages[index2] = temp;
            allStages = tutorialStages.ToArray();
        }

        private void StartTutorial()
        {
            SetCurrentStage(currentStage);
        }

        public void NextStage()
        {
            currentStage++;
            SetCurrentStage(currentStage);
        }

        public void ToggleSheenObjects(bool condition)
        {
            if (toggleObjects.ContainsKey(allStages[currentStage].SheenID))
            {
                List<FeatureIdentifier> sheenObjs = toggleObjects[allStages[currentStage].SheenID];
                for (int i = 0; i < sheenObjs.Count; i++)
                    sheenObjs[i].SetActive(condition);
            }
        }

        public void CompleteCurrentStage()
        {
            ToggleSheenObjects(false);
            if (TutorialBanner.Instance)
            {
                if (TutorialBanner.Instance.IsActive)
                {
                    TutorialBanner.Instance.Pulse();
                    //StartCoroutine(Fireworks());
                    //GameController.Instance.soundManager.PlayExplosionSound();
                }
            }

            NextStage();
        }

        public static void OnCompleteCurrentStage()
        {
            if (OnComplete != null)
                OnComplete();
        }

        public void DisableAllFeatures()
        {
            foreach (Feature key in features.Keys)
            {
                List<FeatureIdentifier> allFeatures = features[key];
                for (int i = 0; i < allFeatures.Count; i++)
                    allFeatures[i].SetActive(false);
            }
        }

        public void DisableAllSheen()
        {
            foreach (string key in toggleObjects.Keys)
            {
                List<FeatureIdentifier> objects = toggleObjects[key];
                for (int i = 0; i < objects.Count; i++)
                    objects[i].SetActive(false);
            }
        }

        private IEnumerator Fireworks()
        {
            if (fireworks.Length > 0)
            {
                List<ParticleSystem> fireworksList = new List<ParticleSystem>(fireworks);
                while (fireworksList.Count > 0)
                {
                    int index = Random.Range(0, fireworksList.Count);
                    fireworksList[index].Emit(Random.Range(10, 20));
                    yield return new WaitForSeconds(0.125f);
                    fireworksList[index].Emit(Random.Range(10, 20));
                    fireworksList.RemoveAt(index);
                    yield return new WaitForSeconds(Random.Range(0.5f, 1f));
                }
            }
        }

        public void UpdateFeatures(TutorialStage newStage)
        {
            List<Feature> changedFeatures = newStage.Features.FindAllChangedFeatures(currentMatrix);
            foreach (Feature key in changedFeatures)
            {
                if (features.ContainsKey(key))
                {
                    List<FeatureIdentifier> allFeatureObjects = features[key];
                    bool featureActive = newStage.Features.GetValueForFeature(key);
                    //Debug.Log(key + " " + featureActive);
                    for (int j = 0; j < allFeatureObjects.Count; j++)
                    {
                        allFeatureObjects[j].SetActive(featureActive);
                    }
                }
            }

            currentMatrix = newStage.Features;
        }

        private void EnableAllFeatures()
        {
            foreach (Feature key in features.Keys)
            {
                List<FeatureIdentifier> allFeatures = features[key];
                for (int i = 0; i < allFeatures.Count; i++)
                    allFeatures[i].SetActive(true);
            }
        }

        // Use this for initialization
        void Awake()
        {
            highlightScreen = ObjectFinder.FindObjectOfType<UIFogTest>();
            instance = this;
        }

        public void DoCustomHighlight(Transform target, float radius, HighlightType highlightType)
        {
            highlightScreen.ShowCustomHighlight(target, radius, highlightType);
        }

        private void AddFeatureObject(Feature feature, FeatureIdentifier obj)
        {
            if (!features.ContainsKey(feature))
                features.Add(feature, new List<FeatureIdentifier>());
            features[feature].Add(obj);
        }

        private void SetupFeatures()
        {
            if (featuresInitialised)
                return;
            List<FeatureIdentifier>
                allFeatures =
                    ObjectFinder
                        .FindAllObjectsOfType<
                            FeatureIdentifier>(); //FeatureIdentifier.GetAllFeaturesByType(Feature.All);
            for (int i = 0; i < allFeatures.Count; i++)
            {
                if (allFeatures[i].FeatureType != Feature.None)
                    AddFeatureObject(allFeatures[i].FeatureType, allFeatures[i]);
                else
                    AddToggleObject(allFeatures[i].FeatureID, allFeatures[i]);
            }

            featuresInitialised = true;
        }

        private void AddToggleObject(string ID, FeatureIdentifier obj)
        {
            if (!toggleObjects.ContainsKey(ID))
                toggleObjects.Add(ID, new List<FeatureIdentifier>());
            toggleObjects[ID].Add(obj);
        }

        public void LoadAtStage(int stage)
        {
            if (!featuresInitialised)
                SetupFeatures();
            if (stage >= allStages.Length)
            {
                currentStage = 100;
                DisableNewTutorial();
                //tutorialCompleted = true;
                return;
            }

            splashScreen.ForceClose();
            if (TutorialBanner.Instance)
                TutorialBanner.Instance.gameObject.SetActive(true);
            if (TutorialCharacter.Instance)
                TutorialCharacter.Instance.gameObject.SetActive(false);
            highlightScreen.Hide();
            DisableAllFeatures();
            DisableAllSheen();
            int newIndex = GetSafeState(stage);
            currentStage = newIndex;
            SetCurrentStage(currentStage);
            allStages[currentStage].Load();
            StartCoroutine(WaitASecondThenSheen());
        }

        IEnumerator WaitASecondThenSheen()
        {
            yield return new WaitForSeconds(1f);
            ToggleSheenObjects(true);
        }

        public void DisableNewTutorial()
        {
            if (!featuresInitialised)
                SetupFeatures();
            currentStage = 100;
            if (TutorialBanner.Instance)
                TutorialBanner.Instance.DisableBanner();
            if (TutorialCharacter.Instance)
            {
                TutorialCharacter.Instance.HideCharacter();
                TutorialCharacter.Instance.gameObject.SetActive(false);
            }

            //if (TownSceneController.Instance)
            //{
            //    TownSceneController.Instance.camControl = true;
            //    TownSceneController.Instance.UnlockAllShops();
            //}
            highlightScreen.Hide();
            DisableAllSheen();
            tutorialCompleted = true;
            //EnableAllFeatures();
        }

        public void InsertNewStage(int index)
        {
            List<TutorialStage> stages = new List<TutorialStage>(allStages);
            stages.Insert(index, new TutorialStage());
            allStages = stages.ToArray();
        }

        void Start()
        {
            if (!featuresInitialised)
                SetupFeatures();
            DisableAllSheen();
            //Debug.Log("FEATURES: " + features.Count);
            //Debug.Log("TOGGLES: " + toggleObjects.Count);
            ////if (GameController.Instance.oldTutorial)
            ////{
            ////    TutorialBanner.Instance.DisableBanner();
            ////    DisableAllSheen();
            ////    return;
            ////}
            //if (!TutorialCompleted)
            //{
            //    DisableAllFeatures();
            //    DisableAllSheen();
            //}
            //StartTutorial();
            //LoadAtStage(PlayerPrefs.GetInt("tutStage", 0));
        }

        private List<int> GetSafeStates()
        {
            List<int> safeStates = new List<int>();
            for (int i = 0; i < allStages.Length; i++)
            {
                if (allStages[i].IsSafeState)
                    safeStates.Add(i);
            }

            return safeStates;
        }

        public void LoadAtStage(string stageID)
        {
            for (int i = 0; i < allStages.Length; i++)
            {
                if (allStages[i].StageID.Equals(stageID))
                {
                    LoadAtStage(i);
                    return;
                }
            }

            LoadAtStage(0);
            return;
        }

        private int GetSafeState(int index)
        {
            List<int> allSafeStates = GetSafeStates();
            if (allSafeStates.Contains(index))
                return index;
            if (index < allStages.Length && index > 0)
            {
                SafeState safety = allStages[index].Safety;
                for (int i = 0; i < allSafeStates.Count; i++)
                {
                    if (safety == SafeState.Next && allSafeStates[i] >= index)
                        return allSafeStates[i];
                    if (safety == SafeState.Previous && allSafeStates[i] >= index && i > 0)
                        return allSafeStates[i - 1];
                }
            }

            return allSafeStates[allSafeStates.Count - 1];
        }

        public void ForceDisableAllSheen()
        {
            StartCoroutine(WaitDisableSheen());
        }

        private int hideFlags = 0;

        public void ToggleShowTutorial(bool show)
        {
            if (show)
                hideFlags--;
            else
                hideFlags++;
            if (hideFlags < 0)
                hideFlags = 0;
            if (!tutorialCompleted)
            {
                if (show && currentStage < allStages.Length)
                {
                    if (allStages[currentStage].BannerActive)
                        TutorialBanner.Instance.EnableBanner();
                }
                else
                    TutorialBanner.Instance.DisableBanner();
            }
        }

        private IEnumerator WaitDisableSheen()
        {
            yield return new WaitForSeconds(1f);
            List<FeatureIdentifier> allSheens = FeatureIdentifier.GetAllFeaturesByType(Feature.None);
            for (int i = 0; i < allSheens.Count; i++)
                allSheens[i].SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            //if (Input.GetKeyDown(KeyCode.P))
            //{
            //    //StartCoroutine(Fireworks());
            //    //Debug.Log("FIRE!");
            //}
            //if (Input.GetKeyDown(KeyCode.L))
            //    currentStage = 0;
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("tutorialStage", CurrentStage);
        }

        public void LoadDefault()
        {
            ////return;
            if (GameVersionManager.CurrentVersion != GameVersion.Version2)
                LoadAtStage(100);
            else
                LoadAtStage(0);
        }

        //public void Load(ES2Data loadData)
        //{
        //    if (loadData.TagExists("tutorialStage"))
        //        LoadAtStage(loadData.Load<int>("tutorialStage"));
        //    else
        //        LoadAtStage(0);
        //}

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            //return;
            int stage = GameVersionManager.CurrentVersion != GameVersion.Version1
                ? loadedData.LoadData("tutorialStage", 100)
                : 100;
            LoadAtStage(stage);
        }
    }

}