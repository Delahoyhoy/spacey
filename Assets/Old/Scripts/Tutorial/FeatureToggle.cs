using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class FeatureToggle : FeatureIdentifier
    {
        [SerializeField]
        private GameObject activeObject, inactiveObject;

        public override void SetActive(bool active)
        {
            activeObject.SetActive(active);
            inactiveObject.SetActive(!active);
        }
    }

}