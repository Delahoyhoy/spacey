using System.Collections;
using System.Collections.Generic;
using Fumb.Attribute;
using UnityEngine.Events;
using UnityEngine;

namespace Legacy
{
    public class FeatureEvent : FeatureIdentifier
    {
        [SerializeField]
        private UnityEvent<bool> onToggleFeature;

        [SerializeField]
        private bool splitToggleEvents;

        [SerializeField] [ConditionalField("splitToggleEvents")]
        private UnityEvent onSetActive, onSetInactive;

        public override void SetActive(bool active)
        {
            onToggleFeature?.Invoke(active);
            if (!splitToggleEvents)
                return;

            if (active)
                onSetActive?.Invoke();
            else
                onSetInactive?.Invoke();
        }
    }

}