﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class TutorialBanner : MonoBehaviour
    {

        [SerializeField]
        private Text beforeText, afterText, counterText;

        [SerializeField]
        private Image image;

        [SerializeField]
        private Slider fillBar;

        private Dictionary<string, TutorialDisplay> displayDictionary = new Dictionary<string, TutorialDisplay>();

        [SerializeField]
        private TutorialDisplay[] allOptions;

        private double currentMaxValue = 0;

        private static TutorialBanner instance;

        public static TutorialBanner Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<TutorialBanner>();
                return instance;
            }
        }

        public static bool InstanceExists
        {
            get { return Instance; }
        }

        public bool IsActive
        {
            get { return gameObject.activeSelf; }
        }

        [SerializeField]
        private RectTransform layoutGroup;

        private float lerp = 0;
        private Vector3 normalScale = Vector3.one, pulseScale = Vector3.one * 1.25f;

        // Use this for initialization
        void Awake()
        {
            instance = this;
            for (int i = 0; i < allOptions.Length; i++)
            {
                if (!displayDictionary.ContainsKey(allOptions[i].name))
                    displayDictionary.Add(allOptions[i].name, allOptions[i]);
            }
        }

        public void SetDisplay(string display, bool hide = false)
        {
            gameObject.SetActive(true);
            if (displayDictionary.ContainsKey(display))
                SetupDisplay(displayDictionary[display]);
            if (layoutGroup)
                LayoutRebuilder.ForceRebuildLayoutImmediate(layoutGroup);
            if (hide)
                gameObject.SetActive(false);
            //Debug.Log(display);
        }

        public void SetupDisplay(TutorialDisplay display)
        {
            beforeText.gameObject.SetActive(display.hasBefore);
            if (display.hasBefore)
                beforeText.text = display.beforeText;
            afterText.gameObject.SetActive(display.hasAfter);
            if (display.hasAfter)
                afterText.text = display.afterText;
            image.gameObject.SetActive(display.hasImage);
            if (display.hasImage)
                image.sprite = display.image;
            fillBar.gameObject.SetActive(display.counted);
            counterText.gameObject.SetActive(display.counted);
            SetupCounter(display.maxValue);
            //Debug.Log("Setup Display!");
        }

        public void SetupCounter(double max)
        {
            currentMaxValue = max;
            counterText.text = "0/" + (int)max;
            fillBar.value = 0;
        }

        public void SetValue(double newValue)
        {
            float value = (float)(newValue / currentMaxValue);
            counterText.text = Mathf.Min((int)newValue, (int)currentMaxValue) + "/" + (int)currentMaxValue;
            fillBar.value = value;
        }

        public void DisableBanner()
        {
            gameObject.SetActive(false);
        }

        public void EnableBanner()
        {
            gameObject.SetActive(true);
        }

        public void Pulse()
        {
            lerp = 1f;
        }

        // Update is called once per frame
        void Update()
        {
            if (lerp > 0)
            {
                lerp = Mathf.Lerp(lerp, 0, Time.deltaTime * 3f);
                if (lerp < 0.05f)
                    lerp = 0;
                transform.localScale = Vector3.Lerp(normalScale, pulseScale, lerp);
            }
        }
    }

}