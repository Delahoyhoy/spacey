﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Legacy
{
    [Serializable]

    public struct TutorialCharacterStage
    {
        public string ID;

        [Multiline]
        public string displayText;

        public bool continueButton;
        public float additionalDelay;
        public UnityEvent onBegin, onEnd;

        public void Begin()
        {
            onBegin?.Invoke();
        }

        public void End()
        {
            onEnd?.Invoke();
        }
    }
}