﻿using System;
using System.Collections.Generic;

namespace Legacy
{
    [Serializable]

    public class FeatureMatrix
    {
        public bool topRight,
            topLeft,
            play,
            bottomBar,
            leftSide,
            mine;

        public FeatureMatrix(bool set, bool cur, bool go, bool ad, bool skill, bool mineButton)
        {
            topRight = set;
            topLeft = cur;
            play = go;
            bottomBar = ad;
            leftSide = skill;
            mine = mineButton;
        }

        public static FeatureMatrix GetEmptyMatrix()
        {
            return new FeatureMatrix(false, false, false, false, false, false);
        }

        public List<Feature> FindAllChangedFeatures(FeatureMatrix otherMatrix)
        {
            List<Feature> changedFeatures = new List<Feature>();
            if (topRight != otherMatrix.topRight)
                changedFeatures.Add(Feature.TopRight);
            if (topLeft != otherMatrix.topLeft)
                changedFeatures.Add(Feature.TopLeft);
            if (play != otherMatrix.play)
                changedFeatures.Add(Feature.Skills);
            if (bottomBar != otherMatrix.bottomBar)
                changedFeatures.Add(Feature.BottomBar);
            if (leftSide != otherMatrix.leftSide)
                changedFeatures.Add(Feature.LeftSide);
            if (mine != otherMatrix.mine)
                changedFeatures.Add(Feature.MineButton);
            return changedFeatures;
        }

        public bool GetValueForFeature(Feature feature)
        {
            switch (feature)
            {
                case Feature.TopRight:
                    return topRight;
                case Feature.TopLeft:
                    return topLeft;
                case Feature.BottomBar:
                    return bottomBar;
                case Feature.LeftSide:
                    return leftSide;
                case Feature.Skills:
                    return play;
                case Feature.MineButton:
                    return mine;
                default:
                    return false;

            }
        }
    }
}