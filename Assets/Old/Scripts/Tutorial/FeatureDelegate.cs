﻿namespace Legacy
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class FeatureDelegate : FeatureIdentifier
    {

        [SerializeField]
        private GameObject delegateObject;

        void OnEnable()
        {
            GetFeaturesByType += IsType;
            GetFeaturesByName += IsID;
        }

        void OnDisable()
        {
            GetFeaturesByType -= IsType;
            GetFeaturesByName -= IsID;
        }

        private void OnDestroy()
        {
            GetFeaturesByType -= IsType;
            GetFeaturesByName -= IsID;
        }

        public override void IsID(string check, ref List<GameObject> objects)
        {
            if (ID.Equals(check) && delegateObject)
                objects.Add(delegateObject);
        }

        public override void SetActive(bool active)
        {
            delegateObject.SetActive(active);
        }

        public override void IsType(Feature type, ref List<FeatureIdentifier> objects)
        {
            // Don't bother for delegates
        }
    }

}