﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.UserInterface;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Legacy
{
    public class TutorialCharacter : MonoBehaviour
    {

        [SerializeField]
        private Text displayText;

        [SerializeField]
        private Animator characterAnim;

        public static TutorialCharacter Instance;

        public bool IsCharacterActive
        {
            get { return gameObject.activeSelf; }
        }

        private bool characterHidden = true;

        private Dictionary<string, TutorialCharacterStage> allStages = new Dictionary<string, TutorialCharacterStage>();

        [SerializeField]
        private TutorialCharacterStage[] characterStages;

        [SerializeField]
        private BaseTypewriter typewriter;

        private void ShowTutorialCharacter(string textToShow)
        {
            gameObject.SetActive(true);
            if (characterHidden)
            {
                if (characterAnim)
                    characterAnim.SetBool("MoveIn", true);
                characterHidden = false;
            }

            if (displayText)
                displayText.text = textToShow;
        }

        public void PlayClip(string clipID)
        {
            if (AudioManager.Instance)
                AudioManager.Instance.PlayClip(clipID);
        }

        public void ShowTutorialCharacterStage(string stageID)
        {
            if (allStages.ContainsKey(stageID))
            {
                ShowTutorialCharacter(allStages[stageID].displayText);
                if (typewriter)
                    typewriter.PlayTypeWriter(allStages[stageID].displayText, allStages[stageID].Begin,
                        allStages[stageID].End, allStages[stageID].continueButton, allStages[stageID].additionalDelay);
            }
        }

        [SerializeField]
        private RectTransform panel;

        public void MoveToTopOfScreen()
        {
            float height = panel.sizeDelta.y;
            panel.localPosition = new Vector3(-(Screen.width / 2f) / transform.root.localScale.x,
                ((Screen.height / 2f) / transform.root.localScale.x) - 160f, 0);
        }

        public void MoveToBottomOfScreen()
        {
            panel.localPosition =
                new Vector3(-Screen.width / 2f, -Screen.height / 2f, 0) / (transform.root.localScale.x);
        }

        public void HideCharacter()
        {
            if (characterAnim)
            {
                characterAnim.SetBool("MoveIn", false);
                characterHidden = true;
                //
            }

            StartCoroutine(WaitDisable());
        }

        void DisableCharacter()
        {
            gameObject.SetActive(false);
        }

        IEnumerator WaitDisable()
        {
            yield return new WaitForSeconds(1f);
            if (characterHidden)
                gameObject.SetActive(false);
        }

        // Use this for initialization
        void Awake()
        {
            Instance = this;
            for (int i = 0; i < characterStages.Length; i++)
            {
                if (!allStages.ContainsKey(characterStages[i].ID))
                    allStages.Add(characterStages[i].ID, characterStages[i]);
            }
        }

        IEnumerator WaitShowCharacter(string textToShow, float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            ShowTutorialCharacter(textToShow);
        }

        private void Start()
        {
            //Invoke("HideCharacter", 1f);
            //StartCoroutine(WaitShowCharacter(characterStages[0].displayText, 5f));
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}