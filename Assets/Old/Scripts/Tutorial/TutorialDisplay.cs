﻿using System;
using UnityEngine;

namespace Legacy
{
    [Serializable]

    public struct TutorialDisplay
    {
        public string name;
        public bool hasBefore;
        public string beforeText;
        public bool hasImage;
        public Sprite image;
        public bool hasAfter;
        public string afterText;

        public bool counted;
        public double maxValue;
    }
}