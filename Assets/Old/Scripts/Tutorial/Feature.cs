﻿namespace Legacy
{
    public enum Feature
    {
        None,
        TopRight,
        TopLeft,
        BottomBar,
        Skills,
        LeftSide,
        MineButton,
        Any,
        All,
    }
}