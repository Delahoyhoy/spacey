﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Legacy
{
    public class AsteroidMiniGameSceneControl : Singleton<AsteroidMiniGameSceneControl>
    {
        protected AsteroidMiniGameSceneControl()
        {
        }

        // Use this for initialization
        [SerializeField]
        Text currencyEarnedText;


        [SerializeField]
        GameObject tutorialGuy;

        [SerializeField]
        GameObject tutorialText;

        [SerializeField]
        GameObject finishedGameText;

        [SerializeField]
        AsteroidMiniGameAsteroidSpawner asteroidMiniGameAsteroidSpawner;

        public bool readyToGo = false;
        public double moneyEarned = 0;

        void Start()
        {
            tutorialGuy.SetActive(true);
            tutorialText.SetActive(true);
            finishedGameText.SetActive(false);
        }

        public void MakeReadyToGo()
        {
            readyToGo = true;
            tutorialGuy.SetActive(false);
            tutorialText.SetActive(false);
            asteroidMiniGameAsteroidSpawner.SetEverythingGoing();
        }

        public void MakeGameOverHappen()
        {
            tutorialGuy.SetActive(true);
            finishedGameText.SetActive(true);
        }

        public void PressHomeButton()
        {
            DontDestroyDataHolder.Instance.amountEarnedFromAsteroid = moneyEarned;
            SceneManager.LoadScene("SystemScene");
        }

        // Update is called once per frame
        void Update()
        {
            currencyEarnedText.text = CurrencyController.CurrencyToString(moneyEarned);
        }

        public void StartMoneyEarnedThing()
        {
            StartCoroutine(WaitSecThenAddCurrency());
        }

        IEnumerator WaitSecThenAddCurrency()
        {
            yield return new WaitForSeconds(1.4f);
            moneyEarned += DontDestroyDataHolder.Instance.perSecAmount;
        }
    }

}