﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class EveryoneLeaveController : MonoBehaviour
    {
        [SerializeField]
        BasicSystemController[] allSystems;

        [SerializeField]
        BasicShipEngineController mainShip;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Space))
            {
                // MakeShipsGoByeBye();
                GameController.Instance.currencyController.AddCurrency(100000000, CurrencyType.SoftCurrency);
            }

            if (Input.GetKey(KeyCode.V))
            {
                // MakeShipsGoByeBye();
                GameController.Instance.currencyController.AddCurrency(
                    -GameController.Instance.currencyController.GetCurrency(CurrencyType.SoftCurrency), CurrencyType.SoftCurrency);
            }
        }

        public void MakeShipsGoByeBye()
        {
            for (int i = 0; i < allSystems.Length; i++)
            {
                if (allSystems[i] != null)
                {
                    allSystems[i].SetToLeave();
                }
            }

            mainShip.SetToLeave();
        }
    }

}