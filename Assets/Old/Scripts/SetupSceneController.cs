﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Legacy
{
	public class SetupSceneController : MonoBehaviour
	{

		// Use this for initialization
		void Start()
		{
			StartCoroutine(WaitSecThenLoadScene());
		}

		IEnumerator WaitSecThenLoadScene()
		{
			yield return new WaitForSeconds(0.1f);
			SceneManager.LoadScene("SystemScene");
		}

		// Update is called once per frame
		void Update()
		{

		}
	}

}