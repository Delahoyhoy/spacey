﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Legacy
{
    public class GalaxySystemController : MonoBehaviour
    {
        List<SpriteRenderer> allSpriteRenderers;

        [SerializeField]
        Animator uiAnimator;

        // Use this for initialization
        void Start()
        {
            allSpriteRenderers = new List<SpriteRenderer>();
            for (int i = 0; i < this.transform.childCount; i++)
            {
                if (this.transform.GetChild(i).GetComponent<SpriteRenderer>() != null)
                {
                    allSpriteRenderers.Add(transform.GetChild(i).GetComponent<SpriteRenderer>());
                }
            }

            foreach (SpriteRenderer item in allSpriteRenderers)
            {
                StartCoroutine(AlphaUpPlease(item));
            }

            uiAnimator.Play("Maximise");
        }

        IEnumerator AlphaUpPlease(SpriteRenderer whichOneBro)
        {
            float targetAlpha = whichOneBro.color.a;
            whichOneBro.color = new Color(whichOneBro.color.r, whichOneBro.color.g, whichOneBro.color.b, 0);

            while (whichOneBro.color.a < targetAlpha)
            {
                whichOneBro.color = new Color(whichOneBro.color.r, whichOneBro.color.g, whichOneBro.color.b,
                    whichOneBro.color.a + (targetAlpha / 100.0f));
                yield return 0;
            }
        }

        IEnumerator AlphaDownPlease(SpriteRenderer whichOneBro)
        {
            float targetAlpha = 0;
            float originalAlpha = whichOneBro.color.a;
            //whichOneBro.color = new Color(whichOneBro.color.r, whichOneBro.color.g, whichOneBro.color.b, 0);

            while (whichOneBro.color.a > targetAlpha)
            {
                whichOneBro.color = new Color(whichOneBro.color.r, whichOneBro.color.g, whichOneBro.color.b,
                    whichOneBro.color.a - (originalAlpha / 200.0f));
                yield return 0;
            }
        }

        public void ClosingSystems()
        {
            uiAnimator.Play("Minimise");
            foreach (SpriteRenderer item in allSpriteRenderers)
            {
                StartCoroutine(AlphaDownPlease(item));
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}