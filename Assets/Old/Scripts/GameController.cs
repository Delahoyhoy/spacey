﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
//TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
//using Facebook.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Legacy
{
    public class GameController : Singleton<GameController>
    {
        protected GameController()
        {
        }

        public CurrencyController currencyController;
        public GameObject upgradePopup;
        public Transform mainUICanvas;
        public GameObject VIPPopup;
        public BoostsController boostsController;
        public BasicAdController basicAdController;
        public GameObject advertPopup;
        public TutorialController tutorialController;
        public SaveLoadDataController saveLoadDataController;
        public BasicSystemController[] allSystems;
        public GameObject returnToGamePopup;
        public Camera mainCameraInScene;
        public Animator mainUIAnimator;
        public TasksController tasksController;
        public GameObject tasksPopup;
        public GameObject testfullscreenPopup;
        public ResearchController researchController;
        public GameObject optionsPopup;
        public GameObject newShipPopup;
        public GameObject chestTest;
        public GameObject shipyardPopup;
        public AllShipCardsController allShipCardsController;



        public bool IsPopupOpenNow = false;





        private void Start()
        {
            allShipCardsController.allShipCardsCurrentlyHeld = new int[allSystems.Length];

            if (DontDestroyDataHolder.Instance.overrideCamera)
            {
                mainCameraInScene.enabled = false;
                mainUIAnimator.gameObject.SetActive(false);
                StartCoroutine(CheckEverySoOftenIfCameraShouldBeOn());
            }
            else
            {
                MoveUIIn();
            }

            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            //if (FB.IsInitialized)
            //{
            //    FB.ActivateApp();
            //}
            //else
            //{
            //Handle FB.Init
            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            //FB.Init(() => {
            //    FB.ActivateApp();
            //});
            //  }
            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            //BaseTenjin instance = Tenjin.getInstance("E7SU2GYZMENBNX7FGUYRRS9NKWAARTF6");
            // instance.Connect();
        }

        bool TESTBOOLONLYONCE = false;

        IEnumerator CheckEverySoOftenIfCameraShouldBeOn()
        {
            yield return 0;
            yield return 0;
            yield return 0;
            yield return 0;
            yield return 0;
            yield return 0;
            if (DontDestroyDataHolder.Instance.overrideCamera)
            {
                StartCoroutine(CheckEverySoOftenIfCameraShouldBeOn());
            }
            else
            {
                mainCameraInScene.orthographicSize = DontDestroyDataHolder.Instance.currentCameraZoomToMimick;
                mainCameraInScene.transform.position = DontDestroyDataHolder.Instance.cameraPositionToMimick;

                mainCameraInScene.enabled = true;
                mainUIAnimator.gameObject.SetActive(true);
                MoveUIIn();
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                //do nothing
            }
            else
            {
                //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
                //BaseTenjin instance = Tenjin.getInstance("E7SU2GYZMENBNX7FGUYRRS9NKWAARTF6");
                //instance.Connect();
            }
        }

        public void SpawnNewShipPopup(BasicSystemController bsc)
        {
            for (int i = 0; i < allSystems.Length; i++)
            {
                if (allSystems[i] == bsc)
                {
                    GameObject temp = Instantiate(newShipPopup, mainUICanvas);
                    temp.GetComponent<NewShipUnlockedPopup>().SetupShipSprites(i);
                }
            }
        }

        public void SpawnShipyardPopup()
        {
            if (!IsPopupOpenNow)
            {
                GameObject temp = Instantiate(shipyardPopup, mainUICanvas);
                IsPopupOpenNow = true;
            }
        }

        public double GetAllPerSecAmounts()
        {
            double returnAmount = 0;
            for (int i = 0; i < allSystems.Length; i++)
            {
                returnAmount += allSystems[i].perSecAmount;
            }

            return returnAmount;
        }

        public double GetTotalEarningsFromMiniGame()
        {
            return GetEarningsPerSecFromMiniGame() * 40.0d;
        }

        public double GetEarningsPerSecFromMiniGame()
        {
            return (GetAllPerSecAmounts() * 20.0d);
        }

        public void TesttestfullscreenPopup()
        {
            if (!IsPopupOpenNow)
            {
                GameObject temp = Instantiate(testfullscreenPopup, mainUICanvas);
                IsPopupOpenNow = true;
            }
        }

        public void OpenTasksPopup()
        {
            if (!IsPopupOpenNow)
            {
                GameObject temp = Instantiate(tasksPopup, mainUICanvas);
                IsPopupOpenNow = true;
            }
        }

        public void OpenOptionsPopup()
        {
            if (!IsPopupOpenNow)
            {
                GameObject temp = Instantiate(optionsPopup, mainUICanvas);
                IsPopupOpenNow = true;
            }
        }

        public void SetupUpgradePopup(BasicSystemController systemCon)
        {
            if (!IsPopupOpenNow)
            {
                GameObject temp = Instantiate(upgradePopup, mainUICanvas);

                temp.GetComponent<UpgradePopupController>().SetupPopup(systemCon);
                IsPopupOpenNow = true;
            }
        }

        public void OpenVIPPopup()
        {
            if (!IsPopupOpenNow)
            {
                GameObject temp = Instantiate(VIPPopup, mainUICanvas);
                IsPopupOpenNow = true;
            }
        }

        public void OpenAdPopup()
        {
            if (!IsPopupOpenNow)
            {
                basicAdController.TurnOffUFO();
                GameObject temp = Instantiate(advertPopup, mainUICanvas);
                temp.GetComponent<AdWatchPopupController>().isx2MultiplierAd = true;
                temp.GetComponent<AdWatchPopupController>().Setup();
                IsPopupOpenNow = true;
            }
        }

        public void OpenMinigamePopup()
        {
            if (!IsPopupOpenNow)
            {
                basicAdController.TurnOffAsteroidFound();
                GameObject temp = Instantiate(advertPopup, mainUICanvas);
                temp.GetComponent<AdWatchPopupController>().isx2MultiplierAd = false;
                temp.GetComponent<AdWatchPopupController>().Setup(GetTotalEarningsFromMiniGame());
                IsPopupOpenNow = true;
            }
        }



        public void OpenReturnToGamePopup(double howMuch)
        {
            GameObject temp = Instantiate(returnToGamePopup, mainUICanvas);
            temp.GetComponent<ReturnToGamePopup>().SetupEarnedPopup(howMuch);
        }

        public Transform coinBlastStartPos;

        public void Update()
        {
            //if (spaceBackground)
            //{
            //    MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();
            //    materialPropertyBlock.SetVector("_Position", Camera.main.transform.position);
            //    spaceBackground.SetPropertyBlock(materialPropertyBlock);
            //}
            if (Input.GetKeyDown(KeyCode.P))
            {
                //StartZoomToGalaxy();
                //SetupMiniGameStuffAndGo();
                TESTChestScene();
            }

            if (Input.touchCount == 4)
            {
                if (!TESTBOOLONLYONCE)
                {
                    TESTBOOLONLYONCE = true;
                    //TODO: Prerelease
                    //currencyController.AddCurrency(1000000000, CurrencyType.Gold);
                    //StartZoomToGalaxy();
                    //SetupMiniGameStuffAndGo();
                    TESTChestScene();
                }
            }
        }

        void TESTChestScene()
        {
            saveLoadDataController.Save();
            // DontDestroyDataHolder.Instance.chestTypesToOpen = new List<ChestData.ChestType>();
            // DontDestroyDataHolder.Instance.chestTypesToOpen.Add(ChestData.ChestType.Normal);
            // DontDestroyDataHolder.Instance.chestTypesToOpen.Add(ChestData.ChestType.Legendary);
            // DontDestroyDataHolder.Instance.chestTypesToOpen.Add(ChestData.ChestType.Epic);
            // DontDestroyDataHolder.Instance.chestTypesToOpen.Add(ChestData.ChestType.Rare);
            SceneManager.LoadSceneAsync("ChestScene");
        }

        public void LoadFinished()
        {
            UpdateAllLevel2Systems();
            if (DontDestroyDataHolder.Instance.hasHadNewCardsAdded)
            {
                allShipCardsController.AddMoreShipCards();
                DontDestroyDataHolder.Instance.hasHadNewCardsAdded = false;
            }
        }

        public void SetupMiniGameStuffAndGo()
        {
            DontDestroyDataHolder.Instance.perSecAmount = GetEarningsPerSecFromMiniGame();
            saveLoadDataController.Save();
            StartCoroutine(WaitTinySecThenGo());
        }

        IEnumerator WaitTinySecThenGo()
        {
            yield return 0;
            yield return 0;
            yield return 0;
            SceneManager.LoadScene("AsteroidFieldScene");
        }

        [SerializeField]
        Transform coinExplosionTarget;

        [SerializeField]
        Transform gemExplosionTarget;

        public void PlayRegularCoinBlast()
        {
            CurrencyExplosion.Instance.target = coinExplosionTarget;
            CurrencyExplosion.Instance.CreateNewBlast(coinBlastStartPos.transform.position);
        }

        public void PlayGemsCoinBlast(Transform whereFrom)
        {
            CurrencyExplosion.Instance.target = gemExplosionTarget;
            CurrencyExplosion.Instance.CreateNewBlast(whereFrom.position, false);
        }

        public void MoveUIIn()
        {
            mainUIAnimator.Play("Maximise");
        }

        public void MoveUIOut()
        {
            mainUIAnimator.Play("Minimise");
        }

        bool onlyZoomToGalaxyOnce = false;

        public void StartZoomToGalaxy()
        {
            if (!onlyZoomToGalaxyOnce)
            {
                onlyZoomToGalaxyOnce = true;
                saveLoadDataController.Save();
                DontDestroyDataHolder.Instance.cameraPositionToMimick = mainCameraInScene.transform.position;
                DontDestroyDataHolder.Instance.currentCameraZoomToMimick = mainCameraInScene.orthographicSize;
                MoveUIOut();
                SceneManager.LoadSceneAsync("GalaxyScene", LoadSceneMode.Additive);
                StartCoroutine(turnOffCam());
            }
        }

        public void UpdateAllLevel2Systems()
        {
            for (int i = 0; i < allSystems.Length; i++)
            {
                allSystems[i].SetupUsability();
            }
        }

        IEnumerator turnOffCam()
        {
            yield return new WaitForSeconds(0.3f);
            mainCameraInScene.gameObject.SetActive(false);
        }

        private void LateUpdate()
        {

        }
    }
}