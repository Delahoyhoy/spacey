﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

namespace Legacy
{
    public class AdWatchPopupController : MonoBehaviour
    {
        [SerializeField]
        Animator theAnimator;

        [SerializeField]
        GameObject x2TextStuff;

        [SerializeField]
        GameObject minigameStuff;

        [SerializeField]
        Text amountEarnedFromMiniGame;

        public bool isx2MultiplierAd = true;

        // Use this for initialization
        void Start()
        {
            theAnimator.Play("Maximise");
        }

        public void Setup(double amountEarned = 0)
        {
            if (isx2MultiplierAd)
            {
                x2TextStuff.SetActive(true);
                minigameStuff.SetActive(false);
            }
            else
            {
                x2TextStuff.SetActive(false);
                minigameStuff.SetActive(true);
                amountEarnedFromMiniGame.text = CurrencyController.CurrencyToString(amountEarned);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void DismissPopup()
        {
            GameController.Instance.IsPopupOpenNow = false;
            theAnimator.Play("Minimise");
            StartCoroutine(WaitThenDestroyThisBud());
        }

        //public void PressWatch(){
        //    if (Advertisement.IsReady())
        //    {
        //        var options = new ShowOptions { resultCallback = HandleShowResult };
        //        Advertisement.Show(options);
        //    }
        //}

        //public void HandleShowResult(ShowResult result){
        //    switch (result)
        //    {
        //        case ShowResult.Finished:
        //            if (isx2MultiplierAd)
        //            {
        //                GameController.Instance.basicAdController.SetAdToOn();
        //                GameController.Instance.boostsController.AddVIPPoints(20);
        //            } else {
        //                GameController.Instance.basicAdController.TurnOffAsteroidFound();
        //                GameController.Instance.boostsController.AddVIPPoints(20);
        //                GameController.Instance.SetupMiniGameStuffAndGo();
        //            }
        //            DismissPopup();
        //            break;
        //        case ShowResult.Skipped:
        //            break;
        //        case ShowResult.Failed:
        //            break;
        //    }
        //}

        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }
    }

}