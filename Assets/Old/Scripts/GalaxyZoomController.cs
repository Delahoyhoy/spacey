﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using Core.Gameplay.CameraControl;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Legacy
{
    public class GalaxyZoomController : MonoBehaviour
    {
        [SerializeField]
        BasicCameraController basicCameraController;

        [SerializeField]
        GalaxySystemController system1;

        [SerializeField]
        Animator cameraAnimator;

        bool cameraAllTheWayBack = false;

        // Use this for initialization
        void Start()
        {
            basicCameraController.gameObject.transform.position = DontDestroyDataHolder.Instance.cameraPositionToMimick;
            basicCameraController.mainCamera.orthographicSize =
                DontDestroyDataHolder.Instance.currentCameraZoomToMimick;
            StartCoroutine(ZoomCameraBack());
        }

        IEnumerator ZoomCameraBack()
        {
            cameraAllTheWayBack = false;
            float currentCamOrtho = basicCameraController.mainCamera.orthographicSize;
            float time = 0f;
            while (basicCameraController.mainCamera.orthographicSize < 299.5f)
            {
                time += Time.deltaTime / 3.0f;
                basicCameraController.mainCamera.orthographicSize = Mathf.SmoothStep(currentCamOrtho, 300, time);
                //basicCameraController.myCamera.orthographicSize += basicCameraController.cameraZoomSpeed;
                yield return 0;
            }

            SceneManager.UnloadSceneAsync("SystemScene");
            cameraAllTheWayBack = true;
        }

        IEnumerator ZoomCameraIn()
        {
            StopCoroutine(ZoomCameraBack());
            float currentCamOrtho = basicCameraController.mainCamera.orthographicSize;
            float time = 0f;
            while (basicCameraController.mainCamera.orthographicSize > 5.46f)
            {
                time += Time.deltaTime / 3.0f;
                basicCameraController.mainCamera.orthographicSize = Mathf.SmoothStep(currentCamOrtho, 5.45f, time);
                //basicCameraController.myCamera.orthographicSize -= basicCameraController.cameraZoomSpeed;
                yield return 0;
            }

            DontDestroyDataHolder.Instance.cameraPositionToMimick = basicCameraController.gameObject.transform.position;
            DontDestroyDataHolder.Instance.currentCameraZoomToMimick =
                basicCameraController.mainCamera.orthographicSize;
            DontDestroyDataHolder.Instance.overrideCamera = false;
            SceneManager.UnloadSceneAsync("GalaxyScene");
        }

        IEnumerator GetCameraIntoPosition(Vector3 toGetTo)
        {
            float time = 0f;
            while (!IsNearEnough(basicCameraController.mainCamera.transform.position.x, toGetTo.x) ||
                   !IsNearEnough(basicCameraController.mainCamera.transform.position.y, toGetTo.y))
            {
                time += Time.deltaTime / 10.0f;
                float xValue = Mathf.SmoothStep(basicCameraController.mainCamera.transform.position.x, toGetTo.x, time);
                float yValue = Mathf.SmoothStep(basicCameraController.mainCamera.transform.position.y, toGetTo.y, time);
                basicCameraController.mainCamera.transform.position = new Vector3(xValue, yValue,
                    basicCameraController.mainCamera.transform.position.z);
                /*if(!IsNearEnough(basicCameraController.myCamera.transform.position.x, toGetTo.x)){
                if(basicCameraController.myCamera.transform.position.x < toGetTo.x){
                    basicCameraController.myCamera.transform.position = new Vector3(
                        basicCameraController.myCamera.transform.position.x + 1f,
                        basicCameraController.myCamera.transform.position.y,
                        basicCameraController.myCamera.transform.position.z);
                } else if (basicCameraController.myCamera.transform.position.x > toGetTo.x){
                    basicCameraController.myCamera.transform.position = new Vector3(
                        basicCameraController.myCamera.transform.position.x - 1f,
                        basicCameraController.myCamera.transform.position.y,
                        basicCameraController.myCamera.transform.position.z);
                }
            }
            if (!IsNearEnough(basicCameraController.myCamera.transform.position.y, toGetTo.y))
            {
                if (basicCameraController.myCamera.transform.position.y < toGetTo.y)
                {
                    basicCameraController.myCamera.transform.position = new Vector3(
                        basicCameraController.myCamera.transform.position.x,
                        basicCameraController.myCamera.transform.position.y + 1f,
                        basicCameraController.myCamera.transform.position.z);
                }
                else if (basicCameraController.myCamera.transform.position.y > toGetTo.y)
                {
                    basicCameraController.myCamera.transform.position = new Vector3(
                        basicCameraController.myCamera.transform.position.x,
                        basicCameraController.myCamera.transform.position.y - 1f,
                        basicCameraController.myCamera.transform.position.z);
                }
            }*/

                yield return 0;
            }
        }

        private bool IsNearEnough(float whatNumber, float nearThisOne)
        {
            if (whatNumber - 0.5f < nearThisOne && whatNumber + 0.5f > nearThisOne)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void PressOnGalaxy1()
        {
            if (cameraAllTheWayBack && !onlyPressThisOnceEver)
            {
                onlyPressThisOnceEver = true;
                DontDestroyDataHolder.Instance.overrideCamera = true;
                SceneManager.LoadSceneAsync("SystemScene", LoadSceneMode.Additive);
                system1.ClosingSystems();
                StartCoroutine(ZoomCameraIn());
                StartCoroutine(GetCameraIntoPosition(new Vector3(3.36f, -3.05f, -10)));
            }
        }

        bool onlyPressThisOnceEver = false;

        // Update is called once per frame
        void Update()
        {

        }
    }

}