﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Facebook.Unity;

namespace Legacy
{
    public class DontDestroyDataHolder : Singleton<DontDestroyDataHolder>
    {
        protected DontDestroyDataHolder()
        {
        }

        public Vector3 cameraPositionToMimick;
        public float currentCameraZoomToMimick;

        public double perSecAmount = 10;

        public double amountEarnedFromAsteroid = 0;

        [SerializeField]
        AudioSource theSauce;

        public bool muteAudio = false;
        public bool quickBuy = false;

        public bool overrideCamera = false;

        public int[] allChestOpenedItemsInts;

        public bool hasHadNewCardsAdded = false;

        public bool GetIsAround()
        {
            return true;
        }

        // Use this for initialization
        void Start()
        {
            DontDestroyOnLoad(this.gameObject);
            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            //BaseTenjin instance = Tenjin.getInstance("E7SU2GYZMENBNX7FGUYRRS9NKWAARTF6");
            //instance.Connect();
        }

        public void ToggleAudio()
        {
            //muteAudio = !muteAudio;
            //if (theSauce != null)
            //{
            //    theSauce.mute = muteAudio;
            //}
        }

        public void ToggleQuickBuy()
        {
            //quickBuy = !quickBuy;
        }

        public void AudioLoaded()
        {
            //if (theSauce != null)
            //{
            //    theSauce.mute = muteAudio;
            //}
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}