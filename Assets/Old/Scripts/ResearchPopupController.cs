﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class ResearchPopupController : MonoBehaviour
    {
        [SerializeField]
        Animator theAnimator;

        [SerializeField]
        SingleResearchItemInPopup[] allSingleResearchItemsOnScreen;

        [SerializeField]
        SingleResearchItemInPopup selectedResearchItem;

        [SerializeField]
        Text currentActiveResearchText;

        [SerializeField]
        Text currentActiveResearchTimer;

        [SerializeField]
        GameObject topInformationExtras;

        [SerializeField]
        Text hardCurrencySpeedupCost;

        // Use this for initialization
        void Start()
        {
            theAnimator.Play("Maximise");
            SetupScreen();
        }

        void SetupScreen()
        {
            for (int i = 0; i < allSingleResearchItemsOnScreen.Length; i++)
            {
                allSingleResearchItemsOnScreen[i]
                    .SetupResItem(
                        GameController.Instance.researchController.GetItemFromID(allSingleResearchItemsOnScreen[i]
                            .researchIdHere));
            }

            selectedResearchItem.SetupToBeBlank();



            if (GameController.Instance.researchController.isActivelyResearching)
            {
                topInformationExtras.SetActive(true);
                currentActiveResearchText.text = GameController.Instance.researchController.currentActiveResearch
                    .researchDescription;
            }
            else
            {
                topInformationExtras.SetActive(false);
                currentActiveResearchText.text = "None Selected!";
            }
        }

        public void PressSpeedUp()
        {
            if (GameController.Instance.researchController.isActivelyResearching)
            {
                if (GameController.Instance.researchController.DiamondCostToSpeedUp() <=
                    GameController.Instance.currencyController.GetCurrency(CurrencyType.HardCurrency))
                {
                    GameController.Instance.currencyController.AddCurrency(
                        -GameController.Instance.researchController.DiamondCostToSpeedUp(), CurrencyType.HardCurrency);
                    GameController.Instance.researchController.FinishResearchNow();
                    SetupScreen();
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (GameController.Instance.researchController.isActivelyResearching)
            {
                WorkOutResearchSpeedUpCost();
                currentActiveResearchTimer.gameObject.SetActive(true);
                currentActiveResearchTimer.text = GameController.Instance.researchController.researchTimerString;
            }
            else
            {
                currentActiveResearchTimer.gameObject.SetActive(false);
            }
        }

        void WorkOutResearchSpeedUpCost()
        {
            if (GameController.Instance.researchController.DiamondCostToSpeedUp() > 0)
            {
                hardCurrencySpeedupCost.text =
                    GameController.Instance.researchController.DiamondCostToSpeedUp().ToString();
            }
            else
            {
                hardCurrencySpeedupCost.text = "FREE!";
            }
        }

        public void DismissPopup()
        {
            GameController.Instance.IsPopupOpenNow = false;
            theAnimator.Play("Minimise");
            StartCoroutine(WaitThenDestroyThisBud());
        }

        public void PressOnAResearchItem(SingleResearchItemInPopup selectedOne)
        {
            selectedResearchItem.SetupResItem(selectedOne.singleResearchItem);
        }

        public void PressStartResearchButton(SingleResearchItemInPopup _sayWut)
        {
            if (GameController.Instance.currencyController.GetCurrency(CurrencyType.SoftCurrency) >=
                _sayWut.singleResearchItem.cost)
            {
                GameController.Instance.currencyController.AddCurrency(-_sayWut.singleResearchItem.cost,
                    CurrencyType.SoftCurrency);
                GameController.Instance.researchController.SetResearchToStart(_sayWut.researchIdHere);
                SetupScreen();
            }
        }

        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }

    }

}