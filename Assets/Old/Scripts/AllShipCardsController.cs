﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using UnityEngine;

namespace Legacy
{
	public class AllShipCardsController : MonoBehaviour
	{
		public int[] allShipCardsCurrentlyHeld;

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		public void AddMoreShipCards()
		{
			for (int i = 0; i < allShipCardsCurrentlyHeld.Length; i++)
			{
				allShipCardsCurrentlyHeld[i] += DontDestroyDataHolder.Instance.allChestOpenedItemsInts[i];
				Debug.Log("Ship " + i.ToString() + " has " + allShipCardsCurrentlyHeld[i].ToString());
			}
		}
	}
}
