﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
	public class AsteroidMiniGameShot : MonoBehaviour
	{
		[SerializeField]
		float moveSpeed = 0.01f;

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			transform.Translate(0, moveSpeed, 0, Space.Self);
		}

		private void OnTriggerEnter(Collider other)
		{
			CurrencyExplosion.Instance.CreateNewBlast(Camera.main.WorldToScreenPoint(this.transform.position));
			AsteroidMiniGameSceneControl.Instance.StartMoneyEarnedThing();
			Destroy(other.gameObject);
			Destroy(this.gameObject);
		}
	}

}