﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class VIPPopupController : MonoBehaviour
    {
        [SerializeField]
        Animator theAnimator;

        [SerializeField]
        Text currentVIPLevelText;

        [SerializeField]
        Text currentVIPProgressText;

        [SerializeField]
        Text unlocksText;

        [SerializeField]
        Image fillbar;

        [SerializeField]
        GameObject upgradeButton;

        [SerializeField]
        Text extraOfflineText;

        [SerializeField]
        Text extraAdText;

        [SerializeField]
        Text extraDiamondText;

        [SerializeField]
        Text extraAdMultText;

        [SerializeField]
        Text extraGoldText;

        // Use this for initialization
        void Start()
        {
            theAnimator.Play("Maximise");
            ResetScreenText();
            GameController.Instance.boostsController.notificationIcon.SetActive(false);
        }

        void ResetScreenText()
        {
            currentVIPLevelText.text = GameController.Instance.boostsController.currentVIPLevel.ToString();
            currentVIPProgressText.text = GameController.Instance.boostsController.currentVIPExperience.ToString() +
                                          "/" +
                                          GameController.Instance.boostsController.VIPExperienceToNextLevel.ToString();
            fillbar.fillAmount = GameController.Instance.boostsController.GetVIPUpgradeProgress();
            upgradeButton.SetActive(GameController.Instance.boostsController.IsVIPUpgradeAvailable());
            unlocksText.text = GameController.Instance.boostsController.nextBoostUnlocksText;
            extraOfflineText.text = GameController.Instance.boostsController.extraOfflineHoursText;
            extraAdText.text = GameController.Instance.boostsController.extraAdMinuteText;
            extraDiamondText.text = GameController.Instance.boostsController.extraDiamondsMultiplierText;
            extraAdMultText.text = GameController.Instance.boostsController.extraAdMultipliersText;
            extraGoldText.text = GameController.Instance.boostsController.extraGoldMultipliersText;
        }

        public void PressUpgradeButton()
        {
            GameController.Instance.boostsController.UpgradeVIPLevel();
            ResetScreenText();
        }

        public void DismissPopup()
        {
            GameController.Instance.IsPopupOpenNow = false;
            theAnimator.Play("Minimise");
            StartCoroutine(WaitThenDestroyThisBud());
        }

        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }

        private void Update()
        {
        }
    }

}