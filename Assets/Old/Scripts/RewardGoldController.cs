﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class RewardGoldController : MonoBehaviour
    {
        [SerializeField]
        Text rewardText;

        public void Setup(double rewardAmount)
        {
            rewardText.text = CurrencyController.CurrencyToString(rewardAmount);
            this.gameObject.SetActive(true);
        }

        public void DestroySelf()
        {
            Destroy(this.gameObject);
        }
    }

}