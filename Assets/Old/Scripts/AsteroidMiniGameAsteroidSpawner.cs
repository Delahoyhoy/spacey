﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class AsteroidMiniGameAsteroidSpawner : MonoBehaviour
    {
        [SerializeField]
        GameObject[] allAsteroidObjects;

        [SerializeField]
        GameObject[] allLeftTransforms;

        [SerializeField]
        GameObject[] allRightTransforms;

        [SerializeField]
        float moveSpeed = 1.0f;

        [SerializeField]
        Image fillbar;

        // Use this for initialization
        void Start()
        {
        }

        bool isOverNow = false;
        float totalTimer = 30;

        public void SetEverythingGoing()
        {
            StartCoroutine(SpawnAsteroids());
            StartCoroutine(TimerThing());
        }

        IEnumerator TimerThing()
        {
            while (totalTimer > 0)
            {
                totalTimer -= Time.deltaTime;
                fillbar.fillAmount = totalTimer / 30;
                yield return 0;
            }

            isOverNow = true;
            AsteroidMiniGameSceneControl.Instance.MakeGameOverHappen();
        }

        IEnumerator SpawnAsteroids()
        {
            yield return new WaitForSecondsRealtime(0.75f);
            if (!isOverNow)
            {
                GameObject temp = Instantiate(allAsteroidObjects[0]);
                temp.SetActive(true);
                int rando = Random.Range(0, 10);
                if (rando < 5)
                {
                    temp.GetComponent<AsteroidMiniGameAsteroid>().Setup(
                        allLeftTransforms[Random.Range(0, allLeftTransforms.Length)].transform.position,
                        allRightTransforms[Random.Range(0, allRightTransforms.Length)].transform.position,
                        moveSpeed);
                }
                else
                {
                    temp.GetComponent<AsteroidMiniGameAsteroid>().Setup(
                        allRightTransforms[Random.Range(0, allRightTransforms.Length)].transform.position,
                        allLeftTransforms[Random.Range(0, allLeftTransforms.Length)].transform.position,
                        moveSpeed);
                }

                StartCoroutine(SpawnAsteroids());
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}