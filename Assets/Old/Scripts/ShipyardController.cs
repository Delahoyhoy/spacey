﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class ShipyardController : MonoBehaviour
    {
        [SerializeField]
        Animator theAnimator;

        [SerializeField]
        ShipCardPopulator[] shipCardPopulators;

        // Use this for initialization
        void Start()
        {
            theAnimator.Play("Maximise");
            for (int i = 0; i < GameController.Instance.allShipCardsController.allShipCardsCurrentlyHeld.Length; i++)
            {
                shipCardPopulators[i].shipCardLevel.text = "Lvl 0";
                shipCardPopulators[i].shipCardAmountHeld.text = GameController.Instance.allShipCardsController
                    .allShipCardsCurrentlyHeld[i].ToString();
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void DismissPopup()
        {
            theAnimator.Play("Minimise");
            StartCoroutine(WaitThenDestroyThisBud());
        }

        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }
    }

}