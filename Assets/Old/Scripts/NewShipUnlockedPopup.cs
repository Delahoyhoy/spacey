﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
	public class NewShipUnlockedPopup : MonoBehaviour
	{
		[SerializeField]
		GameObject[] allShipSprites;

		[SerializeField]
		Animator theAnimator;


		public void SetupShipSprites(int whichOne)
		{
			allShipSprites[whichOne].SetActive(true);
		}

		// Update is called once per frame
		void Update()
		{

		}

		public void Dismiss()
		{
			theAnimator.StopPlayback();
			Destroy(this.gameObject);
		}
	}

}