﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class StarsController : MonoBehaviour
    {
        public float alphaSpeed = 0.01f;
        float currentAlpha = 1.0f;

        void Start()
        {

            float startScale = this.gameObject.transform.localScale.x;
            float rando = Random.Range(startScale / 1.5f, startScale * 1.1f);
            this.gameObject.transform.localScale = new Vector3(rando, rando, rando);
            StartCoroutine(WaitSecThenSeeIfTwinkling());
        }

        void Update()
        {

        }

        IEnumerator WaitSecThenSeeIfTwinkling()
        {
            yield return new WaitForSecondsRealtime(Random.Range(3, 6));
            float rando = Random.Range(0.0f, 100.0f);
            if (rando < 20.0f)
            {
                StartCoroutine(TwinkleTwinkle());
            }
            else
            {
                StartCoroutine(WaitSecThenSeeIfTwinkling());
            }
        }

        IEnumerator TwinkleTwinkle()
        {
            bool onWayUp = false;
            bool completed = false;
            while (!completed)
            {
                if (!onWayUp)
                {
                    this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, currentAlpha);
                    currentAlpha -= alphaSpeed;
                    if (currentAlpha < 0)
                    {
                        onWayUp = true;
                    }
                }
                else
                {
                    this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, currentAlpha);
                    currentAlpha += alphaSpeed;
                    if (currentAlpha > 1)
                    {
                        completed = true;
                    }
                }

                yield return 0;
            }

            StartCoroutine(WaitSecThenSeeIfTwinkling());
        }
    }

}