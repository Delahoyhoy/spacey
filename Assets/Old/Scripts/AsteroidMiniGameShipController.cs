﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
    public class AsteroidMiniGameShipController : MonoBehaviour
    {
        [SerializeField]
        GameObject shotToFire;

        // Use this for initialization
        void Start()
        {

        }

        bool wasTouchDown = false;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0) && !wasTouchDown)
            {
                if (AsteroidMiniGameSceneControl.Instance.readyToGo)
                {
                    Fire();
                }
                else
                {
                    AsteroidMiniGameSceneControl.Instance.MakeReadyToGo();
                }
            }

            wasTouchDown = Input.GetMouseButtonDown(0);
        }

        void Fire()
        {
            Vector3 bleh = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Debug.Log("Firing at: " + getAngle(new Vector2(0, 0), new Vector2(bleh.x, bleh.y)));
            Debug.Log("Mouse Pos: " + Input.mousePosition);

            this.transform.eulerAngles = new Vector3(0, 0,
                (float) getAngle(new Vector2(this.transform.position.x, this.transform.position.y),
                    new Vector2(bleh.x, bleh.y)) + 270);

            GameObject temp = Instantiate(shotToFire);
            temp.transform.position = this.transform.position;
            temp.transform.localEulerAngles = this.transform.localEulerAngles;
        }

        public double getAngle(Vector2 me, Vector2 target)
        {
            return Math.Atan2(target.y - me.y, target.x - me.x) * (180 / Math.PI);
        }
    }

}