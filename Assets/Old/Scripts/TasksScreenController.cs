﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class TasksScreenController : MonoBehaviour
    {
        [SerializeField]
        Animator theAnimator;

        [SerializeField]
        GameObject[] taskXGameObject;

        [SerializeField]
        Text[] taskXTitle;

        [SerializeField]
        Text[] taskXProgressText;

        [SerializeField]
        Image[] taskXFillbar;

        [SerializeField]
        Text[] taskXRewardAmount;

        [SerializeField]
        Button[] taskXButton;

        [SerializeField]
        GameObject moreTasksComingSoonObject;


        List<TasksController.SingleTask> eachTask = new List<TasksController.SingleTask>();

        // Use this for initialization
        void Start()
        {
            theAnimator.Play("Maximise");
            GetTasksAndSortScreen();
            GameController.Instance.tasksController.notificationIcon.SetActive(false);
        }

        void GetTasksAndSortScreen()
        {
            eachTask = GameController.Instance.tasksController.GetActiveTasks();
            int turnedOff = 0;
            for (int i = 0; i < 3; i++)
            {
                if (i <= eachTask.Count - 1)
                {
                    taskXGameObject[i].SetActive(true);
                    taskXTitle[i].text = eachTask[i].taskName;
                    taskXProgressText[i].text = eachTask[i].GetProgress();
                    taskXFillbar[i].fillAmount = (float) eachTask[i].GetPercentage();
                    taskXRewardAmount[i].text = CurrencyController.CurrencyToString(eachTask[i].rewardAmount);
                    taskXButton[i].interactable = eachTask[i].IsReadyToCollect();
                    if (eachTask[i].isCompleted)
                    {
                        taskXGameObject[i].SetActive(false);
                        turnedOff++;
                    }
                }
                else
                {
                    taskXGameObject[i].SetActive(false);
                    turnedOff++;
                }
            }

            if (turnedOff > 2)
            {
                moreTasksComingSoonObject.SetActive(true);
            }
        }

        public void PressCompleteButton(int whichButton)
        {
            GameController.Instance.tasksController.CompleteTaskPressed(eachTask[whichButton].taskShortID,
                taskXButton[whichButton].transform);
            GetTasksAndSortScreen();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void DismissPopup()
        {
            GameController.Instance.IsPopupOpenNow = false;
            theAnimator.Play("Minimise");
            StartCoroutine(WaitThenDestroyThisBud());
        }

        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }
    }

}