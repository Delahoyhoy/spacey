﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class SingleResearchItemInPopup : MonoBehaviour
    {
        [SerializeField]
        public Text shortDescr;

        [SerializeField]
        public GameObject lockObject;

        public string researchIdHere;

        [SerializeField]
        GameObject unlockedIndicator;

        [SerializeField]
        Text costAmount;

        [SerializeField]
        Text timerIndicator;

        [SerializeField]
        Text longDescription;

        [SerializeField]
        Button startResearchButton;

        public ResearchController.SingleResearchItem singleResearchItem;


        public void SetupResItem(ResearchController.SingleResearchItem eyeTem)
        {
            singleResearchItem = eyeTem;

            if (shortDescr != null)
            {
                shortDescr.text = eyeTem.shortDescription;
                if (singleResearchItem.isOwned)
                {
                    unlockedIndicator.SetActive(true);
                }
                else
                {
                    unlockedIndicator.SetActive(false);
                }
            }

            costAmount.text = CurrencyController.CurrencyToString(singleResearchItem.cost);

            if (timerIndicator != null)
            {

                timerIndicator.gameObject.SetActive(true);
                startResearchButton.gameObject.SetActive(true);
                researchIdHere = singleResearchItem.researchID;
                timerIndicator.text = GetTimerString(singleResearchItem.totalMinutes);
                longDescription.text = singleResearchItem.researchDescription;
                startResearchButton.interactable = !GameController.Instance.researchController.isActivelyResearching;

                if (startResearchButton.interactable)
                {
                    if (GameController.Instance.currencyController.GetCurrency(CurrencyType.SoftCurrency) >=
                        singleResearchItem.cost)
                    {
                        startResearchButton.interactable = true;
                    }
                    else
                    {
                        startResearchButton.interactable = false;
                    }
                }
            }
        }

        public string GetTimerString(float totalMinutes)
        {
            float hours = 0;
            if (totalMinutes > 60)
            {
                hours = Mathf.Floor(totalMinutes / 60.0f);
                totalMinutes = totalMinutes - (hours * 60.0f);
            }

            if (hours > 0)
            {
                return "Research time: " + hours + "h " + totalMinutes.ToString() + "m";
            }
            else
            {
                return "Research time: " + totalMinutes.ToString() + "m";
            }
        }

        public void SetupToBeBlank()
        {
            timerIndicator.gameObject.SetActive(false);
            startResearchButton.gameObject.SetActive(false);
            longDescription.text = "Select research above to view more info";
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}