﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class ResearchController : MonoBehaviour
    {

        public class SingleResearchItem
        {
            public bool isOwned = false;
            public double cost;
            public float totalMinutes;
            public string researchID;
            public string researchDescription;
            public string shortDescription;
            public ResearchType researchType;

            public enum ResearchType
            {
                Engines,
                Offline,
                Scan,
                Gold,
                Soon
            }
        }

        public SingleResearchItem[] allResearchItems;

        public Text timerLabel;

        public SingleResearchItem currentActiveResearch;
        TimeSpan researchTime;
        public DateTime currentResearchTimer;

        public bool isActivelyResearching;

        public void SetResearchToStart(string idOfResearch)
        {
            if (!isActivelyResearching)
            {
                for (int i = 0; i < allResearchItems.Length; i++)
                {
                    if (allResearchItems[i].researchID == idOfResearch)
                    {
                        currentResearchTimer = UnbiasedTime.Instance.Now().AddMinutes(allResearchItems[i].totalMinutes);
                        currentActiveResearch = allResearchItems[i];
                        isActivelyResearching = true;
                        GameController.Instance.tasksController.AddProgress(1, TasksController.TaskType.research);
                    }
                }
            }
        }

        public void LoadResearchTimer(DateTime wotTimeIsIt, string researchEyeDee, double timeSince)
        {
            currentResearchTimer = wotTimeIsIt;
            currentResearchTimer = currentResearchTimer.AddSeconds(-timeSince);
            currentActiveResearch = GetItemFromID(researchEyeDee);
            isActivelyResearching = true;
        }

        public float miningShipSpeedBoost = 1.0f;
        public int offlineEarningsHours = 0;
        public bool firstScanComplete = false;
        public float cargoEngingeSpeedBoost = 1.0f;
        public double extraGoldEarned = 1.0d;


        public void LoadAllResearchBoosts()
        {
            miningShipSpeedBoost = 1.0f;
            offlineEarningsHours = 0;
            firstScanComplete = false;
            cargoEngingeSpeedBoost = 1.0f;
            extraGoldEarned = 1.0d;

            for (int i = 0; i < allResearchItems.Length; i++)
            {
                if (allResearchItems[i].isOwned)
                {
                    switch (allResearchItems[i].researchID)
                    {
                        case "01":
                            miningShipSpeedBoost += 0.05f;
                            break;
                        case "02":
                            offlineEarningsHours += 1;
                            break;
                        case "03":
                            firstScanComplete = true;
                            FirstScanComplete();
                            break;
                        case "04":
                            cargoEngingeSpeedBoost += 0.05f;
                            break;
                        case "05":
                            extraGoldEarned += 0.1d;
                            break;
                        case "06":
                            offlineEarningsHours += 1;
                            break;
                        case "07":

                            break;
                        case "08":
                            cargoEngingeSpeedBoost += 0.05f;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        [SerializeField]
        GameObject darkeningOfSystem;

        void FirstScanComplete()
        {
            darkeningOfSystem.SetActive(false);
            GameController.Instance.UpdateAllLevel2Systems();
        }

        public int DiamondCostToSpeedUp()
        {
            double returnAmt = researchTime.TotalMinutes / 5.0d;
            if (returnAmt <= 1)
            {
                return 0;
            }
            else
            {
                return (int) returnAmt;
            }
        }

        public void FinishResearchNow()
        {
            if (currentActiveResearch != null)
            {
                currentActiveResearch.isOwned = true;
                currentResearchTimer = UnbiasedTime.Instance.Now();
                isActivelyResearching = false;
                LoadAllResearchBoosts();
            }
        }

        public SingleResearchItem GetItemFromID(string idToCheck)
        {
            for (int i = 0; i < allResearchItems.Length; i++)
            {
                if (allResearchItems[i].researchID == idToCheck)
                {
                    return allResearchItems[i];
                }
            }

            return null;
        }

        // Use this for initialization
        void Start()
        {
            SetupResearchItems();
        }

        public string researchTimerString;

        // Update is called once per frame
        void Update()
        {
            researchTime = currentResearchTimer - UnbiasedTime.Instance.Now();
            if (researchTime.TotalSeconds > 1)
            {
                isActivelyResearching = true;
                timerLabel.gameObject.SetActive(true);
                researchTimerString = GetTimerString(researchTime.Minutes, researchTime.Seconds);
                timerLabel.text = researchTimerString;
            }
            else
            {
                FinishResearchNow();
                timerLabel.gameObject.SetActive(false);
            }
        }

        public string GetTimerString(int totalMinutes, int totalSeconds)
        {
            if (totalMinutes > 0)
            {
                return totalMinutes.ToString() + "m " + totalSeconds.ToString() + "s";
            }
            else
            {
                return totalSeconds.ToString() + "s";
            }
        }

        public void SetupResearchItems()
        {
            allResearchItems = new SingleResearchItem[16];
            int counter = 0;

            SingleResearchItem temp = new SingleResearchItem();
            temp.researchID = "01";
            temp.researchDescription = "+5% Mining Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 100;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "02";
            temp.researchDescription = "+1 hour offline earnings";
            temp.shortDescription = "+1 hour";
            temp.totalMinutes = 10;
            temp.cost = 1000;
            temp.researchType = SingleResearchItem.ResearchType.Offline;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "03";
            temp.researchDescription = "See more asteroids in the galaxy";
            temp.shortDescription = "+50% Scan";
            temp.totalMinutes = 30;
            temp.cost = 10000000;
            temp.researchType = SingleResearchItem.ResearchType.Scan;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "04";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 15;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "05";
            temp.researchDescription = "+10% Gold Earned";
            temp.shortDescription = "+10%";
            temp.totalMinutes = 60;
            temp.cost = 2000000;
            temp.researchType = SingleResearchItem.ResearchType.Gold;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "06";
            temp.researchDescription = "+1h Offline Earnings";
            temp.shortDescription = "+1h";
            temp.totalMinutes = 90;
            temp.cost = 4000000;
            temp.researchType = SingleResearchItem.ResearchType.Offline;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "07";
            temp.researchDescription = "Research Coming Soon!";
            temp.shortDescription = "Mystery";
            temp.totalMinutes = 120;
            temp.cost = 0;
            temp.researchType = SingleResearchItem.ResearchType.Soon;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "08";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 120;
            temp.cost = 15000000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "09";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "10";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "11";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "12";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "13";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "14";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;


            temp = new SingleResearchItem();
            temp.researchID = "15";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;

            temp = new SingleResearchItem();
            temp.researchID = "16";
            temp.researchDescription = "+5% Cargo Engine Speed";
            temp.shortDescription = "+5%";
            temp.totalMinutes = 5;
            temp.cost = 10000;
            temp.researchType = SingleResearchItem.ResearchType.Engines;
            allResearchItems[counter] = temp;
            counter++;
        }
    }

}