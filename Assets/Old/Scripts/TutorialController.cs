﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Legacy
{
    public class TutorialController : MonoBehaviour
    {
        [SerializeField]
        GameObject bigShipToFollow;

        [SerializeField]
        GameObject mainCam;

        [SerializeField]
        GameObject boyCharacter;

        [SerializeField]
        GameObject girlCharacter;

        [SerializeField]
        GameObject tutorialTextBox;

        [SerializeField]
        Text tutorialText;

        [SerializeField]
        GameObject tutorialHand1;

        public bool tutorialCompleted = false;

        public int currentTutorialState = 0;

        // Use this for initialization
        void Start()
        {
            SetTutorialUp();
        }

        // Update is called once per frame
        void Update()
        {
            if (currentTutorialState == 0)
            {
                mainCam.transform.position = new Vector3(bigShipToFollow.transform.position.x,
                    bigShipToFollow.transform.position.y,
                    mainCam.transform.position.z);
                mainCam.GetComponent<Camera>().orthographicSize = 8;
            }

            if (currentTutorialState >= 2)
            {
                boyCharacter.SetActive(false);
                girlCharacter.SetActive(false);
                tutorialTextBox.SetActive(false);
                tutorialHand1.SetActive(false);
                tutorialCompleted = true;
            }
        }

        public void IncrementTutorialState()
        {
            currentTutorialState++;
            SetTutorialUp();
        }

        public void LoadTutorial(int whatStage)
        {
            if (whatStage > 0)
            {
                bigShipToFollow.GetComponent<Animator>().Play("ShipAnimation", 0, 7.8f);
            }

            currentTutorialState = whatStage;
            SetTutorialUp();
        }

        public void SetTutorialUp()
        {
            switch (currentTutorialState)
            {
                case 0:
                    boyCharacter.SetActive(true);
                    tutorialTextBox.SetActive(true);
                    tutorialText.text = "We're coming up on the Asteroid field Captain!";
                    break;
                case 1:
                    boyCharacter.SetActive(false);
                    girlCharacter.SetActive(true);
                    tutorialTextBox.SetActive(true);
                    tutorialText.text = "Finally! Time to get our ships mining.";
                    tutorialHand1.SetActive(true);
                    //GameController.Instance.MoveUIIn();
                    break;
                case 2:
                    boyCharacter.SetActive(false);
                    girlCharacter.SetActive(false);
                    tutorialTextBox.SetActive(false);
                    tutorialHand1.SetActive(false);
                    tutorialCompleted = true;
                    //GameController.Instance.MoveUIIn();
                    break;
                default:
                    boyCharacter.SetActive(false);
                    girlCharacter.SetActive(false);
                    tutorialTextBox.SetActive(false);
                    tutorialHand1.SetActive(false);
                    tutorialCompleted = true;
                    break;
            }
        }
    }

}