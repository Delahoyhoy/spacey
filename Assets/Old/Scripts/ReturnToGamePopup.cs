﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

namespace Legacy
{
    public class ReturnToGamePopup : MonoBehaviour
    {
        [SerializeField]
        Text totalEarnedText;

        [SerializeField]
        Button showDoubleButton;

        [SerializeField]
        Text doubleButtonText;

        [SerializeField]
        Animator theAnimator;

        double amountToEarn = 0;

        // Use this for initialization
        void Start()
        {
            theAnimator.Play("Maximise");
        }

        public void SetupEarnedPopup(double amountToGive)
        {
            amountToEarn = amountToGive;
            totalEarnedText.text = CurrencyController.CurrencyToString(amountToGive);
            //if (!Advertisement.IsReady())
            //{
            //    showDoubleButton.interactable = false;
            //    doubleButtonText.text = "No Ads Available";
            //}
        }

        public void PressCollect()
        {
            GameController.Instance.currencyController.AddCurrency(amountToEarn, CurrencyType.SoftCurrency);
            DismissPopup();
        }

        //public void PressDouble() {
        //    var options = new ShowOptions { resultCallback = HandleShowResult };
        //    Advertisement.Show(options);
        //}

        //public void HandleShowResult(ShowResult result)
        //{
        //    switch (result)
        //    {
        //        case ShowResult.Finished:
        //            GameController.Instance.currencyController.AddCurrency(amountToEarn * 2.0d, CurrencyType.Gold);
        //            DismissPopup();
        //            break;
        //        case ShowResult.Skipped:
        //            break;
        //        case ShowResult.Failed:
        //            break;
        //    }
        //}


        public void DismissPopup()
        {
            GameController.Instance.IsPopupOpenNow = false;
            GameController.Instance.PlayRegularCoinBlast();
            theAnimator.Play("Minimise");
            StartCoroutine(WaitThenDestroyThisBud());
        }

        // Update is called once per frame
        void Update()
        {

        }


        IEnumerator WaitThenDestroyThisBud()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            Destroy(this.gameObject);
        }
    }

}