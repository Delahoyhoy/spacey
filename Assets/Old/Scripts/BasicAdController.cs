﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

namespace Legacy
{
    public class BasicAdController : MonoBehaviour
    {
        [SerializeField]
        GameObject advertUFO;

        [SerializeField]
        GameObject asteroidFoundPopupThing;

        TimeSpan time;
        public DateTime adTimer;

        [SerializeField]
        Text adTimerText;

        // Use this for initialization
        void Start()
        {
            StartCoroutine(TESTAdUFO(40));
            //#if UNITY_IOS
            //    Advertisement.Initialize("1348291", true);
            //    Debug.Log("Ads initialised I think");
            //#else
            //    Advertisement.Initialize("3029409");
            //#endif
            //TODO: PRERELEASE: CHANGE AD IDs ABOVE
        }

        bool closedPopup = false;

        IEnumerator TESTAdUFO(int waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            if (!closedPopup)
            {
                if (!isAdModeActive && GameController.Instance.tutorialController.tutorialCompleted)
                {
                    //if (Advertisement.IsReady())
                    //{
                    //    if (!advertUFO.activeSelf && !asteroidFoundPopupThing.activeSelf)
                    //    {
                    //        int rando = UnityEngine.Random.Range(0, 100);

                    //        if(rando < 50)
                    //        {
                    //            advertUFO.SetActive(true);
                    //            advertUFO.GetComponent<Animator>().Play(0, 0, 0);
                    //        }
                    //        else
                    //        {
                    //            asteroidFoundPopupThing.SetActive(true);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    Debug.Log("No Ads Ready");
                    //}
                    //StartCoroutine(TESTAdUFO(30));
                }
                else
                {
                    StartCoroutine(TESTAdUFO(30));
                }
            }
            else
            {
                closedPopup = false;
                StartCoroutine(TESTAdUFO(30));
            }
        }

        public void TurnOffUFO()
        {
            advertUFO.SetActive(false);
            closedPopup = true;
        }

        public void TurnOffAsteroidFound()
        {
            asteroidFoundPopupThing.SetActive(false);
        }

        public void SetAdToOn()
        {
            adTimer = UnbiasedTime.Instance.Now()
                .AddMinutes(5 + GameController.Instance.boostsController.extraAdMinutes);
            isAdModeActive = true;
        }

        public bool isAdModeActive = false;


        // Update is called once per frame
        void Update()
        {
            time = adTimer - UnbiasedTime.Instance.Now();
            if (time.TotalSeconds > 1)
            {
                isAdModeActive = true;
                adTimerText.gameObject.SetActive(true);
                adTimerText.text = GetTimerString(time.Minutes, time.Seconds);
            }
            else
            {
                isAdModeActive = false;
                adTimerText.gameObject.SetActive(false);
            }
        }

        public string GetTimerString(int totalMinutes, int totalSeconds)
        {
            if (totalMinutes > 0)
            {
                return totalMinutes.ToString() + "m " + totalSeconds.ToString() + "s";
            }
            else
            {
                return totalSeconds.ToString() + "s";
            }
        }
    }

}