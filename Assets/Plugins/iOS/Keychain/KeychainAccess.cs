﻿using System.Runtime.InteropServices;

namespace Fumb.iOS.Keychain {
	public class KeychainAccess {

		#if UNITY_IOS && !UNITY_EDITOR
		[DllImport ("__Internal")]
		private static extern string _GetKeychainPassword(string service, string account);

		[DllImport ("__Internal")]
		private static extern void _SetKeychainPassword(string password, string service, string account);

		[DllImport ("__Internal")]
		private static extern void _DeleteKeychainPassword(string service, string account);
		#else
		private static string _GetKeychainPassword(string service, string account) {
			throw new System.InvalidOperationException("Cannot access keychain information on this platform (attempted to GET keychain password)");
		}

		private static void _SetKeychainPassword(string password, string service, string account) {
			throw new System.InvalidOperationException("Cannot access keychain information on this platform (attempted to SET keychain password)");
		}

		private static void _DeleteKeychainPassword(string service, string account) {
			throw new System.InvalidOperationException("Cannot access keychain information on this platform (attempted to DELETE keychain password)");
		}
		#endif

		public static string GetKeychainPassword(string service, string account) {
			return _GetKeychainPassword(service, account);
		}

		public static void SetKeychainPassword(string password, string service, string account) {
			_SetKeychainPassword(password, service, account);
		}

		public static void DeleteKeychainPassword(string service, string account) {
			_DeleteKeychainPassword(service, account);
		}
	}
}