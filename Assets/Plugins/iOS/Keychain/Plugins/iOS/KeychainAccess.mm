//Bridge between Unity and iOS Keychain access.
//Allows getting, setting and deleting of keychain values.

#import <Foundation/Foundation.h>
#import "SAMKeychain.h"
#import "UnityBridgeHelper.h"

extern "C" {
    
    const char* _GetKeychainPassword(char* cStringService, char* cStringAccount) {
        NSString *service = [NSString stringWithUTF8String:cStringService];
        NSString *account = [NSString stringWithUTF8String:cStringAccount];
        NSString *keychainPassword = [SAMKeychain passwordForService:service account:account];
        
        return [UnityBridgeHelper cStringCopy:[keychainPassword UTF8String]];
    }
    
    void _SetKeychainPassword(char* cStringPassword, char* cStringService, char* cStringAccount) {
        NSString *password = [NSString stringWithUTF8String:cStringPassword];
        NSString *service = [NSString stringWithUTF8String:cStringService];
        NSString *account = [NSString stringWithUTF8String:cStringAccount];
        
        [SAMKeychain setPassword:password forService:service account:account];
    }
    
    void _DeleteKeychainPassword(char* cStringService, char* cStringAccount) {
        NSString *service = [NSString stringWithUTF8String:cStringService];
        NSString *account = [NSString stringWithUTF8String:cStringAccount];
        
        [SAMKeychain deletePasswordForService:service account:account];
    }
}
