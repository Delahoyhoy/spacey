﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Legacy
{
	public class ChestAnimator : MonoBehaviour
	{
		public GameObject[] card;
		public Animator anima;
		public ChestScreenController theScreenController;

		void Start()
		{

		}

		void Update()
		{

		}

		void OnMouseDown()
		{
			if (theScreenController.cardsToOpenInThisChest > 0)
			{
				Destroy(GameObject.FindWithTag("card"));
				anima.Play("wooden_reopen");
				anima.Play("silver_reopen");
				anima.Play("golden_reopen");
				anima.Play("magical_reopen");
				theScreenController.IncrementCardsOpened();
				GameObject temp = Instantiate(card[Random.Range(1, 4)], this.transform.parent.transform);
				temp.GetComponent<CardController>().theSpriteRenderer.sprite = theScreenController.GetShipSprite();
			}
		}
	}

}