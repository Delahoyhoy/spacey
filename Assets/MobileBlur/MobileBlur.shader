Shader "SupGames/Mobile/BlurOnePass"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "" {}
	}

	CGINCLUDE
	#include "UnityCG.cginc"


	struct appdata {
		half4 pos : POSITION;
		half2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float4 pos  : SV_POSITION;
		half2  uv  : TEXCOORD0;
	};

	struct v2fb2
	{
		float4 pos  : SV_POSITION;
		half4  uv : TEXCOORD0;
	};

	struct v2fb3
	{
		float4 pos  : SV_POSITION;
		half4  uv  : TEXCOORD0;
		half4  uv1 : TEXCOORD1;
	};

	sampler2D _MainTex;
	sampler2D _MaskTex;
	sampler2D _BlurTex;
	uniform half4 _MainTex_TexelSize;
	uniform half _BlurAmount;

	v2f vert(appdata v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.pos);
		o.uv = v.uv;
		return o;
	}

	v2fb2 vertb2(appdata v)
	{
		v2fb2 o;
		o.pos = UnityObjectToClipPos(v.pos);
		half2 offset = (_MainTex_TexelSize.xy) * _BlurAmount;
		o.uv = half4(v.uv - offset, v.uv + offset);
		return o;
	}

	v2fb3 vertb3(appdata v)
	{
		v2fb3 o;
		o.pos = UnityObjectToClipPos(v.pos);
		half4 offset;
		offset.xy = (_MainTex_TexelSize.xy) * _BlurAmount;
		offset.zw = offset.xy * 2.0h;
		o.uv = half4(v.uv - offset.xy, v.uv + offset.xy);
		o.uv1 = half4(v.uv - offset.zw, v.uv + offset.zw);
		return o;
	}

	fixed4 fragBlur2(v2fb2 i) : COLOR
	{
		fixed4 result = tex2D(_MainTex, i.uv.xy);
		result += tex2D(_MainTex, i.uv.xw);
		result += tex2D(_MainTex, i.uv.zy);
		result += tex2D(_MainTex, i.uv.zw);
		return result * 0.25h;
	}

	fixed4 fragBlur3(v2fb3 i) : COLOR
	{
		fixed4 result = tex2D(_MainTex, i.uv.xy);
		result += tex2D(_MainTex, i.uv.xw);
		result += tex2D(_MainTex, i.uv.zy);
		result += tex2D(_MainTex, i.uv.zw);
		result += tex2D(_MainTex, i.uv1.xy);
		result += tex2D(_MainTex, i.uv1.xw);
		result += tex2D(_MainTex, i.uv1.zy);
		result += tex2D(_MainTex, i.uv1.zw);
		return result * 0.125h;
	}

	fixed4 fragBlurOnly(v2f i) : COLOR
	{
		fixed4 c = tex2D(_MainTex, i.uv);
		fixed4 b = tex2D(_BlurTex, i.uv);
		fixed4 m = tex2D(_MaskTex, i.uv);
		return lerp(c, b, m.r);
	}

	ENDCG

	Subshader
	{
		Pass
		{
		  ZTest Always Cull Off ZWrite Off
		  Fog { Mode off }
		  CGPROGRAM
		  #pragma vertex vertb2
		  #pragma fragment fragBlur2
		  #pragma fragmentoption ARB_precision_hint_fastest
		  ENDCG
		}
		Pass
		{
		  ZTest Always Cull Off ZWrite Off
		  Fog { Mode off }
		  CGPROGRAM
		  #pragma vertex vertb3
		  #pragma fragment fragBlur3
		  #pragma fragmentoption ARB_precision_hint_fastest
		  ENDCG
		}
		Pass
		{
		  ZTest Always Cull Off ZWrite Off
		  Fog { Mode off }
		  CGPROGRAM
		  #pragma vertex vert
		  #pragma fragment fragBlurOnly
		  #pragma fragmentoption ARB_precision_hint_fastest
		  ENDCG
		}
	}
	Fallback off
}