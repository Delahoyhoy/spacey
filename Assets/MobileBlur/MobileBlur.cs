﻿using UnityEngine;

public class MobileBlur : MonoBehaviour
{
    public enum BlurPass
    {
        SinglePass,
        ThreePass = 50,
    }

    public BlurPass blurPassCount;
    [Range(2, 4)]
    public int KernelSize = 2;
    [Range(0, 3)]
    public float BlurAmount = 2;
    public Texture2D maskTexture;
    public Material material = null;

    public float Blur { set { targetBlur = value; } }

    private float targetBlur = 0;

    static readonly int blurAmountString = Shader.PropertyToID("_BlurAmount");
    static readonly int blurTexString = Shader.PropertyToID("_BlurTex");
    static readonly int maskTexString = Shader.PropertyToID("_MaskTex");

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (BlurAmount <= 0.1f)
        {
            Graphics.Blit(source, destination);
            return;
        }

        material.SetFloat(blurAmountString, BlurAmount);

        if (maskTexture == null)
        {
            material.SetTexture(maskTexString, Texture2D.whiteTexture);
        }
        else material.SetTexture(maskTexString, maskTexture);

        if (blurPassCount==BlurPass.SinglePass)
        {
            var temp = RenderTexture.GetTemporary(Screen.width, Screen.height, 0, source.format);
            Graphics.Blit(source, temp, material, KernelSize - 2);

            material.SetTexture(blurTexString, temp);

            Graphics.Blit(source, destination, material, 2);
            RenderTexture.ReleaseTemporary(temp);
        }
        else
        {
            var temp = RenderTexture.GetTemporary(Screen.width / 2, Screen.height / 2, 0, source.format);
            Graphics.Blit(source, temp, material, KernelSize - 2);

            var temp1 = RenderTexture.GetTemporary(Screen.width / 4, Screen.height / 4, 0, source.format);
            Graphics.Blit(temp, temp1, material, KernelSize - 2);
            RenderTexture.ReleaseTemporary(temp);

            var temp2 = RenderTexture.GetTemporary(Screen.width / 2, Screen.height / 2, 0, source.format);
            Graphics.Blit(temp1, temp2, material, KernelSize - 2);
            RenderTexture.ReleaseTemporary(temp1);

            material.SetTexture(blurTexString, temp2);

            Graphics.Blit(source, destination, material, 2);
            RenderTexture.ReleaseTemporary(temp2);
        }
    }

    private void Update()
    {
        BlurAmount = Mathf.Lerp(BlurAmount, targetBlur, Time.deltaTime * 4f);
        //BlurAmount = (1.5f * Mathf.Sin(Time.time)) + 1.5f;
    }
}