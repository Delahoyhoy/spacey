﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextOverObject : MonoBehaviour {
    [SerializeField]
    GameObject planetToFollow;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.gameObject.transform.position = Camera.main.WorldToScreenPoint(planetToFollow.transform.position);
	}
}
