﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickerController : Singleton<ClickerController>
{
    protected ClickerController() { }

    [SerializeField]
    Text thePickingUpableText;

    public List<GameObject> allShipsGrabbed = new List<GameObject>();
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Camera.main.transform.forward, out hit))
            {
                if (hit.transform.gameObject.tag == "Planet")
                {
                    Debug.Log("Should be picking up");
                    allShipsGrabbed = hit.transform.gameObject.GetComponent<ShipGenerator>().allShipsDockedHere;
                    hit.transform.gameObject.GetComponent<ShipGenerator>().allShipsDockedHere = new List<GameObject>();

                    thePickingUpableText.gameObject.transform.position = Camera.main.WorldToScreenPoint(Input.mousePosition);
                    thePickingUpableText.text = allShipsGrabbed.Count.ToString();
                }
            }
        }
	}
}
