﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipGenerator : MonoBehaviour {
    public enum PlanetState { PlayerOwned, EnemyOwned, Neutral };

    public PlanetState myPlanetState;

    [SerializeField]
    GameObject shipPrefab;
    [SerializeField]
    float timeBetweenShips = 1;
    [SerializeField]
    GameObject TEMPGoal;
    [SerializeField]
    Text uiText;
    [SerializeField]
    SpriteRenderer theOwnedSprite;

    public int startShipsAmount = 5;

    List<GameObject> allShipsMoving;
    public List<GameObject> allShipsDockedHere;
	// Use this for initialization
	void Start () {
        ChangePlanetState(myPlanetState);
        allShipsDockedHere = new List<GameObject>();
        allShipsMoving = new List<GameObject>();

        for (int i = 0; i < startShipsAmount; i++)
        {
            if (myPlanetState != PlanetState.Neutral)
            {
                AddShip();
            }
        }

        if(myPlanetState == PlanetState.Neutral){
            startShipsAmount--;
            uiText.text = startShipsAmount.ToString();
        }

        StartCoroutine(BuildShips());
	}

    IEnumerator BuildShips(){
        yield return new WaitForSeconds(timeBetweenShips);
        if (myPlanetState != PlanetState.Neutral)
        {
            AddShip();
        }
        StartCoroutine(BuildShips());
    }

    void AddShip() {
        GameObject temp = Instantiate(shipPrefab);
        temp.SetActive(false);
        allShipsDockedHere.Add(temp);
        uiText.text = allShipsDockedHere.Count.ToString();
    }
	
	// Update is called once per frame
	void Update () {
        if (myPlanetState == PlanetState.PlayerOwned)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                for (int i = 0; i < allShipsDockedHere.Count; i++)
                {
                    allShipsDockedHere[i].GetComponent<NavMeshMoveAgentInnit>().goal = TEMPGoal.transform;
                    allShipsDockedHere[i].SetActive(true);
                }
            }
            /*if (Input.GetMouseButtonDown(0))
            {
                SendShipsToAPlanet();
            }*/
        }
	}

    public void SendShipsToAPlanet(){
        allShipsMoving = allShipsDockedHere;
        allShipsDockedHere = new List<GameObject>();
        for (int i = 0; i < allShipsMoving.Count; i++)
        {
            allShipsMoving[i].GetComponent<NavMeshMoveAgentInnit>().goal = TEMPGoal.transform;
            allShipsMoving[i].SetActive(true);
        }
    }

    void ChangePlanetState(PlanetState theState){
        myPlanetState = theState;
        switch (theState)
        {
            case PlanetState.EnemyOwned:
                theOwnedSprite.color = Color.red;
                break;
            case PlanetState.PlayerOwned:
                theOwnedSprite.color = Color.blue;
                break;
            case PlanetState.Neutral:
                theOwnedSprite.color = Color.white;
                break;
            default:
                break;
        }
    }

    public void ShipArrived(){
        if(myPlanetState == PlanetState.PlayerOwned){
            AddShip();
        } else {
            RemoveShip();
        }
    }

    public void RemoveShip(){
        if (myPlanetState != PlanetState.Neutral)
        {
            if (allShipsDockedHere.Count > 0)
            {
                allShipsDockedHere.RemoveAt(0);
            }
            if (allShipsDockedHere.Count <= 0)
            {
                ChangePlanetState(PlanetState.PlayerOwned);
            }
            uiText.text = allShipsDockedHere.Count.ToString();
        } else {
            startShipsAmount--;
            if(startShipsAmount <=0){
                ChangePlanetState(PlanetState.PlayerOwned);
            }
            uiText.text = startShipsAmount.ToString();
        }
    }
}
