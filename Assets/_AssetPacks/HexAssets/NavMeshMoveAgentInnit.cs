﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshMoveAgentInnit : MonoBehaviour {
    
    public Transform goal;

    public LineRenderer lineRenderer;

    void Start()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = goal.position;

    }

    private void Update()
    {
        if(Vector3.Distance(this.transform.position, goal.position) < 1.2965f){
            goal.GetComponent<ShipGenerator>().ShipArrived();
            Destroy(this.gameObject);
        }

        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, this.transform.position);
        lineRenderer.SetPosition(1, goal.position);
    }
}
