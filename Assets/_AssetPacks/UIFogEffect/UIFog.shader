Shader "Custom/UIReveal"
{
	Properties
	{
		_MainTex ( "Screen", 2D ) = "black" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		_BorderColour("BorderColour", Color) = (0.1633326,0.3132755,0.4191176,0.75)
		_Revealer("Revealer", Vector) = (0,0,0,0)
		_BorderWidth("BorderWidth", Float) = 0.005
		_ObscureColour("ObscureColour", Color) = (0,0,0,0.75)
	}

	SubShader
	{
		Tags{  }
		
		ZTest Always Cull Off ZWrite Off
		


		Pass
		{ 
			CGPROGRAM 

			#pragma vertex vert_img_custom 
			#pragma fragment frag
			#pragma target 3.0
			#include "UnityCG.cginc"
			


			struct appdata_img_custom
			{
				float4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
			};

			struct v2f_img_custom
			{
				float4 pos : SV_POSITION;
				half2 uv   : TEXCOORD0;
				half2 stereoUV : TEXCOORD2;
		#if UNITY_UV_STARTS_AT_TOP
				half4 uv2 : TEXCOORD1;
				half4 stereoUV2 : TEXCOORD3;
		#endif
			};

			uniform sampler2D _MainTex;
			uniform half4 _MainTex_TexelSize;
			uniform half4 _MainTex_ST;
			
			uniform float4 _BorderColour;
 uniform float4 _Revealer;
 uniform float _BorderWidth;
 uniform float4 _ObscureColour;

			v2f_img_custom vert_img_custom ( appdata_img_custom v  )
			{
				v2f_img_custom o;
				o.pos = UnityObjectToClipPos ( v.vertex );
				o.uv = float4( v.texcoord.xy, 1, 1 );

				#ifdef UNITY_HALF_TEXEL_OFFSET
						o.uv.y += _MainTex_TexelSize.y;
				#endif

				#if UNITY_UV_STARTS_AT_TOP
					o.uv2 = float4( v.texcoord.xy, 1, 1 );
					o.stereoUV2 = UnityStereoScreenSpaceUVAdjust ( o.uv2, _MainTex_ST );

					if ( _MainTex_TexelSize.y < 0.0 )
						o.uv.y = 1.0 - o.uv.y;
				#endif
				o.stereoUV = UnityStereoScreenSpaceUVAdjust ( o.uv, _MainTex_ST );
				return o;
			}

			half4 frag ( v2f_img_custom i ) : SV_Target
			{
				#ifdef UNITY_UV_STARTS_AT_TOP
					half2 uv = i.uv2;
					half2 stereoUV = i.stereoUV2;
				#else
					half2 uv = i.uv;
					half2 stereoUV = i.stereoUV;
				#endif	
				
				half4 finalColor;

				
				float2 uv_MainTex = i.uv.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode68 = tex2D( _MainTex, uv_MainTex );
				float2 appendResult5 = (float2(_Revealer.x , _Revealer.y));
				float temp_output_4_0 = distance( appendResult5 , uv_MainTex );
				float temp_output_74_0 = ( _Revealer.w - _BorderWidth );
				float clampResult85 = clamp( ( ( temp_output_4_0 - temp_output_74_0 ) / ( _Revealer.w - temp_output_74_0 ) ) , 0.0 , 1.0 );
				float4 lerpResult75 = lerp( tex2DNode68 , _BorderColour , clampResult85);
				float clampResult84 = clamp( ( ( temp_output_4_0 - _Revealer.w ) / ( ( _Revealer.w + _BorderWidth ) - _Revealer.w ) ) , 0.0 , 1.0 );
				float4 lerpResult80 = lerp( lerpResult75 , _ObscureColour , clampResult84);
				

				finalColor = lerpResult80;

				return finalColor;
			} 
			ENDCG 
		}
	}
}