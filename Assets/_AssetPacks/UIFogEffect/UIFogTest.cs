﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HighlightType
{
    World,
    Screen,
}

[System.Serializable]
public struct Highlight
{
    public string highlightID;
    public Transform highlightTarget;
    public HighlightType highlightType;
    public float targetRadius;
}

public class UIFogTest : MonoBehaviour {

    [SerializeField]
    private Material material;

    [SerializeField]
    private float radius = 0.2f, focusSpeed = 1f;

    [SerializeField]
    private Highlight[] highlights;

    private Dictionary<string, Highlight> highlightDictionary = new Dictionary<string, Highlight>();

    private Transform currentTarget;
    private HighlightType mode;

    private Vector3 currentPosition = Vector3.zero;
    private float currentRadius = 2f;

    private Vector3 highlightPoint = Vector3.zero;
    private float aspectRatio = 1;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null || currentRadius >= 1.5f)
        {
            Graphics.Blit(source, destination);
            return;
        }
        material.SetVector("_Revealer", new Vector4(highlightPoint.x, highlightPoint.y, 0, currentRadius + (Mathf.Sin(Time.time * 5f) * 0.005f)));
        
        Graphics.Blit(source, destination, material);
    }

    // Use this for initialization
    private void Awake () {
        material = new Material(Shader.Find("Custom/UIReveal"));
        aspectRatio = (float)Screen.width / Screen.height;
        material.SetVector("_Revealer", new Vector4(highlightPoint.x, highlightPoint.y, 0, currentRadius));
        for (int i = 0; i < highlights.Length; i++)
        {
            highlightDictionary.Add(highlights[i].highlightID, highlights[i]);
        }
	}

    public void Hide()
    {
        radius = 1.75f;
    }

    public void ShowHighlight(string id)
    {
        if (!highlightDictionary.ContainsKey(id))
            return;
        radius = highlightDictionary[id].targetRadius;
        currentTarget = highlightDictionary[id].highlightTarget;
        mode = highlightDictionary[id].highlightType;
        switch (mode)
        {
            case HighlightType.Screen:
                highlightPoint = GetPositionFromScreen(currentTarget.position);
                break;
            case HighlightType.World:
                highlightPoint = GetPositionFromWorld(currentTarget.position);
                break;
        }
    }

    public void ShowCustomHighlight(Transform target, float rad, HighlightType highlightType)
    {
        currentTarget = target;
        radius = rad;
        mode = highlightType; 
    }

    private Vector3 GetPositionFromScreen(Vector3 from)
    {
        aspectRatio = (float)Screen.width / Screen.height;
        float normalisedX = from.x / Screen.width, normalisedY = from.y / Screen.height;
        float minX = (1 - aspectRatio) / 2f, maxX = 1 - minX;
        normalisedX = Mathf.Lerp(minX, maxX, normalisedX);
        return new Vector3(normalisedX, normalisedY, 0);
    }

    private Vector3 GetPositionFromWorld(Vector3 position)
    {
        return GetPositionFromScreen(Camera.main.WorldToScreenPoint(position));
    }

    // Update is called once per frame
    void Update () {
        //mousePos = GetPositionFromScreen(Input.mousePosition); // USE THIS ONE
        //mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        if (currentTarget != null && currentRadius < 1.5f)
        {
            switch (mode)
            {
                case HighlightType.Screen:
                    highlightPoint = GetPositionFromScreen(currentTarget.position);
                    break;
                case HighlightType.World:
                    highlightPoint = GetPositionFromWorld(currentTarget.position);
                    break;
            }
        }
        currentRadius = Mathf.Lerp(currentRadius, radius, Time.deltaTime * focusSpeed);
        //float scrollDelta = Input.GetAxis("Mouse ScrollWheel");
        //radius -= scrollDelta;
    }
}
