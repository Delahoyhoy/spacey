using System.Collections.Generic;
#if USE_FUMB_REVIEWS
using Fumb.Reviews;
#endif
using Fumb.Editor;

namespace Fumb.Modules
{
    public class ModuleReviews : FumbModule
    {
        public override string ScriptingDefine {
            get {
                return "USE_FUMB_REVIEWS";
            }
        }
        
        public override string ExampleScenePath => "Assets/InAppReview/Example/InAppReviewExample";

        protected override List<FumbModuleIncompleteStep> IncompleteSetupSteps
        {
            get
            {
                List<FumbModuleIncompleteStep> incompleteSetupSteps = new List<FumbModuleIncompleteStep>();
                
#if USE_FUMB_REVIEWS
                ReviewSettings reviewSettings = EditorFumbResourcesHelper.GetResourcesObject<ReviewSettings>(false);

                if (reviewSettings == null)
                    incompleteSetupSteps.Add(new FumbModuleIncompleteStep("No review settings found", EditorReviews.GoToSettings));
                else
                {
                    bool useStore = !reviewSettings.UseInAppReviewFlow || reviewSettings.FallbackToStoreOnFail;  
#if UNITY_ANDROID
                    if (string.IsNullOrEmpty(reviewSettings.GooglePlayStoreIdentifier) && useStore)
                        incompleteSetupSteps.Add(new FumbModuleIncompleteStep("Store identifier cannot be blank, add one in settings", EditorReviews.GoToSettings));
#elif UNITY_IOS
                    if (string.IsNullOrEmpty(reviewSettings.AppStoreId) && useStore)
                        incompleteSetupSteps.Add(new FumbModuleIncompleteStep("Store identifier cannot be blank, add one in settings", EditorReviews.GoToSettings));
#endif
                }
#endif

                return incompleteSetupSteps;
            }
        }
    }
}
