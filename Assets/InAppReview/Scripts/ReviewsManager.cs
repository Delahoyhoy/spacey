﻿#if USE_FUMB_REVIEWS
using UnityEngine;
using UnityEngine.Events;
#if UNITY_ANDROID
using Google.Play.Review;
#elif UNITY_IOS
using UnityEngine.iOS;

#endif

namespace Fumb.Reviews
{
    public static class ReviewsManager
    {
        public static void OpenReviewFlow(UnityAction<bool> callback)
        {
            ReviewSettings reviewSettings = FumbResourcesHelper.LoadObject<ReviewSettings>();
            
#if UNITY_ANDROID
            RequestAndroidReview(callback, !reviewSettings.UseInAppReviewFlow, reviewSettings.FallbackToStoreOnFail, reviewSettings.GooglePlayStoreIdentifier);
#elif UNITY_IOS
            RequestIOSReview(callback, !reviewSettings.UseInAppReviewFlow, reviewSettings.FallbackToStoreOnFail, reviewSettings.AppStoreId);
#endif
        }

#if UNITY_ANDROID

        /// <summary>
        /// Filter review flow for Android, fallback to store page if review process fails.
        /// </summary>
        /// <param name="callback">On Complete action, declare whether in app review was successful</param>
        /// <param name="forceToStore">Should the review flow go directly to the store page instead of in app</param>
        /// <param name="storeOnFail">Should open store if in app flow fails</param>
        /// <param name="fallbackId">The Store ID to fallback to if In App Review is not used</param>
        private static void RequestAndroidReview(UnityAction<bool> callback, bool forceToStore, bool storeOnFail, string fallbackId = "")
        {
            if (forceToStore)
            {
                if (storeOnFail)
                    OpenGooglePlayStorePage(fallbackId);
                callback?.Invoke(false);
                return;
            }

            try
            {
                var reviewManager = new ReviewManager();

                var playReviewInfoAsyncOperation = reviewManager.RequestReviewFlow();

                playReviewInfoAsyncOperation.Completed += playReviewInfoAsync =>
                {
                    if (playReviewInfoAsync.Error == ReviewErrorCode.NoError)
                    {
                        // Display the review prompt
                        var playReviewInfo = playReviewInfoAsync.GetResult();
                        reviewManager.LaunchReviewFlow(playReviewInfo);
                        callback?.Invoke(true);
                    }
                    else
                    {
                        if (storeOnFail)
                            OpenGooglePlayStorePage(fallbackId);
                        callback?.Invoke(false);
                        // Handle error when loading review prompt 
                    }
                };
            }
            catch
            {
                if (storeOnFail)
                    OpenGooglePlayStorePage(fallbackId);
                callback?.Invoke(false);
            }
        }

        private static void OpenGooglePlayStorePage(string id)
        {
            // Should automatically find the relevant store page using Application Identifier
            Application.OpenURL("market://details?id=" + id);
        }
#elif UNITY_IOS
        /// <summary>
        /// Filter review flow for iOS, fallback to store page if review process fails or is disabled.
        /// </summary>
        /// <param name="callback">On Complete action, declare whether in app review was successful</param>
        /// <param name="forceToStore">Should the review flow go directly to the store page instead of in app</param>
        /// <param name="storeOnFail">Should open store if in app flow fails</param>
        /// <param name="fallbackId">The Store ID to fallback to if In App Review is not used</param>
        private static void RequestIOSReview(UnityAction<bool> callback, bool forceToStore, bool storeOnFail, string fallbackId)
        {
            if (forceToStore)
            {
                if (storeOnFail)
                    OpenAppStorePage(fallbackId);
                callback?.Invoke(false);
                return;
            }

            if (!Device.RequestStoreReview())
            {
                if (storeOnFail)
                    OpenAppStorePage(fallbackId);
                callback?.Invoke(false);
                return;
            }
            
            callback?.Invoke(true);
        }

        private static void OpenAppStorePage(string id)
        {
            //string identifier = "id1455782143";
            Application.OpenURL(string.Format("itms-apps://itunes.apple.com/app/{0}", id));
        }
#endif
    }
}
#endif