using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fumb.Reviews
{
    public class ReviewSettings : ScriptableObject
    {
        [SerializeField]
        private bool overrideAndroidIdentifier;

        [SerializeField]
        private string googlePlayStoreIdentifier;

        public string GooglePlayStoreIdentifier =>
            overrideAndroidIdentifier ? googlePlayStoreIdentifier : Application.identifier;        
        
        [SerializeField]
        private string appStoreId;

        public string AppStoreId => appStoreId;

        [SerializeField]
        private bool useInAppReviewFlow = true;

        public bool UseInAppReviewFlow => useInAppReviewFlow;

        [SerializeField]
        private bool fallbackToStoreOnFail = true;

        public bool FallbackToStoreOnFail => fallbackToStoreOnFail;
    }
}
