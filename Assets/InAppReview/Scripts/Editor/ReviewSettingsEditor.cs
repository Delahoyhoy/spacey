#if USE_FUMB_REVIEWS
namespace Fumb.Reviews
{
    using UnityEditor;
    using UnityEngine;
    
    [CustomEditor(typeof(ReviewSettings))]
    public class ReviewSettingsEditor : Editor
    {
        private SerializedProperty overrideAndroidIdentifier;
        private SerializedProperty googlePlayStoreIdentifier;
        private SerializedProperty appStoreId;
        private SerializedProperty useInAppReviewFlow;
        private SerializedProperty fallbackToStoreOnFail;
        
        private void OnEnable()
        {
            overrideAndroidIdentifier = serializedObject.FindProperty("overrideAndroidIdentifier");
            googlePlayStoreIdentifier = serializedObject.FindProperty("googlePlayStoreIdentifier");
            appStoreId = serializedObject.FindProperty("appStoreId");
            useInAppReviewFlow = serializedObject.FindProperty("useInAppReviewFlow");
            fallbackToStoreOnFail = serializedObject.FindProperty("fallbackToStoreOnFail");
        }

        public override void OnInspectorGUI()
        {
            bool useStore = (!useInAppReviewFlow.boolValue || fallbackToStoreOnFail.boolValue);
            Color guiStartColour = GUI.color;
            serializedObject.Update();
            bool currentOverrideValue = overrideAndroidIdentifier.boolValue;
            EditorGUILayout.PropertyField(overrideAndroidIdentifier);
            bool overrideAndroidId = overrideAndroidIdentifier.boolValue;
            GUI.enabled = overrideAndroidId && useStore;
            if (string.IsNullOrEmpty(googlePlayStoreIdentifier.stringValue) && !overrideAndroidId || currentOverrideValue && !overrideAndroidId)
                googlePlayStoreIdentifier.stringValue = Application.identifier;
            bool hasValueForGP = !string.IsNullOrEmpty(googlePlayStoreIdentifier.stringValue) || !useStore; 
            if (!hasValueForGP)
                GUI.color = Color.red;
            EditorGUILayout.PropertyField(googlePlayStoreIdentifier);
            if (!hasValueForGP)
                EditorGUILayout.LabelField("No store link for Google Play, add your App's Bundle Identifier");
            GUI.enabled = true;
            GUI.color = guiStartColour;
            
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Apple ID: (e.g. 'id1234567890')");
            GUI.enabled = useStore;
            bool hasValueForAS = !string.IsNullOrEmpty(appStoreId.stringValue) || !useStore;
            if (!hasValueForAS)
                GUI.color = Color.red;
            EditorGUILayout.PropertyField(appStoreId);
            if (!hasValueForAS)
                EditorGUILayout.LabelField("No App ID for iOS. Find your ID on App Store Connect");
            GUI.enabled = true;
            GUI.color = guiStartColour;
            
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(useInAppReviewFlow);
            GUI.enabled = useInAppReviewFlow.boolValue;
            EditorGUILayout.PropertyField(fallbackToStoreOnFail);
            GUI.enabled = true;
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
