#if UNITY_ANDROID
using System.Collections.Generic;

namespace Fumb.Build {
    public class ProguardEntryInAppReview : ProguardEntry {
        public override List<string> Entries {
            get {
                return new List<string>() {
                    "-keep class com.google.android.play.core.** { *; }"
                };
            }
        }

        public override bool RequiresEntries {
            get {
#if USE_FUMB_REVIEWS
                return true;
#else
                return false;
#endif
            }
        }
    }
}
#endif