#if USE_FUMB_REVIEWS
using Fumb.Editor;
using UnityEditor;

namespace Fumb.Reviews {
    public class EditorReviews {
        [MenuItem("Fumb/Reviews/Settings")]
        public static void GoToSettings() {
            EditorFumbResourcesHelper.SelectResourcesObject<ReviewSettings>(true);
        }
    }
}
#endif