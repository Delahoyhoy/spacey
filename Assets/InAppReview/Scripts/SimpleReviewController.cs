#if USE_FUMB_REVIEWS
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Fumb.UI;

namespace Fumb.Reviews
{
    public class SimpleReviewController : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent onSuccessIar, // Called if player was successfully shown In App Review flow
            onFailIar, // Called if In App Review failed (i.e. if the player is sent to store page instead)
            onFinalComplete; // Always called once review flow has been displayed regardless of 

        [SerializeField]
        private float builtInDelay = 8f; // Adjustable minimum delay for overlay to pop up for

        /// <summary>
        /// Call this method to bring up relevant Review page for your app
        /// </summary>
        public void DisplayReviewFlow()
        {
            StartCoroutine(WaitComplete());
        }

        private IEnumerator WaitComplete()
        {
            float timeRemaining = builtInDelay;
            bool inApp = false;

            void OnReviewDisplayed(bool useIar)
            {
                if (!useIar)
                    timeRemaining = 1f;
                inApp = useIar;
            }

            // Popup loading screen
            LoadingScreenController.Display(true, "Please Wait...");

            ReviewsManager.OpenReviewFlow(OnReviewDisplayed);

            // Wait until review flow has been processed or minimum time has elapsed
            while (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                yield return null;
            }

            if (inApp)
                onSuccessIar?.Invoke();
            else
                onFailIar?.Invoke();

            // Close loading screen on flow completed
            LoadingScreenController.Display(false);

            onFinalComplete?.Invoke();
        }
    }
}
#endif
