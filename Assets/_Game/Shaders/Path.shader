// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/Line"
{
	Properties
	{
		_LineColour("LineColour", Color) = (0,0.7103448,1,1)
		_Color0("Color 0", Color) = (1,1,1,1)
		_ArrowGap("ArrowGap", Float) = 0
		_Opacity("Opacity", Float) = 0
		[PerRendererData]_TotalLength("TotalLength", Float) = 0
		_ArrowWidth("ArrowWidth", Float) = 0
		_Offset("Offset", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _Offset;
		uniform float _TotalLength;
		uniform float _ArrowWidth;
		uniform float _ArrowGap;
		uniform float4 _LineColour;
		uniform float4 _Color0;
		uniform float _Opacity;

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_TexCoord31 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float temp_output_37_0 = abs( ( uv_TexCoord31.y - 0.5 ) );
			float temp_output_13_0 = ( _ArrowWidth + _ArrowGap );
			float4 ifLocalVar25 = 0;
			if( fmod( ( ( ( uv_TexCoord31.x + (_Offset / _TotalLength) ) + ( ( temp_output_37_0 / _TotalLength ) * -0.25 ) ) * _TotalLength ) , temp_output_13_0 ) <= _ArrowWidth )
				ifLocalVar25 = _Color0;
			else
				ifLocalVar25 = _LineColour;
			o.Emission = ifLocalVar25.rgb;
			o.Alpha = ( ifLocalVar25.a * _Opacity * ( 1.0 - ( temp_output_37_0 ) ) );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit alpha:fade keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 texcoords01 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.texcoords01 = float4( v.texcoord.xy, v.texcoord1.xy );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : SV_POSITION
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord.xy = IN.texcoords01.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
-1921;54;1906;1004;1836.466;750.7646;1.482853;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;31;-1412.205,-463.9834;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;38;-1232.285,-271.7616;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.AbsOpNode;37;-1076.234,-253.4356;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;3;-1255.03,173.3643;Float;False;Property;_TotalLength;TotalLength;1;1;[PerRendererData];0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;40;-942.2167,-230.2929;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;44;-1415.06,-572.8225;Float;False;Property;_Offset;Offset;7;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-778.4628,-273.286;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;-0.25;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;43;-814.0513,-451.2287;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;11;-1203.716,-80.85713;Float;False;Property;_ArrowGap;ArrowGap;1;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;4;-1204.716,48.14287;Float;False;Property;_ArrowWidth;ArrowWidth;1;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;39;-574.0149,-405.9636;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;13;-873.5413,-86.29693;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-470.1709,-255.0212;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;1;-160.7238,-253.0438;Float;False;Property;_LineColour;LineColour;0;0;0,0.7103448,1,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.FmodOpNode;32;-454.3716,-20.92791;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;5;-173.4523,-442.4186;Float;False;Property;_Color0;Color 0;0;0;1,1,1,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-493.001,112.256;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;3.0;False;1;FLOAT
Node;AmplifyShaderEditor.ConditionalIfNode;25;83.04023,-184.6644;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;COLOR;0.0;False;3;COLOR;0.0;False;4;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;2;84,238;Float;False;Property;_Opacity;Opacity;1;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;51;-270.1244,141.9131;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.BreakToComponentsNode;50;274.4658,-70.13536;Float;False;COLOR;1;0;COLOR;0.0;False;16;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;35;-742.109,24.53427;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;45;-1039.826,-571.3395;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;6;114,337;Float;False;Property;_CurrentPos;CurrentPos;5;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;444.582,91.49574;Float;False;3;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;698.2271,-86.13438;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Custom/Line;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;0;0;False;0;0;Transparent;0.5;True;True;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;14;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;38;0;31;2
WireConnection;37;0;38;0
WireConnection;40;0;37;0
WireConnection;40;1;3;0
WireConnection;42;0;40;0
WireConnection;43;0;31;1
WireConnection;43;1;44;0
WireConnection;39;0;43;0
WireConnection;39;1;42;0
WireConnection;13;0;4;0
WireConnection;13;1;11;0
WireConnection;34;0;39;0
WireConnection;34;1;3;0
WireConnection;32;0;34;0
WireConnection;32;1;13;0
WireConnection;52;0;37;0
WireConnection;25;0;32;0
WireConnection;25;1;4;0
WireConnection;25;2;1;0
WireConnection;25;3;5;0
WireConnection;25;4;5;0
WireConnection;51;0;52;0
WireConnection;50;0;25;0
WireConnection;35;0;3;0
WireConnection;35;1;13;0
WireConnection;45;0;44;0
WireConnection;45;1;3;0
WireConnection;47;0;50;3
WireConnection;47;1;2;0
WireConnection;47;2;51;0
WireConnection;0;2;25;0
WireConnection;0;9;47;0
ASEEND*/
//CHKSM=DAA73A2B00A169434B4096CA60F36F993AE95FEF