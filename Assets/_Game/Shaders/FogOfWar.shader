// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/Fog"
{
	Properties
	{
		_FogColour("FogColour", Color) = (0,0,0,1)
		_Revealer("Revealer", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _FogColour;
		uniform float4 _Revealer;

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Emission = _FogColour.rgb;
			float4 appendResult5 = (float4(_Revealer.x , _Revealer.y , _Revealer.z , 0.0));
			float2 uv_TexCoord13 = (i.uv_texcoord - float2(0.5, 0.5)) * float2( 1,1 ) + float2(0.5, 0.5);
			float temp_output_4_0 = distance( appendResult5 , float4( uv_TexCoord13, 0.0 , 0.0 ) );
			float temp_output_48_0 = ( _Revealer.w * 0.875 );
			float temp_output_59_0 = ( 1.0 - ( ( _Revealer.w - temp_output_4_0 ) / ( _Revealer.w - temp_output_48_0 ) ) );
			float ifLocalVar47 = 0;
			if( temp_output_4_0 <= _Revealer.w )
				ifLocalVar47 = temp_output_59_0;
			else
				ifLocalVar47 = 1.0;
			float ifLocalVar25 = 0;
			if( temp_output_4_0 <= temp_output_48_0 )
				ifLocalVar25 = 0.0;
			else
				ifLocalVar25 = ifLocalVar47;
			o.Alpha = ( _FogColour.a * ifLocalVar25 );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit alpha:fade keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 texcoords01 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.texcoords01 = float4( v.texcoord.xy, v.texcoord1.xy );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : SV_POSITION
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord.xy = IN.texcoords01.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}

/*ASEBEGIN
Version=13801
-1913;29;1906;1004;473.5764;398.2982;1;True;True
Node;AmplifyShaderEditor.Vector4Node;3;-913,-138;Float;False;Property;_Revealer;Revealer;2;0;0,0,0,0;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;13;-778,168;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.DynamicAppendNode;5;-631,-71;Float;False;FLOAT4;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.DistanceOpNode;4;-384,68;Float;False;2;0;FLOAT4;0,0,0;False;1;FLOAT2;0,0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-472.5764,334.7018;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.875;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;58;-183.5764,468.7018;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;54;-181.5764,352.7018;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;57;-19.57642,435.7018;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;59;115.4236,435.7018;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;10;-115,123;Float;False;Constant;_Float1;Float 1;4;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ConditionalIfNode;47;305.4236,319.7018;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;9;75,146;Float;False;Constant;_Float0;Float 0;4;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;60;186.4236,-301.2982;Float;False;Property;_FogColour;FogColour;1;0;0,0,0,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ConditionalIfNode;25;382.4236,73.70181;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;605.4236,228.7018;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;824,52;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Custom/Fog;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;0;0;False;0;0;Transparent;0.5;True;True;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;14;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;5;0;3;1
WireConnection;5;1;3;2
WireConnection;5;2;3;3
WireConnection;4;0;5;0
WireConnection;4;1;13;0
WireConnection;48;0;3;4
WireConnection;58;0;3;4
WireConnection;58;1;48;0
WireConnection;54;0;3;4
WireConnection;54;1;4;0
WireConnection;57;0;54;0
WireConnection;57;1;58;0
WireConnection;59;0;57;0
WireConnection;47;0;4;0
WireConnection;47;1;3;4
WireConnection;47;2;10;0
WireConnection;47;3;59;0
WireConnection;47;4;59;0
WireConnection;25;0;4;0
WireConnection;25;1;48;0
WireConnection;25;2;47;0
WireConnection;25;3;9;0
WireConnection;25;4;9;0
WireConnection;61;0;60;4
WireConnection;61;1;25;0
WireConnection;0;2;60;0
WireConnection;0;9;61;0
ASEEND*/
//CHKSM=6CA3612CDB9E3785F9856BAFC2B258DC32BF97BC