// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/Lazer"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_CoreWidth("CoreWidth", Range( 0 , 1)) = 0.5
		_OuterColour("OuterColour", Color) = (0.1607843,0.8000001,0,1)
		_CoreColour("CoreColour", Color) = (1,1,1,1)
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode" = "ForwardBase" }
		LOD 100
		Cull Off
		


		Pass
		{
			CGPROGRAM
			#pragma target 3.0 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			


			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 texcoord : TEXCOORD0;
			};

			uniform sampler2D _MainTex;
			uniform fixed4 _Color;
			uniform float _CoreWidth;
 uniform float4 _OuterColour;
 uniform float4 _CoreColour;
			
			v2f vert ( appdata v )
			{
				v2f o;
				o.texcoord.xy = v.texcoord.xy;
				o.texcoord.zw = v.texcoord1.xy;
				
				// ase common template code
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				fixed4 myColorVar;
				// ase common template code
				float2 uv6 = i.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float temp_output_3_0 = ( _CoreWidth / 2.0 );
				float4 ifLocalVar5 = 0;
				if( uv6.y <= temp_output_3_0 )
				ifLocalVar5 = _OuterColour;
				else
				ifLocalVar5 = _CoreColour;
				float4 ifLocalVar9 = 0;
				if( uv6.y <= ( 1.0 - temp_output_3_0 ) )
				ifLocalVar9 = ifLocalVar5;
				else
				ifLocalVar9 = _OuterColour;
				
				
				myColorVar = ifLocalVar9;
				return myColorVar;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
-1913;29;1906;1004;860;497;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;1;-595,-268;Float;False;Property;_CoreWidth;CoreWidth;0;0;0.5;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;3;-269,-222;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;2.0;False;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;-406,-476;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;7;-285,-7;Float;False;Property;_CoreColour;CoreColour;1;0;1,1,1,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;8;-230,184;Float;False;Property;_OuterColour;OuterColour;1;0;0.1607843,0.8000001,0,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;2;16,-203;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ConditionalIfNode;5;216,72;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;COLOR;0.0;False;3;COLOR;0.0;False;4;COLOR;0.0;False;1;COLOR
Node;AmplifyShaderEditor.ConditionalIfNode;9;426,-112;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.TemplateMasterNode;0;616,-13;Float;False;True;2;Float;ASEMaterialInspector;0;2;Custom/Lazer;6e114a916ca3e4b4bb51972669d463bf;ASETemplateShaders/DefaultUnlit;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;3;0;1;0
WireConnection;2;0;3;0
WireConnection;5;0;6;2
WireConnection;5;1;3;0
WireConnection;5;2;7;0
WireConnection;5;3;8;0
WireConnection;5;4;8;0
WireConnection;9;0;6;2
WireConnection;9;1;2;0
WireConnection;9;2;8;0
WireConnection;9;3;5;0
WireConnection;9;4;5;0
WireConnection;0;0;9;0
ASEEND*/
//CHKSM=2440A89C366A57F59F8A3C3678FFFF952CA91F57