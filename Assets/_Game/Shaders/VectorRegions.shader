Shader "Custom/VectorGradientRegion"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        _VisibilityTexture("Visibility Texture", 2D) = "white" {}
        _ColourMapTexture("Colour Map Texture", 2D) = "white" {}
        _CommonColor("Common Colour", Color) = (0.5,0.5,0.5,1)
        _UncommonColor("Uncommon Colour", Color) = (0,0.333,1,1)
        _RareColor("Rare Colour", Color) = (.75,0.25,0.5,1)
        _EpicColor("Epic Colour", Color) = (.75,0.25,0.25,1)
        _LegendColor("Legendary Colour", Color) = (.667,0.5,0,1)
        _AsteroidContents("Contents", Vector) = (0.25, 0.125, 0.1, 0.025) 
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "PreviewType" = "Plane"
        }
        LOD 100

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex GradientVert
            #pragma fragment GradientFrag
            #pragma multi_compile_instancing

            #include "UnityCG.cginc"
            #include "StandaloneVectorGradient.cginc"

            #ifdef UNITY_INSTANCING_ENABLED
            UNITY_INSTANCING_BUFFER_START(PerDrawSprite)
                UNITY_DEFINE_INSTANCED_PROP(fixed4, unity_SpriteRendererColorArray)
            UNITY_INSTANCING_BUFFER_END(PerDrawSprite)
            #define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
            #endif

            #ifndef UNITY_INSTANCING_ENABLED
            fixed4 _RendererColor;
            #endif

            struct appdata
            {
                float4 vertex : POSITION;
                fixed4 color : COLOR;
                float2 uv : TEXCOORD0;
                float2 settingIndex : TEXCOORD2;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                fixed4 color : COLOR;
                float2 uv : TEXCOORD0; // uv.z is used for setting index
                float2 settingIndex : TEXCOORD2;
                float4 vertex : SV_POSITION;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            float3 HSVToRGB(float3 c)
			{
				float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
				float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
				return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
			};

			float3 RGBToHSV(float3 c)
			{
				float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
				float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
				float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));
				float d = q.x - min(q.w, q.y);
				float e = 1.0e-10;
				return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
			};

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_TexelSize;
            fixed4 _Color;

             sampler2D _VisibilityTexture;
             sampler2D _ColourMapTexture;
             fixed4 _CommonColor;
             fixed4 _UncommonColor;
             fixed4 _RareColor;
             fixed4 _EpicColor;
             fixed4 _LegendColor;
             float4 _AsteroidContents;

            float Clamp01(float val)
            {
                return clamp(val, 0, 1);
            }
            
            v2f GradientVert (appdata IN)
            {
                v2f OUT;

                UNITY_SETUP_INSTANCE_ID (IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                #ifdef UNITY_COLORSPACE_GAMMA
                OUT.color = IN.color;
                #else
                OUT.color = fixed4(GammaToLinearSpace(IN.color.rgb), IN.color.a);
                #endif
                OUT.color *= _Color * _RendererColor;
                OUT.uv = TRANSFORM_TEX(IN.uv, _MainTex);
                OUT.settingIndex = IN.settingIndex;
                return OUT;
            }

            fixed4 GradientFrag (v2f i) : SV_Target
            {
                float2 texCoord = i.vertex.xy / 2500;
                fixed4 gradColor = EvaluateGradient(i.settingIndex.x, i.uv, _MainTex, _MainTex_TexelSize.xy);
                
                #ifndef UNITY_COLORSPACE_GAMMA
                gradColor = fixed4(GammaToLinearSpace(gradColor.rgb), gradColor.a);
                #endif

                fixed4 finalColor = gradColor * i.color;
                finalColor.rgb *= finalColor.a;

                float totalContents = _AsteroidContents.x + _AsteroidContents.y + _AsteroidContents.z + _AsteroidContents.w;
                float commonContents = 1 - totalContents;

                float overallLerp = tex2D(_ColourMapTexture, texCoord).r;
                float visibilityLerp = tex2D(_VisibilityTexture, texCoord).r;
                
                float4 displayColour = _CommonColor;

                float totalProcessed = commonContents;
                float totalAfter = totalProcessed;

                if (_AsteroidContents.x > 0)
                {
                    totalAfter += _AsteroidContents.x;
                    displayColour = lerp(displayColour, _UncommonColor, Clamp01((overallLerp - totalProcessed) / _AsteroidContents.x));
                    totalProcessed = totalAfter;
                }
                if (_AsteroidContents.y > 0)
                {
                    totalAfter += _AsteroidContents.y;
                    displayColour = lerp(displayColour, _RareColor, Clamp01((overallLerp - totalProcessed) / _AsteroidContents.y));
                    totalProcessed = totalAfter;
                }
               if (_AsteroidContents.z > 0)
                {
                    totalAfter += _AsteroidContents.z;
                    displayColour = lerp(displayColour, _EpicColor, Clamp01((overallLerp - totalProcessed) / _AsteroidContents.z));
                    totalProcessed = totalAfter;
                }
                if (_AsteroidContents.w > 0)
                {
                    totalAfter += _AsteroidContents.w;
                    displayColour = lerp(displayColour, _LegendColor, Clamp01((overallLerp - totalProcessed) / _AsteroidContents.w));
                    totalProcessed = totalAfter;
                }

                float a = finalColor.a;
                displayColour.a = a;
                float3 finalHSV = RGBToHSV(float3(finalColor.x, finalColor.y, finalColor.z));
                float3 displayHSV = RGBToHSV(float3(displayColour.x, displayColour.y, displayColour.z));
                finalHSV.x = displayHSV.x;
                finalHSV.y *= displayHSV.y;

                float3 hueShifted = HSVToRGB(float3(displayHSV.x, displayHSV.y, finalHSV.z * displayHSV.z));
                fixed4 hueShiftedRGBA = fixed4(hueShifted.x, hueShifted.y, hueShifted.z, a);
                finalColor = lerp(finalColor, hueShiftedRGBA, visibilityLerp);
                //finalColor = hueShiftedRGBA;
                
                return finalColor;
            }
            ENDCG
        }
    }
}
