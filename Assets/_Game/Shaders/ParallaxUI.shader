// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/SpaceUI"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_Background("Background", 2D) = "white" {}
		_CameraSize("CameraSize", Float) = 5
		_Position("Position", Vector) = (0,0,0,0)
		_Foreground("Foreground", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_BaseColour("BaseColour", Color) = (0.02692698,0,0.06617647,1)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
			
		}
		
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		

		Pass
		{
			Name "Default"
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _Background;
 uniform float _CameraSize;
 uniform float2 _Position;
 uniform sampler2D _Foreground;
 uniform sampler2D _TextureSample0;
 uniform float4 _BaseColour;
			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				OUT.worldPosition = IN.vertex;
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				#ifdef UNITY_HALF_TEXEL_OFFSET
				OUT.vertex.xy += (_ScreenParams.zw-1.0) * float2(-1,1) * OUT.vertex.w;
				#endif
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				float4 _Color0 = float4(1,1,1,1);
				float2 temp_cast_0 = (( 2.5 * _CameraSize )).xx;
				float2 uv21 = IN.texcoord.xy * temp_cast_0 + ( _Position * 0.175 );
				float2 temp_cast_1 = (( 1.0 * _CameraSize )).xx;
				float2 uv17 = IN.texcoord.xy * temp_cast_1 + ( _Position * 0.225 );
				float2 temp_cast_2 = (( 1.75 * _CameraSize )).xx;
				float2 uv31 = IN.texcoord.xy * temp_cast_2 + ( _Position * 0.2 );
				
				half4 color = ( ( _Color0 * tex2D( _Background, uv21 ).r ) + ( _Color0 * tex2D( _Foreground, uv17 ).r ) + ( _Color0 * tex2D( _TextureSample0, uv31 ).r ) + _BaseColour );
				
				color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
-1913;29;1906;1004;2352.387;711.2946;1.3;True;True
Node;AmplifyShaderEditor.RangedFloatNode;37;-1982.192,-323.8945;Float;False;Property;_CameraSize;CameraSize;5;0;5;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;27;-1580,-622;Float;False;Constant;_Float2;Float 2;3;0;2.5;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;33;-1580.124,89.28036;Float;False;Constant;_Float4;Float 4;3;0;1.75;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;42;-1505.586,-291.3945;Float;False;Constant;_Float5;Float 5;6;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;24;-1795,-560;Float;False;Constant;_Float0;Float 0;2;0;0.175;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;25;-1782.5,-16;Float;False;Constant;_Float1;Float 1;2;0;0.225;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;29;-1787.853,273.9847;Float;False;Constant;_Float3;Float 3;2;0;0.2;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.Vector2Node;22;-1814.601,-158.7003;Float;False;Property;_Position;Position;2;0;0,0;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1494,-466;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;49;-1357.887,107.7054;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-1531.654,268.685;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1492.501,-101.8999;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-1350.087,-615.0945;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;-1359.188,-236.7945;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;21;-1032.967,-592.833;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;31;-1044.154,157.3852;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;17;-1041.4,-146.9;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;32;-664.3533,200.9849;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;2;-659,-89;Float;True;Property;_Foreground;Foreground;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-758,-356;Float;True;Property;_Background;Background;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;3;-727,-610;Float;False;Constant;_Color0;Color 0;2;0;1,1,1,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;-239,-130;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-213.1997,182.1212;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-258,-376;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ColorNode;36;-55.57756,257.2054;Float;False;Property;_BaseColour;BaseColour;4;0;0.02692698,0,0.06617647,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;6;235.012,-204.0646;Float;False;4;4;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.TemplateMasterNode;35;618.799,-186.5999;Float;False;True;2;Float;ASEMaterialInspector;0;3;Custom/SpaceUI;5056123faa0c79b47ab6ad7e8bf059a4;ASETemplateShaders/UIDefault;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;23;0;22;0
WireConnection;23;1;24;0
WireConnection;49;0;33;0
WireConnection;49;1;37;0
WireConnection;30;0;22;0
WireConnection;30;1;29;0
WireConnection;26;0;22;0
WireConnection;26;1;25;0
WireConnection;45;0;27;0
WireConnection;45;1;37;0
WireConnection;47;0;42;0
WireConnection;47;1;37;0
WireConnection;21;0;45;0
WireConnection;21;1;23;0
WireConnection;31;0;49;0
WireConnection;31;1;30;0
WireConnection;17;0;47;0
WireConnection;17;1;26;0
WireConnection;32;1;31;0
WireConnection;2;1;17;0
WireConnection;1;1;21;0
WireConnection;5;0;3;0
WireConnection;5;1;2;1
WireConnection;34;0;3;0
WireConnection;34;1;32;1
WireConnection;4;0;3;0
WireConnection;4;1;1;1
WireConnection;6;0;4;0
WireConnection;6;1;5;0
WireConnection;6;2;34;0
WireConnection;6;3;36;0
WireConnection;35;0;6;0
ASEEND*/
//CHKSM=BF3BA995882C76717975B265317CEBBC9A91677E