// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/Space"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_Background("Background", 2D) = "white" {}
		_Position("Position", Vector) = (0,0,0,0)
		_Foreground("Foreground", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode" = "ForwardBase" }
		LOD 100
		Cull Off
		


		Pass
		{
			CGPROGRAM
			#pragma target 3.0 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			


			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 texcoord : TEXCOORD0;
			};

			uniform sampler2D _MainTex;
			uniform fixed4 _Color;
			uniform sampler2D _Background;
 uniform float2 _Position;
 uniform sampler2D _Foreground;
 uniform sampler2D _TextureSample0;
			
			v2f vert ( appdata v )
			{
				v2f o;
				o.texcoord.xy = v.texcoord.xy;
				o.texcoord.zw = v.texcoord1.xy;
				
				// ase common template code
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				fixed4 myColorVar;
				// ase common template code
				float4 _Color0 = float4(1,1,1,1);
				float2 temp_cast_0 = (2.5).xx;
				float2 uv21 = i.texcoord.xy * temp_cast_0 + ( _Position * 0.0175 );
				float2 uv17 = i.texcoord.xy * float2( 1,1 ) + ( _Position * 0.0225 );
				float2 temp_cast_1 = (1.75).xx;
				float2 uv31 = i.texcoord.xy * temp_cast_1 + ( _Position * 0.02 );
				
				
				myColorVar = ( ( _Color0 * tex2D( _Background, uv21 ).r ) + ( _Color0 * tex2D( _Foreground, uv17 ).r ) + ( _Color0 * tex2D( _TextureSample0, uv31 ).r ) );
				return myColorVar;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
-1913;29;1906;1004;2848.182;603.3951;1.3;True;True
Node;AmplifyShaderEditor.RangedFloatNode;25;-1782.5,-16;Float;False;Constant;_Float1;Float 1;2;0;0.225;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.Vector2Node;22;-1737.901,-140.5003;Float;False;Property;_Position;Position;2;0;0,0;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;24;-1795,-560;Float;False;Constant;_Float0;Float 0;2;0;0.175;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;29;-1787.853,273.9847;Float;False;Constant;_Float3;Float 3;2;0;0.2;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-1458.853,165.9851;Float;False;2;2;0;FLOAT2;0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1453.5,-124;Float;False;2;2;0;FLOAT2;0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.RangedFloatNode;27;-1580,-622;Float;False;Constant;_Float2;Float 2;3;0;2.5;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;33;-1433.223,74.98035;Float;False;Constant;_Float4;Float 4;3;0;1.75;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1494,-466;Float;False;2;2;0;FLOAT2;0.0;False;1;FLOAT;0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.TextureCoordinatesNode;31;-1179.354,146.9852;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;17;-1174,-143;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;21;-1200.667,-560.3333;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;32;-664.3533,200.9849;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;3;-727,-610;Float;False;Constant;_Color0;Color 0;2;0;1,1,1,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-758,-356;Float;True;Property;_Background;Background;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;2;-659,-89;Float;True;Property;_Foreground;Foreground;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;-239,-130;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-213.1997,182.1212;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-258,-376;Float;False;2;2;0;COLOR;0.0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleAddOpNode;6;75.11182,-167.6645;Float;False;3;3;0;COLOR;0.0;False;1;COLOR;0.0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.TemplateMasterNode;0;351,-249;Float;False;True;2;Float;ASEMaterialInspector;0;2;Custom/Space;6e114a916ca3e4b4bb51972669d463bf;ASETemplateShaders/DefaultUnlit;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;30;0;22;0
WireConnection;30;1;29;0
WireConnection;26;0;22;0
WireConnection;26;1;25;0
WireConnection;23;0;22;0
WireConnection;23;1;24;0
WireConnection;31;0;33;0
WireConnection;31;1;30;0
WireConnection;17;1;26;0
WireConnection;21;0;27;0
WireConnection;21;1;23;0
WireConnection;32;1;31;0
WireConnection;1;1;21;0
WireConnection;2;1;17;0
WireConnection;5;0;3;0
WireConnection;5;1;2;1
WireConnection;34;0;3;0
WireConnection;34;1;32;1
WireConnection;4;0;3;0
WireConnection;4;1;1;1
WireConnection;6;0;4;0
WireConnection;6;1;5;0
WireConnection;6;2;34;0
WireConnection;0;0;6;0
ASEEND*/
//CHKSM=29F29B375FF68D622D68A0190FECF3A7A90EC1F2