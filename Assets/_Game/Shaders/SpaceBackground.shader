// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/SpaceUI"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_Background("Background", 2D) = "white" {}
		_CameraSize("CameraSize", Float) = 5
		_Position("Position", Vector) = (0,0,0,0)
		_Foreground("Foreground", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		
		[PerRendererData] _BaseColour("BaseColour", Color) = (0,0,0,1)
		
		_NebulaTexture("Nebula Texture", 2D) = "white" {}
		_NebulaColour0("Nebula Color 0", Color) = (0.09534144,1,0,1)
		_NebulaColour1("Nebula Color 1", Color) = (0.5265188,0,1,1)
		_PrimaryOffset("Nebula Offset", Vector) = (0,0,0,0)
		_NebulaMoveMult("Nebula Offset Mult",Float) = 0.008
		_NebulaGlow("Nebula Glow",Float) = 0.667
		
		[Toggle(NEBULA_EFFECT)]_NebulaEffect("Nebula Effect", Float) = 0
		[PerRendererData]_NebulaEffectOffset("Nebula Effect Offset",Float)=0
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode" = "ForwardBase" }
		LOD 100
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma target 3.0 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			


			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 texcoord : TEXCOORD0;
			};

			uniform sampler2D _MainTex;
			uniform fixed4 _Color;
			uniform sampler2D _Background;
 uniform float _CameraSize;
 uniform float2 _Position;
 uniform float2 _PrimaryOffset;
 uniform sampler2D _Foreground;
 uniform sampler2D _TextureSample0;
 uniform sampler2D _NebulaTexture;
 uniform float4 _BaseColour;
 uniform float4 _NebulaColour0;
 uniform float4 _NebulaColour1;
			float _NebulaMoveMult;
			float _NebulaEffect;
			float _NebulaEffectOffset;
			float _NebulaGlow;
			
			v2f vert ( appdata v )
			{
				v2f o;
				o.texcoord.xy = v.texcoord.xy;
				o.texcoord.zw = v.texcoord1.xy;
				
				// ase common template code
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				fixed4 myColorVar;
				// ase common template code
				float2 texCoord = i.texcoord.xy;
				float2 scaleCenter = float2(0.5, 0.5);
				float4 _Color0 = float4(1,1,1,1);
				float2 temp_cast_0 = (( 4 * _CameraSize )*2).xx;
				float2 uv21 = ((texCoord - scaleCenter) * temp_cast_0 + ((_Position) * 0.0175) + scaleCenter);
				//uv21 = (uv21 - scaleCenter) * temp_cast_0;
				float2 temp_cast_1 = (( 2 * _CameraSize )*2).xx;
				float2 uv17 = ((texCoord - scaleCenter) * temp_cast_1 + ((_Position) * 0.0225) + scaleCenter);// / _CameraSize;
				//uv17 = uv17 - (uv17 / 2);
				float2 temp_cast_2 = (( 3 * _CameraSize )*2).xx;
				float2 uv31 = ((texCoord - scaleCenter) * temp_cast_2 + ((_Position) * 0.02) + scaleCenter);// / _CameraSize;
				//uv31 = uv31 - (uv31 / 2);

				float2 temp_cast_3 = (( 0.8 * _CameraSize )).xx;
				float2 temp_cast_4 = (( 0.1 * _CameraSize )).xx;
				float2 temp_cast_5 = (( 0.5 * _CameraSize )).xx;
				float2 temp_cast_6 = (( 0.25 * _CameraSize )).xx;
				float2 uv32 = ((texCoord - scaleCenter) * temp_cast_3 + ((_Position) * 0.8 * _NebulaMoveMult) + scaleCenter) + (_PrimaryOffset);
				float2 uv33 = ((texCoord - scaleCenter) * temp_cast_4 + ((_Position) * 0.1 * _NebulaMoveMult) + scaleCenter)  + (_PrimaryOffset * 0.5);
				float2 uv34 = ((texCoord - scaleCenter) * temp_cast_5 + ((_Position) * 0.5 * _NebulaMoveMult) + scaleCenter)  + (_PrimaryOffset * 0.25);
				float2 uv35 = ((texCoord - scaleCenter) * temp_cast_6 + ((_Position) * 0.25 * _NebulaMoveMult) + scaleCenter)  + (_PrimaryOffset * 0.125);
				
				//myColorVar = ( ( _Color0 * tex2D( _Background, uv21 ).r ) + ( _Color0 * tex2D( _Foreground, uv17 ).r ) + ( _Color0 * tex2D( _TextureSample0, uv31 ).r ) + _BaseColour );
				myColorVar = ( ( _Color0 * tex2D( _Background, uv21 ).r ) + ( _Color0 * tex2D( _TextureSample0, uv31 ).r ) + _BaseColour );

				float4 lerpResult12 = lerp( _NebulaColour0 , _NebulaColour1 , ( tex2D( _NebulaTexture, uv34 ) * tex2D( _NebulaTexture, uv35 ) ));
				float4 alphaThing = tex2D( _NebulaTexture, uv32 );
				alphaThing *= tex2D( _NebulaTexture, uv33 );
				if (_NebulaEffect <= 0)
				{
					myColorVar += lerpResult12 * alphaThing.r * _NebulaGlow;
					return myColorVar;
				}

				float2 temp_cast_7 = ((_CameraSize )).xx;

				float2 nebulaOffset = float2(_NebulaEffectOffset, _NebulaEffectOffset);
				
				float2 uv36 = ((texCoord - scaleCenter) * temp_cast_7 + ((_Position) * _NebulaMoveMult) + scaleCenter)  + (nebulaOffset * 0.9);
				float2 uv37 = ((texCoord - scaleCenter) * temp_cast_7 + ((_Position) * _NebulaMoveMult) + scaleCenter) - (nebulaOffset * 0.05);
				float2 uv38 = ((texCoord - scaleCenter) * temp_cast_7 + ((_Position) * _NebulaMoveMult) + scaleCenter);

				myColorVar += lerpResult12 * alphaThing.r * _NebulaGlow;
				myColorVar += alphaThing.r * tex2D( _NebulaTexture, uv36 ) * tex2D( _NebulaTexture, uv37 ) * tex2D( _NebulaTexture, uv38 ) * 0.1;
				return myColorVar;
			}
			ENDCG
		}
	}

}