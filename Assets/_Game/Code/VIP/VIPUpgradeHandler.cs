using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.VIP;
using UnityEngine;
using UnityEngine.Events;

public class VIPUpgradeHandler : MonoBehaviour
{
    [SerializeField]
    private VIPCategory category;

    [SerializeField]
    private UnityEvent<double> onSetValue;
    
    private void Start()
    {
        onSetValue?.Invoke(VIPManager.Instance.GetValue(category));
        VIPManager.Instance.onSetUpgradeLevel += OnCategoryUpdate;
    }

    private void OnCategoryUpdate(VIPCategory changedCategory, double value)
    {
        if (category != changedCategory)
            return;
        onSetValue?.Invoke(value);
    }
}
