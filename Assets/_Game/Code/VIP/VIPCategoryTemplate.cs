using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.VIP
{
    [Serializable]
    public class VIPCategoryTemplate
    {
        public VIPCategory category;
        public double[] costs = new double[10];
        public double[] values = new double[10];
        public string name, format, nextLevelFormat;
        public Text description, cost, nextLevel;
        public Button purchaseButton;
        public int currentLevel = 0;

        [SerializeField]
        private Color categoryColour;

        public Color CategoryColour => categoryColour;

        public bool MaxLevel
        {
            get { return currentLevel >= values.Length - 1; }
        }

        public double GetCost()
        {
            if (MaxLevel)
                return double.MaxValue;
            return costs[currentLevel];
        }

        public double GetValue()
        {
            if (MaxLevel)
                return values[values.Length - 1];
            return values[currentLevel];
        }

        public double GetNextValue()
        {
            if (MaxLevel)
                return values[values.Length - 1];
            return values[currentLevel + 1];
        }

        private string FormatNumber(double value)
        {
            if (value < 1000)
                return value.ToString();
            return CurrencyController.FormatToString(value);
        }

        public string GetDescription()
        {
            return string.Format(format, FormatNumber(GetValue()));
        }

        public string GetNextLevelText()
        {
            if (MaxLevel)
                return "";
            return string.Format(nextLevelFormat, FormatNumber(GetNextValue()));
        }

        public string GetCostText()
        {
            if (MaxLevel)
                return "MAX";
            return CurrencyController.FormatToString(GetCost());
        }

        public void Setup(double currentPoints)
        {
            description.text = string.Format(format, FormatNumber(GetValue()));
            if (MaxLevel)
            {
                nextLevel.text = "";
                cost.text = "MAX";
            }
            else
            {
                nextLevel.text = string.Format(nextLevelFormat, FormatNumber(GetNextValue()));
                cost.text = CurrencyController.FormatToString(GetCost());
            }

            purchaseButton.interactable = !MaxLevel && GetCost() <= currentPoints;
        }

        public bool GetPurchasable(double currentPoints)
        {
            return !MaxLevel && GetCost() <= currentPoints;
        }
    }
}