﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.VIP;
using UnityEngine;
using UnityEngine.UI;

public class VIPSlot : MonoBehaviour
{
    public VIPCategory category;
    public Text nameText, description, cost, nextLevel;
    public Button purchaseButton;

    [SerializeField]
    private Image nameBacking;

    public void Setup(VIPCategory vipCategory, double currentPoints)
    {
        category = vipCategory;
        VIPCategoryTemplate categoryTemplate = VIPManager.Instance.GetTemplate(category);
        nameText.text = categoryTemplate.name;
        description.text = categoryTemplate.GetDescription();
        cost.text = categoryTemplate.GetCostText();
        nextLevel.text = categoryTemplate.GetNextLevelText();
        purchaseButton.interactable = categoryTemplate.GetPurchasable(currentPoints);
        nameBacking.color = categoryTemplate.CategoryColour;
    }

    public void Refresh(double currentPoints)
    {
        VIPCategoryTemplate categoryTemplate = VIPManager.Instance.GetTemplate(category);
        description.text = categoryTemplate.GetDescription();
        cost.text = categoryTemplate.GetCostText();
        nextLevel.text = categoryTemplate.GetNextLevelText();
        purchaseButton.interactable = categoryTemplate.GetPurchasable(currentPoints);
    }

    public void Purchase()
    {
        VIPManager.Instance.Purchase(category);
    }
}
