﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Save;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.VIP
{
    public class VIPManager : MonoBehaviour, ISaveLoad
    {
        public VIPCategoryTemplate[] categoryTemplates;

        private Dictionary<VIPCategory, int> templateDictionary = new Dictionary<VIPCategory, int>();

        [SerializeField]
        private VIPSlot slotPrefab;

        [SerializeField]
        private RectTransform slotListParent;

        [SerializeField]
        private List<VIPSlot> slotDictionary = new List<VIPSlot>();

        [SerializeField]
        private UIScreen screen;

        [SerializeField]
        private Text currentVIPText;

        [SerializeField]
        private GameObject notificationIcon;

        [SerializeField]
        private Toggle notificationToggle;

        private bool showNotification;

        public delegate void VIPUpgrade(VIPCategory category, double value);

        public event VIPUpgrade onSetUpgradeLevel;

        private static VIPManager instance;

        public static VIPManager Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<VIPManager>();
                return instance;
            }
        }

        private double currentVIPPoints = 0;

        public void Purchase(int index)
        {
            double cost = categoryTemplates[index].GetCost();
            if (currentVIPPoints >= cost)
            {
                AudioManager.Play("Milestone", 0.75f);
                categoryTemplates[index].currentLevel++;
                currentVIPPoints -= cost;
                currentVIPText.text = CurrencyController.FormatToString(currentVIPPoints);
                onSetUpgradeLevel?.Invoke(categoryTemplates[index].category, categoryTemplates[index].GetValue());
                //for (int i = 0; i < categoryTemplates.Length; i++)
                //    categoryTemplates[i].Setup(currentVIPPoints);
                SetupSlots();
                notificationIcon.SetActive(GetVIPAvailable() && showNotification);
            }
        }

        public void Purchase(VIPCategory category)
        {
            if (templateDictionary.TryGetValue(category, out var value))
                Purchase(value);
        }

        public VIPCategoryTemplate GetTemplate(VIPCategory category)
        {
            return categoryTemplates[templateDictionary[category]];
        }

        public void AddVIPPoints(int count)
        {
            currentVIPPoints += count;
            StatTracker.Instance.IncrementStat("vipPoints", count);
            currentVIPText.text = CurrencyController.FormatToString(currentVIPPoints);
            notificationIcon.SetActive(GetVIPAvailable() && showNotification);
        }

        public void ToggleNotifications(bool show)
        {
            showNotification = show;
            notificationIcon.SetActive(GetVIPAvailable() && showNotification);
        }

        private void SetupSlots()
        {
            for (int i = 0; i < categoryTemplates.Length; i++)
            {
                if (i >= slotDictionary.Count)
                {
                    VIPSlot newSlot = Instantiate(slotPrefab, slotListParent);
                    newSlot.Setup(categoryTemplates[i].category, currentVIPPoints);
                    slotDictionary.Add(newSlot);
                }
                else
                    slotDictionary[i].Refresh(currentVIPPoints);
            }
        }

        public void OpenScreen()
        {
            currentVIPText.text = CurrencyController.FormatToString(currentVIPPoints);
            SetupSlots();
            screen.Open();
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            currentVIPPoints = loadedData.LoadData("vipPoints", 0d);
            for (int i = 0; i < categoryTemplates.Length; i++)
            {
                categoryTemplates[i].currentLevel = loadedData.LoadData("vip" + i, 0);
                onSetUpgradeLevel?.Invoke(categoryTemplates[i].category, categoryTemplates[i].GetValue());
                //categoryTemplates[i].Setup(currentVIPPoints);
            }

            showNotification = loadedData.LoadData("vipNotify", true);
            notificationToggle.isOn = showNotification;
            notificationIcon.SetActive(GetVIPAvailable() && showNotification);
        }

        private bool GetVIPAvailable()
        {
            for (int i = 0; i < categoryTemplates.Length; i++)
            {
                if (currentVIPPoints >= categoryTemplates[i].GetCost())
                    return true;
            }

            return false;
        }

        public void LoadDefault()
        {
            currentVIPPoints = 0;
            for (int i = 0; i < categoryTemplates.Length; i++)
            {
                categoryTemplates[i].currentLevel = 0;
                onSetUpgradeLevel?.Invoke(categoryTemplates[i].category, categoryTemplates[i].GetValue());
                //categoryTemplates[i].Setup(currentVIPPoints);
            }

            showNotification = true;
            notificationToggle.isOn = showNotification;
            notificationIcon.SetActive(GetVIPAvailable() && showNotification);
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("vipPoints", currentVIPPoints);
            for (int i = 0; i < categoryTemplates.Length; i++)
            {
                saveData.Add("vip" + i, categoryTemplates[i].currentLevel);
            }

            saveData.Add("vipNotify", showNotification);
        }

        public double GetValue(VIPCategory category)
        {
            if (templateDictionary.ContainsKey(category))
                return categoryTemplates[templateDictionary[category]].GetValue();
            return 0;
        }

        // Start is called before the first frame update
        private void Awake()
        {
            instance = this;
            for (int i = 0; i < categoryTemplates.Length; i++)
            {
                templateDictionary.Add(categoryTemplates[i].category, i);
            }
        }
    }

}