namespace Core.Gameplay.VIP
{
    public enum VIPCategory
    {
        OfflineEarnings,
        BoostTime,
        Diamonds,
        Debris,
    }
}