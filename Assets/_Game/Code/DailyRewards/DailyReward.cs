﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Save;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Fumb.LocalNotifications;

namespace Core.Gameplay.DailyRewards
{
    public class DailyReward : MonoBehaviour, INotify, ISaveLoad
    {

        public DailyRewardItem[] allRewards;

        private int currentDay, lastClaimedDay = -1;

        private DateTime lastClaimed;

        [SerializeField]
        private RectTransform gridParent;

        [SerializeField]
        private DailyRewardTile rewardTilePrefab;

        private List<DailyRewardTile> allTiles = new List<DailyRewardTile>();

        [SerializeField]
        private GameObject popupClaim;

        [SerializeField]
        private UIScreen popup;

        [SerializeField]
        private Text dayText, popupDayText, popupTitle, popupDesc;

        [SerializeField]
        private Image popupImage;

        [SerializeField]
        private Button claimButton;

        private int selectedIndex = 0;

        public GameObject notificationSymbol;

        [SerializeField]
        private UIScreen screen;

        private void LoadRewardData()
        {
            int daysPassed = DaysPassed(lastClaimed);
            if (daysPassed <= 0)
            {
                currentDay = lastClaimedDay < 0 ? 0 : lastClaimedDay;
            }
            else if (daysPassed <= 2)
            {
                if (lastClaimedDay >= allRewards.Length - 1)
                    lastClaimedDay = -1;
                currentDay = lastClaimedDay + 1;
            }
            else
            {
                currentDay = 0;
                lastClaimedDay = -1;
            }

            claimButton.interactable = currentDay > lastClaimedDay;
            dayText.text = string.Format("DAY {0}", (currentDay + 1));
        }

        private int currentIndex = 0;

        public void OpenPopup(int index)
        {
            //AudioManager.Play("Click");
            //popupObject.SetActive(true);
            popup.Open();
            currentIndex = index;
            popupDayText.text = string.Format("DAY {0}", (index + 1));
            popupClaim.SetActive(currentDay == index && index == lastClaimedDay + 1);
            popupTitle.text = FormatTitle(allRewards[index]);
            popupDesc.text = allRewards[index].formatDescription ?
                string.Format((allRewards[index].description), allRewards[index].format) :
                (allRewards[index].description);
            popupImage.sprite = allRewards[index].sprite;
        }

        private string FormatTitle(DailyRewardItem rewardItem)
        {
            string core = rewardItem.localiseTitle ? (rewardItem.title) : rewardItem.title;
            if (rewardItem.formatTitle)
                return string.Format(core, rewardItem.titleFormat);
            return core;
        }

        public void ClosePopup()
        {
            //AudioManager.Play("Click");
            //popupObject.SetActive(false);
            popup.Close();
        }

        public void ClaimToday()
        {
            //AudioManager.Instance.PlayClip("Click");
            //Claim(currentDay);
            OpenPopup(currentDay);
        }

        public void ClaimSelected()
        {
            if (Claim(currentIndex))
                ClosePopup();
        }

        private int DaysPassed(DateTime from)
        {
            DateTime today = DateTime.Today;
            TimeSpan timeSpan = today - from;

            return (int) timeSpan.TotalDays;
        }

        public bool Claim(int index)
        {
            if (index == lastClaimedDay + 1 && currentDay == index)
            {
                allRewards[index].Claim();
                lastClaimedDay = index;
                lastClaimed = DateTime.Today;
                //SaveRewardData();
                RefreshTiles();
                SetNotification();
                return true;
            }

            return false;
        }

        public void OpenMenu()
        {
            //gameObject.SetActive(true);
            screen.Open();
            //LoadRewardData();
            if (!initialised)
            {
                for (int i = 0; i < allRewards.Length; i++)
                {
                    DailyRewardTile newTile = Instantiate(rewardTilePrefab, gridParent);
                    newTile.Initialise(allRewards[i].sprite, FormatTitle(allRewards[i]), currentDay == i,
                        lastClaimedDay >= i, i, this);
                    allTiles.Add(newTile);
                }

                initialised = true;
            }
            else
                RefreshTiles();
        }

        // Use this for initialization
        void Start()
        {
            SetNotification();
            StartCoroutine(CheckEveryMinute());
        }

        public void Close()
        {
            AudioManager.Play("Click");
            screen.Close();
        }

        private bool initialised = false;

        private void RefreshTiles()
        {
            for (int i = 0; i < allTiles.Count; i++)
            {
                allTiles[i].Initialise(allRewards[i].sprite, FormatTitle(allRewards[i]), currentDay == i,
                    lastClaimedDay >= i, i, this);
            }

            claimButton.interactable = currentDay > lastClaimedDay;
        }

        IEnumerator CheckEveryMinute()
        {
            while (true)
            {
                yield return new WaitForSeconds(60f);
                SetNotification();
            }
        }

        public bool CheckNotification()
        {
            //LoadRewardData();
            return currentDay > lastClaimedDay;
        }

        public void SetNotification()
        {
            notificationSymbol.SetActive(CheckNotification());
        }

        public void CompileNotifications(ref List<Notification> notifications)
        {
            //System.TimeSpan timeSpan = System.DateTime.Today.AddHours(31) - System.DateTime.Now;
            //notifications.Add(new Notification("Daily Reward!", "Your free reward is ready, keep your daily streak going!", (int)timeSpan.TotalSeconds));
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("lastClaim", lastClaimedDay);
            saveData.Add("claimTime", JC_SaveManager.SaveDateTime(lastClaimed));
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            lastClaimedDay = loadedData.LoadData("lastClaim", -1);
            lastClaimed = JC_SaveManager.GetDateTime(loadedData.LoadData("claimTime", ""), DateTime.Today);
            LoadRewardData();
            SetNotification();
        }

        public void LoadDefault()
        {
            lastClaimedDay = -1;
            lastClaimed = DateTime.Today;
            LoadRewardData();
            SetNotification();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
                return;
            DateTime nextRewardNotify = lastClaimed.AddHours(32);
            if (DateTime.Now >= nextRewardNotify)
                return;
            if (!LocalNotificationManager.TryGetCopyOfLocalNotificationMeta(0, out LocalNotificationMeta meta))
                return;
            meta.TimeSpanFromScheduled = nextRewardNotify - DateTime.Now;
            LocalNotificationManager.ScheduleNotification(meta);
        }
    }

}