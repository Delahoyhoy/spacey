using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.DailyRewards
{
    [Serializable]
    public struct DailyRewardItem
    {
        public string title;
        public bool localiseTitle, formatTitle;
        public string titleFormat;
        public string description;
        public bool formatDescription;
        public string format;
        public Sprite sprite;
        public UnityEvent OnClaimAction;

        public void Claim()
        {
            OnClaimAction?.Invoke();
        }
    }
}