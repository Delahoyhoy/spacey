﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.DailyRewards
{
    public class DailyRewardTile : MonoBehaviour
    {

        private DailyReward controllerRef;

        [SerializeField]
        private Image icon;

        [SerializeField]
        private Text titleText;

        [SerializeField]
        private GameObject checkmark, grey, glow, currentHighlight;

        [SerializeField]
        private Outline outline;

        private int index;

        public void Initialise(Sprite iconSprite, string displayText, bool ready, bool complete, int rewardIndex,
            DailyReward dailyReward)
        {
            icon.sprite = iconSprite;
            index = rewardIndex;
            controllerRef = dailyReward;
            titleText.text = displayText;
            checkmark.SetActive(complete);
            //outline.enabled = ready;
            currentHighlight.SetActive(ready && !complete);
            glow.SetActive(ready && !complete);
            grey.SetActive(!ready && !complete);
        }

        public void Select()
        {
            //Debug.Log(index);
            controllerRef.OpenPopup(index);
        }
    }
}
