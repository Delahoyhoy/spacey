namespace Core.Purchasing
{
    public enum AdRemoveType
    {
        None,
        Rewarded,
        Interstitial,
        All,
    }
}