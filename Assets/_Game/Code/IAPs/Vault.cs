﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Save;
using Core.Gameplay.VIP;
using Fumb.RemoteConfig;
using UnityEngine;
using UnityEngine.UI;
using Fumb.Serialization;
using Fumb.Save;
using UnityEngine.Events;
using Random = UnityEngine.Random;

////using Firebase.Analytics;

public class Vault : MonoBehaviour, ISaveLoad {

    [SerializeField]
    private Text vaultGems, claimTenCount, needsFifty, secondDisplay, maxFill;

    [SerializeField]
    private GameObject fullObject, notFullObject, claimableObject;

    [SerializeField]
    private Transform vaultIcon;

    [SerializeField]
    private Slider fillAmountSlider;

    [ManagedSaveValue("VaultCurrentGems")]
    private SaveValueDouble saveCurrentGems;

    [ManagedSaveValue("VaultTimesClaimed")]
    private SaveValueInt saveTimesClaimed;

    [ManagedSaveValue("VaultTimeFilled")]
    private SaveValueDateTime saveTimeFilled;

    [ManagedSaveValue("VaultHasOfferedDouble")]
    private SaveValueBool saveHasOfferedDouble;

    [SerializeField]
    private UnityEvent<float> onSetFillAmount;

    private RemoteFloat vaultBaseCapacity = new RemoteFloat("vaultBase", 250);

    private DateTime TimeFilled => saveTimeFilled.IsPopulated ? saveTimeFilled.DateTime : UnbiasedTime.Instance.Now();

    public double CurrentGems
    {
        get => saveCurrentGems.Value;
        set => saveCurrentGems.Value = value;
    }

    private double Capacity => GetCapacityAtLevel(saveTimesClaimed.Value);

    private bool IsMaxLevel => saveTimesClaimed.Value >= vaultMaxLevel;
    private bool Full => CurrentGems >= Capacity;

    private int vaultMaxLevel = 7;

    [SerializeField]
    private Button claimTen, claimAll;

    private int gemsInVault = 0, gemsEarnedToday = 0, poolToAdd = 0;

    private DateTime today, tenGemClaimTimer;

    private bool resettingTenTimer = false;

    [SerializeField]
    private GameObject vipEarnsPanel;

    public static Vault Instance { get { if (!instance) instance = FindObjectOfType<Vault>(); return instance; } }
    private static Vault instance;

    private double GetCapacityAtLevel(int level)
    {
        return vaultBaseCapacity * Mathf.Pow(2, level);
    }

    private void OnVIPLevelChange(VIPCategory category, double value)
    {
        if (category != VIPCategory.Diamonds)
            return;
        UpdateDisplay();
    }

    public void DoubleVault()
    {
        if (IsMaxLevel)
            return;
        saveTimesClaimed.Value++;
        CurrentGems *= 2;
        UpdateDisplay();
    }

    public bool ShouldOfferDouble()
    {
        return Full && !IsMaxLevel && !saveHasOfferedDouble.Value && UnbiasedTime.Instance.Now() >= TimeFilled.AddHours(24);
    }

    public void RegisterDoubleOffered()
    {
        saveHasOfferedDouble.Value = true;
    }

    public void AddGemsToVault(int count)
    {
        //Setup(true);
        if (!setup)
        {
            poolToAdd += count;
            return;
        }
        if (Full)
            return;
        CurrentGems += count;
        if (Full)
        {
            saveTimeFilled.Value = new SerializableDateTime(UnbiasedTime.Instance.Now());
            CurrentGems = Capacity;
            notFullObject.SetActive(false);
            fullObject.SetActive(true);
        }

        UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        maxFill.text = CurrencyController.FormatToString(Capacity);
        float fillPercent = (float) (CurrentGems / Capacity);
        fillAmountSlider.value = fillPercent;
        onSetFillAmount?.Invoke(fillPercent);
        double multiplier = VIPManager.Instance.GetValue(VIPCategory.Diamonds);
        double extra = Math.Round((multiplier - 1) * CurrentGems);
        vaultGems.text = extra > 0 ? string.Format("{0} <size=24>(+{1})</size>", 
                CurrencyController.FormatToString(CurrentGems), CurrencyController.FormatToString(extra)) :
            CurrencyController.FormatToString(CurrentGems);
        secondDisplay.text = CurrencyController.FormatToString(CurrentGems);
        claimableObject.SetActive(!IsMaxLevel && CurrentGems >= 30);
    }

    public void ClaimGems()
    {
        double multiplier = VIPManager.Instance.GetValue(VIPCategory.Diamonds);
        double totalGems = Math.Round(CurrentGems * multiplier);
        CoinBurster.Instance.NewBurst(CurrencyType.HardCurrency, vaultIcon, Random.Range(7, 12), totalGems);
        double currentlyInVault = CurrentGems;
        CurrentGems = 0;
        double extra = Math.Round((multiplier - 1) * CurrentGems);
        vaultGems.text = extra > 0 ? string.Format("{0} <size=24>(+{1})</size>", CurrencyController.FormatToString(CurrentGems), CurrencyController.FormatToString(extra)) :
            CurrencyController.FormatToString(CurrentGems); ;
        if (!IsMaxLevel)
            saveTimesClaimed.Value++;
        saveHasOfferedDouble.Value = false;
        notFullObject.SetActive(true);
        fullObject.SetActive(false);
        UpdateDisplay();
        SetupButtons();
        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        /*
        List<Firebase.Analytics.Parameter> analyticsParameters = new List<Firebase.Analytics.Parameter>();
        analyticsParameters.Add(new Firebase.Analytics.Parameter("GemsClaimed", currentlyInVault));
        AnalyticsManager.Instance.TriggerEvent(AnalyticsEvent.OpenVault, analyticsParameters);
        */
    }

    private void Awake()
    {
        instance = this;
    }

    private void CheckTime()
    {
        if (DateTime.Today != today)
        {
            gemsEarnedToday = 0;
            today = DateTime.Today;
        }
    }

    private bool setup = false;

    private void Setup(bool fresh)
    {
        if (setup)
            return;
        DateTime lastDay = today;
        today = DateTime.Today;
        if (!fresh)
        {
            if (DateTime.Today != lastDay)
                gemsEarnedToday = 0;
        }
        else
        {
            gemsEarnedToday = 0;
        }
        
        notFullObject.SetActive(!Full);
        fullObject.SetActive(Full);
        
        UpdateDisplay();

        //tenGemClaimTimer = MergeManager.GetDateTime(SaveManager.GetString("tenClaimTime", ""), UnbiasedTime.Instance.Now().AddHours(-1));
        if (tenGemClaimTimer > UnbiasedTime.Instance.Now())
        {
            claimTenCount.text = FormatMMSS(tenGemClaimTimer - UnbiasedTime.Instance.Now());
            vipEarnsPanel.SetActive(false);
            resettingTenTimer = true;
        }
        else
        {
            claimTenCount.text = "";
            vipEarnsPanel.SetActive(true);
            resettingTenTimer = false;
        }
        SetupButtons();
        StartCoroutine(UpdateEverySecond());
        if (poolToAdd > 0)
            AddGemsToVault(poolToAdd);
        setup = true;
    }

    public void SetupButtons()
    {
        CheckTime();
        claimTen.interactable = CurrentGems >= 10 && !resettingTenTimer && AdHub.Ready;
        claimAll.interactable = CurrentGems >= 30;
        needsFifty.gameObject.SetActive(CurrentGems < 30);
    }

    public void ClaimTen()
    {
        if (AdHub.Ready)
            AdHub.ShowAd("VaultClaim10", HandleAdResultIfSucceeded);
    }

    public void HandleAdResultIfSucceeded()
    {
        VIPManager.Instance.AddVIPPoints(20);
        double multiplier = VIPManager.Instance.GetValue(VIPCategory.Diamonds);
        CurrentGems = Math.Max(CurrentGems - 10, 0);
        double extra = Math.Round((multiplier - 1) * CurrentGems);
        UpdateDisplay();
        CoinBurster.Instance.NewBurst(CurrencyType.HardCurrency, vaultIcon, Random.Range(5, 8), 10);
        tenGemClaimTimer = UnbiasedTime.Instance.Now().AddMinutes(15);
        claimTenCount.text = "15:00";
        vipEarnsPanel.SetActive(false);
        resettingTenTimer = true;
        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        /*
        List<Firebase.Analytics.Parameter> parameters = new List<Firebase.Analytics.Parameter>();
        parameters.Add(new Firebase.Analytics.Parameter("AdType", "VaultClaim10"));
        AnalyticsManager.Instance.TriggerEvent(AnalyticsEvent.WatchAd, parameters);*/
        SetupButtons();
    }

    private IEnumerator UpdateEverySecond()
    {
        while (true)
        {
            if (resettingTenTimer)
            {
                if (UnbiasedTime.Instance.Now() >= tenGemClaimTimer)
                {
                    claimTen.interactable = CurrentGems >= 10 && AdHub.Ready;
                    claimTenCount.text = "";
                    vipEarnsPanel.SetActive(true);
                }
                else
                {
                    claimTenCount.text = FormatMMSS(tenGemClaimTimer - UnbiasedTime.Instance.Now());
                    vipEarnsPanel.SetActive(false);
                }
            }
            yield return new WaitForSeconds(1f);
        }
    }

    private string FormatMMSS(System.TimeSpan timeSpan)
    {
        return string.Format("{0}:{1}", timeSpan.Minutes.ToString("00"), timeSpan.Seconds.ToString("00"));
    }

    public void Save(ref Dictionary<string, object> saveData)
    {
        saveData.Add("gemsToday", gemsEarnedToday);
        saveData.Add("tenClaimTime", JC_SaveManager.SaveDateTime(tenGemClaimTimer));
        saveData.Add("vaultToday", JC_SaveManager.SaveDateTime(today));
    }

    public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
    {
        today = JC_SaveManager.GetDateTime(loadedData.LoadData("vaultToday", ""), System.DateTime.Today.AddDays(-1));
        gemsEarnedToday = loadedData.LoadData("gemsToday", 0);
        gemsInVault = loadedData.LoadData("gemsInVault", 0);

        if (!saveCurrentGems.IsPopulated)
        {
            for (int i = 0; i <= vaultMaxLevel; i++)
            {
                double maxAtLevel = GetCapacityAtLevel(i);
                if (gemsInVault > maxAtLevel && i < vaultMaxLevel) 
                    continue;
                saveTimesClaimed.Value = i;
                saveCurrentGems.Value = Math.Min(gemsInVault, maxAtLevel);
                break;
            }
        }

        tenGemClaimTimer = JC_SaveManager.GetDateTime(loadedData.LoadData("tenClaimTime", ""), UnbiasedTime.Instance.Now().AddHours(-1));
        Setup(false);
    }

    public void LoadDefault()
    {
        today = DateTime.Today.AddDays(-1);
        gemsEarnedToday = 0;
        gemsInVault = 0;
        tenGemClaimTimer = UnbiasedTime.Instance.Now().AddHours(-1);
        Setup(true);
    }
}
