﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Purchasing
{
    [RequireComponent(typeof(Text))]
    public class LocalisePrice : MonoBehaviour
    {

        [SerializeField]
        private string priceID;

        // Use this for initialization
        private void Start()
        {
            //GetComponent<Text>().text = IAPManager.Instance.GetLocalisedPrice(priceID);
            GetComponent<Text>().text = IAPManager.Instance.Connected ?
                IAPManager.Instance.GetLocalisedPrice(priceID) :
                "NOT CONNECTED";
        }

        private void OnEnable()
        {
            IAPManager.OnIAPServiceConnect += OnConnect;
        }

        private void OnDestroy()
        {
            IAPManager.OnIAPServiceConnect -= OnConnect;
        }

        public void OnConnect(bool connected)
        {
            if (connected)
                GetComponent<Text>().text = IAPManager.Instance.GetLocalisedPrice(priceID);
            else
                GetComponent<Text>().text = "NOT CONNECTED";
        }
    }
}
