using System;
using UnityEngine.Events;
using UnityEngine;
using Fumb.Attribute;
using Fumb.Purchasing;
using UnityEngine.Purchasing;

namespace Core.Purchasing
{
    [Serializable]
    public class SubscriptionItem
    {
        public string subscriptionId;
        public AdRemoveType removeAds;
        [SerializeField]
        private UnityEvent<bool> onSetSubscriptionActive;
        public Action<bool> onSetActiveStatus;

        [SerializeField]
        private bool separateStatusActions;

        [SerializeField] [ConditionalField("separateStatusActions")]
        private UnityEvent<string> onSetActive, onSetInactive;

        private bool initialised, isRunning;
        private SubscriptionInfo cachedInfo;
        private DateTime lastCached;
        private DateTime expectedEnd;

        public bool IsRunning => isRunning;

        public void UpdateInfo(SubscriptionInfo info, bool forceUpdate = false)
        {
            bool lastRunning = isRunning;
            cachedInfo = info;
            bool nowRunning = info.isSubscribed() == Result.True;
            lastCached = UnbiasedTime.Instance.Now();
            if (forceUpdate || lastRunning != nowRunning)
                SetActive(nowRunning);
            if (isRunning)
                expectedEnd = UnbiasedTime.Instance.Now().Add(info.getRemainingTime());
        }

        public void Update(DateTime time, float autoRefreshPeriod)
        {
            if (initialised && time < expectedEnd && time < lastCached.AddSeconds(autoRefreshPeriod))
                return;
            initialised = true;
            if (!PurchasingManager.TryGetSubscriptionInfo(subscriptionId, out SubscriptionInfo info))
            {
                if (!isRunning) 
                    return;
                lastCached = UnbiasedTime.Instance.Now();
                SetActive(false);
                return;
            }
            UpdateInfo(info);
        }

        public void SetActive(bool active)
        {
            isRunning = active;
            onSetSubscriptionActive?.Invoke(active);
            if (separateStatusActions)
            {
                if (active)
                    onSetActive?.Invoke(subscriptionId);
                else
                    onSetInactive?.Invoke(subscriptionId);
            }
            onSetActiveStatus?.Invoke(active);
            if (removeAds == AdRemoveType.None) 
                return;
            if (active)
                AdHub.Instance.AddAdRemovedFlag(removeAds, subscriptionId);
            else
                AdHub.Instance.RemoveAdRemovedFlag(removeAds, subscriptionId);
        }
    }
}