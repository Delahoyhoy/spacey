using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fumb.Purchasing;
using UnityEngine.Purchasing;

namespace Core.Purchasing
{
    public class SubscriptionManager : MonoBehaviour
    {
        private static SubscriptionManager instance;
        public static SubscriptionManager Instance => instance ??= ObjectFinder.FindObjectOfType<SubscriptionManager>();

        [SerializeField]
        private SubscriptionItem[] subscriptionItems;

        [SerializeField]
        private float autoUpdateInterval = 90f;
        
        private Dictionary<string, SubscriptionItem> subscriptionLookup = new Dictionary<string, SubscriptionItem>();

        private bool initialised;

        private void Awake()
        {
            instance = this;
            for (int i = 0; i < subscriptionItems.Length; i++)
                subscriptionLookup.Add(subscriptionItems[i].subscriptionId, subscriptionItems[i]);
        }

        public void Purchase(string subscriptionId)
        {
            if (!subscriptionLookup.TryGetValue(subscriptionId, out SubscriptionItem subscriptionItem))
                return;

            void PurchaseSuccess()
            {
                subscriptionItem.Update(UnbiasedTime.Instance.Now(), 0);
            }
            
            PurchasingManager.PurchaseProduct(subscriptionId, "", PurchaseSuccess);
        }

        private void Start()
        {
            if (initialised)
                return;
            GameManager.OnSecondUpdate += UpdateSubscriptions;
            PurchasingManager.PurchasedProductAction += PurchaseComplete;
            initialised = true;
        }

        private void OnDestroy()
        {
            if (!initialised)
                return;
            GameManager.OnSecondUpdate -= UpdateSubscriptions;
            PurchasingManager.PurchasedProductAction -= PurchaseComplete;
        }

        public bool GetSubscriptionActive(string id)
        {
            if (!subscriptionLookup.TryGetValue(id, out SubscriptionItem item))
                return false;
            return item.IsRunning;
        }

        public void ListenForSubscription(string id, Action<bool> onSetActive)
        {
            if (!subscriptionLookup.TryGetValue(id, out SubscriptionItem item))
                return;
            item.onSetActiveStatus += onSetActive;
        }
        
        private void PurchaseComplete(Product product)
        {
            if (!subscriptionLookup.TryGetValue(product.definition.id, out SubscriptionItem subscriptionItem))
                return;
            subscriptionItem.SetActive(true);
        }

        private void UpdateSubscriptions()
        {
            DateTime currentTime = UnbiasedTime.Instance.Now();
            foreach (SubscriptionItem subscriptionItem in subscriptionLookup.Values)
            {
                subscriptionItem.Update(currentTime, autoUpdateInterval);
            }
        }
    }
}
