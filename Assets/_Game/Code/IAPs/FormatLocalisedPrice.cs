﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Purchasing
{
    public class FormatLocalisedPrice : MonoBehaviour
    {
        [SerializeField]
        private string priceID;

        [SerializeField] [Multiline]
        private string format;

        [SerializeField]
        private string defaultPrice;
        
        [SerializeField]
        private UnityEvent<string> onSetText;
        
        private void Start()
        {
            string priceText = IAPManager.Instance.Connected ? IAPManager.Instance.GetLocalisedPrice(priceID) :
                defaultPrice;
            onSetText?.Invoke(string.Format(format, priceText));
        }

        private void OnEnable()
        {
            IAPManager.OnIAPServiceConnect += OnConnect;
        }

        private void OnDestroy()
        {
            IAPManager.OnIAPServiceConnect -= OnConnect;
        }

        private void OnConnect(bool connected)
        {
            string priceText = IAPManager.Instance.Connected ?
                IAPManager.Instance.GetLocalisedPrice(priceID) :
                defaultPrice;
            onSetText?.Invoke(string.Format(format, priceText));
        }
    }
}