using System;
using Fumb.Purchasing;

namespace Core.Gameplay.Utilities
{
    public static class PurchaseRestoreHelper
    {
        public static Action onPurchasesRestored;

        public static void RestorePurchases()
        {
            PurchasingManager.RestorePurchases();
            onPurchasesRestored?.Invoke();
        }
    }
}
