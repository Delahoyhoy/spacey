using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Fumb.Purchasing;

namespace Core.Purchasing
{
    public class PurchaseAction : MonoBehaviour
    {
        [SerializeField]
        private string purchaseId, placement;
        
        [SerializeField]
        private UnityEvent onPurchaseComplete, onPurchaseCancel, onPurchaseFailed;

        public void Purchase()
        {
            void PurchaseSuccess()
            {
                onPurchaseComplete?.Invoke();
            }

            void PurchaseFail()
            {
                onPurchaseFailed?.Invoke();
            }

            void PurchaseCancelled()
            {
                onPurchaseCancel?.Invoke();
            }

            PurchasingManager.PurchaseProduct(purchaseId, placement, PurchaseSuccess, PurchaseFail, PurchaseCancelled);
        }
    }
}
