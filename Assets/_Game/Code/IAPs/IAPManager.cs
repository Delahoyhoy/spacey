using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
//TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
/*
using SA.CrossPlatform.GameServices;
using SA.CrossPlatform.InApp;
using SA.Android.Vending.Billing;
using SA.iOS.StoreKit;
*/
using Newtonsoft.Json;
using System.Globalization;
using System;
using System.Text.RegularExpressions;
using Fumb.Purchasing;
using Fumb;
using UnityEngine.Purchasing;

//using Facebook.Unity;

[System.Serializable]
public class IAP
{
    public string ID;
    public string localisedPrice;
    public string description;
    public UnityEvent onPurchaseComplete;
}

public class IAPManager : MonoBehaviour
{
    bool isConnected = false;
    public bool isWaitingOnIAPToReturn = false;

    [SerializeField]
    private IAP[] inAppPurchases;

    private Dictionary<string, IAP> iapDictionary = new Dictionary<string, IAP>();

    [SerializeField]
    private GameObject overlayBlocker;



    private PurchaserSettings purchaserSettings;
    // DEBUGGING AND TESTING STUFF

    public Text areYouSure;
    public GameObject debugDisplay;
    private string currentID;

    public static IAPManager Instance { get { if (!instance) instance = FindObjectOfType<IAPManager>(); return instance; } }
    private static IAPManager instance;

    public int purchasesThisSession = 0;

    public delegate void IAPServiceConnect(bool connected);
    public static event IAPServiceConnect OnIAPServiceConnect;

    public bool IsThisLikelyACheater()
    {
        return purchasesThisSession >= 3;
    }

    // Use this for initialization
    void Start()
    {
        // Initialise the IAP stuff!
        purchaserSettings = FumbResourcesHelper.LoadObject<PurchaserSettings>();
        IsConnectedToIAPService();
    }

    public bool Connected { get { return isConnected; } }

    IEnumerator WaitThenRetry()
    {
        yield return new WaitForSeconds(0.3f);
        IsConnectedToIAPService();
    }

    void IsConnectedToIAPService()
    {
        if (!PurchasingManager.IsInitialized)
        {
            StartCoroutine(WaitThenRetry());
            return;
        }
        isConnected = true;

        for (int i = 0; i < inAppPurchases.Length; i++)
        {
            Product product = PurchasingManager.GetProduct(inAppPurchases[i].ID);
            string priceString = product.metadata.localizedPriceString;
            if (Application.isEditor)
            {
                priceString = "�" + purchaserSettings.GetProduct(inAppPurchases[i].ID).basePrice;
            }

            inAppPurchases[i].localisedPrice = priceString;
           
            
            if (!iapDictionary.ContainsKey(inAppPurchases[i].ID))
                iapDictionary.Add(inAppPurchases[i].ID, inAppPurchases[i]);
        }
    }

    public string GetLocalisedPrice(string id)
    {
        return ReturnItemPrice(id);
    }

    private void Awake()
    {
        instance = this;
    }

    public string ReturnItemPrice(string productID)
    {
        Product product = PurchasingManager.GetProduct(productID);
        string priceString = product.metadata.localizedPriceString;
        return priceString;
    }

    public bool IsConnectedToStore()
    {
        return PurchasingManager.IsInitialized;
    }

    
    public void PurchaseAnItem(string ID)
    {
        void PurchaseSuccess()
        {
            iapDictionary[ID].onPurchaseComplete.Invoke();
            overlayBlocker.SetActive(false);
        }

        PurchasingManager.PurchaseProduct(ID, "", PurchaseSuccess, TurnOffIAPBlocked, TurnOffIAPBlocked);

        overlayBlocker.SetActive(true);
    }

    public void TurnOffIAPBlocked()
    {
        overlayBlocker.SetActive(false);
    }


    public static decimal Parse(string input)
    {
        return decimal.Parse(Regex.Match(input, @"-?\d{1,3}(,\d{3})*(\.\d+)?").Value);
    }

    public void PurchaseFailure()
    {
        debugDisplay.SetActive(false);
    }
}
