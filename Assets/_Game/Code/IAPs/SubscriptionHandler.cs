﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Purchasing
{
    public class SubscriptionHandler : MonoBehaviour
    {
        [SerializeField]
        private string subscriptionId;

        [SerializeField]
        private UnityEvent<bool> onSetSubscriptionActive;

        private bool initialised = false;

        private void Start()
        {
            if (initialised)
                return;
            SubscriptionManager.Instance.ListenForSubscription(subscriptionId, active => onSetSubscriptionActive?.Invoke(active));
            onSetSubscriptionActive?.Invoke(SubscriptionManager.Instance.GetSubscriptionActive(subscriptionId));
            initialised = true;
        }

        public void PurchaseSubscription()
        {
            SubscriptionManager.Instance.Purchase(subscriptionId);
        }
    }
}