using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VaultManager : MonoBehaviour
{
    [SerializeField]
    private UIScreen doublePrompt;

    [SerializeField]
    private Text currentText, doubledText;

    public void CheckShouldDisplay()
    {
        if (!Vault.Instance.ShouldOfferDouble())
            return;
        if (UIManager.Instance.InMenu)
            return;
        OpenScreen();
    }

    private void OpenScreen()
    {
        doublePrompt.Open();
        double currentVaultGems = Vault.Instance.CurrentGems;
        currentText.text = CurrencyController.FormatToString(currentVaultGems);
        doubledText.text = CurrencyController.FormatToString(currentVaultGems * 2);
        Vault.Instance.RegisterDoubleOffered();
    }
    
}
