namespace Core.Gameplay.Boosts
{
    public enum BoostType
    {
        Tap,
        MineSpeed,
        CargoSpeed,
        Efficiency,
    }
}