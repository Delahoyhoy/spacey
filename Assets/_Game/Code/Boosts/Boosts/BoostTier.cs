using System;

namespace Core.Gameplay.Boosts
{
    [Serializable]
    public struct BoostTier
    {
        public float weight;
        public double seconds, magnitude;
    }
}