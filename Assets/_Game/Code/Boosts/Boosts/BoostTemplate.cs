using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Boosts
{
    [Serializable]
    public struct BoostTemplate
    {
        public Sprite boostIcon;
        public Color boostColour;
        public bool timeBased;
        public BoostType boostType;
        public BoostTier[] tiers;
        public BoostTier superTier;
        public string titleText, descriptionText, superDescription;
        public string shortFormat;

        public void SelectTier(out double time, out double effect)
        {
            int bestIndex = 0;
            float bestValue = 0;
            for (int i = 0; i < tiers.Length; i++)
            {
                float value = Random.value * tiers[i].weight;
                if (value > bestValue)
                {
                    bestValue = value;
                    bestIndex = i;
                }
            }

            effect = tiers[bestIndex].magnitude;
            time = tiers[bestIndex].seconds;
        }

        public string FormatDescription(double effect, double time)
        {
            return string.Format(descriptionText, effect, time);
        }

        public string FormatSuperDescription(double effect, double time)
        {
            return string.Format(descriptionText, effect, time);
        }

        public string GetShortMult(double effect)
        {
            return string.Format(shortFormat, effect);
        }
    }
}