﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Reset;
using UnityEngine;

public class EventPather : MonoBehaviour
{
    [SerializeField]
    private BezierPoint pointA, pointB;

    [SerializeField]
    private Transform curvePoint;

    [SerializeField]
    private float constantDepth = 6f;

    [SerializeField]
    private Transform ufo;

    [SerializeField]
    private float travelSpeed = 1f;

    private Coroutine travelCoroutine;

    private bool paused = false;

    public bool pathing = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EmitTrail());
        ResetManager.Instance.beginReset += HandleReset;
    }

    public void HandleReset(ResetType resetType)
    {
        DisableUFO();
    }

    public void DisableUFO()
    {
        if (travelCoroutine != null)
            StopCoroutine(travelCoroutine);
        pathing = false;
        ufo.gameObject.SetActive(false);
    }

    public void BurstAtUFO()
    {
        EffectManager.Instance.EmitAt("sparkleBurst", ufo.position, 30);
    }

    IEnumerator EmitTrail()
    {
        while (true)
        {
            if (pathing && !paused)
            {
                EffectManager.Instance.EmitAt("sparkleTrail", ufo.position, 1);
                yield return new WaitForSeconds(0.1f);
            }
            else
                yield return null;
        }
    }

    public void PausePath(bool pausePath)
    {
        paused = pausePath;
    }

    public void Setup()
    {
        float radius = GameManager.Instance.FogRadius + 10f;
        float startAngle = Random.Range(0, 360f);
        float endRotation = Random.Range(160f, 200f);
        float endAngle = startAngle + endRotation;
        float midAngle = startAngle + (Random.value > 0.5f ? 90f : -90f);
        Vector3 origin = GameManager.Instance.FogCentre;
        pointA.transform.position = origin + GetPosFromAngle(startAngle, radius);
        pointB.transform.position = origin + GetPosFromAngle(endAngle, radius);
        curvePoint.position = origin + GetPosFromAngle(midAngle, radius / 3f);
        pointA.Handle = pointA.Point + (curvePoint.position - pointA.Point);
        pointB.Handle = pointB.Point + (curvePoint.position - pointB.Point);
        if (travelCoroutine != null)
            StopCoroutine(travelCoroutine);
        travelCoroutine = StartCoroutine(TravelPath());
    }

    IEnumerator TravelPath()
    {
        pathing = true;
        ufo.position = pointA.Point;
        ufo.gameObject.SetActive(true);
        BezierPath bezierPath = new BezierPath(pointA, pointB);
        float distance = Vector3.Distance(pointA.Point, curvePoint.position) + Vector3.Distance(pointB.Point, curvePoint.position);
        float step = travelSpeed / distance;
        Vector3 pos = pointA.Point;
        Vector3 lastPos = pos;
        do
        {
            ufo.position = pos;
            //ufo.up = pos - lastPos;
            lastPos = pos;
            while (paused)
                yield return null;
            yield return null;
        } 
        while (!bezierPath.TraversePath(travelSpeed * Time.deltaTime, out pos));
        //for (float t = 0; t < 1f; t += Time.deltaTime * step)
        //{
        //    ufo.transform.position = BezierCurve.InterpolatePos(pointA, pointB, t);
        //    while (paused)
        //        yield return null;
        //    yield return null;
        //}
        ufo.gameObject.SetActive(false);
        pathing = false;
    }

    private Vector3 GetPosFromAngle(float angle, float offset)
    {
        float x = Mathf.Sin(angle * Mathf.Deg2Rad) * offset;
        float y = Mathf.Cos(angle * Mathf.Deg2Rad) * offset;
        return new Vector3(x, y, constantDepth);
    }
}
