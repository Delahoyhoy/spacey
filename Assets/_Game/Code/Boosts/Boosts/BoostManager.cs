﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Save;
using Core.Gameplay.Tutorial;
using Core.Gameplay.VIP;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Boosts
{
    public class BoostManager : MonoBehaviour, ISaveLoad
    {
        [SerializeField]
        private EventPather eventPath;

        private DateTime boostStart, boostEnd, nextBoost;

        [SerializeField]
        private float minDelay = 240f, maxDelay = 360f;

        [SerializeField]
        private Text boostTimer;

        [SerializeField]
        private Image boostIcon, windowIcon, superIcon;

        [SerializeField]
        private Text displayText, superText, notifyText;

        [SerializeField]
        private Button watchAdButton;

        private BoostState boostState = BoostState.Waiting;

        private bool boostRunning = false;
        private BoostType boostType = BoostType.Tap;

        [SerializeField]
        private BoostTemplate[] boostTemplates;

        private Dictionary<BoostType, BoostTemplate> boostDictionary = new Dictionary<BoostType, BoostTemplate>();

        [SerializeField]
        private UnityEvent<bool> onSetAvailable, onSetRunning;

        [SerializeField]
        private UnityEvent<Color> onSetBoostColour;

        private bool Available => boostState == BoostState.Available;
        private bool Running => boostState == BoostState.Running;

        int countDownToSuper = 4;

        private void SelectBoost()
        {
            int templateIndex = Random.Range(0, boostTemplates.Length);
            boostType = boostTemplates[templateIndex].boostType;
            boostTemplates[templateIndex].SelectTier(out secondsToRun, out multiplier);
        }

        private BoostState State
        {
            set
            {
                boostState = value;
                switch (boostState)
                {
                    case BoostState.Available:
                        boostTimer.text = "";
                        //NotificationDisplay.Instance.Display("UFO SIGHTED!");
                        boostIcon.gameObject.SetActive(false);
                        GameManager.Instance.DisableBoost(boostType);
                        SelectBoost();
                        //eventPath.Setup();
                        break;
                    case BoostState.Running:
                        //eventPath.DisableUFO();
                        if (!boostDictionary.TryGetValue(boostType, out BoostTemplate boostTemplate))
                            break;
                        GameManager.Instance.EnableBoost(boostType, multiplier);
                        boostEnd = UnbiasedTime.Instance.Now().AddSeconds(secondsToRun);
                        boostTimer.text = FormatTime(boostEnd - UnbiasedTime.Instance.Now());
                        notifyText.text = boostTemplate.GetShortMult(multiplier);
                        boostIcon.gameObject.SetActive(true);
                        boostIcon.sprite = boostTemplate.boostIcon;
                        Color boostColour = boostTemplate.boostColour;
                        //boostIcon.color = boostColour;
                        onSetBoostColour?.Invoke(boostColour);
                        break;
                    case BoostState.Waiting:
                        boostTimer.text = "";
                        //eventPath.DisableUFO();
                        GameManager.Instance.DisableBoost(boostType);
                        nextBoost = UnbiasedTime.Instance.Now().AddSeconds(Random.Range(minDelay, maxDelay));
                        boostIcon.gameObject.SetActive(false);
                        break;
                }

                onSetAvailable?.Invoke(Available);
                onSetRunning?.Invoke(Running);
            }
        }

        private double secondsToRun = 120;
        private double multiplier = 2;

        [SerializeField]
        private UIScreen screen, superScreen;

        public void OpenScreen()
        {
            //secondsToRun = 120;// VIPManager.Instance.GetValue(VIPCategory.BoostTime);
            if (!boostDictionary.TryGetValue(boostType, out BoostTemplate boostTemplate))
                return;
            screen.Open();
            windowIcon.sprite = boostTemplate.boostIcon;
            //windowIcon.color = boostTemplate.boostColour;
            displayText.text =
                boostTemplate.FormatDescription(multiplier,
                    secondsToRun); //string.Format("Watch a short ad to {1} your income from tapping for {0} seconds", secondsToRun.ToString(), multipliers[multiplier]);
            //watchAdButton.interactable = AdHub.Ready;
        }

        public void OpenSuperScreen()
        {
            //secondsToRun = 120;// VIPManager.Instance.GetValue(VIPCategory.BoostTime);
            if (!boostDictionary.TryGetValue(boostType, out BoostTemplate boostTemplate))
                return;
            superScreen.Open();
            superIcon.sprite = boostTemplate.boostIcon;
            //superIcon.color = boostTemplate.boostColour;
            double superMod = boostTemplate.superTier.magnitude;
            double superTime = boostTemplate.superTier.seconds;
            superText.text = string.Format("SUPERCHARGE YOUR BOOST:\n{0}",
                boostDictionary[boostType]
                    .FormatDescription(superMod,
                        superTime)); //string.Format("Watch a short ad to {1} your income from tapping for {0} seconds", secondsToRun.ToString(), multipliers[multiplier]);
            watchAdButton.interactable = AdHub.Ready;
        }

        public void HandleAdResultIfSucceeded()
        {
            VIPManager.Instance.AddVIPPoints(20);

            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            /*List<Firebase.Analytics.Parameter> parameters = new List<Firebase.Analytics.Parameter>();
            parameters.Add(new Firebase.Analytics.Parameter("AdType", "UFOBoost"));
            AnalyticsManager.Instance.TriggerEvent(AnalyticsEvent.WatchAd, parameters);*/
            ActivateSuperBoost();

        }

        public void ActivateBoost()
        {
            AudioManager.Play("Milestone", 0.5f);
            screen.Close();
            // eventPath.BurstAtUFO();
            // eventPath.DisableUFO();
            State = BoostState.Running;
        }

        public void ActivateSuperBoost()
        {
            multiplier = boostDictionary[boostType].superTier.magnitude;
            secondsToRun = boostDictionary[boostType].superTier.seconds;
            superScreen.Close();
            ActivateBoost();
        }

        public void Activate()
        {
            countDownToSuper--;
            if (countDownToSuper <= 0)
            {
                countDownToSuper = Random.Range(1, 3);
                if (AdHub.Ready)
                {
                    screen.Close();
                    OpenSuperScreen();
                }
            }
            else
            {
                ActivateBoost();
            }
        }

        public void WatchAd()
        {
            if (AdHub.Ready)
                AdHub.ShowAd("ActivateBoost", HandleAdResultIfSucceeded);
        }

        private string[] multipliers = {"", "", "double", "triple", "quadruple", "quintuple"};

        private IEnumerator EverySecond()
        {
            while (true)
            {
                DateTime now = UnbiasedTime.Instance.Now();
                switch (boostState)
                {
                    case BoostState.Running:
                        if (now > boostEnd)
                        {
                            State = BoostState.Waiting;
                        }
                        else
                            boostTimer.text = FormatTime(boostEnd - now);

                        break;
                    case BoostState.Waiting:
                        if (now >= nextBoost)
                        {
                            if (FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) && AdHub.Ready)
                                State = BoostState.Available;
                            else
                                nextBoost = now.AddSeconds(Random.Range(minDelay, maxDelay));
                        }

                        break;
                    case BoostState.Available:
                        // if (!eventPath.pathing)
                        // {
                        //     State = BoostState.Waiting;
                        // }
                        break;
                }

                yield return new WaitForSeconds(1f);
            }
        }

        private string FormatTime(TimeSpan timeSpan)
        {
            if (timeSpan.TotalHours > 1)
                return string.Format("{0:00}:{1:00}", timeSpan.Hours, timeSpan.Minutes);
            else
                return string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
        }

        // Start is called before the first frame update
        private void Start()
        {
            for (int i = 0; i < boostTemplates.Length; i++)
            {
                if (!boostDictionary.ContainsKey(boostTemplates[i].boostType))
                    boostDictionary.Add(boostTemplates[i].boostType, boostTemplates[i]);
            }

            State = BoostState.Waiting;
            StartCoroutine(EverySecond());
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("superCountdown", countDownToSuper);
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            countDownToSuper = loadedData.LoadData("superCountdown", Random.Range(3, 5));
        }

        public void LoadDefault()
        {
            countDownToSuper = Random.Range(2, 4);
        }
    }

}