namespace Core.Gameplay.Boosts
{
    public enum BoostState
    {
        Waiting,
        Available,
        Running,
    }
}