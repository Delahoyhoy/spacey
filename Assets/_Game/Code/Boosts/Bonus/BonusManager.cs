﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Core.Gameplay.Audio;
using Core.Gameplay.Save;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Utilities;
using Core.Gameplay.VIP;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Boosts
{
    public class BonusManager : MonoBehaviour, ISaveLoad
    {
        [SerializeField]
        private DateTime boostEnd;

        [SerializeField]
        private Text boostTimer, screenTimer;

        [SerializeField]
        private GameObject boostIcon;

        [SerializeField]
        private Text displayText;

        [SerializeField]
        private AdButton watchAdButton;

        [SerializeField]
        private UnityEvent<bool> onSetRunning;

        [SerializeField]
        private MultiSegmentFillBar fillBar;

        [SerializeField]
        private UIScreen screen;

        private const int MaxBoostStack = 5;
        
        private BoostState state = BoostState.Available;
        private float MaxTime => (float)(VIPManager.Instance.GetValue(VIPCategory.BoostTime) * 60) * MaxBoostStack;

        private bool CanAddTime => (boostEnd.AddMinutes(VIPManager.Instance.GetValue(VIPCategory.BoostTime)) - 
                                    UnbiasedTime.Instance.Now()).TotalSeconds < MaxTime;

        private bool IsRunning => state == BoostState.Running;

        private BoostState State
        {
            set
            {
                bool wasRunning = IsRunning;
                state = value;
                GameManager.Instance.SetBonusActive(IsRunning);
                SetButton();
                switch (state)
                {
                    case BoostState.Available:
                        boostTimer.text = "";
                        boostTimer.gameObject.SetActive(false);
                        screenTimer.text = AdHub.Ready ? "READY" : "";
                        boostIcon.SetActive(false);
                        fillBar.SetValue(0);
                        break;
                    case BoostState.Running:
                        boostEnd = UnbiasedTime.Instance.Now()
                            .AddMinutes(VIPManager.Instance.GetValue(VIPCategory.BoostTime));
                        UpdateDisplay();
                        boostTimer.gameObject.SetActive(true);
                        boostIcon.SetActive(true);
                        GameManager.OnSecondUpdate += EverySecond;
                        break;
                    case BoostState.Waiting:
                        State = BoostState.Available;
                        break;
                }

                onSetRunning?.Invoke(IsRunning);
            }
        }

        private void Start()
        {
            VIPManager.Instance.onSetUpgradeLevel += OnVIPUpgrade;
        }

        private void OnVIPUpgrade(VIPCategory category, double value)
        {
            if (category != VIPCategory.BoostTime)
                return;
            UpdateDisplay();
        }

        private string FormatTime(TimeSpan timeSpan)
        {
            if (timeSpan.TotalHours > 1)
                return string.Format("{0:00}:{1:00}", timeSpan.Hours, timeSpan.Minutes);
            else
                return string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
        }

        public void OpenScreen()
        {
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            double minutesToRun = VIPManager.Instance.GetValue(VIPCategory.BoostTime);
            screen.Open();
            if (state == BoostState.Available)
                screenTimer.text = AdHub.Ready ? "READY" : "";
            displayText.text = string.Format("Double your income for {0} minutes", minutesToRun.ToString(CultureInfo.InvariantCulture));
            SetButton();
        }

        public void HandleAdResultIfSucceeded()
        {
            VIPManager.Instance.AddVIPPoints(20);
            AudioManager.Play("Milestone", 1f);

            screen.Close();
            
            if (state == BoostState.Running)
                boostEnd = boostEnd.AddMinutes(VIPManager.Instance.GetValue(VIPCategory.BoostTime));
            else
                State = BoostState.Running;
            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            TimeSpan remainingTime = boostEnd - UnbiasedTime.Instance.Now();
            screenTimer.text = boostTimer.text = FormatTime(remainingTime);
            fillBar.SetValue((float)(remainingTime.TotalSeconds / MaxTime));
            watchAdButton.Interactable = state == BoostState.Available || CanAddTime;
        }

        public void EverySecond()
        {
            if (UnbiasedTime.Instance.Now() >= boostEnd)
            {
                State = BoostState.Available;
                GameManager.OnSecondUpdate -= EverySecond;
            }
            else
            {
                UpdateDisplay();
            }
        }

        public void WatchAd()
        {
            if (AdHub.Ready && state != BoostState.Running)
            {
                AdHub.ShowAd("DoubleIncome", HandleAdResultIfSucceeded);
            }
        }

        private void SetButton()
        {
            watchAdButton.Interactable = state == BoostState.Available || CanAddTime;
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("boostEnd", JC_SaveManager.SaveDateTime(boostEnd));
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            boostEnd = JC_SaveManager.GetDateTime(loadedData.LoadData("boostEnd", ""),
                UnbiasedTime.Instance.Now().AddHours(-1));
            if (UnbiasedTime.Instance.Now() > boostEnd)
            {
                State = BoostState.Available;
                boostIcon.SetActive(false);
                GameManager.Instance.SetBonusActive(false);
            }
            else
            {
                state = BoostState.Running;
                GameManager.Instance.SetBonusActive(true);
                boostTimer.gameObject.SetActive(true);
                boostIcon.SetActive(true);
                UpdateDisplay();
                GameManager.OnSecondUpdate += EverySecond;
            }

            onSetRunning?.Invoke(IsRunning);
        }

        public void LoadDefault()
        {
            State = BoostState.Available;
        }
    }

}