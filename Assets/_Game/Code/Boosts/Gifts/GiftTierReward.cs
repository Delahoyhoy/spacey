﻿using System;
using UnityEngine;

namespace Core.Gameplay.Boosts
{
    [Serializable]
    public class GiftTierReward
    {
        [SerializeField]
        private GiftTier tier;

        public GiftTier Tier => tier;

        [SerializeField]
        public int rewardMagnitude;

        public int RewardMagnitude => rewardMagnitude;

        [SerializeField]
        private Sprite rewardIcon;

        public Sprite RewardIcon => rewardIcon;
    }
}