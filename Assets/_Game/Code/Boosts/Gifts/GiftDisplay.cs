﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Boosts
{
    public class GiftDisplay : MonoBehaviour
    {
        [SerializeField]
        private Image icon;

        [SerializeField]
        private TextMeshProUGUI descriptionText;

        public void Display(Gift gift, bool premium, double magnitude)
        {
            GiftTierReward reward = premium ? gift.PremiumReward : gift.StandardReward;
            icon.sprite = reward.RewardIcon;
            descriptionText.text = gift.FormatDescription(CurrencyController.FormatToString(magnitude));
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}