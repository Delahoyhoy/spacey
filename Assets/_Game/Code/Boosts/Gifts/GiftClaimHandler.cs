﻿using System;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Boosts
{
    public class GiftClaimHandler : MonoBehaviour
    {
        [SerializeField]
        private GiftType giftType;

        [SerializeField]
        private GiftRewardMagnitudeType rewardMagnitudeType;
        
        [SerializeField] [ConditionalField("rewardMagnitudeType", false, GiftRewardMagnitudeType.Double)]
        private UnityEvent<double> onClaimDouble;

        [SerializeField] [ConditionalField("rewardMagnitudeType", false, GiftRewardMagnitudeType.Int)]
        private UnityEvent<int> onClaimInt;

        private void Start()
        {
            GiftManager.OnGiftClaimed += ClaimReward;
        }

        private void OnDestroy()
        {
            GiftManager.OnGiftClaimed -= ClaimReward;
        }
        
        public void ClaimReward(GiftType type, double rewardMagnitude)
        {
            if (!type.Equals(giftType))
                return;
            switch (rewardMagnitudeType)
            {
                case GiftRewardMagnitudeType.Double:
                    onClaimDouble?.Invoke(rewardMagnitude);
                    break;
                case GiftRewardMagnitudeType.Int:
                    onClaimInt?.Invoke((int)rewardMagnitude);
                    break;
            }
        }
    }
}