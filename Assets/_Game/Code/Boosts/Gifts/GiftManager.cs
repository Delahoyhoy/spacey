using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Reset;
using Core.Gameplay.Tutorial;
using Fumb.Save;
using Fumb.Serialization;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Boosts
{
    public class GiftManager : MonoBehaviour
    {
        private static GiftManager instance;
        public static GiftManager Instance => instance ??= ObjectFinder.FindObjectOfType<GiftManager>();
        
        [SerializeField]
        private Gift[] giftOptions;

        private int currentGiftSelection;

        [SerializeField]
        private RemoteGiftSettings giftSettings = new RemoteGiftSettings("gift_settings", new GiftSettings(true, 90, 150, 86400));

        [SerializeField]
        private UnityEvent onGiftSpawn;
        [SerializeField]
        private UnityEvent onGiftClaimed;
        
        private GiftSettings GiftSettings => giftSettings.Value;

        [ManagedSaveValue("GiftManagerFirstLogin")]
        private SaveValueDateTime saveFirstLogin;

        [SerializeField]
        private EventPather pathObject;

        private DateTime FreeOfferEnd => saveFirstLogin.DateTime.AddSeconds(GiftSettings.FreeOfferTime);

        private float countdownToNext;
        private double currentEarningRate;
        public double CurrentEarningRate => currentEarningRate;

        public static event Action<Gift> OnGiftSelected;
        public static event Action<GiftType, double> OnGiftClaimed;

        private float MinDelay => GiftSettings.MinDelay;
        private float MaxDelay => GiftSettings.MaxDelay;

        public bool OfferFree => UnbiasedTime.Instance.Now() <= FreeOfferEnd;

        public Gift CurrentGift => giftOptions[currentGiftSelection];

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            ResetManager.Instance.preReset += HandleReset;
            if (!saveFirstLogin.IsPopulated)
                saveFirstLogin.Value = new SerializableDateTime(UnbiasedTime.Instance.Now());
            countdownToNext = Random.Range(MinDelay, MaxDelay);
            
        }

        private void HandleReset(ResetType resetType)
        {
            countdownToNext = Random.Range(MinDelay, MaxDelay);
        }

        public void ClaimStandard()
        {
            double rewardMagnitude = CurrentGift.StandardReward.rewardMagnitude;
            if (CurrentGift.GiftType.Equals(GiftType.Gold))
                rewardMagnitude *= 60 * currentEarningRate;
            OnGiftClaimed?.Invoke(CurrentGift.GiftType, rewardMagnitude);
            onGiftClaimed?.Invoke();
        }
        
        public void ClaimPremium()
        {
            double rewardMagnitude = CurrentGift.PremiumReward.rewardMagnitude;
            if (CurrentGift.GiftType.Equals(GiftType.Gold))
                rewardMagnitude *= 60 * currentEarningRate;
            OnGiftClaimed?.Invoke(CurrentGift.GiftType, rewardMagnitude);
            onGiftClaimed?.Invoke();
        }

        public void SpawnGift()
        {
            countdownToNext = Random.Range(MinDelay, MaxDelay);
            currentEarningRate = GameManager.Instance.GetTotalEarnings(true);
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            if (ResetManager.Instance.resetting)
                return;
            if (currentEarningRate <= 0)
                return;
            currentGiftSelection = Random.Range(0, giftOptions.Length);
            onGiftSpawn?.Invoke();
            OnGiftSelected?.Invoke(CurrentGift);
        }

        private void Update()
        {
            if (pathObject.pathing)
                return;
            countdownToNext -= Time.deltaTime;
            if (countdownToNext > 0)
                return;
            SpawnGift();
        }
    }
}
