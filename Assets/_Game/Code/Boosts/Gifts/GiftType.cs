namespace Core.Gameplay.Boosts
{
    public enum GiftType
    {
        AugmentChest,
        Gold,
        Gems,
    }
}