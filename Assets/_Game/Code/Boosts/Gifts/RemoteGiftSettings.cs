﻿using System;
using Fumb.RemoteConfig;

namespace Core.Gameplay.Boosts
{
    [Serializable]
    public class RemoteGiftSettings : RemoteJson<GiftSettings>
    {
        public RemoteGiftSettings(string key, GiftSettings defaultValue) : base(key, defaultValue) { }
    }
}