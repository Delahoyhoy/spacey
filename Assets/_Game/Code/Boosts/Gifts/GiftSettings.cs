﻿using System;
using UnityEngine;

namespace Core.Gameplay.Boosts
{
    [Serializable]
    public class GiftSettings
    {
        [SerializeField]
        private bool giftsEnabled;

        public bool GiftsEnabled => giftsEnabled;

        [SerializeField]
        private float minDelay;

        public float MinDelay => minDelay;

        [SerializeField]
        private float maxDelay;

        public float MaxDelay => maxDelay;

        [SerializeField]
        private float freeOfferTime;

        public float FreeOfferTime => freeOfferTime;

        public GiftSettings(bool enabled, float min, float max, float freeTime)
        {
            giftsEnabled = enabled;
            minDelay = min;
            maxDelay = max;
            freeOfferTime = freeTime;
        }
    }
}