﻿using UnityEngine;

namespace Core.Gameplay.Boosts
{
    public class GiftScreen : MonoBehaviour
    {
        [SerializeField]
        private UIScreen screen;

        [SerializeField]
        private GiftDisplay standardDisplay;

        [SerializeField]
        private GiftDisplay premiumDisplay;

        public void Open()
        {
            screen.Open();
            GiftManager giftManager = GiftManager.Instance;
            bool freeOfferAvailable = giftManager.OfferFree;

            Gift currentGift = giftManager.CurrentGift;

            standardDisplay.SetActive(freeOfferAvailable);
            if (freeOfferAvailable)
            {
                double standardMagnitude = currentGift.StandardReward.rewardMagnitude;
                if (currentGift.GiftType == GiftType.Gold)
                    standardMagnitude *= 60 * giftManager.CurrentEarningRate;
                standardDisplay.Display(currentGift, false, standardMagnitude);
            }
            double premiumMagnitude = currentGift.PremiumReward.rewardMagnitude;
            if (currentGift.GiftType == GiftType.Gold)
                premiumMagnitude *= 60 * giftManager.CurrentEarningRate;
            premiumDisplay.Display(currentGift, true, premiumMagnitude);
        }

        public void ClaimStandard()
        {
            GiftManager.Instance.ClaimStandard();
            screen.Close();
        }
        
        public void ClaimPremium()
        {
            GiftManager.Instance.ClaimPremium();
            screen.Close();
        }
    }
}