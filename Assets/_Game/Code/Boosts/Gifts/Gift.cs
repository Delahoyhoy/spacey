using System;
using UnityEngine;

namespace Core.Gameplay.Boosts
{
    [Serializable]
    public struct Gift
    {
        [SerializeField]
        private GiftType giftType;

        public GiftType GiftType => giftType;

        [SerializeField]
        private float weight;

        public float Weight => weight;

        [SerializeField]
        private string descriptionFormat;

        [SerializeField]
        private GiftTierReward standardReward;

        public GiftTierReward StandardReward => standardReward;
        
        [SerializeField]
        private GiftTierReward premiumReward;

        public GiftTierReward PremiumReward => premiumReward;

        public string FormatDescription(params object[] content)
        {
            return string.Format(descriptionFormat, content);
        }


    }
}