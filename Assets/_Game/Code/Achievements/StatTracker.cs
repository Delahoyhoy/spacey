﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Save;
using Fumb.Save;
using UnityEngine;

public class StatTracker : MonoBehaviour, ISaveLoad
{
    private static StatTracker instance;
    public static StatTracker Instance { get { if (!instance) instance = ObjectFinder.FindObjectOfType<StatTracker>(); return instance; } }

    [ManagedSaveValue("StatTrackerStats")]
    private SaveValueDictionaryStringDouble saveStatData;

    public Action<string, double> onUpdateStat;

    [SerializeField]
    private string[] ids;

    public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
    {
        if (saveStatData.IsPopulated)
            return;
        saveStatData.Value = new Dictionary<string, double>();
        for (int i = 0; i < ids.Length; i++)
        {
            saveStatData.Value[ids[i]] = loadedData.LoadData<double>("stat_" + ids[i], 0);
            idsToRemove.Add("stat_" + ids[i]);
        }
    }

    public void LoadDefault()
    {
        if (!saveStatData.IsPopulated)
            saveStatData.Value = new Dictionary<string, double>();
        for (int i = 0; i < ids.Length; i++)
            saveStatData.Value[ids[i]] = 0;
    }

    public void Save(ref Dictionary<string, object> saveData)
    {
        // for (int i = 0; i < ids.Length; i++)
        // {
        //     saveData.Add("stat_" + ids[i], statData[ids[i]]);
        // }
    }

    public double IncrementStat(string id, double by)
    {
        if (!saveStatData.IsPopulated)
            saveStatData.Value = new Dictionary<string, double>();
        if (saveStatData.Value.ContainsKey(id))
            saveStatData.Value[id] += by;
        else
            saveStatData.Value[id] = by;
        onUpdateStat?.Invoke(id, saveStatData.Value[id]);
        return saveStatData.Value[id];
    }

    public void SetStat(string id, double to)
    {
        if (saveStatData.Value.ContainsKey(id))
            saveStatData.Value[id] = to;
        else
            saveStatData.Value[id] = to;
        onUpdateStat?.Invoke(id, to);
    }

    public double GetStat(string id)
    {
        if (!saveStatData.IsPopulated)
            return 0;
        if (saveStatData.Value.TryGetValue(id, out double value))
            return value;
        return 0;
    }
}
