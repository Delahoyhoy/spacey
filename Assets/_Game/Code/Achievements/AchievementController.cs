﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Save;
using Core.Gameplay.Version;
using Fumb.Save;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Achievements
{
    public class AchievementController : MonoBehaviour, ISaveLoad
    {

        public Achievement[] achievements;

        private Dictionary<string, Achievement> achievementDictionary = new Dictionary<string, Achievement>();

        private List<AchievementSlot> achievementSlots = new List<AchievementSlot>();

        private Dictionary<string, AchievementSlot> achievementSlotDictionary =
            new Dictionary<string, AchievementSlot>();

        [ManagedSaveValue("AchievementManagerTiers")]
        private SaveValueDictionaryStringInt savedCurrentTiers;

        [SerializeField]
        private AchievementSlot achievementSlotPrefab;

        [SerializeField]
        private RectTransform parent;

        [SerializeField]
        private UIScreen achievementScreen;

        public static AchievementController Instance
        {
            get
            {
                if (!instance) instance = FindObjectOfType<AchievementController>();
                return instance;
            }
        }

        private static AchievementController instance;

        // Use this for initialization
        void Awake()
        {
            instance = this;
            for (int i = 0; i < achievements.Length; i++)
            {
                //if (SaveManager.HasKey("ach" + achievements[i].id))
                //achievements[i].currentTier = SaveManager.GetInt("ach" + achievements[i].id, 0);
                if (!achievementDictionary.ContainsKey(achievements[i].id))
                {
                    achievementDictionary.Add(achievements[i].id, achievements[i]);
                }

                AchievementSlot newSlot = Instantiate(achievementSlotPrefab, parent);
                achievementSlotDictionary.Add(achievements[i].id, newSlot);
                //double currentValue = StatTracker.Instance.GetStat(achievements[i].statID);
                //newSlot.Setup(achievements[i].GetDescription(), achievements[i].currentTier, currentValue, achievements[i].GetCurrentRequirement(),
                //    achievements[i].GetCurrentReward(), achievements[i].id, this);
                achievementSlots.Add(newSlot);
            }
        }

        IEnumerator UpdateAchievements()
        {
            while (true)
            {
                if (achievementScreen.IsOpen)
                    ResetSlots();
                yield return new WaitForSeconds(1f);
            }
        }

        public bool GetAchievementReady()
        {
            for (int i = 0; i < achievements.Length; i++)
            {
                if (!achievements[i].ignore && achievements[i]
                    .GetHasEnoughForNext(StatTracker.Instance.GetStat(achievements[i].statID)))
                    return true;
            }

            return false;
        }

        public void OpenScreen()
        {
            if (achievementScreen)
                achievementScreen.Open();
            ResetSlots();
        }

        private void Start()
        {
            Setup();
            StartCoroutine(UpdateAchievements());
        }

        private void ResetSlots()
        {
            //for (int i = 0; i < achievementSlots.Count; i++)
            //    Destroy(achievementSlots[i].gameObject);
            //achievementSlots.Clear();
            Setup();
        }

        private void Setup()
        {
            for (int i = 0; i < achievements.Length; i++)
            {
                if (achievements[i].ignore)
                    continue;
                //AchievementSlot newSlot = Instantiate(achievementSlotPrefab, parent);
                //double currentValue = StatTracker.Instance.GetStat(achievements[i].statID);
                //newSlot.Setup(achievements[i].GetDescription(), achievements[i].currentTier, currentValue, achievements[i].GetCurrentRequirement(),
                //    achievements[i].GetCurrentReward(), achievements[i].id, this);
                //achievementSlots.Add(newSlot);
                double currentValue = StatTracker.Instance.GetStat(achievements[i].statID);
                if (achievementSlotDictionary.ContainsKey(achievements[i].id))
                    achievementSlotDictionary[achievements[i].id].Setup(achievements[i].GetDescription(),
                        achievements[i].currentTier, currentValue, achievements[i].GetCurrentRequirement(),
                        achievements[i].GetCurrentReward(), achievements[i].id, this, achievements[i].IsMax);
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(parent);
        }

        //private void Save()
        //{
        //    for (int i = 0; i < achievements.Length; i++)
        //    {
        //        PlayerPrefs.SetInt("ach" + achievements[i].id, achievements[i].currentTier);
        //    }
        //    PlayerPrefs.Save();
        //}

        public bool Claim(string id)
        {
            if (achievementDictionary[id].currentTier >= 5)
                return false;
            AudioManager.Play("Click", 1f);
            CurrencyController.Instance.AddCurrency(achievementDictionary[id].GetCurrentReward(),
                CurrencyType.HardCurrency, false, "achievement", $"achieve_{id}");
            achievementDictionary[id].currentTier++;

            //SaveManager.SetInt("ach" + id, achievementDictionary[id].currentTier);
            ResetSlots();
            return true;
            //Save();
        }

        public int GetCurrentLevel(string id)
        {
            return achievementDictionary[id].currentTier;
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            if (!savedCurrentTiers.IsPopulated)
                savedCurrentTiers.Value = new Dictionary<string, int>();
            //throw new System.NotImplementedException();
            for (int i = 0; i < achievements.Length; i++)
            {
                string id = achievements[i].id;
                if (!savedCurrentTiers.Value.ContainsKey(id))
                    savedCurrentTiers.Value.Add(id, achievementDictionary[achievements[i].id].currentTier);
                else
                    savedCurrentTiers.Value[id] = achievementDictionary[achievements[i].id].currentTier;
            }
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            if (!savedCurrentTiers.IsPopulated)
                savedCurrentTiers.Value = new Dictionary<string, int>();
            //throw new System.NotImplementedException();
            for (int i = 0; i < achievements.Length; i++)
            {
                if (savedCurrentTiers.Value.TryGetValue(achievements[i].id, out int tier))
                    achievementDictionary[achievements[i].id].currentTier = tier;
                else
                {
                    achievementDictionary[achievements[i].id].currentTier =
                        loadedData.LoadData("ach" + achievements[i].id, 0);
                    idsToRemove.Add("ach" + achievements[i].id);
                }

                bool ignore = achievements[i].ShouldIgnore(GameVersionManager.CurrentVersion != GameVersion.Version1);
                if (ignore)
                    achievementSlots[i].gameObject.SetActive(false);
            }
        }

        public void RecycleAchievements()
        {
            for (int i = 0; i < achievements.Length; i++)
            {
                //achievementDictionary[achievements[i].id].currentTier = loadedData.LoadData("ach" + achievements[i].id, 0);
                bool ignore = achievements[i].ShouldIgnore(GameVersionManager.CurrentVersion != GameVersion.Version1);
                if (ignore)
                    achievementSlots[i].gameObject.SetActive(false);
                else
                    achievementSlots[i].gameObject.SetActive(true);
            }
        }

        public void LoadDefault()
        {
            if (!savedCurrentTiers.IsPopulated)
                savedCurrentTiers.Value = new Dictionary<string, int>();
            //throw new System.NotImplementedException();
            for (int i = 0; i < achievements.Length; i++)
            {
                bool ignore = achievements[i].ShouldIgnore(GameVersionManager.CurrentVersion != GameVersion.Version1);
                if (ignore)
                    achievementSlots[i].gameObject.SetActive(false);
            }
        }
    }

}