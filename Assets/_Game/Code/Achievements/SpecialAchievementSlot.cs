﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Achievements
{
    public class SpecialAchievementSlot : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent OnTrigger;

        [SerializeField]
        private string statID;

        [SerializeField]
        private double gemReward = 15;

        public void Setup()
        {
            if (StatTracker.Instance.GetStat(statID) > 0)
                gameObject.SetActive(false);
        }

        public void Trigger()
        {
            gameObject.SetActive(false);
            OnTrigger.Invoke();
            StatTracker.Instance.IncrementStat(statID, 1);
            CoinBurster.Instance.AddGemsWithEffect(15);
        }

        private void Start()
        {
            Setup();
        }
    }
}
