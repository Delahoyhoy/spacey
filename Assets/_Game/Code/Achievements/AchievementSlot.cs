﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;

namespace Core.Gameplay.Achievements
{
    public class AchievementSlot : MonoBehaviour
    {

        [SerializeField]
        private Text description, current, reward;

        [SerializeField]
        private GameObject[] stars;

        private AchievementController controller;

        [SerializeField]
        private Button claimButton;

        [SerializeField]
        private Color activeColor = Color.white;

        private string id;

        public void Setup(string descriptionText, int currentLevel, double currentValue, double maxValue,
            double rewardValue, string achievementId, AchievementController achievementController, bool isMax = false)
        {
            description.text = descriptionText;
            for (int i = 0; i < stars.Length; i++)
                stars[i].SetActive(i < currentLevel);
            
            
            id = achievementId;
            controller = achievementController;

            if (isMax)
            {
                current.text = CurrencyController.FormatToString(currentValue);
                reward.text = "MAX";
                reward.color = activeColor;
                claimButton.interactable = false;
                return;
            }

            current.text = CurrencyController.FormatToString(currentValue) + "/" +
                           CurrencyController.FormatToString(maxValue);
            reward.text = "+" + CurrencyController.FormatToString(rewardValue);
            if (currentValue >= maxValue)
            {
                claimButton.interactable = true;
                reward.color = activeColor;
            }
            else
            {
                claimButton.interactable = false;
                reward.color = Color.red;
            }
        }

        public void Claim()
        {
            if (!controller.Claim(id)) 
                return;
            AudioManager.Play("Milestone", 1f);
            CoinBurster.Instance.NewBurst(CurrencyType.HardCurrency, reward.transform,
                controller.GetCurrentLevel(id) + 1);
        }
    }

}