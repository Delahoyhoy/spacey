namespace Core.Gameplay.Achievements
{
    public enum AchievementProfile
    {
        Both,
        NewSystem,
        OldSystem,
    }
}