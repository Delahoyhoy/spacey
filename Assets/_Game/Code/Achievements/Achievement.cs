using System;

namespace Core.Gameplay.Achievements
{
    [Serializable]
    public class Achievement
    {
        public string id, format, statID;
        public double[] tierRequirements = new double[3];
        public int[] tierRewards = new int[3];

        public bool ignore = false;
        public AchievementProfile profile = AchievementProfile.Both;

        public bool ShouldIgnore(bool usingNewSystem)
        {
            bool shouldIgnore = !(profile == AchievementProfile.Both ||
                                  (usingNewSystem && profile == AchievementProfile.NewSystem) ||
                                  (!usingNewSystem && profile == AchievementProfile.OldSystem));
            ignore = shouldIgnore;
            return shouldIgnore;
        }

        public int currentTier = 0;

        public string GetFormatedTextAtTier(int tier)
        {
            if (tier < tierRequirements.Length)
                return string.Format(format, CurrencyController.FormatToString(tierRequirements[tier]));
            return string.Format(format,
                CurrencyController.FormatToString(tierRequirements[tierRequirements.Length - 1]));
        }

        public double GetReqAtTier(int tier)
        {
            if (tier < tierRequirements.Length)
                return tierRequirements[tier];
            return tierRequirements[tierRequirements.Length - 1];
        }

        public string GetDescription()
        {
            return GetFormatedTextAtTier(currentTier);
        }

        public double GetCurrentRequirement()
        {
            return GetReqAtTier(currentTier);
        }

        public double GetCurrentReward()
        {
            return GetRewardAtTier(currentTier);
        }

        public bool IsMax
        {
            get { return currentTier >= tierRequirements.Length; }
        }

        public int GetRewardAtTier(int tier)
        {
            if (tier < tierRewards.Length)
                return tierRewards[tier];
            return tierRewards[tierRewards.Length - 1];
        }

        public bool GetHasEnoughForNext(double currentValue)
        {
            if (currentTier >= tierRequirements.Length)
                return false;
            return currentValue >= GetCurrentRequirement();
        }
    }
}