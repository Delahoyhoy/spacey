using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class ScreenPosHelper : MonoBehaviour
    {
        private void Update()
        {
            Debug.Log($"{transform.position.y} / {Screen.height} : {Camera.main.ScreenToViewportPoint(transform.position).y}");
        }
    }
}
