﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Events;

[System.Serializable]
public struct FumbAd
{
    public string videoURL, appURL;
}

public class TestAd : MonoBehaviour {

    [SerializeField]
    private VideoPlayer videoPlayer;

    [SerializeField]
    private FumbAd[] urls;

    [SerializeField]
    private Image closeFill;

    [SerializeField]
    private Button closeButton, targetButton;

    [SerializeField]
    private GameObject adPanel;

    private delegate void AdResultCallback(AdResult adResult);

    private UnityAction<AdResult> resultCallback;

    

    private string targetURL = "https://google.com";
    private bool finished = false, ready = false;

    public bool AdReady { get { return Application.internetReachability != NetworkReachability.NotReachable && 
                videoPlayer.isPrepared; } }

    public static bool Ready { get { if (Instance) return Instance.AdReady; return false; } }

    public void Setup()
    {
        FumbAd chosenAd = urls[Random.Range(0, urls.Length)];
        videoPlayer.url = chosenAd.videoURL;
        targetURL = chosenAd.appURL;
        videoPlayer.Prepare();
    }

    public void OpenTarget()
    {
        Application.OpenURL(targetURL);
        Complete();
    }

    public static TestAd Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        videoPlayer.prepareCompleted += PrepareCompleted;
    }

    // Use this for initialization
    void Start () {
        Setup();
	}

    public void Close()
    {
        AudioManager.Instance.TempMute(false);
        videoPlayer.Stop();
        adPanel.SetActive(false);
        Setup();
    }

    public void ShowAd(UnityAction<AdResult> callback)
    {
        if (AdReady)
        {
            finished = false;
            closeButton.interactable = false;
            targetButton.interactable = false;
            videoPlayer.Play();
            AudioManager.Instance.TempMute(true);
            adPanel.SetActive(true);
            resultCallback = callback;
        }
        else
            callback.Invoke(AdResult.Fail);
    }

    public void Complete()
    {
        if (resultCallback != null)
        {
            resultCallback.Invoke(AdResult.Success);
        }
        StatTracker.Instance.IncrementStat("adsWatched", 1);
        Close();
    }

    public void Skip()
    {
        if (resultCallback != null)
        {
            resultCallback.Invoke(AdResult.Skip);
        }
        Close();
    }
	
	// Update is called once per frame
	void Update () {
		if (videoPlayer.isPlaying)
        {
            //double totalTime = videoPlayer.frameCount * videoPlayer.frameRate;
            closeFill.fillAmount = (float)videoPlayer.frame / videoPlayer.frameCount;
        }
        if (!finished && !videoPlayer.isPlaying)
        {
            finished = true;
            closeButton.interactable = true;
            targetButton.interactable = true;
        }
	}

    private void PrepareCompleted(VideoPlayer source)
    {
        Debug.Log("Ready in " + Time.time);
    }
}
