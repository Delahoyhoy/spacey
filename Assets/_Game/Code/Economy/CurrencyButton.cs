﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Economy
{
    [RequireComponent(typeof(Button))]
    public class CurrencyButton : CurrencyUpdateAction
    {
        [SerializeField]
        private double threshold;

        [SerializeField]
        private UnityEvent<string> onSetThreshold;

        [SerializeField]
        private UnityEvent<bool> onSetAffordable;

        private double cachedCurrentValue;
        
        private Button button;

        private Button Button
        {
            get
            {
                if (buttonInitialised)
                    return button;
                button = GetComponent<Button>();
                buttonInitialised = true;
                return button;
            }
        }
        
        private bool buttonInitialised;

        private void Awake()
        {
            button = GetComponent<Button>();
            buttonInitialised = true;
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
        
        protected override void Initialise()
        {
            onSetThreshold?.Invoke(CurrencyController.FormatToString(threshold));
            
            UpdateCurrent(CurrencyController.Instance.GetCurrency(currencyType));
            onCurrencyUpdate += UpdateCurrent;
        }

        public void SetThreshold(double newThreshold)
        {
            threshold = newThreshold;
            onSetThreshold?.Invoke(CurrencyController.FormatToString(threshold));
            UpdateCurrent(cachedCurrentValue);
        }
        

        private void UpdateCurrent(double value)
        {
            cachedCurrentValue = value;
            bool affordable = value >= threshold;
            Button.interactable = affordable;
            onSetAffordable?.Invoke(affordable);
        }
    }
}