﻿using UnityEngine;

namespace Core.Gameplay.Economy
{
    public class EnsureCurrencyAmount : MonoBehaviour
    {
        [SerializeField]
        private CurrencyType currencyType;

        public void EnsureCurrency(float amount)
        {
            EnsureHasAtLeast(amount);
        }

        protected void EnsureHasAtLeast(double amount)
        {
            double currentAmount = CurrencyController.Instance.GetCurrency(currencyType);
            if (currentAmount >= amount)
                return;
            CurrencyController.Instance.AddCurrency(amount - currentAmount, currencyType);
        }
    }
}