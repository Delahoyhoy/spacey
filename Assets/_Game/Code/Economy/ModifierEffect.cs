﻿using System;
using Core.Gameplay.Sectors;
using UnityEngine;

namespace Core.Gameplay.Economy
{
    [Serializable]
    public class ModifierEffect
    {
        [SerializeField]
        private SectorModifier modifier;

        private float value = 1;
        private bool initialised;

        public ModifierEffect(SectorModifier mod)
        {
            modifier = mod;
        }

        ~ModifierEffect()
        {
            if (!initialised)
                return;
            if (SectorModifierManager.Instance)
                SectorModifierManager.Instance.onSetModifier -= UpdateValue;
        }

        public float Value
        {
            get
            {
                if (initialised)
                    return value;
                value = GlobalModifierManager.Instance.GetModifier(modifier);
                SectorModifierManager.Instance.onSetModifier += UpdateValue;
                initialised = true;
                return value;
            }
        }

        private void UpdateValue(SectorModifier mod, float newValue)
        {
            if (modifier == mod)
                value = newValue;
        }
    }
}