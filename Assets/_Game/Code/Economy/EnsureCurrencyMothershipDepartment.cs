﻿using Core.Gameplay.Mothership;
using UnityEngine;

namespace Core.Gameplay.Economy
{
    public class EnsureCurrencyMothershipDepartment : EnsureCurrencyAmount
    {
        [SerializeField]
        private MothershipDepartment department;

        public void EnsureUpgradable()
        {
            EnsureHasAtLeast(MothershipManager.Instance.GetDepartmentLevelUpCost(department));
        }
    }
}