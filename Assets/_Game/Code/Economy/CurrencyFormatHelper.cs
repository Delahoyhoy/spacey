﻿public static class CurrencyFormatHelper
{
    public static string FormatAsCurrency(this double value)
    {
        return CurrencyController.FormatToString(value);
    }
}