﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Globalization;
using Core.Gameplay.Reset;
using Core.Gameplay.Save;
using Core.Gameplay.StarFragments;
using Core.Gameplay.UserInterface;
using Core.Gameplay.Version;
using Data;
using Fumb.Analytics;
using Fumb.Data;
using Zebedee;

public enum CurrencyType { SoftCurrency, Satoshis, HardCurrency, Vault, StarFragments, PrestigeCurrency };

public class CurrencyController : MonoBehaviour, ISaveLoad
{
    [SerializeField]
    Text topCurrencyAmountGold;

    [SerializeField]
    Text topCurrencyAmountDiamonds;

    [SerializeField]
    private NumberCounterUpper currencyIndicator;

    [SerializeField]
    GameObject originalGoldRewardPrefab;

    [SerializeField]
    private Text popoffPrefab;

    [SerializeField]
    private Transform popoffParent;

    private Queue<Text> popoffPool = new Queue<Text>();

    public Action<CurrencyType, double> onCurrencyUpdate;

    public static CurrencyController Instance
    {
        get
        {
            if (!instance) instance = FindObjectOfType<CurrencyController>();
            return instance;
        }
    }

    private static CurrencyController instance;

    private double totalDiamonds;
    private double totalGoldEver;
    private double totalGoldThisRound;
    private double totalGold;
    private double resetAmount = 1;
    private double resetMultiplier = 0.000000001d;
    private int timesGameReset = 0;
    bool wasLoaded = false;

    private float prestigeAmountMultiplier = 1f;

    private int timesReset = 0;

    private PrestigeData PrestigeData => ResetManager.Instance.PrestigeSetup;
    
    private double prestigeCurrency = 0d, prestigeMult = 1d;

    // Public Access Properties

    public double CurrentGold => GetCurrency(CurrencyType.SoftCurrency);
    public double TotalGoldEver => totalGoldEver;
    public double CurrentDiamonds => GetCurrency(CurrencyType.HardCurrency);
    public double CurrentPrestige => prestigeCurrency;
    public double PrestigeMultiplier => prestigeMult < 1 ? 1 : prestigeMult;

    bool isRunningCoroutine = false;

    public void AddGoldWithEffect(double toAdd)
    {
        AddCurrency(toAdd, CurrencyType.SoftCurrency, true);
    }

    public void SetPrestigeAmountMultiplier(float multiplier)
    {
        prestigeAmountMultiplier = multiplier;
    }

    //double currencyBeforeWait = 0;
    double perSecondValue = 0;

    public void LoadCurrency(double amtOfGold, double amtOfGoldEver, double amtOfGThisRound, double amtOfDiamonds,
        double resetAmt)
    {
        totalGold = amtOfGold;
        onCurrencyUpdate?.Invoke(CurrencyType.SoftCurrency, totalGold);
        totalGoldEver = amtOfGoldEver;
        totalGoldThisRound = amtOfGThisRound;
        totalDiamonds = amtOfDiamonds;
        onCurrencyUpdate?.Invoke(CurrencyType.HardCurrency, totalDiamonds);
        resetAmount = resetAmt;
        wasLoaded = true;

        ZebedeeManager.Instance.OnSatsValueChange += amount =>
        {
            onCurrencyUpdate?.Invoke(CurrencyType.Satoshis, amount);
        };

        for (int i = 0; i < Enum.GetValues(typeof(CurrencyType)).Length; i++)
            onCurrencyUpdate?.Invoke((CurrencyType)i, GetCurrency((CurrencyType)i));

        currencyIndicator.SetVisualUpdate(GetCurrency(CurrencyType.SoftCurrency));
        //topCurrencyAmountGold.text = CurrencyToString(GetCurrency(CurrencyType.Gold));
        topCurrencyAmountDiamonds.text = CurrencyToString(GetCurrency(CurrencyType.HardCurrency));
    }

    public void AddCurrency(double amtToAdd, CurrencyType type, bool visually = false, params string[] sinkOrSourceInfo)
    {
        if (amtToAdd == 0) return;

        switch (type)
        {
            case CurrencyType.SoftCurrency:
                double totalAfter = totalGold + amtToAdd;
                if (double.IsNaN(totalAfter) || double.IsInfinity(totalAfter))
                {
                    NotificationDisplay.Instance.Display(
                        "SOMETHING CAUSED YOUR GOLD TO INCREASE TO NaN,\n PLEASE REPORT THIS TO THE DEVELOPERS");
                    return;
                }

                totalGold = totalAfter;
                if (amtToAdd >= 0)
                {
                    totalGoldEver += amtToAdd;
                    totalGoldThisRound += amtToAdd;
                    StatTracker.Instance.SetStat("goldEarned", totalGoldEver);
                    //TutorialManager.OnIncrementStage("earnGold", totalGold);
                }
                else
                {
                    StatTracker.Instance.IncrementStat("goldSpent", -amtToAdd);
                }

                onCurrencyUpdate?.Invoke(CurrencyType.SoftCurrency, totalGold);
                break;
            case CurrencyType.Satoshis:
                int sats = amtToAdd < int.MaxValue ? (int)amtToAdd : int.MaxValue;
                ZebedeeManager.Instance.AddSats(sats);
                break;
            case CurrencyType.HardCurrency:
                totalDiamonds += amtToAdd;
                onCurrencyUpdate?.Invoke(CurrencyType.HardCurrency, totalDiamonds);
                break;
            case CurrencyType.Vault:
                Vault.Instance.AddGemsToVault((int)amtToAdd);
                break;
            case CurrencyType.StarFragments:
                StarFragmentManager.Instance.Fragments += (int)amtToAdd;
                break;
            default:
                break;
        }
        
        if (sinkOrSourceInfo.Length > 0)
            CurrencyEvent(type, amtToAdd, sinkOrSourceInfo);

        if (visually && amtToAdd > 0)
        {
            SpawnReward(amtToAdd, type);
        }

        currencyIndicator.SetVisualUpdate(GetCurrency(CurrencyType.SoftCurrency));
        //topCurrencyAmountGold.text = CurrencyToString(GetCurrency(CurrencyType.Gold));
        topCurrencyAmountDiamonds.text = CurrencyToString(GetCurrency(CurrencyType.HardCurrency));
    }

    public void CurrencyEvent(CurrencyType currencyType, double amount, params string[] info)
    {
        if (currencyType.Equals(CurrencyType.Vault))
            return;
        
        if (amount == 0)
            return;
        
        if (amount < 0)
        {
            AnalyticsEventCurrencySpend spendEvent = new AnalyticsEventCurrencySpend(((int)(currencyType)).ToString(),
                currencyType.ToString(), -amount, true, info);
            AnalyticsManager.SendAnalyticEvent(spendEvent);
            return;
        }
        AnalyticsEventCurrencyEarn earnEvent = new AnalyticsEventCurrencyEarn(((int)(currencyType)).ToString(),
            currencyType.ToString(), amount, true, info);
        AnalyticsManager.SendAnalyticEvent(earnEvent);
    }

    public static string FormatToString(double value)
    {
        if (GameManager.useScientific)
            return GetScientificFormat(value);
        return CurrencyToString(value);
    }

    public static string GetScientificFormat(double value)
    {
        if (value < 1000)
            return Math.Round(value).ToString();
        if (value < 10000)
            return $"{value:0,0}";
        int expLevel = 0;
        double mantissa = value;
        while (mantissa >= 10)
        {
            mantissa /= 10;
            expLevel++;
            if (expLevel > 500)
                break;
        }

        return $"{mantissa:0.00}e{expLevel}";
    }


    public double GetCurrency(CurrencyType type)
    {
        switch (type)
        {
            case CurrencyType.SoftCurrency:
                return totalGold;
            case CurrencyType.Satoshis:
                return ZebedeeManager.Instance.Satoshis;
            case CurrencyType.HardCurrency:
                return totalDiamonds;
            default:
                break;
        }

        return 0;
    }

    public bool SpendCurrency(double amount, CurrencyType currencyType, params string[] spendInfo)
    {
        if (GetCurrency(currencyType) < amount)
            return false;
        AddCurrency(-amount, currencyType, false, spendInfo);
        return true;
    }

    public void EnsureGold(double gold)
    {
        if (GetCurrency(CurrencyType.SoftCurrency) < gold)
        {
            double required = gold - totalGold;
            AddCurrency(required, CurrencyType.SoftCurrency, false);
        }
    }

    public void EnsureAtLeast(int gold)
    {
        EnsureGold(gold);
    }

    public void ResetCurrency()
    {
        double prestigeBefore = prestigeCurrency;
        double toAdd = GetPrestigeToAdd();

        prestigeCurrency += toAdd; 
        prestigeMult = 1d + (prestigeCurrency * PrestigeData.PrestigeExchange);
        
        CurrencyEvent(CurrencyType.PrestigeCurrency, toAdd, "prestige", "reset");

        GlobalData globalData = GlobalDataManager.Instance.Data;
        
        if (totalGold > globalData.DefaultGold)
            CurrencyEvent(CurrencyType.SoftCurrency, -(totalGold - globalData.DefaultGold), "reset_soft_currency", "prestige");
        
        totalGold = globalData.DefaultGold;
        totalGoldThisRound = totalGold;
        if (currencyIndicator)
        {
            currencyIndicator.ResetCounter(CurrentGold);
        }

        RecalculatePrestigeMultiplier();
        timesReset++;
        StatTracker.Instance.IncrementStat("timesPrestiged", 1);
    }

    public double GetPrestigeToAdd()
    {
        return GetPrestigeToAdd(totalGoldThisRound);
    }

    private double GetPrestigeToAdd(double fromGold)
    {
        if (GameVersionManager.CurrentVersion < GameVersion.Version3)
            return Math.Floor(Math.Pow(fromGold / 1e5, 0.25d));
        return PrestigeData.GetPrestigeCurrency(fromGold) * prestigeAmountMultiplier;
        //float numerator = (float)(totalGold / 1e6);
        //return Mathf.FloorToInt(Mathf.Pow(numerator, 0.25f));
    }

    private static readonly string[] Suffix = new string[]
    {
        "", "k", "m", "b", "t", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
        "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "II", "JJ", "KK", "LL", "MM", "NN"
    };

    public static string CurrencyToString(double valueToConvert)
    {
        int scale = 0;
        double v = valueToConvert;
        while (v >= 1000d)
        {
            v /= 1000d;
            scale++;
            if (scale >= Suffix.Length)
                return
                    ((double)valueToConvert).ToString("e2"); // overflow, can't display number, fallback to exponential
        }

        string endString = "";
        endString = v.ToString(scale == 0 ? "0" : "0.###", CultureInfo.InvariantCulture);
        //if (scale == 0)
        //{
        if (!endString.Contains("."))
            return endString + Suffix[scale];
        if (endString.Length > 4)
        {
            endString = endString.Remove(endString.Length - (endString.Length - 4));
        }

        if (endString[endString.Length - 1] == ".".ToCharArray()[0])
        {
            endString = endString.Replace(".", "");
        }

        string separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        if (separator != ".")
            endString = endString.Replace(".", separator);

        return endString + Suffix[scale];
    }

    void SpawnReward(double amtToAdd, CurrencyType type)
    {
        //GameObject temp = Instantiate(originalGoldRewardPrefab, originalGoldRewardPrefab.transform.parent);
        //temp.GetComponent<RewardGoldController>().Setup(amtToAdd);
        if (type != CurrencyType.SoftCurrency)
            return;
        Text newPopoff = NewPopoff();
        newPopoff.text = FormatToString(amtToAdd);
        StartCoroutine(DelayedEnqueue(newPopoff));
        newPopoff.transform.localPosition = Vector3.zero;
    }

    public void SpawnReward(double amount, CurrencyType type, Vector3 worldPos)
    {
        if (type != CurrencyType.SoftCurrency)
            return;
        Text newPopoff = NewPopoff();
        newPopoff.text = FormatToString(amount);
        StartCoroutine(DelayedEnqueue(newPopoff));
        newPopoff.transform.position = worldPos;
    }

    private IEnumerator DelayedEnqueue(Text popoff)
    {
        yield return new WaitForSeconds(1f);
        popoff.gameObject.SetActive(false);
        popoffPool.Enqueue(popoff);
    }

    public void Save(ref Dictionary<string, object> saveData)
    {
        saveData.Add("gold", totalGold);
        saveData.Add("totalGold", totalGoldEver);
        saveData.Add("prestige", prestigeCurrency);
        saveData.Add("totalThisRound", totalGoldThisRound);
        saveData.Add("diamonds", totalDiamonds);
        saveData.Add("timesReset", timesGameReset);
    }

    public void RecalculatePrestigeMultiplier()
    {
        if (prestigeCurrency > 0)
        {
            prestigeMult = 1d + (prestigeCurrency / 100d);
        }
        else
            prestigeMult = 1d;
    }

    public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
    {
        totalGold = loadedData.LoadData("gold", 0d);
        totalGoldEver = loadedData.LoadData("totalGold", 0d);
        prestigeCurrency = loadedData.LoadData("prestige", 0d);
        totalGoldThisRound = loadedData.LoadData("totalThisRound", 0d);
        totalDiamonds = loadedData.LoadData("diamonds", 10d);
        timesReset = loadedData.LoadData("timesReset", 0);
        wasLoaded = true;
        currencyIndicator.SetVisualUpdate(GetCurrency(CurrencyType.SoftCurrency));
        //topCurrencyAmountGold.text = CurrencyToString(GetCurrency(CurrencyType.Gold));
        topCurrencyAmountDiamonds.text = CurrencyToString(GetCurrency(CurrencyType.HardCurrency));
        RecalculatePrestigeMultiplier();
        //StartCoroutine(TrackEarningsLoop());

        onCurrencyUpdate?.Invoke(CurrencyType.SoftCurrency, totalGold);
        onCurrencyUpdate?.Invoke(CurrencyType.HardCurrency, totalDiamonds);

        ZebedeeManager.Instance.OnSatsValueChange += amount =>
        {
            onCurrencyUpdate?.Invoke(CurrencyType.Satoshis, amount);
        };

        for (int i = 0; i < Enum.GetValues(typeof(CurrencyType)).Length; i++)
            onCurrencyUpdate?.Invoke((CurrencyType)i, GetCurrency((CurrencyType)i));

        if (double.IsNaN(totalGold) || double.IsInfinity(totalGold))
        {
            Invoke(nameof(SetupAfterReset), 5f);
        }
    }

    private void SetupAfterReset()
    {
        NotificationDisplay.Instance.Display("YOUR GOLD LOADED AS NaN, PLEASE REPORT THIS TO THE DEVELOPER");
        ResetMoney();
        GameManager.Instance.AddHoursOfGold(8);
    }

    public void AddNaNGold()
    {
        AddCurrency(double.PositiveInfinity, CurrencyType.SoftCurrency);
    }

    public Text NewPopoff()
    {
        if (popoffPool.Count > 0)
        {
            Text newPopoff = popoffPool.Dequeue();
            newPopoff.gameObject.SetActive(true);
            newPopoff.transform.SetAsLastSibling();
            return newPopoff;
        }
        else
        {
            return Instantiate(popoffPrefab, popoffParent);
        }
    }

    public void ResetMoney()
    {
        totalGold = 0;
        totalGoldEver = 0;
        totalGoldThisRound = 0;
        currencyIndicator.ResetCounter(0);
    }

    public void AddGold(float gold)
    {
        AddCurrency(gold, CurrencyType.SoftCurrency, false);
    }

    public void AddGems(int gems)
    {
        AddCurrency(gems, CurrencyType.HardCurrency, false);
    }

    public void LoadDefault()
    {
        GlobalData globalData = GlobalDataManager.Instance.Data;
        totalGold = globalData.DefaultGold;
        prestigeCurrency = 0d;
        totalGoldEver = totalGold;
        totalGoldThisRound = totalGold;
        totalDiamonds = globalData.DefaultDiamonds;
        timesGameReset = 0;
        currencyIndicator.SetVisualUpdate(GetCurrency(CurrencyType.SoftCurrency));
        //topCurrencyAmountGold.text = CurrencyToString(GetCurrency(CurrencyType.Gold));
        topCurrencyAmountDiamonds.text = CurrencyToString(GetCurrency(CurrencyType.HardCurrency));
        RecalculatePrestigeMultiplier();
        //throw new NotImplementedException();

        ZebedeeManager.Instance.OnSatsValueChange += amount =>
        {
            onCurrencyUpdate?.Invoke(CurrencyType.Satoshis, amount);
        };

        for (int i = 0; i < Enum.GetValues(typeof(CurrencyType)).Length; i++)
            onCurrencyUpdate?.Invoke((CurrencyType)i, GetCurrency((CurrencyType)i));
    }
}
