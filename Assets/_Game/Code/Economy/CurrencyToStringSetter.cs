using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Economy
{
    public class CurrencyToStringSetter : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<string> onSetValue;

        public void SetValue(double value)
        {
            onSetValue?.Invoke(CurrencyController.FormatToString(value));
        }
    }
}
