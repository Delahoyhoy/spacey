using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Economy
{
    public class CurrencyUpdateUnityEvent : CurrencyUpdateAction
    {
        [SerializeField]
        private UnityEvent<double> currencyUpdated;

        protected override void Initialise()
        {
            onCurrencyUpdate += amount => currencyUpdated?.Invoke(amount);
        }
    }
}
