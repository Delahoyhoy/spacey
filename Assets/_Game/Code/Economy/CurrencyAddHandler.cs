﻿using UnityEngine;

namespace Core.Gameplay.Economy
{
    public class CurrencyAddHandler : MonoBehaviour
    {
        [SerializeField]
        private CurrencyType currencyType;
        
        [SerializeField]
        private string[] placementInfo;

        public void AddCurrency(double amount)
        {
            CurrencyController.Instance.AddCurrency(amount, currencyType, false, placementInfo);
        }

        public void AddIntCurrency(int amount)
        {
            AddCurrency(amount);
        }
    }
}