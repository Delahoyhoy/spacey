using System;
using UnityEngine;

namespace Core.Gameplay.Economy
{
    public class CurrencyUpdateAction : MonoBehaviour
    {
        [SerializeField]
        protected CurrencyType currencyType;

        protected Action<double> onCurrencyUpdate;

        private bool initialised;

        protected virtual void Start()
        {
            if (initialised)
                return;
            Initialise();
            CurrencyController.Instance.onCurrencyUpdate += OnCurrencyUpdate;
            onCurrencyUpdate?.Invoke(CurrencyController.Instance.GetCurrency(currencyType));
            initialised = true;
        }

        protected virtual void Initialise()
        {
            // Do initialisation actions here instead of on start per script
        }

        private void OnCurrencyUpdate(CurrencyType type, double newAmount)
        {
            if (currencyType.Equals(type))
                onCurrencyUpdate?.Invoke(newAmount);
        }
    }
}
