﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Sectors;
using UnityEngine;

namespace Core.Gameplay.Economy
{
    public class GlobalModifierManager : MonoBehaviour
    {
        private static GlobalModifierManager instance;
        public static GlobalModifierManager Instance => ObjectFinder.FindObjectOfType<GlobalModifierManager>();

        private Dictionary<SectorModifier, float> activeModifiers = new Dictionary<SectorModifier, float>();

        private bool initialised;

        private void Start()
        {
            if (initialised)
                return;
            SectorModifierManager.Instance.onSetModifier += SetModifier;
        }

        public void SetModifier(SectorModifier modifier, float value)
        {
            activeModifiers[modifier] = value;
        }

        public float GetModifier(SectorModifier modifier)
        {
            if (activeModifiers.TryGetValue(modifier, out float value))
                return value;
            return 1;
        }
    }
}