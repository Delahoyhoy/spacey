using System;
using System.Collections;
using System.Collections.Generic;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Economy
{
    public class CurrencyThresholdAction : CurrencyUpdateAction
    {
        [SerializeField]
        private double threshold;

        [SerializeField]
        private bool activateAboveThreshold;

        [SerializeField] [ConditionalField("activateAboveThreshold")]
        private UnityEvent aboveThresholdAction;

        [SerializeField]
        private bool activateAtThreshold;

        [SerializeField] [ConditionalField("activateAtThreshold")]
        private UnityEvent atThresholdAction;

        [SerializeField]
        private bool activateBelowThreshold;

        [SerializeField] [ConditionalField("activateBelowThreshold")]
        private UnityEvent belowThresholdAction;

        protected override void Initialise()
        {
            onCurrencyUpdate += CheckThreshold;
        }

        private void CheckThreshold(double value)
        {
            if (value > threshold && activateAboveThreshold)
                aboveThresholdAction?.Invoke();
            else if (Math.Abs(value - threshold) < 0.000001 && activateAtThreshold)
                atThresholdAction?.Invoke();
            else if (value < threshold && activateBelowThreshold)
                belowThresholdAction?.Invoke();
        }
    }
}
