﻿using System;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class ChunkPreGenData
    {
        [SerializeField]
        private ChunkSize chunkSize;

        public int Layers => (int)chunkSize;
        
        [SerializeField]
        private bool hasReward;

        [SerializeField]
        private ResourceChunkReward chunkReward;

        public bool GetReward(out ResourceChunkReward reward)
        {
            reward = chunkReward;
            return hasReward;
        }
    }
}