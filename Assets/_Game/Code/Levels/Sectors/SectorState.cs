﻿namespace Core.Gameplay.Sectors
{
    public enum SectorState
    {
        Unvisited,
        Available,
        Unreachable,
        InProgress,
        Completed,
    }
}