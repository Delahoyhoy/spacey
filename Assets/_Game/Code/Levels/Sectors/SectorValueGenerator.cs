﻿using System;
using System.Linq;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Utilities;
using Data;
using Fumb.Data;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorValueGenerator
    {
        [SerializeField]
        private MineralDistributionController[] mineralDistributions;

        [SerializeField]
        private double minStepOffset = -1, maxStepOffset = 3;

        [SerializeField]
        private float maxVariance = 0.2f;

        [SerializeField]
        private float minThreshold = 0.05f;

        [SerializeField]
        private float distributionStepRate = 1f;
        
        private bool initialised;
        private double power, baseVal;

        public SectorValue[] data;

        [SerializeField]
        private RemoteSectorDynamicValueGeneratorData dynamicGeneratorData = new RemoteSectorDynamicValueGeneratorData(
            "dynamicSectorValue", new SectorDynamicValueGeneratorData(300, 15, 0.1f));

        private SectorDynamicValueGeneratorData DynamicGenerator => dynamicGeneratorData.Value;

        public double[] GetNormalisedDistributionAtStep(double step)
        {
            double[] minerals = new double[mineralDistributions.Length];
            double total = 0;
            for (int i = 0; i < mineralDistributions.Length; i++)
            {
                double value = mineralDistributions[i].GetValueAtStep(step * distributionStepRate) *
                               Random.Range(1f - maxVariance, 1f + maxVariance);
                if (value < minThreshold)
                    value = 0;
                minerals[i] = value;
                total += value;
            }

            double overallValue = 0;

            for (int i = 0; i < minerals.Length; i++)
            {
                minerals[i] /= total;
                overallValue += minerals[i] * TierHelper.GetValue(mineralDistributions[i].MineralTier);
            }

            return minerals;
        }

        public double GetOverallValueAtStep(int step)
        {
            //return baseValue * Math.Pow(stepMult, step);
            Initialise();
            if (step > 0 && step <= data.Length)
                return data[step - 1].Value;
            return baseVal * Math.Pow(step, power);
        }
        

        // Fit the existing data points to a power curve equation
        private void Initialise()
        {
            if (initialised)
                return;
            data = DataAccessor.GetData<DataAssetSectorValue>().Data;

            // Generate x and y arrays for polynomial regression
            double[] xValues = new double[data.Length];
            double[] yValues = new double[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                xValues[i] = i + 1;
                yValues[i] = data[i].Value;
            }
            baseVal = yValues[0];
            power = CalculatePowerValue(xValues, yValues); //CalculatePower(yValues, yValues[0]);
            
            initialised = true;
        }
        
        private double CalculatePowerValue(double[] xValues, double[] yValues)
        {
            int n = xValues.Length;

            // Log-transform the equation: log(y - a) = b * log(x)
            double sumXLog = 0;
            double sumYLog = 0;
            double sumXYLog = 0;
            double sumXLogSquared = 0;

            for (int i = 0; i < n; i++)
            {
                double xLog = Mathf.Log((float)xValues[i]);
                double yLog = Mathf.Log((float)(yValues[i] - xValues[i]));

                sumXLog += xLog;
                sumYLog += yLog;
                sumXYLog += xLog * yLog;
                sumXLogSquared += xLog * xLog;
            }

            // Calculate the values for the linear equation: y = slope * x + intercept
            double slope = (n * sumXYLog - sumXLog * sumYLog) / (n * sumXLogSquared - sumXLog * sumXLog);
            double intercept = (sumYLog - slope * sumXLog) / n;

            // Calculate the value of b
            double b = slope;
            return b;
        }

        public double GenerateDynamicSectorValue(int sectorIndex, int clusterIndex)
        {
            return DynamicGenerator.GenerateValue(sectorIndex, clusterIndex);
        }
    }
}