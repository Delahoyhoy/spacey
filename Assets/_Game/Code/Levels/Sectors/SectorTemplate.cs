using System;
using System.Linq;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorTemplate
    {
        [SerializeField]
        private string sectorName;

        public string SectorName => sectorName;
        
        public string sectorId;
        public string generationTemplate;
        public double totalValue = 1000000;

        [SerializeField]
        private SectorType sectorType;
        public SectorType SectorType => sectorType;

        public int totalChunks;
        public int minLayers, maxLayers;

        private bool isPreGenerated;

        public List<SectorModifierData> sectorModifiers = new List<SectorModifierData>();
        public List<SectorOpportunity> sectorOpportunities = new List<SectorOpportunity>();

        public double[] minDistribution, maxDistribution; // These will be derived from LevelValueGenerator

        [SerializeField]
        private bool hasPreGenerated;
        [SerializeField]
        private ChunkPreGenData[] preGenData;

        public SectorTemplate(string id, string name, string genTemplate, SectorType type, double value, int chunks,
            double[] min, double[] max)
        {
            sectorId = id;
            sectorName = name;
            generationTemplate = genTemplate;
            sectorType = type;
            
            if (type == SectorType.WarpGate)
                return;
            
            totalValue = value;
            totalChunks = chunks;
            sectorModifiers = new List<SectorModifierData>();
            sectorOpportunities = new List<SectorOpportunity>();
            minDistribution = min;
            maxDistribution = max;
            
            // Derive the min and max layer counts from the sectorType
            if (!SectorManager.Instance.TryGetSectorData(genTemplate, out SectorSetupData data)) 
                return;
            minLayers = (int)data.MinChunkSize;
            maxLayers = (int)data.MaxChunkSize;
            branchConnectors = new List<string>();
            
            SectorModifierManager modifierManager = SectorModifierManager.Instance;
            if (modifierManager.GetSectorModifier(data.Modifier01, out SectorModifier modifier01))
                sectorModifiers.Add(new SectorModifierData(modifier01, modifierManager.GenerateModifierEffect(modifier01)));
            if (modifierManager.GetSectorModifier(data.Modifier02, out SectorModifier modifier02) && modifier01 != modifier02)
                sectorModifiers.Add(new SectorModifierData(modifier02, modifierManager.GenerateModifierEffect(modifier02)));
            
            SectorOpportunityManager opportunityManager = SectorOpportunityManager.Instance;
            if (opportunityManager.GetSectorOpportunity(data.Opportunity01, out SectorOpportunity opportunity01))
                sectorOpportunities.Add(opportunity01);
            if (opportunityManager.GetSectorOpportunity(data.Opportunity02, out SectorOpportunity opportunity02) && opportunity01 != opportunity02)
                sectorOpportunities.Add(opportunity02);
            isPreGenerated = false;
        }

        public SectorTemplate(SectorPreGenerationData preGenerationData, string genTemplate, SectorType type, 
            double value, double[] min, double[] max)
        {
            sectorId = preGenerationData.SectorId;
            sectorName = preGenerationData.SectorName;
            generationTemplate = genTemplate;
            sectorType = type;
            
            if (type == SectorType.WarpGate)
                return;

            hasPreGenerated = true;
            preGenData = preGenerationData.ChunkData;
            
            totalValue = value;
            totalChunks = preGenData.Length;
            sectorModifiers = preGenerationData.Modifiers;
            sectorOpportunities = preGenerationData.Opportunities;
            minDistribution = min;
            maxDistribution = max;
            
            branchConnectors = new List<string>();
            isPreGenerated = true;
        }

        private List<AsteroidSetupData> SetupFromData(ChunkPreGenData[] preGeneratedData)
        {
            SensorTier[] tiers = SensorTiersManager.Instance.SensorTiers;

            List<double[]> mineralDistributions = new List<double[]>();
            List<Vector3> positions = new List<Vector3>();

            List<int> possibleRewardIndices = new List<int>();
            
            double totalNormalisedValue = 0;
            
            int totalAsteroids = 0;
            int layer = 0;
            while (totalAsteroids < totalChunks)
            {
                if (layer >= tiers.Length)
                    break;
                int toGenerate = Mathf.Min(totalChunks - totalAsteroids, tiers[layer].asteroidCount);
                totalAsteroids += toGenerate;
                List<Vector3> newPositions = tiers[layer].GetAsteroidPositions(toGenerate, new List<Vector3>(positions));
                for (int j = 0; j < toGenerate; j++)
                {
                    mineralDistributions.Add(GenerateMineralDistribution(out double normalisedValue));
                    totalNormalisedValue += normalisedValue;
                    positions.Add(newPositions[j]);
                    possibleRewardIndices.Add(possibleRewardIndices.Count);
                }
                layer++;
            }

            totalNormalisedValue /= totalAsteroids;
            
            mineralDistributions = mineralDistributions.OrderBy(distribution =>
            {
                double total = 0;
                for (int i = 0; i < distribution.Length; i++)
                    total += distribution[i] * TierHelper.GetValue((Tier) i);
                return total;
            }).ToList();

            List<int> layers = new List<int>();
            int totalLayers = 0;
            for (int i = 0; i < totalAsteroids; i++)
            {
                int newLayers = preGeneratedData[i].Layers;
                layers.Add(newLayers);
                totalLayers += newLayers;
            }

            double totalMass = totalValue / totalNormalisedValue;
            
            layers = layers.OrderBy(value => value).ToList();
            double resourcesPerLayer = totalMass / totalLayers;

            Dictionary<int, ResourceChunkReward> rewardDistribution =
                new Dictionary<int, ResourceChunkReward>();

            for (int i = 0; i < preGeneratedData.Length; i++)
            {
                if (preGeneratedData[i].GetReward(out ResourceChunkReward reward))
                    rewardDistribution.Add(i, reward);
            }
            
            List<AsteroidSetupData> data = new List<AsteroidSetupData>();
            for (int i = 0; i < totalAsteroids; i++)
            {
                List<AsteroidLayer> asteroidLayers = new List<AsteroidLayer>();
                for (int j = 0; j < layers[i]; j++)
                {
                    asteroidLayers.Add(new AsteroidLayer(resourcesPerLayer, mineralDistributions[i]));
                }

                bool hasReward = rewardDistribution.TryGetValue(i, out ResourceChunkReward reward);
                AsteroidData newData = new AsteroidData(0, asteroidLayers, hasReward ? reward : null);
                data.Add(new AsteroidSetupData(positions[i], newData));
            }

            return data;
        }

        public void RegenerateValue(double value)
        {
            if (isPreGenerated)
                return;
            totalValue = Math.Max(value, totalValue);
        }
        
        public List<AsteroidSetupData> GenerateAsteroidData()
        {
            if (hasPreGenerated)
                return SetupFromData(preGenData);
            
            SensorTier[] tiers = SensorTiersManager.Instance.SensorTiers;

            List<double[]> mineralDistributions = new List<double[]>();
            List<Vector3> positions = new List<Vector3>();

            List<int> possibleRewardIndices = new List<int>();
            
            double totalNormalisedValue = 0;
            
            int totalAsteroids = 0;
            int layer = 0;
            while (totalAsteroids < totalChunks)
            {
                if (layer >= tiers.Length)
                    break;
                int toGenerate = Mathf.Min(totalChunks - totalAsteroids, tiers[layer].asteroidCount);
                totalAsteroids += toGenerate;
                List<Vector3> newPositions = tiers[layer].GetAsteroidPositions(toGenerate, new List<Vector3>(positions));
                for (int j = 0; j < toGenerate; j++)
                {
                    mineralDistributions.Add(GenerateMineralDistribution(out double normalisedValue));
                    totalNormalisedValue += normalisedValue;
                    positions.Add(newPositions[j]);
                    possibleRewardIndices.Add(possibleRewardIndices.Count);
                }
                layer++;
            }

            totalNormalisedValue /= totalAsteroids;
            
            mineralDistributions = mineralDistributions.OrderBy(distribution =>
            {
                double total = 0;
                for (int i = 0; i < distribution.Length; i++)
                    total += distribution[i] * TierHelper.GetValue((Tier) i);
                return total;
            }).ToList();

            List<int> layers = new List<int>();
            int totalLayers = 0;
            for (int i = 0; i < totalAsteroids; i++)
            {
                int newLayers = Random.Range(minLayers, maxLayers);
                layers.Add(newLayers);
                totalLayers += newLayers;
            }

            double totalMass = totalValue / totalNormalisedValue;
            
            layers = layers.OrderBy(value => value).ToList();
            double resourcesPerLayer = totalMass / totalLayers;

            int starFragmentsToDistribute = 0;

            for (int i = 0; i < sectorOpportunities.Count; i++)
            {
                if (sectorOpportunities[i].Equals(SectorOpportunity.StarFragment))
                    starFragmentsToDistribute++;
            }

            Dictionary<int, ResourceChunkReward> rewardDistribution =
                new Dictionary<int, ResourceChunkReward>();

            if (starFragmentsToDistribute > 0)
            {
                while (starFragmentsToDistribute > 0)
                {
                    if (possibleRewardIndices.Count <= 0)
                        break;
                    int selectedIndex = Random.Range(0, possibleRewardIndices.Count);
                    rewardDistribution.Add(possibleRewardIndices[selectedIndex], 
                        new ResourceChunkReward(ResourceChunkRewardType.StarFragment, 1));
                    possibleRewardIndices.RemoveAt(selectedIndex);
                    starFragmentsToDistribute--;
                }
            }

            for (int i = 0; i < sectorOpportunities.Count; i++)
            {
                if (sectorOpportunities[i].Equals(SectorOpportunity.StarFragment))
                    continue;
                int selectedIndex = Random.Range(0, possibleRewardIndices.Count);
                switch (sectorOpportunities[i])
                {
                    case SectorOpportunity.GemCache:
                        rewardDistribution.Add(possibleRewardIndices[i], new ResourceChunkReward(ResourceChunkRewardType.HardCurrency, 
                            SectorOpportunityManager.Instance.GetMagnitude(SectorOpportunity.GemCache)));
                        break;
                }
                possibleRewardIndices.RemoveAt(selectedIndex);
            }

            List<AsteroidSetupData> data = new List<AsteroidSetupData>();
            for (int i = 0; i < totalAsteroids; i++)
            {
                List<AsteroidLayer> asteroidLayers = new List<AsteroidLayer>();
                for (int j = 0; j < layers[i]; j++)
                {
                    asteroidLayers.Add(new AsteroidLayer(resourcesPerLayer, mineralDistributions[i]));
                }

                bool hasReward = rewardDistribution.TryGetValue(i, out ResourceChunkReward reward);
                AsteroidData newData = new AsteroidData(0, asteroidLayers, hasReward ? reward : null);
                data.Add(new AsteroidSetupData(positions[i], newData));
            }

            return data;
        }

        private double[] GenerateMineralDistribution(out double normalisedValue)
        {
            // Insert level value generator here
            double[] result = new double[minDistribution.Length];
            double total = 0;
            for (int i = 0; i < minDistribution.Length; i++)
            {
                double value = Random.Range((float)minDistribution[i], (float)maxDistribution[i]) * Random.Range(0.9f, 1.1f);
                if (value <= 0.05)
                {
                    result[i] = 0;
                    continue;
                }
                
                result[i] = value;
                total += result[i];
            }

            normalisedValue = 0;
            for (int i = 0; i < result.Length; i++)
            {
                result[i] /= total;
                normalisedValue += result[i] * TierHelper.GetValue((Tier)i);
            }

            return result;
        }
        
        public List<string> branchConnectors;
    }
}