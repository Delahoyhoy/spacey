﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public struct SectorPreGenerationData
    {
        [SerializeField]
        private string sectorId;

        public string SectorId => sectorId;

        [SerializeField]
        private string sectorName;

        public string SectorName => sectorName;

        [SerializeField]
        private string sectorType;

        public string SectorType => sectorType;
        
        [SerializeField]
        private List<SectorModifierData> modifiers;

        public List<SectorModifierData> Modifiers => modifiers;
        
        [SerializeField]
        private List<SectorOpportunity> opportunities;

        public List<SectorOpportunity> Opportunities => opportunities;
        
        [SerializeField]
        private ChunkPreGenData[] chunkData;

        public ChunkPreGenData[] ChunkData => chunkData;
    }
}