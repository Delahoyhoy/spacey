﻿using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorValue
    {
        [SerializeField]
        private int sectorNumber;

        public int SectorNumber => sectorNumber;
        
        [SerializeField]
        private double sectorValue;

        public double Value => sectorValue;
    }
}