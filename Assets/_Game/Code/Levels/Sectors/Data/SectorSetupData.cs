using System;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorSetupData
    {
        [SerializeField]
        private string sectorTypeID;

        public string SectorTypeId => sectorTypeID;

        [SerializeField]
        private float weighting;

        public float Weighting => weighting;

        [SerializeField]
        private ChunkSize minChunkSize;

        public ChunkSize MinChunkSize => minChunkSize;

        [SerializeField]
        private ChunkSize maxChunkSize;

        public ChunkSize MaxChunkSize => maxChunkSize;

        [SerializeField]
        private int chunkMin;

        public int MinimumChunks => chunkMin;

        [SerializeField]
        private int chunkMax;

        public int MaximumChunks => chunkMax;

        [SerializeField]
        private string modifier01;

        public string Modifier01 => modifier01;

        [SerializeField]
        private string modifier02;

        public string Modifier02 => modifier02;

        [SerializeField]
        private string opportunity01;

        public string Opportunity01 => opportunity01;

        [SerializeField]
        private string opportunity02;

        public string Opportunity02 => opportunity02;
    }
}
