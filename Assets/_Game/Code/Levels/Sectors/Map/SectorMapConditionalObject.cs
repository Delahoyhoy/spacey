﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorMapConditionalObject
    {
        [SerializeField]
        private List<SectorState> validStates;

        [SerializeField]
        private GameObject[] objects;

        public void SetActive(SectorState state)
        {
            bool active = validStates.Contains(state);
            for (int i = 0; i < objects.Length; i++)
                objects[i].SetActive(active);
        }
    }
}