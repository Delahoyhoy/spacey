using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Mothership;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.Gameplay.Sectors
{
    public class SectorMarker : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private Image marker, ring;

        [SerializeField]
        private GameObject currentLocationMarker, endPointMarker;
        
        [SerializeField]
        private SectorEffectDisplay[] modifierDisplays, opportunityDisplays;

        [SerializeField]
        private SectorMarkerConfig[] sectorConfigs;

        [SerializeField]
        private SectorMapConditionalObject[] conditionalObjects;

        [SerializeField]
        private Image mothershipIcon;

        [SerializeField]
        private UnityEvent<bool> onSetJumpAvailable;
        
        private string markerId;
        public string MarkerId => markerId;
        
        private bool interactable = true;
        
        private Action onPress;

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
        
        public void SetupFromSector(SectorTemplate sectorTemplate, SectorState state, Action<string> pressAction, bool jumpAvailable)
        {
            markerId = sectorTemplate.sectorId;
            gameObject.name = $"Marker - {sectorTemplate.sectorId}";
            bool isEndPoint = sectorTemplate.SectorType.Equals(SectorType.WarpGate);
            SetActive(true);
            onPress = () => pressAction?.Invoke(sectorTemplate.sectorId);
            bool currentLocation = state.Equals(SectorState.InProgress) && !isEndPoint;
            currentLocationMarker.SetActive(currentLocation);
            if (currentLocation && mothershipIcon)
                mothershipIcon.sprite = MothershipManager.Instance.MothershipIcon;

            endPointMarker.SetActive(isEndPoint);
            marker.enabled = !isEndPoint;
            
            bool showMods = !isEndPoint && (state == SectorState.Available || state == SectorState.InProgress);

            for (int i = 0; i < modifierDisplays.Length; i++)
            {
                bool show = showMods && i < sectorTemplate.sectorModifiers.Count;
                modifierDisplays[i].SetActive(show);
                if (show)
                    modifierDisplays[i].Setup(sectorTemplate.sectorModifiers[i].modifier);
            }
            
            for (int i = 0; i < opportunityDisplays.Length; i++)
            {
                bool show = showMods && i < sectorTemplate.sectorOpportunities.Count;
                opportunityDisplays[i].SetActive(show);
                if (show)
                    opportunityDisplays[i].Setup(sectorTemplate.sectorOpportunities[i]);
            }
            

            for (int i = 0; i < sectorConfigs.Length; i++)
            {
                if (!sectorConfigs[i].GetMarkerValid(state, out Color colour, out Sprite sprite, out Sprite ringSprite))
                    continue;
                marker.color = colour;
                marker.sprite = sprite;
                bool ringActive = ringSprite != null;
                ring.enabled = ringActive;
                if (ringActive)
                    ring.sprite = ringSprite;
                break;
            }

            for (int i = 0; i < conditionalObjects.Length; i++)
                conditionalObjects[i].SetActive(state);
            onSetJumpAvailable?.Invoke(state.Equals(SectorState.Available) && jumpAvailable);
        }

        public void SetInteractable(bool isInteractable)
        {
            interactable = isInteractable;
        }
        
        public void OnPointerClick(PointerEventData eventData)
        {
            if (interactable)
                onPress?.Invoke();
        }
    }

}