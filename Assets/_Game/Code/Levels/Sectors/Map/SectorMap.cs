using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.StarFragments;
using UnityEngine.UI;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    public class SectorMap : MonoBehaviour
    {
        [SerializeField]
        private UIScreen uiScreen;
        
        [SerializeField]
        private Text clusterName, starFragmentCount;
        
        [SerializeField]
        private SectorRow rowPrefab;

        [SerializeField]
        private SectorMapConnector connectorPrefab;
        
        [SerializeField]
        private SectorMarker markerPrefab;

        [SerializeField]
        private RectTransform holder, connectorHolder, nextClusterHolder, warpScreenAnchor;

        [SerializeField]
        private UIScreen warpScreen;

        [SerializeField]
        private Button nextClusterButton, warpButton;

        [SerializeField]
        private SectorInfoDisplay infoDisplay, upperInfoDisplay;

        private List<SectorRow> currentRows = new List<SectorRow>();
        private List<SectorMapConnector> currentConnectors = new List<SectorMapConnector>();

        private Dictionary<string, SectorMarker> markerLookup = new Dictionary<string, SectorMarker>();

        public void Setup()
        {
            SetupFromCluster(ClusterManager.Instance.CurrentCluster);
        }

        public void SetupFromCluster(Cluster cluster)
        {
            markerLookup = new Dictionary<string, SectorMarker>();
            List<ClusterStep> steps = cluster.Steps;
            bool jumpAvailable = SectorManager.Instance.SectorCompleted;
            for (int i = 0; i < steps.Count; i++)
            {
                if (i < currentRows.Count)
                {
                    currentRows[i].SetupRow(steps[i].SectorTemplates, markerPrefab, ref markerLookup, OpenInfo, jumpAvailable);
                    continue;
                }
                SectorRow newRow = Instantiate(rowPrefab, holder);
                newRow.SetupRow(steps[i].SectorTemplates, markerPrefab, ref markerLookup, OpenInfo, jumpAvailable);
                currentRows.Add(newRow);
            }
            
            for (int i = steps.Count; i < currentRows.Count; i++)
                currentRows[i].SetActive(false);


            List<string> currentBranchConnectors = ClusterManager.Instance.GetCurrentSector().branchConnectors;
            bool warpGateAccessible = currentBranchConnectors.Contains("sector_end") && 
                                      SectorManager.Instance.SectorCompleted;
            
            nextClusterHolder.SetAsLastSibling();
            nextClusterButton.interactable = warpGateAccessible;
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(holder);
            int totalConnectors = 0;

            for (int i = 0; i < steps.Count - 1; i++)
            {
                for (int j = 0; j < steps[i].SectorTemplates.Count; j++)
                {
                    string fromId = steps[i].SectorTemplates[j].sectorId;
                    if (!markerLookup.TryGetValue(fromId, out SectorMarker fromMarker))
                        continue;
                    
                    SectorState fromState = SectorManager.Instance.GetState(fromId);
                    for (int k = 0; k < steps[i].SectorTemplates[j].branchConnectors.Count; k++)
                    {
                        string toId = steps[i].SectorTemplates[j].branchConnectors[k];
                        if (!markerLookup.TryGetValue(toId, out SectorMarker toMarker))
                            continue;
                        
                        SectorState toState = SectorManager.Instance.GetState(toId);
                        if (totalConnectors < currentConnectors.Count)
                        {
                            currentConnectors[totalConnectors].Setup(fromMarker, fromState, toMarker, toState);
                            totalConnectors++;
                            continue;
                        }
                        SectorMapConnector newConnector = Instantiate(connectorPrefab, connectorHolder);
                        newConnector.Setup(fromMarker, fromState, toMarker, toState);
                        currentConnectors.Add(newConnector);
                        totalConnectors++;
                    }
                }
            }

            for (int i = totalConnectors; i < currentConnectors.Count; i++)
            {
                currentConnectors[i].SetActive(false);
            }
            clusterName.text = cluster.ClusterName;
            int clusterIndex = ClusterManager.Instance.ClusterIndex;
            starFragmentCount.text = $"{StarFragmentManager.Instance.GetStarFragmentsClaimedInCluster(clusterIndex)}" +
                                     $"/{ClusterManager.Instance.TotalStarFragmentsInCluster}";
        }

        public void JumpToNextCluster()
        {
            uiScreen.CloseInstant();
            ClusterManager.Instance.NextCluster();
            infoDisplay.Close();
        }

        private readonly List<SectorState> validStates = new List<SectorState>(new []{ SectorState.Available, SectorState.Completed, SectorState.InProgress});
        
        public void OpenInfo(string sectorId)
        {
            if (!markerLookup.TryGetValue(sectorId, out SectorMarker marker))
                return;
            if (!ClusterManager.Instance.TryGetSector(sectorId, out SectorTemplate sector))
                return;
            
            if (!validStates.Contains(SectorManager.Instance.GetState(sectorId)))
                return;
            if (sector.SectorType.Equals(SectorType.WarpGate))
            {
                List<string> currentBranchConnectors = ClusterManager.Instance.GetCurrentSector().branchConnectors;
                bool warpGateAccessible = currentBranchConnectors.Contains("sector_end") && 
                                          SectorManager.Instance.SectorCompleted;
                
                warpScreenAnchor.transform.position = marker.transform.position;
                warpScreen.Open();
                warpButton.interactable = warpGateAccessible;
                
                return;
            }

            SectorInfoDisplay displayPanel = marker.transform.position.y > Screen.height / 2f ? upperInfoDisplay : infoDisplay;
            displayPanel.DisplaySectorInfo(sector, marker, () => uiScreen.CloseInstant());
        }
    }
}
