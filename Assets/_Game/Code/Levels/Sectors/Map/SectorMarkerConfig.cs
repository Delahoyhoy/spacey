﻿using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public struct SectorMarkerConfig
    {
        [SerializeField]
        private SectorState targetState;
        
        [SerializeField]
        private Color markerColour;

        [SerializeField]
        private Sprite markerSprite, ringSprite;

        public bool GetMarkerValid(SectorState state, out Color colour, out Sprite sprite, out Sprite ring)
        {
            colour = markerColour;
            sprite = markerSprite;
            ring = ringSprite;
            return targetState.Equals(state);
        }
    }
}