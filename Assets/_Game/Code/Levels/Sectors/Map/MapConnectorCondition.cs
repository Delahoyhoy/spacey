﻿namespace Core.Gameplay.Sectors
{
    public enum MapConnectorCondition
    {
        OriginDependent,
        TargetDependent,
        RequiresBoth,
    }
}