﻿using System;
using Fumb.Attribute;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public struct SectorMapConnectorConfig
    {
        [SerializeField]
        private MapConnectorCondition connectionCondition;
        
        [SerializeField] [ConditionalField("connectionCondition", false, 
            MapConnectorCondition.OriginDependent, MapConnectorCondition.RequiresBoth)]
        private SectorState originState;
        
        [SerializeField] [ConditionalField("connectionCondition", false, 
            MapConnectorCondition.TargetDependent, MapConnectorCondition.RequiresBoth)]
        private SectorState targetState;

        [SerializeField]
        private Color lineColour;

        [SerializeField]
        private Sprite lineSprite;

        public bool ShouldApplyEffect(SectorState origin, SectorState target, out Color colour, out Sprite sprite)
        {
            colour = lineColour;
            sprite = lineSprite;
            return GetOriginMatch(origin) && GetTargetMatch(target);
        }

        private bool GetOriginMatch(SectorState origin)
        {
            return connectionCondition == MapConnectorCondition.TargetDependent || originState.Equals(origin);
        }
        
        private bool GetTargetMatch(SectorState target)
        {
            return connectionCondition == MapConnectorCondition.OriginDependent || targetState.Equals(target);
        }
    }
}