﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Sectors
{
    public class SectorModifierInfoPanel : MonoBehaviour
    {
        [SerializeField]
        private Image icon, frame;

        [SerializeField]
        private Text modifierHeader, modifierBody;

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
        
        public void Setup(SectorModifierData modifier)
        {
            SetActive(true);
            SectorModifierManager sectorModifierManager = SectorModifierManager.Instance;

            SectorModifier sectorModifier = modifier.modifier;
            
            icon.sprite = sectorModifierManager.GetIcon(sectorModifier);
            modifierHeader.color = frame.color = sectorModifierManager.GetColour(sectorModifier);

            modifierHeader.text = sectorModifierManager.GetTitle(sectorModifier);
            modifierBody.text = sectorModifierManager.GetDescription(modifier);
        }

        public void Setup(SectorOpportunity opportunity)
        {
            SetActive(true);
            SectorOpportunityManager sectorOpportunityManager = SectorOpportunityManager.Instance;

            icon.sprite = sectorOpportunityManager.GetIcon(opportunity);
            modifierHeader.color = frame.color = sectorOpportunityManager.GetColour(opportunity);

            modifierHeader.text = sectorOpportunityManager.GetTitle(opportunity);
            modifierBody.text = sectorOpportunityManager.GetDescription(opportunity);
        }
    }
}