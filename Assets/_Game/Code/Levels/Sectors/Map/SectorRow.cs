using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    public class SectorRow : MonoBehaviour
    {
        private List<SectorMarker> markers = new List<SectorMarker>();

        public void SetupRow(List<SectorTemplate> levels, SectorMarker markerPrefab, ref Dictionary<string, 
            SectorMarker> markerLookup, Action<string> pressAction, bool jumpAvailable)
        {
            gameObject.SetActive(true);
            for (int i = 0; i < levels.Count; i++)
            {
                if (i < markers.Count)
                {
                    markers[i].SetupFromSector(levels[i], SectorManager.Instance.GetState(levels[i].sectorId), 
                        pressAction, jumpAvailable);
                    markerLookup.Add(levels[i].sectorId, markers[i]);
                    continue;
                }

                SectorMarker newMarker = Instantiate(markerPrefab, transform);
                newMarker.SetupFromSector(levels[i], SectorManager.Instance.GetState(levels[i].sectorId), 
                    pressAction, jumpAvailable);
                markers.Add(newMarker);
                markerLookup.Add(levels[i].sectorId, newMarker);
            }

            for (int i = levels.Count; i < markers.Count; i++)
            {
                markers[i].SetActive(false);
            }
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}
