using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Core.Gameplay.Sectors
{
    public class SectorInfoDisplay : MonoBehaviour
    {
        [SerializeField]
        private UIScreen uiScreen;

        [SerializeField]
        private Text sectorName, sectorState;

        [SerializeField]
        private Text totalValue, asteroidCount;

        [SerializeField]
        private SectorModifierInfoPanel[] modifierInfoPanels, opportunityInfoPanels;

        [SerializeField]
        private Button travelButton;

        [SerializeField]
        private RectTransform mainBubble, bubble;

        private Action onNavigate;

        [SerializeField]
        private UnityEvent onNavigateEvent;

        public void DisplaySectorInfo(SectorTemplate sector, SectorMarker marker, Action navigate)
        {
            onNavigate = navigate;
            gameObject.SetActive(true);
            Transform markerTransform = marker.transform;

            mainBubble.position = markerTransform.position;

            Vector3 bubblePos = bubble.localPosition;
            bubblePos.x = -mainBubble.localPosition.x;
            bubble.localPosition = bubblePos;

            uiScreen.Open();
            sectorName.text = sector.SectorName;
            sectorState.text = SectorManager.Instance.GetState(sector.sectorId).ToString();
            totalValue.text = CurrencyController.FormatToString(sector.totalValue);
            asteroidCount.text = CurrencyController.FormatToString(sector.totalChunks);

            SectorTemplate currentSector = SectorManager.Instance.CurrentSectorTemplate;

            for (int i = 0; i < modifierInfoPanels.Length; i++)
            {
                bool active = i < sector.sectorModifiers.Count;
                modifierInfoPanels[i].SetActive(active);
                if (active)
                    modifierInfoPanels[i].Setup(sector.sectorModifiers[i]);
            }

            List<SectorOpportunity> sectorOpportunities = sector.sectorOpportunities;
            for (int i = 0; i < opportunityInfoPanels.Length; i++)
            {
                bool active = sectorOpportunities != null && i < sectorOpportunities.Count;
                opportunityInfoPanels[i].SetActive(active);
                if (active)
                    opportunityInfoPanels[i].Setup(sector.sectorOpportunities[i]);
            }

            bool accessible = SectorManager.Instance.SectorCompleted &&
                              currentSector.branchConnectors.Contains(sector.sectorId);
            travelButton.gameObject.SetActive(accessible);
            if (!accessible)
                return;

            void Navigate()
            {
                ClusterManager.Instance.NavigateToSector(sector.sectorId);
                onNavigate?.Invoke();
                onNavigateEvent?.Invoke();
                Close();
            }

            travelButton.interactable = SectorManager.Instance.SectorCompleted;
            travelButton.onClick.RemoveAllListeners();
            travelButton.onClick.AddListener(Navigate);
        }

        public void Close()
        {
            uiScreen.Close();
        }
    }
}
