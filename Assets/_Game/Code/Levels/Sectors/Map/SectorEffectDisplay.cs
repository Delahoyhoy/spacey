﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Sectors
{
    public class SectorEffectDisplay : MonoBehaviour
    {
        [SerializeField]
        private Image icon;

        public void Setup(SectorModifier modifier)
        {
            SectorModifierManager modifierManager = SectorModifierManager.Instance;
            icon.sprite = modifierManager.GetIcon(modifier);
            //icon.color = modifierManager.GetColour(modifier);
        }

        public void Setup(SectorOpportunity opportunity)
        {
            SectorOpportunityManager opportunityManager = SectorOpportunityManager.Instance;
            icon.sprite = opportunityManager.GetIcon(opportunity);
            //icon.color = opportunityManager.GetColour(opportunity);
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}