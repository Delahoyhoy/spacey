﻿using System;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class MineralDistributionController
    {
        [SerializeField]
        private Tier tier;

        public Tier MineralTier => tier;

        [SerializeField]
        private float mult, power;

        public double GetValueAtStep(double step)
        {
            return Math.Pow(step * mult, power);
        }
    }
}