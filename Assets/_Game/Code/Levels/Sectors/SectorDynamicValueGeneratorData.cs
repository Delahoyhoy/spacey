﻿using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorDynamicValueGeneratorData
    {
        [SerializeField]
        private float minSeconds = 600f;

        public float MinSeconds => minSeconds;

        [SerializeField]
        private float secondsPerSector = 15f;

        public float SecondsPerSector => secondsPerSector;

        [SerializeField]
        private float clusterGrowth = 0.1f;

        public float ClusterGrowth => clusterGrowth;

        public SectorDynamicValueGeneratorData(float secondsBase, float sectorAdd, float clusterExp)
        {
            minSeconds = secondsBase;
            secondsPerSector = sectorAdd;
            clusterGrowth = clusterExp;
        }
        
        public SectorDynamicValueGeneratorData() {}

        public double GenerateValue(int sectorIndex, int clusterIndex)
        {
            return (minSeconds + (sectorIndex * secondsPerSector)) * Math.Exp(clusterIndex * clusterGrowth);
        }
    }
}