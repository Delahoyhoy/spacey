﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    public class SectorOpportunityManager : MonoBehaviour
    {
        private static SectorOpportunityManager instance;

        public static SectorOpportunityManager Instance =>
            instance ??= ObjectFinder.FindObjectOfType<SectorOpportunityManager>();

        [SerializeField]
        private Sprite defaultSprite;
        
        [SerializeField]
        private SectorOpportunityTemplate[] opportunityTemplates;

        private Dictionary<SectorOpportunity, SectorOpportunityTemplate> opportunityLookup =
            new Dictionary<SectorOpportunity, SectorOpportunityTemplate>();
        
        private void Awake()
        {
            instance = this;
            for (int i = 0; i < opportunityTemplates.Length; i++)
            {
                if (!opportunityLookup.ContainsKey(opportunityTemplates[i].Opportunity))
                    opportunityLookup.Add(opportunityTemplates[i].Opportunity, opportunityTemplates[i]);
            }
        }

        public bool GetSectorOpportunity(string id, out SectorOpportunity opportunity)
        {
            opportunity = SectorOpportunity.GemCache;
            if (string.IsNullOrEmpty(id))
                return false;
            if (id.Equals("any"))
            {
                if (Random.value < 0.9f)
                    return false;
                opportunity = opportunityTemplates[Random.Range(1, opportunityTemplates.Length)].Opportunity;
                return true;
            }

            for (int i = 0; i < opportunityTemplates.Length; i++)
            {
                if (!opportunityTemplates[i].EvaluateId(id))
                    continue;
                opportunity = opportunityTemplates[i].Opportunity;
                return true;
            }

            return false;
        }
        
        public string GetTitle(SectorOpportunity opportunity)
        {
            if (opportunityLookup.TryGetValue(opportunity, out SectorOpportunityTemplate opportunityTemplate))
                return opportunityTemplate.Title;
            return "Unknown";
        }

        public int GetMagnitude(SectorOpportunity opportunity)
        {
            if (opportunityLookup.TryGetValue(opportunity, out SectorOpportunityTemplate opportunityTemplate))
                return opportunityTemplate.GenerateValue();
            return 1;
        }
        
        public string GetDescription(SectorOpportunity opportunity)
        {
            if (opportunityLookup.TryGetValue(opportunity, out SectorOpportunityTemplate opportunityTemplate))
                return opportunityTemplate.Description;
            return "This sector contains an unknown signal";
        }
        
        public float GenerateOpportunityValue(SectorOpportunity opportunity)
        {
            if (!opportunityLookup.TryGetValue(opportunity, out SectorOpportunityTemplate opportunityTemplate))
                return 0;
            return opportunityTemplate.GenerateValue();
        }

        public Sprite GetIcon(SectorOpportunity opportunity)
        {
            if (opportunityLookup.TryGetValue(opportunity, out SectorOpportunityTemplate opportunityTemplate))
                return opportunityTemplate.OpportunityIcon;
            return defaultSprite;
        }
        
        public Color GetColour(SectorOpportunity opportunity)
        {
            if (opportunityLookup.TryGetValue(opportunity, out SectorOpportunityTemplate opportunityTemplate))
                return opportunityTemplate.OpportunityColour;
            return Color.white;
        }
    }
}