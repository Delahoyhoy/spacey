﻿namespace Core.Gameplay.Sectors
{
    public enum SectorOpportunity
    {
        StarFragment,
        GemCache,
    }
}