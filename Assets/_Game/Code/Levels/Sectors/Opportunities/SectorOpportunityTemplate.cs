﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorOpportunityTemplate
    {
        [SerializeField]
        private SectorOpportunity opportunity;

        public SectorOpportunity Opportunity => opportunity;

        [SerializeField]
        private float defaultValue;

        public float DefaultValue => defaultValue;

        [SerializeField]
        private List<string> relevantIds = new List<string>();

        [SerializeField]
        private Sprite opportunityIcon;

        [SerializeField]
        private int minValue = 1, maxValue = 1;

        public Sprite OpportunityIcon => opportunityIcon;

        [SerializeField]
        private Color opportunityColour;

        public Color OpportunityColour => opportunityColour;
        
        [SerializeField]
        private string title;

        public string Title => title;
        
        [SerializeField]
        private string description;

        public string Description => description;

        public bool EvaluateId(string id)
        {
            return relevantIds.Contains(id);
        }

        public int GenerateValue()
        {
            return Mathf.RoundToInt(Random.Range(minValue, maxValue + 1));
        }
    }
}