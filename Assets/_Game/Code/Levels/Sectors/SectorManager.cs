using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Utilities;
using Data;
using Fumb.Data;
using Fumb.Save;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Sectors
{
    public class SectorManager : MonoBehaviour
    {
        private static SectorManager instance;
        public static SectorManager Instance => instance ??= ObjectFinder.FindObjectOfType<SectorManager>();

        [ManagedSaveValue("SectorManagerVisitedSectors")]
        private SaveValueListString saveVisitedSectors;

        [ManagedSaveValue("SectorManagerCurrentSectorId")]
        private SaveValueString saveCurrentSectorId;

        public string CurrentSectorId => saveCurrentSectorId.Value;
        
        public SectorTemplate CurrentSectorTemplate => ClusterManager.Instance.GetCurrentSector();
        
        private bool sectorCompleted;

        public bool SectorCompleted => sectorCompleted;

        public bool IsLastSector => CurrentSectorTemplate.branchConnectors.Contains("sector_end");

        [SerializeField]
        private string completionFormat = "{0}/{1}";
        
        [SerializeField]
        private UnityEvent<float> onUpdateProgress;
        [SerializeField]
        private UnityEvent<string> onUpdateCompletion;
        
        [SerializeField]
        private UnityEvent<bool> onSetCompleted;

        private SectorSetupData[] sectorData;

        public Action onBeginNewSector;

        private Dictionary<string, int> sectorTypeLookup = new Dictionary<string, int>();

        private void Awake()
        {
            instance = this;
            sectorData = DataAccessor.GetData<DataAssetSectorSetupData>().Data;
            for (int i = 0; i < sectorData.Length; i++)
            {
                sectorTypeLookup.Add(sectorData[i].SectorTypeId, i);
            }
        }

        public void PromptCompletionCheck()
        {
            onSetCompleted?.Invoke(SectorCompleted);
        }

        public SectorSetupData SelectSectorType()
        {
            WeightedList<SectorSetupData> weightedList = new WeightedList<SectorSetupData>();
            for (int i = 0; i < sectorData.Length; i++)
            {
                if (sectorData[i].Weighting <= 0)
                    continue;
                weightedList.Add(sectorData[i], sectorData[i].Weighting);
            }

            return weightedList.PickRandom();
        }

        public SectorSetupData GetSectorType(string sectorType)
        {
            for (int i = 0; i < sectorData.Length; i++)
            {
                if (sectorType.Equals(sectorData[i].SectorTypeId))
                    return sectorData[i];
            }

            return SelectSectorType();
        }

        public bool TryGetSectorData(string id, out SectorSetupData data)
        {
            if (sectorTypeLookup.TryGetValue(id, out int index))
            {
                data = sectorData[index];
                return true;
            }

            data = null;
            return false;
        }

        public void InitialiseFromTemplate(SectorTemplate template)
        {
            saveCurrentSectorId.Value = template.sectorId;
            if (!saveVisitedSectors.IsPopulated)
                saveVisitedSectors.Value = new List<string>();
            if (!saveVisitedSectors.Value.Contains(template.sectorId))
                saveVisitedSectors.Value.Add(template.sectorId);
            SectorModifierManager.Instance.Setup(template.sectorModifiers);
            onBeginNewSector?.Invoke();
        }

        private void Start()
        {
            AsteroidManager.Instance.onUpdateProgress += UpdateProgress;
            AsteroidManager.Instance.onUpdateCompletion += UpdateCompletion;
        }

        public bool SectorVisited(string sectorId)
        {
            return saveVisitedSectors.IsPopulated && saveVisitedSectors.Value.Contains(sectorId);
        }

        public void ClearVisitedSectors()
        {
            saveVisitedSectors.Value = new List<string>();
        }

        public bool GetReachable(SectorTemplate fromSector, string sectorId, int iterator = 0)
        {
            if (iterator > 100)
                return false;
            if (fromSector.branchConnectors == null || fromSector.branchConnectors.Count <= 0)
                return false;
            if (fromSector.branchConnectors.Contains(sectorId))
                return true;
            foreach (var branch in fromSector.branchConnectors)
            {
                if (!ClusterManager.Instance.TryGetSector(branch, out SectorTemplate level))
                    continue;
                if (GetReachable(level, sectorId, iterator + 1))
                    return true;
            }
            return false;
        }

        public SectorState GetState(string sectorId)
        {
            if (saveCurrentSectorId.Value.Equals(sectorId))
                return SectorState.InProgress;
            if (SectorVisited(sectorId))
                return SectorState.Completed;
            if (ClusterManager.Instance.GetCurrentSector().branchConnectors.Contains(sectorId))
                return SectorState.Available;
            return GetReachable(ClusterManager.Instance.GetCurrentSector(), sectorId) ? SectorState.Unvisited : SectorState.Unreachable;
        }

        private void UpdateProgress(float progress)
        {
            onUpdateProgress?.Invoke(progress);
        }

        private void UpdateCompletion(int completed, int total)
        {
            sectorCompleted = completed >= total;
            onSetCompleted?.Invoke(sectorCompleted);
            onUpdateCompletion?.Invoke(string.Format(completionFormat, completed, total));
        }
        
    }

}