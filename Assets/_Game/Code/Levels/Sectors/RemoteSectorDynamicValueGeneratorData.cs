﻿using System;
using Fumb.RemoteConfig;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class RemoteSectorDynamicValueGeneratorData : RemoteJson<SectorDynamicValueGeneratorData>
    {
        public RemoteSectorDynamicValueGeneratorData(string key, SectorDynamicValueGeneratorData defaultValue) : base(key, defaultValue)
        {
        }
    }
}