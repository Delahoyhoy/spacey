using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Sectors
{
    public class SectorModifierListener : MonoBehaviour
    {
        [SerializeField]
        private SectorModifier modifier;

        [SerializeField]
        private UnityEvent<float> onSetModifier;

        private void Start()
        {
            SectorModifierManager.Instance.onSetModifier += OnSetModifier;
        }

        private void OnSetModifier(SectorModifier mod, float value)
        {
            if (modifier.Equals(mod))
                onSetModifier?.Invoke(value);
        }
    }
}