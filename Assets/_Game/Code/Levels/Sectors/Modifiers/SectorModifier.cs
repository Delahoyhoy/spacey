namespace Core.Gameplay.Sectors
{
    public enum SectorModifier
    {
        MiningSpeed,
        ShipSpeed,
        MineralRarity,
        DebrisRate,
        ResearchSpeed,
        BoostAugments,
    }
}