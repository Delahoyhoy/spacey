using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorModifierEffect
    {
        [SerializeField]
        private SectorModifier modifier;

        public SectorModifier Modifier => modifier;

        [SerializeField]
        private string modifierTitle;

        public string Title => modifierTitle;

        [SerializeField]
        private string modifierFormat;

        [SerializeField]
        private double displayValueMultiplier = 1d;
        
        public string FormatBody(double value)
        {
            return string.Format(modifierFormat, Math.Floor(value * displayValueMultiplier));
        }

        [SerializeField]
        private float defaultValue;

        public float DefaultValue => defaultValue;

        [SerializeField]
        private List<string> relevantIds = new List<string>();

        [SerializeField]
        private Sprite modifierIcon;

        [SerializeField]
        private float minValue = 0.1f, maxValue = 0.5f, valueIncrement = 0.1f;

        public Sprite ModifierIcon => modifierIcon;

        [SerializeField]
        private Color modifierColour;

        public Color ModifierColour => modifierColour;

        [SerializeField]
        private UnityEvent<float> onSetValue;

        public bool EvaluateId(string id)
        {
            return relevantIds.Contains(id);
        }

        public float GenerateValue()
        {
            return Mathf.Round(Random.Range(minValue, maxValue) / valueIncrement) * valueIncrement;
        }
        
        public void SetValue(float value)
        {
            onSetValue?.Invoke(value);
        }
    }
}