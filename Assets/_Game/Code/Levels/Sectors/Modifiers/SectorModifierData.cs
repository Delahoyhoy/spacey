using System;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class SectorModifierData
    {
        public SectorModifier modifier;
        public float magnitude;

        public SectorModifierData(SectorModifier sectorModifier, float value)
        {
            modifier = sectorModifier;
            magnitude = value;
        }
    }
}