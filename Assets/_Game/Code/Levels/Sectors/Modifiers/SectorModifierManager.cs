using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    public class SectorModifierManager : MonoBehaviour
    {
        private static SectorModifierManager instance;

        public static SectorModifierManager Instance =>
            instance ??= ObjectFinder.FindObjectOfType<SectorModifierManager>();

        [SerializeField]
        private Sprite defaultSprite;
        
        [SerializeField]
        private SectorModifierEffect[] modifierEffects;

        private Dictionary<SectorModifier, SectorModifierEffect> modifierLookup =
            new Dictionary<SectorModifier, SectorModifierEffect>();

        public Action<SectorModifier, float> onSetModifier;

        private void Awake()
        {
            instance = this;
            for (int i = 0; i < modifierEffects.Length; i++)
            {
                if (!modifierLookup.ContainsKey(modifierEffects[i].Modifier))
                    modifierLookup.Add(modifierEffects[i].Modifier, modifierEffects[i]);
            }
        }

        public bool GetSectorModifier(string id, out SectorModifier modifier)
        {
            modifier = SectorModifier.MiningSpeed;
            if (string.IsNullOrEmpty(id))
                return false;
            if (id.Equals("any"))
            {
                modifier = modifierEffects[Random.Range(0, modifierEffects.Length)].Modifier;
                return true;
            }

            for (int i = 0; i < modifierEffects.Length; i++)
            {
                if (!modifierEffects[i].EvaluateId(id))
                    continue;
                modifier = modifierEffects[i].Modifier;
                return true;
            }

            return false;
        }

        public float GenerateModifierEffect(SectorModifier modifier)
        {
            if (!modifierLookup.TryGetValue(modifier, out SectorModifierEffect modifierEffect))
                return 0;
            return modifierEffect.GenerateValue();
        }

        public Sprite GetIcon(SectorModifier modifier)
        {
            if (modifierLookup.TryGetValue(modifier, out SectorModifierEffect modifierEffect))
                return modifierEffect.ModifierIcon;
            return defaultSprite;
        }

        public Color GetColour(SectorModifier modifier)
        {
            if (modifierLookup.TryGetValue(modifier, out SectorModifierEffect modifierEffect))
                return modifierEffect.ModifierColour;
            return Color.white;
        }

        public string GetTitle(SectorModifier modifier)
        {
            if (modifierLookup.TryGetValue(modifier, out SectorModifierEffect modifierEffect))
                return modifierEffect.Title;
            return "Unknown";
        }
        
        public string GetDescription(SectorModifierData modifier)
        {
            if (modifierLookup.TryGetValue(modifier.modifier, out SectorModifierEffect modifierEffect))
                return modifierEffect.FormatBody(modifier.magnitude);
            return "This sector is experiencing an unknown anomaly";
        }

        public void Setup(List<SectorModifierData> modifiers)
        {
            for (int i = 0; i < modifierEffects.Length; i++)
            {
                SectorModifier modifier = modifierEffects[i].Modifier;
                float totalModifier = modifierEffects[i].DefaultValue;
                for (int j = 0; j < modifiers.Count; j++)
                {
                    if (!modifier.Equals(modifiers[j].modifier))
                        continue;
                    totalModifier += modifiers[j].magnitude;
                }
                modifierEffects[i].SetValue(totalModifier);
                onSetModifier?.Invoke(modifier, totalModifier);
            }
        }
    }
}