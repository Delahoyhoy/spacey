﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class ClusterPreGenerationData
    {
        [SerializeField]
        private List<ClusterStepPreGenerationData> clusterSteps;

        public List<ClusterStepPreGenerationData> ClusterSteps => clusterSteps;
    }
}