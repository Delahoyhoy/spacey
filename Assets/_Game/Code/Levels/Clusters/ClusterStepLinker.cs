﻿using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class ClusterStepLinker
    {
        [SerializeField]
        private ClusterStepLinkCondition[] conditions;
        
        public void Link(ClusterStep fromStep, ClusterStep toStep)
        {
            for (int i = 0; i < conditions.Length; i++)
            {
                if (!conditions[i].GetValid(fromStep.SectorCount, toStep.SectorCount, out ClusterStepLink[] stepLinks))
                    continue;

                for (int j = 0; j < stepLinks.Length; j++)
                {
                    stepLinks[j].Link(fromStep, toStep);
                }
                
                return;
            }
        }
    }
}