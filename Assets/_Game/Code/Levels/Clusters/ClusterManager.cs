using System;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Reset;
using Core.Gameplay.StarFragments;
using Core.Gameplay.Utilities;
using Core.Gameplay.VisualEffects;
using Data;
using Fumb.Analytics;
using Fumb.Data;
using Fumb.RemoteConfig;
using Fumb.Save;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    public class ClusterManager : MonoBehaviour
    {
        private static ClusterManager instance;
        public static ClusterManager Instance => instance ??= ObjectFinder.FindObjectOfType<ClusterManager>();
        
        [SerializeField]
        private Cluster debugCluster;
        
        [ManagedSaveValue("ClusterManagerCurrentCluster")]
        private SaveValueCluster saveCurrentCluster;

        [ManagedSaveValue("ClusterManagerHighestCluster")]
        private SaveValueInt saveHighestCluster;

        [ManagedSaveValue("ClusterManagerClusterIndex")]
        private SaveValueInt saveClusterIndex;

        [ManagedSaveValue("ClusterManagerClusterNames")]
        private SaveValueListString saveClusterNames;

        [SerializeField]
        private SectorJump sectorJump;

        [SerializeField]
        private NameGenerator nameGenerator;

        [SerializeField]
        private ClusterPreGenerationData tutorialCluster;
        
        [SerializeField]
        private UnityEvent<string> onSetSectorName, onSetArrivalName;

        public bool ClusterCompleted => SectorManager.Instance.SectorCompleted && SectorManager.Instance.IsLastSector;

        public int TotalStarFragmentsInCluster => GetStarFragmentsAtCluster(ClusterIndex);

        [SerializeField]
        private RemoteFloat chunkClusterCount = new RemoteFloat("chunkClusterMult", 0.02f);
        
        public int ClusterIndex
        {
            get => saveClusterIndex.Value;
            set
            {
                saveClusterIndex.Value = value >= clusterData.Length ? 0 : value;
                if (value > saveHighestCluster.Value)
                    saveHighestCluster.Value = value;
            }
        }

        public int HighestCluster => saveHighestCluster.Value;
        
        public Cluster CurrentCluster
        {
            get {
                if (saveCurrentCluster.IsPopulated && saveCurrentCluster.Value != null)
                    return saveCurrentCluster.Value;
                CurrentCluster = GenerateCluster(ClusterIndex);
                return saveCurrentCluster.Value;
            }
            set => saveCurrentCluster.Value = value;
        }

        private string CurrentSectorId => SectorManager.Instance.CurrentSectorId;

        [SerializeField]
        private ClusterGenerator clusterGenerator;

        [SerializeField]
        private ClusterStepLinker clusterStepLinker;

        [SerializeField]
        private SectorValueGenerator valueGenerator;

        private ClusterData[] clusterData;
        
        public void StartCluster(Cluster cluster)
        {
            CurrentCluster = cluster;
            SectorTemplate sector = cluster.GetLevel(0);
            int sectorIndex = GetTotalSectorsToCluster(ClusterIndex) + CurrentCluster.GetRowIndex(sector.sectorId);
            double value = GenerateNewValueForSector(sectorIndex, ClusterIndex);
            sector.RegenerateValue(value);
            SectorManager.Instance.InitialiseFromTemplate(sector);
            GameManager.Instance.InitialiseAsteroidsFromCurrentLevel();
            onSetSectorName?.Invoke(sector.SectorName);
        }


        private double GenerateNewValueForSector(int sectorIndex, int clusterIndex)
        {
            double value = GameManager.Instance.GetBaseEarnings(valueGenerator.GenerateDynamicSectorValue(sectorIndex, clusterIndex));
            if (HighestCluster <= 0 || StatTracker.Instance.GetStat("timesPrestiged") <= 0) 
                return value;
            double baseValue = value / CurrencyController.Instance.PrestigeMultiplier;
            value = InterpolationHelper.Lerp(baseValue, value, 0.2f + ((float)clusterIndex / HighestCluster) * 0.8f);
            return value;
        }

        public SectorTemplate GetCurrentSector()
        {
            if (saveCurrentCluster.IsPopulated && CurrentCluster.TryGetLevel(SectorManager.Instance.CurrentSectorId, 
                    out SectorTemplate sectorTemplate))
                return sectorTemplate;
            return null;
        }

        public int GetStarFragmentsAtCluster(int clusterIndex)
        {
            return clusterData[clusterIndex].TotalStarFragments;
        }

        public void InitialiseInCurrentSector()
        {
            SectorTemplate sector = GetCurrentSector();
            SectorModifierManager.Instance.Setup(sector.sectorModifiers);
            onSetSectorName?.Invoke(sector.SectorName);
        }

        private double GetCumulativeRequiredMinValue(int starFragments)
        {
            int totalAcquired = 0;

            int clusterIndex = 0;
            int overallIndex = 0;
            double totalValue = 0;
            while (totalAcquired < starFragments)
            {
                if (clusterIndex >= clusterData.Length)
                    break;
                int remainingFragments = clusterData[clusterIndex].TotalStarFragments;
                for (int i = 0; i < clusterData[clusterIndex].SectorRows + 1; i++)
                {
                    if (i > 0 && remainingFragments > 0)
                    {
                        totalAcquired++;
                        remainingFragments--;
                    }

                    totalValue += valueGenerator.GetOverallValueAtStep(overallIndex);
                    overallIndex++;

                    if (totalAcquired >= starFragments)
                        break;
                }

                clusterIndex++;
            }

            return totalValue;
        }

        public int GetTotalSectorsToCluster(int clusterIndex)
        {
            int total = 0;
            for (int i = 0; i < clusterIndex; i++)
            {
                if (clusterIndex >= clusterData.Length)
                    break;
                total += clusterData[i].SectorRows + 1;
            }
            return total;
        }

        private Cluster GenerateCluster(int clusterIndex)
        {
            List<int> rows = clusterGenerator.GenerateSectorRows(clusterData[clusterIndex].SectorRows);
            
            // Determine the remaining star fragments from the total available in data and the number already claimed
            int totalStarFragments = GetStarFragmentsAtCluster(clusterIndex) - StarFragmentManager.Instance.GetFragmentsObtained(clusterIndex);
            List<ClusterStep> steps = new List<ClusterStep>();
            
            steps.Add(new ClusterStep(GenerateSectorTemplate(clusterIndex, 0, 0, 0, SectorType.Playable, "sector_start")));
            int total = 1;
            for (int i = 0; i < rows.Count; i++)
            {
                List<SectorTemplate> sectorTemplates = new List<SectorTemplate>();
                for (int j = 0; j < rows[i]; j++)
                {
                    sectorTemplates.Add(GenerateSectorTemplate(clusterIndex, i + 1, j, total, SectorType.Playable));
                    total++;
                }
                steps.Add(new ClusterStep(sectorTemplates));
            }

            // Distribute the undiscovered star fragments
            if (totalStarFragments > 0)
            {
                List<SectorTemplate> allSectors = new List<SectorTemplate>();
                for (int i = 1; i < steps.Count; i++)
                {
                    allSectors.AddRange(steps[i].SectorTemplates);
                }

                while (totalStarFragments > 0)
                {
                    if (allSectors.Count <= 0)
                        break;
                    int selectedIndex = Random.Range(0, allSectors.Count);
                    allSectors[selectedIndex].sectorOpportunities.Add(SectorOpportunity.StarFragment);
                    allSectors.RemoveAt(selectedIndex);
                    totalStarFragments--;
                }
            }
            
            steps.Add(new ClusterStep(GenerateSectorTemplate(clusterIndex, rows.Count, 0, total, SectorType.WarpGate, "sector_end")));

            for (int i = 0; i < steps.Count - 1; i++)
            {
                clusterStepLinker.Link(steps[i], steps[i + 1]);
            }

            string clusterName = GetClusterName(clusterIndex);
            
            return new Cluster(clusterName, steps);
        }

        private SectorTemplate GenerateSectorTemplate(int clusterIndex, int rowIndex, int sectorIndex, int overallIndex, SectorType type, string overrideId = "")
        {
            ClusterData data = clusterData[Mathf.Clamp(clusterIndex, 0, clusterData.Length - 1)]; 
            string id = string.IsNullOrEmpty(overrideId) ? $"sector_{rowIndex}_{sectorIndex}" : overrideId;
            string sectorName = $"{nameGenerator.GetName()} Sector";
            SectorSetupData sectorSetupData = SectorManager.Instance.SelectSectorType();
            string generatorTemplate = sectorSetupData.SectorTypeId;
            int sectorsToCluster = GetTotalSectorsToCluster(clusterIndex);
            double value = valueGenerator.GetOverallValueAtStep(sectorsToCluster + rowIndex + 1) * data.ChunkValueModifier;
            int totalChunks = Mathf.RoundToInt(Random.Range(sectorSetupData.MinimumChunks, sectorSetupData.MaximumChunks + 1) * 
                                               data.ChunkCountModifier * (1f + (ClusterIndex * chunkClusterCount)));
            SectorTemplate newTemplate = new SectorTemplate(id, sectorName, generatorTemplate, type,
                value, totalChunks,
                valueGenerator.GetNormalisedDistributionAtStep(sectorsToCluster + rowIndex + 1),
                valueGenerator.GetNormalisedDistributionAtStep(sectorsToCluster + rowIndex + 5));
            return newTemplate;
        }

        private SectorTemplate GenerateSectorDataFromPreGen(int clusterIndex, int rowIndex, SectorType type, 
            SectorPreGenerationData preGenerationData)
        {
            SectorSetupData data = SectorManager.Instance.GetSectorType(preGenerationData.SectorType); 
            string generatorTemplate = data.SectorTypeId;
            int sectorsToCluster = GetTotalSectorsToCluster(clusterIndex);
            double value = valueGenerator.GetOverallValueAtStep(sectorsToCluster + rowIndex + 1);
            SectorTemplate newTemplate = new SectorTemplate(preGenerationData, generatorTemplate, type,
                value, valueGenerator.GetNormalisedDistributionAtStep(sectorsToCluster + rowIndex + 1),
                valueGenerator.GetNormalisedDistributionAtStep(sectorsToCluster + rowIndex + 5));
            return newTemplate;
        }

        private Cluster GenerateClusterFromPreGen(ClusterPreGenerationData preGenerationData)
        {
            List<ClusterStepPreGenerationData> stepsData = preGenerationData.ClusterSteps;
            List<int> rows = new List<int>();//clusterGenerator.GenerateSectorRows(stepsData.Count);
            
            List<ClusterStep> steps = new List<ClusterStep>();

            int total = 0;
            for (int i = 0; i < stepsData.Count; i++)
            {
                rows.Add(stepsData[i].Sectors.Count);
                List<SectorTemplate> sectorTemplates = new List<SectorTemplate>();
                for (int j = 0; j < stepsData[i].Sectors.Count; j++)
                {
                    SectorPreGenerationData data = stepsData[i].Sectors[j];
                    sectorTemplates.Add(GenerateSectorDataFromPreGen(0, i, SectorType.Playable, data));
                    total++;
                }
                steps.Add(new ClusterStep(sectorTemplates));
            }

            // Distribute the undiscovered star fragments
            // if (totalStarFragments > 0)
            // {
            //     List<SectorTemplate> allSectors = new List<SectorTemplate>();
            //     for (int i = 1; i < steps.Count; i++)
            //     {
            //         allSectors.AddRange(steps[i].SectorTemplates);
            //     }
            //
            //     while (totalStarFragments > 0)
            //     {
            //         if (allSectors.Count <= 0)
            //             break;
            //         int selectedIndex = Random.Range(0, allSectors.Count);
            //         allSectors[selectedIndex].sectorOpportunities.Add(SectorOpportunity.StarFragment);
            //         allSectors.RemoveAt(selectedIndex);
            //         totalStarFragments--;
            //     }
            // }
            
            steps.Add(new ClusterStep(GenerateSectorTemplate(0, rows.Count, 0, total, SectorType.WarpGate, "sector_end")));

            for (int i = 0; i < steps.Count - 1; i++)
            {
                clusterStepLinker.Link(steps[i], steps[i + 1]);
            }

            string clusterName = GetClusterName(0);
            
            return new Cluster(clusterName, steps);
        }

        public string GetClusterName(int clusterIndex)
        {
            if (!saveClusterNames.IsPopulated)
                saveClusterNames.Value = new List<string>();
            while (saveClusterNames.Value.Count <= clusterIndex)
                saveClusterNames.Value.Add($"{nameGenerator.GetName()} Cluster");
            return saveClusterNames.Value[clusterIndex];
        }

        public void NavigateToSector(string sectorId)
        {
            void SetupAtSector()
            {
                AsteroidManager.Instance.ClearAllUnclaimedRewards();
                ResetManager.Instance.ResetForSectorJump();
                LoadSector(CurrentCluster, sectorId);    
                onSetArrivalName?.Invoke(GetCurrentSector().SectorName);
            }
            ResetManager.Instance.RegisterPreReset();
            sectorJump.ExecuteJump(SetupAtSector);
        }

        public bool TryGetSector(string levelId, out SectorTemplate sector)
        {
            return CurrentCluster.TryGetLevel(levelId, out sector);
        }

        private void LoadSector(Cluster cluster, string levelId)
        {
            CurrentCluster = cluster;
            if (!TryGetSector(levelId, out SectorTemplate sector))
            {
                StartCluster(cluster);
                return;
            }
            int sectorIndex = GetTotalSectorsToCluster(ClusterIndex) + CurrentCluster.GetRowIndex(sector.sectorId);
            double value = GenerateNewValueForSector(sectorIndex, ClusterIndex);
            sector.RegenerateValue(value);
            //currentSector = levelId;
            SectorManager.Instance.InitialiseFromTemplate(sector);
            GameManager.Instance.InitialiseAsteroidsFromCurrentLevel();
            onSetSectorName?.Invoke(sector.SectorName);
        }

        private void Awake()
        {
            instance = this;
            clusterData = DataAccessor.GetData<DataAssetClusterData>().Data;
        }

        public void NextCluster()
        {
            void CompleteJump()
            {
                if (ClusterIndex <= saveHighestCluster.Value)
                {
                    KeyValuePair<string, object> progressionParameters =
                        new KeyValuePair<string, object>("progression", $"complete_cluster_{ClusterIndex}");
                    AnalyticsEvent progressionEvent = new AnalyticsEventCustom("progression", progressionParameters);
                    AnalyticsManager.SendAnalyticEvent(progressionEvent);
                }
                AsteroidManager.Instance.ClearAllUnclaimedRewards();
                ResetManager.Instance.ResetForSectorJump();
                ClusterIndex++;
                SectorManager.Instance.ClearVisitedSectors();
                CurrentCluster = GenerateCluster(ClusterIndex);
                StartCluster(CurrentCluster);
                onSetArrivalName?.Invoke(CurrentCluster.ClusterName);
            }
            ResetManager.Instance.RegisterPreReset();
            sectorJump.ExecuteClusterJump(CompleteJump);
        }

        public void RestartCluster()
        {
            void CompleteJump()
            {
                AsteroidManager.Instance.ClearAllUnclaimedRewards();
                ResetManager.Instance.ResetForSectorJump();
                SectorManager.Instance.ClearVisitedSectors();
                CurrentCluster = GenerateCluster(ClusterIndex);
                StartCluster(CurrentCluster);
                onSetArrivalName?.Invoke(CurrentCluster.ClusterName);
            }
            ResetManager.Instance.RegisterPreReset();
            sectorJump.ExecuteClusterJump(CompleteJump);
        }

        public void ReturnToSecondCluster()
        {
            ClusterIndex = 1;
            SectorManager.Instance.ClearVisitedSectors();
            CurrentCluster = GenerateCluster(ClusterIndex);
            StartCluster(CurrentCluster);
            onSetArrivalName?.Invoke(CurrentCluster.ClusterName);
        }
        
        public void StartAtFirstCluster()
        {
            ClusterIndex = 0;
            CurrentCluster = GenerateClusterFromPreGen(tutorialCluster);//GenerateCluster(ClusterIndex);
            StartCluster(CurrentCluster);
        }

        public ClusterState GetClusterState(int clusterIndex)
        {
            int current = ClusterIndex;
            if (clusterIndex < current)
                return ClusterState.Completed;
            if (clusterIndex == current)
                return ClusterState.InProgress;
            if (clusterIndex == current + 1)
                return ClusterState.Available;
            return clusterIndex < saveHighestCluster.Value ? ClusterState.Visited : ClusterState.Unvisited;
        }

        // private void Start()
        // {
        //     Debug.Log(GetCumulativeRequiredMinValue(2));
        //     Debug.Log(GetCumulativeRequiredMinValue(5));
        //     Debug.Log(GetCumulativeRequiredMinValue(20));
        //     Debug.Log(GetCumulativeRequiredMinValue(50));
        //     Debug.Log(GetCumulativeRequiredMinValue(100));
        // }
    }
}