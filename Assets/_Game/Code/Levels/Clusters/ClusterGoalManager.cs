using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Tutorial;
using Fumb.Save;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Sectors
{
    public class ClusterGoalManager : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<int> onSetTargetCluster;

        [SerializeField]
        private FTUEValueProgressDisplay progressDisplay;

        [ManagedSaveValue("ClusterGoalManagerCurrentGoal")]
        private SaveValueInt saveCurrentGoalIndex;

        [SerializeField]
        private int[] goals = { 3, 6, 9, 12, 15, 18 };

        public void UpdateProgress()
        {
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            
            if (saveCurrentGoalIndex >= goals.Length)
            {
                progressDisplay.Setup(false, "");
                onSetTargetCluster?.Invoke(-1);
                return;
            }

            void OnComplete()
            {
                saveCurrentGoalIndex.Value++;
                UpdateProgress();
            }

            int highestReached = ClusterManager.Instance.HighestCluster;
            int currentGoal = goals[saveCurrentGoalIndex.Value];
            
            progressDisplay.Setup(true, $"Reach Cluster #{currentGoal}");
            progressDisplay.UpdateValue(highestReached, currentGoal);
            
            onSetTargetCluster?.Invoke(currentGoal);
            
            if (highestReached >= currentGoal)
                progressDisplay.CompleteStep(OnComplete);
        }
    }
}
