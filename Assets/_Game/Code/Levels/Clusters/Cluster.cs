using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class Cluster
    {
        [SerializeField]
        private string clusterName;

        public string ClusterName => clusterName;
        
        [SerializeField]
        private List<ClusterStep> steps;

        public List<ClusterStep> Steps => steps;

        private List<SectorTemplate> LevelTemplates
        {
            get
            {
                List<SectorTemplate> allTemplates = new List<SectorTemplate>();
                for (int i = 0; i < steps.Count; i++)
                    allTemplates.AddRange(steps[i].SectorTemplates);
                return allTemplates;
            }
        }

        public Cluster(string name, List<ClusterStep> clusterSteps)
        {
            clusterName = name;
            steps = clusterSteps;
        }

        public SectorTemplate GetLevel(int index)
        {
            return LevelTemplates[Mathf.Clamp(index, 0, LevelTemplates.Count - 1)];
        }

        public bool TryGetLevel(string levelId, out SectorTemplate sectorTemplate)
        {
            for (int i = 0; i < LevelTemplates.Count; i++)
            {
                if (!LevelTemplates[i].sectorId.Equals(levelId)) 
                    continue;
                sectorTemplate = LevelTemplates[i];
                return true;
            }

            sectorTemplate = null;
            return false;
        }

        public int GetRowIndex(string levelId)
        {
            for (int i = 0; i < steps.Count; i++)
            {
                if (steps[i].SectorTemplates.Any(sector => sector.sectorId.Equals(levelId)))
                    return i;
            }

            return 0;
        }
    }
}