using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class ClusterData
    {
        [SerializeField]
        public string clusterID;

        public string ClusterId => clusterID;

        [SerializeField]
        private int sectorRows;

        public int SectorRows => sectorRows;

        [SerializeField]
        private double costModifier;

        public double CostModifier => costModifier;

        [SerializeField]
        private double valueModifier;

        public double ValueModifier => valueModifier;

        [SerializeField]
        private int totalStarFragments;

        public int TotalStarFragments => totalStarFragments;

        [SerializeField]
        private float chunkCountModifier;

        public float ChunkCountModifier => chunkCountModifier <= 0 ? 1 : chunkCountModifier;

        [SerializeField]
        private double chunkValueModifier;

        public double ChunkValueModifier => chunkValueModifier <= 0 ? 1 : chunkValueModifier;
    }

}