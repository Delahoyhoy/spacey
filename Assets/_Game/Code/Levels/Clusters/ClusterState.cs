﻿namespace Core.Gameplay.Sectors
{
    public enum ClusterState
    {
        Unvisited,
        Visited,
        Available,
        InProgress,
        Completed,
    }
}