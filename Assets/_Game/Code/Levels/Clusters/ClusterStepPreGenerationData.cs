﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class ClusterStepPreGenerationData
    {
        [SerializeField]
        private List<SectorPreGenerationData> sectors;

        public List<SectorPreGenerationData> Sectors => sectors;
    }
}