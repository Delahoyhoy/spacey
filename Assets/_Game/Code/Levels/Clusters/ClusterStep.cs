using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class ClusterStep
    {
        [SerializeField]
        private List<SectorTemplate> sectorTemplates;

        public List<SectorTemplate> SectorTemplates => sectorTemplates;

        public int SectorCount => sectorTemplates.Count;

        public ClusterStep(List<SectorTemplate> sectors)
        {
            sectorTemplates = sectors;
        }

        public ClusterStep(SectorTemplate singleSector)
        {
            sectorTemplates = new List<SectorTemplate>();
            sectorTemplates.Add(singleSector);
        }
    }
}