﻿using System;
using System.Collections.Generic;
using Fumb.Attribute;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class ClusterGenerator
    {
        [SerializeField]
        private int minRowSectors = 2, maxRowSectors = 4;

        [SerializeField]
        private bool useFixedStartRow;

        [SerializeField] [ConditionalField("useFixedStartRow")]
        private int startRowSectors = 2;
        
        [SerializeField]
        private bool useFixedEndRow;

        [SerializeField] [ConditionalField("useFixedEndRow")]
        private int endRowSectors = 2;

        public List<int> GenerateSectorRows(int totalRows)
        {
            List<int> usedCounts = new List<int>();
            List<int> rowCounts = new List<int>();
            int i = 0;
            if (useFixedStartRow)
            {
                rowCounts.Add(startRowSectors);
                i++;
            }

            for (; i < (useFixedEndRow ? totalRows - 1 : totalRows); i++)
            {
                int selectedCount = Random.Range(minRowSectors, maxRowSectors + 1);
                if (!usedCounts.Contains(selectedCount))
                {
                    rowCounts.Add(selectedCount);
                    usedCounts.Add(selectedCount);
                    continue;
                }
                
                selectedCount = Random.Range(minRowSectors, maxRowSectors + 1);
                
                if (!usedCounts.Contains(selectedCount))
                    usedCounts.Add(selectedCount);
                rowCounts.Add(selectedCount);
            }
            
            if (useFixedEndRow && i < totalRows)
                rowCounts.Add(endRowSectors);

            return rowCounts;
        }
    }
}