﻿using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public struct ClusterStepLink
    {
        [SerializeField]
        private int fromIndex, toIndex;

        public void Link(ClusterStep fromStep, ClusterStep toStep)
        {
            fromStep.SectorTemplates[fromIndex].branchConnectors.Add(toStep.SectorTemplates[toIndex].sectorId);
        }
    }
}