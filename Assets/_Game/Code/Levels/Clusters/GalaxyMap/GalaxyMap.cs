using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.StarFragments;
using UnityEngine.UI;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    public class GalaxyMap : MonoBehaviour
    {
        [SerializeField]
        private UIScreen uiScreen;

        [SerializeField]
        private UIScreen sectorMap;

        [SerializeField]
        private GalaxyMapConnector connectorPrefab;

        [SerializeField]
        private RectTransform holder, connectorHolder;

        [SerializeField]
        private GalaxyMapInfoPanel infoDisplay, upperInfoDisplay;

        [SerializeField]
        private ClusterMarker[] markers;

        private List<GalaxyMapConnector> currentConnectors = new List<GalaxyMapConnector>();

        [SerializeField]
        private RectTransform holderRect;

        [SerializeField]
        private ScrollRect mapScroller;

        private float target = 0.5f;

        public void Setup()
        {
            SetupFromCluster(ClusterManager.Instance.CurrentCluster);
        }

        public void SetupFromCluster(Cluster cluster)
        {
            List<ClusterStep> steps = cluster.Steps;
            bool jumpAvailable = ClusterManager.Instance.ClusterCompleted;

            for (int i = 0; i < markers.Length; i++)
                markers[i].SetupFromCluster(i, ClusterManager.Instance.GetClusterState(i), OpenInfo, jumpAvailable);

            LayoutRebuilder.ForceRebuildLayoutImmediate(holder);
            int totalConnectors = 0;

            int connectorIndex = 0;
            for (int i = 1; i < markers.Length - 1; i++)
            {
                LinkMarkers(i, i + 1, connectorIndex);
                connectorIndex++;
            }
            LinkMarkers(markers.Length - 1, 0, connectorIndex);
        }

        public void SetTarget(int index)
        {
            for (int i = 0; i < markers.Length; i++)
            {
                markers[i].SetAsTarget(index == i);
            }
        }

        private void LinkMarkers(int fromIndex, int toIndex, int connectorIndex)
        {
            GalaxyMapConnector connector;
            if (connectorIndex < currentConnectors.Count)
                connector = currentConnectors[connectorIndex];
            else
                connector = Instantiate(connectorPrefab, connectorHolder);

            connector.Setup(markers[fromIndex], ClusterManager.Instance.GetClusterState(fromIndex),
                markers[toIndex], ClusterManager.Instance.GetClusterState(toIndex));
            currentConnectors.Add(connector);
        }

        public float GetNormalisedMarkerPos(int markerIndex)
        {
            if (markerIndex >= markers.Length)
                return 0.5f;
            ClusterMarker marker = markers[markerIndex];
            RectTransform markerTransform = (RectTransform)marker.transform;

            return mapScroller.CalculateFocusedScrollPosition(markerTransform).x;

            // Vector3 localPosition = markerTransform.localPosition;
            //
            // Rect rect = holderRect.rect;
            // Vector2 pivot = holder.pivot;
            //
            // float aspectRatio = (float)Screen.width / Screen.height;
            //
            // // Get the normalised position
            // float normalisedX = 0.5f + ((localPosition.x / rect.width) - 0.5f) * 2f;// - pivot.x;

            //return normalisedX;
        }

        public void ShowSectorMap()
        {
            sectorMap.Open();
            //uiScreen.Hide();
        }

        public void JumpToNextCluster()
        {
            uiScreen.CloseInstant();
            ClusterManager.Instance.NextCluster();
            infoDisplay.Close();
        }

        public void ResetTarget()
        {
            target = 0.5f;
        }

        private readonly List<ClusterState> validStates = new List<ClusterState>(new[]
            { ClusterState.Available, ClusterState.Completed, ClusterState.InProgress });

        public void OpenInfo(int clusterIndex)
        {
            if (clusterIndex >= markers.Length)
                return;
            ClusterMarker marker = markers[clusterIndex];

            // if (!validStates.Contains(ClusterManager.Instance.GetClusterState(clusterIndex)))
            //     return;

            float scrollPos = GetNormalisedMarkerPos(clusterIndex);
            //Debug.Log($"Scrolling to {scrollPos}");
            target = scrollPos;
            GalaxyMapInfoPanel displayPanel =
                marker.transform.position.y > Screen.height / 2f ? upperInfoDisplay : infoDisplay;
            displayPanel.DisplayClusterInfo(clusterIndex, ClusterManager.Instance.GetClusterName(clusterIndex), marker,
                () => uiScreen.CloseInstant(), ShowSectorMap);
        }

        private void Update()
        {
            mapScroller.horizontalNormalizedPosition =
                Mathf.Lerp(mapScroller.horizontalNormalizedPosition, target, Time.deltaTime * 4f);
        }
    }
}