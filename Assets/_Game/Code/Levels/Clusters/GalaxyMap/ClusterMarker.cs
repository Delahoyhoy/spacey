﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Mothership;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.Gameplay.Sectors
{
    public class ClusterMarker : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private Image marker, ring;

        [SerializeField]
        private GameObject currentLocationMarker, targetMarker;

        [SerializeField]
        private ClusterMarkerConfig[] ClusterConfigs;

        [SerializeField]
        private GalaxyMapConditionalObject[] conditionalObjects;

        [SerializeField]
        private Image mothershipIcon;

        [SerializeField]
        private UnityEvent<bool> onSetJumpAvailable;

        private Action onPress;

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void SetAsTarget(bool target)
        {
            targetMarker.SetActive(target);
        }
        
        public void SetupFromCluster(int clusterIndex, ClusterState state, Action<int> pressAction, bool jumpAvailable)
        {
            gameObject.name = $"Marker - {clusterIndex}";
            SetActive(true);
            onPress = () => pressAction?.Invoke(clusterIndex);
            bool currentLocation = state.Equals(ClusterState.InProgress);
            currentLocationMarker.SetActive(currentLocation);
            if (currentLocation && mothershipIcon)
                mothershipIcon.sprite = MothershipManager.Instance.MothershipIcon;

            // endPointMarker.SetActive(isEndPoint);
            // marker.enabled = !isEndPoint;

            for (int i = 0; i < ClusterConfigs.Length; i++)
            {
                if (!ClusterConfigs[i].GetMarkerValid(state, out Color colour, out Sprite sprite, out Sprite ringSprite))
                    continue;
                marker.color = colour;
                marker.sprite = sprite;
                bool ringActive = ringSprite != null;
                ring.enabled = ringActive;
                if (ringActive)
                    ring.sprite = ringSprite;
                break;
            }

            for (int i = 0; i < conditionalObjects.Length; i++)
                conditionalObjects[i].SetActive(state);
            onSetJumpAvailable?.Invoke(state.Equals(ClusterState.Available) && jumpAvailable);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            onPress?.Invoke();
        }
    }

}