﻿using System;
using Fumb.Attribute;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public struct ClusterMapConnectorConfig
    {
        [SerializeField]
        private MapConnectorCondition connectionCondition;
        
        [SerializeField] [ConditionalField("connectionCondition", false, 
            MapConnectorCondition.OriginDependent, MapConnectorCondition.RequiresBoth)]
        private ClusterState originState;
        
        [SerializeField] [ConditionalField("connectionCondition", false, 
            MapConnectorCondition.TargetDependent, MapConnectorCondition.RequiresBoth)]
        private ClusterState targetState;

        [SerializeField]
        private Color lineColour;

        [SerializeField]
        private Sprite lineSprite;

        public bool ShouldApplyEffect(ClusterState origin, ClusterState target, out Color colour, out Sprite sprite)
        {
            colour = lineColour;
            sprite = lineSprite;
            return GetOriginMatch(origin) && GetTargetMatch(target);
        }

        private bool GetOriginMatch(ClusterState origin)
        {
            return connectionCondition == MapConnectorCondition.TargetDependent || originState.Equals(origin);
        }
        
        private bool GetTargetMatch(ClusterState target)
        {
            return connectionCondition == MapConnectorCondition.OriginDependent || targetState.Equals(target);
        }
    }
}