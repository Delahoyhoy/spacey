﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public class GalaxyMapConditionalObject
    {
        [SerializeField]
        private List<ClusterState> validStates;

        [SerializeField]
        private GameObject[] objects;

        public void SetActive(ClusterState state)
        {
            bool active = validStates.Contains(state);
            for (int i = 0; i < objects.Length; i++)
                objects[i].SetActive(active);
        }
    }
}