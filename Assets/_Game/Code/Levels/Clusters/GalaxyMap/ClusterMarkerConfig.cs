﻿using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public struct ClusterMarkerConfig
    {
        [SerializeField]
        private ClusterState targetState;
        
        [SerializeField]
        private Color markerColour;

        [SerializeField]
        private Sprite markerSprite, ringSprite;

        public bool GetMarkerValid(ClusterState state, out Color colour, out Sprite sprite, out Sprite ring)
        {
            colour = markerColour;
            sprite = markerSprite;
            ring = ringSprite;
            return targetState.Equals(state);
        }
    }
}