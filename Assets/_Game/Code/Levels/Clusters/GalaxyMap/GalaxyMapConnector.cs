﻿using System;
using UnityEngine;
using UnityEngine.UI;

using Core.Gameplay.UserInterface;

namespace Core.Gameplay.Sectors
{
    public class GalaxyMapConnector : ConnectorLine
    {
        [SerializeField]
        private Image graphic;

        [SerializeField]
        private ClusterMapConnectorConfig[] connectorConfigs;
        
        [SerializeField]
        private Color defaultColour;

        [SerializeField]
        private Sprite defaultSprite;

        public void Setup(ClusterMarker fromMarker, ClusterState fromState, ClusterMarker toMarker, ClusterState toState)
        {
            gameObject.SetActive(true);
            SetPosition(fromMarker.transform, toMarker.transform);
            for (int i = 0; i < connectorConfigs.Length; i++)
            {
                if (!connectorConfigs[i].ShouldApplyEffect(fromState, toState, out Color colour, out Sprite sprite))
                    continue;
                graphic.color = colour;
                graphic.sprite = sprite;
                return;
            }
            graphic.color = defaultColour;
            graphic.sprite = defaultSprite;
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}