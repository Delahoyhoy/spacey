﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.StarFragments;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Core.Gameplay.Sectors
{
    public class GalaxyMapInfoPanel : MonoBehaviour
    {
        [SerializeField]
        private UIScreen uiScreen;

        [SerializeField]
        private Text clusterNameText;

        [SerializeField]
        private Text clusterState;

        [SerializeField]
        private Text totalFragments, totalArtifacts;
        
        [SerializeField]
        private Button travelButton, viewButton;

        [SerializeField]
        private RectTransform mainBubble, bubble;

        [SerializeField]
        private AnchoredUI bubbleAnchor;

        private Action onNavigate, onView;

        public void DisplayClusterInfo(int clusterIndex, string clusterName, ClusterMarker marker, Action navigate, Action view)
        {
            onNavigate = navigate;
            gameObject.SetActive(true);
            Transform markerTransform = marker.transform;

            //bubbleAnchor.Target = markerTransform;

            mainBubble.position = markerTransform.position;

            // Vector3 bubblePos = bubble.localPosition;
            // bubblePos.x = -mainBubble.localPosition.x;
            // bubble.localPosition = bubblePos;

            uiScreen.Open();
            ClusterState state = ClusterManager.Instance.GetClusterState(clusterIndex);
            clusterNameText.text = state == ClusterState.Unvisited ? "??? Cluster" : clusterName;
            clusterState.text = state.ToString();
            
            totalFragments.text = state == ClusterState.Unvisited ? "?/?" : $"{StarFragmentManager.Instance.GetFragmentsObtained(clusterIndex)}/{ClusterManager.Instance.GetStarFragmentsAtCluster(clusterIndex)}";

            bool isActiveCluster = state.Equals(ClusterState.InProgress);

            onView = view;
            void ViewCluster()
            {
                onView?.Invoke();
            }
            
            viewButton.gameObject.SetActive(isActiveCluster);
            viewButton.onClick.RemoveAllListeners();
            viewButton.onClick.AddListener(ViewCluster);
            
            
            bool accessible = SectorManager.Instance.SectorCompleted &&
                              SectorManager.Instance.IsLastSector;
            travelButton.gameObject.SetActive(accessible);
            if (!accessible)
                return;

            void Navigate()
            {
                ClusterManager.Instance.NextCluster();
                onNavigate?.Invoke();
                Close();
            }

            travelButton.interactable = state.Equals(ClusterState.Available);
            travelButton.onClick.RemoveAllListeners();
            travelButton.onClick.AddListener(Navigate);
        }

        public void Close()
        {
            uiScreen.Close();
        }
    }
}
