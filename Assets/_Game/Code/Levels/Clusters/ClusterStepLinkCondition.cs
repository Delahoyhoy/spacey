﻿using System;
using UnityEngine;

namespace Core.Gameplay.Sectors
{
    [Serializable]
    public struct ClusterStepLinkCondition
    {
        [SerializeField]
        private int fromCount, toCount;

        [SerializeField]
        private ClusterStepLink[] stepLinks;

        public bool GetValid(int from, int to, out ClusterStepLink[] links)
        {
            links = stepLinks;
            return fromCount.Equals(from) && toCount.Equals(to);
        }
    }
}