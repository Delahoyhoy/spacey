﻿using System;
using System.Collections.Generic;
using System.IO;
using Fumb.Save;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Sectors
{
    public class NameGenerator : MonoBehaviour
    {
        [SerializeField]
        private string fileName;
        
        [SerializeField]
        private List<string> possibleNames;

        private List<string> usableNames = new List<string>(); 

        [ManagedSaveValue("NameGeneratorUsedNames")]
        private SaveValueListString saveUsedNames;

        private bool initialised;
        
        [ContextMenu("Import Names")]
        public void ImportNames()
        {
            string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

            if (File.Exists(filePath))
            {
                string[] lines = File.ReadAllLines(filePath);
                possibleNames.Clear();
                possibleNames.AddRange(lines);
                Debug.Log("Names imported successfully.");
            }
            else
            {
                Debug.LogError("File not found: " + filePath);
            }
        }

        private void Initialise()
        {
            if (initialised)
                return;
            initialised = true;
            if (!saveUsedNames.IsPopulated)
            {
                usableNames = new List<string>(possibleNames.ToArray());
                return;
            }

            List<string> usedNames = saveUsedNames.Value; 
            usableNames = new List<string>();
            for (int i = 0; i < possibleNames.Count; i++)
            {
                if (!usableNames.Contains(possibleNames[i]))
                    usableNames.Add(possibleNames[i]);
            }
        }

        private void Start()
        {
            Initialise();
        }

        public string GetName()
        {
            Initialise();
            if (!saveUsedNames.IsPopulated)
                saveUsedNames.Value = new List<string>();
            if (usableNames.Count <= 0)
            {
                usableNames = new List<string>(possibleNames.ToArray());
                saveUsedNames.Value.Clear();
            }
            int selectedIndex = Random.Range(0, usableNames.Count);
            string selected = usableNames[selectedIndex];
            
            saveUsedNames.Value.Add(selected);
            usableNames.RemoveAt(selectedIndex);

            return selected;
        }
    }
}