﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[System.Serializable]
//public struct Highlight
//{
//    public string highlightID;
//    public Transform target;
//    public float radius;
//}

public class HighlightScreen : MonoBehaviour {

    public Image image;

    [SerializeField]
    private Highlight[] allHighlights;

    private Dictionary<string, Highlight> highlightDictionary = new Dictionary<string, Highlight>();

    int imageHeight = 512, imageWidth;

    Texture2D GetTexture(Vector2 screenPos, float maxDist = 128f)
    {
        
        Texture2D newTexture = new Texture2D(imageWidth, imageHeight, TextureFormat.RGBA32, false);
        //Color[,] colors = new Color[Screen.width, Screen.height];
        for (int x = 0; x < newTexture.width; x++)
        {
            for (int y = 0; y < newTexture.height; y++)
            {
                //Vector3 mousePos = Input.mousePosition;
                float halfDist = maxDist / 2f;
                float distance = Vector2.Distance(screenPos, new Vector2(x, y));
                if (distance < maxDist)
                //if (Mathf.Abs(x - mousePos.x) < 32 && Mathf.Abs(y - mousePos.y) < 32)
                {
                    if (distance >= halfDist)
                    {
                        newTexture.SetPixel(x, y, new Color(0, 0, 0, 0.75f * ((distance - halfDist) / halfDist)));
                    }
                    else
                    newTexture.SetPixel(x, y, Color.clear);
                }
                else
                {
                    newTexture.SetPixel(x, y, new Color(0, 0, 0, 0.75f));
                }
            }
        }
        newTexture.Apply();
        return newTexture;
    }

    public void ShowHighlight(string highlightID)
    {
        if (!initialised)
            Init();
        if (highlightDictionary.ContainsKey(highlightID))
        {
            gameObject.SetActive(true);
            Highlight highlight = highlightDictionary[highlightID];
            imageWidth = Mathf.RoundToInt(((float)Screen.width / Screen.height) * imageHeight);
            //Vector2 relativeScreenPos = new Vector2(highlight.target.position.x / Screen.width, highlight.target.position.y / Screen.height);
            //Vector2 scaledPos = new Vector2(relativeScreenPos.x * imageWidth, relativeScreenPos.y * imageHeight);
            //Sprite newSprite = Sprite.Create(GetTexture(scaledPos, highlight.radius), new Rect(0, 0, imageWidth, imageHeight), Vector2.zero);
            //image.sprite = newSprite;
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    Texture2D GetSquareTexture(Vector2 screenPos)
    {
        Texture2D newTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBA32, false);
        //Color[,] colors = new Color[Screen.width, Screen.height];
        for (int x = 0; x < newTexture.width; x++)
        {
            for (int y = 0; y < newTexture.height; y++)
            {
                //Vector3 mousePos = Input.mousePosition;
                float maxDist = 128f;
                float halfDist = maxDist / 2f;
                float distance = Vector2.Distance(screenPos, new Vector2(x, y));
                //if (distance < maxDist)
                float dx = Mathf.Abs(x - screenPos.x), dy = Mathf.Abs(y - screenPos.y);
                if (Mathf.Abs(x - screenPos.x) < maxDist && Mathf.Abs(y - screenPos.y) < maxDist)
                {
                    float minDist = Mathf.Max(dx, dy);
                    if (minDist < halfDist)
                        newTexture.SetPixel(x, y, Color.clear);
                    else
                        newTexture.SetPixel(x, y, new Color(0, 0, 0, 0.75f * ((minDist - halfDist) / halfDist)));
                }
                else
                {
                    newTexture.SetPixel(x, y, new Color(0, 0, 0, 0.75f));
                }
            }
        }
        newTexture.Apply();
        return newTexture;
    }

    private bool initialised = false;

    private void Init()
    {
        for (int i = 0; i < allHighlights.Length; i++)
        {
            if (!highlightDictionary.ContainsKey(allHighlights[i].highlightID))
                highlightDictionary.Add(allHighlights[i].highlightID, allHighlights[i]);
        }
        initialised = true;
    }

    // Use this for initialization
    void Awake () {
        
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
