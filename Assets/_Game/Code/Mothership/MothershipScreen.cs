using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.StarFragments;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Mothership
{
    public class MothershipScreen : MonoBehaviour
    {
        [SerializeField]
        private MothershipDepartmentDisplay[] departments;

        private Dictionary<MothershipDepartment, MothershipDepartmentDisplay> departmentDisplayLookup =
            new Dictionary<MothershipDepartment, MothershipDepartmentDisplay>();

        [SerializeField]
        private Slider overallProgressBar, previewProgressBar;

        [SerializeField]
        private MothershipSocket[] sockets;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private UnityEvent<bool> onSetLevelUpAvailable;

        [SerializeField]
        private GameObject maxText;

        [SerializeField]
        private Text starFragmentCountText;

        private int hideFlags;
        private static readonly int HideDisplay = Animator.StringToHash("Hide");
        private static readonly int DisplayIn = Animator.StringToHash("In");
        private static readonly int DisplayReturn = Animator.StringToHash("Return");

        [SerializeField]
        private Text classText;

        public void Hide()
        {
            bool hidden = hideFlags > 0;
            hideFlags++;
            if (!hidden && hideFlags > 0)
                animator.SetTrigger(HideDisplay);
        }

        public void Unhide()
        {
            bool hidden = hideFlags > 0;
            hideFlags--;
            if (hidden && hideFlags <= 0)
                animator.SetTrigger(DisplayReturn);
        }

        public void Setup()
        {
            MothershipManager mothershipManager = MothershipManager.Instance;
            int currentLevel = mothershipManager.CurrentLevel;
            float progress = mothershipManager.GetProgressToNext();

            overallProgressBar.value = currentLevel + (progress / 2f) + 0.3f;
            previewProgressBar.value = currentLevel + 1;

            MothershipSocketState GetState(int index)
            {
                if (index > currentLevel + 1)
                    return MothershipSocketState.Locked;
                if (index > currentLevel)
                    return MothershipSocketState.Preview;
                return currentLevel == index ? MothershipSocketState.Active : MothershipSocketState.Unlocked;
            }
            
            for (int i = 0; i < sockets.Length; i++)
            {
                sockets[i].Setup(GetState(i), mothershipManager.GetStarFragmentsRequired(i));
            }

            classText.text = $"{mothershipManager.MothershipClass} Class";

            bool maxLevel = mothershipManager.IsMaxLevel;
            bool levelUpAvailable = mothershipManager.CanLevelUp;

            maxText.SetActive(maxLevel);
            starFragmentCountText.gameObject.SetActive(!maxLevel);

            if (!maxLevel)
                starFragmentCountText.text =
                    $"{StarFragmentManager.Instance.Fragments}/{mothershipManager.NextLevelUpCost}";
            
            onSetLevelUpAvailable?.Invoke(levelUpAvailable);
        }
    }
}