using System;
using UnityEngine;

namespace Core.Gameplay.Mothership
{
    [Serializable]
    public class MothershipLevel
    {
        [SerializeField]
        private int mothershipLevel;

        public int Level => mothershipLevel;

        [SerializeField]
        private int starFragmentLevel;

        public int StarFragmentRequirement => starFragmentLevel;
    }
}
