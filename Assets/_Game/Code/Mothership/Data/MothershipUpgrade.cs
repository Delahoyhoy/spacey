﻿using System;
using UnityEngine;

namespace Core.Gameplay.Mothership
{
    [Serializable]
    public class MothershipUpgrade
    {
        [SerializeField]
        private MothershipDepartment departmentID;

        public MothershipDepartment DepartmentId => departmentID;
        
        [SerializeField]
        private int shipLevelUnlock;

        public int ShipLevelUnlock => shipLevelUnlock;

        [SerializeField]
        private int departmentLevel;

        public int DepartmentLevel => departmentLevel;

        [SerializeField]
        private double upgradeCost;

        public double UpgradeCost => upgradeCost;
    }
}