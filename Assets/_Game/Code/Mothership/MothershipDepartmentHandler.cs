﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipDepartmentHandler : MonoBehaviour
    {
        [SerializeField]
        private MothershipDepartment department;

        [SerializeField]
        private UnityEvent<int> onSetDepartmentLevel;

        [SerializeField]
        private UnityEvent<bool> onSetDepartmentPremium;

        [SerializeField]
        private UnityEvent<int, bool> onSetDepartmentValues;

        private bool setup;

        private void Start()
        {
            if (setup)
                return;

            //onSetDepartmentLevel?.Invoke(MothershipManager.Instance.GetDepartmentLevel(department));
            MothershipManager.Instance.onSetMothershipDepartmentLevel += UpdateDepartment;

            setup = true;
        }

        private void UpdateDepartment(MothershipDepartment dept, int level, bool premium)
        {
            if (!department.Equals(dept))
                return;
            onSetDepartmentLevel?.Invoke(level);
            onSetDepartmentPremium?.Invoke(premium);
            onSetDepartmentValues?.Invoke(level, premium);
        }
    }
}