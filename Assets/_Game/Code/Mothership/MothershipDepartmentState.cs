﻿namespace Core.Gameplay.Mothership
{
    public enum MothershipDepartmentState
    {
        Locked,
        Unlockable,
        MaxLevel,
    }
}