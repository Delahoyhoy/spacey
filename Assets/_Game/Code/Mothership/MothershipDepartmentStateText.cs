﻿using System;
using UnityEngine;

namespace Core.Gameplay.Mothership
{
    [Serializable]
    public class MothershipDepartmentStateText
    {
        [SerializeField]
        private MothershipDepartmentState state;

        public MothershipDepartmentState State => state;
        
        [SerializeField]
        private string displayText;

        public string DisplayText => displayText;
    }
}