﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Mothership
{
    public class MothershipSocket : MonoBehaviour
    {
        [SerializeField]
        private GameObject lockedIcon, glow;

        [SerializeField]
        private Image icon;

        [SerializeField]
        private Color activeColour, inactiveColour;
        
        [SerializeField]
        private Text fragmentRequirement;

        public void Setup(MothershipSocketState state, int fragmentsRequired)
        {
            fragmentRequirement.gameObject.SetActive(state.Equals(MothershipSocketState.Preview));
            fragmentRequirement.text = fragmentsRequired.ToString();
            
            icon.gameObject.SetActive(!state.Equals(MothershipSocketState.Locked));
            icon.color = state.Equals(MothershipSocketState.Active) ? activeColour : inactiveColour;

            transform.localScale = Vector3.one * (state.Equals(MothershipSocketState.Active) ? 1.2f : 1f);
            
            lockedIcon.SetActive(state.Equals(MothershipSocketState.Locked));
            glow.SetActive(state >= MothershipSocketState.Unlocked);
        }
    }
}