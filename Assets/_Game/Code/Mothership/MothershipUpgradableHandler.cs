using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.StarFragments;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipUpgradableHandler : StarFragmentEvent
    {
        [SerializeField]
        private UnityEvent<bool> onMothershipUpgradable;

        private bool isInitialised;

        protected override void Start()
        {
            base.Start();
            if (isInitialised)
                return;
            MothershipManager.Instance.onSetMothershipLevel += CheckCurrentCount;
            isInitialised = true;
        }

        private void CheckCurrentCount(int mothershipLevel)
        {
            OnFragmentCountUpdated(StarFragmentManager.Instance.Fragments);
        }
        
        protected override void OnFragmentCountUpdated(int newCount)
        {
            onMothershipUpgradable?.Invoke(newCount >= MothershipManager.Instance.NextLevelUpCost);
        }
    }

}