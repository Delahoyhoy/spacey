using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipDepartmentUpgradableHandler : MonoBehaviour
    {
        [SerializeField]
        private MothershipDepartment department;

        [SerializeField]
        private UnityEvent<bool> onSetUpgradable;

        private double cachedValue;

        private bool initialised;

        private bool CanLevelUp => MothershipManager.Instance.GetCanDepartmentLevelUp(department, cachedValue);
        
        private void Start()
        {
            if (initialised)
                return;
            
            CurrencyController.Instance.onCurrencyUpdate += UpdateCurrency;
            MothershipManager.Instance.onSetMothershipDepartmentLevel += UpdateDepartmentLevel;
            MothershipManager.Instance.onSetMothershipLevel += MothershipUpgrade; 
            
            UpdateCurrency(CurrencyType.SoftCurrency, CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency));
            
            initialised = true;
        }

        private void MothershipUpgrade(int level)
        {
            onSetUpgradable?.Invoke(CanLevelUp);
        }

        private void UpdateCurrency(CurrencyType currencyType, double amount)
        {
            if (currencyType != CurrencyType.SoftCurrency)
                return;
            cachedValue = amount;
            onSetUpgradable?.Invoke(CanLevelUp);
        }

        private void UpdateDepartmentLevel(MothershipDepartment dept, int level, bool premium)
        {
            if (dept != department)
                return;
            onSetUpgradable?.Invoke(CanLevelUp);
        }
    }
}
