﻿using Core.Gameplay.Economy;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Mothership
{
    public class MothershipDepartmentDisplay : MonoBehaviour
    {
        [SerializeField]
        private MothershipDepartment department;

        [SerializeField]
        private MothershipDepartmentLevelIcon[] levelIcons;

        [SerializeField]
        private Text descriptionText;
        
        [SerializeField]
        private CurrencyButton upgradeButton;

        [SerializeField]
        private GameObject lockedButton;

        [SerializeField]
        private UnityEvent<bool> onSetPremiumActive;

        [SerializeField]
        private UnityEvent onPurchased;

        private int CurrentDepartmentLevel => MothershipManager.Instance.GetDepartmentLevel(department);
        private int MaxDepartmentLevel => MothershipManager.Instance.GetMaxAvailableLevel(department);

        public void Setup()
        {
            int mothershipLevel = MaxDepartmentLevel;
            int departmentLevel = CurrentDepartmentLevel;
            for (int i = 0; i < levelIcons.Length; i++)
            {
                int requiredLevel = MothershipManager.Instance.GetRequiredShipLevel(department, i);
                levelIcons[i].Setup(i, departmentLevel, requiredLevel, mothershipLevel);
            }

            MothershipDepartmentState state =
                MothershipManager.Instance.GetDepartmentState(department, out string description, out double cost);
            
            upgradeButton.SetActive(state.Equals(MothershipDepartmentState.Unlockable));
            lockedButton.SetActive(state.Equals(MothershipDepartmentState.Locked));

            descriptionText.text = description;

            switch (state)
            {
                case MothershipDepartmentState.Unlockable:
                    upgradeButton.SetThreshold(cost);
                    break;
            }
            onSetPremiumActive?.Invoke(MothershipManager.Instance.GetDepartmentPremiumActive(department));
        }

        public void Upgrade()
        {
            if (MothershipManager.Instance.LevelUpDepartment(department, null))
                Setup();
        }

        public void PurchasePremium()
        {
            void OnComplete()
            {
                onPurchased?.Invoke();
                onSetPremiumActive?.Invoke(true);
            }
            
            MothershipManager.Instance.PurchasePremiumDepartment(department, OnComplete);
        }
    }
}