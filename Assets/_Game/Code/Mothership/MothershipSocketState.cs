﻿namespace Core.Gameplay.Mothership
{
    public enum MothershipSocketState
    {
        Locked,
        Preview,
        Unlocked,
        Active,
    }
}