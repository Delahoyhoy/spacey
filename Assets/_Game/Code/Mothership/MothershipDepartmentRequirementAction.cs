using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipDepartmentRequirementAction : MonoBehaviour
    {
        [SerializeField]
        private MothershipDepartment department;

        [SerializeField]
        private int minLevel;

        [SerializeField]
        private UnityEvent onRequirementMet, onRequirementNotMet;

        public void Trigger()
        {
            if (GameVersionManager.CurrentVersion < GameVersion.Version3)
            {
                onRequirementMet?.Invoke();
                return;
            }
            
            int level = MothershipManager.Instance.GetDepartmentLevel(department);
            if (level >= minLevel)
                onRequirementMet?.Invoke();
            else
                onRequirementNotMet?.Invoke();
        }
    }
}
