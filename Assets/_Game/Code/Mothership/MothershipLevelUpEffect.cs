using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipLevelUpEffect : MonoBehaviour
    {
        [SerializeField]
        private Animator screenAnim;

        [SerializeField]
        private ParticleSystem[] particleSystems;
        
        public float shakeIntensity, zoomLevel;

        [SerializeField]
        private float totalPlayTime = 5f;

        private bool interrupted, running;

        [SerializeField]
        private UnityEvent onProcessLevelUp;

        // Animator property indices
        private static readonly int LevelUp = Animator.StringToHash("LevelUp");
        private static readonly int In = Animator.StringToHash("In");

        public void RunLevelUp()
        {
            if (!running && MothershipManager.Instance.CanLevelUp)
                StartCoroutine(RunLevelUpEffect());
        }
        
        public void TriggerLevelUp()
        {
            MothershipManager.Instance.LevelUp();
            onProcessLevelUp?.Invoke();
        }
        
        private IEnumerator RunLevelUpEffect()
        {
            running = true;
            interrupted = false;
            screenAnim.SetTrigger(LevelUp);
            
            for (int i = 0; i < particleSystems.Length; i++)
                particleSystems[i].Play();
            
            CameraShake.Instance.StartShake();
            for (float f = 0; f < totalPlayTime; f += Time.deltaTime)
            {
                BasicCameraController.Instance.SetZoomMult(zoomLevel);
                CameraShake.Instance.IntensityMult = shakeIntensity;
                if (interrupted)
                    break;
                yield return null;
            }
            BasicCameraController.Instance.SetZoomMult(1f);
            CameraShake.Instance.StopShaking();
            running = false;
        }

        public void Done()
        {
            interrupted = true;
            screenAnim.SetTrigger(In);
        }
    }
}
