﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Mothership
{
    public class MothershipDepartmentLevelIcon : MonoBehaviour
    {
        [SerializeField]
        private GameObject unlockedFrame, availableFrame, lockedFrame;

        public void Setup(int level, int currentDepartmentLevel, int requiredLevel, int mothershipLevel)
        {
            bool available = mothershipLevel > requiredLevel;
            bool unlocked = currentDepartmentLevel > requiredLevel;
            unlockedFrame.SetActive(unlocked);
            availableFrame.SetActive(available && !unlocked);
            lockedFrame.SetActive(!available && !unlocked);
        }
    }
}