﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipLevelHandler : MonoBehaviour
    {
        private bool setup;

        [SerializeField]
        private UnityEvent<int> onSetMothershipLevel;

        private void Start()
        {
            if (setup)
                return;

            SetMothershipLevel(MothershipManager.Instance.CurrentLevel);
            MothershipManager.Instance.onSetMothershipLevel += SetMothershipLevel;
            
            setup = true;
        }

        private void SetMothershipLevel(int level)
        {
            onSetMothershipLevel?.Invoke(level);
        }
    }
}