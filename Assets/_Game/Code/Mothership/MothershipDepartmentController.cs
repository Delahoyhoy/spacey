﻿using System;
using System.Collections.Generic;
using Fumb.Attribute;
using Fumb.Purchasing;
using Fumb.Save;
using UnityEngine;

namespace Core.Gameplay.Mothership
{
    [Serializable]
    public class MothershipDepartmentController : MonoBehaviour, ISaveValueVariant
    {
        [SerializeField]
        private MothershipDepartment department;

        public MothershipDepartment Department => department;

        [SerializeField]
        private int defaultLevel;

        public int DefaultLevel => defaultLevel;
        
        [SerializeField]
        private MothershipDepartmentStateText[] stateDescriptions;

        [SerializeField]
        private bool hasPremiumPurchase;
        
        [SerializeField] [ConditionalField("hasPremiumPurchase")]
        private string premiumPurchaseId;

        private Dictionary<MothershipDepartmentState, string> stateDescriptionLookup =
            new Dictionary<MothershipDepartmentState, string>();

        [ManagedSaveValue("MothershipDepartmentControllerLevel")]
        private SaveValueInt saveCurrentLevel;

        [ManagedSaveValue("MothershipDepartmentControllerPremiumActive")]
        private SaveValueBool savePremiumActive;

        public bool PremiumActive => hasPremiumPurchase && (savePremiumActive.Value || 
                                                            PurchasingManager.HasPurchasedProduct(premiumPurchaseId));

        private List<int> shipRequiredLevels = new List<int>();
        private List<double> unlockCosts = new List<double>();
        private List<int> levels = new List<int>();

        public int CurrentLevel {
            get => saveCurrentLevel.IsPopulated? saveCurrentLevel.Value : defaultLevel;
            set => saveCurrentLevel.Value = value;
        }

        public int AvailableLevel => levels[MothershipManager.Instance.CurrentLevel];
        
        public double NextUnlockCost => GetCostAtLevel(CurrentLevel + 1);
        public bool IsMaxLevel => CurrentLevel >= shipRequiredLevels.Count;
        public bool IsMaxAvailableLevel => CurrentLevel >= AvailableLevel;

        private void Awake()
        {
            for (int i = 0; i < stateDescriptions.Length; i++)
            {
                MothershipDepartmentState state = stateDescriptions[i].State;
                if (stateDescriptionLookup.ContainsKey(state))
                    continue;
                stateDescriptionLookup.Add(state, stateDescriptions[i].DisplayText);
            }
        }
        public void ResetDepartment()
        {
            saveCurrentLevel.Value = DefaultLevel;
        }
        
        public MothershipDepartmentState GetState(out string displayText, out double cost)
        {
            displayText = "";
            cost = 0;
            MothershipDepartmentState state = GetCurrentState();
            displayText = stateDescriptionLookup.TryGetValue(state, out string display) ? display : "";
            if (state != MothershipDepartmentState.MaxLevel)
                cost = GetCostAtLevel(CurrentLevel + 1);
            return state;
        }

        private MothershipDepartmentState GetCurrentState()
        {
            if (IsMaxLevel)
                return MothershipDepartmentState.MaxLevel;
            if (AvailableLevel > CurrentLevel)
                return MothershipDepartmentState.Unlockable;
            return MothershipDepartmentState.Locked;
        }

        public void SetupValues(MothershipUpgrade[] upgrades)
        {
            shipRequiredLevels = new List<int>();
            unlockCosts = new List<double>();
            
            for (int i = 0; i < upgrades.Length; i++)
            {
                if (!department.Equals(upgrades[i].DepartmentId))
                    continue;
                shipRequiredLevels.Add(upgrades[i].ShipLevelUnlock);
                unlockCosts.Add(upgrades[i].UpgradeCost);
                levels.Add(upgrades[i].DepartmentLevel);
            }
        }

        public int GetRequiredMothershipLevel(int departmentLevel)
        {
            return shipRequiredLevels[Mathf.Clamp(departmentLevel, 0, shipRequiredLevels.Count - 1)];
        }

        public double GetCostAtLevel(int departmentLevel)
        {
            return unlockCosts[Mathf.Clamp(departmentLevel, 0, unlockCosts.Count - 1)];
        }

        public bool LevelUp(out double cost, Action<bool> onComplete)
        {
            cost = GetCostAtLevel(CurrentLevel + 1);
            if (IsMaxLevel)
            {
                onComplete?.Invoke(false);
                return false;
            }
            if (!CurrencyController.Instance.SpendCurrency(cost, CurrencyType.SoftCurrency, "mothership", $"upgrade_{department}"))
            {
                onComplete?.Invoke(false);
                return false;
            }
            IncrementLevel();
            onComplete?.Invoke(true);
            return true;
        }

        public void EnsureDepartmentLevel(int level)
        {
            if (CurrentLevel < level)
                saveCurrentLevel.Value = level;
        }

        public bool GetCanLevelUp(double currency)
        {
            if (IsMaxLevel)
                return false;
            if (CurrentLevel >= AvailableLevel)
                return false;
            double cost = GetCostAtLevel(CurrentLevel + 1);
            return currency >= cost;
        }

        public void PurchasePremium(Action onComplete)
        {
            if (!hasPremiumPurchase)
                return;
            if (PremiumActive)
            {
                OnComplete();
                return;
            }

            void OnComplete()
            {
                savePremiumActive.Value = true;
                onComplete?.Invoke();
            }
            
            PurchasingManager.PurchaseProduct(premiumPurchaseId, "MothershipScreen", OnComplete);
        }

        public void IncrementLevel()
        {
            CurrentLevel++;
        }
        
        public string SaveValueVariant => department.ToString();
        
        
        
    }
}