﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipDepartmentLevelCheck : MonoBehaviour
    {
        [SerializeField]
        private MothershipDepartment department;

        [SerializeField]
        private int levelCheck;

        [SerializeField]
        private UnityEvent onLevelReached;

        public void CheckLevel()
        {
            if (MothershipManager.Instance.GetDepartmentLevel(department) >= levelCheck)
                onLevelReached?.Invoke();
        }
    }
}