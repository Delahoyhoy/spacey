using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using Core.Gameplay.Reset;
using Core.Gameplay.StarFragments;
using Core.Gameplay.Utilities;
using Core.Gameplay.Version;
using Data;
using Fumb.Data;
using Fumb.Save;
using UnityEngine;

namespace Core.Gameplay.Mothership
{
    public class MothershipManager : MonoBehaviour
    {
        private static MothershipManager instance;
        public static MothershipManager Instance => instance ??= ObjectFinder.FindObjectOfType<MothershipManager>();
        
        [ManagedSaveValue("MothershipManagerCurrentLevel")]
        private SaveValueInt saveCurrentLevel;

        private MothershipLevel[] mothershipLevels;
        private MothershipUpgrade[] mothershipUpgrades;

        private Dictionary<MothershipDepartment, MothershipDepartmentController> departmentLookup = 
            new Dictionary<MothershipDepartment, MothershipDepartmentController>();

        [SerializeField]
        private int[] levelThresholds;

        [SerializeField]
        private MothershipDepartmentController[] departments;

        [SerializeField]
        private string[] mothershipClassLevels;

        [SerializeField]
        private Sprite[] mothershipIcons;

        [SerializeField]
        private Transform mothershipTransform;

        [SerializeField]
        private UIScreen mothershipScreen;

        public Action<int> onSetMothershipLevel;
        public Action<MothershipDepartment, int, bool> onSetMothershipDepartmentLevel;

        public string MothershipClass => GetMothershipClassAtLevel(CurrentLevel);
        public bool CanLevelUp => MaxAvailableLevel > CurrentLevel;
        public bool IsMaxLevel => CurrentLevel >= levelThresholds.Length - 1;
        public int NextLevelUpCost => GetStarFragmentsRequired(CurrentLevel + 1);

        public Sprite MothershipIcon => mothershipIcons[Mathf.Clamp(CurrentLevel, 0, mothershipIcons.Length)];

        private void Awake()
        {
            mothershipLevels = DataAccessor.GetData<DataAssetMothershipLevel>().Data;
            mothershipUpgrades = DataAccessor.GetData<DataAssetMothershipUpgrade>().Data;

            for (int i = 0; i < departments.Length; i++)
            {
                departmentLookup.Add(departments[i].Department, departments[i]);
                departments[i].SetupValues(mothershipUpgrades);
            }
            
            int levelTotal = 0;
            levelThresholds = new int[mothershipLevels.Length];
            for (int i = 1; i < mothershipLevels.Length; i++)
            {
                levelTotal += mothershipLevels[i].StarFragmentRequirement;
                levelThresholds[i] = levelTotal;
            }
        }

        private void Start()
        {
            ResetManager.Instance.finishReset += ResetAllDepartments;
            PurchaseRestoreHelper.onPurchasesRestored += HandlePurchasesRestored;
        }

        private void OnDestroy()
        {
            PurchaseRestoreHelper.onPurchasesRestored -= HandlePurchasesRestored;
        }

        private void HandlePurchasesRestored()
        {
            for (int i = 0; i < departments.Length; i++)
            {
                onSetMothershipDepartmentLevel?.Invoke(departments[i].Department, departments[i].CurrentLevel, 
                    departments[i].PremiumActive);
            }
        }
        

        public int GetStarFragmentsRequired(int level)
        {
            return levelThresholds[Mathf.Clamp(level, 0, levelThresholds.Length - 1)];
        }

        public float GetProgressToNext()
        {
            int currentLevel = CurrentLevel;
            int nextLevel = currentLevel + 1;

            if (nextLevel >= levelThresholds.Length)
                return 1;
            
            int currentStarFragments = StarFragmentManager.Instance.Fragments;
            
            int prevRequirement = GetStarFragmentsRequired(currentLevel);
            int nextRequirement = GetStarFragmentsRequired(nextLevel);

            return 1 - Mathf.Clamp01((float)(nextRequirement - currentStarFragments) / (nextRequirement - prevRequirement));
        }

        public string GetMothershipClassAtLevel(int level)
        {
            return mothershipClassLevels[Mathf.Clamp(level, 0, mothershipClassLevels.Length - 1)];
        }
        
        public void OpenMothershipScreen()
        {
            BasicCameraController.Instance.LockOnTarget(mothershipTransform);
            mothershipScreen.Open();
        }

        public void OnScreenClose()
        {
            BasicCameraController.Instance.UnlockTarget();
        }

        private bool GetDepartment(MothershipDepartment department, out MothershipDepartmentController departmentController)
        {
            return departmentLookup.TryGetValue(department, out departmentController);
        }

        public bool LevelUpDepartment(MothershipDepartment department, Action<bool> onComplete)
        {
            if (!departmentLookup.TryGetValue(department, out MothershipDepartmentController controller))
                return false;
            if (!controller.LevelUp(out double cost, onComplete))
                return false;
            onSetMothershipDepartmentLevel?.Invoke(department, controller.CurrentLevel, controller.PremiumActive);
            return true;
        }

        public bool GetCanDepartmentLevelUp(MothershipDepartment department, double currency)
        {
            if (!departmentLookup.TryGetValue(department, out MothershipDepartmentController controller))
                return false;
            return controller.GetCanLevelUp(currency);
        }

        public bool GetDepartmentAtMaxAvailableLevel(MothershipDepartment department)
        {
            if (!departmentLookup.TryGetValue(department, out MothershipDepartmentController controller))
                return false;
            return controller.IsMaxAvailableLevel;
        }

        public void PurchasePremiumDepartment(MothershipDepartment department, Action onComplete)
        {
            if (!departmentLookup.TryGetValue(department, out MothershipDepartmentController controller))
                return;
            
            void OnComplete()
            {
                onSetMothershipDepartmentLevel?.Invoke(department, controller.CurrentLevel, controller.PremiumActive);
                onComplete?.Invoke();
            }
            
            controller.PurchasePremium(OnComplete);
        }
        
        public int GetDepartmentLevel(MothershipDepartment department)
        {
            if (GetDepartment(department, out MothershipDepartmentController departmentController))
                return departmentController.CurrentLevel;
            return 0;
        }

        public int GetMaxAvailableLevel(MothershipDepartment department)
        {
            if (GetDepartment(department, out MothershipDepartmentController departmentController))
                return departmentController.AvailableLevel;
            return 0;
        }

        public double GetDepartmentLevelUpCost(MothershipDepartment department)
        {
            if (GetDepartment(department, out MothershipDepartmentController departmentController))
                return departmentController.NextUnlockCost;
            return 0;
        }

        public int GetRequiredShipLevel(MothershipDepartment department, int departmentLevel)
        {
            if (GetDepartment(department, out MothershipDepartmentController departmentController))
                return departmentController.GetRequiredMothershipLevel(departmentLevel);
            return 0;
        }

        public MothershipDepartmentState GetDepartmentState(MothershipDepartment department, out string displayText, out double nextUnlockCost)
        {
            if (GetDepartment(department, out MothershipDepartmentController departmentController))
                return departmentController.GetState(out displayText, out nextUnlockCost);
            
            displayText = "";
            nextUnlockCost = 0;
            return MothershipDepartmentState.Locked;
        }

        public bool GetDepartmentPremiumActive(MothershipDepartment department)
        {
            if (GetDepartment(department, out MothershipDepartmentController departmentController))
                return departmentController.PremiumActive;
            return false;
        }

        public int CurrentLevel 
        { 
            get => saveCurrentLevel.IsPopulated ? saveCurrentLevel.Value : 0;
            set => saveCurrentLevel.Value = value;
        }

        private int MaxAvailableLevel
        {
            get
            {
                int starFragments = StarFragmentManager.Instance.Fragments;
                for (int i = 0; i < levelThresholds.Length; i++)
                {
                    if (starFragments < levelThresholds[i])
                        return i - 1;
                }

                return levelThresholds.Length - 1;
            }
        }

        public void LevelUp()
        {
            if (MaxAvailableLevel <= CurrentLevel)
                return;
            CurrentLevel++;
            onSetMothershipLevel?.Invoke(CurrentLevel);
        }

        public void ResetAllDepartments(ResetType resetType)
        {
            if (resetType != ResetType.Prestige || GameVersionManager.CurrentVersion < GameVersion.Version3)
                return;
            for (int i = 0; i < departments.Length; i++)
            {
                departments[i].ResetDepartment();
                onSetMothershipDepartmentLevel?.Invoke(departments[i].Department, departments[i].CurrentLevel, departments[i].PremiumActive);
            }
        }
    }
}
