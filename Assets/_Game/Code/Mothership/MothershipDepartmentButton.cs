﻿using Core.Gameplay.Economy;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Mothership
{
    public class MothershipDepartmentButton : MonoBehaviour
    {
        [SerializeField]
        private MothershipDepartment department;
        
        [SerializeField]
        private CurrencyButton buildButton;

        [SerializeField]
        private GameObject mothershipUpgradeButton;

        [SerializeField]
        private UnityEvent onPurchaseSuccess;

        private bool initialised;

        private bool CanLevelUp => !MothershipManager.Instance.GetDepartmentAtMaxAvailableLevel(department);
        private bool MothershipMaxLevel => MothershipManager.Instance.IsMaxLevel;
        
        private void Start()
        {
            if (initialised)
                return;
            
            MothershipManager.Instance.onSetMothershipDepartmentLevel += UpdateDepartmentLevel;
            MothershipManager.Instance.onSetMothershipLevel += MothershipUpgrade; 
            
            buildButton.SetThreshold(MothershipManager.Instance.GetDepartmentLevelUpCost(department));
            CheckActive();
            initialised = true;
        }

        private void MothershipUpgrade(int level)
        {
            CheckActive();
        }

        private void CheckActive()
        {
            bool featureEnabled = GameVersionManager.CurrentVersion >= GameVersion.Version3;
            mothershipUpgradeButton.SetActive(featureEnabled && !CanLevelUp && !MothershipMaxLevel);
            buildButton.SetActive(featureEnabled && CanLevelUp);
        }
        

        private void UpdateDepartmentLevel(MothershipDepartment dept, int level, bool premium)
        {
            if (dept != department)
                return;
            buildButton.SetThreshold(MothershipManager.Instance.GetDepartmentLevelUpCost(department));
            CheckActive();
        }

        public void LevelUpDepartment()
        {
            void PurchaseComplete(bool success)
            {
                if (success)
                    onPurchaseSuccess?.Invoke();
            }

            MothershipManager.Instance.LevelUpDepartment(department, PurchaseComplete);
        }
    }
}