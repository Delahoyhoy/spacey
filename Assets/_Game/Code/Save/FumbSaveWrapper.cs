using System.Collections.Generic;
using Fumb.Save;

namespace Core.Gameplay.Save
{
    public class FumbSaveWrapper : SaveDataWrapper
    {
        private SaveValueDictionaryStringObject rawSaveData;

        public FumbSaveWrapper(SaveValueDictionaryStringObject data)
        {
            if (!data.IsPopulated)
                data.Value = new Dictionary<string, object>();
            rawSaveData = data;
        }

        public FumbSaveWrapper(SaveValueDictionaryStringObject data, ES2Data legacyData)
        {
            string[] allTags = legacyData.GetTags();
            if (!data.IsPopulated)
                data.Value = new Dictionary<string, object>();
            for (int i = 0; i < allTags.Length; i++)
            {
                object value = legacyData.loadedData[allTags[i]];
                data.Value.Add(allTags[i], value);
            }

            rawSaveData = data;
        }

        public override T LoadData<T>(string tag, T defaultValue)
        {
            if (!rawSaveData.IsPopulated)
                return defaultValue;
            if (!rawSaveData.Value.TryGetValue(tag, out object rawData))
                return defaultValue;
            if (rawData is T typedData)
                return typedData;
            return defaultValue;
        }

        public override bool HasTag(string tag)
        {
            return rawSaveData.IsPopulated && rawSaveData.Value.ContainsKey(tag);
        }

        public override void RemoveIds(List<string> idsToRemove)
        {
            for (int i = 0; i < idsToRemove.Count; i++)
            {
                if (rawSaveData.Value.ContainsKey(idsToRemove[i]))
                    rawSaveData.Value.Remove(idsToRemove[i]);
            }
        }
    }
}