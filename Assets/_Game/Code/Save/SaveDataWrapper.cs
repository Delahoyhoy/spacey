using System.Collections.Generic;

namespace Core.Gameplay.Save
{
    public abstract class SaveDataWrapper
    {
        public abstract T LoadData<T>(string tag, T defaultValue);
        public abstract bool HasTag(string tag);
        public abstract void RemoveIds(List<string> idsToRemove);
    }
}