using System.Collections.Generic;

namespace Core.Gameplay.Save
{
    public class ES2SaveDataWrapper : SaveDataWrapper
    {
        private ES2Data data;

        public ES2SaveDataWrapper(ES2Data d)
        {
            data = d;
        }

        public override bool HasTag(string tag)
        {
            return data.TagExists(tag);
        }

        public override void RemoveIds(List<string> idsToRemove)
        {
            // Do nothing
        }

        public override T LoadData<T>(string tag, T defaultValue)
        {
            if (data.TagExists(tag))
                return data.Load<T>(tag);
            return defaultValue;
        }
    }
}