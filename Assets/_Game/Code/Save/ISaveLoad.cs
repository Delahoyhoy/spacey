using System.Collections.Generic;

namespace Core.Gameplay.Save
{
    public interface ISaveLoad
    {
        void Save(ref Dictionary<string, object> saveData);
        void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove);
        void LoadDefault();
    }
}