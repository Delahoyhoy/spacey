﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Fumb.Save;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Gameplay.Save
{
    public class JC_SaveManager : MonoBehaviour
    {
        private string filename = "Space1/saveData.txt", backup = "Space1/backupData.txt";

        [ManagedSaveValue("SaveManagerRawSaveData")]
        private SaveValueDictionaryStringObject rawSaveData;

        public delegate void ReturnToGame(double seconds);

        public static event ReturnToGame OnReturnToGame;

        private DateTime timeLeft;

        private bool preventSave = false, gameStarted;

        List<ISaveLoad> GetAllSaveable()
        {
            return ObjectFinder.FindAllObjectsOfType<ISaveLoad>();
        }

        private GameManager IsolateGameManager(ref List<ISaveLoad> saveList)
        {
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i] is GameManager)
                {
                    GameManager manager = (GameManager) saveList[i];
                    saveList.RemoveAt(i);
                    return manager;
                }
            }

            return null;
        }

        public void RemoveSaveKey(string saveKey)
        {
            if (!rawSaveData.IsPopulated)
                return;
            if (rawSaveData.Value.ContainsKey(saveKey))
                rawSaveData.Value.Remove(saveKey);
        }

        public void Load()
        {
            SaveDataWrapper saveWrapper;

            if (rawSaveData.IsPopulated)
            {
                saveWrapper = new FumbSaveWrapper(rawSaveData);
                Load(saveWrapper);
            }
            else if (ES2.Exists(filename))
            {
                ES2Data data = ES2.LoadAll(filename);
                //ES2SaveDataWrapper dataWrapper = new ES2SaveDataWrapper(data);
                saveWrapper = new ES2SaveDataWrapper(data);
                Load(saveWrapper);
                ES2.Rename(filename, backup);
            }
            else
            {
                List<ISaveLoad> allSaveable = GetAllSaveable();
                GameManager gameManager = IsolateGameManager(ref allSaveable);
                if (gameManager)
                    gameManager.LoadDefault();
                for (int i = 0; i < allSaveable.Count; i++)
                {
                    allSaveable[i].LoadDefault();
                }
            }

            SaveManager.PreSaveAction += Save;
            gameStarted = true;
        }

        private void Load(SaveDataWrapper saveWrapper)
        {
            List<ISaveLoad> allSaveable = GetAllSaveable();
            List<string> idsToRemove = new List<string>();
            GameManager gameManager = IsolateGameManager(ref allSaveable);
            if (gameManager)
            {
                try
                {
                    gameManager.Load(saveWrapper, ref idsToRemove);
                }
                catch (Exception e)
                {
                    Debug.Log("Exception caught whilst loading: " + gameManager.GetType());
                    Debug.LogException(e);
                }
            }
            for (int i = 0; i < allSaveable.Count; i++)
            {
                try
                {
                    //Debug.Log(string.Format("Loading item {0} of type {1}", i, allSaveable[i].GetType()));
                    allSaveable[i].Load(saveWrapper, ref idsToRemove);
                }
                catch (Exception e)
                {
                    Debug.Log("Exception caught whilst loading: " + allSaveable[i].GetType());
                    Debug.LogException(e);
                }
            }

            if (saveWrapper.HasTag("timeLeft"))
            {
                timeLeft = GetDateTime(saveWrapper.LoadData("timeLeft", ""), UnbiasedTime.Instance.Now());
                TimeSpan timeSinceAway = UnbiasedTime.Instance.Now() - timeLeft;
                OnReturnToGame?.Invoke(timeSinceAway.TotalSeconds);
                
            }

            saveWrapper.RemoveIds(idsToRemove);
            gameManager.CheckLoadSuccess();
        }

        private void OnDestroy()
        {
            if (gameStarted)
                SaveManager.PreSaveAction -= Save;
        }

        public void Save()
        {
            if (preventSave)
                return;
            timeLeft = UnbiasedTime.Instance.Now();
            Debug.Log("Saving!");
            List<ISaveLoad> allSaveable = GetAllSaveable();
            Dictionary<string, object> saveData = new Dictionary<string, object>();
            for (int i = 0; i < allSaveable.Count; i++)
            {
                allSaveable[i].Save(ref saveData);
            }

            saveData.Add("timeLeft", SaveDateTime(UnbiasedTime.Instance.Now()));
            if (!rawSaveData.IsPopulated)
                rawSaveData.Value = new Dictionary<string, object>();
            foreach (string key in saveData.Keys)
            {
                object value = saveData[key];
                if (!rawSaveData.Value.ContainsKey(key))
                    rawSaveData.Value.Add(key, value);
                else
                    rawSaveData.Value[key] = value;
            }
        }

        #region Static Save Helpers

        public static string SaveVector3(Vector3 vector3)
        {
            return vector3.x.ToString(CultureInfo.InvariantCulture) + "," + 
                   vector3.y.ToString(CultureInfo.InvariantCulture) + "," + 
                   vector3.z.ToString(CultureInfo.InvariantCulture);
        }

        public static Vector3 LoadVector3(string data)
        {
            Vector3 result = Vector3.zero;
            string[] splitData = data.Split(',');
            if (splitData.Length > 0)
                float.TryParse(splitData[0], out result.x);
            if (splitData.Length > 1)
                float.TryParse(splitData[1], out result.y);
            if (splitData.Length > 2)
                float.TryParse(splitData[2], out result.z);
            return result;
        }

        public static string SaveDateTime(DateTime dateTime)
        {
            return dateTime.Year + ":" + dateTime.Month + ":" + dateTime.Day + ":" + dateTime.Hour + ":" +
                   dateTime.Minute + ":" + dateTime.Second;
        }

        public static DateTime GetDateTime(string dataTime, DateTime defaultTime)
        {
            if (string.IsNullOrEmpty(dataTime))
                return defaultTime;
            string[] splitData = dataTime.Split(':');
            if (splitData.Length == 6)
                return new DateTime(int.Parse(splitData[0]),
                    int.Parse(splitData[1]),
                    int.Parse(splitData[2]),
                    int.Parse(splitData[3]),
                    int.Parse(splitData[4]),
                    int.Parse(splitData[5]));
            Debug.Log("Failed to load time!");
            return defaultTime;
        }

        #endregion

        private void OnApplicationPause(bool pause)
        {
            if (pause || !gameStarted) 
                return;
            TimeSpan timeSinceAway = UnbiasedTime.Instance.Now() - timeLeft;
            OnReturnToGame?.Invoke(timeSinceAway.TotalSeconds);
        }
        
        private void Start()
        {
            Load();
        }
    }

}