using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Tutorial;
using Fumb.RemoteConfig;
using Fumb.General;
using Fumb.Save;
using UnityEngine;

public class AsteroidUnlockSats : MonoBehaviour
{
    [ManagedSaveValue("AsteroidUnlockSatsInterval")]
    private SaveValueInt saveCurrentInterval;

    private static AsteroidUnlockSats instance;
    public static AsteroidUnlockSats Instance => instance ??= SceneHelper.GetComponentInScene<AsteroidUnlockSats>();

    private int CurrentInterval
    {
        get => saveCurrentInterval.Value;
        set => saveCurrentInterval.Value = value;
    }

    [SerializeField]
    private RemoteInt asteroidUnlockInterval = new RemoteInt("asteroidSatInterval", 0);
    [SerializeField]
    private RemoteInt satsPerAsteroid = new RemoteInt("asteroidSats", 0);

    private bool Enabled => SatsEarningManager.Instance.CanEarnSats && asteroidUnlockInterval > 0 &&
                            satsPerAsteroid > 0;// && FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core);

    private void Awake()
    {
        instance = this;
    }

    public bool CheckShouldUnlock(out int count)
    {
        count = satsPerAsteroid;
        if (!Enabled)
            return false;
        CurrentInterval++;
        if (CurrentInterval % asteroidUnlockInterval != 0) 
            return false;
        CurrentInterval = 0;
        return true;
    }
}
