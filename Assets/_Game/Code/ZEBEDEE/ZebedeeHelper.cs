using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zebedee;
using Fumb.Analytics;
using ZBDFumb;

public class ZebedeeHelper : MonoBehaviour
{
    private void Start()
    {
        ZebedeeManager.Instance.OnSatsWithdrawn += OnSatsWithdrawn;
    }

    private void OnEnable()
    {
        AnalyticsManager.PreSendAnalyticsEventAction += OnPreSendAnalyticsEvent;
    }

    private void OnDestroy()
    {
        AnalyticsManager.PreSendAnalyticsEventAction -= OnPreSendAnalyticsEvent;
    }
    
    private void OnSatsWithdrawn(int withdrawn)
    {
        CurrencyController.Instance.CurrencyEvent(CurrencyType.Satoshis, -withdrawn, "cash_out", "full_withdraw");
        // AnalyticsHelper.CurrencySpend(CurrencyType.Satoshis, withdrawn, 
        //     true, "CashOut");
    }

    private void OnPreSendAnalyticsEvent(AnalyticsEvent analyticsEvent)
    {
        if (analyticsEvent.GetType() != typeof(AnalyticsEventSessionInfo)) {
            return;
        }

        string currentGamertag = ZBDFumbAuthLogin.GamerTag; 
        analyticsEvent.AddAdditionalParameters(new KeyValuePair<string, object>("zbd_gamertag", currentGamertag));
    }
}
