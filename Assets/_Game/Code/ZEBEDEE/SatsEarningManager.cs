using System;
using System.Collections;
using System.Collections.Generic;
using Fumb.RemoteConfig;
using UnityEngine.Events;
using UnityEngine;

public class SatsEarningManager : MonoBehaviour
{
    private static SatsEarningManager instance;
    public static SatsEarningManager Instance => instance ??= ObjectFinder.FindObjectOfType<SatsEarningManager>();
    
    [SerializeField]
    private RemoteBool canEarnSats = new RemoteBool("satsEnabled", true);

    [SerializeField]
    private RemoteString validVersionIds = new RemoteString("satsEarningVersions", "");

    private bool InternetConnected => Application.internetReachability != NetworkReachability.NotReachable;
    
    private bool PlatformValid => true;

    public bool SatsEarningEnabled => PlatformValid && canEarnSats;
    
    public bool CanEarnSats => SatsEarningEnabled && InternetConnected && IsVersionValid;

    private List<string> ValidVersions
    {
        get
        {
            if (string.IsNullOrEmpty(validVersionIds))
                return new List<string>{Application.version};
            return new List<string>(((string) validVersionIds).Split(','));
        }
    }

    private bool IsVersionValid => ValidVersions.Contains(Application.version);
    
    [SerializeField]
    private UnityEvent<bool> onSetCanEarnSats;

    private bool initialised = false;
    
    private void Start()
    {
        onSetCanEarnSats?.Invoke(SatsEarningEnabled);
        canEarnSats.RegisterListenForConfigChanges();
        canEarnSats.SubscribeToValueChangeDetection(OnValueChange);
        initialised = true;
    }

    private void OnDestroy()
    {
        if (!initialised)
            return;
        canEarnSats.UnsubscribeFromValueChangeDetection(OnValueChange);
    }

    private void OnValueChange()
    {
        onSetCanEarnSats?.Invoke(SatsEarningEnabled);
    }
}
