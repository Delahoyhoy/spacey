using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Outposts;
using Core.Gameplay.Reset;
using Core.Gameplay.Tutorial;
using Core.Gameplay.UserInterface;
using Fumb.RemoteConfig;
using UnityEngine;
using Random = UnityEngine.Random;

public class SatoshiSpawnManager : MonoBehaviour
{
    private float nextSpawnTime;
    private float minDelay = 60, maxDelay = 90;

    [SerializeField]
    private RemoteString satSpawnDelay = new RemoteString("satSpawnDelay", "45,90");

    [SerializeField]
    private RemoteInt satsPerTap = new RemoteInt("satsToEarn", 1);
    
    [SerializeField]
    private SatoshiCoin satPrefab;
    private Queue<SatoshiCoin> satPool = new Queue<SatoshiCoin>();

    private bool SatSpawningActive => SatsEarningManager.Instance.CanEarnSats && satsPerTap > 0 && 
                                      FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) && !ResetManager.Instance.resetting;

    [SerializeField]
    private Transform satParent;

    private SatoshiCoin GetNewSat()
    {
        if (satPool.Count <= 0)
            return Instantiate(satPrefab, satParent);
        return satPool.Dequeue();
    }

    private void Start()
    {
        nextSpawnTime = Time.time + GetNewDelay();
    }

    private float GetNewDelay()
    {
        string delayValues = satSpawnDelay; 
        if (delayValues.Contains(","))
        {
            string[] values = delayValues.Split(',');
            if (values.Length < 2)
                return 60;
            float.TryParse(values[0], out minDelay);
            float.TryParse(values[1], out maxDelay);
            return Random.Range(minDelay, maxDelay);
        }
        if (float.TryParse(satSpawnDelay, out float delay))
            return delay;
        return 60;
    }
    
    public void SpawnNewCoin()
    {
        if (!SatSpawningActive)
            return;
        List<Outpost> activeOutposts = OutpostManager.Instance.ActiveOutposts;
        int totalOutposts = activeOutposts.Count;//OutpostManager.Instance.GetOutpostCount();
        if (totalOutposts <= 0)
            return;
        SatoshiCoin newSatCoin = GetNewSat();

        void OnExpire()
        {
            satPool.Enqueue(newSatCoin);
        }
        
        NotificationDisplay.Instance.Display("SATOSHI APPEARED");

        Outpost selectedOutpost = activeOutposts[Random.Range(0, totalOutposts)];
        
        newSatCoin.Setup(satsPerTap, selectedOutpost.GetPath(), OnExpire);
    }

    private void Update()
    {
        if (Time.time < nextSpawnTime)
            return;
        nextSpawnTime = Time.time + GetNewDelay();
        SpawnNewCoin();
    }
}
