using System;
using Core.Gameplay.Audio;
using Core.Gameplay.Reset;
using UnityEngine;
using Random = UnityEngine.Random;

public class SatoshiCoin : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private BezierPath path;

    private int value;

    private Action endJourney;
    private bool subscribedToResetEvent;

    public void Tap()
    {
        var position = transform.position;
        Vector3 screenPos = Camera.main.WorldToScreenPoint(position);
        CoinBurster.Instance.NewBurst(CurrencyType.Satoshis, screenPos, value, value);
        EffectManager.Instance.EmitAt("sparkleBurst", position, Random.Range(10, 15));
        AudioManager.Play("Milestone", 0.5f); 
        ExpireCoin();
        AnalyticsHelper.CurrencyEarn(CurrencyType.Satoshis, value, true, "TapToken");
        CurrencyController.Instance.CurrencyEvent(CurrencyType.Satoshis, value, "token", "tap_satoshi");
    }

    public void Setup(int satValue, BezierPath bezierPath, Action onEndJourney)
    {
        gameObject.SetActive(true);
        value = satValue;
        path = bezierPath;
        path.TraversePath(0, out Vector3 pos);
        transform.position = pos;
        endJourney = onEndJourney;
        if (subscribedToResetEvent) 
            return;
        ResetManager.Instance.beginReset += HandleReset;
        subscribedToResetEvent = true;
    }

    private void ExpireCoin()
    {
        endJourney?.Invoke();
        gameObject.SetActive(false);
        if (!subscribedToResetEvent) 
            return;
        ResetManager.Instance.beginReset -= HandleReset;
        subscribedToResetEvent = false;
    }

    private void HandleReset(ResetType resetType)
    {
        ExpireCoin();
    }

    private void Update()
    {
        if (path.TraversePath(speed * Time.deltaTime, out Vector3 pos))
        {
            ExpireCoin();
            return;
        }
        transform.position = pos;
    }
}
