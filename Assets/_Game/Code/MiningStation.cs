﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Outposts;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.UI;

public class MiningStation : MonoBehaviour
{
    [SerializeField]
    private LineRenderer laser;

    [SerializeField]
    private GameObject miningStation;

    [SerializeField]
    private Transform laserHit;

    private int currentAsteroidTarget = -1;
    private Asteroid currentAsteroid;

    public int CurrentTarget { get { return currentAsteroidTarget; } }

    [SerializeField]
    private float maxWidth = 0.25f;

    [SerializeField]
    private Image borderFill;

    private float currentPercentage = 0.01f, countdownDelay = 0f;

    [SerializeField]
    private Button button;

    [SerializeField]
    private Image targetImage;

    [SerializeField]
    private Vector3 scale, hitMaxScale;

    private static MiningStation instance;
    public static MiningStation Instance { get { if (!instance) instance = ObjectFinder.FindObjectOfType<MiningStation>(); return instance; } }

    private double miningSpeed = 2d;
    private double maxEfficiency = 25d;
    private double multiplier = 1d;

    [SerializeField]
    private Text popoffPrefab;

    bool moving = false;

    [SerializeField]
    private Transform popoffParent;

    private Queue<Text> popoffPool = new Queue<Text>();

    public delegate void TargetAsteroid(int asteroid);
    public static event TargetAsteroid OnTargetAsteroid;

    private double mineMult, efficiencyMult;

    private bool newSystem = true;

    public void SetTarget(int target)
    {
        if (newSystem)
        {
            currentAsteroidTarget = target;
            miningStation.SetActive(false);
            button.interactable = OutpostManager.Instance.GetOutpostCount(true) > 0;
            return;
        }

        if (moving)
            return;
        if (target < 0)
        {
            currentAsteroidTarget = -1;
            miningStation.SetActive(false);
            button.interactable = false;
            targetImage.CrossFadeAlpha(0, 0, true);
            return;
        }
        if (target == currentAsteroidTarget)
            return;
        targetImage.CrossFadeAlpha(1, 0, true);
        Asteroid newTarget = GameManager.Instance.GetAsteroid(target);
        targetImage.sprite = newTarget.AsteroidSprite;
        targetImage.transform.localEulerAngles = newTarget.Rotation;
        targetImage.transform.localScale = Vector3.one * (newTarget.ScaleFactor / 1.5f);
        StartCoroutine(MoveToTarget(target));
        AudioManager.Play("Click", 0.75f);
    }

    public void UpdateOutpostCount(int count)
    {
        button.interactable = count > 0;
    }

    IEnumerator MoveToTarget(int newTarget)
    {
        moving = true;
        laser.gameObject.SetActive(false);
        laserHit.gameObject.SetActive(false);
        if (currentAsteroidTarget >= 0)
        {
            for (float f = 1f; f > 0; f -= Time.deltaTime * 2f)
            {
                miningStation.transform.localScale = scale * f;
                yield return null;
            }
        }
        else
        {
            button.interactable = true;
            miningStation.SetActive(true);
        }
        currentAsteroid = GameManager.Instance.GetAsteroid(newTarget);
        currentAsteroidTarget = newTarget;
        OnTargetAsteroid?.Invoke(newTarget);
        if (currentAsteroid != null)
        {
            miningStation.transform.parent = currentAsteroid.miningStationAnchor;
            miningStation.transform.localPosition = Vector3.zero;
        }
        for (float f = 0; f < 1f; f += Time.deltaTime * 2f)
        {
            miningStation.transform.localScale = scale * f;
            yield return null;
        }
        moving = false;
        miningStation.transform.localScale = scale;
        laser.gameObject.SetActive(true);
        laserHit.gameObject.SetActive(true);
    }

    public void SetMiningSpeed(double speed)
    {
        if (GameVersionManager.CurrentVersion == GameVersion.Version1)
            miningSpeed = speed;
        else
        {
            mineMult = 1d + (speed / 100d);
            miningSpeed = System.Math.Max((GameManager.Instance.GetTotalMinedPerSecond(true) / 100d) * mineMult, 2d);
        }
    }

    public void SetMaxEfficiency(double efficiency)
    {
        if (GameVersionManager.CurrentVersion == GameVersion.Version1)
            maxEfficiency = efficiency;
        else
        {
            efficiencyMult = 1d + (efficiency / 100d);
            maxEfficiency = 20d + efficiency;
        }
    }

    public delegate void TapMine(double efficiency, double amount, ref double mined);
    public static event TapMine OnTap;

    public delegate void SetupLaser(float percent);
    public static event SetupLaser OnLaserSetup;

    public void Tap()
    {
        if (tapDelay > 0)
            return;
        AudioManager.Play("Coin", 0.1f);
        //TutorialManager.OnTrigger("mineAsteroid");
        currentPercentage += 0.05f;
        currentPercentage = Mathf.Clamp01(currentPercentage);
        borderFill.fillAmount = currentPercentage;
        //EffectManager.Instance.EmitAt(currentAsteroid.RockID, laserHit.position, Random.Range(1, 3));
        button.transform.localScale = Vector3.one * 1.1f;
        double earnings = 0;// currentAsteroid.MineAmount(miningSpeed, currentPercentage * maxEfficiency) * GameManager.Instance.IncomeMultiplier * multiplier;
        if (OnTap != null)
            OnTap(currentPercentage * maxEfficiency, miningSpeed, ref earnings);
        earnings *= GameManager.Instance.IncomeMultiplier * multiplier;
        CurrencyController.Instance.AddCurrency(earnings, CurrencyType.SoftCurrency, false);
        SpawnReward(earnings);
        countdownDelay = 0.2f;
        AudioManager.Play("Click", 0.25f);
        tapDelay = 0.05f;
    }

    public void SetMultiplier(double newValue)
    {
        multiplier = newValue;
        //Debug.Log("NEW MULTIPLIER = " + newValue);
    }

    void SpawnReward(double amtToAdd)
    {
        Text newPopoff = NewPopoff();
        //newPopoff.transform.localPosition = Vector3.zero;
        newPopoff.text = CurrencyController.FormatToString(amtToAdd);
        StartCoroutine(DelayedEnqueue(newPopoff));
    }

    public Text NewPopoff()
    {
        if (popoffPool.Count > 0)
        {
            Text newPopoff = popoffPool.Dequeue();
            newPopoff.gameObject.SetActive(true);
            newPopoff.transform.SetAsLastSibling();
            return newPopoff;
        }
        else
        {
            return Instantiate(popoffPrefab, popoffParent);
        }
    }

    IEnumerator DelayedEnqueue(Text popoff)
    {
        yield return new WaitForSeconds(1f);
        popoff.gameObject.SetActive(false);
        popoffPool.Enqueue(popoff);
    }

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //laserHit.transform.localPosition = Vector3.up * .9f;
        //laser.SetPositions(new Vector3[] { Vector3.zero, new Vector3(0.625f, -0.625f, 0), new Vector3(1.25f, -1.25f, 0) });
        //SetTarget(-1);
    }

    private float tapDelay = 0f;
    private float checkInterval = 5f;

    // Update is called once per frame
    private void Update()
    {
        if (tapDelay > 0)
            tapDelay -= Time.deltaTime;
        button.transform.localScale = Vector3.Lerp(button.transform.localScale, Vector3.one, Time.deltaTime * 2f);
        if (GameVersionManager.CurrentVersion != GameVersion.Version1)
        {
            checkInterval -= Time.deltaTime;
            if (checkInterval <= 0)
            {
                checkInterval += 5;
                miningSpeed = System.Math.Max((GameManager.Instance.GetTotalMinedPerSecond(true) / 100d) * mineMult, 2d);
            }
        }
        if (OnLaserSetup != null)
            OnLaserSetup(currentPercentage);
        if (currentPercentage > 0)
        {
            if (countdownDelay > 0)
                countdownDelay -= Time.deltaTime;
            if (countdownDelay <= 0)
            {
                currentPercentage -= Time.deltaTime / 4f;
                currentPercentage = Mathf.Clamp01(currentPercentage);
            }
            borderFill.fillAmount = currentPercentage;
            //laser.widthMultiplier = maxWidth * currentPercentage * (1f + (Mathf.Sin(Time.time * 20f) * 0.125f));
            //laserHit.transform.localScale = hitMaxScale * currentPercentage;
            //laserHit.transform.Rotate(0, 0, Time.deltaTime * 360f);
        }
    }
}
