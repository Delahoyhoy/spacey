﻿using System.Collections.Generic;
using Fumb.Save;

namespace Core.Gameplay.Research
{
    public class SaveValueLabDictionary : SaveValueDictionary<int, ResearchLabData> { }
}