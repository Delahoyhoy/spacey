﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Economy;
using Core.Gameplay.Save;
using Core.Gameplay.Sectors;
using Core.Gameplay.Tutorial;
using Core.Gameplay.UserInterface;
using Core.Gameplay.Version;
using Core.Gameplay.VIP;
using Fumb.General;
using Fumb.Save;
using Fumb.RemoteConfig;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Research
{
   public class ResearchManager : MonoBehaviour, ISaveLoad
    {
        [SerializeField]
        private Text currentResearchTitle,
            currentResearchDesc,
            countDownTimer,
            buttonCountdown,
            hurryCooldownText,
            speedResearchCountdown,
            gemCost;

        [SerializeField]
        private UIScreen researchScreen, hurryScreen;

        public delegate void SetResearchLevel(ResearchType type, double magnitude, int level);

        public static event SetResearchLevel OnResearchUpgrade;
        public static event SetResearchLevel OnResearchLoaded;

        [SerializeField]
        private GameObject hurryButton, vipEarns, getMoreGemsButton;

        [SerializeField]
        private Button instantButton, adWatchButton;

        [SerializeField]
        private ResearchItem[] researchItems, newSystemResearchItems;

        [SerializeField]
        private ResearchVersionHandler versionHandler;

        private ResearchItem[] ResearchItems => versionHandler.ResearchItems;
        
        private ModifierEffect sectorResearchMult = new ModifierEffect(SectorModifier.ResearchSpeed);

        private static ResearchManager instance;

        public static ResearchManager Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<ResearchManager>();
                return instance;
            }
        }

        [SerializeField]
        private ResearchSlot slotPrefab;

        [SerializeField]
        private Transform slotParent;

        private List<ResearchSlot> researchSlots = new List<ResearchSlot>();

        [SerializeField]
        private GameObject notificationIcon;

        [ManagedSaveValue("ResearchManagerCurrentResearch")]
        private SaveValueInt currentlyResearching;
        private DateTime researchEnd, hurryCooldownEnd;
        private bool hurryOnCooldown;

        private RemoteDouble hurryCostPerMin = new RemoteDouble("researchHurryCost", 1.5d);

        [ManagedSaveValue("ResearchManagerIsResearching")]
        private SaveValueBool isResearching;

        private bool IsResearching => saveLabsResearching.Value.Values.Any(researching => researching.IsResearching);

        [ManagedSaveValue("ResearchManagerSaveResearchLevels")]
        private SaveValueListInt saveResearchLevels;

        [ManagedSaveValue("ResearchManagerLabsResearching")]
        private SaveValueLabDictionary saveLabsResearching;

        [ManagedSaveValue("ResearchManagerAvailableLabs")]
        private SaveValueInt saveAvailableLabs;

        [ManagedSaveValue("ResearchManagerPremiumLabsUnlocked")]
        private SaveValueBool savePremiumLabsUnlocked;
        
        private int availableLabs = 1;

        private ResearchQueueDisplaySlot CurrentSlot => labs[0];

        [SerializeField]
        private ResearchQueueDisplaySlot[] labs, premiumLabs, queueSlots;

        private List<int> availableLabIndices = new List<int>();

        private List<ResearchQueueDisplaySlot> Labs
        {
            get
            {
                var allLabs = new List<ResearchQueueDisplaySlot>(labs);
                allLabs.AddRange(premiumLabs);
                return allLabs;
            }
        }

        [SerializeField]
        private Image progressBar;

        [SerializeField]
        private int maxQueueLength = 6;

        private Queue<int> researchQueue = new Queue<int>();

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            if (!isResearching.IsPopulated)
                isResearching.Value = loadedData.LoadData("w0research", false);
            researchEnd =
                JC_SaveManager.GetDateTime(loadedData.LoadData("w0researchEnd", ""), UnbiasedTime.Instance.Now());
            if (GameVersionManager.CurrentVersion == GameVersion.Version3)
                SetupUnlockedLabs(saveAvailableLabs.Value, savePremiumLabsUnlocked.Value);
            else
                SetupUnlockedLabs(1, false);
            if (!saveLabsResearching.IsPopulated)
            {
                saveLabsResearching.Value = new Dictionary<int, ResearchLabData>();
                if (currentlyResearching.IsPopulated)
                    saveLabsResearching.Value.Add(0, isResearching ? new ResearchLabData(currentlyResearching.Value, researchEnd) : 
                        new ResearchLabData(true));
                else
                {
                    
                    saveLabsResearching.Value.Add(0, isResearching ? new ResearchLabData(loadedData.LoadData("w0researchIndex", 0), researchEnd) : 
                        new ResearchLabData(true));
                    idsToRemove.Add("w0researchIndex");
                }
            }
            GameManager.Instance.SetUseOldSensors(loadedData.LoadData("useOldSensors", true));
            if (!saveResearchLevels.IsPopulated)
            {
                //saveResearchLevels.Value = new List<int>();
                for (int i = 0; i < ResearchItems.Length; i++)
                {
                    ResearchItems[i].currentLevel = loadedData.LoadData("w0research" + i, 0);
                    idsToRemove.Add("w0research" + i);
                    OnResearchLoaded?.Invoke(ResearchItems[i].researchType, GetMagnitude(i),
                        ResearchItems[i].currentLevel);
                }
            }
            else
            {
                for (int i = 0; i < ResearchItems.Length; i++)
                {
                    ResearchItems[i].currentLevel = saveResearchLevels.Value.Count <= i ? 0 : saveResearchLevels.Value[i];
                    OnResearchLoaded?.Invoke(ResearchItems[i].researchType, GetMagnitude(i),
                        ResearchItems[i].currentLevel);
                }
            }

            string queueData = loadedData.LoadData("w0researchQueue", "");
            if (!string.IsNullOrEmpty(queueData))
            {
                string[] splitQueue = queueData.Split(',');
                for (int i = 0; i < splitQueue.Length; i++)
                {
                    int researchIndex = 0;
                    if (int.TryParse(splitQueue[i], out researchIndex))
                        researchQueue.Enqueue(researchIndex);
                }
            }

            SetupResearchQueue();
            if (IsResearching)
            {
                currentResearchDesc.text = GetDescription(currentlyResearching);
                currentResearchTitle.text = ResearchItems[currentlyResearching].displayName;
                speedResearchCountdown.text = countDownTimer.text =
                    buttonCountdown.text = FormatTimeWithColon(researchEnd - UnbiasedTime.Instance.Now());
                hurryButton.SetActive(true);
                SetupSlots();
                CheckResearchProgress();
            }
            else
            {
                currentResearchTitle.text = "Select an option above to begin research";
                currentResearchDesc.text = "";
                speedResearchCountdown.text = countDownTimer.text = "";
                buttonCountdown.text = "RESEARCH";
                SetupSlots();
            }
            
            PopulateEmptyLabs();

            hurryOnCooldown = loadedData.LoadData("hurryCooldown", false);
            if (hurryOnCooldown)
            {
                hurryCooldownEnd =
                    JC_SaveManager.GetDateTime(loadedData.LoadData("hurryEnd", ""), UnbiasedTime.Instance.Now());
                hurryCooldownText.text = FormatTimeWithColon(hurryCooldownEnd - UnbiasedTime.Instance.Now());
                vipEarns.SetActive(false);
                GameManager.OnSecondUpdate += CheckHurryCooldown;
            }
            else
            {
                hurryCooldownText.text = "";
                vipEarns.SetActive(true);
            }
        }

        private void PopulateEmptyLabs()
        {
            int[] currentResearch = GetCurrentResearch(false);
            while (researchQueue.Count > 0 && GetAnyLabsFree(out int freeLab))
            {
                int research = researchQueue.Dequeue();
                ResearchLabData data = new ResearchLabData(research, UnbiasedTime.Instance.Now().AddSeconds(
                    GetTimeForResearch(research, ResearchItems[research].currentLevel + 1 + currentResearch[research])));
                SetLabResearching(freeLab, data);
                currentResearch[research]++;
            }
        }

        public double GetTimeForResearch(int researchItem, int level)
        {
            return GetTimeAtLevel(researchItem, level) / sectorResearchMult.Value;
        }

        private int GetPremiumLabIndex(int lab)
        {
            return labs.Length + lab;
        }

        public void SetLabsUnlocked(int unlocked, bool premiumUnlocked)
        {
            saveAvailableLabs.Value = unlocked;
            savePremiumLabsUnlocked.Value = premiumUnlocked;
            SetupUnlockedLabs(unlocked, premiumUnlocked);
            
            PopulateEmptyLabs();
            SetupResearchQueue();
            CheckResearchProgress();
        }

        public void EnsureLabsUnlocked(int labs)
        {
            if (saveAvailableLabs.Value < labs)
                SetLabsUnlocked(labs, savePremiumLabsUnlocked.Value);
        }

        private void SetupUnlockedLabs(int unlocked, bool premiumUnlocked = false)
        {
            availableLabIndices.Clear();
            availableLabs = unlocked;
            int index = 0;
            for (int i = 0; i < labs.Length; i++, index++)
            {
                bool labUnlocked = unlocked > i; 
                labs[i].SetLocked(!labUnlocked);
                if (labUnlocked)
                    availableLabIndices.Add(index);
            }

            for (int i = 0; i < premiumLabs.Length; i++, index++)
            {
                premiumLabs[i].SetLocked(!premiumUnlocked);
                if (premiumUnlocked)
                    availableLabIndices.Add(index);
            }
            
        }

        private void Awake()
        {
            instance = this;
        }

        public void ResetResearch()
        {
            researchQueue.Clear();
            isResearching.Value = false;
            researchEnd = UnbiasedTime.Instance.Now();

            saveLabsResearching.Value = new Dictionary<int, ResearchLabData>();
            
            for (int i = 0; i < ResearchItems.Length; i++)
            {
                ResearchItems[i].currentLevel = 0;
                OnResearchLoaded?.Invoke(ResearchItems[i].researchType, ResearchItems[i].GetMagnitude(),
                    ResearchItems[i].currentLevel);
            }

            currentResearchTitle.text = "Select an option above to begin research";
            currentResearchDesc.text = "";
            speedResearchCountdown.text = countDownTimer.text = "";
            buttonCountdown.text = "RESEARCH";
            SetupResearchQueue();
            hurryButton.SetActive(false);
            versionHandler.OnFirstLoad();
            ClearSlots();
            SetupSlots();
        }

        private void SetupResearchQueue()
        {
            int[] researchingAlready = new int[ResearchItems.Length];
            int[] queueItems = researchQueue.ToArray();

            List<ResearchQueueDisplaySlot> allLabs = Labs;
            List<int> labIndices = SortAvailableIndices();
            
            for (int i = 0; i < labIndices.Count; i++)
            {
                int index = labIndices[i];
                bool hasResearch = HasResearch(index, out int researchIndex, out DateTime end);
                if (!hasResearch)
                {
                    allLabs[index].SetupEmpty();
                    continue;
                }

                TimeSpan remainingTime = end - UnbiasedTime.Instance.Now();
                
                ResearchItem researchItem = ResearchItems[researchIndex];
                int researchingLevel = researchItem.currentLevel + researchingAlready[researchIndex] + 1;
                allLabs[index].SetupFromResearch(researchItem.displaySprite, researchItem.iconColour, researchingLevel, 
                    FormatTimeWithColon(remainingTime));
                researchingAlready[researchIndex]++;
            }
            
            // if (!isResearching.Value)
            // {
            //     CurrentSlot.SetupEmpty();
            //     progressBar.fillAmount = 0;
            // }
            // else
            // {
            //     ResearchItem researchItem = ResearchItems[currentlyResearching];
            //     double maxTime = GetTime(currentlyResearching);
            //     float progress = 1f - ((float) ((researchEnd - UnbiasedTime.Instance.Now()).TotalSeconds / maxTime));
            //     CurrentSlot.SetupFromResearch(researchItem.displaySprite, researchItem.iconColour,
            //         researchItem.currentLevel + 1, FormatTimeWithColon(researchEnd - UnbiasedTime.Instance.Now()));
            //     progressBar.fillAmount = progress;
            //     researchingAlready[currentlyResearching]++;
            // }

            for (int i = 0; i < queueSlots.Length; i++)
            {
                if (i >= queueItems.Length)
                    queueSlots[i].SetupEmpty();
                else
                {
                    ResearchItem researchItem = ResearchItems[queueItems[i]];
                    int researchingLevel = researchItem.currentLevel + researchingAlready[queueItems[i]] + 1;
                    queueSlots[i].SetupFromResearch(researchItem.displaySprite, researchItem.iconColour,
                        researchingLevel, FormatTime(GetTimeForResearch(queueItems[i], researchingLevel)));
                    researchingAlready[queueItems[i]]++;
                }
            }
        }

        private bool HasResearch(int labIndex, out int researchIndex, out DateTime end)
        {
            end = UnbiasedTime.Instance.Now();
            researchIndex = 0;
            if (!saveLabsResearching.IsPopulated)
                return false;
            if (!saveLabsResearching.Value.ContainsKey(labIndex))
                return false;
            ResearchLabData labData = saveLabsResearching.Value[labIndex];
            researchIndex = labData.ResearchIndex;
            end = labData.ResearchEnd;
            return labData.IsResearching;
        }

        public double GetMagnitude(int index)
        {
            return GetMagnitudeAtLevel(index, ResearchItems[index].currentLevel);
        }

        public double GetNextMagnitude(int index)
        {
            return GetMagnitudeAtLevel(index, ResearchItems[index].currentLevel + 1);
        }

        public double GetCostToComplete(double seconds)
        {
            return Math.Ceiling((seconds / 60d) * hurryCostPerMin);
        }

        private void HurryResearch()
        {
            if (hurryOnCooldown)
                return;
            hurryOnCooldown = true;
            hurryCooldownEnd = UnbiasedTime.Instance.Now().AddMinutes(5);
            hurryCooldownText.text = FormatTimeWithColon(hurryCooldownEnd - UnbiasedTime.Instance.Now());
            vipEarns.SetActive(false);
            GameManager.OnSecondUpdate += CheckHurryCooldown;
            //researchEnd = researchEnd.AddMinutes(-15);
            HurryBy(900);
            hurryScreen.Close();
            //CheckResearchProgress();
        }

        private void HandleAdResultIfSucceeded()
        {
            VIPManager.Instance.AddVIPPoints(20);

            HurryResearch();
        }

        public void Hurry()
        {
            if (AdHub.Ready && !hurryOnCooldown)
                AdHub.ShowAd("HurryResearch", HandleAdResultIfSucceeded);
        }

        public void OpenScreen()
        {
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
            {
                CompleteResearch();
                return;
            }

            hurryScreen.Open();
            adWatchButton.interactable = AdHub.Ready && !hurryOnCooldown;
            double cost = GetCostToComplete((researchEnd - UnbiasedTime.Instance.Now()).TotalSeconds);
            gemCost.text = CurrencyController.FormatToString(cost);
            bool canAfford = CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) >= cost;
            instantButton.interactable = canAfford;
            getMoreGemsButton.SetActive(!canAfford);
        }

        public void InstantComplete()
        {
            double cost = GetCostToComplete((researchEnd - UnbiasedTime.Instance.Now()).TotalSeconds);
            if (CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) >= cost)
            {
                CurrencyController.Instance.AddCurrency(-cost, CurrencyType.HardCurrency, false, "quick_research", "hurry_research");
                researchEnd = UnbiasedTime.Instance.Now();
                hurryScreen.Close();
                CheckResearchProgress();
            }
        }

        public double GetMagnitudeAtLevel(int index, int level)
        {
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
            {
                ResearchType researchType = ResearchItems[index].researchType;
                switch (researchType)
                {
                    case ResearchType.CargoSpeed:
                        return level * 1.5d;
                    case ResearchType.CargoCap:
                        return level * 2d;
                    case ResearchType.TapSpeed:
                        return level * 2.5d;
                    case ResearchType.TapEfficiency:
                        return level * 2.5d;
                    case ResearchType.SensorRange:
                        return level;
                    default:
                        return ResearchItems[index].GetMagnitudeAtLevel(level);
                }
            }
            else
            {
                ResearchType researchType = ResearchItems[index].researchType;
                switch (researchType)
                {
                    case ResearchType.CargoSpeed:
                        if (level == 0)
                            return 1d;
                        return Math.Round((1 + (Math.Pow(level, 0.8d) / 2d)) * 100d) / 100d;
                    case ResearchType.CargoCap:
                        if (level == 0)
                            return 50d;
                        return Math.Round(Math.Exp((level + 1) / 2d)) * 52.5d;
                    case ResearchType.TapSpeed:
                        if (level == 0)
                            return 2;
                        //Debug.Log("Got here with " + System.Math.Round(2 + ((System.Math.Exp((level + 1) / 2d)) / 2d)));
                        return Math.Round(1 + (GetMagnitudeAtLevel(index, level - 1) *
                                               1.2475d)); //System.Math.Round(2 + (System.Math.Exp((level + 1) / 2.33333d) / 2d));
                    case ResearchType.TapEfficiency:
                        if (level == 0)
                            return 15;
                        return 20 + (level * 1.5d);
                    default:
                        return ResearchItems[index].GetMagnitudeAtLevel(level);
                }
            }
        }

        private bool CanAffordUpgrade()
        {
            int[] researchingAlready = GetCurrentResearch();
            for (int i = 0; i < ResearchItems.Length; i++)
            {
                if (!ResearchItems[i].MaxLevel && CurrencyController.Instance.CurrentGold >=
                    GetCostAtLevel(i, ResearchItems[i].currentLevel + 1 + researchingAlready[i]))
                    return true;
            }

            return false;
        }

        public double GetCost(int index)
        {
            return GetCostAtLevel(index, ResearchItems[index].currentLevel + 1);
        }

        public double GetCostAtLevel(int index, int level)
        {
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
            {
                ResearchType researchType = ResearchItems[index].researchType;
                switch (researchType)
                {
                    case ResearchType.CargoSpeed:
                        if (level <= 1)
                            return 500d;
                        return Math.Round((Math.Exp((level + 1) / 4d) *
                                           (500d * (Math.Pow(level - 1, 1.01d) / 2.5d))) / 50d) * 50d;
                    case ResearchType.CargoCap:
                        if (level <= 1)
                            return 750d;
                        return Math.Round((Math.Exp((level + 1) / 3d) *
                                           (750d * (Math.Pow(level - 1, 1.01d) / 1.5d))) / 50d) * 50d;
                    case ResearchType.TapSpeed:
                        if (level <= 1)
                            return 450d;
                        return Math.Round((Math.Exp((level + 1) / 3.5d) *
                                           (450d * (Math.Pow(level, 1.025d) / 1.5d))) / 50d) * 50d;
                    case ResearchType.TapEfficiency:
                        if (level <= 1)
                            return 800;
                        return Math.Round((Math.Exp((level + 1) / 4d) *
                                           (800d * (Math.Pow(level - 1, 1.01d) / 1.625d))) / 50d) * 50d;
                    case ResearchType.SensorRange:
                        if (level <= 1)
                            return 25000;
                        return Math.Round((GetCostAtLevel(index, level - 1) * (4 * (level + 1))) / 5000d) *
                               5000d;
                    default:
                        return ResearchItems[index].GetCostAtLevel(level);
                }
            }
            else
            {
                ResearchType researchType = ResearchItems[index].researchType;
                switch (researchType)
                {
                    case ResearchType.CargoSpeed:
                        if (level <= 1)
                            return 500d;
                        return Math.Round((Math.Exp((level + 1) / 2d) *
                                           (500d * (Math.Pow(level - 1, 1.05d) / 2d))) / 50d) * 50d;
                    case ResearchType.CargoCap:
                        if (level <= 1)
                            return 750d;
                        return Math.Round((Math.Exp((level + 1) / 1.8d) *
                                           (750d * (Math.Pow(level - 1, 1.05d) / 1.5d))) / 50d) * 50d;
                    case ResearchType.TapSpeed:
                        if (level <= 1)
                            return 450d;
                        return Math.Round((Math.Exp((level + 1) / 2.25d) *
                                           (450d * (Math.Pow(level, 1.075d) / 1.5d))) / 50d) * 50d;
                    case ResearchType.TapEfficiency:
                        if (level <= 1)
                            return 800;
                        return Math.Round((Math.Exp((level + 1) / 2.5d) *
                                           (800d * (Math.Pow(level - 1, 1.05d) / 1.625d))) / 50d) * 50d;
                    case ResearchType.SensorRange:
                        if (level <= 1)
                            return 25000;
                        return Math.Round((GetCostAtLevel(index, level - 1) * (4 * (level + 1))) / 5000d) *
                               5000d;
                    default:
                        return ResearchItems[index].GetCostAtLevel(level);
                }
            }
        }

        public double GetTime(int index)
        {
            return GetTimeForResearch(index, ResearchItems[index].currentLevel + 1);
        }

        public double GetTimeAtLevel(int index, int level)
        {
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
            {
                // Setup for new system
                ResearchType researchType = ResearchItems[index].researchType;
                switch (researchType)
                {
                    case ResearchType.CargoSpeed:
                        if (level <= 1)
                            return 120d;
                        double speedTime = 2 + (0.2 * Math.Pow(level, 1.25));
                        speedTime = RoundTime(speedTime);
                        return speedTime * 60d;
                    case ResearchType.CargoCap:
                        if (level <= 1)
                            return 90d;
                        double capTime = 1.5 + (0.15 * Math.Pow(level, 1.25));
                        capTime = RoundTime(capTime);
                        return capTime * 60d;
                    case ResearchType.TapSpeed:
                        if (level <= 1)
                            return 75d;
                        double tapTime = 1.25 + (0.125 * Math.Pow(level, 1.25));
                        tapTime = RoundTime(tapTime);
                        return tapTime * 60d;
                    case ResearchType.TapEfficiency:
                        if (level <= 1)
                            return 120d;
                        double effTime = 2 + (0.25 * Math.Pow(level, 1.25));
                        effTime = RoundTime(effTime);
                        return effTime * 60d;
                    case ResearchType.SensorRange:
                        if (level <= 1)
                            return 1200d;
                        double sensTime = 10 + (10 / 1.125) * Math.Pow(level, 1.25);
                        sensTime = RoundTime(sensTime);
                        return sensTime * 60d;
                    default:
                        return ResearchItems[index].GetTimeAtLevel(level);
                }
            }
            else
            {
                ResearchType researchType = ResearchItems[index].researchType;
                switch (researchType)
                {
                    case ResearchType.CargoSpeed:
                        if (level <= 1)
                            return 120d;
                        double speedTime = 2 + (0.2 * Math.Pow(level, 1.25));
                        speedTime = RoundTime(speedTime);
                        return speedTime * 60d;
                    case ResearchType.CargoCap:
                        if (level <= 1)
                            return 90d;
                        double capTime = 1.5 + (0.15 * Math.Pow(level, 1.25));
                        capTime = RoundTime(capTime);
                        return capTime * 60d;
                    case ResearchType.TapSpeed:
                        if (level <= 1)
                            return 75d;
                        double tapTime = 1.25 + (0.125 * Math.Pow(level, 1.25));
                        tapTime = RoundTime(tapTime);
                        return tapTime * 60d;
                    case ResearchType.TapEfficiency:
                        if (level <= 1)
                            return 120d;
                        double effTime = 2 + (0.25 * Math.Pow(level, 1.25));
                        effTime = RoundTime(effTime);
                        return effTime * 60d;
                    case ResearchType.SensorRange:
                        if (level <= 1)
                            return 1200d;
                        double sensTime = 20 + ((20 / 1.125) * Math.Pow(level, 1.25));
                        sensTime = RoundTime(sensTime);
                        return sensTime * 60d;
                    default:
                        return ResearchItems[index].GetTimeAtLevel(level);
                }
            }
        }

        public string GetShortDescriptionAtLevel(int index, int level)
        {
            ResearchItem researchItem = ResearchItems[index];
            if (researchItem.researchType != ResearchType.SensorRange) 
                return string.Format(researchItem.shortFormat, GetMagnitudeAtLevel(index, level));
            
            int currentAsteroids = AsteroidManager.Instance.AsteroidCount;
            int totalAsteroids = currentAsteroids;
            int currentLevel = researchItem.currentLevel;
            for (int i = currentLevel + 1; i <= level; i++)
            {
                totalAsteroids += GameManager.Instance.GetNewAsteroidsAtLevel(i);
            }

            return string.Format(researchItem.shortFormat, totalAsteroids);
        }

        private double RoundTime(double time)
        {
            if (time > 60)
                return Math.Round(time / 10) * 10;
            else if (time > 10)
                return Math.Round(time * 2) / 2;
            else
                return Math.Round(time * 4) / 4;
        }

        public string GetDescription(int index)
        {
            return GetDescriptionAtLevel(index, ResearchItems[index].currentLevel + 1);
        }

        public string GetDescriptionAtLevel(int index, int level)
        {
            return ResearchItems[index].GetCustomDescription(GetMagnitudeAtLevel(index, level));
        }

        public void SwapLastTwoSlots()
        {
            //ResearchItem temp = ResearchItems[ResearchItems.Length - 1];
            //ResearchItems[ResearchItems.Length - 1] = ResearchItems[ResearchItems.Length - 2];
            //ResearchItems[ResearchItems.Length - 2] = temp;
        }

        public static string FormatTimeWithColon(TimeSpan timeSpan)
        {
            if (timeSpan.TotalHours >= 1)
                return string.Format("{0}:{1}:{2}", Math.Floor(timeSpan.TotalHours).ToString("#00"),
                    timeSpan.Minutes.ToString("00"), timeSpan.Seconds.ToString("00"));
            return string.Format("{0}:{1}", timeSpan.Minutes.ToString("00"), timeSpan.Seconds.ToString("00"));
        }

        public void CompleteResearch()
        {
            researchEnd = UnbiasedTime.Instance.Now();
            CheckResearchProgress();
        }

        public ResearchItem GetResearch(int index)
        {
            return ResearchItems[index];
        }

        private void ClearSlots()
        {
            for (int i = 0; i < researchSlots.Count; i++)
            {
                Destroy(researchSlots[i].gameObject);
            }

            researchSlots.Clear();
        }

        private void SetupSlots()
        {
            int[] researchingAlready = GetCurrentResearch();
            for (int i = 0; i < ResearchItems.Length; i++)
            {
                ResearchSlot slot = Instantiate(slotPrefab, slotParent);
                slot.SetupFromResearch(i, this, researchingAlready, isResearching.Value && QueueFull);
                researchSlots.Add(slot);
            }
        }

        public void LoadDefault()
        {
            isResearching.Value = false;
            researchEnd = UnbiasedTime.Instance.Now();
            currentlyResearching.Value = 0;

            saveLabsResearching.Value = new Dictionary<int, ResearchLabData>();
            
            if (GameVersionManager.CurrentVersion == GameVersion.Version3)
                SetupUnlockedLabs(saveAvailableLabs.Value, savePremiumLabsUnlocked.Value);
            else
                SetupUnlockedLabs(1, false);
            
            for (int i = 0; i < ResearchItems.Length; i++)
                ResearchItems[i].currentLevel = 0;

            currentResearchTitle.text = "Select an option above to begin research";
            currentResearchDesc.text = "";
            speedResearchCountdown.text = countDownTimer.text = "";
            buttonCountdown.text = "RESEARCH";
            hurryOnCooldown = false;
            hurryCooldownEnd = UnbiasedTime.Instance.Now();
            hurryCooldownText.text = "";
            vipEarns.SetActive(true);
            versionHandler.OnFirstLoad();
            SetupResearchQueue();
            SetupSlots();
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            //saveData.Add("w0research", isResearching);
            saveData.Add("w0researchEnd", JC_SaveManager.SaveDateTime(researchEnd));
            //saveData.Add("w0researchIndex", currentlyResearching);
            if (!saveResearchLevels.IsPopulated)
                saveResearchLevels.Value = new List<int>();
            for (int i = 0; i < ResearchItems.Length; i++)
            {
                //saveData.Add("w0research" + i, ResearchItems[i].currentLevel);
                if (i >= saveResearchLevels.Value.Count)
                    saveResearchLevels.Value.Add(ResearchItems[i].currentLevel);
                else
                    saveResearchLevels.Value[i] = ResearchItems[i].currentLevel;
            }

            if (researchQueue.Count <= 0)
                saveData.Add("w0researchQueue", "");
            else
            {
                string queueData = "";
                int[] queueItems = researchQueue.ToArray();
                for (int i = 0; i < queueItems.Length; i++)
                {
                    if (i > 0)
                        queueData += ",";
                    queueData += queueItems[i].ToString();
                }

                saveData.Add("w0researchQueue", queueData);
            }

            saveData.Add("hurryCooldown", hurryOnCooldown);
            saveData.Add("hurryEnd", JC_SaveManager.SaveDateTime(hurryCooldownEnd));
        }

        private void HurryBy(double seconds)
        {
            if (!isResearching.Value)
                return;
            if (UnbiasedTime.Instance.Now().AddSeconds(seconds) >= researchEnd)
            {
                ResearchItems[currentlyResearching].currentLevel++;
                if (OnResearchUpgrade != null)
                    OnResearchUpgrade(ResearchItems[currentlyResearching].researchType,
                        GetMagnitude(currentlyResearching), ResearchItems[currentlyResearching].currentLevel);
                NotificationDisplay.Instance.Display("RESEARCH COMPLETE!");
                if (researchQueue.Count > 0)
                {
                    double remainingSeconds = seconds - (researchEnd - UnbiasedTime.Instance.Now()).TotalSeconds;
                    do
                    {
                        if (researchQueue.Count <= 0)
                        {
                            isResearching.Value = false;
                            currentResearchTitle.text = "Select an option above to begin research";
                            currentResearchDesc.text = "";
                            speedResearchCountdown.text = countDownTimer.text = "";
                            buttonCountdown.text = "RESEARCH";
                            hurryButton.SetActive(false);
                            break;
                        }

                        int current = researchQueue.Dequeue();
                        double timeForResearch = GetTime(current);
                        if (timeForResearch <= remainingSeconds)
                        {
                            ResearchItems[current].currentLevel++;
                            OnResearchUpgrade?.Invoke(ResearchItems[current].researchType,
                                GetMagnitude(currentlyResearching), ResearchItems[current].currentLevel);
                            remainingSeconds -= timeForResearch;
                        }
                        else
                        {
                            BeginResearch(current, remainingSeconds);
                            remainingSeconds = 0;
                            break;
                        }
                    } while (remainingSeconds > 0);
                }
                else
                {
                    isResearching.Value = false;
                    currentResearchTitle.text = "Select an option above to begin research";
                    currentResearchDesc.text = "";
                    speedResearchCountdown.text = countDownTimer.text = "";
                    buttonCountdown.text = "RESEARCH";
                    hurryButton.SetActive(false);
                }
            }
            else
            {
                researchEnd = researchEnd.AddSeconds(-seconds);
                CheckResearchProgress();
            }

            SetupResearchQueue();
            RefreshSlots();
        }

        private void CheckResearchProgress()
        {
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                CheckHasBeenUpgraded();
            if (researchScreen.IsOpen)
            {
                RefreshSlots();
                SetupResearchQueue();
            }

            if (!IsResearching)
            {
                notificationIcon.SetActive(CanAffordUpgrade());
                return;
            }

            notificationIcon.SetActive(false);
            TimeSpan longestRemainingTime;
            
            List<ResearchQueueDisplaySlot> allLabs = Labs;
            List<int> labIndices = SortAvailableIndices();
            int totalCompleted = 0;
            
            for (int i = 0; i < labIndices.Count; i++)
            {
                int index = labIndices[i];
                if (!HasResearch(index, out int researchIndex, out DateTime end))
                    continue;

                TimeSpan remainingTime = end - UnbiasedTime.Instance.Now();
                if (UnbiasedTime.Instance.Now() > end)
                {
                    totalCompleted++;
                    ResearchItems[researchIndex].currentLevel++;
                    NotificationDisplay.Instance.Display("RESEARCH COMPLETE!");
                    //TutorialManager.OnTrigger("waitHurry");
                    //TutorialManager.OnTrigger("hurry");
                    OnResearchUpgrade?.Invoke(ResearchItems[researchIndex].researchType,
                        GetMagnitude(researchIndex), ResearchItems[researchIndex].currentLevel);
                    saveLabsResearching.Value[index] = new ResearchLabData(true);
                    if (researchQueue.Count > 0)
                    {
                        int[] currentResearch = GetCurrentResearch(false);
                        double remainingSeconds = (UnbiasedTime.Instance.Now() - end).TotalSeconds;
                        do
                        {
                            if (researchQueue.Count <= 0)
                            {
                                isResearching.Value = false;
                                currentResearchTitle.text = "Select an option above to begin research";
                                currentResearchDesc.text = "";
                                speedResearchCountdown.text = countDownTimer.text = "";
                                buttonCountdown.text = "RESEARCH";
                                hurryButton.SetActive(false);
                                break;
                            }

                            int current = researchQueue.Dequeue();
                            double timeForResearch = GetTimeForResearch(current, ResearchItems[current].currentLevel + 1 + 
                                                                             currentResearch[current]);
                            if (timeForResearch <= remainingSeconds)
                            {
                                ResearchItems[current].currentLevel++;
                                OnResearchUpgrade?.Invoke(ResearchItems[current].researchType,
                                    GetMagnitude(currentlyResearching), ResearchItems[current].currentLevel);
                                remainingSeconds -= timeForResearch;
                            }
                            else
                            {
                                //BeginResearch(current, remainingSeconds);
                                ResearchLabData labData = new ResearchLabData(current,
                                    UnbiasedTime.Instance.Now().AddSeconds(timeForResearch - remainingSeconds));
                                SetLabResearching(index, labData);
                                remainingSeconds = 0;
                                break;
                            }
                        } while (remainingSeconds > 0);
                    }
                    else
                    {
                        SetLabEmpty(index);
                        currentResearchTitle.text = "Select an option above to begin research";
                        currentResearchDesc.text = "";
                        speedResearchCountdown.text = countDownTimer.text = "";
                        buttonCountdown.text = "RESEARCH";
                        hurryButton.SetActive(false);
                    }
                }
                else
                {
                    allLabs[index].UpdateTimer(FormatTimeWithColon(remainingTime));
                    // speedResearchCountdown.text = countDownTimer.text =
                    //     buttonCountdown.text = FormatTimeWithColon(remainingTime);
                }
            }
            if (totalCompleted > 0)
            {
                SetupResearchQueue();
                SetTimer.StartTimer(0.25f, () => IncrementStatTracker(totalCompleted));
            }
        }

        private List<int> SortAvailableIndices()
        {
            var sorted = availableLabIndices.OrderBy(index =>
            {
                if (!HasResearch(index, out int researchIndex, out DateTime end))
                    return 0;
                return (end - UnbiasedTime.Instance.Now()).TotalSeconds;
            });
            return sorted.ToList();
        }

        private void IncrementStatTracker(int count)
        {
            StatTracker.Instance.IncrementStat("research", count);
        }

        private void SetLabEmpty(int labIndex)
        {
            Labs[labIndex].SetupEmpty();
            if (!saveLabsResearching.IsPopulated)
                saveLabsResearching.Value = new Dictionary<int, ResearchLabData>();
            if (!saveLabsResearching.Value.ContainsKey(labIndex))
                saveLabsResearching.Value.Add(labIndex, new ResearchLabData(true));
            else
                saveLabsResearching.Value[labIndex] = new ResearchLabData(true);
        }

        private void SetLabResearching(int labIndex, ResearchLabData data)
        {
            int researchIndex = data.ResearchIndex;

            Sprite displayIcon = ResearchItems[researchIndex].displaySprite;
            Color iconColour = ResearchItems[researchIndex].iconColour;

            string remainingTime = FormatTimeWithColon(data.ResearchEnd - UnbiasedTime.Instance.Now());
            
            Labs[labIndex].SetupFromResearch(displayIcon, iconColour, 
                researchItems[researchIndex].currentLevel + 1, remainingTime);
            if (!saveLabsResearching.IsPopulated)
                saveLabsResearching.Value = new Dictionary<int, ResearchLabData>();
            saveLabsResearching.Value[labIndex] = data;
        }

        private bool QueueFull => researchQueue.Count >= maxQueueLength;

        private int[] GetCurrentResearch(bool includeQueue = true)
        {
            int[] researchingAlready = new int[ResearchItems.Length];
            int[] queueItems = researchQueue.ToArray();
            // if (isResearching.Value)
            //     researchingAlready[currentlyResearching]++;
            List<int> labIndices = SortAvailableIndices();
            for (int i = 0; i < labIndices.Count; i++)
            {
                int index = labIndices[i];
                if (!HasResearch(index, out int researchIndex, out DateTime end))
                    continue;
                researchingAlready[researchIndex]++;
            }

            if (!includeQueue)
                return researchingAlready;

            for (int i = 0; i < queueItems.Length; i++)
            {
                researchingAlready[queueItems[i]]++;
            }
            return researchingAlready;
        }

        public void StartResearch(int index)
        {
            AudioManager.Play("Confirm", 1f);
            bool anyLabsFree = GetAnyLabsFree(out int labIndex);
            if (!anyLabsFree && QueueFull)
                return;
            int[] researchingAlready = GetCurrentResearch();
            if (!ResearchItems[index].MaxLevel)
            {
                double cost = GetCostAtLevel(index, ResearchItems[index].currentLevel + 1 + researchingAlready[index]);
                if (CurrencyController.Instance.SpendCurrency(cost, CurrencyType.SoftCurrency, "research", 
                        $"research_{ResearchItems[index].researchType}"))
                {
                    if (anyLabsFree)
                    {
                        DateTime end = UnbiasedTime.Instance.Now()
                            .AddSeconds(GetTimeForResearch(index, ResearchItems[index].currentLevel + 
                                                              1 + researchingAlready[index]));
                        ResearchLabData labData = new ResearchLabData(index, end);
                        SetLabResearching(labIndex, labData);
                    }
                    else
                    {
                        researchQueue.Enqueue(index);
                    }

                    SetupResearchQueue();
                    RefreshSlots();
                }
            }
        }

        private bool GetAnyLabsFree(out int firstFreeLab)
        {
            firstFreeLab = 0;
            for (int i = 0; i < availableLabIndices.Count; i++)
            {
                int labIndex = availableLabIndices[i];
                if (!HasResearch(labIndex, out int researchIndex, out DateTime end))
                {
                    firstFreeLab = labIndex;
                    return true;
                }
            }

            return false;
        }

        public void BeginResearch(int index, double overflowTime = 0)
        {
            isResearching.Value = true;
            researchEnd = UnbiasedTime.Instance.Now().AddSeconds(GetTime(index) - overflowTime);
            currentlyResearching.Value = index;

            currentResearchDesc.text = GetDescription(currentlyResearching);
            currentResearchTitle.text = ResearchItems[currentlyResearching].displayName;
            speedResearchCountdown.text = countDownTimer.text =
                buttonCountdown.text = FormatTimeWithColon(researchEnd - UnbiasedTime.Instance.Now());
            //TutorialManager.OnTrigger("researchSpeed");
            RefreshSlots();
            SetupResearchQueue();
            hurryButton.SetActive(true);
        }

        public void CheckHasBeenUpgraded()
        {
            for (int i = 0; i < ResearchItems.Length; i++)
            {
                //Debug.Log(ResearchItems[i].researchType + " " + ResearchItems[i].currentLevel);
                if (ResearchItems[i].currentLevel > 0)
                {
                    //TutorialManager.OnTrigger("waitHurry");
                    //TutorialManager.OnTrigger("hurry");
                }
            }
        }

        public void RefreshSlots()
        {
            int[] researchingAlready = GetCurrentResearch();
            bool anySlotsFree = GetAnyLabsFree(out int labIndex);
            for (int i = 0; i < researchSlots.Count; i++)
            {
                //if (FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                researchSlots[i].gameObject.SetActive(true);
                // else
                //     researchSlots[i].gameObject.SetActive(ResearchItems[i].researchType == ResearchType.TapSpeed);
                researchSlots[i].Refresh(researchingAlready, !anySlotsFree && QueueFull);
            }
        }

        public void HighlightLaserSpeed()
        {
            for (int i = 0; i < researchSlots.Count; i++)
            {
                //if (ResearchItems[i].researchType == ResearchType.TapSpeed)
                    //TutorialManager.Instance.DoCustomHighlight(researchSlots[i].PurchaseButtonTransform, 0.125f, HighlightType.Screen);
            }
        }

        public static string FormatTime(double seconds)
        {
            int hours = (int) seconds / 3600;
            int mins = ((int) seconds % 3600) / 60;
            int secs = ((int) seconds % 3600) % 60;
            TimeSpan timeSpan = new TimeSpan(hours, mins, secs);
            return FormatTimeWithColon(timeSpan); //FormatHMS(timeSpan);
        }

        public static string FormatHMS(TimeSpan timeSpan)
        {
            if (timeSpan.TotalHours >= 1)
                return string.Format("{0}h {1}m {2}s", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
            if (timeSpan.TotalMinutes >= 1)
                return string.Format("{0}m {1}s", timeSpan.Minutes, timeSpan.Seconds);
            return string.Format("{0}s", timeSpan.Seconds);
        }

        private void CheckHurryCooldown()
        {
            if (UnbiasedTime.Instance.Now() >= hurryCooldownEnd)
            {
                hurryOnCooldown = false;
                hurryCooldownText.text = "";
                vipEarns.SetActive(true);
                adWatchButton.interactable = AdHub.Ready && !hurryOnCooldown;
                GameManager.OnSecondUpdate -= CheckHurryCooldown;
            }
            else
            {
                hurryCooldownText.text = FormatTimeWithColon(hurryCooldownEnd - UnbiasedTime.Instance.Now());
                vipEarns.SetActive(false);
            }
        }

        private void OnEnable()
        {
            GameManager.OnSecondUpdate += CheckResearchProgress;
        }

        private void OnDisable()
        {
            GameManager.OnSecondUpdate -= CheckResearchProgress;
        }

        private void OnDestroy()
        {
            GameManager.OnSecondUpdate -= CheckResearchProgress;
        }
    }

}