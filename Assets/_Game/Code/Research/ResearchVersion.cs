using System;
using System.Collections.Generic;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Research
{
    [Serializable]
    public struct ResearchVersion
    {
        public List<GameVersion> validVersions;
        
        [SerializeField]
        private ResearchItem[] researchItems;
        public ResearchItem[] ResearchItems => researchItems;

        [SerializeField]
        private UnityEvent onFirstLoad;

        public bool GetValid(GameVersion version, out ResearchItem[] items)
        {
            items = researchItems;
            return validVersions != null && validVersions.Contains(version);
        }

        public void OnFirstLoad()
        {
            onFirstLoad?.Invoke();
        }
    }
}