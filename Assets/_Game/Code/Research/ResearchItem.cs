using System;
using Core.Gameplay.Economy;
using Core.Gameplay.Sectors;
using UnityEngine;
using Core.Gameplay.Version;

namespace Core.Gameplay.Research
{
    [Serializable]
    public class ResearchItem
    {
        public ResearchType researchType;
        public ResultTimeCost[] tiers;
        public string displayName, desc;
        public bool formatDescription;

        public GameVersion[] availableVersions;

        public string shortFormat;

        public Sprite displaySprite;

        public int currentLevel, maxLevel;

        public Color iconColour = Color.white;

        public double GetMagnitudeAtLevel(int level)
        {
            if (level >= tiers.Length)
                return 0;
            else
                return tiers[level].value;
        }

        public bool GetValidInVersion(GameVersion version)
        {
            if (availableVersions.Length <= 0)
                return true;
            for (int i = 0; i < availableVersions.Length; i++)
            {
                if (version.Equals(availableVersions[i]))
                    return true;
            }

            return false;
        }

        public string GetShortFormatAtLevel(int level)
        {
            return string.Format(shortFormat, FormatWithDecimal(GetMagnitudeAtLevel(level)));
        }

        public double GetMagnitude()
        {
            return GetMagnitudeAtLevel(currentLevel);
        }

        public string GetDescription()
        {
            return GetDescriptionAtTier(currentLevel + 1);
        }

        private string FormatWithDecimal(double value)
        {
            return value < 1000 ? value.ToString("##0.##") : CurrencyController.FormatToString(value);
        }

        public double GetNextMagnitude()
        {
            return GetMagnitudeAtLevel(currentLevel + 1);
        }

        public double GetCostAtLevel(int level)
        {
            if (level >= tiers.Length)
                return 0;
            else
                return tiers[level].cost;
        }

        public double GetCost()
        {
            return GetCostAtLevel(currentLevel + 1);
        }

        public double GetTimeAtLevel(int level)
        {
            if (level >= tiers.Length)
                return 0;
            else
                return tiers[level].seconds;
        }

        public double GetTime()
        {
            return GetTimeAtLevel(currentLevel + 1);
        }

        public string GetCustomDescription(double magnitude)
        {
            return formatDescription ? string.Format(desc, FormatWithDecimal(magnitude)) : desc;
        }

        public bool MaxLevel
        {
            get { return maxLevel > 0 ? currentLevel >= maxLevel : (currentLevel >= tiers.Length - 1); }
        }

        public bool GetMaxLevel(int level)
        {
            return maxLevel > 0 ? level >= maxLevel : (level >= tiers.Length - 1);
        }

        public string GetDescriptionAtTier(int tier)
        {
            if (tier >= tiers.Length)
                return formatDescription ? string.Format(desc, FormatWithDecimal(tiers[tiers.Length - 1].value)) : desc;
            return formatDescription ? string.Format(desc, FormatWithDecimal(tiers[tier].value)) : desc;
        }
    }
}