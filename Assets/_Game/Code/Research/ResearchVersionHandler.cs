using System;
using Core.Gameplay.Version;
using UnityEngine;

namespace Core.Gameplay.Research
{
    [Serializable]
    public class ResearchVersionHandler
    {
        [SerializeField]
        private ResearchVersion[] versions;
        private bool initialised;
        
        private ResearchVersion selectedVersion;

        public ResearchVersion SelectedVersion
        {
            get
            {
                if (initialised)
                    return selectedVersion;
                GameVersion version = GameVersionManager.CurrentVersion;
                for (int i = 0; i < versions.Length; i++)
                {
                    if (!versions[i].GetValid(version, out ResearchItem[] items))
                        continue;
                    selectedVersion = versions[i];
                    initialised = true;
                    return selectedVersion;
                }

                selectedVersion = versions[versions.Length - 1];
                initialised = true;
                return selectedVersion;
            }
        }

        public ResearchItem[] ResearchItems => SelectedVersion.ResearchItems;

        public void OnFirstLoad()
        {
            initialised = false;
            SelectedVersion.OnFirstLoad();
        }
    }
}