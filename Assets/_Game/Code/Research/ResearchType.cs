namespace Core.Gameplay.Research
{
    public enum ResearchType
    {
        CargoCap,
        CargoSpeed,
        SensorRange,
        TapSpeed,
        TapEfficiency,
    }
}