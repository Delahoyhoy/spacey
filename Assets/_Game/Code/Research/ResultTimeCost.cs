using System;

namespace Core.Gameplay.Research
{
    [Serializable]
    public struct ResultTimeCost
    {
        public double cost, value, seconds;
    }
}