﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Research
{
    public class ResearchSlot : MonoBehaviour
    {
        [SerializeField]
        private Text researchTitle, researchDescription, costText, timeText, levelText;

        [SerializeField]
        private Image displayImage;

        [SerializeField]
        private Button researchButton;

        [SerializeField]
        private GameObject nextLevelValueHolder;

        [SerializeField]
        private Text currentValue, nextValue;

        [SerializeField]
        private Color affordablePriceColour = new Color(0.1960784f, 0.1960784f, 0.1960784f, 1f);

        private ResearchManager researchManager;

        public Transform PurchaseButtonTransform => researchButton.transform;

        private ResearchType researchType;
        public ResearchType ResearchType => researchType;
        
        private int index;

        public void SetupFromResearch(int targetIndex, ResearchManager managerRef, int[] researchingAlready,
            bool slotsFull = false)
        {
            index = targetIndex;
            researchManager = managerRef;
            ResearchItem research = researchManager.GetResearch(index);

            researchType = research.researchType;
            researchTitle.text = research.displayName;
            displayImage.sprite = research.displaySprite;
            displayImage.color = research.iconColour;
            
            Refresh(researchingAlready, slotsFull);
        }

        public void Refresh(int[] researchingAlready, bool slotsFull = false)
        {
            ResearchItem research = researchManager.GetResearch(index);
            
            int researchLevel = research.currentLevel + 1 + researchingAlready[index];
            bool alreadyMax = research.GetMaxLevel(research.currentLevel + researchingAlready[index]);
            researchDescription.text =
                researchManager.GetDescriptionAtLevel(index, researchLevel);
            double cost = researchManager.GetCostAtLevel(index, researchLevel);
            costText.text = alreadyMax ? "MAX" : CurrencyController.FormatToString(cost);
            timeText.text = alreadyMax ? "" : ResearchManager.FormatTime(researchManager.GetTimeForResearch(index, researchLevel));
            researchButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost &&
                                          !slotsFull && !alreadyMax;
            costText.color = alreadyMax || CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost ?
                affordablePriceColour : Color.red;
            levelText.text = "LEVEL " + researchLevel;

            nextLevelValueHolder.SetActive(!alreadyMax);
            if (!alreadyMax)
                nextValue.text = researchManager.GetShortDescriptionAtLevel(index, researchLevel);
            currentValue.text = researchManager.GetShortDescriptionAtLevel(index, research.currentLevel);
        }

        public void Research()
        {
            researchManager.StartResearch(index);
        }
    }

}