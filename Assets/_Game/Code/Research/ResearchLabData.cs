﻿using System;
using Fumb.Serialization;
using UnityEngine;

namespace Core.Gameplay.Research
{
    [Serializable]
    public class ResearchLabData
    {
        [SerializeField]
        private bool isResearching;

        public bool IsResearching => isResearching;
        
        [SerializeField]
        private int researchIndex;

        public int ResearchIndex => researchIndex;

        [SerializeField]
        private SerializableDateTime researchEnd;

        public DateTime ResearchEnd
        {
            get => researchEnd.DateTime;
            set => researchEnd = new SerializableDateTime(value);
        }

        public ResearchLabData(int index, DateTime end)
        {
            isResearching = true;
            researchIndex = index;
            ResearchEnd = end;
        }

        public ResearchLabData(bool empty)
        {
            isResearching = false;
            researchIndex = 0;
            ResearchEnd = UnbiasedTime.Instance.Now();
        }
    }
}