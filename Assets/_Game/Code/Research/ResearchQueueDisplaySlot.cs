﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Research
{
    public class ResearchQueueDisplaySlot : MonoBehaviour
    {
        [SerializeField]
        private GameObject emptyCover, locked;

        [SerializeField]
        private Image icon, levelHolder;
        
        [SerializeField]
        private Text toLevel, timeText;

        [SerializeField]
        private string levelTextFormat = "LEVEL {0}";

        public void SetupFromResearch(Sprite iconSprite, Color spriteColour, int level, string time)
        {
            emptyCover.SetActive(false);
            icon.enabled = true;
            icon.sprite = iconSprite;
            icon.color = spriteColour;
            levelHolder.enabled = true;
            toLevel.enabled = true;
            toLevel.text = string.Format(levelTextFormat, level);
            timeText.enabled = true;
            timeText.text = time;
        }

        public void SetLocked(bool isLocked)
        {
            locked.SetActive(isLocked);
            if (!locked)
                return;
            emptyCover.SetActive(false);
            icon.enabled = false;
            levelHolder.enabled = false;
            toLevel.enabled = false;
            timeText.enabled = false;
        }

        public void SetupEmpty()
        {
            emptyCover.SetActive(true);
            icon.enabled = false;
            levelHolder.enabled = false;
            toLevel.enabled = false;
            timeText.enabled = false;
        }

        public void UpdateTimer(string remainingTimeText)
        {
            timeText.text = remainingTimeText;
        }
    }
}