using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Research
{
    [Serializable]
    public struct ResearchQueueSlot
    {
        public GameObject emptyCover;
        public Image icon;
        public Text toLevel, timeText;

        public void SetupFromResearch(Sprite iconSprite, Color spriteColour, int level, string time)
        {
            emptyCover.SetActive(false);
            icon.enabled = true;
            icon.sprite = iconSprite;
            icon.color = spriteColour;
            toLevel.enabled = true;
            toLevel.text = "LEVEL " + level.ToString();
            timeText.enabled = true;
            timeText.text = time;
            
        }

        public void SetupEmpty()
        {
            emptyCover.SetActive(true);
            icon.enabled = false;
            toLevel.enabled = false;
            timeText.enabled = false;
        }
    }
}