namespace Core.Gameplay.Cheats
{
    public enum CodeError
    {
        NoError,
        NoCode,
        InvalidCode,
        CodeUsed
    }
}