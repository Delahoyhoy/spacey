﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Save;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Core.Gameplay.Cheats
{
    public class CheatCodes : MonoBehaviour, ISaveLoad
    {
        [SerializeField]
        private CheatCode[] cheatCodes;

        private Dictionary<string, CheatCode> codeDictionary = new Dictionary<string, CheatCode>();

        [SerializeField]
        private InputField codeInput;

        [SerializeField]
        private Button submitButton;

        private List<string> usedCodes = new List<string>();

        private string currentCode = "";

        [SerializeField]
        private UIScreen cheatPopup, errorPopup;

        [SerializeField]
        private Text errorText;

        public void UpdateInput(string code)
        {
            currentCode = code;
            submitButton.interactable = !string.IsNullOrEmpty(code);
        }

        public void OpenCheatMenu()
        {
            cheatPopup.Open();
            codeInput.text = "";
            codeInput.Select();
        }

        private string SaveUsedCodes()
        {
            string data = "";
            for (int i = 0; i < usedCodes.Count; i++)
            {
                if (i > 0)
                    data += ",";
                data += usedCodes[i];
            }

            return data;
            //SaveManager.SetString("cheatCodes", data);
        }

        private void LoadUsedCodes(string data)
        {
            //string data = SaveManager.GetString("cheatCodes", "");
            if (string.IsNullOrEmpty(data))
                return;
            string[] splitData = data.Split(',');
            usedCodes = new List<string>(splitData);
        }

        public void Submit()
        {
            CodeError validation = ValidateCode(currentCode);
            switch (validation)
            {
                case CodeError.CodeUsed:
                    errorText.text = "Code has already been used!";
                    errorPopup.Open();
                    break;
                case CodeError.NoCode:
                    errorText.text = "Please enter a code and try again";
                    errorPopup.Open();
                    break;
                case CodeError.InvalidCode:
                    errorText.text = "Invalid code!\nCheck your code and try again.";
                    errorPopup.Open();
                    break;
                case CodeError.NoError:
                    codeDictionary[currentCode].Trigger();
                    if (codeDictionary.ContainsKey(currentCode))
                        if (!codeDictionary[currentCode].reuseable)
                            usedCodes.Add(currentCode);
                    codeInput.text = "";
                    cheatPopup.Close();
                    SaveUsedCodes();
                    break;
            }

            codeInput.text = "";

        }

        private CodeError ValidateCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                return CodeError.NoCode;
            if (usedCodes.Contains(code))
                return CodeError.CodeUsed;
            if (!codeDictionary.ContainsKey(code))
                return CodeError.InvalidCode;
            return CodeError.NoError;
        }

        // Use this for initialization
        private void Awake()
        {
            //LoadUsedCodes();
            for (int i = 0; i < cheatCodes.Length; i++)
            {
                if (!codeDictionary.ContainsKey(cheatCodes[i].code))
                    codeDictionary.Add(cheatCodes[i].code, cheatCodes[i]);
            }
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("usedCheats", SaveUsedCodes());
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            LoadUsedCodes(loadedData.LoadData("usedCheats", ""));
        }

        public void LoadDefault()
        {
            usedCodes = new List<string>();
        }
    }

}