using System;
using UnityEngine.Events;

namespace Core.Gameplay.Cheats
{
    [Serializable]
    public struct CheatCode
    {
        public string code;
        public bool reuseable;
        public UnityEvent onTrigger;

        public void Trigger()
        {
            onTrigger?.Invoke();
        }
    }
}