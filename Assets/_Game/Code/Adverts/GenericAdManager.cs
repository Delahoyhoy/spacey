﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
//using GoogleMobileAds.Api;
using System;

public enum AdState
{
    NotReady,
    Ready,
    Started,
    Complete,
    WaitingOnResult,
}
public enum AdResult
{
    Fail,
    Skip,
    Success,
}

public class GenericAdManager : MonoBehaviour {

    private AdState adState;

    private UnityAction<AdResult> resultCallback;

    public bool AdReady { get { return adState == AdState.Ready; } }

    public static bool Ready { get { if (Instance) return Instance.AdReady; return false; } }

    public static GenericAdManager Instance { get { if (!instance) instance = FindObjectOfType<GenericAdManager>(); return instance; } }
    private static GenericAdManager instance;

    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
    //private RewardedAd rewardedAd;
    //private RewardBasedVideoAd rewardBasedVideo;
    
    // Use this for initialization
    void Start () {
        //OSCAR ADMOB
#if UNITY_ANDROID
        string appId = "ca-app-pub-2787517188133075~7584915162";
#elif UNITY_IPHONE
        string appId = "ca-app-pub-2787517188133075~2662867100";
#else
        string appId = "unexpected_platform";
#endif

        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        //MobileAds.Initialize(appId);

        StartCoroutine(WaitSecondsThenLoadAd());
       

        LoadAd();
    }

    IEnumerator WaitAction(UnityAction<AdResult> action, AdResult result, int framesToWait = 1)
    {
        int waitCount = 0;

        while(waitCount < framesToWait){
            waitCount++;
            yield return null;
        }
        action.Invoke(result);
    }

    IEnumerator WaitSecondsThenLoadAd(){
        yield return new WaitForSecondsRealtime(3.0f);
        CreateAndLoadRewardedAd();
    }

    public void CreateAndLoadRewardedAd(){
        //OSCAR ADMOB

#if UNITY_ANDROID
        string addUnitID = "ca-app-pub-2787517188133075/7789268603";
#elif UNITY_IPHONE
        string addUnitID = "ca-app-pub-2787517188133075/1321865277";
#else
        string addUnitID = "unexpected_platform";
#endif

        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        /*
        this.rewardedAd = new RewardedAd(addUnitID);
        

        rewardedAd.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardedAd.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardedAd.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        //rewardedAd.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardedAd.OnUserEarnedReward += HandleRewardBasedVideoRewarded;

        // Called when the ad is closed.
        rewardedAd.OnAdClosed += HandleRewardBasedVideoClosed;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);
        */
    }


    private void Awake()
    {
        instance = this;


    }

    private void LoadAd()
    {
        /*Debug.Log(GoogleMobileAd.RewardedVideoUnitId);
        GoogleMobileAd.LoadRewardedVideo();
        */
        /*
#if UNITY_ANDROID
            string adUnitId = "ca-app-pub-2787517188133075/7262918672";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-2787517188133075/1321865277";
#else
            string adUnitId = "unexpected_platform";
#endif

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the rewarded video ad with the request.
            this.rewardBasedVideo.LoadAd(request, adUnitId);
        */
    }

    public void ShowAd(UnityAction<AdResult> callback)
    {
        resultCallback = callback;
        if (adState == AdState.Ready)
        {
            hasRunVideoClosedLogic = false;
            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            //rewardedAd.Show();
        }
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        StartCoroutine(IEnumHandleRewardBasedVideoLoaded());   
    }

    IEnumerator IEnumHandleRewardBasedVideoLoaded(){
        yield return 0;
        adState = AdState.Ready;
    }

    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
    /*
    public void HandleRewardBasedVideoFailedToLoad(object sender, AdErrorEventArgs args)
    {
        StartCoroutine(IEnumHandleRewardBasedVideoFailedToLoad());
    }*/

    IEnumerator IEnumHandleRewardBasedVideoFailedToLoad(){
        yield return 0;
        Invoke("LoadAd", 6f); // Retry in 6 seconds
        StartCoroutine(WaitSecondsThenLoadAd());
        adState = AdState.NotReady;
    }


    bool hasRunVideoClosedLogic = false;

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        StartCoroutine(IEnumHandleRewardBasedVideoOpened());
    }

    IEnumerator IEnumHandleRewardBasedVideoOpened(){
        yield return 0;
        adState = AdState.Started;
        hasRunVideoClosedLogic = false;
        Debug.Log("Rewarded vid opened");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        StartCoroutine(IEnumHandleRewardBasedVideoStarted());
    }

    IEnumerator IEnumHandleRewardBasedVideoStarted()
    {
        yield return 0;
        // Mute audio/pause game/whatever here
        hasRunVideoClosedLogic = false;
        Debug.Log("Rewarded vid opened");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        //Debug.Log("111111RewardBasedVidClosed");
        if (resultCallback != null)
        {
            StartCoroutine(IEnumHandleRewardBasedVideoClosed());
        }
        //Debug.Log("111111Pre-loading next ad");

        CreateAndLoadRewardedAd();
        //Debug.Log("111111Post-loading next ad");
        //LoadAd(); // Once ad has been closed, load a new one
    }

    IEnumerator IEnumHandleRewardBasedVideoClosed()
    {
        yield return 0;
        //Debug.Log("111111RewardedCallback was NOT null and adState was: " + adState);
        switch (adState) // Switch case for if you want to handle the other states differently :)
        {
            case AdState.Complete:
                StatTracker.Instance.IncrementStat("adsWatched", 1); // Remove this, it's for my own stat tracker
                                                                     //Debug.Log("111111here");
                int totalWatched = (int)StatTracker.Instance.GetStat("adsWatched");
                //Debug.Log("222222");
                //if (totalWatched > 0 && totalWatched % 5 == 0)
                //{
                //    DontDestroyOnLoadSaveLoad.Instance.TenjinCustomEvent("WatchedAds");
                //    Debug.Log("33333");
                //}
                StartCoroutine(WaitAction(resultCallback, AdResult.Success,3)); //Apparently Unity ads success calls through mediated ads take more frames than other callbacks?
                //Debug.Log("4444");
                break;
            default:
                StartCoroutine(WaitAction(resultCallback, AdResult.Skip));
                break;
        }
        hasRunVideoClosedLogic = true;
    }

    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
    /*
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        StartCoroutine(IEnumHandleRewardBasedVideoRewarded());
    }
    */
    IEnumerator IEnumHandleRewardBasedVideoRewarded()
    {
        yield return 0;
        adState = AdState.Complete;

        if (hasRunVideoClosedLogic)
        {
            HandleRewardBasedVideoClosed(null, null);
        }
    }


    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        
    }
}
