using Core.Gameplay.Tutorial;
using Fumb.Purchasing;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Adverts
{
    public class InterstitialAdTimer : MonoBehaviour
    {
        [SerializeField]
        private GameObject promptObject;
        
        [SerializeField]
        private UnityEvent<bool> setPromptActive;

        [SerializeField]
        private UnityEvent<bool> setPromptShown;

        [SerializeField]
        private UnityEvent<string> setRemainingTime;

        [SerializeField]
        private UnityEvent<float> setCountdownPercent;
        
        [SerializeField]
        private float threshold = 5f;

        private float nextAd;
        private float NextPrompt => nextAd - threshold;
        private bool shouldPrompt;

        private bool isCountingDown;
        
        private void Start()
        {
            UpdateNextTime(InterstitialManager.instance.NextAd);
            InterstitialManager.OnSetNextAdTime += UpdateNextTime;
            UpdateState();
            PurchasingManager.PurchasingStateChangedAction += UpdateState;
            //TutorialManager.OnComplete += UpdateState;
            if (FTUEManager.Instance.TryGetModule(FTUEModuleId.Core, out FTUEModule coreModule))
                coreModule.onFinalComplete += UpdateState;
        }

        private void OnDestroy()
        {
            InterstitialManager.OnSetNextAdTime -= UpdateNextTime;
            PurchasingManager.PurchasingStateChangedAction -= UpdateState;
            //TutorialManager.OnComplete -= UpdateState;
        }

        private void UpdateState()
        {
            threshold = InterstitialManager.instance.CountdownThreshold;
            shouldPrompt = InterstitialManager.instance.DisplayInterstitialCountdown;
            setPromptActive?.Invoke(shouldPrompt);
            if (!shouldPrompt)
            {
                promptObject.SetActive(false);
                setPromptShown?.Invoke(false);
            }
        }

        private void UpdateNextTime(float time)
        {
            nextAd = time;
            bool show = Time.time > nextAd - threshold && shouldPrompt;
            if (promptObject.activeSelf == show)
                return;
            promptObject.SetActive(show);
            setPromptShown?.Invoke(show);
        }

        private string FormatTime(float seconds)
        {
            int secondsInteger = Mathf.CeilToInt(seconds);
            if (seconds >= 60)
                return $"in {secondsInteger / 60}m {secondsInteger % 60}s";
            return $"in {secondsInteger}";
        }

        private void Update()
        {
            if (!shouldPrompt)
                return;
            if (Time.time < NextPrompt)
                return;
            if (!promptObject.activeSelf)
            {
                promptObject.SetActive(true);
                setPromptShown?.Invoke(true);
            }
            setRemainingTime?.Invoke(FormatTime(nextAd - Time.time));
            if (threshold == 0)
                return;
            float progress = 1f - (nextAd - Time.time) / threshold;
            setCountdownPercent?.Invoke(progress);
        }
    }
}