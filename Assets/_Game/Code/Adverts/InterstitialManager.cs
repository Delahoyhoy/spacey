﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Tutorial;
using UnityEngine;
using Fumb.Purchasing;
using Fumb.Adverts;
using Fumb.General;
using Fumb.UI;

namespace Game.Adverts
{
   public class InterstitialManager : MonoBehaviour
   {
      [SerializeField] 
      private float inputCutoffTime = 2f;
      
      private float lastInput;
      private float nextAd;
      private Queue<Action> adActions = new Queue<Action>();
      
      private int adOverrideFlags;

      public static InterstitialManager instance;
      private bool crossedThreshold;

      [SerializeField]
      private RemoteInterstitialSettings interstitialSettings =
         new RemoteInterstitialSettings("interstitial_settings",
            new InterstitialSettings(true, 60f, InterstitialType.Countdown, 5f));
      
      
      public static event Action<float> OnSetNextAdTime;
      private float remainingTime;
      private bool timerPaused;

      private InterstitialSettings InterstitialSettings => interstitialSettings.Value;
      private float AdDelay => InterstitialSettings.InterstitialDelay;
      
      public bool InterstitialsEnabled => InterstitialSettings.InterstitialsEnabled && 
                                          InterstitialSettings.InterstitialDelay > 0 && 
                                          !AdHub.Instance.GetAdRemoved(AdType.Interstitial);
      public bool DisplayInterstitialCountdown => InterstitialsEnabled && (!FTUEManager.Instance.TryGetModule(FTUEModuleId.Core, out FTUEModule coreModule) || coreModule.Completed) && 
                                                  InterstitialSettings.InterstitialType == InterstitialType.Countdown;

      public float CountdownThreshold => InterstitialSettings.InterstitialCountdownThreshold;
      private float ThresholdCutoff => NextAd - CountdownThreshold;
      
      private bool HasCrossedThreshold => DisplayInterstitialCountdown && Time.time >= ThresholdCutoff;

      public bool QueueEmpty => adActions.Count <= 0;

      private bool AdReady => AdvertManager.IsAdLoaded(AdType.Interstitial) || !QueueEmpty;

      public float NextAd
      {
         get => nextAd;
         set
         {
            nextAd = value;
            OnSetNextAdTime?.Invoke(nextAd);
            crossedThreshold = false;
         }
      }
      
      private bool PurchasingSystemInitialized => PurchasingManager.IsInitialized;

      private void Awake()
      {
         instance = this;
      }
      
      private void Start()
      {
         if (InterstitialsEnabled)
         {
            NextAd = Time.time + AdDelay + 15f;
         }

         SplashScreen.OnCompleteSplashScreen += ResetTimer;
      }

      private void OnDestroy()
      {
         SplashScreen.OnCompleteSplashScreen -= ResetTimer;
      }

      private void Update()
      {
         if (Input.anyKeyDown && !HasCrossedThreshold)
            lastInput = Time.time;

         if (timerPaused)
            return;
         
         if (!crossedThreshold && DisplayInterstitialCountdown && Time.time >= ThresholdCutoff)
         {
            if (!UIManager.Instance.InMenu && Time.time - lastInput > inputCutoffTime && adOverrideFlags <= 0)
               crossedThreshold = true;
            else
               NextAd = Time.time + AdDelay * 0.5f;
         }
         
         if (!InterstitialsEnabled || Time.time < NextAd) 
            return;

         if (!crossedThreshold && (Time.time - lastInput <= inputCutoffTime || UIManager.Instance.InMenu))
         {
            NextAd = Time.time + AdDelay * 0.5f;
            return;
         }
         
         ResetTimer();
         
         if (!SplashScreen.SplashScreenCompleted)
            return;

         bool tutorialCompleted = FTUEManager.Instance.TryGetModule(FTUEModuleId.Core, out FTUEModule coreModule) &&
                                  coreModule.Completed;

         if (AdHub.Instance.GetAdRemoved(AdType.Interstitial) || !tutorialCompleted || adOverrideFlags > 0) 
            return;
         
         if (!QueueEmpty)
         {
            Action action = adActions.Dequeue();
            LoadingScreenController.Display(true);
            
            SetTimer.StartTimer(0.5f, () =>
            {
               LoadingScreenController.Display(false);
               action?.Invoke();
            });
            
            return;
         }

         if (AdvertManager.IsAdLoaded(AdType.Interstitial))
            AdvertManager.DisplayAd(AdType.Interstitial, "Interstitial",
               () => { NextAd = Time.time + AdDelay * 1.25f; });
         
      }

      private void OnApplicationFocus(bool hasFocus)
      {
         if (hasFocus) {
            TryRefreshNextAd();
         }
      }

      private void TryRefreshNextAd() {
         if (!PurchasingSystemInitialized) {
            return;
         }
      
         if (InterstitialsEnabled && NextAd - Time.time < 10f && !crossedThreshold)
            NextAd = Time.time + AdDelay;
      }
      
      public void AddOverrideFlag()
      {
         adOverrideFlags++;
      }

      public void ClearOverrideFlag()
      {
         adOverrideFlags--;
      }

      public bool AddActionToQueue(Action action)
      {
         bool adsEnabled = InterstitialsEnabled;
         
         if (InterstitialsEnabled)
            adActions.Enqueue(action);

         return adsEnabled;
      }

      public void PauseTimer()
      {
         if (timerPaused)
            return;
         remainingTime = NextAd - Time.time;
         timerPaused = true;
      }

      public void UnpauseTimer()
      {
         if (!timerPaused)
            return;
         NextAd = Time.time + remainingTime;
         timerPaused = false;
      }
      
      public void ResetTimer()
      {
         if (Time.unscaledTime < 20f)
            NextAd = Time.time + AdDelay + 15f;
         else
            NextAd = Time.time + AdDelay;
      }
   }
}