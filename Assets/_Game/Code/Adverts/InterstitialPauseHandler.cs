using UnityEngine;

namespace Game.Adverts
{
    public class InterstitialPauseHandler : MonoBehaviour
    {
        public void SetPaused(bool paused)
        {
            if (paused)
                InterstitialManager.instance.PauseTimer();
            else
                InterstitialManager.instance.UnpauseTimer();
        }

        public void ResetTimer()
        {
            InterstitialManager.instance.ResetTimer();
        }
    }
}