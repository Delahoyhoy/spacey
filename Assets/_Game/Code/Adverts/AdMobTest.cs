﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdMobTest : MonoBehaviour {

    [SerializeField]
    private GenericAdManager adManager;

    [SerializeField]
    private Text output;

	// Use this for initialization
	void Start () {
        output.text = SystemInfo.deviceUniqueIdentifier;
	}

    public void Test()
    {
        if (adManager && adManager.AdReady)
            adManager.ShowAd(HandleResult);
        else
            output.text = "Ad Unavailable";
    }

    void HandleResult(AdResult result)
    {
        switch (result)
        {
            case AdResult.Fail:
                output.text = "Failed!";
                break;
            case AdResult.Skip:
                output.text = "Skipped!";
                break;
            case AdResult.Success:
                output.text = "Success!";
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
