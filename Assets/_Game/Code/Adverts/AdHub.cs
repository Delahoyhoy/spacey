﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Core.Purchasing;
using Fumb.Adverts;
using Game.Adverts;

public class AdHub : MonoBehaviour 
{
    public static AdHub Instance { get { if (!instance) instance = FindObjectOfType<AdHub>(); return instance; } }
    private static AdHub instance;

    public bool RewardedAdReady => GetAdRemoved(AdType.VideoReward) || AdvertManager.IsAdLoaded(AdType.VideoReward);

    private Dictionary<AdRemoveType, List<string>> adsRemovedFlags = new Dictionary<AdRemoveType, List<string>>();

    private static bool hasGoneToAd;
    
    public Action<AdType, bool> onSetAdRemoved;

    public bool GetAdRemoved(AdType type)
    {
        if (adsRemovedFlags.TryGetValue(AdRemoveType.All, out List<string> allAdsRemovedFlags) &&
            allAdsRemovedFlags.Count > 0)
            return true;

        return type switch
        {
            AdType.Interstitial => adsRemovedFlags.TryGetValue(AdRemoveType.Interstitial, out List<string> interstitialAdsRemovedFlags) &&
                                   interstitialAdsRemovedFlags.Count > 0,
            AdType.VideoReward => adsRemovedFlags.TryGetValue(AdRemoveType.Rewarded, out List<string> rewardedAdsRemovedFlags) &&
                                  rewardedAdsRemovedFlags.Count > 0,
            _ => false
        };
    }

    public void AddAdRemovedFlag(AdRemoveType type, string flagIdentifier)
    {
        if (!adsRemovedFlags.ContainsKey(type))
            adsRemovedFlags.Add(type, new List<string>());
        if (adsRemovedFlags[type].Contains(flagIdentifier)) 
            return;
        adsRemovedFlags[type].Add(flagIdentifier);
        UpdateAdRemovedStatus();
    }

    public void RemoveAdRemovedFlag(AdRemoveType type, string flagIdentifier)
    {
        if (!adsRemovedFlags.ContainsKey(type))
            adsRemovedFlags.Add(type, new List<string>());
        if (!adsRemovedFlags[type].Contains(flagIdentifier)) 
            return;
        adsRemovedFlags[type].Remove(flagIdentifier);
        UpdateAdRemovedStatus();
    }

    private void UpdateAdRemovedStatus()
    {
        onSetAdRemoved?.Invoke(AdType.Interstitial, GetAdRemoved(AdType.Interstitial));
        onSetAdRemoved?.Invoke(AdType.VideoReward, GetAdRemoved(AdType.VideoReward));

        int interstitialFlags = 0;
        int rvFlags = 0;

        foreach (AdRemoveType adType in adsRemovedFlags.Keys)
        {
            switch (adType)
            {
                case AdRemoveType.Interstitial:
                    interstitialFlags += adsRemovedFlags[adType].Count;
                    break;
                case AdRemoveType.Rewarded:
                    rvFlags += adsRemovedFlags[adType].Count;
                    break;
                case AdRemoveType.All:
                    int count = adsRemovedFlags[adType].Count;
                    rvFlags += count;
                    interstitialFlags += count;
                    break;
            }
        }

        AdvertManager.ManualRemoveAds = interstitialFlags > 0 && rvFlags > 0;
    }
    
    public static bool Ready
    {
        get
        {
#if UNITY_EDITOR
            return Time.time > 5f;
#else
            return Instance.RewardedAdReady;
#endif
        }
    }

    public static void ShowAd(string placementDescription, System.Action callbackSuccess, System.Action userCancelled = null, System.Action<string> completionError = null)
    {
        if (Instance.GetAdRemoved(AdType.VideoReward))
        {
            callbackSuccess?.Invoke();
            return;
        }

        void Success()
        {
            callbackSuccess?.Invoke();
            InterstitialManager.instance.ResetTimer();
            hasGoneToAd = false;
        }
        
        hasGoneToAd = true;
        AdvertManager.DisplayAd(AdType.VideoReward, placementDescription, Success, userCancelled, completionError);
    }

    private void Awake()
    {
        instance = this;
    }
}
