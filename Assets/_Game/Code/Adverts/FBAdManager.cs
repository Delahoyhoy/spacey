﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//using AudienceNetwork;

public class FBAdManager : MonoBehaviour {

    private UnityAction<AdResult> resultCallback;

    public bool AdReady { get { return IsFacebookAdReady(); } }

    public static bool Ready { get { if (Instance) return Instance.AdReady; return false; } }

    public static FBAdManager Instance { get { if (!instance) instance = FindObjectOfType<FBAdManager>(); return instance; } }
    private static FBAdManager instance;

//    private RewardedVideoAd rewardedVideoAd;
    private bool isFBAdLoaded = false;

    public bool IsFacebookAdReady()
    {
        //if (!isFBAdLoaded)
        //    LoadFBAd();
        return isFBAdLoaded;
    }

    private void LoadFBAd()
    {
        try
        {
            LoadRewardedVideo();
        }
        catch
        {
            Debug.Log("Failed to load FB Ad");
            isFBAdLoaded = false;
        }
    }

    public void LoadRewardedVideo()
    {
        Debug.Log("Loading rewardedVideo ad...");

        // Create the rewarded video unit with a placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
/*#if UNITY_ANDROID || UNITY_EDITOR
        RewardedVideoAd rewardedVideoAd = new RewardedVideoAd("254381745439170_261502904727054");
#elif UNITY_IOS
        RewardedVideoAd rewardedVideoAd = new RewardedVideoAd("254381745439170_262955074581837"); //TODO: PRERELEASE: UPDATE IOS ADS
#endif
        this.rewardedVideoAd = rewardedVideoAd;
        this.rewardedVideoAd.Register(this.gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        this.rewardedVideoAd.RewardedVideoAdDidLoad = (delegate () {
            Debug.Log("RewardedVideo ad loaded.");
            this.isFBAdLoaded = true;
            Debug.Log("Ad loaded. Click show to present!");
        });
        rewardedVideoAd.RewardedVideoAdDidFailWithError = (delegate (string error) {
            Debug.Log("RewardedVideo ad failed to load with error: " + error);
            HandleShowResult(AdResult.Fail);
        });
        rewardedVideoAd.RewardedVideoAdWillLogImpression = (delegate () {
            Debug.Log("RewardedVideo ad logged impression.");
            HandleShowResult(AdResult.Success);
            LoadFBAd();
        });
        rewardedVideoAd.RewardedVideoAdDidClick = (delegate () {
            Debug.Log("RewardedVideo ad clicked.");
        });

        // Initiate the request to load the ad.
        this.rewardedVideoAd.LoadAd();
        */
    }

    private void HandleShowResult(AdResult result)
    {
        if (resultCallback != null)
        {
            resultCallback.Invoke(result);
        }
    }

    public void ShowAd(UnityAction<AdResult> callback)
    {
        resultCallback = callback;
        if (isFBAdLoaded)
        {
//            rewardedVideoAd.Show();
            isFBAdLoaded = false;
            // this.statusLabel.text = "";
        }
        else
        {
            Debug.Log("Ad not loaded!");
        }
    }

    // Use this for initialization
    void Start () {
        LoadFBAd();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
