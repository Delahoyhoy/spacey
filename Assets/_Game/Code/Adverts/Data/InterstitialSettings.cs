using System;
using UnityEngine;

namespace Game.Adverts
{
    [Serializable]
    public class InterstitialSettings
    {
        [SerializeField]
        private bool interstitialsEnabled;

        public bool InterstitialsEnabled => interstitialsEnabled;
        
        [SerializeField]
        private float interstitialDelay;

        public float InterstitialDelay => interstitialDelay;

        [SerializeField]
        private InterstitialType interstitialType;

        public InterstitialType InterstitialType => interstitialType;

        [SerializeField]
        private float interstitialCountdownThreshold;

        public float InterstitialCountdownThreshold => interstitialCountdownThreshold;

        public InterstitialSettings(bool enabled, float delay, InterstitialType type, float threshold = 0)
        {
            interstitialsEnabled = enabled;
            interstitialDelay = delay;
            interstitialType = type;
            interstitialCountdownThreshold = threshold;
        }

    }
}