using System;
using Fumb.RemoteConfig;

namespace Game.Adverts
{
    [Serializable]
    public class RemoteInterstitialSettings : RemoteJson<InterstitialSettings>
    {
        public RemoteInterstitialSettings(string key, InterstitialSettings defaultValue) : base(key, defaultValue) { }
    }
}