using System.Collections;
using System.Collections.Generic;
using Fumb;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Gameplay.Utilities
{
    public class SceneUnloader : MonoBehaviour
    {
        [SerializeField]
        private int sceneIndex;

        public void UnloadScene()
        {
            SceneLoader.UnloadSceneAsync(sceneIndex);
        }
    }
}
