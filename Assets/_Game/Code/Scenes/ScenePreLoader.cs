using System;
using System.Collections;
using Fumb;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Core.Gameplay.Utilities
{
    public class ScenePreLoader : MonoBehaviour
    {
        [SerializeField]
        private int sceneToPreLoad = 1;

        private bool ready;

        [SerializeField]
        private UnityEvent<bool> onReady;

        private Action goAction;
        

        private void Start()
        {
            PreLoadScene();
        }
    
        private IEnumerator LoadScene()
        {
            AsyncOperation sceneLoading = SceneLoader.LoadSceneAsync(sceneToPreLoad, LoadSceneMode.Additive);
            sceneLoading.allowSceneActivation = false;
            while (sceneLoading.progress < 0.9f)
                yield return null;
            yield return new WaitForSeconds(1f);
            ready = true;

            void Go()
            {
                sceneLoading.allowSceneActivation = true;
            }

            goAction = Go;
            onReady?.Invoke(ready);
        }

        public void PreLoadScene()
        {
            ready = false;
            onReady?.Invoke(false);
            StartCoroutine(LoadScene());
        }

        public void GoToScene()
        {
            goAction?.Invoke();
        }
    }
}
