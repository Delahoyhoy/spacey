﻿using System.Collections;
using System.Collections.Generic;
using Fumb.LocalNotifications;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#endif
using UnityEngine;

//TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
//using LocalNotifications;

[System.Serializable]
public struct Notification
{
    public Notification(string t, string m, double s)
    {
        title = t;
        allMessages = new string[] { m };
        seconds = s;
    }

    public string title;

    public string[] allMessages;

    public string GetMessage()
    {
        return allMessages[Random.Range(0, allMessages.Length)];
    }

    public double seconds;
}

public interface INotify
{
    void CompileNotifications(ref List<Notification> notifications);
}

public class NotificationController : MonoBehaviour {

    private List<INotify> notificationSetters = new List<INotify>();

    [SerializeField]
    private Notification[] notifications;

    private bool notificationsReady = false;

    public static NotificationController Instance { get { if (!instance) instance = FindObjectOfType<NotificationController>(); return instance; } }
    private static NotificationController instance;

    private bool notificationsEnabled = true;

    void OnManagerInitialised()
    {
        notificationsReady = true;
        Debug.Log("Notifications Ready");

        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        //LocalNotificationManager.CancelAllNotifications();
        notificationSetters = ObjectFinder.FindAllObjectsOfType<INotify>();
    }

	// Use this for initialization
	void Start () {

        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        //if (!LocalNotificationManager.HasInitialised)
        //    LocalNotificationManager.Initialise(OnManagerInitialised);
	}

    public void CancelNotifications()
    {

        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        //if (notificationsReady)
        //    LocalNotificationManager.CancelAllNotifications();
        StartCoroutine(WaitCancelNotifications());
    }

    private IEnumerator WaitCancelNotifications()
    {
        while (!LocalNotificationManager.HasInitialised)
            yield return null;
#if UNITY_ANDROID
        AndroidNotificationCenter.CancelAllNotifications();
        AndroidNotificationCenter.CancelAllDisplayedNotifications();
#endif
    }

    public void ScheduleNotifications()
    {

        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        /*if (!notificationsEnabled || !notificationsReady || LocalNotificationManager.NotificationsEnabledState == NotificationsEnabledState.Disabled || PlayerPrefs.GetInt("notificationsEnabled", 2) <= 0)
            return;
        Debug.Log(string.Format("Setting {0} notifications", notificationSetters.Count));
        LocalNotificationManager.CancelAllNotifications();
        List<Notification> allNotifications = new List<Notification>(notifications);
        for (int i = 0; i < notificationSetters.Count; i++)
        {
            notificationSetters[i].CompileNotifications(ref allNotifications);
        }
        for (int i = 0; i < allNotifications.Count; i++)
            ScheduleNotification(allNotifications[i]);
        */
    }

    void ScheduleNotification(Notification notification)
    {
        //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
        //LocalNotificationManager.ScheduleNotification(new LocalNotificationFromTimespan(notification.seconds, Random.Range(0, int.MaxValue), notification.title, notification.GetMessage()));
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
            ScheduleNotifications();
        else
            CancelNotifications();
    }
}
