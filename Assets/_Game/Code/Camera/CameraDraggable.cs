﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Core.Gameplay.CameraControl
{
    public class CameraDraggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler,
        IPointerClickHandler
    {
        private bool dragging;

        [SerializeField]
        private bool interactable = true;
        
        [SerializeField]
        private UnityEvent onClick;

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (dragging)
                return;
            dragging = true;
            BasicCameraController.Instance.currentlyDragging = true;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (dragging)
                BasicCameraController.Instance.MoveBy((eventData.delta / Screen.height) * 900);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (!dragging)
                return;
            dragging = false;
            BasicCameraController.Instance.currentlyDragging = false;
            BasicCameraController.Instance.SetInertia((eventData.delta / Screen.height) * 900);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (dragging || !interactable)
                return;
            onClick?.Invoke();
        }

        public void SetInteractable(bool active)
        {
            interactable = active;
        }
    }
}
