﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Ships;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.CameraControl
{
    public class BasicCameraController : MonoBehaviour
    {
        [SerializeField]
        public Camera mainCamera;

        [SerializeField]
        public float cameraZoomSpeed = 0.05f;
        //[SerializeField]
        //float minX = -9;
        //[SerializeField]
        //float maxX = 14;
        //[SerializeField]
        //float minY = -14;
        //[SerializeField]
        //float maxY = 5;

        private Vector3 inertia;

        [SerializeField]
        private float drag = 1f;

        private bool dragging = false;

        Vector3 previousTouchPos;
        Vector3 currentTouchPos;

        [SerializeField]
        float minOrthographicCamSize = 1.45f;

        [SerializeField]
        float maxOrthographicCamSize = 25.0f;

        [SerializeField]
        Transform topZoomBarTrans;

        [SerializeField]
        Transform bottomZoomBarTrans;

        [SerializeField]
        GameObject currentZoomLevelObject;

        [SerializeField]
        private AnimationCurve galaxyZoomRate;

        [SerializeField]
        private Camera[] additionalCameras;

        public Vector3 CurrentPos => transform.position;
        public float CurrentZoom => mainCamera.orthographicSize;

        public bool InSystemView => currentView == CurrentView.System;

        [SerializeField]
        private float galaxyZoomTime = 2f;

        private CurrentView currentView = CurrentView.System;

        Vector3 previousZoomDiff;
        Vector3 currentZoomDiff;
        float currentZoomDistance;
        float previousZoomDistance;

        bool waitFrameToUpdate = true;
        bool waitFrameToUpdateZoomDiff = true;
        public bool isGalaxyScene = false;

        private bool locked = false;

        private float zoomIntertia = 0;

        private static BasicCameraController instance;

        public static BasicCameraController Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<BasicCameraController>();
                return instance;
            }
        }

        private float zoomMult = 1f;

        [SerializeField]
        private GaussianTest cameraPostProcess;

        [SerializeField]
        private AnimationCurve panCurve;

        private bool lockedOn = false;
        private Transform lockedTarget;

        [SerializeField]
        private Transform tutorialTarget;

        public bool currentlyDragging = false;

        [SerializeField]
        private GameObject galaxyView;

        [SerializeField]
        private Animator systemUIAnim;

        private float lockedZoom = 8f;
        private float zoomBeforeLock = 8f;

        private bool zoomAltered;

        public void LockOnTarget(Transform target, float targetZoom = 8)
        {
            if (!lockedOn && zoomAltered)
                zoomBeforeLock = mainCamera.orthographicSize;
            lockedOn = true;
            inertia = Vector3.zero;
            lockedZoom = targetZoom;
            lockedTarget = target;
            zoomAltered = false;
        }

        public void LockTutorialTarget()
        {
            LockOnTarget(tutorialTarget);
        }

        public void UnlockTarget()
        {
            lockedOn = false;
        }

        public void ZoomOutToGalaxyView()
        {
            if (currentView == CurrentView.System)
                StartCoroutine(ZoomToGalaxy());
        }

        public void ZoomInToSystemView()
        {
            if (currentView == CurrentView.Galaxy)
                StartCoroutine(ZoomToSystem());
        }

        public void SetUI(bool active)
        {
            //systemUIAnim.Play(active ? "SystemUIIn" : "SystemUIOut");
        }

        public void SetZoomMult(float mult)
        {
            float baseZoom = mainCamera.orthographicSize / zoomMult;
            zoomMult = mult;
            float newZoom = mainCamera.orthographicSize = baseZoom * zoomMult;
            GameManager.ChangeZoom(newZoom);
        }

        private IEnumerator ZoomToGalaxy()
        {
            currentView = CurrentView.Transitioning;
            galaxyView.gameObject.SetActive(true);
            systemUIAnim.Play("SystemUIOut");
            float startZoom = mainCamera.orthographicSize;
            float targetZoom = 300f;
            Vector3 startPos = transform.position;
            Vector3 targetPos = GameManager.Instance.FogCentre;
            targetPos.z = startPos.z;
            if (cameraPostProcess)
                cameraPostProcess.OnStartTransition(CurrentView.Galaxy);
            for (float f = 0; f < 1f; f += Time.deltaTime / galaxyZoomTime)
            {
                float t = galaxyZoomRate.Evaluate(f);
                SetZoom(Mathf.Lerp(startZoom, targetZoom, t));
                transform.position = Vector3.Lerp(startPos, targetPos, t);
                GameManager.MoveCamera(transform.position);
                if (cameraPostProcess)
                {
                    cameraPostProcess.SetTransitionAmount(t);
                    cameraPostProcess.SetSecondaryCameraSize(mainCamera.orthographicSize);
                }

                yield return null;
            }

            SetZoom(targetZoom);
            if (cameraPostProcess)
                cameraPostProcess.OnEndTransition(CurrentView.Galaxy);
            currentView = CurrentView.Galaxy;
        }

        public void ResetCameraToPos(Vector3 targetPos)
        {
            targetPos.z = transform.position.z;
            transform.position = targetPos;
            mainCamera.orthographicSize = 8f;
            ClampCamera();
            GameManager.MoveCamera(transform.position);
            GameManager.ChangeZoom(mainCamera.orthographicSize);
            dragging = false;
            zooming = false;
        }

        private IEnumerator ZoomToSystem()
        {
            currentView = CurrentView.Transitioning;
            float startZoom = mainCamera.orthographicSize;
            float targetZoom = 6f;
            Vector3 startPos = transform.position;
            Vector3 targetPos = GameManager.Instance.FogCentre;
            targetPos.z = startPos.z;
            if (cameraPostProcess)
                cameraPostProcess.OnStartTransition(CurrentView.System);
            for (float f = 0; f < 1f; f += Time.deltaTime / galaxyZoomTime)
            {
                float t = galaxyZoomRate.Evaluate(f);
                transform.position = Vector3.Lerp(startPos, targetPos, galaxyZoomRate.Evaluate(f * 1.5f));
                GameManager.MoveCamera(transform.position);
                SetZoom(Mathf.Lerp(startZoom, targetZoom, f));
                if (cameraPostProcess)
                {
                    cameraPostProcess.SetTransitionAmount(1f - t);
                    cameraPostProcess.SetSecondaryCameraSize(mainCamera.orthographicSize);
                }

                yield return null;
            }

            SetZoom(targetZoom);
            if (cameraPostProcess)
                cameraPostProcess.OnEndTransition(CurrentView.System);
            systemUIAnim.Play("SystemUIIn");
            galaxyView.gameObject.SetActive(false);
            currentView = CurrentView.System;
        }

        private void Start()
        {
            GameManager.ChangeZoom(mainCamera.orthographicSize);
            GameManager.MoveCamera(transform.position);
        }

        float currentZoomLevel = 0;

        private void Update()
        {
            if (lockedOn)
            {
                if (lockedTarget)
                {
                    Vector3 targetPos = lockedTarget.transform.position;
                    targetPos.z = transform.position.z;
                    transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * 8f);
                    mainCamera.orthographicSize =
                        Mathf.Lerp(mainCamera.orthographicSize, lockedZoom * zoomMult, Time.deltaTime * 4f);
                    ClampCamera();
                    GameManager.MoveCamera(transform.position);
                    GameManager.ChangeZoom(mainCamera.orthographicSize);
                    dragging = false;
                    zooming = false;
                }

                return;
            }

            if (!zoomAltered)
            {
                mainCamera.orthographicSize =
                    Mathf.Lerp(mainCamera.orthographicSize, zoomBeforeLock * zoomMult, Time.deltaTime * 4f);
                GameManager.ChangeZoom(mainCamera.orthographicSize);
            }

            if (UIManager.Instance && UIManager.Instance.InMenu)
                return;
            
            //if (Input.GetKeyDown(KeyCode.J))
            //    LockOntoNearestShip();
            if (scanningForTutorial)
            {
                Asteroid asteroid;
                if (GetAsteroidInRange(out asteroid))
                {
                    //TutorialManager.OnTrigger("search");
                    //TutorialManager.Instance.DoCustomHighlight(asteroid.transform, 0.2f, HighlightType.World);
                    SetScanning(false);
                    LockOnTarget(asteroid.tutorialAnchor);
                }
            }

            if (Input.GetKey(KeyCode.Q))
            {
                Zoom(cameraZoomSpeed);
            }

            if (Input.GetKey(KeyCode.W))
            {
                Zoom(-cameraZoomSpeed);
            }

            float scroll = Input.GetAxis("Mouse ScrollWheel");
            if (Mathf.Abs(scroll) > 0)
                Zoom(cameraZoomSpeed * -scroll * 30f);
#if UNITY_EDITOR

            if (currentView == CurrentView.Transitioning)
            {
                inertia = Vector3.zero;
                return;
            }

            if (currentlyDragging) 
                return; //else
            waitFrameToUpdate = true;
            dragging = false;
            if (inertia.magnitude > 0 && !locked)
            {
                transform.position += inertia;
                ClampCamera();
                inertia = Vector3.Lerp(inertia, Vector3.zero, drag * Time.deltaTime);
                GameManager.MoveCamera(transform.position);
            }

#else
            if (currentView == CurrentView.Transitioning)
            {
                inertia = Vector3.zero;
                return;
            }

            if (!currentlyDragging && !locked)
            {
                if (inertia.magnitude > 0)
                {
                    transform.position += inertia;
                    ClampCamera();
                    inertia = Vector3.Lerp(inertia, Vector3.zero, drag * Time.deltaTime);
                    GameManager.MoveCamera(transform.position);
                }
            }

            if (!zooming)
            {
                if (Input.touchCount >= 2)
                    StartCoroutine(ZoomCamera());
                if (zoomIntertia > 0)
                {
                    Zoom(-(zoomIntertia) / 100.0f);
                    zoomIntertia = Mathf.Lerp(zoomIntertia, 0, drag * Time.deltaTime * 4f);
                }
            }
#endif
            
        }

        private bool scanningForTutorial = false;

        public void SetScanning(bool scan)
        {
            scanningForTutorial = scan;
        }

        public void HighlightScannedAsteroid()
        {
            Asteroid scannedAsteroid = GameManager.Instance.GetScannedAsteroid();
            if (scannedAsteroid)
                LockOnTarget(scannedAsteroid.tutorialAnchor);
        }

        public void HighlightBuiltAsteroid()
        {
            Asteroid scannedAsteroid = GameManager.Instance.GetBuiltAsteroid();
            if (scannedAsteroid)
                LockOnTarget(scannedAsteroid.tutorialAnchor);
        }

        public Vector3 GetRandomWorldPosOnScreen()
        {
            float orthoSize = mainCamera.orthographicSize;
            Vector3 offset = new Vector3(orthoSize, orthoSize, 0);
            Vector3 position = transform.position;
            Vector3 min = position - offset, max = position + offset;
            return new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), 0);
        }

        public void LockOntoNearestShip()
        {
            MiningShip[] allShips = FindObjectsOfType<MiningShip>();
            float closest = float.MaxValue;
            MiningShip closestObject = null;
            for (int i = 0; i < allShips.Length; i++)
            {
                allShips[i].IsTrackedShip = false;
                float distance = Vector3.Distance(transform.position, allShips[i].transform.position);
                if (distance < closest)
                {
                    closest = distance;
                    closestObject = allShips[i];
                }
            }

            if (closestObject)
            {
                closestObject.IsTrackedShip = true;
                LockOnTarget(closestObject.transform);
            }
        }

        private bool GetAsteroidInRange(out Asteroid asteroid)
        {
            for (int i = 0; i < AsteroidManager.Instance.AsteroidCount; i++)
            {
                Asteroid current = GameManager.Instance.GetAsteroid(i);
                Vector3 targetPos = current.transform.position;
                targetPos.z = transform.position.z;
                if (Vector3.Distance(targetPos, transform.position) < mainCamera.orthographicSize)
                {
                    if (current.State == AsteroidState.Unscanned)
                    {
                        asteroid = current;
                        return true;
                    }
                }
            }

            asteroid = null;
            return false;
        }

        public void SetInertia(Vector2 newValue)
        {
            if (locked || lockedOn)
                return;
            inertia = newValue;
            inertia /= 450f;
            inertia *= -1;
            inertia *= mainCamera.orthographicSize;
        }

        public void MoveBy(Vector2 move)
        {
            if (lockedOn || locked || Input.touchCount > 1 || currentView == CurrentView.Transitioning)
                return;
            Vector3 diff = move;
            diff *= -1;
            diff /= 450f;
            diff *= mainCamera.orthographicSize;
            transform.position += diff;
            ClampCamera();
            GameManager.MoveCamera(transform.position);
        }

        private bool zooming = false;
        private bool istopZoomBarTransNotNull;

        private IEnumerator Drag()
        {
            dragging = true;
            //Debug.Log("Touch Pos");

            currentTouchPos = Input.touches[0].position; // Input.mousePosition;
            previousTouchPos = currentTouchPos;


            while (Input.touchCount == 1)
            {
                if (UIManager.Instance.InMenu)
                    break;
                if (locked)
                    break;
                previousTouchPos = currentTouchPos;
                currentTouchPos = Input.touches[0].position; // Input.mousePosition;

                Vector3 diff = currentTouchPos - previousTouchPos;

                diff /= 475;
                diff *= -1;
                diff *= (mainCamera.orthographicSize / 1.5f);
                inertia = diff;

                transform.position += diff;
                ClampCamera();

                GameManager.MoveCamera(transform.position);

                yield return null;
            }

            dragging = false;
        }

        //IEnumerator Zoom()
        //{
        //    zooming = true;
        //    inertia = Vector3.zero;
        //    while (Input.touchCount == 2)
        //    {
        //        Zoom(-(currentZoomDistance - previousZoomDistance) / 100.0f);
        //    }
        //}

        private void ClampCamera()
        {
            if (currentView == CurrentView.System)
            {
                Vector3 position = transform.position - GameManager.Instance.FogCentre;
                position.z = 0;
                position = Vector3.ClampMagnitude(position, GameManager.Instance.FogRadius) +
                           GameManager.Instance.FogCentre;
                position.z = transform.position.z;
                transform.position = position;
            }
            else if (currentView == CurrentView.Galaxy)
            {
                Vector3 position = transform.position - GameManager.Instance.FogCentre;
                position.z = 0;
                position = Vector3.ClampMagnitude(position, 250f) + GameManager.Instance.FogCentre;
                position.z = transform.position.z;
                transform.position = position;
            }
        }

        private IEnumerator ZoomCamera()
        {
            zooming = true;
            currentZoomDistance = Vector3.Distance(Input.touches[0].position, Input.touches[1].position);
            previousZoomDistance = currentZoomDistance;
            inertia = Vector3.zero;
            while (Input.touchCount >= 2)
            {
                if (UIManager.Instance.InMenu)
                    break;
                previousZoomDistance = currentZoomDistance;
                currentZoomDistance = Vector3.Distance(Input.touches[0].position, Input.touches[1].position);
                float zoomInertia = currentZoomDistance - previousZoomDistance;
                Zoom(-(currentZoomDistance - previousZoomDistance) / 100.0f);
                yield return null;
            }

            zooming = false;
        }

        public void Pan(Transform target, Vector2 screenOffset = default)
        {
            if (locked)
                return;
            StartCoroutine(PanTo(target, screenOffset));
        }

        private IEnumerator PanTo(Transform target, Vector2 screenOffset)
        {
            Vector3 offset = screenOffset * mainCamera.orthographicSize;
            Vector3 targetPos = target.position + offset;
            Vector3 startPos = transform.position;
            targetPos.z = startPos.z;
            inertia = Vector3.zero;
            locked = true;
            for (float t = 0; t < 1f; t += Time.deltaTime * 2f)
            {
                transform.position = Vector3.Lerp(startPos, targetPos, panCurve.Evaluate(t));
                ClampCamera();
                GameManager.MoveCamera(transform.position);
                yield return null;
            }

            locked = false;
        }

        private void Awake()
        {
            instance = this;
        }

        public void PressZoom(bool zoomIn)
        {
            AudioManager.Play(zoomIn ? "Click" : "Close", 0.75f);
            Zoom(cameraZoomSpeed * (zoomIn ? 30 : -30));

            if (!isGalaxyScene)
            {
                if (mainCamera.orthographicSize < 1.45f)
                {
                    mainCamera.orthographicSize = 1.45f;
                }
                else if (mainCamera.orthographicSize > 25.0f)
                {
                    mainCamera.orthographicSize = 25.0f;
                }
            }
        }

        private void SetZoom(float newZoom)
        {
            mainCamera.orthographicSize = newZoom * zoomMult;
            if (currentView == CurrentView.System)
                mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, minOrthographicCamSize, maxOrthographicCamSize);
            else if (currentView == CurrentView.Galaxy)
                mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, 200f, 550f);
            zoomAltered = true;
            
            GameManager.ChangeZoom(mainCamera.orthographicSize);
        }

        private void Zoom(float by)
        {
            mainCamera.orthographicSize += by;
            if (currentView == CurrentView.System)
                mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, minOrthographicCamSize, maxOrthographicCamSize);
            else if (currentView == CurrentView.Galaxy)
                mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, 200f, 550f);
            zoomAltered = true;

            GameManager.ChangeZoom(mainCamera.orthographicSize);
        }
    }

}