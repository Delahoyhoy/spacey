namespace Core.Gameplay.CameraControl
{
    public enum CurrentView
    {
        System,
        Transitioning,
        Galaxy,
    }
}