using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using UnityEngine;

public class AuxillaryCamera : MonoBehaviour
{
    [SerializeField]
    private Camera targetCamera;

    private bool setup;

    private void Start()
    {
        Setup();
    }

    private void Setup()
    {
        if (setup)
            return;
        
        SetPos(BasicCameraController.Instance.CurrentPos);
        SetZoom(BasicCameraController.Instance.CurrentZoom);
        
        GameManager.OnCameraMove += SetPos;
        GameManager.OnZoomChange += SetZoom;
        
        setup = true;
    }

    private void OnDestroy()
    {
        GameManager.OnCameraMove -= SetPos;
        GameManager.OnZoomChange -= SetZoom;
    }

    private void SetZoom(float zoom)
    {
        targetCamera.orthographicSize = zoom;
    }

    private void SetPos(Vector3 position)
    {
        transform.position = position;
    }
}
