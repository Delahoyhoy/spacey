using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Augments;
using Core.Gameplay.Earnings;
using Core.Gameplay.Reset;
using Core.Gameplay.Ships;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Utilities;
using Core.Gameplay.Version;
using Core.Gameplay.VIP;
using Data;
using Fumb.Data;
using Fumb.LocalNotifications;
using Fumb.Save;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Outposts
{
    public class OutpostManager : MonoBehaviour
    {
        private static OutpostManager instance;
        public static OutpostManager Instance => instance ??= ObjectFinder.FindObjectOfType<OutpostManager>();
        
        [ManagedSaveValue("OutpostManagerPurchasedPremiumOutposts")]
        private SaveValueBool saveHasPurchasedPremiumOutposts;

        [ManagedSaveValue("OutpostManagerHighestLevelsReached")]
        private SaveValueDictionaryIntInt saveHighestLevelsReached, saveHighestPremiumLevelsReached;

        [SerializeField]
        private OutpostMenuVersion[] outpostMenuVersions;

        [SerializeField]
        private OutpostPrefabVersion[] outpostPrefabVersions;

        private bool menuSelected;
        private OutpostMenu outpostMenu;

        private OutpostMenu OutpostMenu
        {
            get
            {
                if (menuSelected)
                    return outpostMenu;
                for (int i = 0; i < outpostMenuVersions.Length; i++)
                {
                    if (!outpostMenuVersions[i].IsValid(GameVersionManager.CurrentVersion, out OutpostMenu menu))
                        continue;
                    outpostMenu = menu;
                    menuSelected = true;
                    return outpostMenu;
                }

                return outpostMenuVersions[outpostMenuVersions.Length - 1].Menu;
            }
        }

        [SerializeField]
        private Outpost outpostPrefab;
        
        [SerializeField]
        private Outpost premiumOutpostPrefab;

        private Outpost OutpostPrefab
        {
            get
            {
                if (prefabSelected)
                    return outpostPrefab;
                for (int i = 0; i < outpostPrefabVersions.Length; i++)
                {
                    if (!outpostPrefabVersions[i].GetValid(GameVersionManager.CurrentVersion, out Outpost prefab))
                        continue;
                    outpostPrefab = prefab;
                    prefabSelected = true;
                    return outpostPrefab;
                }

                return outpostPrefab;
            }
        }

        private bool prefabSelected;

        [SerializeField]
        private MilestoneManager milestoneManager;

        [SerializeField]
        private AnchoredUI uiAnchor;

        [SerializeField]
        private Transform[] outpostPlaceholderAnchors;
        
        [SerializeField]
        private Transform outpostRoot;
        private Queue<Outpost> outpostPool = new Queue<Outpost>();

        [SerializeField]
        private MiningShip miningShipPrefab;

        [SerializeField]
        private List<Outpost> outposts = new List<Outpost>(), premiumOutposts = new List<Outpost>();

        public List<Outpost> Outposts
        {
            get
            {
                if (premiumOutposts.Count <= 0)
                    return outposts;
                List<Outpost> allOutposts = new List<Outpost>();
                allOutposts.AddRange(outposts);
                allOutposts.AddRange(premiumOutposts);
                return allOutposts;
            }
        }

        [SerializeField]
        private OutpostButton outpostButtonPrefab;

        [SerializeField]
        private List<int> availableOutposts = new List<int>(), premiumAvailableOutposts = new List<int>();

        private List<int> AvailableOutposts
        {
            get
            {
                if (premiumAvailableOutposts.Count <= 0)
                    return availableOutposts;
                List<int> allAvailableOutposts = new List<int>();
                allAvailableOutposts.AddRange(availableOutposts);
                for (int i = 0; i < premiumAvailableOutposts.Count; i++)
                    allAvailableOutposts.Add(premiumAvailableOutposts[i] + GetOutpostCount(false));
                return allAvailableOutposts;
            }
        }
        
        public bool CanBuildMoreOutposts => outposts.Count < MaxAvailableOutposts;
        
        public bool AnyOutpostsAvailable => AvailableOutposts.Count > 0;

        public int MaxAvailableOutposts => outpostInventory.MaxCapacity;

        public Action onBuiltOutpost;
        public Action<bool> onSetOutpostAvailable;

        private List<OutpostButton> outpostButtons = new List<OutpostButton>(), 
            premiumOutpostButtons = new List<OutpostButton>();

        [SerializeField]
        private OutpostInventory outpostInventory;

        [SerializeField]
        private OutpostUpgradeController upgradeController;

        [SerializeField]
        private OutpostData[] outpostData;
        [SerializeField]
        private OutpostStage[] outpostStages;

        public List<Outpost> ActiveOutposts
        {
            get
            {
                List<Outpost> activeOutposts = new List<Outpost>();
                for (int i = 0; i < Outposts.Count; i++)
                {
                    if (!Outposts[i].IsMining)
                        continue;
                    activeOutposts.Add(Outposts[i]);
                }

                return activeOutposts;
            }
        }

        [SerializeField]
        private UnityEvent<bool> promptSpawnPremium;

        public double NextOutpostCost => OutpostCost(outposts.Count, 0);

        private void Awake()
        {
            instance = this;
        }

        public double OutpostCost(int index, int level)
        {
            return GameVersionManager.CurrentVersion switch
            {
                GameVersion.Version1 => index <= 0 && level <= 0 ?
                    100d : (100d * Math.Pow(3, index) * Math.Pow(2d, level)),
                GameVersion.Version2 => 
                    index <= 0 && level <= 0 ? 50d :
                    index <= 0 && level <= 1 ? 100d :
                    Math.Floor(100d * Math.Max(Math.Pow(1.2d, index), 1d) * Math.Max(Math.Pow(1.35d, level + 1), 1d)),
                _ => Math.Round(upgradeController.GetCostValue(level) * outpostData[index].CostModifier), // Use the value derived from data
                //_ => Math.Round((level <= 0 ? 10d : 10d * Math.Exp(0.357902521 * (level - 1))) * outpostData[index].CostModifier),
            };
        }

        public double GetEffectiveValueAtLevel(int level)
        {
            return upgradeController.GetValueMultiplier(level) * Math.Pow(GlobalDataManager.Instance.Data.AdditionalMilestoneMultiplier, milestoneManager.GetMilestonesCrossed(level));;
        }

        public double GetValueMultForIndex(int index)
        {
            return outpostData[index].ValueModifier;
        }

        private Transform GetBestOutpostAnchor()
        {
            List<Asteroid> asteroids = AsteroidManager.Instance.Asteroids;

            List<Transform> activeTransforms = new List<Transform>();
            for (int i = 0; i < asteroids.Count; i++)
            {
                if (asteroids[i].State == AsteroidState.Finished)
                    continue;
                activeTransforms.Add(asteroids[i].transform);
            }

            for (int i = 0; i < Outposts.Count; i++)
                activeTransforms.Add(Outposts[i].transform);

            float furthestOverall = 0;
            int bestIndex = 0;

            for (int i = 0; i < outpostPlaceholderAnchors.Length; i++)
            {
                Vector3 placeholderPos = outpostPlaceholderAnchors[i].position;
                float minDistance = float.MaxValue;
                for (int j = 0; j < activeTransforms.Count; j++)
                {
                    float distance = Vector3.Distance(placeholderPos, activeTransforms[j].position);
                    if (distance < minDistance)
                        minDistance = distance;
                }

                if (minDistance < furthestOverall)
                    continue;
                furthestOverall = minDistance;
                bestIndex = i;
            }

            return outpostPlaceholderAnchors[bestIndex];
        }
        
        public int GetOutpostCount(bool includePremium)
        {
            return includePremium ? Outposts.Count : outposts.Count;
        }

        public OutpostUpgradeCount UpgradeCount
        {
            get => (OutpostUpgradeCount) saveUpgradeCount.Value;
            private set
            {
                saveUpgradeCount.Value = (int) value;
                onUpgradeCountUpdated?.Invoke(UpgradeCount);
            }
        }

        public void SetOutpostAvailable(int outpostIndex, bool premium)
        {
            if (premium)
            {
                if (premiumAvailableOutposts.Contains(outpostIndex))
                    return;
                premiumAvailableOutposts.Add(outpostIndex);
                onSetOutpostAvailable?.Invoke(AvailableOutposts.Count > 0);    
                return;
            }
            if (availableOutposts.Contains(outpostIndex))
                return;
            availableOutposts.Add(outpostIndex);
            onSetOutpostAvailable?.Invoke(AvailableOutposts.Count > 0);
        }

        public void OpenDeployMenu(Asteroid asteroid)
        {
            // List<Outpost> available = new List<Outpost>();
            // for (int i =0 ; i < availableOutposts.Count; i++)
            //     available.Add(outposts[availableOutposts[i]]);
            outpostInventory.OpenMenu(asteroid, AvailableOutposts, Outposts);
        }

        public void SetOutpostsInactive()
        {
            List<Outpost> allOutposts = Outposts;
            for (int i = 0; i < allOutposts.Count; i++)
            {
                allOutposts[i].transform.parent = transform;
                allOutposts[i].SetInactive();
            }
        }

        public int GetEffectiveIndex(Outpost outpost)
        {
            if (!outpost.IsPremium)
                return outpost.OutpostIndex;
            return outpost.OutpostIndex + GetOutpostCount(false);
        }

        public bool DeployOutpost(int outpostIndex, out Outpost outpost)
        {
            outpost = null;
            if (outpostIndex >= Outposts.Count)
                return false;
            if (!AvailableOutposts.Contains(outpostIndex))
                return false;
            int totalStandardOutposts = GetOutpostCount(false);
            if (outpostIndex >= totalStandardOutposts)
                outpost = premiumOutposts[outpostIndex - totalStandardOutposts];
            else
                outpost = outposts[outpostIndex];
            
            if (outpost.IsPremium)
                premiumAvailableOutposts.Remove(outpost.OutpostIndex);
            else
                availableOutposts.Remove(outpostIndex);
            onSetOutpostAvailable?.Invoke(AnyOutpostsAvailable);
            return true;
        }

        public void RefreshButtonLocation(int outpostIndex, bool premium)
        {
            if (premium)
                premiumOutpostButtons[outpostIndex].SetPosition(Vector3.zero);
            else
                outpostButtons[outpostIndex].SetPosition(Vector3.zero);
        }
        
        private void DestroyOutpost(int index)
        {
            if (index >= outposts.Count)
                return;
            Outpost outpost = outposts[index];
            outposts.RemoveAt(index);
            if (!outpost)
                return;
            AugmentManager.Instance.ClearAugments(outpost);
            outpost.OnDisableOutpost();
            outpost.gameObject.SetActive(false);
            outpost.transform.parent = outpostRoot;
            outpostPool.Enqueue(outpost);
        }

        private void DestroyPremiumOutpost(int index)
        {
            Outpost outpost = premiumOutposts[index];
            premiumOutposts.RemoveAt(index);
            AugmentManager.Instance.ClearAugments(outpost);
            outpost.OnDisableOutpost();
            Destroy(outpost.gameObject);
        }
        
        private Outpost NewOutpost(Asteroid asteroid, bool premium = false)
        {
            return NewOutpost(asteroid.outpostAnchor, premium);
        }

        private Outpost NewOutpost(Transform parent, bool premium)
        {
            if (!premium && outpostPool.Count > 0)
            {
                Outpost newOutpost = outpostPool.Dequeue();
                newOutpost.transform.parent = parent;
            }
            return Instantiate(premium ? premiumOutpostPrefab : OutpostPrefab, parent);
        }

        public bool CanOutpostUpgrade()
        {
            for (int i = 0; i < outposts.Count; i++)
            {
                if (outposts[i].CanAffordUpgrade())
                    return true;
            }
            return false;
        }

        [ManagedSaveValue("OutpostManagerUpgradeCount")]
        private SaveValueInt saveUpgradeCount;

        public Action<OutpostUpgradeCount> onUpgradeCountUpdated;

        public void CycleUpgradeCount()
        {
            int index = saveUpgradeCount.Value;
            index++;
            if (index >= Enum.GetValues(typeof(OutpostUpgradeCount)).Length)
                index = 0;
            UpgradeCount = (OutpostUpgradeCount) index;
        }

        public static int GetTotalPossibleUpgrades(Outpost outpost, double totalFunds, out double totalCost)
        {
            int outpostIndex = outpost.OutpostIndex;
            int outpostLevel = outpost.Level;
            int maxLevel = outpost.MaximumLevel;
            double remainingFunds = totalFunds;//CurrencyController.Instance.CurrentGold;
            totalCost = 0;
            int totalLevels = 0;
            for (int i = outpostLevel + 1; i < maxLevel; i++)
            {
                double cost = Instance.OutpostCost(outpostIndex, i);
                if (cost > remainingFunds)
                    break;
                totalLevels++;
                remainingFunds -= cost;
                totalCost += cost;
            }
            return totalLevels;
        }

        private static double GetTotalUpgradesCost(Outpost outpost, int upgrades)
        {
            int outpostIndex = outpost.OutpostIndex;
            int outpostLevel = outpost.Level;
            double totalCost = 0;
            for (int i = outpostLevel + 1; i <= outpostLevel + upgrades; i++)
            {
                double cost = Instance.OutpostCost(outpostIndex, i);
                totalCost += cost;
            }
            return totalCost;
        }

        public bool CanAffordOutpost(Outpost outpost, out int levels, out double totalCost)
        {
            double availableFunds = CurrencyController.Instance.CurrentGold;
            
            levels = 0;
            totalCost = 0;
            
            switch (UpgradeCount)
            {
                case OutpostUpgradeCount.x1:
                    levels = 1;
                    totalCost = outpost.GetUpgradeCost();
                    return availableFunds >= totalCost;
                case OutpostUpgradeCount.x10:
                    levels = 10;
                    totalCost = GetTotalUpgradesCost(outpost, levels);
                    return availableFunds >= totalCost;
                case OutpostUpgradeCount.Max:
                    int totalLevels = GetTotalPossibleUpgrades(outpost, availableFunds, out totalCost);
                    if (totalLevels <= 0)
                    {
                        levels = 1;
                        totalCost = outpost.GetUpgradeCost();
                        return false;    
                    }
                    levels = totalLevels;
                    return true;
                case OutpostUpgradeCount.Next:
                    levels = milestoneManager.GetLevelsToNextMilestone(outpost.Level);
                    totalCost = GetTotalUpgradesCost(outpost, levels);
                    return availableFunds >= totalCost;
            }
            return false;
        }
        
        public bool BuildOutpost(int asteroidIndex)
        {
            if (!(CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= NextOutpostCost))
                return false;
            if (GameManager.Instance.GetAsteroid(asteroidIndex).State < AsteroidState.Scanned)
                return false;
            
            CurrencyController.Instance.AddCurrency(-NextOutpostCost, CurrencyType.SoftCurrency, false, "outpost", "build_outpost");
            BuildOutpostAtAsteroid(asteroidIndex);
            return true;
        }

        public void BuildOutpostAtAsteroid(int asteroidIndex, bool premium = false)
        {
            Asteroid targetAsteroid = GameManager.Instance.GetAsteroid(asteroidIndex);
            Outpost newOutpost = NewOutpost(targetAsteroid, premium);

            double cargoCapacity = GameManager.Instance.CargoCapacity;
            double cargoSpeed = GameManager.Instance.CargoSpeed;
            
            newOutpost.transform.localPosition = Vector3.zero;
            newOutpost.Setup(asteroidIndex, targetAsteroid, premium ? premiumOutposts.Count : outposts.Count, cargoCapacity);
            newOutpost.SetupMiningShips(miningShipPrefab);
            newOutpost.SetCargoCapacity(cargoCapacity);
            newOutpost.SetCargoSpeed((float)cargoSpeed);
            newOutpost.ActivateOutpost();
            targetAsteroid.SetState(AsteroidState.OutpostBuilt);

            InitialiseNewButton(newOutpost);
            
            GameManager.Instance.RefreshCamera();
            if (premium) 
                premiumOutposts.Add(newOutpost);
            else 
                outposts.Add(newOutpost);
            //GameManager.OnRefreshOutpostCost(NextOutpostCost, CurrencyController.Instance.GetCurrency(CurrencyType.Gold));
            //TutorialManager.OnTrigger("build");
            StatTracker.Instance.IncrementStat("outpostsBuilt", 1);
            onBuiltOutpost?.Invoke();
            MiningStation.Instance.UpdateOutpostCount(Outposts.Count);
        }

        public void BuildOutpostAtPlaceholder(bool autoHide, bool premium)
        {
            Outpost newOutpost = NewOutpost(GetBestOutpostAnchor(), premium);

            double cargoCapacity = GameManager.Instance.CargoCapacity;
            double cargoSpeed = GameManager.Instance.CargoSpeed;
            
            newOutpost.transform.localPosition = Vector3.zero;
            newOutpost.Setup(-1, null, premium ? premiumOutposts.Count : outposts.Count, cargoCapacity);
            newOutpost.SetupMiningShips(miningShipPrefab);
            newOutpost.SetCargoCapacity(cargoCapacity);
            newOutpost.SetCargoSpeed((float)cargoSpeed);
            newOutpost.ActivateOutpost();

            InitialiseNewButton(newOutpost);
            
            GameManager.Instance.RefreshCamera();
            if (premium) 
                premiumOutposts.Add(newOutpost);
            else 
                outposts.Add(newOutpost);
            //GameManager.OnRefreshOutpostCost(NextOutpostCost, CurrencyController.Instance.GetCurrency(CurrencyType.Gold));
            //TutorialManager.OnTrigger("build");
            StatTracker.Instance.IncrementStat("outpostsBuilt", 1);
            onBuiltOutpost?.Invoke();
            MiningStation.Instance.UpdateOutpostCount(outposts.Count);
            if (autoHide)
                newOutpost.SetInactive();
        }

        private void InitialiseNewButton(Outpost outpost)
        {
            OutpostButton newButton = Instantiate(outpostButtonPrefab, GameManager.Instance.AsteroidUIParent);
            newButton.Setup(outpost);
            if (outpost.IsPremium)
                premiumOutpostButtons.Add(newButton);
            else
                outpostButtons.Add(newButton);
        }

        public bool GetOutpostMiningAsteroid(int asteroidIndex, out Outpost outpost)
        {
            outpost = null;
            List<Outpost> allOutposts = Outposts;
            for (int i = 0; i < allOutposts.Count; i++)
            {
                if (allOutposts[i].AsteroidIndex != asteroidIndex)
                    continue;
                outpost = allOutposts[i];
                return true;
            }

            return false;
        }

        private bool GetAnyOutpostStillMining(out double longestOffline)
        {
            double offlineMult = VIPManager.Instance.GetValue(VIPCategory.OfflineEarnings) / 100d;
            TimeSpan longest = new TimeSpan(0);
            bool anyMining = false;
            
            List<Outpost> allOutposts = Outposts;
            for (int i = 0; i < allOutposts.Count; i++)
            {
                if (!allOutposts[i].IsMining)
                    continue;
                anyMining = true;
                TimeSpan timeToComplete = allOutposts[i].GetTimeRemainingOnTarget();
                if (timeToComplete > longest)
                    longest = timeToComplete;
            }

            longestOffline = longest.TotalSeconds * offlineMult;
            return anyMining;
        }

        public double SimulateOfflineEarnings(double seconds)
        {
            double total = 0;
            List<Outpost> allOutposts = Outposts;
            for (int i = 0; i < allOutposts.Count; i++) 
                total += allOutposts[i].SimulateOfflineTime(seconds);

            return total;
        }

        public void OnReset()
        {
            availableOutposts.Clear();
            try
            {
                while (outposts.Count > 0)
                {
                    DestroyOutpost(0);
                }
                while (outpostButtons.Count > 0)
                {
                    Destroy(outpostButtons[0].gameObject);
                    outpostButtons.RemoveAt(0);
                }
            }
            catch (Exception e)
            {
                Debug.Log("--- ERROR OCCURRED ---");
                Debug.LogException(e);
                Debug.Log("----------------------");
                throw;
            }
            
            premiumAvailableOutposts.Clear();
            while (premiumOutposts.Count > 0)
            {
                DestroyPremiumOutpost(0);
            }
            while (premiumOutpostButtons.Count > 0)
            {
                Destroy(premiumOutpostButtons[0].gameObject);
                premiumOutpostButtons.RemoveAt(0);
            }
        }

        public void OnCompleteReset(ResetType resetType)
        {
            prefabSelected = false;
            if (resetType != ResetType.Prestige)
            {
                StartCoroutine(PopInOutposts());
                return;
            }
            if (GameVersionManager.CurrentVersion <= GameVersion.Version2)
                return;
            for (int i = 0; i < outpostData.Length; i++)
            {
                if (outpostData[i].DefaultBuild)
                    BuildOutpostAtPlaceholder(true, false);
            }
            promptSpawnPremium?.Invoke(saveHasPurchasedPremiumOutposts.Value);
        }

        private IEnumerator PopInOutposts()
        {
            yield return new WaitForSeconds(4f);
            List<Outpost> allOutposts = Outposts;
            for (int i = 0; i < allOutposts.Count; i++)
            {
                Transform outpostAnchor = GetBestOutpostAnchor();
                allOutposts[i].transform.parent = outpostAnchor;
                allOutposts[i].transform.localPosition = Vector3.zero;
                allOutposts[i].ClearCargo();
                allOutposts[i].PopIn();
                yield return new WaitForSeconds(Random.Range(0.15f, 0.45f));
            }
        }
        
        public void RegisterVersionUpdated()
        {
            prefabSelected = false;
            menuSelected = false;
            ClearPool();
        }

        public void ClearPool()
        {
            while (outpostPool.Count > 0)
            {
                Outpost outpost = outpostPool.Dequeue();
                Destroy(outpost.gameObject);
            }
        }

        public void SetPremiumOutpostsActive(bool active, int total)
        {
            if (!active)
                return;
            saveHasPurchasedPremiumOutposts.Value = true;
            for (int i = 0; i < total; i++)
            {
                if (i < premiumOutposts.Count)
                    continue;
                BuildOutpostAtPlaceholder(false, true);
            }
        }
        
        public void OpenOutpostMenu(Outpost outpost)
        {
            OutpostMenu.Open(outpost);
            if (uiAnchor)
                uiAnchor.Target = outpost.transform;
        }

        public void OpenOutpostMenu(int index)
        {
            OpenOutpostMenu(Outposts[index]);
        }

        public double GetTotalEarnings(bool includeMinIfNotMining)
        {
            double total = 0;
            List<Outpost> allOutposts = Outposts; 
            for (int i = 0; i < allOutposts.Count; i++)
            {
                total += allOutposts[i].GetTotalGoldPerSecond(includeMinIfNotMining);
            }
            return total;
        }

        public double GetBaseEarningsPerSecond()
        {
            double total = 0;
            List<Outpost> allOutposts = Outposts;

            if (allOutposts.Count <= 0)
                return 0;
            
            for (int i = 0; i < allOutposts.Count; i++)
            {
                total += allOutposts[i].GetBaseMassMinedPerSecond();
            }

            return InterpolationHelper.Lerp(total / allOutposts.Count, total, 0.375f);
        }
        
        public Outpost GetOutpostFromAsteroid(int index)
        {
            for (int i = 0; i < outposts.Count; i++)
            {
                if (outposts[i].AsteroidIndex == index)
                    return outposts[i];
            }
            return null;
        }
        
        public Outpost GetOutpost(int index)
        {
            if (index >= outposts.Count)
                return null;
            return outposts[index];
        }

        public int RetreatAllShips(Action<bool> onShipRetreated)
        {
            int totalToRetreat = 0;
            for (int i = 0; i < outposts.Count; i++)
            {
                totalToRetreat += outposts[i].RetreatShips(onShipRetreated);
            }

            return totalToRetreat;
        }

        private void InitialiseData()
        {
            outpostData = DataAccessor.GetData<DataAssetOutpostData>().Data;
            outpostStages = DataAccessor.GetData<DataAssetOutpostStage>().Data;
            milestoneManager.PopulateMilestones();
        }

        public void RefreshPositions()
        {
            for (int i = 0; i < outposts.Count; i++)
                outposts[i].RefreshPosition();
        }
        
        public string SaveOutposts()
        {
            string data = "";
            for (int i = 0; i < outposts.Count; i++)
            {
                if (i > 0)
                    data += "|";
                data += outposts[i].Save();
            }
            return data;
        }

        public bool SavePremiumOutposts(out string data)
        {
            data = "";

            if (!saveHasPurchasedPremiumOutposts.Value)
                return false;
            
            for (int i = 0; i < premiumOutposts.Count; i++)
            {
                if (i > 0)
                    data += "|";
                data += premiumOutposts[i].Save();
            }

            return true;
        }

        public void LoadDefault()
        {
            InitialiseData();
            for (int i = 0; i < outpostData.Length; i++)
            {
                if (outpostData[i].DefaultBuild)
                    BuildOutpostAtPlaceholder(true, false);
            }
        }

        public void OnBeginReset(ResetType resetType)
        {
            List<Outpost> allOutposts = Outposts;
            for (int i = 0; i < allOutposts.Count; i++)
            {
                allOutposts[i].ClearCargo();
                allOutposts[i].HidePath();
            }
        }

        private void Start()
        {
            ResetManager.Instance.preReset += OnBeginReset;
            ResetManager.Instance.finishReset += OnCompleteReset;
        }

        public void LoadOutposts(string data, bool premium)
        {
            InitialiseData();
            if (string.IsNullOrEmpty(data))
                return;
            string[] splitData = data.Split('|');
            for (int i = 0; i < splitData.Length; i++)
            {
                //Debug.Log(splitData[i]);
                int asteroidIndex = int.Parse(splitData[i].Split('^')[0]);
                bool hasTarget = asteroidIndex >= 0;
                Asteroid targetAsteroid = hasTarget ? GameManager.Instance.GetAsteroid(asteroidIndex) : null;
                Outpost newOutpost = Instantiate(premium ? premiumOutpostPrefab : OutpostPrefab, hasTarget ? targetAsteroid.outpostAnchor : transform);
                newOutpost.transform.localPosition = Vector3.zero;
                //newOutpost.Setup(asteroidIndex, targetAsteroid, outposts.Count, 50, newOutpost.transform.position.y > -5.88 ? topBay : bottomBay);
                newOutpost.Load(splitData[i], premium ? premiumOutposts.Count : outposts.Count, 50);
                newOutpost.SetupMiningShips(miningShipPrefab);
                
                if (hasTarget)
                    newOutpost.ActivateOutpost();
                else
                    newOutpost.SetInactive();
                
                if (hasTarget && targetAsteroid.State < AsteroidState.OutpostBuilt)
                    targetAsteroid.SetState(AsteroidState.OutpostBuilt);
                
                InitialiseNewButton(newOutpost);
                
                GameManager.Instance.RefreshCamera();
                if (!premium)
                    outposts.Add(newOutpost);
                else
                    premiumOutposts.Add(newOutpost);
                //OnRefreshOutpostCost?.Invoke(NextOutpostCost, CurrencyController.Instance.GetCurrency(CurrencyType.Gold));
            }
        }

        public void OnApplicationPause(bool paused)
        {
            if (!paused)
                return;
            
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            bool isAnyMining = GetAnyOutpostStillMining(out double longestOffline);
            if (!isAnyMining)
                return;
            double maxOfflineSeconds = OfflineTimeMultiplier.Instance.MaxTotalSeconds;
            double secondsToTrigger = Math.Min(longestOffline, maxOfflineSeconds);
            DateTime triggerTime = DateTime.Now.AddSeconds(secondsToTrigger);
            
            DateTime earlyLimit = DateTime.Today.AddHours(22), lateLimit = DateTime.Today.AddHours(30);

            if (triggerTime > earlyLimit && triggerTime < lateLimit)
                secondsToTrigger = (lateLimit - DateTime.Now).TotalSeconds;

            triggerTime = DateTime.Now.AddSeconds(Math.Max(1800, secondsToTrigger));
            
            if (longestOffline < maxOfflineSeconds)
            {
                if (!LocalNotificationManager.TryGetCopyOfLocalNotificationMeta(2, out LocalNotificationMeta earningsStoppedMeta))
                    return;
                earningsStoppedMeta.TimeSpanFromScheduled = triggerTime - DateTime.Now;
                LocalNotificationManager.ScheduleNotification(earningsStoppedMeta);
                return;
            }
            
            if (!LocalNotificationManager.TryGetCopyOfLocalNotificationMeta(3, out LocalNotificationMeta outpostsFinishedMeta))
                return;
            outpostsFinishedMeta.TimeSpanFromScheduled = triggerTime - DateTime.Now;
            LocalNotificationManager.ScheduleNotification(outpostsFinishedMeta);
        }
    }

}