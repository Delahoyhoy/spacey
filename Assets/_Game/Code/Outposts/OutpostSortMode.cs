namespace Core.Gameplay.Outposts
{
    public enum OutpostSortMode
    {
        Default,
        Distance,
        Yield,
        Earnings,
    }
}