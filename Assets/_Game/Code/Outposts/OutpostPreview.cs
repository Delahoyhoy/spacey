﻿using System;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    public class OutpostPreview : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer lineRenderer;

        [SerializeField]
        private float moveRate = 0.25f;

        private MaterialPropertyBlock propertyBlock;

        private float currentOffset;
        private static readonly int Offset = Shader.PropertyToID("_Offset");

        private void Awake()
        {
            propertyBlock = new MaterialPropertyBlock();
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
            if (!active)
                return;
            transform.eulerAngles = new Vector3(0, 0, transform.position.x > 0 ? 90f : -90f);
        }
        
        private void Update()
        {
            currentOffset += Time.deltaTime * moveRate;
            propertyBlock.SetFloat(Offset, currentOffset);
            lineRenderer.SetPropertyBlock(propertyBlock);
        }
    }
}