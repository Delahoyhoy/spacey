using System;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Augments;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class OutpostInventorySlot : MonoBehaviour
    {
        [SerializeField]
        private Text nameText, levelText, remainingTimeText, perSecondText, massPerMinText;
        
        [SerializeField]
        private Image[] augments = new Image[5];
        
        [SerializeField]
        private GameObject activeDisplay, inactiveDisplay;

        [SerializeField]
        private Color standardColour = Color.white, premiumColour = Color.magenta;

        [SerializeField]
        private UnityEvent<Color> onSetColour;

        private Action onSelect;

        public void Setup(Outpost outpost, bool available, Action<Outpost> selectAction)
        {
            gameObject.SetActive(true);
            inactiveDisplay.SetActive(available);
            activeDisplay.SetActive(!available);

            SetupAugments(outpost);

            if (!available)
            {
                remainingTimeText.text = ScanResults.FormatRemainingTime(outpost.GetTimeRemainingOnTarget());
                perSecondText.text = $"{(outpost.GetTotalGoldPerSecond(false) * 60).FormatAsCurrency()}/m";
            }
            massPerMinText.text = $"{(outpost.GetMassMinedPerSecond() * 60).FormatAsCurrency()} tons/m";
            

            onSetColour?.Invoke(outpost.IsPremium ? premiumColour : standardColour);
            
            levelText.text = $"LEVEL {outpost.Level + 1}";
            nameText.text = outpost.OutpostName;
            onSelect = () => selectAction?.Invoke(outpost);
            transform.SetAsLastSibling();
        }
        
        private void SetupAugments(Outpost outpost)
        {
            int countAvailable = outpost.AugmentsAvailable;
            for (int i = 0; i < augments.Length; i++)
            {
                if (i < countAvailable)
                {
                    augments[i].gameObject.SetActive(true);
                    Augment augment = outpost.GetAugment(i);
                    if (augment != null)
                    {
                        augments[i].sprite = AugmentManager.Instance.GetSymbol(augment.augmentEffect);
                        augments[i].color = ColourHelper.Instance.GetTierColour(augment.augmentTier);
                    }
                    else
                        augments[i].gameObject.SetActive(false);
                }
                else
                    augments[i].gameObject.SetActive(false);
            }
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void Select()
        {
            onSelect?.Invoke();
        }
    }
}