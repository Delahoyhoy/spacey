﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Version;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public class OutpostPrefabVersion
    {
        [SerializeField]
        private List<GameVersion> validVersions;

        [SerializeField]
        private Outpost prefab;

        public Outpost Prefab => prefab;

        public bool GetValid(GameVersion version, out Outpost outpostPrefab)
        {
            outpostPrefab = prefab;
            return validVersions.Contains(version);
        }
    }
}