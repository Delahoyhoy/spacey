using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class OutpostShipSlot : MonoBehaviour
    {
        [SerializeField]
        private Image shipIcon, rewardIcon;

        [SerializeField]
        private UnityEvent onSetActive;

        public void SetupReward(MilestoneReward reward)
        {
            // Initialise reward info
        }
    }
}