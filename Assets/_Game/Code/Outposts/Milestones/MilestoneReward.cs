using System;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public struct MilestoneReward
    {
        public MilestoneRewardType rewardType;
        public int rewardMagnitude;

        public MilestoneReward(MilestoneRewardType type, int magnitude)
        {
            rewardType = type;
            rewardMagnitude = magnitude;
        }
    }
}