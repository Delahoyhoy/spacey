using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Alerts;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Outposts
{
    public class MilestoneRewardHandler : MonoBehaviour
    {
        [SerializeField]
        private MilestoneRewardType rewardType;

        [SerializeField]
        private UnityEvent<int> onConferReward;

        private bool setup;

        private void Start()
        {
            if (setup)
                return;
            MilestoneManager.Instance.ConferReward += ConferReward;
            setup = true;
        }

        private void ConferReward(MilestoneRewardType type, int amount)
        {
            if (type != rewardType)
                return;
            onConferReward?.Invoke(amount);
        }
    }
}
