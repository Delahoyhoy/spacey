using System;
using System.Collections.Generic;
using Data;
using UnityEngine;
using Fumb.Data;
using Fumb.RemoteConfig;
using Core.Gameplay.Outposts.Data;

namespace Core.Gameplay.Outposts
{
    public class MilestoneManager : MonoBehaviour
    {
        private static MilestoneManager instance;
        public static MilestoneManager Instance => instance ??= ObjectFinder.FindObjectOfType<MilestoneManager>();
        
        [SerializeField]
        private Milestone[] milestones;

        [SerializeField]
        private RemoteOutpostMilestoneData milestoneData = new RemoteOutpostMilestoneData("outpostMilestoneValues", 
            new OutpostMilestoneData(5, 14.83333333333f, 120));

        [SerializeField]
        private int recurringMilestone = 1000;

        private IntermediateReward[] intermediateRewards;

        public Action<MilestoneRewardType, int> ConferReward;

        [SerializeField]
        private RemoteBool awardIntermediate = new RemoteBool("awardIntermediateMilestones", false);

        private void Awake()
        {
            instance = this;
        }

        public void PopulateMilestones()
        {
            milestoneData.RegisterListenForConfigChanges();
            milestoneData.SubscribeToValueChangeDetection(Populate);

            void Populate()
            {
                OutpostMilestoneData data = milestoneData.Value;
                milestones = new Milestone[data.Count];
                for (int i = 0; i < data.Count; i++)
                {
                    Milestone newMilestone = new Milestone(data.GetMilestoneLevel(i));
                    milestones[i] = newMilestone;
                }
            }
            Populate();
            intermediateRewards = DataAccessor.GetData<DataAssetIntermediateReward>().Data;
        }
        
        public void PopulateMilestones(OutpostStage[] stages)
        {
            milestones = new Milestone[stages.Length];
            for (int i = 0; i < milestones.Length; i++)
            {
                Milestone newMilestone = new Milestone(stages[i]);
                milestones[i] = newMilestone;
            }

            intermediateRewards = DataAccessor.GetData<DataAssetIntermediateReward>().Data;
        }

        public bool GetAnyIntermediateRewards(int level, out MilestoneReward reward, out float position)
        {
            reward = new MilestoneReward();
            position = 0.5f;

            if (!awardIntermediate.Value)
                return false;
            
            int previousMilestone = GetPreviousMilestoneLevel(level + 1);
            int nextMilestone = GetNextMilestoneLevel(level);
            for (int i = 0; i < intermediateRewards.Length; i++)
            {
                if (intermediateRewards[i].UnlockLevel - 1 < level)
                    continue;
                if (intermediateRewards[i].UnlockLevel - 1 >= nextMilestone)
                    break;
                reward = intermediateRewards[i].Reward;
                position = (float)(intermediateRewards[i].UnlockLevel - previousMilestone) /
                           (nextMilestone - previousMilestone);
                return true;
            }

            return false;
        }

        public void ConferRewards(List<MilestoneReward> rewards)
        {
            for (int i = 0; i < rewards.Count; i++)
            {
                ConferReward?.Invoke(rewards[i].rewardType, rewards[i].rewardMagnitude);
            }
        }

        public int GetMilestonesCrossed(int currentLevel)
        {
            //double totalBonus = 1;
            int adjustedLevel = currentLevel + 1;
            int totalMilestones = 0;
            for (int i = 0; i < milestones.Length; i++)
            {
                if (adjustedLevel < milestones[i])
                    return totalMilestones;
                totalMilestones++;
            }

            double remaining = adjustedLevel;
            while (remaining >= recurringMilestone)
            {
                totalMilestones++;
                remaining -= recurringMilestone;
            }

            return totalMilestones;
        }

        public int GetMilestonesCrossed(int fromLevel, int toLevel, out List<Milestone> crossedMilestones)
        {
            int adjustedStart = fromLevel + 1;
            int adjustedEnd = toLevel + 1;
            crossedMilestones = new List<Milestone>();

            bool startIndexBelowRecurring = false;
            int startIndex = 0;
            for (int i = 0; i < milestones.Length; i++)
            {
                if (milestones[i].milestoneValue <= adjustedStart)
                    continue;
                startIndex = i;
                startIndexBelowRecurring = true;
                break;
            }

            if (!startIndexBelowRecurring)
            {
                int recurringMilestonesCrossed = 0;
                double startRemainder = adjustedStart;
                while (startRemainder >= recurringMilestone)
                {
                    recurringMilestonesCrossed++;
                    startRemainder -= recurringMilestone;
                }

                int totalCrossedBetweenLevels = 0;
                double endRemainder = adjustedEnd - (recurringMilestonesCrossed * recurringMilestone);
                while (endRemainder >= recurringMilestone)
                {
                    totalCrossedBetweenLevels++;
                    recurringMilestonesCrossed++;
                    endRemainder -= recurringMilestone;
                    crossedMilestones.Add(new Milestone(milestones[milestones.Length - 1].milestoneValue + 
                                                        recurringMilestone * recurringMilestonesCrossed));
                }

                return totalCrossedBetweenLevels;
            }
            
            int totalMilestones = 0;
            for (int i = startIndex; i < milestones.Length; i++)
            {
                if (adjustedEnd < milestones[i])
                    return totalMilestones;
                totalMilestones++;
                crossedMilestones.Add(milestones[i]);
            }

            double remaining = adjustedEnd;
            int recurredMilestones = 0;
            while (remaining >= recurringMilestone)
            {
                totalMilestones++;
                recurredMilestones++;
                remaining -= recurringMilestone;
                crossedMilestones.Add(new Milestone(milestones[milestones.Length - 1].milestoneValue + 
                                                    recurringMilestone * recurredMilestones));
            }

            return totalMilestones;
        }

        public bool GetIsMilestone(int currentLevel, out Milestone? milestone)
        {
            int adjustedLevel = currentLevel + 1;
            milestone = null;
            for (int i = 0; i < milestones.Length; i++)
            {
                if (adjustedLevel < milestones[i])
                    return false;
                if (adjustedLevel == milestones[i])
                {
                    milestone = milestones[i];
                    return true;
                }
            }
            bool isRecurringMilestone = currentLevel % recurringMilestone == 0;

            if (isRecurringMilestone)
                milestone = new Milestone(currentLevel);
            
            return isRecurringMilestone;
        }

        public bool HasCrossedMilestone(int fromLevel, int toLevel, out List<Milestone> crossedMilestones)
        {
            return GetMilestonesCrossed(fromLevel, toLevel, out crossedMilestones) > 0;
        }

        public float GetMilestoneProgress(int currentLevel)
        {
            int adjustedLevel = currentLevel + 1;
            if (adjustedLevel < milestones[0])
            {
                return (adjustedLevel / (float) GetNextMilestoneLevel(currentLevel));
            }
            else
            {
                float previousMilestone = GetPreviousMilestoneLevel(adjustedLevel);
                return ((adjustedLevel - previousMilestone) / (GetNextMilestoneLevel(currentLevel) - previousMilestone));
            }
        }

        public int GetLevelsToNextMilestone(int currentLevel)
        {
            int milestoneLvl = GetNextMilestoneLevel(currentLevel);
            return milestoneLvl - currentLevel;
        }

        private int GetPreviousMilestoneLevel(int currentLevel)
        {
            if (currentLevel >= recurringMilestone)
            {
                return Mathf.FloorToInt((float) currentLevel / recurringMilestone) * recurringMilestone;
            }

            for (int i = milestones.Length - 1; i >= 0; i--)
            {
                if (currentLevel >= milestones[i])
                    return milestones[i];
            }

            return milestones[0];
        }

        public List<MilestoneReward> GetIntermediateRewardsCrossed(int fromLevel, int toLevel)
        {
            List<MilestoneReward> intermediateRewardsEarned = new List<MilestoneReward>();
            if (!awardIntermediate.Value)
                return intermediateRewardsEarned;
            for (int i = 0; i < intermediateRewards.Length; i++)
            {
                if (intermediateRewards[i].UnlockLevel - 1 < fromLevel)
                    continue;
                if (intermediateRewards[i].UnlockLevel - 1 >= toLevel)
                    break;
                intermediateRewardsEarned.Add(intermediateRewards[i].Reward);
            }

            return intermediateRewardsEarned;
        }

        private int GetNextMilestoneLevel(int currentLevel)
        {
            int adjustedLevel = currentLevel + 1;
            for (int i = 0; i < milestones.Length; i++)
            {
                if (adjustedLevel < milestones[i])
                    return milestones[i] - 1;
            }

            if (adjustedLevel == recurringMilestone)
            {
                return recurringMilestone * 2 - 1;
            }

            int returnVal = (Mathf.CeilToInt((float) adjustedLevel / recurringMilestone) * recurringMilestone) - 1;
            if (returnVal < adjustedLevel)
            {
                return adjustedLevel + recurringMilestone - 1;
            }

            return returnVal;
        }
    }
}