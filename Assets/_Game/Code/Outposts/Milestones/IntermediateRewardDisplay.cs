﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class IntermediateRewardDisplay : MonoBehaviour
    {
        [SerializeField]
        private Image icon;

        private RectTransform rectTransform;
        private bool initialised;

        private void Initialise()
        {
            if (initialised)
                return;
            rectTransform = (RectTransform)transform;
            initialised = true;
        }

        public void Setup(MilestoneRewardType rewardType, float position)
        {
            gameObject.SetActive(true);
            Initialise();
            Vector2 anchor = new Vector2(position, 0.5f);

            rectTransform.anchorMin = rectTransform.anchorMax = anchor;
            rectTransform.anchoredPosition = Vector2.zero;
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}