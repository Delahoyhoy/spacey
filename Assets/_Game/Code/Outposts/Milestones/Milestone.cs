using System;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public struct Milestone
    {
        public int milestoneValue;
        public MilestoneReward[] milestoneRewards;

        public bool HasReward => milestoneRewards.Length > 0;

        public static implicit operator int(Milestone milestone)
        {
            return milestone.milestoneValue;
        }

        public Milestone(int value)
        {
            milestoneValue = value;
            milestoneRewards = Array.Empty<MilestoneReward>();
        }

        public Milestone(OutpostStage stage)
        {
            milestoneValue = stage.UnlockLevel;
            milestoneRewards = new[] { new MilestoneReward(stage.StageReward, stage.StageRewardAmount) };
        }
    }
}