﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Augments;
using Core.Gameplay.Economy;
using Core.Gameplay.Reset;
using Core.Gameplay.Sectors;
using UnityEngine;
using UnityEngine.Events;
using Core.Gameplay.Ships;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Outposts
{
    public class Outpost : MonoBehaviour
    {
        private MiningShip[] ships = new MiningShip[12];

        private int asteroidIndex, index;

        private string outpostName = "Outpost #1";

        public string OutpostName => outpostName;
        public int OutpostIndex => index;
        public int EffectiveIndex => OutpostManager.Instance.GetEffectiveIndex(this);

        public int UpgradeIndex => !IsPremium ? index : 1 + index * 2;

        private double ValueMult => OutpostManager.Instance.GetEffectiveValueAtLevel(Level) * 
                                    OutpostManager.Instance.GetValueMultForIndex(EffectiveIndex);

        private bool hasTarget;
        public bool HasTarget => hasTarget && asteroidIndex >= 0;

        public bool IsMining => HasTarget && asteroid && asteroid.State != AsteroidState.Finished;
        
        public int AsteroidIndex => asteroidIndex;
        private Asteroid asteroid;

        private Augment[] augments = new Augment[5];

        [SerializeField]
        private Transform hub;

        [SerializeField]
        private int currentLevel = 0;

        private double[] transitLoad;

        public int Level => currentLevel;

        // Current level adjusted to start from level 1 (instead of 0)
        private int AdjustedLevel => currentLevel + 1;

        [SerializeField]
        private Transform[] dockingPoints;

        [SerializeField]
        private Transform cargoShip;

        [SerializeField]
        private BezierPoint cargoBay, mainShip;

        [SerializeField]
        private PathLine cargoPath;

        public MineralLoad cargo;

        [SerializeField]
        private LineRenderer laser;

        [SerializeField]
        private Transform laserHit;

        private float cargoSpeed = 2f;

        private float totalDistance = 5f;

        public double mineSpeedMult = 1d, speedMult = 1d, capacityMult = 1d, efficiencyMult = 1d, cargoMult = 1d;

        private ModifierEffect sectorMineMult = new ModifierEffect(SectorModifier.MiningSpeed),
            sectorShipSpeed = new ModifierEffect(SectorModifier.ShipSpeed);

        public double MineSpeedMult => GetMineSpeedMultAtLevel(currentLevel);
        public double SpeedMult => speedMult * sectorShipSpeed.Value;
        public double CapacityMult => GetCapacityMultAtLevel(currentLevel);
        public double EfficiencyMult => efficiencyMult;
        public double CargoMult => GetCargoMultAtLevel(currentLevel);
        private float bonusSpeedMult = 1;

        [SerializeField]
        private UnityEvent onReleaseCargo;

        [SerializeField]
        private UnityEvent<float> onSetCargoFillAmount;

        [SerializeField]
        private UnityEvent<bool> onSetCargoFull;
        
        [SerializeField]
        private UnityEvent<bool> onSetIdle;

        [SerializeField]
        private bool isPremium;

        private bool setup;

        public bool IsPremium => isPremium;

        private void Start()
        {
            if (setup)
                return;
            SetBonusMult(GameManager.Instance.BonusMultiplier);
            GameManager.OnSetBonusActive += SetBonusMult;
            setup = true;
        }

        private void OnDestroy()
        {
            if (setup)
                GameManager.OnSetBonusActive -= SetBonusMult;
        }

        private void SetBonusMult(double multiplier)
        {
            bonusSpeedMult = (float)multiplier;
        }

        private double GetMineSpeedMultAtLevel(int level)
        {
            return (GameVersionManager.CurrentVersion >= GameVersion.Version3 ? OutpostManager.Instance.GetEffectiveValueAtLevel(level) * 
                       OutpostManager.Instance.GetValueMultForIndex(EffectiveIndex) * mineSpeedMult : mineSpeedMult) * 
                   sectorMineMult.Value;
        }

        private double GetBaseMineSpeedMultAtLevel(int level)
        {
            return (GameVersionManager.CurrentVersion >= GameVersion.Version3 ? OutpostManager.Instance.GetEffectiveValueAtLevel(level) * 
                       OutpostManager.Instance.GetValueMultForIndex(EffectiveIndex) : 1f) * sectorMineMult.Value;
        }

        private double GetCargoMultAtLevel(int level)
        {
            return GameVersionManager.CurrentVersion >= GameVersion.Version3 ? OutpostManager.Instance.GetEffectiveValueAtLevel(level) * 
                                                                               OutpostManager.Instance.GetValueMultForIndex(EffectiveIndex) * cargoMult : cargoMult;
        }
        
        private double GetCapacityMultAtLevel(int level)
        {
            return GameVersionManager.CurrentVersion >= GameVersion.Version3 ? OutpostManager.Instance.GetEffectiveValueAtLevel(level) * 
                                                                               OutpostManager.Instance.GetValueMultForIndex(EffectiveIndex) * capacityMult : capacityMult;
        }

        private Coroutine handleCargo, miningLaserLoop;

        private bool cargoInterrupt;

        private int[] miningShips = new int[]
        {
            -1, -1, -1, -1,
            -1, -1, -1, -1,
            -1, -1, -1, -1
        };

        public int GetShipAtIndex(int index)
        {
            if (index >= miningShips.Length)
                return -1;
            return miningShips[index];
        }

        public void OpenMenu()
        {
            OutpostManager.Instance.OpenOutpostMenu(this);
        }

        public Augment[] GetAugments()
        {
            return augments;
        }

        public Augment GetAugment(int index)
        {
            if (index >= augments.Length)
                return null;
            return augments[index];
        }

        public void RemoveAugment(int index)
        {
            if (index >= augments.Length)
                return;
            augments[index] = null;
            SetupAugments();
        }

        public void SetAugment(int index, Augment augment)
        {
            if (index >= augments.Length)
                return;
            augments[index] = augment;
            SetupAugments();
        }

        private void SetupAugments()
        {
            double capStart = capacityMult;
            // Switch case incoming
            mineSpeedMult = 1d;
            speedMult = 1d;
            capacityMult = 1d;
            efficiencyMult = 1d;
            for (int i = 0; i < augments.Length; i++)
            {
                if (augments[i] == null)
                    continue;
                double magnitude =
                    AugmentManager.Instance.GetMagnitude(augments[i].augmentEffect, augments[i].augmentTier) / 100d;
                switch (augments[i].augmentEffect)
                {
                    case AugmentEffect.Capacity:
                        capacityMult += magnitude;
                        RefreshCargo();
                        break;
                    case AugmentEffect.Efficiency:
                        efficiencyMult += magnitude;
                        break;
                    case AugmentEffect.FlySpeed:
                        speedMult += magnitude;
                        break;
                    case AugmentEffect.MineSpeed:
                        mineSpeedMult += magnitude;
                        break;
                }
            }

            if (Math.Abs(capacityMult - capStart) > 0.00001d)
            {
                for (int i = 0; i < miningShips.Length; i++)
                    if (miningShips[i] >= 0)
                        ships[i].RefreshCapacity();
            }
        }

        private bool waitingForShips, cargoEnRoute;
        
        private IEnumerator HandleCargo()
        {
            while (!cargo.Full)
            {
                if (waitingForShips)
                {
                    bool allShipsEmpty = true;
                    for (int i = 0; i < miningShips.Length; i++)
                    {
                        if (miningShips[i] < 0)
                            continue;
                        if (!ships[i].HasCargo)
                            continue;
                        allShipsEmpty = false;
                        break;
                    }
                    if (allShipsEmpty)
                    {
                        waitingForShips = false;
                        onSetIdle?.Invoke(!IsMining);
                        break;
                    }
                }
                
                yield return null;
            }

            onReleaseCargo?.Invoke();

            cargoInterrupt = false;
            RefreshCargo();
            transitLoad = cargo.GetLoad();
            cargo.Reset();
            
            cargoShip.position = cargoBay.Point;
            cargoShip.gameObject.SetActive(true);
            Vector3 lastPos = cargoShip.position;
            BezierPath bezierPath = cargoPath.GetPath();
            Vector3 pos = cargoBay.Point;

            cargoEnRoute = true;
            
            do
            {
                if (ResetManager.Instance.resetting)
                {
                    while (ResetManager.Instance.resetting)
                        yield return null;
                }

                lastPos = cargoShip.position;
                cargoShip.position = pos;
                cargoShip.up = cargoShip.position - lastPos;
                if (cargoInterrupt)
                    break;

                //Debug.Log("Distance travelled: " + Vector3.Distance(lastPos, cargoShip.position));
                yield return null;
            } while (!bezierPath.TraversePath(
                cargoSpeed * (float) SpeedMult * bonusSpeedMult * Time.deltaTime * (float) GameManager.Instance.cargoShipSpeedMult,
                out pos));

            cargoEnRoute = false;
            
            cargoShip.gameObject.SetActive(false);
            if (cargoInterrupt)
            {
                handleCargo = StartCoroutine(HandleCargo());
                yield break;
            }

            double total = 0;
            for (int i = 0; i < transitLoad.Length; i++)
                total += transitLoad[i] * TierHelper.GetValue((Tier) i);
            CurrencyController.Instance.AddCurrency(total * GameManager.Instance.IncomeMultiplier, CurrencyType.SoftCurrency,
                true, "mining", "passive");
            AudioManager.Play("Coin", 0.1f);
            handleCargo = StartCoroutine(HandleCargo());
        }

        public void StopCargo()
        {
            if (handleCargo != null)
                StopCoroutine(handleCargo);
            cargoShip.gameObject.SetActive(false);
        }

        public float GetPercentageFull()
        {
            return (float) (cargo.TotalMinerals / cargo.maxCapacity);
        }

        public double GetMaxCapacity()
        {
            return cargo.maxCapacity;
        }

        public double GetCurrentFill()
        {
            return cargo.TotalMinerals;
        }

        public void SetFillAmount(float fillAmount)
        {
            onSetCargoFillAmount?.Invoke(fillAmount);
        }

        public void SetFull(bool full)
        {
            onSetCargoFull?.Invoke(full);
        }

        private void EmitBuildParticles()
        {
            if (GameVersionManager.CurrentVersion < GameVersion.Version3)
            {
                EffectManager.Instance.EmitAt("buildMain", hub.position, 50);
                EffectManager.Instance.EmitAt("buildCross", hub.position, hub.rotation, 30);
                return;
            }
            EffectManager.Instance.EmitAt("buildNew", hub.position, 30);
        }

        public void Setup(int asteroidNumber, Asteroid asteroidRef, int outpostIndex, double cargoCapacity)
        {
            asteroidIndex = asteroidNumber;
            asteroid = asteroidRef;
            index = outpostIndex;
            hasTarget = true;
            
            outpostName = isPremium ? $"Station #{(outpostIndex + 1)}" : $"Outpost #{(outpostIndex + 1)}";
            miningShips = new int[]
            {
                -1, -1, -1, -1,
                -1, -1, -1, -1,
                -1, -1, -1, -1
            };
            mainShip = GameManager.Instance.GetDockingBay(transform.position);
            
            mainShip = GameManager.Instance.GetDockingBay(transform.position);

            EmitBuildParticles();
            
            cargo = new MineralLoad(cargoCapacity, SetFillAmount, SetFull);
            if (hub.position.x > 0)
                hub.eulerAngles = new Vector3(0, 0, 90f);
            else
                hub.eulerAngles = new Vector3(0, 0, -90f);
            
            AudioManager.Play("Build", 0.5f);
            
            if (miningLaserLoop != null)
                StopCoroutine(miningLaserLoop);
            miningLaserLoop = StartCoroutine(LaserTargetLoop());
            
            if (!asteroidRef)
            {
                hasTarget = false;
                OutpostManager.Instance.SetOutpostAvailable(OutpostIndex, IsPremium);
                cargoPath.gameObject.SetActive(false);
                return;
            }
            
            cargoPath.gameObject.SetActive(true);
            
            if (cargoPath)
            {
                cargoPath.Setup(cargoBay, mainShip);
            }
            
            totalDistance = PathLine.GetTotalDistance(cargoBay, mainShip);
            
            
            if (asteroidRef.State == AsteroidState.Finished)
            {
                OutpostManager.Instance.SetOutpostAvailable(OutpostIndex, IsPremium);
                return;
            }
            
            asteroidRef.chunkCompleteAction += OnAsteroidComplete;
        }

        public void PopIn()
        {
            gameObject.SetActive(true);
            EmitBuildParticles();
            AudioManager.Play("Build", 0.5f);
        }

        private void OnAsteroidComplete()
        {
            waitingForShips = true;
            OutpostManager.Instance.SetOutpostAvailable(OutpostIndex, IsPremium);
        }
        
        public TimeSpan GetTimeRemainingOnTarget()
        {
            return new TimeSpan(0, 0,
                (int)asteroid.GetApproxTimeRemaining(GetTotalMinedPerSecond(), GetAverageEfficiency()));
        }

        public void SetInactive()
        {
            gameObject.SetActive(false);
            hasTarget = false;
            asteroid = null;
            asteroidIndex = -1;
            OutpostManager.Instance.SetOutpostAvailable(OutpostIndex, IsPremium);
            onSetIdle?.Invoke(false);
        }

        public void HidePath()
        {
            cargoPath.SetActive(false);
            ConferAnyRemainingCargo();
            if (cargoEnRoute)
            {
                cargoShip.gameObject.SetActive(false);
                TapCargo();
            }
        }

        public void SetTarget(Asteroid target)
        {
            onSetIdle?.Invoke(false);
            gameObject.SetActive(true);
            ConferAnyRemainingCargo();
            if (cargoEnRoute)
            {
                cargoShip.gameObject.SetActive(false);
                TapCargo();
            }

            if (asteroid)
                asteroid.chunkCompleteAction -= OnAsteroidComplete;
            hasTarget = true;
            asteroid = target;
            asteroidIndex = target.Index;

            transform.parent = asteroid.outpostAnchor;
            transform.localPosition = Vector3.zero;
            OutpostManager.Instance.RefreshButtonLocation(OutpostIndex, IsPremium);

            mainShip = GameManager.Instance.GetDockingBay(transform.position);
            
            cargoPath.gameObject.SetActive(true);
            if (hub.position.x > 0)
                hub.eulerAngles = new Vector3(0, 0, 90f);
            else
                hub.eulerAngles = new Vector3(0, 0, -90f);
            if (cargoPath)
                cargoPath.Setup(cargoBay, mainShip);
            EmitBuildParticles();
            AudioManager.Play("Build", 0.5f);
            totalDistance = PathLine.GetTotalDistance(cargoBay, mainShip);
            asteroid.State = AsteroidState.OutpostBuilt;
            
            if (asteroid.State == AsteroidState.Finished)
            {
                OutpostManager.Instance.SetOutpostAvailable(OutpostIndex, IsPremium);
                return;
            }

            for (int i = 0; i < ships.Length; i++)
            {
                if (miningShips[i] >= 0)
                {
                    ships[i].WakeShip(asteroid);
                }
            }
            ActivateOutpost();
            asteroid.chunkCompleteAction += OnAsteroidComplete;
        }

        public int GetNextUnlockIndex()
        {
            int baseIndex = miningShips[0];
            for (int i = 1; i < miningShips.Length; i++)
            {
                if (miningShips[i] < baseIndex)
                    return baseIndex;
            }

            return baseIndex + 1;
        }

        private void ConferAnyRemainingCargo()
        {
            double totalValue = cargo.TotalValue;
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (miningShips[i] < 0)
                    continue;
                totalValue += ships[i].TotalMineralValue;
            }
            if (totalValue > 0)
                CurrencyController.Instance.AddCurrency(totalValue, CurrencyType.SoftCurrency, false, "mining", "passive");
        }

        public void TapCargo()
        {
            if (!cargoShip.gameObject.activeSelf)
                return;
            double total = 0;
            for (int i = 0; i < transitLoad.Length; i++)
                total += transitLoad[i] * TierHelper.GetValue((Tier) i);
            cargoInterrupt = true;
            CoinBurster.Instance.NewBurstFromWorld(CurrencyType.SoftCurrency, cargoShip, Random.Range(6, 10), total);
            cargoEnRoute = false;
            //CurrencyController.Instance.AddCurrency(total * GameManager.Instance.IncomeMultiplier, CurrencyType.Gold, true);
        }

        public void ClearCargo()
        {
            ConferAnyRemainingCargo();
            if (cargoEnRoute)
                TapCargo();
            cargoShip.gameObject.SetActive(false);
        }

        public void Mine(double efficiency, double amount, ref double output)
        {
            double amountToMine = GameVersionManager.CurrentVersion != GameVersion.Version1 ?
                Math.Max(GetMassMinedPerSecond() / 100d, 2d) :
                amount;
            output += asteroid.MineAmount(amountToMine, efficiency);
            EffectManager.Instance.EmitAt(asteroid.RockID, laserHit.transform.position, 1);
        }
        
        public double SimulateOfflineTime(double offlineTime)
        {
            MineralLoad load = new MineralLoad(double.MaxValue);
            if (!HasTarget || asteroid.State == AsteroidState.Finished)
                return 0;
            asteroid.Mine(GetTotalMinedPerSecond() * offlineTime, GetAverageEfficiency(), ref load);
            return load.TotalValue;
        }

        public void UpdateLaser(float percent)
        {
            if (percent <= 0)
            {
                if (laser.gameObject.activeSelf)
                    laser.gameObject.SetActive(false);
                if (laserHit.gameObject.activeSelf)
                    laserHit.gameObject.SetActive(false);
            }
            else
            {
                if (!laser.gameObject.activeSelf)
                    laser.gameObject.SetActive(true);
                if (!laserHit.gameObject.activeSelf)
                    laserHit.gameObject.SetActive(true);
                Vector3[] positions = new Vector3[3];
                positions[0] = turret.position;
                positions[1] = Vector3.Lerp(turret.position, currentTarget, 0.5f);
                positions[2] = currentTarget;
                laser.SetPositions(positions);
                laserHit.transform.position = currentTarget;
                laserHit.Rotate(0, 0, Time.deltaTime * 540f, Space.World);
                laser.widthMultiplier = 0.075f * percent * (1f + (Mathf.Sin(Time.time * 20f) * 0.125f));
                laserHit.transform.localScale = new Vector3(0.33f, 0.33f, 0.33f) * percent;
            }
        }

        public double GetTotalGoldPerSecond(bool includeMinIfNotMining)
        {
            if (!HasTarget || asteroid.State == AsteroidState.Finished)
                return includeMinIfNotMining ? GetTotalMinedPerSecond() : 0;

            double valueMined = GetGoldPerSecond();
            double massMined = GetMassMinedPerSecond();
            if (massMined <= 0)
                return 0;
            
            double timeToFill = cargo.maxCapacity / massMined;
            double timeToTransport = totalDistance / (cargoSpeed * GameManager.Instance.cargoShipSpeedMult * 
                                                      SpeedMult * bonusSpeedMult);
            if (timeToTransport <= timeToFill)
                return valueMined;
            double ratio = 1 - ((timeToTransport - timeToFill) / timeToTransport);
            return valueMined * ratio;
        }

        public double GetTotalGoldPerSecondAtLevel(int level)
        {
            if (!HasTarget)
                return 0;
            if (asteroid.State == AsteroidState.Finished)
                return 0;
            
            GetLevelDetails(level, out double totalGold, out double totalMass, out double totalCapacity);
            if (totalMass <= 0)
                return 0;
            double timeToFill = totalCapacity / totalMass;
            // FUTURE_VERSION Re-add * speedMult to timeToTransport divisor
            double timeToTransport = totalDistance / (cargoSpeed * GameManager.Instance.cargoShipSpeedMult * 
                                                      SpeedMult * bonusSpeedMult);
            if (timeToTransport <= timeToFill)
                return totalGold;
            double ratio = 1 - ((timeToTransport - timeToFill) / timeToTransport);
            return totalGold * ratio;
        }

        private bool GetBottleneck(double perSecond, out double bottleneck)
        {
            bottleneck = 1f;
            if (perSecond <= 0)
                return false;
            double timeToFill = cargo.maxCapacity / perSecond;
            double timeToTransport = totalDistance / (cargoSpeed * GameManager.Instance.cargoShipSpeedMult * SpeedMult * 
                                                      bonusSpeedMult);
            
            if (timeToTransport <= timeToFill)
                return false;
            bottleneck = 1 - ((timeToTransport - timeToFill) / timeToTransport);
            return true;
        }

        public double GetMassMinedPerSecond()
        {
            double total = 0;
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (miningShips[i] < 0)
                    continue;
                total += ShipManager.Instance.GetMiningSpeed(miningShips[i]) * MineSpeedMult *
                         GameManager.Instance.globalMineSpeedMult * GameManager.Instance.BonusMultiplier;
            }

            return total / 3f;
        }

        public double GetBaseMassMinedPerSecond()
        {
            double total = 0;
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (miningShips[i] < 0)
                    continue;
                total += ShipManager.Instance.GetMiningSpeed(miningShips[i]) * GetMineSpeedMultAtLevel(currentLevel);
            }

            return total / 3f;
        }
        
        public double GetTotalMinedPerSecond()
        {
            double mined = GetMassMinedPerSecond();
            if (!GetBottleneck(mined, out double bottleneck))
                return mined;
            return mined * bottleneck;
        }

        private double GetAverageEfficiency()
        {
            double efficiency = 0;
            int total = 0;
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (miningShips[i] < 0)
                    continue;
                total++;
                efficiency += ShipManager.Instance.GetEfficiency(miningShips[i]) * efficiencyMult *
                              GameManager.Instance.efficiencyMult;
            }

            if (total <= 0)
                return 0;
            return efficiency / total;
        }

        public float[] GetNormalisedCargo()
        {
            return cargo.GetFloatComposition();
        }

        public string Save()
        {
            string data = asteroidIndex.ToString() + "^" + currentLevel.ToString() + "^" + outpostName + "^";
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (i > 0)
                    data += ",";
                data += miningShips[i].ToString();
            }

            data += "^" + SaveAugments();
            return data;
        }

        public int MaximumLevel => GameVersionManager.CurrentVersion switch
        {
            GameVersion.Version1 => miningShips.Length,
            GameVersion.Version2 => ShipManager.Instance.GetTotalShipTypes() * miningShips.Length,
            _ => int.MaxValue,
        };

        public bool IsMaxLevel => currentLevel >= MaximumLevel - 1;

        public double GetUpgradeCost()
        {
            return OutpostManager.Instance.OutpostCost(UpgradeIndex, currentLevel + 1);
        }

        private Vector3 currentTarget = Vector3.zero;

        [SerializeField]
        private Transform turret;

        private IEnumerator LaserTargetLoop()
        {
            if (GameVersionManager.CurrentVersion >= GameVersion.Version3)
            {
                turret.gameObject.SetActive(false);
                yield break;
            }
            currentTarget = asteroid.transform.position;
            while (true)
            {
                Vector3 startTarget = currentTarget;
                Vector3 newTarget = asteroid.GetRandomLaserPos();
                float totalDistance = Vector3.Distance(currentTarget, newTarget);
                if (totalDistance <= 0)
                    continue;
                for (float f = 0; f < 1f; f += (Time.deltaTime * 0.25f) / totalDistance)
                {
                    currentTarget = Vector3.Lerp(startTarget, newTarget, f);
                    turret.eulerAngles = new Vector3(0, 0, GetAngleBetween(turret.position, currentTarget));
                    yield return null;
                }
            }
        }

        private float GetAngleBetween(Vector3 from, Vector3 to)
        {
            float dx = to.x - from.x, dy = to.y - from.y;
            float result = (Mathf.Atan2(dy, dx) * Mathf.Rad2Deg) - 90f;
            return result;
        }

        public double GetGoldPerSecond()
        {
            double total = 0;
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (miningShips[i] >= 0)
                    total += ships[i].GetGoldPerSecond();
            }

            return total * GameManager.Instance.GlobalMultiplier;
        }

        public bool GetCurrentlyMining()
        {
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (miningShips[i] >= 0)
                    return true;
            }

            return false;
        }

        private void SetupAtLevel(int level, bool fromUpgrade)
        {
            double totalCapacity = 0;
            int actualLevel = level + 1;
            int shipsAvailable = actualLevel < miningShips.Length ? actualLevel : miningShips.Length;
            int highest = 0;
            int[] newShips = GetShipsAtLevel(level);
            bool anyChange = false;
            // for (int i = 0; i < miningShips.Length; i++) 
            // {
            //     int current = miningShips[i];
            //     if (i >= shipsAvailable)
            //         miningShips[i] = -1;
            //     else
            //     {
            //         int minIndex = (actualLevel / miningShips.Length) - 1;
            //         int selectedIndex = i < actualLevel % miningShips.Length ? minIndex + 1 : minIndex;
            //         if (selectedIndex > highest)
            //             highest = selectedIndex;
            //         miningShips[i] = selectedIndex;
            //         totalCapacity += ShipManager.Instance.GetCapacity(selectedIndex);
            //     }
            //
            //     if (miningShips[i] < 0 || miningShips[i] != current)
            //         ships[i].Setup(miningShips[i]);
            // }
            
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (newShips[i] >= 0 && newShips[i].Equals(miningShips[i]))
                    continue;
                if (newShips[i] >= 0)
                    anyChange = true;
                if (newShips[i] > highest)
                    highest = newShips[i];
                ships[i].SetTarget(asteroid);
                ships[i].Setup(newShips[i]);
            }

            miningShips = newShips;
            
            for (int i = 0; i < miningShips.Length; i++)
                if (miningShips[i] >= 0)
                    ships[i].RefreshCapacity();

            if (fromUpgrade && anyChange)
                GameManager.Instance.UnlockShip(highest);

//            double finalMult = GameVersionManager.CurrentVersion > GameVersion.Version2 ? 1d : 1.5d;
            RefreshCargo();
        }

        private int[] GetShipsAtLevel(int level)
        {
            int actualLevel = level + 1;

            if (GameVersionManager.CurrentVersion == GameVersion.Version3)
                actualLevel = MilestoneManager.Instance.GetMilestonesCrossed(level) + 1;
            
            int shipsAvailable = actualLevel < miningShips.Length ? actualLevel : miningShips.Length;
            int[] shipsAtLevel = new int[miningShips.Length];
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (i >= shipsAvailable)
                    shipsAtLevel[i] = -1;
                else
                {
                    int minIndex = (actualLevel / shipsAtLevel.Length) - 1;
                    int selectedIndex = Mathf.Min(i < actualLevel % shipsAtLevel.Length ? minIndex + 1 : minIndex, 
                        ShipManager.Instance.TotalShipTemplates);
                    shipsAtLevel[i] = selectedIndex;
                    //totalCapacity += ShipManager.Instance.GetCapacity(selectedIndex);
                }
            }


            return shipsAtLevel;
        }

        public void GetLevelDetails(int level, out double totalGold, out double totalMass, out double totalCapacity)
        {
            totalGold = totalMass = totalCapacity = 0;
            int[] nextLevelShips = GetShipsAtLevel(level);
            for (int i = 0; i < nextLevelShips.Length; i++)
            {
                if (nextLevelShips[i] < 0)
                    continue;
                totalGold += MiningShip.GetGoldPerSecond(nextLevelShips[i], asteroid, this);
                totalMass += ShipManager.Instance.GetMiningSpeed(nextLevelShips[i]);
                totalCapacity += ShipManager.Instance.GetCapacity(nextLevelShips[i]);
            }

            double mineRate = GetMineSpeedMultAtLevel(level); 
            
            totalMass *= (mineRate * GameManager.Instance.globalMineSpeedMult * bonusSpeedMult) / 3d;
            totalGold *= mineRate * GameManager.Instance.GlobalMultiplier * bonusSpeedMult;
            totalCapacity *= 1.5d * GetCargoMultAtLevel(level) * capacityMult;
        }

        public int RetreatShips(Action<bool> onComplete)
        {
            cargoPath.gameObject.SetActive(false);
            cargoShip.gameObject.SetActive(false);
            int total = 0;
            for (int i = 0; i < ships.Length; i++)
            {
                if (miningShips[i] >= 0)
                {
                    ships[i].RetreatShip(onComplete);
                    total++;
                }
            }

            return total;
        }

        public void RefreshCargo()
        {
            if (GameVersionManager.CurrentVersion == GameVersion.Version1)
                return;

            double totalCapacity = 0;
            for (int i = 0; i < miningShips.Length; i++)
            {
                if (miningShips[i] >= 0)
                    totalCapacity += ShipManager.Instance.GetCapacity(miningShips[i]);
            }

            double finalMult = GameVersionManager.CurrentVersion > GameVersion.Version2 ? 1d : 1.5d;
            double total = totalCapacity * finalMult * CargoMult * capacityMult;
            if (total > cargo.maxCapacity)
                cargo.SetCapacity(total);
        }

        public void Load(string data, int outpostIndex, double cargoCapacity)
        {
            index = outpostIndex;
            string[] splitData = data.Split('^');
            if (int.TryParse(splitData[0], out asteroidIndex))
            {
                hasTarget = asteroidIndex >= 0;
                if (hasTarget)
                    asteroid = GameManager.Instance.GetAsteroid(asteroidIndex);
                else
                    SetInactive();
            }
            else
            {
                hasTarget = false;
                SetInactive();
            }
            int.TryParse(splitData[1], out currentLevel);
            outpostName = splitData[2];
            if (GameVersionManager.CurrentVersion == GameVersion.Version1)
            {
                string[] shipData = splitData[3].Split(',');
                for (int i = 0; i < shipData.Length; i++)
                {
                    int.TryParse(shipData[i], out miningShips[i]);
                }
            }
            mainShip = GameManager.Instance.GetDockingBay(transform.position);
            cargo = new MineralLoad(cargoCapacity, SetFillAmount, SetFull);
            if (hub.position.x > 0)
                hub.eulerAngles = new Vector3(0, 0, 90f);
            if (cargoPath)
                cargoPath.Setup(cargoBay, mainShip);
            totalDistance = PathLine.GetTotalDistance(cargoBay, mainShip);
            if (splitData.Length >= 5)
            {
                LoadAugments(splitData[4]);
            }

            if (miningLaserLoop != null)
                StopCoroutine(miningLaserLoop);
            miningLaserLoop = StartCoroutine(LaserTargetLoop());
            
            if (!HasTarget || asteroid.State == AsteroidState.Finished)
            {
                OutpostManager.Instance.SetOutpostAvailable(OutpostIndex, IsPremium);
                onSetIdle?.Invoke(true);
                return;
            }
            
            asteroid.chunkCompleteAction += OnAsteroidComplete;
            onSetIdle?.Invoke(!IsMining);
        }

        public void RefreshPosition()
        {
            if (hub.position.x > 0)
                hub.eulerAngles = new Vector3(0, 0, 90f);
            if (cargoPath)
                cargoPath.Setup(cargoBay, mainShip);
            totalDistance = PathLine.GetTotalDistance(cargoBay, mainShip);
        }

        public void CheckStatus()
        {
            //bool mining = false;
            //for (int i = 0; i < ships.Length; i++)
            //{
            //    if (ships[i].IsMining)
            //    {
            //        mining = true;
            //        break;
            //    }
            //}
            //asteroid.SetMining(mining);
        }

        private bool miningShipsSetup = false;

        public void SetupMiningShips(MiningShip miningShipPrefab)
        {
            if (!miningShipsSetup)
            {
                for (int i = 0; i < miningShips.Length; i++)
                {
                    MiningShip newShip = Instantiate(miningShipPrefab, transform);
                    GameObject miningPoint = new GameObject("MiningPoint" + (i + 1));
                    miningPoint.transform.parent = transform;
                    miningPoint.transform.localPosition = Vector3.zero;
                    newShip.Setup(miningShips[i], dockingPoints[i], miningPoint.transform, this, asteroid);
                    ships[i] = newShip;
                }

                miningShipsSetup = true;
            }
            else
            {
                for (int i = 0; i < ships.Length; i++)
                {
                    //MiningShip newShip = Instantiate(miningShipPrefab, transform);
                    GameObject miningPoint = new GameObject("MiningPoint" + (i + 1));
                    miningPoint.transform.parent = transform;
                    miningPoint.transform.localPosition = Vector3.zero;
                    ships[i].Setup(miningShips[i], dockingPoints[i], miningPoint.transform, this, asteroid);
                }
            }

            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
                SetupAtLevel(currentLevel, false);
        }

        public void OnDisableOutpost()
        {
            if (handleCargo != null)
                StopCoroutine(handleCargo);
            cargoShip.position = cargoBay.Point;
            cargoShip.gameObject.SetActive(false);
        }

        public void ActivateOutpost()
        {
            if (handleCargo != null)
                StopCoroutine(handleCargo);
            handleCargo = StartCoroutine(HandleCargo());
        }

        public string SaveAugments()
        {
            string data = "";
            for (int i = 0; i < augments.Length; i++)
            {
                if (i > 0)
                    data += "#";
                if (augments[i] != null)
                {
                    data += augments[i].SaveAugment();
                }
            }

            return data;
        }

        public void LoadAugments(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                for (int i = 0; i < augments.Length; i++)
                    augments[i] = null;
                //DisableRemainingAugments(0);
            }
            else
            {
                Debug.Log(data);
                string[] splitData = data.Split('#');
                for (int i = 0; i < splitData.Length; i++)
                {
                    if (string.IsNullOrEmpty(splitData[i]))
                        continue;
                    if (i < augments.Length)
                        augments[i] = new Augment(splitData[i]);
                }
            }

            SetupAugments();
            //RefreshAugments();
        }

        public int AugmentsAvailable
        {
            get
            {
                int maxAvailable = Mathf.CeilToInt((Level + 1) / (GameVersionManager.CurrentVersion != GameVersion.Version1 ? 12f : 2f));
                if (maxAvailable <= augments.Length)
                    return maxAvailable;
                return augments.Length;
            }
        }

        public void SetShip(int index, int ship)
        {
            miningShips[index] = ship;
            ships[index].Setup(ship);
        }

        public void SetUpgradeLevel(int level)
        {
            currentLevel = level;
        }

        public bool CanAffordUpgrade()
        {
            double cost = GetUpgradeCost();
            switch (OutpostManager.Instance.UpgradeCount)
            {
                case OutpostUpgradeCount.x1:
                case OutpostUpgradeCount.Max:
                    return CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost;
                default:
                    return OutpostManager.Instance.CanAffordOutpost(this, out int levels, out double totalCost);
            }
        }

        public void Upgrade()
        {
            if (IsMaxLevel)
                return;
            if (!OutpostManager.Instance.CanAffordOutpost(this, out int totalLevels, out double cost))
                return;

            AudioManager.Play("Upgrade", 0.5f);
            CurrencyController.Instance.AddCurrency(-cost, CurrencyType.SoftCurrency, false, "outpost", "upgrade_outpost");
            if (GameVersionManager.CurrentVersion >= GameVersion.Version3)
            {
                List<MilestoneReward> rewards = MilestoneManager.Instance.GetIntermediateRewardsCrossed(currentLevel, currentLevel + totalLevels);
                if (MilestoneManager.Instance.HasCrossedMilestone(currentLevel, currentLevel + totalLevels,
                        out List<Milestone> crossedMilestones))
                {
                    for (int i = 0; i < crossedMilestones.Count; i++)
                    {
                        if (!crossedMilestones[i].HasReward)
                            continue;
                        rewards.AddRange(crossedMilestones[i].milestoneRewards);
                    }
                }
                if (rewards.Count > 0)
                    MilestoneManager.Instance.ConferRewards(rewards);
            }
            currentLevel += totalLevels;
            if (GameVersionManager.CurrentVersion > GameVersion.Version1)
                SetupAtLevel(currentLevel, true);
            //TutorialManager.OnTrigger("upgrade");
            StatTracker.Instance.IncrementStat("outpostUpgrades", 1);

            // double cost = GetUpgradeCost();
            // if (CurrencyController.Instance.GetCurrency(CurrencyType.Gold) >= cost)
            // {
            //     AudioManager.Play("Upgrade", 0.5f);
            //     CurrencyController.Instance.AddCurrency(-cost, CurrencyType.Gold, false);
            //     currentLevel++;
            //     if (GameManager.Instance.UsingNewSystem)
            //         SetupAtLevel(currentLevel);
            //     //TutorialManager.OnTrigger("upgrade");
            //     StatTracker.Instance.IncrementStat("outpostUpgrades", 1);
            // }
        }

        public BezierPath GetPath()
        {
            return cargoPath.GetPath(); //new BezierPath(cargoBay, mainShip);
        }

        public void SetCargoCapacity(double capacity)
        {
            if (GameVersionManager.CurrentVersion == GameVersion.Version1)
                cargo.SetCapacity(capacity);
            else
            {
                cargoMult = 1d + (capacity / 100d);
                //StartCoroutine(WaitSetupAtLevel());
                SetupAtLevel(currentLevel, false);
            }
        }

        public void SetCargoSpeed(float speed)
        {
            if (GameVersionManager.CurrentVersion == GameVersion.Version1)
                cargoSpeed = speed;
            else
                cargoSpeed = 2.5f * (1f + (speed / 100f));
        }

        private void OnEnable()
        {
            GameManager.OnCargoCapacityChange += SetCargoCapacity;
            GameManager.OnCargoSpeedChange += SetCargoSpeed;
            GameManager.OnSecondUpdate += RefreshCargo;
            MiningStation.OnTap += Mine;
            MiningStation.OnLaserSetup += UpdateLaser;
        }

        private void OnDisable()
        {
            GameManager.OnCargoCapacityChange -= SetCargoCapacity;
            GameManager.OnCargoSpeedChange -= SetCargoSpeed;
            GameManager.OnSecondUpdate -= RefreshCargo;
            MiningStation.OnTap -= Mine;
            MiningStation.OnLaserSetup -= UpdateLaser;
        }

        private void Update()
        {
            //cargoPath.Setup(cargoBay, mainShip);
        }
    }

}