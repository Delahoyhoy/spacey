﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Gameplay.Outposts.Data;
using Core.Gameplay.Utilities;
using Data;
using Fumb.Data;
using Fumb.RemoteConfig;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public class OutpostUpgradeController
    {
        private bool initialised;

        private OutpostUpgrade[] upgrades;
        private double[] costInput, valueInput;

        private double valueBase, valueStep;

        [SerializeField]
        private RemoteBool useRemoteValue = new RemoteBool("useRemoteOutpostValues", true);
        
        [SerializeField]
        private RemoteOutpostValue outpostValue = new RemoteOutpostValue("outpostValues",
            new OutpostUpgradeValue(15, 0.1149598509, 1, 0.3));

        private void Initialise()
        {
            if (initialised)
                return;
            upgrades = DataAccessor.GetData<DataAssetOutpostUpgrade>().Data;
            costInput = new double[upgrades.Length];
            valueInput = new double[upgrades.Length];
            
            for (int i = 0; i < upgrades.Length; i++)
            {
                costInput[i] = upgrades[i].UpgradeCost;
                valueInput[i] = upgrades[i].UpgradeValue;
            }

            valueBase = upgrades[1].UpgradeValue;
            valueStep = upgrades[2].UpgradeValue - valueBase;
            
            initialised = true;

            // string info = "";
            // for (int i = 0; i < 100; i++)
            // {
            //     if (i > 0)
            //         info += "\n";
            //     info += $"index = {i}, cost = {GetValue(i).FormatAsCurrency()}, value = {GetValueMultiplier(i)}";
            // }
            // TextEditor te = new TextEditor();
            // te.text = info;
            // te.SelectAll();
            // te.Copy();
        }

        public double GetCostValue(int index)
        {
            if (useRemoteValue)
                return outpostValue.Value.GetCostAtLevel(index);
            Initialise();
            if (index < 0)
                return ExtrapolateLow(index, costInput);
            if (index >= costInput.Length)
                return ExtrapolateHigh(index, costInput);
            return costInput[index];
        }

        public double GetValueMultiplier(int index)
        {
            if (useRemoteValue)
                return outpostValue.Value.GetValueAtLevel(index);
            Initialise();
            return valueBase + (valueStep * index);
            // if (index < 0)
            //     return ExtrapolateLow(index, valueInput);
            // if (index >= valueInput.Length)
            //     return ExtrapolateHigh(index, valueInput);
            // return valueInput[index];
        }

        // Extrapolate a value for indices lower than 0
        private double ExtrapolateLow(int index, double[] inputValues)
        {
            double extrapolatedValue = inputValues[0] * Math.Pow((float)inputValues[1] / (float)inputValues[0], -index);
            return extrapolatedValue;
        }

        // Extrapolate a value for indices higher than the last index
        private double ExtrapolateHigh(int index, double[] inputValues)
        {
            double extrapolatedValue = inputValues[inputValues.Length - 1] * 
                                       Math.Pow((float)inputValues[inputValues.Length - 1] / (float)inputValues[inputValues.Length - 2], index - (inputValues.Length - 1));
            return extrapolatedValue;
        }
        
        // Redundant method, but interesting...
        private double ApproximateExponential(int x)
        {
            // Calculate the natural logarithm of input values
            double[] logValues = new double[costInput.Length];
            for (int i = 0; i < costInput.Length; i++)
            {
                logValues[i] = Math.Log(costInput[i]);
            }
        
            // Calculate the linear regression coefficients
            double sumX = 0f;
            double sumY = 0f;
            double sumXY = 0f;
            double sumXX = 0f;
        
            for (int i = 0; i < costInput.Length; i++)
            {
                sumX += x;
                sumY += logValues[i];
                sumXY += x * logValues[i];
                sumXX += x * x;
            }
        
            double n = costInput.Length;
            double denominator = n * sumXX - sumX * sumX;
            double slope;
            if (denominator == 0)
            {
                slope = 0f; // Handle zero denominator case
            }
            else
            {
                slope = (n * sumXY - sumX * sumY) / denominator;
            }
        
            double intercept = (sumY - slope * sumX) / n;
        
            // Calculate the exponential value
            double approximatedValue = Math.Exp(intercept + slope * x);
            return approximatedValue;
        }
    }
}