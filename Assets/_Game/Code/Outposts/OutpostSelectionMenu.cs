using System;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.CameraControl;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class OutpostSelectionMenu : AnchoredUI
    {
        [SerializeField]
        private UIScreen uiScreen;
        
        [SerializeField]
        private OutpostInventorySlot selectionPrefab;

        [SerializeField]
        private Transform holder;

        [SerializeField]
        private Button buildButton, upgradeButton;

        [SerializeField]
        private GameObject noOutposts;

        [SerializeField]
        private Transform workingSection;

        private List<OutpostInventorySlot> slots = new List<OutpostInventorySlot>();

        private Action<Outpost> onSelect;
        private Action buildOutpost;

        public void OpenMenu(Asteroid targetAsteroid, List<int> availableOutposts, List<Outpost> allOutposts)
        {
            Target = targetAsteroid.outpostAnchor;
            uiScreen.Open();
            BasicCameraController.Instance.Pan(targetAsteroid.outpostAnchor);
            SetupSelection(availableOutposts, allOutposts);
            onSelect = outpost =>
            {
                if (OutpostManager.Instance.DeployOutpost(outpost.EffectiveIndex, out Outpost result))
                    result.SetTarget(targetAsteroid);
            };
            buildOutpost = () => OutpostManager.Instance.BuildOutpostAtAsteroid(targetAsteroid.Index);
        }
        
        public void SetupSelection(List<int> availableOutposts, List<Outpost> allOutposts)
        {
            List<Outpost> workingOutposts = new List<Outpost>();

            int slotIndex = 0;

            for (int i = 0; i < allOutposts.Count; i++)
            {
                if (!availableOutposts.Contains(i))
                {
                    workingOutposts.Add(allOutposts[i]);
                    continue;
                }
                
                if (slotIndex < slots.Count)
                {
                    slots[slotIndex].Setup(allOutposts[i], true, SelectOutpost);
                    slotIndex++;
                    continue;
                }
                OutpostInventorySlot newSlot = Instantiate(selectionPrefab, holder);
                newSlot.Setup(allOutposts[i], true, SelectOutpost);
                slots.Add(newSlot);
                slotIndex++;
            }

            if (workingOutposts.Count > 0)
            {
                workingSection.gameObject.SetActive(true);
                workingSection.SetAsLastSibling();

                for (int i = 0; i < workingOutposts.Count; i++, slotIndex++)
                {
                    if (slotIndex < slots.Count)
                    {
                        slots[slotIndex].Setup(workingOutposts[i], false, SelectOutpost);
                        continue;
                    }
                    OutpostInventorySlot newSlot = Instantiate(selectionPrefab, holder);
                    newSlot.Setup(workingOutposts[i], false, SelectOutpost);
                    slots.Add(newSlot);
                }
            }
            else
                workingSection.gameObject.SetActive(false);

            // Disable unavailable slots
            for (int i = slotIndex; i < slots.Count; i++)
            {
                slots[i].SetActive(false);
            }
            
            noOutposts.SetActive(availableOutposts.Count <= 0);
        }

        public void RegisterClose()
        {
            
        }

        public void SelectOutpost(Outpost outpost)
        {
            uiScreen.Close();
            onSelect?.Invoke(outpost);
        }

        public void BuildNewOutpost()
        {
            uiScreen.Close();
            buildOutpost?.Invoke();
        }
    }
}