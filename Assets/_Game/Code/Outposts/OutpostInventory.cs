using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    public class OutpostInventory : MonoBehaviour
    {
        [SerializeField]
        private int maxCapacity = 1;

        [SerializeField]
        private int premiumCount = 2;

        public int MaxCapacity => maxCapacity;

        [SerializeField]
        private OutpostSelectionMenu selectionMenu;

        public void SetMaxCapacity(int capacity)
        {
            maxCapacity = capacity;
            while (OutpostManager.Instance.Outposts.Count < maxCapacity)
                OutpostManager.Instance.BuildOutpostAtPlaceholder(false, false);
        }

        public void SetPremiumActive(bool active)
        {
            OutpostManager.Instance.SetPremiumOutpostsActive(active, premiumCount);
        }

        public void OpenMenu(Asteroid target, List<int> availableOutposts, List<Outpost> allOutposts)
        {
            selectionMenu.OpenMenu(target, availableOutposts, allOutposts);
        }
    }
}