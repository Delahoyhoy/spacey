﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Augments;
using Core.Gameplay.CameraControl;
using Core.Gameplay.Outposts;
using Core.Gameplay.Ships;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class OutpostMenu : UIScreen
    {
        [SerializeField]
        private SlotSelection[] slotSelections;

        private Outpost outpost;
        private int selectedIndex;

        [SerializeField]
        private Image[] cargoCapacities = new Image[5];

        [SerializeField]
        private Button upgradeButton, nextButton, prevButton;

        [SerializeField]
        private Text upgradeCost, upgradeText;

        [SerializeField]
        private GameObject coinImage;

        [SerializeField]
        private ShipSelection shipMenu;

        [SerializeField]
        private AugmentSelectionMenu augmentMenu;

        [SerializeField]
        private Text outpostNameText;

        [SerializeField]
        private TierProbability[] tierProbabilities = new TierProbability[5];

        [SerializeField]
        private AugmentMenuSlot[] augmentMenuSlots;

        [SerializeField]
        private Text earningText, nextLevelText;

        [SerializeField]
        private GameObject nextLevelHolder;

        [SerializeField]
        private Slider levelProgress;

        [SerializeField]
        private Text augmentTextLeft, augmentTextRight, levelText, capacityText;

        [SerializeField]
        private IntermediateRewardDisplay intermediateRewardDisplay;

        [SerializeField]
        private Image nextUnlockPreview;

        [SerializeField]
        private bool gateAugments;

        [SerializeField] [ConditionalField("gateAugments")]
        private int gateAugmentsLevel;

        [SerializeField] [ConditionalField("gateAugments")]
        private GameObject augmentsLockedObject;

        [SerializeField]
        private UnityEvent<bool> onSetIsMining;

        [SerializeField]
        private UnityEvent<string> onSetUpgradeCount;

        private float toNextUpdate = 0.5f;

        public void Open(Outpost target)
        {
            outpost = target;
            outpostNameText.text = outpost.OutpostName;
            levelText.text = "LEVEL " + (outpost.Level + 1).ToString();
            SetupCapacityDisplay();
            for (int i = 0; i < slotSelections.Length; i++)
            {
                slotSelections[i].Setup(outpost.GetShipAtIndex(i), outpost.Level >= i);
            }

            double cost = outpost.GetUpgradeCost();
            if (outpost.IsMaxLevel)
            {
                coinImage.SetActive(false);
                upgradeCost.text = "MAX";
                upgradeButton.interactable = false;
            }
            else
            {
                coinImage.SetActive(true);
                upgradeCost.text = CurrencyController.FormatToString(cost);
                upgradeButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost;
            }

            double[] resourceYields = GameManager.Instance.GetAsteroid(outpost.AsteroidIndex).GetResourceYields();
            for (int i = 0; i < tierProbabilities.Length; i++)
            {
                tierProbabilities[i].Setup(resourceYields[i]);
            }

            SetupAugments();
            earningText.text = string.Format("{0}/m",
                CurrencyController.FormatToString(outpost.GetTotalGoldPerSecond(false) * 60));
            int currentIndex = outpost.OutpostIndex;
            nextButton.interactable = currentIndex < OutpostManager.Instance.GetOutpostCount(true) - 1;
            prevButton.interactable = currentIndex > 0;

            if (levelProgress && GameVersionManager.CurrentVersion >= GameVersion.Version3)
            {
                levelProgress.value = MilestoneManager.Instance.GetMilestoneProgress(outpost.Level);
                bool hasIntermediateReward = MilestoneManager.Instance.GetAnyIntermediateRewards(outpost.Level, 
                    out MilestoneReward reward, out float position);
                if (hasIntermediateReward)
                    intermediateRewardDisplay.Setup(reward.rewardType, position);
                else
                    intermediateRewardDisplay.SetActive(false);

                int nextUnlock = outpost.GetNextUnlockIndex();
                if (nextUnlock < ShipManager.Instance.TotalShips)
                    nextUnlockPreview.sprite = ShipManager.Instance.GetShipSprite(nextUnlock);
            }

            if (nextLevelHolder)
            {
                bool display = GameVersionManager.CurrentVersion > GameVersion.Version1 && !outpost.IsMaxLevel;
                nextLevelHolder.SetActive(display);
                if (display && nextLevelText)
                    nextLevelText.text = string.Format("{0}/m",
                        CurrencyController.FormatToString(outpost.GetTotalGoldPerSecondAtLevel(outpost.Level + 1) * 60));
            }

            onSetIsMining?.Invoke(outpost.IsMining);
            onSetUpgradeCount?.Invoke(OutpostManager.Instance.UpgradeCount.ToString());
            
            PanToOutpost();
            base.Open();
        }

        public void NextOutpost()
        {
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            int currentIndex = outpost.IsPremium
                ? OutpostManager.Instance.GetOutpostCount(false) + outpost.OutpostIndex
                : outpost.OutpostIndex;
            if (currentIndex < OutpostManager.Instance.GetOutpostCount(true) - 1)
                OutpostManager.Instance.OpenOutpostMenu(currentIndex + 1);
        }

        public void CycleUpgradeCount()
        {
            AudioManager.Play("Click", 0.5f);
            OutpostManager.Instance.CycleUpgradeCount();
            onSetUpgradeCount?.Invoke(OutpostManager.Instance.UpgradeCount.ToString());
            Refresh();
        }

        public void PrevOutpost()
        {
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            int currentIndex = outpost.IsPremium
                ? OutpostManager.Instance.GetOutpostCount(false) + outpost.OutpostIndex
                : outpost.OutpostIndex;
            if (currentIndex > 0)
                OutpostManager.Instance.OpenOutpostMenu(currentIndex - 1);
        }

        private List<AugmentEffect> GetAugmentEffects()
        {
            List<AugmentEffect> effects = new List<AugmentEffect>();
            for (int i = 0; i < outpost.AugmentsAvailable; i++)
            {
                Augment augment = outpost.GetAugment(i);
                if (augment == null)
                    continue;
                if (!effects.Contains(augment.augmentEffect))
                {
                    effects.Add(augment.augmentEffect);
                }
            }

            return effects;
        }

        private void SetupAugmentText()
        {
            List<AugmentEffect> totalAugments = GetAugmentEffects();
            augmentTextLeft.text = "";
            augmentTextRight.text = "";
            if (totalAugments.Count <= 0)
            {
                augmentTextRight.gameObject.SetActive(false);
                augmentTextLeft.gameObject.SetActive(true);
                augmentTextLeft.text = "NO AUGMENTS";
                return;
            }

            if (totalAugments.Count <= 2)
            {
                augmentTextRight.gameObject.SetActive(false);
                augmentTextLeft.gameObject.SetActive(true);
                for (int i = 0; i < totalAugments.Count; i++)
                {
                    if (i > 0)
                        augmentTextLeft.text += "\n";
                    augmentTextLeft.text += GetAugmentTextForEffect(totalAugments[i]);
                }

                return;
            }

            augmentTextRight.gameObject.SetActive(true);
            augmentTextLeft.gameObject.SetActive(true);
            for (int i = 0; i < totalAugments.Count; i++)
            {
                Text targetText = i % 2 == 0 ? augmentTextLeft : augmentTextRight;
                if (i > 1)
                    targetText.text += "\n";
                targetText.text += GetAugmentTextForEffect(totalAugments[i]);
            }
        }

        private string GetAugmentTextForEffect(AugmentEffect effect)
        {
            float totalEffect = 0;
            for (int i = 0; i < outpost.AugmentsAvailable; i++)
            {
                Augment augment = outpost.GetAugment(i);
                if (augment == null) 
                    continue;
                if (augment.augmentEffect == effect)
                    totalEffect += AugmentManager.Instance.GetMagnitude(augment.augmentEffect, augment.augmentTier);
            }

            return AugmentManager.Instance.GetDescription(effect, totalEffect);
        }

        private void SetupAugments()
        {
            if (!outpost)
                return;

            if (gateAugments && augmentsLockedObject)
            {
                augmentsLockedObject.SetActive(outpost.Level < gateAugmentsLevel);
            }
            
            int countAvailable = outpost.AugmentsAvailable;
            for (int i = 0; i < augmentMenuSlots.Length; i++)
            {
                if (i < countAvailable)
                {
                    augmentMenuSlots[i].slotHolder.SetActive(true);
                    Augment augment = outpost.GetAugment(i);
                    if (augment != null)
                    {
                        augmentMenuSlots[i].display.gameObject.SetActive(true);
                        augmentMenuSlots[i].display.Setup(augment.augmentEffect, augment.augmentTier);
                    }
                    else
                        augmentMenuSlots[i].display.gameObject.SetActive(false);
                }
                else
                    augmentMenuSlots[i].slotHolder.SetActive(false);
            }

            SetupAugmentText();
        }

        public void OpenShipMenu()
        {
            //shipMenu.Open(true, outpost, 0);
            ShipManager.Instance.SetTarget(outpost, 0);
            ShipManager.Instance.OpenMenu();
        }

        public void SelectAugment(int slot)
        {
            if (!outpost)
                return;
            augmentMenu.Open(outpost, slot);
        }

        public void OpenOnFirstOutpost()
        {
            OutpostManager.Instance.OpenOutpostMenu(0);
        }

        private void SetupCapacityDisplay()
        {
            if (outpost)
            {
                float total = 0;
                float[] cargoComp = outpost.GetNormalisedCargo();
                for (int i = 0; i < cargoCapacities.Length; i++)
                {
                    cargoCapacities[i].fillAmount = total + cargoComp[i];
                    total += cargoComp[i];
                }
            }

            capacityText.text = string.Format("{0}/{1}\nTONS",
                CurrencyController.FormatToString(outpost.GetCurrentFill()),
                CurrencyController.FormatToString(outpost.GetMaxCapacity()));
        }

        public void PanToOutpost()
        {
            if (!outpost)
                return;
            //UIManager.Instance.ForceAllClosed();
            BasicCameraController.Instance.Pan(outpost.transform, new Vector2(0, -0.5f));
        }

        public void Refresh()
        {
            if (!outpost)
                return;
            SetupCapacityDisplay();
            levelText.text = "LEVEL " + (outpost.Level + 1);
            for (int i = 0; i < slotSelections.Length; i++)
            {
                slotSelections[i].Setup(outpost.GetShipAtIndex(i), (GameVersionManager.CurrentVersion >= GameVersion.Version3 ? 
                    MilestoneManager.Instance.GetMilestonesCrossed(outpost.Level) : outpost.Level) >= i);
            }

//        double cost = outpost.GetUpgradeCost();
            bool canAfford = OutpostManager.Instance.CanAffordOutpost(outpost, out int levels, out double totalCost);
            if (outpost.IsMaxLevel)
            {
                coinImage.SetActive(false);
                upgradeCost.text = "MAX";
                upgradeButton.interactable = false;
            }
            else
            {
                //CurrencyController.Instance.CurrentGold >= costToUpgrade;
                coinImage.SetActive(true);
                if (upgradeText)
                    upgradeText.text = levels > 1 ? $"UPGRADE x{levels}" : "UPGRADE";
                upgradeCost.text = CurrencyController.FormatToString(totalCost);
                upgradeButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= totalCost;
            }

            SetupAugments();
            earningText.text = string.Format("{0}/m",
                CurrencyController.FormatToString(outpost.GetTotalGoldPerSecond(false) * 60));
            
            if (GameVersionManager.CurrentVersion >= GameVersion.Version3 && levelProgress)
            {
                levelProgress.value = MilestoneManager.Instance.GetMilestoneProgress(outpost.Level);
                bool hasIntermediateReward = MilestoneManager.Instance.GetAnyIntermediateRewards(outpost.Level, 
                    out MilestoneReward reward, out float position);
                if (hasIntermediateReward)
                    intermediateRewardDisplay.Setup(reward.rewardType, position);
                else
                    intermediateRewardDisplay.SetActive(false);
                
                int nextUnlock = outpost.GetNextUnlockIndex();
                if (nextUnlock < ShipManager.Instance.TotalShips)
                    nextUnlockPreview.sprite = ShipManager.Instance.GetShipSprite(nextUnlock);
            }

            if (nextLevelHolder)
            {
                bool display = GameVersionManager.CurrentVersion != GameVersion.Version1 && !outpost.IsMaxLevel;
                nextLevelHolder.SetActive(display);
                if (display && nextLevelText)
                    nextLevelText.text = string.Format("{0}/m",
                        CurrencyController.FormatToString(outpost.GetTotalGoldPerSecondAtLevel(outpost.Level + levels) *
                                                          60));
            }
            onSetIsMining?.Invoke(outpost.IsMining);
        }

        public void SelectSlot(int slot)
        {
            //shipMenu.Open(true, outpost, slot);
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
            {
                ShipManager.Instance.OpenLibrary(outpost.GetShipAtIndex(slot));
                return;
            }

            ShipManager.Instance.SetTarget(outpost, slot);
            ShipManager.Instance.OpenMenu();
            Refresh();
        }

        public void Upgrade()
        {
            outpost.Upgrade();
            Refresh();
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();
            toNextUpdate -= Time.deltaTime;
            if (toNextUpdate <= 0)
            {
                toNextUpdate += 0.5f;
                Refresh();
                // if (!outpost.IsMaxLevel)
                // {
                //     upgradeButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.Gold) >= outpost.GetUpgradeCost();
                // }
                SetupCapacityDisplay();
            }
        }
    }

}