using System;
using Core.Gameplay.Ships;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public struct SlotSelection
    {
        public Button button;
        public Image shipImage;

        public void Setup(int index, bool accessible)
        {
            button.interactable = accessible;
            if (index < 0)
                shipImage.gameObject.SetActive(false);
            else
            {
                shipImage.gameObject.SetActive(true);
                shipImage.sprite = ShipManager.Instance.GetUISprite(index);
            }
        }
    }
}