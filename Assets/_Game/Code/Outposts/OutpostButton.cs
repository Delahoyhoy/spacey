﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class OutpostButton : AnchoredUI
    {
        private Outpost targetOutpost;

        [SerializeField]
        private Text outpostName;

        [SerializeField]
        private RectTransform root;

        [SerializeField]
        private Image[] tierScales = new Image[5];

        [SerializeField]
        private Text cargoBay;

        [SerializeField]
        private GameObject notification;

        [SerializeField]
        private GameObject display, affordable;

        private bool showDisplay;
        
        public void Setup(Outpost outpost)
        {
            targetOutpost = outpost;
            Target = outpost.transform;
            showDisplay = GameVersionManager.CurrentVersion < GameVersion.Version3;
            display.SetActive(showDisplay);
            affordable.SetActive(false);
            notification.SetActive(false);
            if (!showDisplay) 
                return;
            SetupTiers();
            SetText();
            nextUpdate = Time.time;
        }

        private void SetupTiers()
        {
            double[] tiers = {1, 0, 0, 0, 0};
            if (!targetOutpost.HasTarget)
                return;
            Asteroid asteroid = GameManager.Instance.GetAsteroid(targetOutpost.AsteroidIndex);
            if (asteroid)
                tiers = asteroid.GetResourceYields();
            double total = 0;
            for (int i = 0; i < tiers.Length; i++)
            {
                total += tiers[i];
                tierScales[i].fillAmount = (float) total;
            }
        }

        private float nextUpdate = 3f;

        private void Update()
        {
            nextUpdate -= Time.deltaTime;
            if (nextUpdate <= 0)
            {
                nextUpdate += 1f;
                bool canAfford = targetOutpost.CanAffordUpgrade();
                notification.SetActive(showDisplay && canAfford);
                affordable.SetActive(!showDisplay && canAfford && targetOutpost.HasTarget);
            }

            if (!showDisplay)
                return;
            
            if (targetOutpost.GetCurrentlyMining())
            {
                int percentageFull = Mathf.RoundToInt(targetOutpost.GetPercentageFull() * 100);
                if (percentageFull >= 100)
                    cargoBay.text = "CARGO: <color=#ee0000ff>FULL</color>";
                else
                    cargoBay.text = "CARGO: " + percentageFull.ToString() + "%";
            }
            else
                cargoBay.text = "<color=#ee0000ff>ASSIGN SHIPS</color>";
        }

        private void SetText()
        {
            outpostName.text = targetOutpost.OutpostName;
            LayoutRebuilder.MarkLayoutForRebuild(root);
        }

        public void Select()
        {
            // if (FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
            OutpostManager.Instance.OpenOutpostMenu(targetOutpost);
        }
    }

}