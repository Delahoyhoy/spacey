﻿using System;
using Core.Gameplay.Outposts.Data;
using UnityEngine;

namespace Fumb.RemoteConfig
{
    [Serializable]
    public class RemoteOutpostMilestoneData : RemoteJson<OutpostMilestoneData>
    {
        public RemoteOutpostMilestoneData(string key, OutpostMilestoneData defaultValue) : base(key, defaultValue) { }
    }
}