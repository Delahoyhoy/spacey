﻿using System;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public class OutpostStage
    {
        [SerializeField]
        private string outpostStageID;

        public string OutpostStageId => outpostStageID;

        [SerializeField]
        private bool defaultBuilt;

        public bool DefaultBuilt => defaultBuilt;

        [SerializeField]
        private bool isActive;

        public bool IsActive => isActive;

        [SerializeField]
        private int unlockLevel;

        public int UnlockLevel => unlockLevel;

        [SerializeField]
        private MilestoneRewardType stageReward;

        public MilestoneRewardType StageReward => stageReward;

        [SerializeField]
        private int stageRewardAmount;

        public int StageRewardAmount => stageRewardAmount;
    }
}