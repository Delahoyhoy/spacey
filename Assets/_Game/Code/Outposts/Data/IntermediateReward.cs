﻿using System;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public class IntermediateReward
    {
        [SerializeField]
        private int unlockLevel;

        public int UnlockLevel => unlockLevel;

        [SerializeField]
        private MilestoneRewardType interReward;

        [SerializeField]
        private int interRewardAmount;

        public MilestoneReward Reward => new MilestoneReward(interReward, interRewardAmount);
    }
}