﻿using System;
using UnityEngine;

namespace Core.Gameplay.Outposts.Data
{
    [Serializable]
    public class OutpostUpgradeValue
    {
        [SerializeField]
        private double baseCost;

        [SerializeField]
        private double costExponent;

        public double GetCostAtLevel(int level)
        {
            return baseCost * Math.Exp(costExponent * level);
        }

        [SerializeField]
        private double baseValue;

        [SerializeField]
        private double valueIncrease;

        public double GetValueAtLevel(int level)
        {
            return baseValue + (valueIncrease * level);
        }

        public OutpostUpgradeValue(double costBase, double costExp, double valueBase, double valueIncr)
        {
            baseCost = costBase;
            costExponent = costExp;

            baseValue = valueBase;
            valueIncrease = valueIncr;
        }
    }
}