﻿using System;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public class OutpostUpgrade
    {
        [SerializeField]
        private int outpostLevel;
        public int OutpostLevel => outpostLevel;

        [SerializeField]
        private double upgradeCost;
        public double UpgradeCost => upgradeCost;

        [SerializeField]
        private double upgradeValue;
        public double UpgradeValue => upgradeValue;
    }
}