using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public class OutpostData
    {
        [SerializeField]
        private string outpostStageID;

        public string OutpostStageId => outpostStageID;
        
        [SerializeField] 
        private bool defaultBuilt;

        public bool DefaultBuild => defaultBuilt;

        [SerializeField]
        private bool isActive;

        public bool IsActive => isActive;

        [SerializeField] 
        private double unlockCost;

        public double UnlockCost => unlockCost;

        [SerializeField] 
        private double costModifier;

        public double CostModifier => costModifier;

        [SerializeField] 
        private double valueModifier;

        public double ValueModifier => valueModifier;

    }
}
