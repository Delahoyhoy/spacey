﻿#if USE_FUMB_DATA
using Core.Gameplay.Outposts;
using Fumb.Data;
using UnityEngine;

namespace Data {
    [CreateAssetMenu(fileName = "DataAssetIntermediateReward", menuName = "Data/DataAsset/DataAssetIntermediateReward", order = 10000)]
    public class DataAssetIntermediateReward : DataAsset<IntermediateReward> { 
    
        /// <summary>
        /// If you have multiple instance of the same type of DataAsset, use this override to differentiate them for runtime lookup.
        /// For example, your asset could contain an Enum Category Type, and return that value cast to string.
        /// </summary>
        public override string Variant => "";
    }
}
#endif