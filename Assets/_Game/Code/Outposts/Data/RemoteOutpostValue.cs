﻿using System;
using Core.Gameplay.Outposts.Data;

namespace Fumb.RemoteConfig
{
    [Serializable]
    public class RemoteOutpostValue : RemoteJson<OutpostUpgradeValue>
    {
        public RemoteOutpostValue(string key, OutpostUpgradeValue defaultValue) : base(key, defaultValue) { }
    }
}