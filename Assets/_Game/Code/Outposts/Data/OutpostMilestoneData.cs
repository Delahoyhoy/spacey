using System;
using UnityEngine;

namespace Core.Gameplay.Outposts.Data
{
    [Serializable]
    public class OutpostMilestoneData
    {
        [SerializeField]
        private int baseValue;

        [SerializeField]
        private float r;

        [SerializeField]
        private int n;

        public int Count => n;

        public OutpostMilestoneData(int baseVal, float rVal, int nVal)
        {
            baseValue = baseVal;
            r = rVal;
            n = nVal;
        }

        public int GetMilestoneLevel(int index)
        {
            return Mathf.RoundToInt(baseValue + (index * r));
        }

    }

}