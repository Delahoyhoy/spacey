#if USE_FUMB_DATA
using Core.Gameplay.Outposts;
using Fumb.Data;
using UnityEngine;

namespace Data {
    [CreateAssetMenu(fileName = "DataAssetOutpostData", menuName = "Data/DataAsset/DataAssetOutpostData", order = 10000)]
    public class DataAssetOutpostData : DataAsset<OutpostData> { 
    
        /// <summary>
        /// If you have multiple instance of the same type of DataAsset, use this override to differentiate them for runtime lookup.
        /// For example, your asset could contain an Enum Category Type, and return that value cast to string.
        /// </summary>
        public override string Variant => "";
    }
}
#endif
