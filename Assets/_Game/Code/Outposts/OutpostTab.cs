﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Augments;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class OutpostTab : MonoBehaviour
    {
        private OutpostUI uiRef;

        [SerializeField]
        private Text outpostNameText, earningText, levelText, upgradeCost, upgradeText;

        [SerializeField]
        private Button upgradeButton;

        [SerializeField]
        private Image[] tierScales = new Image[5], augments = new Image[5];

        [SerializeField]
        private Color affordableColour = new Color(0.1960784f, 0.1960784f, 0.1960784f, 1f);
        
        public Outpost Target => OutpostManager.Instance.GetOutpost(index);

        public Asteroid TargetAsteroid => GameManager.Instance.GetAsteroid(Target.AsteroidIndex);

        private void SetupTiers(Outpost outpost)
        {
            double[] tiers = {1, 0, 0, 0, 0};
            Asteroid asteroid = GameManager.Instance.GetAsteroid(outpost.AsteroidIndex);
            if (asteroid)
                tiers = asteroid.GetResourceYields();
            double total = 0;
            for (int i = 0; i < tiers.Length; i++)
            {
                total += tiers[i];
                tierScales[i].fillAmount = (float) total;
            }
        }

        private int index;

        public void Setup(OutpostUI outpostUI, int outpostIndex)
        {
            index = outpostIndex;
            uiRef = outpostUI;
            Outpost outpost = OutpostManager.Instance.GetOutpost(index);
            outpostNameText.text = outpost.OutpostName;
            Refresh();
        }

        private void SetupAugments()
        {
            Outpost outpost = OutpostManager.Instance.GetOutpost(index);
            int countAvailable = outpost.AugmentsAvailable;
            for (int i = 0; i < augments.Length; i++)
            {
                if (i < countAvailable)
                {
                    augments[i].gameObject.SetActive(true);
                    Augment augment = outpost.GetAugment(i);
                    if (augment != null)
                    {
                        augments[i].sprite = AugmentManager.Instance.GetSymbol(augment.augmentEffect);
                        augments[i].color = ColourHelper.Instance.GetTierColour(augment.augmentTier);
                    }
                    else
                        augments[i].gameObject.SetActive(false);
                }
                else
                    augments[i].gameObject.SetActive(false);
            }
        }

        private double UpgradeCost
        {
            get { return OutpostManager.Instance.GetOutpost(index).GetUpgradeCost(); }
        }

        public void Upgrade()
        {
            double cost = UpgradeCost;

            if (CurrencyController.Instance.CurrentGold < cost)
                return;
            OutpostManager.Instance.GetOutpost(index).Upgrade();
            uiRef.Setup();
        }

        public void Refresh()
        {
            Outpost outpost = OutpostManager.Instance.GetOutpost(index);
            earningText.text = CurrencyController.FormatToString(outpost.GetTotalGoldPerSecond(false)) + "/sec";
            levelText.text = "LEVEL " + (outpost.Level + 1).ToString();
            //double costToUpgrade = UpgradeCost;
            bool canAfford =
                OutpostManager.Instance.CanAffordOutpost(outpost, out int levels,
                    out double totalCost); //CurrencyController.Instance.CurrentGold >= costToUpgrade;
            if (upgradeText)
                upgradeText.text = levels > 1 ? $"UPGRADE x{levels}" : "UPGRADE";
            upgradeCost.text = outpost.IsMaxLevel ? "MAX" : CurrencyController.FormatToString(totalCost);
            upgradeCost.color = canAfford && !outpost.IsMaxLevel ? affordableColour : Color.red;
            upgradeButton.interactable = canAfford && !outpost.IsMaxLevel;
            SetupTiers(outpost);
            SetupAugments();
        }

        public void Select()
        {
            Outpost outpost = OutpostManager.Instance.GetOutpost(index);
            OutpostManager.Instance.OpenOutpostMenu(outpost);
            //uiRef.Close();
        }
    }

}