﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Fumb.Save;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Outposts
{
    public class OutpostUI : UIScreen
    {
        [SerializeField]
        private RectTransform listParent;

        [SerializeField]
        private OutpostTab tabPrefab;

        private List<OutpostTab> outpostTabs = new List<OutpostTab>();

        private OutpostSortMode currentSortMode;

        [ManagedSaveValue("OutpostUISortMode")]
        private SaveValueInt saveSortMode;

        private float refreshCountdown = 1f;

        [SerializeField]
        private Text sortModeText, upgradeCountText;

        public void Setup()
        {
            int outpostCount = OutpostManager.Instance.GetOutpostCount(true);
            for (int i = 0; i < outpostTabs.Count; i++)
            {
                if (i < outpostCount)
                {
                    outpostTabs[i].Refresh();
                }
            }

            for (int i = outpostTabs.Count; i < outpostCount; i++)
            {
                OutpostTab newTab = Instantiate(tabPrefab, listParent);
                newTab.Setup(this, i);
                outpostTabs.Add(newTab);
            }

            SortList();
        }

        public void CycleUpgradeCount()
        {
            AudioManager.Play("Click", 0.75f);
            OutpostManager.Instance.CycleUpgradeCount();
            Setup();
            upgradeCountText.text = GetCountText(OutpostManager.Instance.UpgradeCount);
        }

        private void SortList()
        {
            if (outpostTabs.Count <= 0)
                return;
            List<OutpostTab> sortedSlots = new List<OutpostTab>(outpostTabs.ToArray());
            sortedSlots = Sort(sortedSlots, currentSortMode);
            for (int i = 0; i < sortedSlots.Count; i++)
            {
                sortedSlots[i].transform.SetAsLastSibling();
            }
        }

        private string GetModeText(OutpostSortMode sortMode)
        {
            return sortMode switch
            {
                OutpostSortMode.Default => "DEFAULT",
                OutpostSortMode.Distance => "DISTANCE",
                OutpostSortMode.Earnings => "EARNINGS",
                OutpostSortMode.Yield => "YIELD",
                _ => "DEFAULT"
            };
        }

        private string GetCountText(OutpostUpgradeCount upgradeCount)
        {
            return upgradeCount switch
            {
                OutpostUpgradeCount.x1 => "x1",
                OutpostUpgradeCount.Max => "MAX",
                _ => "???"
            };
        }

        public void CycleSortMode()
        {
            AudioManager.Play("Click", 0.75f);
            if ((int) currentSortMode >= Enum.GetValues(typeof(OutpostSortMode)).Length - 1)
                currentSortMode = 0;
            else
                currentSortMode++;
            saveSortMode.Value = (int) currentSortMode;
            SetSortMode(currentSortMode);
        }

        public void SetSortMode(OutpostSortMode mode)
        {
            currentSortMode = mode;
            sortModeText.text = GetModeText(currentSortMode);
            SortList();
        }

        private List<OutpostTab> Sort(List<OutpostTab> tabs, OutpostSortMode sortMode)
        {
            List<double> metric = new List<double>();
            List<OutpostTab> sortedList = new List<OutpostTab>();
            switch (sortMode)
            {
                case OutpostSortMode.Default:
                    return tabs;
                case OutpostSortMode.Distance:
                    for (int i = 0; i < tabs.Count; i++)
                    {
                        metric.Add(Vector3.Distance(tabs[i].TargetAsteroid.transform.position,
                            GameManager.Instance.FogCentre));
                    }

                    while (tabs.Count > 0)
                    {
                        double min = double.MaxValue;
                        int minIndex = 0;
                        for (int i = 0; i < metric.Count; i++)
                        {
                            if (metric[i] >= min)
                                continue;
                            min = metric[i];
                            minIndex = i;
                        }

                        sortedList.Add(tabs[minIndex]);
                        tabs.RemoveAt(minIndex);
                        metric.RemoveAt(minIndex);
                    }

                    break;
                case OutpostSortMode.Yield:
                    for (int i = 0; i < tabs.Count; i++)
                    {
                        metric.Add(GetYield(tabs[i].TargetAsteroid));
                    }

                    while (tabs.Count > 0)
                    {
                        double max = double.MinValue;
                        int maxIndex = 0;
                        for (int i = 0; i < metric.Count; i++)
                        {
                            if (metric[i] > max)
                            {
                                max = metric[i];
                                maxIndex = i;
                            }
                        }

                        sortedList.Add(tabs[maxIndex]);
                        tabs.RemoveAt(maxIndex);
                        metric.RemoveAt(maxIndex);
                    }

                    break;
                case OutpostSortMode.Earnings:
                    for (int i = 0; i < tabs.Count; i++)
                    {
                        metric.Add(tabs[i].Target.GetTotalGoldPerSecond(false));
                    }

                    while (tabs.Count > 0)
                    {
                        double max = double.MinValue;
                        int maxIndex = 0;
                        for (int i = 0; i < metric.Count; i++)
                        {
                            if (metric[i] > max)
                            {
                                max = metric[i];
                                maxIndex = i;
                            }
                        }

                        sortedList.Add(tabs[maxIndex]);
                        tabs.RemoveAt(maxIndex);
                        metric.RemoveAt(maxIndex);
                    }

                    break;
            }

            return sortedList;
        }

        private static double GetYield(Asteroid asteroid)
        {
            if (asteroid.State == AsteroidState.Unscanned || asteroid.State == AsteroidState.Scanning)
                return 0;
            double[] yields = asteroid.GetResourceYields();
            double total = 0;
            for (int i = 0; i < yields.Length; i++)
            {
                total += yields[i] * TierHelper.GetValue((Tier) i);
            }

            return total;
        }

        public void ClearMenu()
        {
            for (int i = 0; i < outpostTabs.Count; i++)
            {
                Destroy(outpostTabs[i].gameObject);
            }

            outpostTabs.Clear();
        }

        public override void Open()
        {
            base.Open();
            Setup();
        }

        protected override void Start()
        {
            currentSortMode = (OutpostSortMode) saveSortMode.Value;
            SetSortMode(currentSortMode);
            upgradeCountText.text = GetCountText(OutpostManager.Instance.UpgradeCount);
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();
            refreshCountdown -= Time.deltaTime;
            if (refreshCountdown > 0)
                return;
            refreshCountdown++;
            Setup();
        }
    }

}