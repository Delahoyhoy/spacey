﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Version;
using UnityEngine;

namespace Core.Gameplay.Outposts
{
    [Serializable]
    public class OutpostMenuVersion
    {
        [SerializeField]
        private List<GameVersion> validVersions = new List<GameVersion>();

        [SerializeField]
        private OutpostMenu menu;

        public OutpostMenu Menu => menu;

        public bool IsValid(GameVersion version, out OutpostMenu outpostMenu)
        {
            outpostMenu = menu;
            return validVersions.Contains(version);
        }
    }
}