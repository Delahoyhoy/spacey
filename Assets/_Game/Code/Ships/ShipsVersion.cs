﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Version;
using UnityEngine;

namespace Core.Gameplay.Ships
{
    [Serializable]
    public class ShipsVersion
    {
        [SerializeField]
        private List<GameVersion> validVersions;

        [SerializeField]
        private Ship[] ships;

        public bool GetValid(GameVersion version, out Ship[] shipTemplates)
        {
            shipTemplates = ships;
            return validVersions.Contains(version);
        }
    }
}