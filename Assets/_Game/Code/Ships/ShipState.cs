namespace Core.Gameplay.Ships
{
    public enum ShipState
    {
        ToMine,
        Mining,
        ToDock,
        Docked,
        Waiting,
    }
}