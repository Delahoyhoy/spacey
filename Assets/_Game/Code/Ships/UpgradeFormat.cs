namespace Core.Gameplay.Ships
{
    public enum UpgradeFormat
    {
        OneLevel,
        TenLevels,
        HundredLevels,
        NextBoundary,
    }
}