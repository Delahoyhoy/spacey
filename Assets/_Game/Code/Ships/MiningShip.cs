﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Economy;
using Core.Gameplay.Outposts;
using UnityEngine;
using UnityEngine.Events;
using Core.Gameplay.Research;
using Core.Gameplay.Sectors;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Ships
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class MiningShip : MonoBehaviour
    {
        private MineralLoad currentLoad = new MineralLoad();

        [SerializeField]
        private Transform miningPos, dockPos;

        [SerializeField]
        private Asteroid target;

        [SerializeField]
        private LineRenderer laserObject;

        [SerializeField]
        private SpriteRenderer shipBody;

        [SerializeField]
        private GameObject laserHit;

        [SerializeField]
        private AnimationCurve speedCurve, rotationCurve;

        private ShipState state = ShipState.ToMine;
        public ShipState State => state;

        public bool HasCargo => currentLoad.TotalMinerals > 0;
        public double TotalMineralValue => currentLoad.TotalValue;

        private Coroutine miningLoop;

        [SerializeField]
        private double speed = 1d, miningSpeed = 1d, efficiency = 10d;

        public double MoveSpeed => speed * globalSpeedMult.Value * bonusSpeedMult;
        public double MineSpeed => miningSpeed * GameManager.Instance.globalMineSpeedMult * bonusSpeedMult;

        private double bonusSpeedMult = 1d;

        private ModifierEffect globalSpeedMult = new ModifierEffect(SectorModifier.ShipSpeed);

        [SerializeField]
        private MeshRenderer[] shipRenderers;

        private Outpost dock;

        private int index = -1;
        private bool setup;

        private void Start()
        {
            if (setup)
                return;
            SetBonusMult(GameManager.Instance.BonusMultiplier);
            GameManager.OnSetBonusActive += SetBonusMult;
            setup = true;
        }

        private void SetBonusMult(double multiplier)
        {
            bonusSpeedMult = multiplier;
        }

        private Vector3 GetRandomMiningPos(float angle, float distance)
        {
            //float angle = Random.Range(0, 360f) * Mathf.Deg2Rad;
            float x = Mathf.Sin(angle) * distance;
            float y = Mathf.Cos(angle) * distance;
            Vector3 pos = target.transform.position + (new Vector3(x, y, 0));
            pos.z = 1;
            return pos;
        }

        private float GetAngleBetween(Vector3 from, Vector3 to)
        {
            float dx = to.x - from.x, dy = to.y - from.y;
            float result = (Mathf.Atan2(dy, dx) * Mathf.Rad2Deg) - 90f;
            return result;
        }

        public void RefreshCapacity()
        {
            if (index < 0)
                return;
            currentLoad.SetCapacity(ShipManager.Instance.GetCapacity(index) * dock.CapacityMult);
        }

        public void WakeShip(Asteroid newTarget)
        {
            target = newTarget;
            transform.position = dockPos.position;
            transform.eulerAngles = dockPos.eulerAngles;
            if (miningLoop != null)
                StopCoroutine(miningLoop);
            miningLoop = StartCoroutine(MiningLoop());
        }

        //private static List<float> distancesTravelled = new List<float>();

        private double GetMiningPercent()
        {
            double timeToFill = currentLoad.maxCapacity / miningSpeed;
            //double approxFlyTime = 2 * (3.0535 / speed);
            double totalTime = timeToFill + 1; // + approxFlyTime;
            return timeToFill / totalTime;
        }

        private readonly float[] lateralSpeeds = {-0.33f, -0.25f, -0.125f, 0.125f, 0.25f, 0.33f};

        private IEnumerator MiningLoop()
        {
            Vector2 miningDistance = target.GetMiningDistance();
            Vector3 laserOffset = Vector3.up * miningDistance.y / 0.15f;
            laserHit.transform.localPosition = laserOffset;
            laserHit.SetActive(false);
            laserObject.SetPositions(new Vector3[] {new Vector3(0, 0.25f, 0), laserOffset / 2f, laserOffset});
            miningPos.position = GetRandomMiningPos(target.GetMiningAngle(), miningDistance.x);
            miningPos.eulerAngles = Vector3.forward * GetAngleBetween(miningPos.position, target.transform.position);
            state = ShipState.ToMine;
            //distancesTravelled.Add(Vector3.Distance(dockPos.position, miningPos.position));
            //Quaternion startRot = transform.rotation;
            //Quaternion targetRotation = Quaternion.FromToRotation(Vector3.up, miningPos.position - transform.position);
            float startAngle = transform.eulerAngles.z;
            float targetAngle = GetAngleBetween(transform.position, miningPos.position);
            //Debug.Log("TARGET = " + targetAngle);
            float rotStep = 180f / Mathf.Abs(Mathf.DeltaAngle(startAngle, targetAngle));
            for (float f = 0; f < 1f; f += Time.deltaTime * rotStep)
            {

                //transform.rotation = Quaternion.Lerp(startRot, targetRotation, speedCurve.Evaluate(f));
                transform.eulerAngles =
                    new Vector3(0, 0, Mathf.LerpAngle(startAngle, targetAngle, speedCurve.Evaluate(f)));

                yield return null;
            }

            //while (transform.rotation != targetRotation)
            //{
            //    transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 270f * Time.deltaTime);
            //    yield return null;
            //}
            Vector3 startPos = transform.position;
            //startRot = transform.rotation;
            startAngle = transform.eulerAngles.z;
            float step = (float) MoveSpeed / Vector3.Distance(startPos, miningPos.position);
            startAngle = transform.eulerAngles.z;
            //targetAngle = GetAngleBetween(transform.position, miningPos.position);
            //Debug.Log("Moving to Mine, step = " + step);
            for (float f = 0; f < 1f; f += Time.deltaTime * step * (float) dock.SpeedMult)
            {
                float t = speedCurve.Evaluate(f);
                float r = rotationCurve.Evaluate(f);
                transform.position = Vector3.Lerp(startPos, miningPos.position, t);
                transform.eulerAngles =
                    new Vector3(0, 0,
                        Mathf.LerpAngle(startAngle, miningPos.eulerAngles.z,
                            r)); //Quaternion.Lerp(startRot, miningPos.rotation, r);
                yield return null;
            }
            transform.position = miningPos.position;
            state = ShipState.Mining;
            dock.CheckStatus();
            laserObject.gameObject.SetActive(true);
            laserHit.SetActive(true);
            StartCoroutine(MineEffects());
            float lateralSpeed = lateralSpeeds[Random.Range(0, lateralSpeeds.Length)];
            Vector3 driftVector = transform.right;
            float mineTime = Random.Range(1f, 2f);
            while (!currentLoad.Full)
            {
                //transform.rotation = Quaternion.FromToRotation(Vector3.up, target.transform.position - transform.position);
                if (target.State == AsteroidState.Finished)
                    break;
                if (GameVersionManager.CurrentVersion < GameVersion.Version3)
                {
                    target.Mine(MineSpeed * Time.deltaTime * dock.MineSpeedMult,
                        ref currentLoad, (efficiency * GameManager.Instance.efficiencyMult) + dock.EfficiencyMult);
                }
                else
                {
                    if (target.Mine(MineSpeed * Time.deltaTime * dock.MineSpeedMult,
                        (efficiency * GameManager.Instance.efficiencyMult) + dock.EfficiencyMult, ref currentLoad))
                        break;
                }
                laserObject.widthMultiplier = 0.2f + (Mathf.Sin(Time.time * 30f) * 0.05f);
                transform.eulerAngles =
                    Vector3.forward * GetAngleBetween(transform.position, target.transform.position);
                driftVector = transform.right;
                transform.Translate(-Time.deltaTime * lateralSpeed, 0, 0, Space.Self);
                //laserHit.transform.Rotate(0, 0, 360f * Time.deltaTime, Space.Self);
                yield return null;
            }

            state = ShipState.ToDock;
            dock.CheckStatus();
            laserObject.gameObject.SetActive(false);
            laserHit.SetActive(false);
            //startRot = transform.rotation;
            startAngle = transform.eulerAngles.z;
            //targetRotation = Quaternion.FromToRotation(Vector3.up, dockPos.position - transform.position);
            targetAngle = GetAngleBetween(transform.position, dockPos.position);
            rotStep = Mathf.Abs(Mathf.DeltaAngle(startAngle, targetAngle)) / 180f;
            for (float f = 0; f < 1f; f += Time.deltaTime / rotStep)
            {
                //transform.rotation = Quaternion.Lerp(startRot, targetRotation, speedCurve.Evaluate(f));
                transform.eulerAngles =
                    new Vector3(0, 0, Mathf.LerpAngle(startAngle, targetAngle, speedCurve.Evaluate(f)));
                transform.Translate(-Time.deltaTime * lateralSpeed * driftVector, Space.World);
                yield return null;
            }

            //StartCoroutine(RotateTowards());
            startPos = transform.position;
            //startRot = transform.rotation;
            startAngle = transform.eulerAngles.z;
            step = (float) MoveSpeed / Vector3.Distance(startPos, dockPos.position);
            for (float f = 0; f < 1f; f += Time.deltaTime * step * (float) dock.SpeedMult)
            {
                float t = speedCurve.Evaluate(f);
                float r = rotationCurve.Evaluate(f);
                transform.position = Vector3.Lerp(startPos, dockPos.position, t);
                transform.eulerAngles = new Vector3(0, 0, Mathf.LerpAngle(startAngle, dockPos.eulerAngles.z, r));
                //if (!rotating)
                //    transform.rotation = Quaternion.Lerp(startRot, dockPos.rotation, r);

                yield return null;
            }

            transform.eulerAngles = dockPos.eulerAngles;
            state = ShipState.Docked;
            while (!currentLoad.Empty)
            {
                if (!dock.cargo.Full)
                {
                    double[] toUnload = currentLoad.GetPercentOfCapacity(Time.deltaTime);
                    dock.cargo.AddMinerals(toUnload);
                    currentLoad.Remove(toUnload);
                }
                transform.position = dockPos.position;
                transform.eulerAngles = dockPos.eulerAngles;

                yield return null;
            }

            if (target.State != AsteroidState.Finished)
                miningLoop = StartCoroutine(MiningLoop());
            else
                state = ShipState.Waiting;
        }

        public void SetTarget(Asteroid asteroid)
        {
            target = asteroid;
        }

        public void Setup(int newIndex, Transform dockPoint, Transform minePoint, Outpost outpost, Asteroid asteroid)
        {
            dockPos = dockPoint;
            miningPos = minePoint;
            dock = outpost;
            SetTarget(asteroid);
            Setup(newIndex);
        }

        public double GetGoldPerSecond()
        {
            if (target)
                return target.GetGoldEarnedPerSecond(
                    miningSpeed * bonusSpeedMult * dock.MineSpeedMult, (efficiency * GameManager.Instance.efficiencyMult) + dock.EfficiencyMult); // * GetMiningPercent();
            return miningSpeed * bonusSpeedMult; // * GetMiningPercent();
        }

        public void Setup(int newIndex)
        {
            index = newIndex;
            transform.position = dockPos.position;
            if (miningLoop != null)
                StopCoroutine(miningLoop);
            currentLoad.Reset();
            laserObject.gameObject.SetActive(false);
            laserHit.SetActive(false);
            if (newIndex < 0)
            {
                gameObject.SetActive(false);
                return;
            }

            gameObject.SetActive(true);
            shipBody.sprite = ShipManager.Instance.GetShipSprite(index);
            int shipShape = ShipManager.Instance.GetShipShapeIndex(index);
            for (int i = 0; i < shipRenderers.Length; i++)
            {
                MeshRenderer shipRenderer = shipRenderers[i];
                if (i != shipShape)
                {
                    shipRenderer.gameObject.SetActive(false);
                    continue;
                }
                shipRenderer.gameObject.SetActive(true);
                shipRenderer.sharedMaterial = ShipManager.Instance.GetShipMaterial(index);
            }

            //Debug.Log(speed + ", " + ShipManager.Instance.GetFlySpeed(index));
            speed = ShipManager.Instance.GetFlySpeed(index);
            efficiency = ShipManager.Instance.GetEfficiency(index);
            miningSpeed = ShipManager.Instance.GetMiningSpeed(index);
            currentLoad.SetCapacity(ShipManager.Instance.GetCapacity(index) * dock.CapacityMult);
            if (target && target.State != AsteroidState.Finished)
            {
                state = ShipState.Docked;
                miningLoop = StartCoroutine(MiningLoop());
            }
            else
                state = ShipState.Waiting;
        }

        public static double GetGoldPerSecond(int shipIndex, Asteroid target, Outpost outpost)
        {
            double efficiency = ShipManager.Instance.GetEfficiency(shipIndex);
            double miningSpeed = ShipManager.Instance.GetMiningSpeed(shipIndex);
            return target.GetGoldEarnedPerSecond(
                miningSpeed * GameManager.Instance.globalMineSpeedMult,
                (efficiency * GameManager.Instance.efficiencyMult) + outpost.EfficiencyMult);
        }

        private IEnumerator MineEffects()
        {
            WaitForSeconds wait = new WaitForSeconds(0.333f);
            while (state == ShipState.Mining)
            {
                EffectManager.Instance.EmitAt(target.RockID, laserHit.transform.position, 1);
                yield return wait;
            }
        }

        private bool rotating = false;

        private IEnumerator RotateTowards()
        {
            rotating = true;
            Quaternion startRot = transform.rotation;
            Quaternion targetRotation = Quaternion.FromToRotation(Vector3.up, dockPos.position - transform.position);
            float rotStep = 180f / Mathf.Abs(Quaternion.Angle(startRot, targetRotation));
            for (float f = 0; f < 1f; f += Time.deltaTime * rotStep)
            {
                transform.rotation = Quaternion.Lerp(startRot, targetRotation, speedCurve.Evaluate(f));
                yield return null;
            }

            rotating = false;
        }

        private void CheckUpgrades(int shipIndex)
        {
            if (index == shipIndex)
            {
                shipBody.sprite = ShipManager.Instance.GetShipSprite(index);
                speed = ShipManager.Instance.GetFlySpeed(index);
                efficiency = ShipManager.Instance.GetEfficiency(index);
                miningSpeed = ShipManager.Instance.GetMiningSpeed(index);
                currentLoad.SetCapacity(ShipManager.Instance.GetCapacity(index) * dock.CapacityMult);
            }
        }

        private void LoadResearch(ResearchType researchType, double magnitude, int level)
        {
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
                CheckUpgrades(index);
        }

        private void UpgradeResearch(ResearchType researchType, double magnitude, int level)
        {
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
                CheckUpgrades(index);
        }

        // Start is called before the first frame update
        void OnEnable()
        {
            //transform.position = dockPos.position;
            if (miningLoop != null)
                StopCoroutine(miningLoop);
            //miningLoop = MiningLoop();
            //StartCoroutine(miningLoop);
            ShipManager.OnShipUpgrade += CheckUpgrades;
            ResearchManager.OnResearchLoaded += LoadResearch;
            ResearchManager.OnResearchUpgrade += UpgradeResearch;
        }

        private void OnDisable()
        {
            ShipManager.OnShipUpgrade -= CheckUpgrades;
            ResearchManager.OnResearchLoaded -= LoadResearch;
            ResearchManager.OnResearchUpgrade -= UpgradeResearch;
        }

        private void OnDestroy()
        {
            ShipManager.OnShipUpgrade -= CheckUpgrades;
            ResearchManager.OnResearchLoaded -= LoadResearch;
            ResearchManager.OnResearchUpgrade -= UpgradeResearch;
        }

        private bool isTrackedShip = false;

        public bool IsTrackedShip
        {
            set => isTrackedShip = value;
        }

        private IEnumerator PathToMainShip(Action<bool> onComplete)
        {
            state = ShipState.ToDock;
            Vector3 lastPos = transform.position;
            TempBezier cargoBay = new TempBezier(transform.position, transform.position + (transform.up * 5f));
            TempBezier mainShip = new TempBezier(GameManager.Instance.GetMainShipBay(transform.position));
            float speedMult = Random.Range(1.25f, 2f);
            laserObject.gameObject.SetActive(false);
            laserHit.SetActive(false);
            BezierPath bezierPath = new BezierPath(cargoBay, mainShip);
            Vector3 pos = cargoBay.point;
            do
            {
                lastPos = transform.position;
                transform.position = pos;
                //transform.up = transform.position - lastPos;
                transform.eulerAngles = Vector3.forward * GetAngleBetween(lastPos, transform.position);
                speedMult += (0.2f * Time.deltaTime);
                //Debug.Log("Distance travelled: " + Vector3.Distance(lastPos, cargoShip.position));
                yield return null;
            } while (!bezierPath.TraversePath((float)MoveSpeed * speedMult * Time.deltaTime, out pos));

            onComplete?.Invoke(isTrackedShip);
            gameObject.SetActive(false);
        }

        public void RetreatShip(Action<bool> onComplete)
        {
            if (miningLoop != null)
                StopCoroutine(miningLoop);
            miningLoop = StartCoroutine(PathToMainShip(onComplete));
        }

        public bool IsMining => gameObject.activeSelf && state == ShipState.Mining;

        private float nextCheck = 1f;

        // Update is called once per frame
        private void Update()
        {
            if (state == ShipState.Waiting)
            {
                transform.position = dockPos.position;
                transform.eulerAngles = dockPos.eulerAngles;
            }
            if (nextCheck > 0)
            {
                nextCheck -= Time.deltaTime;
            }
            else
            {
                nextCheck = 1f;
                if (state != ShipState.Mining) 
                    return;
                if (DebrisManager.Instance.ForceDebris || (target.DebrisReady && Random.value < 0.01f))
                {
                    int gems = FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) ? Random.Range(0, 6) : 0;
                    DebrisManager.Instance.CreateDebris(gems, laserHit.transform.position, target.DebrisIndex);
                    target.SpawnDebris();
                    target.ResetCooldown();
                }
            }
        }
    }

}