using System;
using Core.Gameplay.Version;
using UnityEngine;

namespace Core.Gameplay.Ships
{
    [Serializable]
    public class Ship
    {
        public string shipName, description;
        public Sprite shipSprite, uiSprite;

        public Material shipMaterial;

        public double baseCost, purchaseCost;
        public double miningSpeed, flySpeed, capacity, efficiency;

        public int shipLevel = -1, currentCount, totalPurchased;
        public int baseCombatEffect = 2;
        public int shipShapeIndex;

        [Header("New Attributes")]
        public double newMiningSpeed;

        public double MiningSpeed => GameVersionManager.CurrentVersion switch
        {
            GameVersion.Version1 => miningSpeed,
            GameVersion.Version2 => newMiningSpeed,
            GameVersion.Version3 => newMiningSpeed // Replace with data (global data)
        };

        public double Capacity => newMiningSpeed * 3;

        public static double GetShipMiningSpeed(int level)
        {
            GlobalData data = GlobalDataManager.Instance.Data;
            return data.MinerBaseMultiplier * Math.Pow(data.MinerIncrementMultiplier, level);
        }

        public static double GetCapacityAtLevel(int level)
        {
            return GetShipMiningSpeed(level) * 3;
        }
    }
}