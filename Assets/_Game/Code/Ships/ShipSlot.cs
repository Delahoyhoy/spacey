﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Ships
{
    public class ShipSlot : MonoBehaviour
    {
        [SerializeField]
        private Text nameText, countText;

        [SerializeField]
        private Image shipImage;

        [SerializeField]
        private Button selection;

        [SerializeField]
        private GameObject lockPanel;

        private ShipSelection menuRef;
        private int shipIndex;

        public void Setup(int index, ShipSelection menu)
        {
            menuRef = menu;
            shipIndex = index;
            Ship ship = ShipManager.Instance.GetShip(index);
            if (ship.shipLevel < 0)
            {
                lockPanel.SetActive(true);
                selection.interactable = false;
            }
            else
            {
                lockPanel.SetActive(false);
                selection.interactable = true;
                nameText.text = ship.shipName;
                countText.text = CurrencyController.FormatToString(ship.currentCount);
                shipImage.sprite = ship.uiSprite;
            }
        }

        public void Refresh()
        {
            Ship ship = ShipManager.Instance.GetShip(shipIndex);
            if (ship.shipLevel < 0)
            {
                lockPanel.SetActive(true);
                selection.interactable = false;
            }
            else
            {
                lockPanel.SetActive(false);
                selection.interactable = true;
                nameText.text = ship.shipName;
                countText.text = CurrencyController.FormatToString(ship.currentCount);
                shipImage.sprite = ship.uiSprite;
            }
        }

        public void Select()
        {
            if (menuRef)
                menuRef.SelectShip(shipIndex);
        }
    }

}