using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Ships
{
    public class CargoShip : MonoBehaviour
    {
        private double[] mineralLoad;

        [SerializeField]
        private float baseMoveSpeed;

        private float speedMultiplier = 1;
        private float bonusSpeedMult = 1;

        private Action completePathAction;

        private BezierPath cargoPath;

        private Transform cargoTransform;

        private bool setup;
        
        private void Start()
        {
            if (setup)
                return;
            SetBonusMult(GameManager.Instance.BonusMultiplier);
            GameManager.OnSetBonusActive += SetBonusMult;
            setup = true;
        }

        private void OnDestroy()
        {
            GameManager.OnSetBonusActive -= SetBonusMult;
        }

        private void SetBonusMult(double multiplier)
        {
            bonusSpeedMult = (float)multiplier;
        }
        
        public void Setup(MineralLoad load, BezierPath path, Action onComplete, float speedMult = 1f)
        {
            cargoTransform = transform;
            mineralLoad = load.GetLoad();
            cargoPath = path;
            completePathAction = onComplete;
            transform.position = path.startPosition;
            speedMultiplier = speedMult;
        }

        private void Update()
        {
            Vector3 lastPos = cargoTransform.position;
            if (!cargoPath.TraversePath(baseMoveSpeed * speedMultiplier * bonusSpeedMult * Time.deltaTime, out Vector3 position))
            {
                cargoTransform.position = position;
                cargoTransform.up = position - lastPos;
                return;
            }
            gameObject.SetActive(false);
            double total = 0;
            for (int i = 0; i < mineralLoad.Length; i++)
                total += mineralLoad[i] * TierHelper.GetValue((Tier) i);
            CurrencyController.Instance.AddCurrency(total * GameManager.Instance.IncomeMultiplier, CurrencyType.SoftCurrency,
                true, "mining", "passive");
            completePathAction?.Invoke();
        }
    }
}
