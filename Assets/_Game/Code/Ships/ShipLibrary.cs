﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Ships
{
    public class ShipLibrary : UIScreen
    {
        [SerializeField]
        private Button left, right;

        [SerializeField]
        private Image shipIcon;

        [SerializeField]
        private GameObject statBlock;

        [SerializeField]
        private Text nameText, mineSpeed, efficiency, cargoCap, flySpeed;

        [SerializeField]
        private Sprite lockedSprite;

        private int currentIndex = 0;

        public override void Open()
        {
            base.Open();
            SelectIndex(GameManager.Instance.HighestUnlocked);
        }

        public void SelectIndex(int index)
        {
            currentIndex = index;
            left.interactable = index > 0;
            right.interactable = index < ShipManager.Instance.GetTotalShipTypes() - 1;
            if (GameManager.Instance.HighestUnlocked >= index)
            {
                shipIcon.sprite = ShipManager.Instance.GetUISprite(index);
                nameText.text = ShipManager.Instance.GetShip(index).shipName.ToUpper();
                statBlock.SetActive(true);
                flySpeed.text = ShipManager.Instance.GetFlySpeed(index).ToString("##0.#") + "\nm/s";
                double miningSpeed = ShipManager.Instance.GetMiningSpeed(index);
                mineSpeed.text =
                    (miningSpeed < 1000 ?
                        miningSpeed.ToString("##0.##") :
                        CurrencyController.FormatToString(miningSpeed)) + "\ntons/s";
                efficiency.text = ShipManager.Instance.GetEfficiency(index).ToString("###0.##") + "%";
                double capacity = ShipManager.Instance.GetCapacity(index);
                cargoCap.text =
                    (capacity < 1000 ? capacity.ToString("##0.##") : CurrencyController.FormatToString(capacity)) +
                    "\ntons";
            }
            else
            {
                nameText.text = "LOCKED";
                shipIcon.sprite = lockedSprite;
                statBlock.SetActive(false);
            }
        }

        public void Next()
        {
            SelectIndex(currentIndex + 1);
            AudioManager.Play("Click", 0.5f);
        }

        public void Prev()
        {
            SelectIndex(currentIndex - 1);
            AudioManager.Play("Click", 0.5f);
        }
    }

}