namespace Core.Gameplay.Ships
{
    public enum PurchaseCount
    {
        BuyOne,
        BuyTen,
        BuyMax,
    }
}