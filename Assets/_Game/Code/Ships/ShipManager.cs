﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;
using Core.Gameplay.Research;
using Core.Gameplay.Outposts;
using Core.Gameplay.Save;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using UnityEngine.Serialization;

namespace Core.Gameplay.Ships
{
    public class ShipManager : MonoBehaviour, ISaveLoad
    {
        [SerializeField]
        private Ship[] shipTemplates;

        [SerializeField]
        private int[] levelBoundaries = new int[] {9, 49, 99, 249, 499, 999, 4999, 9999, 49999, 99999, 499999, 999999};

        private static ShipManager instance;

        public static ShipManager Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<ShipManager>();
                return instance;
            }
        }

        public delegate void ShipUpgraded(int shipIndex);

        public static event ShipUpgraded OnShipUpgrade;

        [SerializeField]
        private UIScreen upgradesUI;

        [SerializeField]
        private ShipUpgrade upgradePrefab;

        private List<ShipUpgrade> upgradePanels = new List<ShipUpgrade>();

        [SerializeField]
        private Transform upgradesParent;

        public UpgradeFormat upgradeFormat = UpgradeFormat.OneLevel;
        public PurchaseCount purchaseCount = PurchaseCount.BuyOne;

        [SerializeField]
        private Text buttonText, purchaseCountText;

        [SerializeField]
        private GameObject selectNone;

        public Outpost targetOutpost = null;
        public int targetSlot = 0;

        [SerializeField]
        private OutpostMenu outpostMenu;

        [SerializeField]
        private ShipLibrary shipLibraryMenu;

        [SerializeField]
        private ShipsVersion[] versions;

        private bool shipsInitialised;

        public Ship[] ShipTemplates
        {
            get
            {
                if (shipsInitialised)
                    return shipTemplates;
                for (int i = 0; i < versions.Length; i++)
                {
                    GameVersion version = GameVersionManager.CurrentVersion;
                    if (!versions[i].GetValid(version, out Ship[] ships))
                        continue;
                    shipsInitialised = true;
                    shipTemplates = ships;
                    break;
                }
                return shipTemplates;
            }
        }

        private double mineMult = 1d, speedMult = 1d, efficiencyMult = 1d, cargoMult = 1d;

        public void SetTarget(Outpost outpost, int slot = 0)
        {
            //TutorialManager.OnTrigger("ships");
            targetOutpost = outpost;
            targetSlot = slot;
            selectNone.SetActive(FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) && targetOutpost != null);
        }

        public void RegisterNewVersion()
        {
            shipsInitialised = false;
        }

        public void OpenMenu()
        {
            upgradesUI.Open();
        }

        public void SelectNone()
        {
            if (targetOutpost != null && targetSlot >= 0)
            {
                if (targetOutpost.GetShipAtIndex(targetSlot) >= 0)
                    AddShip(targetOutpost.GetShipAtIndex(targetSlot));
                targetOutpost.SetShip(targetSlot, -1);
                Refresh();
                upgradesUI.Close();
            }
        }

        public void OpenOnNone()
        {
            SetTarget(null);
            OpenMenu();
        }

        public void SelectShip(int ship)
        {
            if (targetOutpost != null && targetSlot >= 0)
            {
                if (targetOutpost.GetShipAtIndex(targetSlot) >= 0)
                    AddShip(targetOutpost.GetShipAtIndex(targetSlot));
                targetOutpost.SetShip(targetSlot, ship);
                RemoveShip(ship);
                //TutorialManager.OnTrigger("select");
                Refresh();
                upgradesUI.Close();
                outpostMenu.Refresh();
            }
        }

        public void OpenLibrary(int shipIndex)
        {
            if (shipIndex < 0)
                return;
            if (!shipLibraryMenu)
                return;
            shipLibraryMenu.Open();
            shipLibraryMenu.SelectIndex(shipIndex);
        }

        private void SetPurchaseCount(PurchaseCount newCount)
        {
            purchaseCount = newCount;
            switch (purchaseCount)
            {
                case PurchaseCount.BuyMax:
                    purchaseCountText.text = "MAX";
                    break;
                case PurchaseCount.BuyOne:
                    purchaseCountText.text = "1";
                    break;
                case PurchaseCount.BuyTen:
                    purchaseCountText.text = "10";
                    break;
            }

            Refresh();
        }

        public void CyclePurchaseCount()
        {
            AudioManager.Play("Click", 0.75f);
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            if ((int) purchaseCount >= Enum.GetValues(typeof(PurchaseCount)).Length - 1)
                purchaseCount = 0;
            else
                purchaseCount++;
            SetPurchaseCount(purchaseCount);
        }

        public void UpgradeResearch(ResearchType researchType, double magnitude, int level)
        {
            if (GameVersionManager.CurrentVersion == GameVersion.Version1)
                return;
            switch (researchType)
            {
                case ResearchType.CargoSpeed:
                    speedMult = 1d + (magnitude / 100d);
                    break;
                case ResearchType.CargoCap:
                    cargoMult = 1d + (magnitude / 100d);
                    break;
                case ResearchType.TapEfficiency:
                    efficiencyMult = 1d + (magnitude / 100d);
                    break;
                case ResearchType.TapSpeed:
                    mineMult = 1d + (magnitude / 100d);
                    break;
            }
        }

        public void LoadResearch(ResearchType researchType, double magnitude, int level)
        {
            if (GameVersionManager.CurrentVersion == GameVersion.Version1)
                return;
            switch (researchType)
            {
                case ResearchType.CargoSpeed:
                    speedMult = 1d + (magnitude / 100d);
                    break;
                case ResearchType.CargoCap:
                    cargoMult = 1d + (magnitude / 100d);
                    break;
                case ResearchType.TapEfficiency:
                    efficiencyMult = 1d + (magnitude / 100d);
                    break;
                case ResearchType.TapSpeed:
                    mineMult = 1d + (magnitude / 100d);
                    break;
            }
        }

        public int GetCurrentShipCount(int index)
        {
            return ShipTemplates[index].currentCount;
        }

        public int GetTotalShipTypes()
        {
            return ShipTemplates.Length;
        }

        public void TutorialMenu(int stage)
        {
            switch (stage)
            {
                case 0:
                    for (int i = 1; i < upgradePanels.Count; i++)
                    {
                        upgradePanels[i].gameObject.SetActive(false);
                    }

                    selectNone.SetActive(false);
                    break;
                case 1:
                    //TutorialManager.Instance.DoCustomHighlight(upgradePanels[0].GetBuyButton(), 0.05f, HighlightType.Screen);
                    for (int i = 1; i < upgradePanels.Count; i++)
                    {
                        upgradePanels[i].gameObject.SetActive(false);
                    }

                    selectNone.SetActive(false);
                    break;
                case 2:
                    //TutorialManager.Instance.DoCustomHighlight(upgradePanels[0].GetShipIcon(), 0.05f, HighlightType.Screen);
                    for (int i = 1; i < upgradePanels.Count; i++)
                    {
                        upgradePanels[i].gameObject.SetActive(false);
                    }

                    selectNone.SetActive(false);
                    break;
                default:
                    for (int i = 0; i < upgradePanels.Count; i++)
                    {
                        upgradePanels[i].gameObject.SetActive(true);
                    }

                    break;
            }
        }

        public Ship GetShip(int index)
        {
            return ShipTemplates[index];
        }

        public int Count(int shipIndex)
        {
            if (shipIndex < ShipTemplates.Length)
                return ShipTemplates[shipIndex].currentCount;
            return 0;
        }

        public int GetNextBoundary(int currentLevel)
        {
            for (int i = 0; i < levelBoundaries.Length; i++)
            {
                if (currentLevel < levelBoundaries[i])
                    return levelBoundaries[i];
            }

            return int.MaxValue;
        }

        public int Level(int shipIndex)
        {
            if (shipIndex < ShipTemplates.Length)
                return ShipTemplates[shipIndex].shipLevel;
            return -1;
        }

        private int GetBoundaries(int level)
        {
            int total = 0;
            for (int i = 0; i < levelBoundaries.Length; i++)
            {
                if (level >= levelBoundaries[i])
                    total++;
                else
                {
                    //Debug.Log(total);
                    return total;
                }
            }

            return total;
        }

        private void OnEnable()
        {
            GameManager.OnSecondUpdate += SecondUpdate;
            ResearchManager.OnResearchLoaded += LoadResearch;
            ResearchManager.OnResearchUpgrade += UpgradeResearch;
        }

        private void OnDisable()
        {
            GameManager.OnSecondUpdate -= SecondUpdate;
            ResearchManager.OnResearchLoaded -= LoadResearch;
            ResearchManager.OnResearchUpgrade -= UpgradeResearch;
        }

        private void OnDestroy()
        {
            GameManager.OnSecondUpdate -= SecondUpdate;
            ResearchManager.OnResearchLoaded -= LoadResearch;
            ResearchManager.OnResearchUpgrade -= UpgradeResearch;
        }

        public Sprite GetShipSprite(int shipIndex)
        {
            return ShipTemplates[shipIndex].shipSprite;
        }

        public Material GetShipMaterial(int shipIndex)
        {
            return ShipTemplates[shipIndex].shipMaterial;
        }

        public int GetShipShapeIndex(int shipIndex)
        {
            return ShipTemplates[shipIndex].shipShapeIndex;
        }

        public Sprite GetUISprite(int shipIndex)
        {
            return ShipTemplates[shipIndex].uiSprite;
        }

        public double GetMiningSpeed(int shipIndex)
        {
            int shipLevel = Level(shipIndex);
            return GetMiningSpeed(shipIndex, shipLevel);
        }

        public double GetMiningSpeed(int shipIndex, int shipLevel)
        {
            double baseSpeed = ShipTemplates[shipIndex].miningSpeed;
            switch (GameVersionManager.CurrentVersion)
            {
                case GameVersion.Version1:
                    if (shipLevel <= 0)
                        return baseSpeed;
                    int quotient = (shipLevel + 3) / 4;
                    return (baseSpeed +
                            ((quotient * (baseSpeed / 10)) + ((baseSpeed * Math.Pow(quotient, 1.05d)) / 17.5d))) *
                           (GetBoundaries(shipLevel) + 1);
                case GameVersion.Version2:
                    return ShipTemplates[shipIndex].MiningSpeed * mineMult;
                case GameVersion.Version3:
                    return Ship.GetShipMiningSpeed(shipIndex) * mineMult;
                    
            }

            return ShipTemplates[shipIndex].MiningSpeed * mineMult;
        }

        public double GetFlySpeed(int shipIndex)
        {
            int shipLevel = Level(shipIndex);
            return GetFlySpeed(shipIndex, shipLevel);
        }

        public double GetFlySpeed(int shipIndex, int shipLevel)
        {
            double baseSpeed = ShipTemplates[shipIndex].flySpeed;
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
                return baseSpeed * speedMult;
            if (shipLevel <= 0)
                return baseSpeed;

            return baseSpeed + ((shipLevel + 2) / 4) * (baseSpeed / 50);
        }

        public double GetCapacity(int shipIndex)
        {
            int shipLevel = Level(shipIndex);
            return GetCapacity(shipIndex, shipLevel);
        }

        public double GetCapacity(int shipIndex, int shipLevel)
        {
            double baseCapacity = ShipTemplates[shipIndex].capacity;
            switch (GameVersionManager.CurrentVersion)
            {
                case GameVersion.Version1:
                    if (shipLevel <= 0)
                        return baseCapacity;
                    //Debug.Log("Cap mod = " + System.Math.Pow(2, GetBoundaries(shipLevel)));
                    //return baseCapacity + ((((shipLevel + 1) / 4) * (baseCapacity / 10)) * (GetBoundaries(shipLevel) + 1));
                    int quotient = (shipLevel + 1) / 4;
                    return (baseCapacity + (((quotient * (baseCapacity / 10)) +
                                             ((baseCapacity * Math.Pow(quotient, 1.05d)) / 17.5d)))) *
                           (GetBoundaries(shipLevel) + 1);
                case GameVersion.Version2:
                    return ShipTemplates[shipIndex].Capacity * cargoMult;
                case GameVersion.Version3:
                    return Ship.GetCapacityAtLevel(shipIndex) * cargoMult;
            }

            return ShipTemplates[shipIndex].Capacity * cargoMult;
        }

        public double GetEfficiency(int shipIndex)
        {
            int shipLevel = Level(shipIndex);
            return GetEfficiency(shipIndex, shipLevel);
        }

        public double GetEfficiency(int shipIndex, int shipLevel)
        {
            double baseEfficiency = ShipTemplates[shipIndex].efficiency;
            if (GameVersionManager.CurrentVersion != GameVersion.Version1)
                return baseEfficiency * efficiencyMult;
            if (shipLevel <= 0)
                return baseEfficiency;

            return baseEfficiency + ((shipLevel / 4) * (baseEfficiency / 20));
        }

        public double GetLevelUpCost(int shipIndex, int levelsUp = 1)
        {
            if (shipIndex >= ShipTemplates.Length)
                return 0;
            //if (shipTemplates[shipIndex].shipLevel < 0)
            //{
            //    double total = 0;
            //    double lastValue = shipTemplates[shipIndex].baseCost;
            //    for (int i = 0; i < levelsUp; i++)
            //    {
            //        total += lastValue;
            //        lastValue = System.Math.Floor(lastValue + (lastValue / 18));
            //    }
            //    return total;
            //}
            double total = 0;
            double lastValue = ShipTemplates[shipIndex].baseCost;
            for (int i = 0; i <= ShipTemplates[shipIndex].shipLevel; i++)
            {
                lastValue = Math.Floor(lastValue + (lastValue / 20));
            }

            for (int i = 0; i < levelsUp; i++)
            {
                total += lastValue;
                lastValue = Math.Floor(lastValue + (lastValue / 20));
            }

            return total;
        }

        public double GetCostToPurchase(int shipIndex)
        {
            int alreadyPurchased = ShipTemplates[shipIndex].totalPurchased;
            return GetCostToPurchase(shipIndex, alreadyPurchased);
        }

        public double GetSalvage(int shipIndex, int count)
        {
            double total = 0;
            int alreadyPurchased = ShipTemplates[shipIndex].totalPurchased;
            for (int i = 0; i < count; i++)
            {
                total += GetCostToPurchase(shipIndex, alreadyPurchased - 1 - i);
            }

            return total / 2d;
        }

        public double GetCostToPurchase(int shipIndex, int alreadyPurchased)
        {
            if (alreadyPurchased <= 0)
            {
                if (shipIndex == 0)
                    return 50d;
                return ShipTemplates[shipIndex].purchaseCost;
            }

            double value = ShipTemplates[shipIndex].purchaseCost;
            for (int i = 0; i < alreadyPurchased; i++)
            {
                value = Math.Floor(value + (value / 3));
            }

            return value;
        }

        public void Purchase(int shipIndex)
        {
            double cost = GetCostToPurchase(shipIndex);
            if (CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost)
            {
                AudioManager.Play("Confirm", 1f);
                StatTracker.Instance.IncrementStat("shipsBought", 1);
                CurrencyController.Instance.AddCurrency(-cost, CurrencyType.SoftCurrency, false, "ships", "buy_ship");
                ShipTemplates[shipIndex].currentCount++;
                ShipTemplates[shipIndex].totalPurchased++;
            }

            Refresh();
        }

        public void RemoveShip(int shipIndex)
        {
            ShipTemplates[shipIndex].currentCount--;
        }

        public void DestroyShip(int shipIndex)
        {
            RemoveShip(shipIndex);
            ShipTemplates[shipIndex].totalPurchased--;
        }

        public void AddShip(int shipIndex)
        {
            ShipTemplates[shipIndex].currentCount++;
        }

        public int TotalShips
        {
            get { return ShipTemplates.Length; }
        }

        public int GetTotalShipCount()
        {
            int total = 0;
            for (int i = 0; i < ShipTemplates.Length; i++)
                total += ShipTemplates[i].currentCount;
            return total;
        }

        public void UpgradeBy(int shipIndex, int levels = 1)
        {
            int targetLevel = ShipTemplates[shipIndex].shipLevel + levels;
            UpgradeTo(shipIndex, targetLevel);
        }

        public void CycleFormat()
        {
            AudioManager.Play("Click", 0.75f);
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            if ((int) upgradeFormat >= Enum.GetValues(typeof(UpgradeFormat)).Length - 1)
                upgradeFormat = 0;
            else
                upgradeFormat++;
            switch (upgradeFormat)
            {
                case UpgradeFormat.OneLevel:
                    buttonText.text = "x1";
                    break;
                case UpgradeFormat.TenLevels:
                    buttonText.text = "x10";
                    break;
                case UpgradeFormat.HundredLevels:
                    buttonText.text = "x100";
                    break;
                case UpgradeFormat.NextBoundary:
                    buttonText.text = "NEXT";
                    break;
            }

            Refresh();
        }

        public int TotalShipTemplates
        {
            get { return ShipTemplates.Length; }
        }

        public int GetCombatEffect(int shipIndex)
        {
            int boundaries = GetBoundaries(ShipTemplates[shipIndex].shipLevel);
            return ShipTemplates[shipIndex].baseCombatEffect + boundaries;
        }

        public int GetMaxPurchasable(int shipIndex)
        {
            if (shipIndex < 0 || shipIndex >= ShipTemplates.Length)
                return 0;
            double currentGold = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency);
            double total = 0;
            int count = 0;
            int startPurchaseCount = ShipTemplates[shipIndex].totalPurchased;
            while (total <= currentGold)
            {
                total += GetCostToPurchase(shipIndex, startPurchaseCount + count);
                if (total > currentGold)
                    break;
                count++;
            }

            return count;
        }

        public double GetCostForCount(int shipIndex, int count)
        {
            if (shipIndex < 0 || shipIndex >= ShipTemplates.Length)
                return 0;
            if (count <= 0)
                return GetCostToPurchase(shipIndex);
            double total = 0;
            int startPurchaseCount = ShipTemplates[shipIndex].totalPurchased;
            for (int i = 0; i < count; i++)
            {
                total += GetCostToPurchase(shipIndex, startPurchaseCount + i);
            }

            return total;
        }

        private bool IsMilestone(int index)
        {
            for (int i = 0; i < levelBoundaries.Length; i++)
            {
                if (levelBoundaries[i] == index)
                    return true;
                if (index > levelBoundaries[i])
                    return false;
            }

            return false;
        }

        public void UpgradeTo(int shipIndex, int toLevel)
        {
            bool firstUnlock = ShipTemplates[shipIndex].shipLevel < 0;
            double upgradeCost = GetCostToGetToLevel(shipIndex, toLevel);
            if (CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= upgradeCost)
            {
                StatTracker.Instance.IncrementStat("upgrades", toLevel - ShipTemplates[shipIndex].shipLevel);
                CurrencyController.Instance.AddCurrency(-upgradeCost, CurrencyType.SoftCurrency, false, "ships", "upgrade_ship");

                AudioManager.Play(IsMilestone(toLevel) ? "Milestone" : "Upgrade", 1f);
                ShipTemplates[shipIndex].shipLevel = toLevel;
                if (OnShipUpgrade != null)
                    OnShipUpgrade(shipIndex);
                if (firstUnlock)
                    ShipTemplates[shipIndex].currentCount++;
            }

            if (OnShipUpgrade != null)
                OnShipUpgrade(shipIndex);
            Refresh();
        }

        public void ClearCurrentShips()
        {
            for (int i = 0; i < ShipTemplates.Length; i++)
            {
                ShipTemplates[i].currentCount = 0;
            }
        }

        public void UpgradeShip(int ship)
        {
            if (ShipTemplates[ship].shipLevel < 0)
            {
                UpgradeBy(ship, 1);
                //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
                /*
            List<Firebase.Analytics.Parameter> parameters = new List<Firebase.Analytics.Parameter>();
            parameters.Add(new Firebase.Analytics.Parameter("ShipIndex", ship));
            AnalyticsManager.Instance.TriggerEvent(AnalyticsEvent.NewShipUnlock);*/
                return;
            }

            switch (upgradeFormat)
            {
                case UpgradeFormat.OneLevel:
                    UpgradeBy(ship, 1);
                    break;
                case UpgradeFormat.TenLevels:
                    UpgradeBy(ship, 10);
                    break;
                case UpgradeFormat.HundredLevels:
                    UpgradeBy(ship, 100);
                    break;
                case UpgradeFormat.NextBoundary:
                    UpgradeTo(ship, GetNextBoundary(Level(ship)));
                    break;
            }
        }

        public bool GetNextAvailable()
        {
            for (int i = 0; i < ShipTemplates.Length; i++)
            {
                if (ShipTemplates[i].shipLevel < 0 &&
                    GetLevelUpCost(i, 1) <= CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency))
                    return true;
                double cost = GetCostToGetToLevel(i, GetNextBoundary(ShipTemplates[i].shipLevel));
                if (CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost)
                    return true;
            }

            return false;
        }

        public float GetBoundaryPoint(int shipIndex)
        {
            int currentLevel = ShipTemplates[shipIndex].shipLevel;
            int minLevel = -1;
            for (int i = 0; i < levelBoundaries.Length; i++)
            {
                if (currentLevel >= levelBoundaries[i])
                {
                    minLevel = i;
                }
                else
                {
                    break;
                }
            }

            int min = minLevel < 0 ? 0 : levelBoundaries[minLevel],
                max = minLevel + 1 >= levelBoundaries.Length ? int.MaxValue : levelBoundaries[minLevel + 1];
            return (float) (currentLevel - min) / (max - min);
        }

        public double GetCostToGetToLevel(int shipIndex, int level)
        {
            if (shipIndex >= ShipTemplates.Length)
                return 0;
            if (ShipTemplates[shipIndex].shipLevel >= level)
                return 0;
            int countToDo = level - ShipTemplates[shipIndex].shipLevel;
            return GetLevelUpCost(shipIndex, countToDo);
        }

        private void LoadShip(int index, string data)
        {
            string[] splitData = data.Split(',');
            int.TryParse(splitData[0], out ShipTemplates[index].currentCount);
            int.TryParse(splitData[1], out ShipTemplates[index].shipLevel);
            int.TryParse(splitData[2], out ShipTemplates[index].totalPurchased);
        }

        private string SaveShip(int shipIndex)
        {
            return ShipTemplates[shipIndex].currentCount.ToString() + ", " +
                   ShipTemplates[shipIndex].shipLevel.ToString() + ", " +
                   ShipTemplates[shipIndex].totalPurchased.ToString();
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            for (int i = 0; i < ShipTemplates.Length; i++)
            {
                saveData.Add("w0s" + i, SaveShip(i));
            }
        }

        private void SecondUpdate()
        {
            if (upgradesUI.IsOpen)
            {
                Refresh();
            }
        }

        private void Refresh()
        {
            for (int i = 0; i < upgradePanels.Count; i++)
                upgradePanels[i].Refresh();
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            for (int i = 0; i < ShipTemplates.Length; i++)
            {
                if (loadedData.HasTag("w0s" + i))
                    LoadShip(i, loadedData.LoadData("w0s" + i, "0,-1,0"));
                ShipUpgrade newUpgrade = Instantiate(upgradePrefab, upgradesParent);
                newUpgrade.SetupFromShip(i);
                upgradePanels.Add(newUpgrade);
                if (OnShipUpgrade != null)
                    OnShipUpgrade(i);
            }
        }

        public void LoadDefault()
        {
            for (int i = 0; i < ShipTemplates.Length; i++)
            {
                ShipTemplates[i].currentCount = 0;
                ShipTemplates[i].shipLevel = i == 0 ? 0 : -1;
                ShipTemplates[i].totalPurchased = 0;
                ShipUpgrade newUpgrade = Instantiate(upgradePrefab, upgradesParent);
                newUpgrade.SetupFromShip(i);
                upgradePanels.Add(newUpgrade);
            }
        }
    }

}