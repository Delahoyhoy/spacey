﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Core.Gameplay.Outposts;

namespace Core.Gameplay.Ships
{
    public class ShipSelection : UIScreen
    {
        private Outpost selectedOutpost;
        private int selectedIndex = 0, selectedShip = 0;

        private bool fromOutpost = false;

        [SerializeField]
        private ShipSlot slotPrefab;

        private List<ShipSlot> allSlots = new List<ShipSlot>();

        [SerializeField]
        private RectTransform slotParent;

        [SerializeField]
        private Text selectionName;

        [SerializeField]
        private Text purchaseCostText;

        [SerializeField]
        private Button buyButton, equipButton;

        [SerializeField]
        private OutpostMenu outpostMenu;

        [SerializeField]
        private GameObject statBlock;

        [SerializeField]
        private Text speedText, mineText, efficiencyText, capacityText;

        public void Open(bool outpostScreen, Outpost outpost = null, int index = 0)
        {
            fromOutpost = outpostScreen;
            if (fromOutpost)
            {
                if (outpost != null)
                    selectedOutpost = outpost;
                selectedIndex = index;
            }

            //TutorialManager.OnTrigger("ships");
            Setup();
            selectionName.text = "PLEASE SELECT A SHIP ABOVE";
            statBlock.SetActive(false);
            buyButton.gameObject.SetActive(false);
            equipButton.gameObject.SetActive(false);
            base.Open();
        }

        public void OpenFromElsewhere()
        {
            Open(false);
        }

        private bool setup = false;

        private void Setup()
        {
            if (!setup)
            {
                for (int i = 0; i < ShipManager.Instance.TotalShips; i++)
                {
                    ShipSlot newSlot = Instantiate(slotPrefab, slotParent);
                    newSlot.Setup(i, this);
                    allSlots.Add(newSlot);
                }

                setup = true;
            }
            else
            {
                for (int i = 0; i < allSlots.Count; i++)
                    allSlots[i].Refresh();
            }
        }

        public void RefreshSlots()
        {
            for (int i = 0; i < allSlots.Count; i++)
                allSlots[i].Refresh();
        }

        public void SelectShip(int shipIndex)
        {
            if (!fromOutpost)
                equipButton.gameObject.SetActive(false);
            else
                equipButton.gameObject.SetActive(ShipManager.Instance.GetShip(shipIndex).currentCount > 0);
            AudioManager.Play("Click", 1f);
            //Debug.Log("SHIP INDEX IS " + shipIndex);
            if (shipIndex == 0)
                //TutorialManager.OnTrigger("jay");
            buyButton.gameObject.SetActive(true);
            selectedShip = shipIndex;
            Ship ship = ShipManager.Instance.GetShip(shipIndex);
            selectionName.text = ship.shipName;
            double cost = ShipManager.Instance.GetCostToPurchase(shipIndex);
            purchaseCostText.text = CurrencyController.FormatToString(cost);
            buyButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost;
            statBlock.SetActive(true);
            speedText.text = ShipManager.Instance.GetFlySpeed(shipIndex).ToString("##0.#") + " m/s";
            double miningSpeed = ShipManager.Instance.GetMiningSpeed(shipIndex);
            mineText.text =
                (miningSpeed < 1000 ? miningSpeed.ToString("##0.##") : CurrencyController.FormatToString(miningSpeed)) +
                " tons/s";
            efficiencyText.text = ShipManager.Instance.GetEfficiency(shipIndex).ToString("###0.##") + " %";
            double capacity = ShipManager.Instance.GetCapacity(shipIndex);
            capacityText.text =
                (capacity < 1000 ? capacity.ToString("##0.##") : CurrencyController.FormatToString(capacity)) + " tons";
            RefreshSlots();
        }

        public void BuyShip()
        {
            ShipManager.Instance.Purchase(selectedShip);
            //TutorialManager.OnTrigger("buy");
            RefreshSlots();
            SelectShip(selectedShip);
        }

        public void ChooseShip()
        {
            if (ShipManager.Instance.GetShip(selectedShip).currentCount <= 0)
                return;
            if (fromOutpost)
            {
                if (selectedOutpost)
                {
                    if (selectedOutpost.GetShipAtIndex(selectedIndex) >= 0)
                        ShipManager.Instance.AddShip(selectedOutpost.GetShipAtIndex(selectedIndex));
                    selectedOutpost.SetShip(selectedIndex, selectedShip);
                    ShipManager.Instance.RemoveShip(selectedShip);
                    outpostMenu.Refresh();
                    //TutorialManager.OnTrigger("select");
                    Close();
                }
            }
        }

        public void OpenFromOutpost()
        {

        }
    }

}