﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Tutorial;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Ships
{
    public class ShipUpgrade : MonoBehaviour
    {
        private int index = 0;

        [SerializeField]
        private Text shipName, levelText, upgradeCost, unlockCost, countText, buyCost, buyText;

        [SerializeField]
        private Button unlockButton, upgradeButton, selection, buyButton;

        [SerializeField]
        private Slider nextBoost;

        [SerializeField]
        private Image shipSprite;

        [SerializeField]
        private GameObject unlockPanel;

        [SerializeField]
        private Text speedText, mineText, efficiencyText, capacityText;

        [SerializeField]
        private string colourText = "#00aa11";

        public Transform GetBuyButton()
        {
            return buyButton.transform;
        }

        public Transform GetShipIcon()
        {
            return shipSprite.transform;
        }

        public void SetupFromShip(int shipIndex)
        {
            index = shipIndex;
            Ship ship = ShipManager.Instance.GetShip(shipIndex);
            shipSprite.sprite = ship.uiSprite;
            shipName.text = ship.shipName;
            if (ship.shipLevel < 0)
            {
                unlockPanel.SetActive(true);
                double cost = ShipManager.Instance.GetLevelUpCost(index, 1);
                unlockButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost;
                unlockCost.text = CurrencyController.FormatToString(cost);
            }
            else
            {
                nextBoost.value = ShipManager.Instance.GetBoundaryPoint(index);
                double cost = 0;
                levelText.text = "Level " + CurrencyController.FormatToString(ship.shipLevel + 1);
                switch (ShipManager.Instance.upgradeFormat)
                {
                    case UpgradeFormat.OneLevel:
                        cost = ShipManager.Instance.GetLevelUpCost(shipIndex, 1);
                        break;
                    case UpgradeFormat.TenLevels:
                        cost = ShipManager.Instance.GetLevelUpCost(shipIndex, 10);
                        break;
                    case UpgradeFormat.HundredLevels:
                        cost = ShipManager.Instance.GetLevelUpCost(shipIndex, 100);
                        break;
                    case UpgradeFormat.NextBoundary:
                        cost = ShipManager.Instance.GetCostToGetToLevel(shipIndex,
                            ShipManager.Instance.GetNextBoundary(ship.shipLevel));
                        break;
                }

                upgradeCost.text = CurrencyController.FormatToString(cost);
                upgradeButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost &&
                                             FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core);
                SetupDisplayText();
                SetupBuyCost();
                countText.text = CurrencyController.FormatToString(ShipManager.Instance.Count(shipIndex));
            }
        }

        public void SetupDisplayText()
        {
            Ship ship = ShipManager.Instance.GetShip(index);
            int currentLevel = ship.shipLevel;
            int toLevel = currentLevel;
            switch (ShipManager.Instance.upgradeFormat)
            {
                case UpgradeFormat.OneLevel:
                    toLevel = currentLevel + 1;
                    break;
                case UpgradeFormat.TenLevels:
                    toLevel = currentLevel + 10;
                    break;
                case UpgradeFormat.HundredLevels:
                    toLevel = currentLevel + 100;
                    break;
                case UpgradeFormat.NextBoundary:
                    toLevel = ShipManager.Instance.GetNextBoundary(ship.shipLevel);
                    break;
            }

            double currentSpeed = ShipManager.Instance.GetFlySpeed(index),
                newSpeed = ShipManager.Instance.GetFlySpeed(index, toLevel);
            double currentMine = ShipManager.Instance.GetMiningSpeed(index),
                newMine = ShipManager.Instance.GetMiningSpeed(index, toLevel);
            double currentCapacity = ShipManager.Instance.GetCapacity(index),
                newCapacity = ShipManager.Instance.GetCapacity(index, toLevel);
            double currentEfficiency = ShipManager.Instance.GetEfficiency(index),
                newEfficiency = ShipManager.Instance.GetEfficiency(index, toLevel);
            speedText.text = Format(currentSpeed, newSpeed, "m/s");
            mineText.text = Format(currentMine, newMine, "t/s");
            capacityText.text = Format(currentCapacity, newCapacity, "tons");
            efficiencyText.text = Format(currentEfficiency, newEfficiency, "%");
        }

        private void SetupBuyCost()
        {
            int toPurchase = 1;
            switch (ShipManager.Instance.purchaseCount)
            {
                case PurchaseCount.BuyOne:
                    toPurchase = 1;
                    break;
                case PurchaseCount.BuyTen:
                    toPurchase = 10;
                    break;
                case PurchaseCount.BuyMax:
                    toPurchase = ShipManager.Instance.GetMaxPurchasable(index);
                    if (toPurchase <= 0)
                        toPurchase = 1;
                    break;
            }

            double cost = ShipManager.Instance.GetCostForCount(index, toPurchase);
            buyText.text = string.Format("BUY {0}", CurrencyController.FormatToString(toPurchase));
            buyCost.text = CurrencyController.FormatToString(cost);
            buyButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost;
        }

        public void SelectShip()
        {
            if (ShipManager.Instance.Count(index) > 0)
            {
                ShipManager.Instance.SelectShip(index);
            }
        }

        public void BuyShip()
        {
            int toPurchase = 1;
            switch (ShipManager.Instance.purchaseCount)
            {
                case PurchaseCount.BuyOne:
                    toPurchase = 1;
                    break;
                case PurchaseCount.BuyTen:
                    toPurchase = 10;
                    break;
                case PurchaseCount.BuyMax:
                    toPurchase = ShipManager.Instance.GetMaxPurchasable(index);
                    if (toPurchase <= 0)
                        toPurchase = 1;
                    break;
            }

            for (int i = 0; i < toPurchase; i++)
            {
                double cost = ShipManager.Instance.GetCostToPurchase(index);
                if (CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost)
                {
                    ShipManager.Instance.Purchase(index);
                }
                else
                    break;
            }

            //TutorialManager.OnTrigger("buy");
        }


        private string Format(double currentValue, double newValue, string format)
        {
            if (newValue > currentValue)
                return string.Format("{0} {1} <color={2}>+{3}</color>", FormatNumber(currentValue), format, colourText,
                    FormatNumber(newValue - currentValue));
            return string.Format("{0} {1}", FormatNumber(currentValue), format);
        }

        private string FormatNumber(double value)
        {
            if (value < 1000)
                return value.ToString("##0.##");
            return CurrencyController.FormatToString(value);
        }

        public void Upgrade()
        {
            if (FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                ShipManager.Instance.UpgradeShip(index);
        }

        public void Refresh()
        {
            Ship ship = ShipManager.Instance.GetShip(index);
            if (ship.shipLevel < 0)
            {
                unlockPanel.SetActive(true);
                double cost = ShipManager.Instance.GetLevelUpCost(index, 1);
                unlockButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost;
                unlockCost.text = CurrencyController.FormatToString(cost);
            }
            else
            {
                nextBoost.value = ShipManager.Instance.GetBoundaryPoint(index);
                unlockPanel.SetActive(false);
                double cost = 0;
                levelText.text = "Level " + CurrencyController.FormatToString(ship.shipLevel + 1);
                switch (ShipManager.Instance.upgradeFormat)
                {
                    case UpgradeFormat.OneLevel:
                        cost = ShipManager.Instance.GetLevelUpCost(index, 1);
                        break;
                    case UpgradeFormat.TenLevels:
                        cost = ShipManager.Instance.GetLevelUpCost(index, 10);
                        break;
                    case UpgradeFormat.HundredLevels:
                        cost = ShipManager.Instance.GetLevelUpCost(index, 100);
                        break;
                    case UpgradeFormat.NextBoundary:
                        cost = ShipManager.Instance.GetCostToGetToLevel(index,
                            ShipManager.Instance.GetNextBoundary(ship.shipLevel));
                        break;
                }

                upgradeCost.text = CurrencyController.FormatToString(cost);
                upgradeButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >= cost &&
                                             FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core);
                SetupDisplayText();
                SetupBuyCost();
                countText.text = CurrencyController.FormatToString(ShipManager.Instance.Count(index));
            }
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}