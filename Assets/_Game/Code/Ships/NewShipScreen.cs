﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Unity.VectorGraphics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Ships
{
    public class NewShipScreen : MonoBehaviour
    {
        [SerializeField]
        private Transform title, nameObject;

        [SerializeField]
        private RectTransform shipIcon;

        [SerializeField]
        private Text nameText;

        [SerializeField]
        private GameObject shipHolder, tapToClose;

        [SerializeField]
        private SpriteRenderer shipPrefab;

        [SerializeField]
        private Image shipImage;

        private List<SpriteRenderer> activeShips = new List<SpriteRenderer>();

        [SerializeField]
        private Queue<SpriteRenderer> shipPool = new Queue<SpriteRenderer>();

        [SerializeField]
        private AnimationCurve scaleCurve;

        private Vector2 scaleProfile = Vector3.one;

        private bool canBeClosed = false;

        private Coroutine process;

        [SerializeField]
        private UnityEvent closeAction;

        public void Setup(int index)
        {
            gameObject.SetActive(true);
            shipImage.sprite = ShipManager.Instance.GetShipSprite(index);
            shipIcon.localScale = Vector3.zero;
            scaleProfile = new Vector2(1f, shipImage.sprite.rect.height / shipImage.sprite.rect.width);
            shipIcon.sizeDelta = scaleProfile * 320f;
            title.localScale = Vector3.zero;
            nameText.text = ShipManager.Instance.GetShip(index).shipName;
            canBeClosed = false;
            if (process != null)
                StopCoroutine(process);
            process = StartCoroutine(RunProcess(index));
        }

        private IEnumerator RunProcess(int index)
        {
            AudioManager.Play("NewShip", 0.25f);
            AudioManager.Play("Upgrade", 0.25f);
            tapToClose.SetActive(false);
            for (float t = 0; t < 2f; t += Time.deltaTime * 1.125f)
            {
                title.localScale = Vector3.one * scaleCurve.Evaluate(Mathf.Clamp01(t));
                shipIcon.localScale = Vector3.one * scaleCurve.Evaluate(Mathf.Clamp01(t - 0.5f));
                nameObject.localScale = Vector3.one * scaleCurve.Evaluate(Mathf.Clamp01(t - 1f));
                yield return null;
            }

            title.localScale = Vector3.one;
            shipIcon.localScale = Vector3.one;
            nameObject.localScale = Vector3.one;
            
            int count = Random.Range(5, 8);
            shipHolder.SetActive(true);
            for (int i = 0; i < count; i++)
            {
                CreateShip(index);
                yield return new WaitForSeconds(Random.Range(0.125f, 0.333f));
            }
            tapToClose.SetActive(true);
            canBeClosed = true;
        }

        private void CreateShip(int index)
        {
            float xPos = (2.25f - (Random.value * 4.8f)) * ((float) Screen.width / Screen.height);
            Vector3 position = new Vector3(xPos, -6f, 1f);
            SpriteRenderer newShip =
                shipPool.Count > 0 ? shipPool.Dequeue() : Instantiate(shipPrefab, shipHolder.transform);
            newShip.sprite = ShipManager.Instance.GetShipSprite(index);
            newShip.transform.localPosition = position;
            newShip.transform.localScale = Vector3.one * Random.Range(0.25f, 0.33f);
            newShip.gameObject.SetActive(true);
            activeShips.Add(newShip);

        }

        public void Close()
        {
            if (!canBeClosed)
                return;
            CullAllShips();
            if (process != null)
                StopCoroutine(process);
            shipHolder.SetActive(false);
            gameObject.SetActive(false);
            AudioManager.Play("Click");
            closeAction?.Invoke();
        }

        private void CullAllShips()
        {
            while (activeShips.Count > 0)
            {
                SpriteRenderer ship = activeShips[0];
                ship.gameObject.SetActive(false);
                activeShips.RemoveAt(0);
                shipPool.Enqueue(ship);
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (activeShips.Count > 0)
            {
                for (int i = 0; i < activeShips.Count;)
                {
                    activeShips[i].transform.Translate(0, Time.deltaTime * activeShips[i].transform.localScale.y * 24f,
                        0, Space.Self);
                    if (activeShips[i].transform.localPosition.y > 6)
                    {
                        SpriteRenderer ship = activeShips[i];
                        ship.gameObject.SetActive(false);
                        activeShips.RemoveAt(i);
                        shipPool.Enqueue(ship);
                    }
                    else
                        i++;
                }
            }
        }
    }

}