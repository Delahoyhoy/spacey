﻿using UnityEngine;

namespace Core.Gameplay.UserInterface
{
    [RequireComponent(typeof(Canvas))]
    public class AutoHideCanvas : UIClearedAction
    {
        private bool initialised = false;

        private Canvas canvas;

        private Canvas Canvas
        {
            get
            {
                if (initialised)
                    return canvas;
                canvas = GetComponent<Canvas>();
                initialised = true;
                return canvas;
            }
        }

        protected override void Start()
        {
            canvas = GetComponent<Canvas>();
            initialised = true;
            base.Start();
        }

        protected override void OnSetScreenClear(bool cleared)
        {
            Canvas.enabled = cleared;
        }
    }
}