﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.VIP;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class GemPurchaseText : MonoBehaviour
{
    private Text text;

    [SerializeField]
    protected double gemsToAdd;

    [SerializeField]
    protected int bonusSize = 24;

    [SerializeField]
    private string format = "{0}";

    private bool subscribed;

    // Start is called before the first frame update
    private void Start()
    {
        UpdateCountText();
        if (subscribed)
            return;
        VIPManager.Instance.onSetUpgradeLevel += OnVIPLevelUpdated;
        subscribed = true;
    }

    private void OnVIPLevelUpdated(VIPCategory category, double value)
    {
        if (category != VIPCategory.Diamonds)
            return;
        SetText(GetTotalGemCount(bonusSize, value));
    }

    private void UpdateCountText()
    {
        SetText(GetTotalGemCount(bonusSize, VIPManager.Instance.GetValue(VIPCategory.Diamonds)));
    }

    protected void SetText(string textString)
    {
        if (!text)
            text = GetComponent<Text>();
        text.text = textString;
    }

    protected string GetTotalGemCount(int size = 24, double multiplier = 1)
    {
        double extra = System.Math.Round((multiplier - 1) * gemsToAdd);
        return string.Format(format, extra > 0 ?  string.Format("{0} <size={2}>(+{1})</size>", CurrencyController.FormatToString(gemsToAdd), CurrencyController.FormatToString(extra), size):
            CurrencyController.FormatToString(gemsToAdd));
    }
}
