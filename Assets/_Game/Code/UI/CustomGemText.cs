﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Gameplay.VIP;

public class CustomGemText : GemPurchaseText
{
    [Multiline]
    [SerializeField]
    private string fullText;

    private bool initialised;
    
    private void Start()
    {
        SetText();
        if (initialised)
            return;
        VIPManager.Instance.onSetUpgradeLevel += OnUpdateVIP;
        initialised = true;
    }

    private void OnUpdateVIP(VIPCategory category, double value)
    {
        if (category == VIPCategory.Diamonds)
            SetText();
    }
    
    private void SetText()
    {
        SetText(string.Format(fullText, GetTotalGemCount(bonusSize)));
    }
}
