﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fumb.Privacy;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SplashScreen : MonoBehaviour
{
    [SerializeField]
    private GameObject rootObject;

    [SerializeField]
    private Graphic[] splashGraphics;

    [SerializeField]
    private Text messageText;

    [SerializeField]
    private string[] messages;

    private static bool splashScreenCompleted;
    public static bool SplashScreenCompleted => splashScreenCompleted;
    
    [SerializeField]
    private UnityEvent onCompleteSplash;

    public static event Action OnCompleteSplashScreen; 

    // Start is called before the first frame update
    private void Awake()
    {
        splashScreenCompleted = false;
        rootObject.SetActive(true);
        messageText.text = GetMessage();//messages[Random.Range(0, messages.Length)];
        StartCoroutine(WaitHide());
    }

    public void CheckSplashScreenCompleted()
    {
        if (!splashScreenCompleted) 
            return;
        onCompleteSplash?.Invoke();
        OnCompleteSplashScreen?.Invoke();
    }

    private string GetMessage()
    {
        string selectedMessage = messages[Random.Range(0, messages.Length)];
        if (selectedMessage.Contains("`"))
        {
            string finalMessage = "";
            string[] splitMessage = selectedMessage.Split('`');
            for (int i = 0; i < splitMessage.Length; i++)
            {
                if (i > 0)
                    finalMessage += "\n";
                finalMessage += splitMessage[i];
            }
            return finalMessage;
        }
        return selectedMessage;
    }

    private IEnumerator WaitHide()
    {
        yield return new WaitForSeconds(4f);

#if !UNITY_IOS
        while (!GDPRManager.HasGivenConsent)
            yield return null;
#endif
        
        for (int i = 0; i < splashGraphics.Length; i++)
            splashGraphics[i].CrossFadeAlpha(0, 0.75f, true);
        //onCompleteSplash?.Invoke();
        yield return new WaitForSeconds(0.75f);

        onCompleteSplash?.Invoke();
        OnCompleteSplashScreen?.Invoke();
        splashScreenCompleted = true;
        rootObject.SetActive(false);
    }

    public void ForceClose()
    {
        rootObject.gameObject.SetActive(false);
        StopAllCoroutines();
    }
}
