﻿using UnityEngine;

namespace Core.Gameplay.UserInterface
{
    public class ConnectorLine : MonoBehaviour
    {
        [SerializeField]
        private RectTransform rectTransform;
        
        protected void SetPosition(Transform from, Transform to)
        {
            var toPos = to.position;
            var fromPos = from.position;
            float dx = toPos.x - fromPos.x;
            float dy = toPos.y - fromPos.y;

            float angle = Mathf.Atan2(dy, dx);
            transform.localEulerAngles = new Vector3(0, 0, angle * Mathf.Rad2Deg);
            transform.position = Vector3.Lerp(toPos, fromPos, 0.5f);

            Vector2 rectSize = rectTransform.sizeDelta;
            rectSize.x = Vector3.Distance(transform.InverseTransformPoint(fromPos), 
                transform.InverseTransformPoint(toPos));
            rectTransform.sizeDelta = rectSize;
        }
    }
}