﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberCounterUpper : MonoBehaviour
{

    private float minScale = 1f, maxScale = 1.125f;

    [SerializeField]
    private AnimationCurve scaleCurve;

    private double current, start, end;

    private float lerp = 0;

    [SerializeField]
    private float speed = 1f;

    private Vector3 min, max;

    [SerializeField]
    private Text outputText;

    // Start is called before the first frame update
    void Start()
    {
        min = new Vector3(minScale, minScale, minScale);
        max = new Vector3(maxScale, maxScale, maxScale);
    }

    public void SetVisualUpdate(double target)
    {
        start = current;
        end = target;
        lerp = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (lerp < 1f)
        {
            lerp = Mathf.Lerp(lerp, 1f, Time.deltaTime * speed);
            transform.localScale = Vector3.Lerp(min, max, scaleCurve.Evaluate(lerp));
            current = LerpDouble(start, end, lerp);
            outputText.text = CurrencyController.FormatToString(current);
        }
    }

    public void ResetCounter(double value)
    {
        lerp = 1f;
        current = value;
        start = value;
        end = value;
        outputText.text = CurrencyController.FormatToString(current);
    }

    double LerpDouble(double a, double b, float t)
    {
        return a + t * (b - a);
    }
}
