using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class MultiSegmentFillBar : MonoBehaviour
    {
        [SerializeField]
        private Image[] fillSegments;

        public void SetValue(float value)
        {
            int totalSegments = fillSegments.Length;
            float remainingValue = value * totalSegments;
            for (int i = 0; i < totalSegments; i++)
            {
                float valueForSegment = Mathf.Min(remainingValue, 1);
                fillSegments[i].fillAmount = valueForSegment;
                remainingValue -= valueForSegment;
            }
        }
    }
}
