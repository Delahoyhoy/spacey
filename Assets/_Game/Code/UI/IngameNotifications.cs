﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Achievements;
using Core.Gameplay.Outposts;
using Core.Gameplay.Ships;
using Core.Gameplay.Version;
using UnityEngine;

public class IngameNotifications : MonoBehaviour
{
    [SerializeField]
    private GameObject shipUpgradeNotification, achievementNotification, shipSpecificHighlight, outpostNotification;

    public void UpdateNotifications()
    {
        shipUpgradeNotification.SetActive(GameVersionManager.CurrentVersion == GameVersion.Version1 && ShipManager.Instance.GetNextAvailable());
        //shipSpecificHighlight.SetActive(ShipManager.Instance.GetNextAvailable());
        achievementNotification.SetActive(AchievementController.Instance.GetAchievementReady());
        outpostNotification.SetActive(GameVersionManager.CurrentVersion != GameVersion.Version1 && OutpostManager.Instance.CanOutpostUpgrade());
    }

    private IEnumerator Cycle()
    {
        yield return null;
        while (true)
        {
            UpdateNotifications();
            yield return new WaitForSeconds(1f);
        }
    }
    
    void Start()
    {
        StartCoroutine(Cycle());
    }

}
