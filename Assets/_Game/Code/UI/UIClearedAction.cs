using System;
using UnityEngine;

namespace Core.Gameplay.UserInterface
{
    public class UIClearedAction : MonoBehaviour
    {
        protected virtual void Start()
        {
            UIManager.Instance.onScreenClear += OnSetScreenClear;
        }

        private void OnDestroy()
        {
            if (UIManager.Instance)
                UIManager.Instance.onScreenClear -= OnSetScreenClear;
        }

        protected virtual void OnSetScreenClear(bool cleared)
        {
            // Perform screen cleared action
        }
    }
}
