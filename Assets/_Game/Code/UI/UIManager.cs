﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private bool blockClose = false, blockBack = false;

    private static UIManager instance;

    public static UIManager Instance { get { if (!instance) instance = ObjectFinder.FindObjectOfType<UIManager>(); return instance; } }

    public bool InMenu => screens.Count > 0 || blockClose || blockBack;

    //[SerializeField]
    //private UIParticles particles;

    private Stack<UIScreen> screens = new Stack<UIScreen>();

    [SerializeField]
    private GaussianTest gaussian;
    [SerializeField]
    private Image overlay;

    private int blockOverlayFlags;

    public Action<bool> onScreenClear;

    void Awake()
    {
        instance = this;
        overlay.gameObject.SetActive(true);
        overlay.CrossFadeAlpha(0, 0, true);
    }

    public void ForceAllClosed()
    {
        while (screens.Count > 0)
        {
            if (screens.Peek().canBeForcedClosed)
                screens.Pop().ForceClose();
            else
                break;
        }
        ManageBlur();
    }

    public void AddOverlayBlockFlags(int flags)
    {
        blockOverlayFlags += flags;
    }
    
    public void RemoveOverlayBlockFlags(int flags)
    {
        blockOverlayFlags -= flags;
    }

    public void AddToStack(UIScreen screen)
    {
        if (screens.Count > 0)
            screens.Peek().Hide();
        if (screens.Contains(screen))
        {
            RemoveFromStack(screen);
        }
        screens.Push(screen);
        ManageBlur();
    }

    public void RemoveFromStack(UIScreen screen)
    {
        if (screens.Count <= 0)
            return;
        if (screens.Peek() == screen)
        {
            screens.Pop();
            ManageBlur();
            if (screens.Count > 0)
                screens.Peek().Unhide();
            return;
        }
        Stack<UIScreen> temp = new Stack<UIScreen>();
        while (screens.Count > 0)
        {
            UIScreen test = screens.Pop();
            if (test != screen)
                temp.Push(test);
        }
        while (temp.Count > 0)
        {
            screens.Push(temp.Pop());
        }
        if (screens.Count > 0)
            screens.Peek().Unhide();
        ManageBlur();
    }

    public void SetBlockBack(bool value)
    {
        blockBack = value;
        ManageBlur();
    }

    public void SetBlockClose(bool value)
    {
        blockClose = value;
        ManageBlur();
    }

    private void ManageBlur()
    {
        gaussian.Blur = screens.Count > 0 ? 0.004f : 0;
        overlay.CrossFadeAlpha(screens.Count > blockOverlayFlags ? 0.333f: 0f, .75f, false);
        onScreenClear?.Invoke(!InMenu);
    }

    // Update is called once per frame
    private void Update()
    {
        if (blockBack)
            return;
        if (!Input.GetKeyDown(KeyCode.Escape)) 
            return;
        if (screens.Count > 0)
            screens.Peek().ForceClose();
        else if (!blockClose)
        {
            if (Application.platform != RuntimePlatform.Android) 
                return;
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
        }
    }
}
