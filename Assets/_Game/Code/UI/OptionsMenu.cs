﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Utilities;
using Fumb.Purchasing;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : UIScreen
{
    [SerializeField]
    private GameObject soundOn, soundOff, notificationsOn, notificationsOff, musicOn, musicOff, hapticOff;

    [SerializeField]
    private Text versionText;
    
    //[SerializeField]
    //private GaussianTest shaderStuff;

    public override void Open()
    {
        //soundOn.SetActive(!AudioManager.Instance.Muted);
        soundOff.SetActive(AudioManager.Instance.Muted);
        //notificationsOn.SetActive(PlayerPrefs.GetInt("notificationsEnabled", 2) > 0);
        notificationsOff.SetActive(PlayerPrefs.GetInt("notificationsEnabled", 2) <= 0);
        //musicOn.SetActive(!AudioManager.Instance.MusicMuted);
        musicOff.SetActive(AudioManager.Instance.MusicMuted);
        versionText.text = $"VERSION {Application.version}";
        base.Open();
    }

    public void ToggleSound()
    {
        bool isMuted = AudioManager.Instance.Muted;
        AudioManager.Instance.Muted = !isMuted;
        //soundOn.SetActive(isMuted);
        soundOff.SetActive(!isMuted);
        AudioManager.Play("Click");
    }

    public void ToggleMusic()
    {
        bool isMuted = AudioManager.Instance.MusicMuted;
        AudioManager.Instance.MusicMuted = !isMuted;
        //musicOn.SetActive(isMuted);
        musicOff.SetActive(!isMuted);
        AudioManager.Play("Click");
    }

    public void ToggleNotifications()
    {
        bool current = PlayerPrefs.GetInt("notificationsEnabled", 2) > 0;
        PlayerPrefs.SetInt("notificationsEnabled", current ? 0 : 1);
        //notificationsOn.SetActive(!current);
        notificationsOff.SetActive(current);
        AudioManager.Play("Click");
    }

    public void RestorePurchases()
    {
        PurchaseRestoreHelper.RestorePurchases();
    }

    public void OpenDiscord()
    {
        Application.OpenURL("https://discord.gg/fumbgames");
    }

    public void OpenMessenger()
    {
        Application.OpenURL("https://m.me/1867904263535842");
    }
}
