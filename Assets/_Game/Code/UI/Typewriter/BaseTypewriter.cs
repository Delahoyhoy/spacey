﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Core.Gameplay.UserInterface
{
    public class BaseTypewriter : MonoBehaviour
    {
        [SerializeField]
        private string textToShow;

        [SerializeField]
        private float characterDuration = 0.1f;

        [SerializeField]
        private Button continueButton;

        [SerializeField]
        private AudioSource typewriterSound;

        protected virtual void SetText(string text) { }

        public void PlayTypeWriter(string displayText)
        {
            textToShow = displayText;
            StartCoroutine(RevealText(null, null));
        }

        public void DebugTest()
        {
            textToShow = "<b>DEBUG</b> <i>TEST</i> <u>123</u>.";
            StartCoroutine(RevealText(null, null));
        }

        private IEnumerator RevealText(Action onBegin, Action onEnd, bool showContinueButton = false,
            float additionalDelay = 0)
        {
            SetText("<color=#00000000>" + textToShow + "</color>");
            onBegin?.Invoke();
            yield return new WaitForSeconds(0.5f + additionalDelay);
            typewriterSound.Play();
            int charsToReveal = Mathf.CeilToInt(0.05f / characterDuration);
            for (int i = 0; i < textToShow.Length; i++)
            {
                bool hasEncounteredTag = false;
                if (Time.deltaTime > characterDuration)
                {
                    int plusCount = charsToReveal; // Mathf.FloorToInt(Time.deltaTime / characterDuration);
                    for (int j = 0; j < plusCount; j++)
                    {
                        i++;
                        if (i >= textToShow.Length)
                            break;
                        if (textToShow[i].Equals('<'))
                            hasEncounteredTag = true;
                        if (textToShow[i - 1].Equals('>'))
                            hasEncounteredTag = false;
                        
                    }
                    if (i >= textToShow.Length)
                    {
                        i = textToShow.Length - 1;
                        hasEncounteredTag = false;
                    }
                }
                if (hasEncounteredTag)
                {
                    for (int j = i; j < textToShow.Length; j++)
                    {
                        if (!textToShow[j].Equals('>'))
                            continue;
                        i = j + 1;
                        break;
                    }
                    if (i >= textToShow.Length)
                        i = textToShow.Length - 1;
                }

                string output = textToShow;
                output = output.Insert(i, "<color=#00000000>");
                output += "</color>";
                SetText(output);
                yield return new WaitForSeconds(0.05f); // new WaitForSeconds(characterDuration);
            }

            typewriterSound.Stop();
            onEnd?.Invoke();
            SetText(textToShow);
            if (showContinueButton && continueButton)
                continueButton.interactable = true;
        }

        public void Stop()
        {
            StopAllCoroutines();
            SetText(textToShow);
            typewriterSound.Stop();
        }

        public void PlayTypeWriter(string displayText, Action onBegin, Action onEnd,
            bool showContinueButton = false, float additionalDelay = 0)
        {
            if (continueButton)
            {
                continueButton.gameObject.SetActive(showContinueButton);
                continueButton.interactable = false;
            }

            textToShow = displayText;
            StartCoroutine(RevealText(() => onBegin?.Invoke(), () => onEnd?.Invoke(), showContinueButton,
                additionalDelay));
        }
    }
}