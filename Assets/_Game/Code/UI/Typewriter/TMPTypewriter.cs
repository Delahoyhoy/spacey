﻿using System;
using TMPro;
using UnityEngine;

namespace Core.Gameplay.UserInterface
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TMPTypewriter : BaseTypewriter
    {
        [SerializeField]
        private TextMeshProUGUI text;

        private void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();
        }

        protected override void SetText(string displayText)
        {
            text.text = displayText;
        }
    }
}