﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.UserInterface;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Typewriter : BaseTypewriter {

    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
        //text.fontSize = ((float)Screen.width / Screen.height) < 0.5 ? 32 : 40;
    }

    protected override void SetText(string displayText)
    {
        text.text = displayText;
    }
}
