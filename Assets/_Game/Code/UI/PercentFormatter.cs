using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    public class PercentFormatter : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<string> onSetPercent;

        public void SetPercent(float percent)
        {
            onSetPercent?.Invoke($"{Mathf.RoundToInt(percent * 100)}%");
        }
    }
}