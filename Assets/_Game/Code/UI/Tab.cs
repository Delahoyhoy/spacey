using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.UserInterface
{
    [Serializable]
    public struct Tab
    {
        public Button tabButton;
        public GameObject tabView;

        [SerializeField]
        private UnityEvent onSetActiveTab;

        public void SetTabSelected(bool selected)
        {
            tabButton.interactable = !selected;
            tabView.SetActive(selected);
            if (selected)
                onSetActiveTab?.Invoke();
        }
    }
}