﻿using UnityEngine;

namespace Core.Gameplay.UserInterface
{
    public class NotificationDisplayRequest : MonoBehaviour
    {
        public void RequestDisplay(string toDisplay)
        {
            NotificationDisplay.Instance.Display(toDisplay);
        }
    }
}