﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.UserInterface
{
    public class NotificationDisplay : MonoBehaviour
    {
        [SerializeField]
        private Text displayText;

        private float timeRemaining = 0;
        private bool revealing = false;

        private Queue<string> toDisplay = new Queue<string>();
        private string currentText;

        [SerializeField]
        private float timePerChar = 0.025f;

        private static NotificationDisplay instance;

        public static NotificationDisplay Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<NotificationDisplay>();
                return instance;
            }
        }

        private Coroutine currentReveal;

        public void Display(string textToDisplay)
        {
            // AudioManager.Play("Radar", .75f);
            // displayText.text = textToDisplay;
            // displayText.CrossFadeAlpha(1f, 0.5f, true);
            // timeRemaining = 4.5f;

            // FUTURE_VERSION To re-add later
            ///*
            if (revealing)
            {
                if (textToDisplay != currentText && !toDisplay.Contains(textToDisplay))
                    toDisplay.Enqueue(textToDisplay);
                return;
            }

            AudioManager.Play("Radar", .75f);
            displayText.CrossFadeAlpha(1f, 0f, true);
            if (currentReveal != null)
                StopCoroutine(currentReveal);
            currentReveal = StartCoroutine(RevealText(textToDisplay)); //*/
        }

        private IEnumerator RevealText(string textToReveal)
        {
            revealing = true;
            currentText = textToReveal;
            int currentChar = 0;
            float lastCharTime = Time.time;
            while (currentChar < textToReveal.Length)
            {
                char scrambleChar = (char) Random.Range(21, 94);
                displayText.text = textToReveal.Substring(0, currentChar) + scrambleChar;
                yield return null;
                if (Time.time < lastCharTime + timePerChar)
                    continue;
                lastCharTime = Time.time;
                currentChar++;
            }

            if (toDisplay.Count > 0)
            {
                yield return new WaitForSeconds(2f);
                currentReveal = StartCoroutine(RevealText(toDisplay.Dequeue()));
                yield break;
            }

            displayText.text = textToReveal;
            timeRemaining = 4.5f;
            revealing = false;
        }

        // Start is called before the first frame update
        private void Start()
        {
            displayText.CrossFadeAlpha(0, 0, true);
        }

        private void Awake()
        {
            instance = this;
        }

        // Update is called once per frame
        private void Update()
        {
            if (revealing || timeRemaining < 0)
                return;
            timeRemaining -= Time.deltaTime;
            if (timeRemaining <= 0)
            {
                displayText.CrossFadeAlpha(0, 1f, true);
            }
        }
    }
}
