﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Fumb.Attribute;
using Fumb.General;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class UIScreen : MonoBehaviour
{

    protected bool open = false;

    public bool IsOpen { get { return open; } }

    [SerializeField]
    public bool canBeForcedClosed = true, autoHide = true;

    private bool forceOpen = false;

    public void SetForcedOpen(bool force)
    {
        forceOpen = force;
    }

    [SerializeField]
    private UnityEvent onOpen, onClose;

    [SerializeField]
    private Transform mainPanel;
    private float lerp;

    [SerializeField]
    private UnityEvent onHide;

    [SerializeField]
    private AnimationCurve scaleCurve;

    [SerializeField]
    private bool overrideScaleEffect;

    [SerializeField] [ConditionalField("overrideScaleEffect")]
    private UnityEvent<float> onSetScaleValue;

    [SerializeField]
    private float animSpeed = 2f;

    private bool transitioning = false;

    public virtual void Open()
    {
        if (open) 
            return;
        transitioning = true;
        SetTimer.StartTimer(1f / animSpeed, StopTransitioning);
        AudioManager.Play("Popup", .5f);
        AudioManager.Play("Click", .5f);
        gameObject.SetActive(true);
        open = true;
        UIManager.Instance.AddToStack(this);
        transform.SetAsLastSibling();
        onOpen?.Invoke();
    }

    public void Unhide()
    {
        if (open) 
            return;
        gameObject.SetActive(true);
        //AudioManager.Play("Click", 1f);
        //AnimationController.SetBool("Open", true);
        open = true;
        transform.SetAsLastSibling();
    }

    public void ForceClose()
    {
        if (!canBeForcedClosed || forceOpen)
            return;
        Close();
    }

    public virtual void Close()
    {
        if (open && !forceOpen && !transitioning)
        {
            transitioning = true;
            AudioManager.Play("Close", .5f);
            AudioManager.Play("Click", .5f);
            //AnimationController.SetBool("Open", false);
            StartCoroutine(WaitClose());
            open = false;
            UIManager.Instance.RemoveFromStack(this);
            onClose?.Invoke();
        }
    }

    public void Hide()
    {
        if (!open || !autoHide) 
            return;
        onHide?.Invoke();
        transitioning = true;
        //AudioManager.Play("Close", 1f);
        StartCoroutine(WaitClose());
        //AnimationController.SetBool("Open", false);
        SetTimer.StartTimer(1f / animSpeed, StopTransitioning);
        open = false;
    }

    public virtual void CloseInstant()
    {
        if (!open || forceOpen) 
            return;
        transitioning = false;
        AudioManager.Play("Close", .5f);
        gameObject.SetActive(false);
        open = false;
        UIManager.Instance.RemoveFromStack(this);
        onClose?.Invoke();
    }

    private void StopTransitioning()
    {
        transitioning = false;
    }

    protected IEnumerator WaitClose()
    {
        yield return new WaitForSeconds(1f / animSpeed);
        transitioning = false;
        gameObject.SetActive(false);
    }
    
    protected virtual void Start()
    {
        if (!mainPanel)
            mainPanel = transform.GetChild(0);
        lerp = 0;
        if (!overrideScaleEffect)
            mainPanel.transform.localScale = Vector3.one * scaleCurve.Evaluate(0);
        else
            UpdateLerpValue(lerp);
    }

    private void UpdateLerpValue(float lerpValue)
    {
        if (overrideScaleEffect)
            onSetScaleValue?.Invoke(lerpValue);
        else
            mainPanel.localScale = Vector3.one * lerpValue;
    }
    
    protected virtual void Update()
    {
        float startLerp = lerp;
        if (open)
        {
            if (lerp < 1)
                lerp = Mathf.Clamp01(lerp + (Time.deltaTime * animSpeed));
        }
        else
        {
            if (lerp > 0)
                lerp = Mathf.Clamp01(lerp - (Time.deltaTime * animSpeed));
        }

        if (Math.Abs(lerp - startLerp) <= 0.001f) 
            return;
        
        float curveValue = scaleCurve.Evaluate(lerp);
        UpdateLerpValue(curveValue);
    }
}
