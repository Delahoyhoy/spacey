﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.UserInterface
{
    public class MultitabWindow : MonoBehaviour
    {
        [SerializeField]
        private Tab[] tabs;

        public void SelectTab(int tab)
        {
            AudioManager.Play("Click", 1f);
            for (int i = 0; i < tabs.Length; i++)
            {
                tabs[i].SetTabSelected(i == tab);
            }
        }
    }

}