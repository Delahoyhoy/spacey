using System;
using UnityEngine.Events;

namespace Core.Gameplay.Version
{
    [Serializable]
    public struct GameVersionOption
    {
        public GameVersion version;
        public UnityEvent onSetVersion;

        public void SetActive()
        {
            onSetVersion?.Invoke();
        }
    }
}