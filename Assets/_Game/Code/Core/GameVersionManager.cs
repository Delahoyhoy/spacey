using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fumb.Save;

namespace Core.Gameplay.Version
{
    public class GameVersionManager : MonoBehaviour
    {
        private static GameVersionManager instance;
        public static GameVersionManager Instance => instance ??= ObjectFinder.FindObjectOfType<GameVersionManager>();

        private GameVersion currentVersion;
        public static GameVersion CurrentVersion => Instance.currentVersion;

        [SerializeField]
        private GameVersionOption[] options;

        private Dictionary<GameVersion, GameVersionOption> versionLookup =
            new Dictionary<GameVersion, GameVersionOption>();
        
        [ManagedSaveValue("GameVersionManagerCurrentVersion")]
        private SaveValueInt saveCurrentVersion;

        public Action<GameVersion> onSetGameVersion;

        private void Awake()
        {
            instance = this;
            for (int i = 0; i < options.Length; i++)
            {
                if (!versionLookup.ContainsKey(options[i].version))
                    versionLookup.Add(options[i].version, options[i]);
            }
        }

        private void Start()
        {
            onSetGameVersion?.Invoke(CurrentVersion);
        }

        public void InitialiseVersion(bool newVersion)
        {
            if (saveCurrentVersion.IsPopulated)
                currentVersion = (GameVersion) saveCurrentVersion.Value;
            else
            {
                currentVersion = newVersion ? GameVersion.Version2 : GameVersion.Version1;
                saveCurrentVersion.Value = (int) currentVersion;   
            }
            if (versionLookup.TryGetValue(currentVersion, out GameVersionOption option))
                option.SetActive();
            onSetGameVersion?.Invoke(currentVersion);
        }

        public void UpdateToLatestVersion()
        {
            int versionIndex = Enum.GetValues(typeof(GameVersion)).Length - 1;
            currentVersion = (GameVersion)versionIndex;
            saveCurrentVersion.Value = versionIndex;
            
            if (versionLookup.TryGetValue(currentVersion, out GameVersionOption option))
                option.SetActive();
            onSetGameVersion?.Invoke(currentVersion);
        }
    }
}