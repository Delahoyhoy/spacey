namespace Core.Gameplay.Version
{
    public enum GameVersion
    {
        Version1, // Manual Ship Management
        Version2, // Automatic Ship Management
        Version3, // Core system overhaul
    }
}