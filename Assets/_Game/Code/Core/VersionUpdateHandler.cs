using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Outposts;
using Core.Gameplay.Research;
using Core.Gameplay.Reset;
using Core.Gameplay.Save;
using Core.Gameplay.Sectors;
using Core.Gameplay.Ships;
using Core.Gameplay.Tutorial;
using Fumb;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Version
{
    public class VersionUpdateHandler : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent onBegin;
        
        public void UpdateToNewVersion()
        {
            GameManager.Instance.PrestigeToUpdate(RunNewVersionFromTutorial);
        }

        public void InstantUpdate()
        {
            RunNewVersionFromTutorial();
            ResetManager.Instance.Prestige();
        }

        public void RunNewVersionFromTutorial()
        {
            AsteroidManager.Instance.RegisterVersionUpdated();
            OutpostManager.Instance.RegisterVersionUpdated();
            GameManager.Instance.ClearScene();
            GameVersionManager.Instance.UpdateToLatestVersion();
            ShipManager.Instance.RegisterNewVersion();
            ClusterManager.Instance.StartAtFirstCluster();
            JC_SaveManager saveManager = ObjectFinder.FindObjectOfType<JC_SaveManager>();
            saveManager.RemoveSaveKey("tutorialStage");
            onBegin?.Invoke();
            if (FTUEManager.Instance.TryGetModule(FTUEModuleId.Core, out FTUEModule module))
                module.ForceStart();
        }
    }
}
