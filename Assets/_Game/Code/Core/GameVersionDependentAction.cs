using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Version
{
    public class GameVersionDependentAction : MonoBehaviour
    {
        [SerializeField]
        private List<GameVersion> validVersions = new List<GameVersion>();

        [SerializeField]
        private UnityEvent<bool> onSetVersionValid;
        
        private void Start()
        {
            SetVersion(GameVersionManager.CurrentVersion);
            GameVersionManager.Instance.onSetGameVersion += SetVersion;
        }

        private void SetVersion(GameVersion version)
        {
            onSetVersionValid?.Invoke(validVersions.Contains(version));
        }
    }
}