﻿using UnityEngine;

namespace Core.Gameplay.Audio
{
    public class AudioTrigger : MonoBehaviour
    {
        [SerializeField]
        private string defaultClip;

        [SerializeField]
        private float defaultVolume = 1f;

        public void PlayClip()
        {
            AudioManager.Play(defaultClip, defaultVolume);
        }

        public void PlayClip(string clip)
        {
            AudioManager.Play(clip, defaultVolume);
        }
    }
}