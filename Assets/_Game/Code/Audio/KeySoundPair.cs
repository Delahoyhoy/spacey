﻿using UnityEngine;

namespace Core.Gameplay.Audio
{
    [System.Serializable]

    public struct KeySoundPair
    {
        public string key;
        public AudioClip[] clips;
        public Vector2 pitchRange;

        public int ClipCount => clips.Length;
    }
}