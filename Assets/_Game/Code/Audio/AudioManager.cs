﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Audio
{
    public class AudioManager : MonoBehaviour
    {

        private Dictionary<string, KeySoundPair> clipDictionary = new Dictionary<string, KeySoundPair>();

        [SerializeField]
        private KeySoundPair[] keySoundPairs;

        [SerializeField]
        private AudioSource audioSource, musicSource;

        [SerializeField]
        private AudioSource[] additionalSources;

        public delegate void SetMuted(bool muted);

        public static event SetMuted OnSetMute;

        private List<string> playedThisPeriod = new List<string>();

        [SerializeField]
        private float clearPeriod = 0.5f;

        private float maxMusicVolume = 1f;

        private bool muted = false;

        public bool Muted
        {
            get => muted;
            set
            {
                muted = value;
                if (audioSource)
                    audioSource.mute = value;
                for (int i = 0; i < additionalSources.Length; i++)
                    additionalSources[i].mute = value;
                OnSetMute?.Invoke(value);
                PlayerPrefs.SetInt("muted", value ? 1 : 0);
                PlayerPrefs.Save();
            }
        }

        private bool musicMuted = false;

        public bool MusicMuted
        {
            get { return musicMuted; }
            set
            {
                musicMuted = value;
                if (musicSource)
                    musicSource.mute = value;
                PlayerPrefs.SetInt("musicMuted", value ? 1 : 0);
                PlayerPrefs.Save();
            }
        }

        public static void Play(string id, float volume = 1f)
        {
            if (Instance)
                Instance.PlayClip(id, volume);
        }

        IEnumerator FadeOutMusic()
        {
            if (musicSource)
            {
                maxMusicVolume = musicSource.volume;
                for (float t = 0; t < 1; t += Time.deltaTime)
                {
                    musicSource.volume = maxMusicVolume * (1 - t);
                    yield return null;
                }

                musicSource.volume = 0;
            }
        }

        public void FadeMusic()
        {
            StartCoroutine(FadeOutMusic());
        }

        bool wasMute = false, wasMusicMute = false;

        public void TempMute(bool mute)
        {
            if (mute)
            {
                wasMute = Muted;
                if (audioSource)
                    audioSource.mute = true;
                for (int i = 0; i < additionalSources.Length; i++)
                    additionalSources[i].mute = true;
                wasMusicMute = musicMuted;
                if (musicSource)
                    musicSource.mute = true;
                if (OnSetMute != null)
                    OnSetMute(true);
            }
            else
            {
                if (audioSource)
                    audioSource.mute = wasMute;
                for (int i = 0; i < additionalSources.Length; i++)
                    additionalSources[i].mute = wasMute;
                if (musicSource)
                    musicSource.mute = wasMusicMute;
                if (OnSetMute != null)
                    OnSetMute(wasMute);
            }
        }

        public static AudioManager Instance;

        // Use this for initialization
        void Awake()
        {
            Instance = this;
            if (PlayerPrefs.HasKey("muted"))
            {
                Muted = PlayerPrefs.GetInt("muted", 0) > 0;
            }

            if (PlayerPrefs.HasKey("musicMuted"))
            {
                MusicMuted = PlayerPrefs.GetInt("musicMuted", 0) > 0;
            }

            for (int i = 0; i < keySoundPairs.Length; i++)
            {
                if (!clipDictionary.ContainsKey(keySoundPairs[i].key))
                    clipDictionary.Add(keySoundPairs[i].key, keySoundPairs[i]);
            }

            StartCoroutine(ClearOverPeriod());
        }

        private bool wasMuted = false;

        public void IsolateSFX()
        {
            wasMuted = audioSource.mute;
            audioSource.mute = true;
            for (int i = 0; i < additionalSources.Length; i++)
                additionalSources[i].mute = true;
            if (OnSetMute != null)
                OnSetMute(true);
        }

        public void ReEnableSFX()
        {
            audioSource.mute = wasMuted;
            for (int i = 0; i < additionalSources.Length; i++)
                additionalSources[i].mute = wasMuted;
            if (OnSetMute != null)
                OnSetMute(wasMuted);
        }

        public void PlayClip(string clip, float volume = 1f)
        {
            if (Time.timeSinceLevelLoad < 1f)
                return;
            if (playedThisPeriod.Contains(clip))
            {
                //Debug.Log("Failed to play " + clip);
                return;
            }

            if (audioSource && clipDictionary.ContainsKey(clip))
            {
                //Debug.Log("Playing " + clip);
                KeySoundPair keySoundPair = clipDictionary[clip];
                //if (keySoundPair.pitchRange.magnitude != 0)
                //    audioSource.pitch = Random.Range(keySoundPair.pitchRange.x, keySoundPair.pitchRange.y);
                //else
                //    audioSource.pitch = 1f;
                audioSource.PlayOneShot(
                    keySoundPair.clips[keySoundPair.ClipCount > 1 ? Random.Range(0, keySoundPair.ClipCount) : 0],
                    volume);
                //Debug.Log("Playing: " + clip);
                playedThisPeriod.Add(clip);
            }
        }

        public static void Fade(float duration)
        {
            if (Instance)
                Instance.BriefFade(duration);
        }

        public void BriefFade(float duration)
        {
            StartCoroutine(FadeMusic(duration));
        }

        private IEnumerator FadeMusic(float duration)
        {
            float startLevel = musicSource.volume;
            for (float t = 0; t < 1f; t += Time.deltaTime * 4f)
            {
                musicSource.volume = Mathf.Lerp(startLevel, 0, t);
                yield return null;
            }

            musicSource.volume = 0;
            yield return new WaitForSeconds(duration);
            for (float t = 0; t < 1f; t += Time.deltaTime)
            {
                musicSource.volume = Mathf.Lerp(0, startLevel, t);
                yield return null;
            }

            musicSource.volume = startLevel;
        }

        public void PlayClipSimple(string clip)
        {
            PlayClip(clip);
        }

        private IEnumerator ClearOverPeriod()
        {
            while (true)
            {
                yield return new WaitForSeconds(clearPeriod);
                playedThisPeriod.Clear();
            }
        }

    }
}
