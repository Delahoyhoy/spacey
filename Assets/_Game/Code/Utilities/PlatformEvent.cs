﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    [Serializable]
    public class PlatformEvent
    {
        public RuntimePlatform platform;
        public UnityEvent action;

        public void Trigger()
        {
            action?.Invoke();
        }
    }
}