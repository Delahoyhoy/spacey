using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class SimpleEventLog : MonoBehaviour
    {
        public void Log(string info)
        {
            Debug.Log(info, gameObject);
        }

        public void LogBool(bool info)
        {
            Debug.Log($"{gameObject.name} logging as : {info}", gameObject);
        }
    }
}
