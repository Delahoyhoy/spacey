﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct BezierHolder
{
    public Vector3 Point { get; private set; }
    public Vector3 Handle { get; private set; }

    /// <summary>
    /// Setup a bezier point at runtime
    /// </summary>
    /// <param name="pos">The starting position of the point</param>
    /// <param name="angle">The angle between the point and the handle in degrees</param>
    /// <param name="distance">How far away to put the handle</param>
    public BezierHolder(Vector3 pos, float angle, float distance)
    {
        Point = pos;
        float x = Mathf.Sin(angle * Mathf.Deg2Rad), y = Mathf.Cos(angle * Mathf.Deg2Rad);
        Handle = pos + new Vector3(x * distance, y * distance, 0);
    }
}

//[ExecuteInEditMode]
public class BezierPoint : MonoBehaviour
{
    [SerializeField]
    private Transform handle;

    public Vector3 Point => transform.position;

    public Vector3 Handle 
    { 
        get => handle.position;
        set => handle.transform.position = value;
    }

    public float GetAngleHeuristic(Vector3 worldPoint)
    {
        Vector3 relativePos = (worldPoint - Point).normalized;
        Vector3 handleRelativePos = (Handle - Point).normalized;

        return CalculateSimilarity(relativePos, handleRelativePos);
    }
    
    public float CalculateSimilarity(Vector3 vectorA, Vector3 vectorB)
    {
        float dotProduct = Vector3.Dot(vectorA, vectorB);
        float angle = Mathf.Acos(dotProduct) * Mathf.Rad2Deg;
        
        // Normalize the angle to a value between 0 and 1
        float similarity = 1f - (angle / 180f);
        
        return similarity;
    }
}

public struct TempBezier
{
    public Vector3 point, handle;
    public TempBezier(Vector3 bezierPoint, Vector3 bezierHandle)
    {
        point = bezierPoint;
        handle = bezierHandle;
    }

    public TempBezier(BezierPoint bezierPoint)
    {
        point = bezierPoint.Point;
        handle = bezierPoint.Handle;
    }
}
