using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class CreateOnStart : MonoBehaviour
    {
        [SerializeField]
        private GameObject prefab;

        private bool setup;

        private void Start()
        {
            if (setup)
                return;
            Instantiate(prefab, transform);
            setup = true;
        }
    }
}
