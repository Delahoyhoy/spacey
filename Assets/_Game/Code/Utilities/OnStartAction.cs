using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    public class OnStartAction : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent onStartEvent;
        
        private void Start()
        {
            onStartEvent?.Invoke();
        }
    }

}