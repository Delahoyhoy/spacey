using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    public class InverseBooleanEvent : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<bool> onTrigger;

        public void Trigger(bool active)
        {
            onTrigger?.Invoke(!active);
        }
    }
}
