using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    public class PlatformDependentEvent : MonoBehaviour
    {
        [SerializeField]
        private PlatformEvent[] platformEvents;

        private void Start()
        {
            for (int i = 0; i < platformEvents.Length; i++)
            {
                if (Application.platform == platformEvents[i].platform)
                    platformEvents[i].Trigger();
            }
        }
    }
}
