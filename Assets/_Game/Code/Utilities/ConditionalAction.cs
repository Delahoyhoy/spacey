using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    public class ConditionalAction : MonoBehaviour
    {
        [SerializeField]
        private bool condition;

        [SerializeField]
        private UnityEvent activeEvent, inactiveEvent;

        public void SetCondition(bool conditionActive)
        {
            condition = conditionActive;
        }

        public void Trigger()
        {
            if (condition)
                activeEvent?.Invoke();
            else
                inactiveEvent?.Invoke();
        }
    }
}
