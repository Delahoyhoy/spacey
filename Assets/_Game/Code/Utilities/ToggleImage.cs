using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Utilities
{
    public class ToggleImage : MonoBehaviour
    {
        [SerializeField]
        private Sprite activeSprite, inactiveSprite;

        [SerializeField]
        private Image target;

        public void SetActive(bool active)
        {
            target.sprite = active ? activeSprite : inactiveSprite;
        }
    }
}
