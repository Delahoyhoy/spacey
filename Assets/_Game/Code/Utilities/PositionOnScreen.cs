using System;
using Core.Gameplay.CameraControl;
using UnityEngine;
using UnityEngine.Events;

public class PositionOnScreen : MonoBehaviour
{
    [SerializeField]
    private Transform target; // The target position in world space you want to check

    [SerializeField]
    private UnityEvent<bool> onSetIsOnScreen;
    
    private bool isOnScreen;
    
    private Camera mainCamera;
    private bool initialised;

    private void Start()
    {
        if (initialised)
            return;
        mainCamera = Camera.main;
        isOnScreen = IsPositionOnScreen(target.position);
        onSetIsOnScreen?.Invoke(isOnScreen);
        initialised = true;
    }

    public void GoToPosition()
    {
        BasicCameraController.Instance.Pan(target);
    }

    private void Update()
    {
        if (!mainCamera || !mainCamera.enabled)
            return;

        // Check if the target position is on the screen
        bool isNowOnScreen = IsPositionOnScreen(target.position);

        if (!isNowOnScreen)
        {
            Vector3 right = (target.position - transform.position);
            right.z = 0;
            transform.right = right.normalized;
        }
        
        if (isOnScreen == isNowOnScreen)
            return;
        isOnScreen = isNowOnScreen;
        onSetIsOnScreen?.Invoke(isOnScreen);
    }

    private bool IsPositionOnScreen(Vector3 position)
    {
        if (!initialised)
            return false;
        Vector3 screenPoint = mainCamera.WorldToViewportPoint(position);

        // Check if the position is within the viewport (screen coordinates range from 0 to 1)
        return screenPoint.x >= 0 && screenPoint.x <= 1 && screenPoint.y >= 0 && screenPoint.y <= 1 && screenPoint.z > 0;
    }
}