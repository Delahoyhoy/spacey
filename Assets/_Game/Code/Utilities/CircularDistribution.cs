using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class CircularDistribution : MonoBehaviour
{
    [SerializeField]
    private float distance = 1f;

    [SerializeField]
    private Transform[] children;
    
    private void Distribute(Transform[] children)
    {
        float anglePer = 360f / children.Length;
        for (int i = 0; i < children.Length; i++)
        {
            float angle = (anglePer * i) * Mathf.Deg2Rad;
            float x = Mathf.Sin(angle) * distance;
            float y = Mathf.Cos(angle) * distance;
            Vector3 pos = new Vector3(x, y, 0);
            children[i].transform.localPosition = pos;
            children[i].transform.up = -pos.normalized;
        }
    }

    private void Update()
    {
        // Transform[] children = new Transform[transform.childCount];
        // for (int i = 0; i < children.Length; i++)
        //     children[i].transform.GetChild(i);
        Distribute(children);
    }
}
