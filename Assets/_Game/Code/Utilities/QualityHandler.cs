﻿using System;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class QualityHandler : MonoBehaviour
    {
        [SerializeField]
        private QualitySetting[] settings;

        private bool initialised;
        
        private void Start()
        {
            if (initialised)
                return;
            SetLevel(GameManager.Instance.Quality);
            GameManager.Instance.onSetQuality += SetLevel;
            initialised = true;
        }

        public void SetLevel(int qualityLevel)
        {
            for (int i = 0; i < settings.Length; i++)
            {
                settings[i].SetLevel(qualityLevel);
            }
        }
    }
}