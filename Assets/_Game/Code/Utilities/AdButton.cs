using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using Fumb.Adverts;

public class AdButton : MonoBehaviour
{
    [SerializeField]
    private Button button;
    
    [SerializeField]
    private bool interactable = true;

    [SerializeField]
    private UnityEvent<bool> onSetAdsRemoved, onSetInteractable;

    [SerializeField]
    private UnityEvent adCompleteAction;

    public UnityEvent AdCompleteAction => adCompleteAction;

    [SerializeField]
    private string placementDescription = "GenericRV";

    public bool Interactable
    {
        get => interactable;
        set
        {
            interactable = value;
            bool canInteract = interactable && AdsRemovedOrReady;
            button.interactable = canInteract;
            onSetInteractable?.Invoke(canInteract);
        }
    }

    private const float UpdateRate = 5f;
    private float nextUpdateTime;

    private bool HasRemovedAds => AdHub.Instance.GetAdRemoved(AdType.VideoReward);
    private bool AdsRemovedOrReady => HasRemovedAds || AdvertManager.IsAdLoaded(AdType.VideoReward);
    
    private void Start()
    {
        onSetAdsRemoved?.Invoke(HasRemovedAds);
        AdHub.Instance.onSetAdRemoved += SetRemoveAdsStatus;
    }

    public void SetRemoveAdsStatus(AdType adType, bool removed)
    {
        if (adType == AdType.VideoReward)
            onSetAdsRemoved?.Invoke(removed);
    }

    public void DisplayAd()
    {
        if (HasRemovedAds)
            adCompleteAction?.Invoke();
        else
            AdHub.ShowAd(placementDescription, () =>
            {
                adCompleteAction?.Invoke();
            });
    }

    private void Update()
    {
        if (Time.unscaledTime <= nextUpdateTime)
            return;
        nextUpdateTime = Time.unscaledTime + UpdateRate;
        bool canInteract = interactable && AdsRemovedOrReady;
        button.interactable = canInteract;
        onSetInteractable?.Invoke(canInteract);
    }
}
