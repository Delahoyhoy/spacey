﻿using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class SetObjectActiveHandler : MonoBehaviour
    {
        [SerializeField]
        private GameObject targetObject;

        public void SetObjectActive(bool active)
        {
            if (gameObject.activeSelf != active)
                gameObject.SetActive(active);
        }
    }
}