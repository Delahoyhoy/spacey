﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Core.Gameplay.Utilities
{
    public class ImageReplacer : MonoBehaviour
    {
        [SerializeField]
        private List<Sprite> testImage = new List<Sprite>();

        public Sprite targetImage;

        public Color colourToSet = Color.white;

        [ContextMenu("Replace Images")]
        public void ReplaceImages()
        {
#if UNITY_EDITOR
            Image[] images = GetComponentsInChildren<Image>(true);
            Undo.RecordObjects(images, "ReplaceText");
            foreach (Image image in images)
            {
                if (!testImage.Contains(image.sprite))
                    continue;
                image.sprite = targetImage;
                image.color = colourToSet;
                EditorUtility.SetDirty(image);
            }

            Undo.RecordObjects(images, "ReplacedText");
#endif
        }
    }
}