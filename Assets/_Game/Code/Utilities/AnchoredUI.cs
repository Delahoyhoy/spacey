﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchoredUI : MonoBehaviour
{
    [SerializeField]
    private HighlightType space = HighlightType.World;
    
    private const float defaultCameraSize = 5f;

    [SerializeField]
    private Transform target;

    private bool tracking = true;

    public Transform Target 
    { 
        set 
        {
            target = value;
            tracking = true;
            UpdatePosition();
        } 
    }

    private void OnEnable()
    {
        GameManager.OnCameraMove += SetPosition;
        GameManager.OnZoomChange += SetScale;
    }

    private void OnDisable()
    {
        GameManager.OnCameraMove -= SetPosition;
        GameManager.OnZoomChange -= SetScale;
    }

    private void OnDestroy()
    {
        GameManager.OnCameraMove -= SetPosition;
        GameManager.OnZoomChange -= SetScale;
    }

    public void SetPosition(Vector3 pos)
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        if (!tracking)
            return;
        if (!target)
        {
            Debug.LogError($"TARGET IS NULL FOR {gameObject.name}", gameObject);
            tracking = false;
            return;
        }
        var worldPos = target.position;
        Vector3 position = space == HighlightType.World ? worldPos : 
            Camera.main.WorldToScreenPoint(worldPos);
        position.z = 0;
        transform.position = position;
    }

    public void SetScale(float zoom)
    {
        UpdatePosition();
        //transform.localScale = Vector3.one * (defaultCameraSize / zoom);
    }
}
