using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public static class ColourHexHelper
    {

        private static List<char> validChars = new List<char>
        {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f'
        };

        public static Color GetColourFromHex(string hex)
        {
            if (hex.StartsWith("#"))
                hex = hex.TrimStart('#');

            if (hex.Length != 6)
            {
                hex = hex.Length < 6 ?
                    hex.PadRight(6, '0') :
                    hex.Substring(0, 6);
            }

            hex = hex.ToLower();
            char[] chars = hex.ToCharArray();
            int[] colourValues = new int[3];

            for (int i = chars.Length - 1; i >= 0; i--)
            {
                int value = 0;
                if (validChars.Contains(chars[i]))
                    value = validChars.IndexOf(chars[i]);

                int colourIndex = i / 2;

                if (i % 2 == 0)
                    colourValues[colourIndex] += 16 * value;
                else
                    colourValues[colourIndex] += value;
            }

            float maxValue = 255f;
            float r = colourValues[0] / maxValue,
                g = colourValues[1] / maxValue,
                b = colourValues[2] / maxValue;

            return new Color(r, g, b);
        }

        public static string GetHexValue(this Color colour)
        {
            string hexValue = GetSingleHexValue((int) (colour.r * 255)) +
                              GetSingleHexValue((int) (colour.g * 255)) +
                              GetSingleHexValue((int) (colour.b * 255));
            return hexValue;
        }


        private static string GetSingleHexValue(int colourValue)
        {
            int[] digits = new int[2];
            digits[0] = colourValue / 16;
            digits[1] = colourValue % 16;

            string result = "";
            for (int i = 0; i < digits.Length; i++)
            {
                if (digits[i] >= validChars.Count)
                    result += '0';
                else
                    result += validChars[digits[i]];
            }

            return result;
        }
    }

}