﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class WeightedList<T>
    {
        private List<WeightedValue<T>> list;

        public int Count => list.Count;

        private float TotalWeight
        {
            get
            {
                float total = 0;
                for (int i = 0; i < list.Count; i++)
                    total += list[i].weight;
                return total;
            }
        }

        public void Add(T newValue, float weight)
        {
            list.Add(new WeightedValue<T>(weight, newValue));
        }

        public void Remove(T item)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].value.Equals(item))
                    continue;
                list.RemoveAt(i);
                return;
            }
        }

        public T PickRandom()
        {
            float selection = Random.value * TotalWeight;
            for (int i = 0; i < list.Count; i++)
            {
                float weight = list[i].weight;
                if (selection <= weight)
                    return list[i].value;
                selection -= weight;
            }

            return list[list.Count - 1].value;
        }

        public WeightedList()
        {
            list = new List<WeightedValue<T>>();
        }
    }
}