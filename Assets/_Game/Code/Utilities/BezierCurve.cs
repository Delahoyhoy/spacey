﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathSegement
{
    public Vector3 start, end;
    public float length = 1f;
    public float currentDist = 0;

    public PathSegement(Vector3 a, Vector3 b)
    {
        start = a;
        end = b;
        length = Vector3.Distance(start, end);
    }

    public bool Traverse(float distance, out Vector3 position, out float remainder)
    {
        if (currentDist + distance > length)
        {
            position = end;
            remainder = (currentDist + distance) - length;
            return true;
        }
        else
        {
            currentDist += distance;
            position = Vector3.Lerp(start, end, currentDist / length);
            remainder = 0;
            return false;
        }
    }
}

public class BezierPath
{
    public Queue<PathSegement> pathSegements = new Queue<PathSegement>();

    public Vector3 startPosition;
    
    private PathSegement currentSegment;

    public BezierPath(BezierPoint start, BezierPoint end)
    {
        startPosition = start.Point;
        int segmentCount = Mathf.CeilToInt(Vector3.Distance(start.Point, end.Point) * 4);
        float increment = 1f / segmentCount;
        for (int i = 0; i < segmentCount; i++)
        {
            float a = i * increment;
            float b = (i + 1) * increment;
            Vector3 startPos = BezierCurve.InterpolatePos(start, end, a);
            Vector3 endPos = BezierCurve.InterpolatePos(start, end, b);
            //Debug.DrawLine(startPos, endPos, i % 2 == 0 ? Color.blue : Color.red, 5f);
            pathSegements.Enqueue(new PathSegement(startPos, endPos));
        }
        if (pathSegements.Count > 0)
            currentSegment = pathSegements.Dequeue();
    }

    public BezierPath(TempBezier start, TempBezier end)
    {
        startPosition = start.point;
        int segmentCount = Mathf.CeilToInt(Vector3.Distance(start.point, end.point) * 4);
        float increment = 1f / segmentCount;
        for (int i = 0; i < segmentCount; i++)
        {
            float a = i * increment;
            float b = (i + 1) * increment;
            Vector3 startPos = BezierCurve.InterpolatePos(start, end, a);
            Vector3 endPos = BezierCurve.InterpolatePos(start, end, b);
            //Debug.DrawLine(startPos, endPos, i % 2 == 0 ? Color.blue : Color.red, 5f);
            pathSegements.Enqueue(new PathSegement(startPos, endPos));
        }
        if (pathSegements.Count > 0)
            currentSegment = pathSegements.Dequeue();
    }

    public bool TraversePath(float speed, out Vector3 position)
    {
        if (currentSegment == null)
        {
            position = Vector3.zero;
            return true;
        }
        return TraverseCurrent(speed, out position);
    }

    private bool TraverseCurrent(float speed, out Vector3 position)
    {
        float remainder = 0f;
        Vector3 pos;
        if (currentSegment.Traverse(speed, out pos, out remainder))
        {
            if (pathSegements.Count > 0)
            {
                currentSegment = pathSegements.Dequeue();
                return TraverseCurrent(remainder, out position);
            }
            position = pos;
            return true;
        }
        position = pos;
        return false;
    }
}

[ExecuteInEditMode]
public class BezierCurve : MonoBehaviour
{
    [SerializeField]
    private Transform object1, object2, handle, handle2;

    public static Vector3 InterpolatePos(BezierPoint start, BezierPoint end, float t)
    {
        return GetCubicBezier(t, start.Point, start.Handle, end.Handle, end.Point);
    }

    public static Vector3 InterpolatePos(TempBezier start, TempBezier end, float t)
    {
        return GetCubicBezier(t, start.point, start.handle, end.handle, end.point);
    }

    public static Vector3 InterpolatePos(BezierHolder start, BezierHolder end, float t)
    {
        return GetCubicBezier(t, start.Point, start.Handle, end.Handle, end.Point);
    }

    public static Vector3 GetCubicBezier(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        return (Mathf.Pow(1f - t, 3) * p0) + (3 * Mathf.Pow((1 - t), 2) * t * p1) + 
               (p2 * (3 * (1 - t) * Mathf.Pow(t, 2))) + (Mathf.Pow(t, 3) * p3);
    }

    Vector3 GetBezier(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        return (Mathf.Pow(1f - t, 2) * p0) + (2 * (1 - t) * t * p1) + (Mathf.Pow(t, 2) * p2);
    }
}
