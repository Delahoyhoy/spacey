using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class MoveToFront : MonoBehaviour
    {
        private void Start()
        {
            transform.SetAsLastSibling();
        }
    }
}
