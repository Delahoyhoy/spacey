﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public static class ObjectFinder
{
    public static T FindObjectOfType<T>() where T : MonoBehaviour
    {
        Scene scene = SceneManager.GetActiveScene();
        GameObject[] roots = scene.GetRootGameObjects();
        for (int i = 0; i < roots.Length; i++)
        {
            T target = roots[i].GetComponentInChildren<T>(true);
            if (target != null)
                return target;
        }
        return null;
    }

    public static List<T> FindAllObjectsOfType<T>()
    {
        List<T> list = new List<T>();
        Scene scene = SceneManager.GetActiveScene();
        GameObject[] roots = scene.GetRootGameObjects();
        for (int i = 0; i < roots.Length; i++)
        {
            T[] targets = roots[i].GetComponentsInChildren<T>(true);
            if (targets != null && targets.Length > 0)
                list.AddRange(targets);
        }
        return list;
    }

#if UNITY_EDITOR
    public static List<T> FindAllObjectsInEditor<T>()
    {
        List<T> list = new List<T>();
        Scene scene = EditorSceneManager.GetActiveScene();
        GameObject[] roots = scene.GetRootGameObjects();
        for (int i = 0; i < roots.Length; i++)
        {
            T[] targets = roots[i].GetComponentsInChildren<T>(true);
            if (targets != null && targets.Length > 0)
                list.AddRange(targets);
        }
        return list;
    }
#endif
}