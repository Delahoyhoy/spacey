﻿using System;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public static class CalculationHelper
    {
        public static double Calculate(double baseValue, double step, CalculationType calculationType)
        {
            return calculationType switch
            {
                CalculationType.Exponential => Math.Exp(baseValue * step),
                CalculationType.Power => Math.Pow(baseValue, step),
                _ => baseValue * step,
            };
        }


        public static CalculationType FindBestFitType(double[] xValues, double[] yValues)
        {
            // Calculate the linear fit
            double linearSlope, linearIntercept;
            CalculateLinearFit(xValues, yValues, out linearSlope, out linearIntercept);
            double linearError = CalculateError(xValues, yValues, linearSlope, linearIntercept);

            // Calculate the logarithmic fit
            double logarithmicIntercept, logarithmicBase;
            CalculateLogarithmicFit(xValues, yValues, out logarithmicIntercept, out logarithmicBase);
            double logarithmicError = CalculateError(xValues, yValues, logarithmicIntercept, logarithmicBase);

            // Calculate the exponential fit
            double exponentialBase, exponentialMultiplier;
            CalculateExponentialFit(xValues, yValues, out exponentialBase, out exponentialMultiplier);
            double exponentialError = CalculateError(xValues, yValues, exponentialBase, exponentialMultiplier);

            // Calculate the power curve fit
            double powerBase, powerMultiplier;
            CalculatePowerCurveFit(xValues, yValues, out powerBase, out powerMultiplier);
            double powerCurveError = CalculateError(xValues, yValues, powerBase, powerMultiplier);

            // Determine the best fit type based on the lowest error
            double minError = Mathf.Min((float)linearError, (float)logarithmicError, (float)exponentialError,
                (float)powerCurveError);

            if (Mathf.Approximately((float)linearError, (float)minError))
                return CalculationType.Linear;
            
            if (Mathf.Approximately((float)logarithmicError, (float)minError))
                return CalculationType.Logarithmic;
            
            if (Mathf.Approximately((float)exponentialError, (float)minError))
                return CalculationType.Exponential;
            
            return CalculationType.Power;
        }

        private static void CalculateLinearFit(double[] xValues, double[] yValues, out double slope,
            out double intercept)
        {
            double sumX = 0;
            double sumY = 0;
            double sumXY = 0;
            double sumXX = 0;

            int n = xValues.Length;

            for (int i = 0; i < n; i++)
            {
                sumX += xValues[i];
                sumY += yValues[i];
                sumXY += xValues[i] * yValues[i];
                sumXX += xValues[i] * xValues[i];
            }

            slope = (n * sumXY - sumX * sumY) / (n * sumXX - sumX * sumX);
            intercept = (sumY - slope * sumX) / n;
        }

        private static void CalculateLogarithmicFit(double[] xValues, double[] yValues, out double intercept,
            out double baseValue)
        {
            int n = xValues.Length;

            double sumXLog = 0;
            double sumY = 0;
            double sumXYLog = 0;
            double sumXLogSquared = 0;

            for (int i = 0; i < n; i++)
            {
                double xLog = Mathf.Log((float)xValues[i]);
                sumXLog += xLog;
                sumY += yValues[i];
                sumXYLog += xLog * yValues[i];
                sumXLogSquared += xLog * xLog;
            }

            double slope = (n * sumXYLog - sumXLog * sumY) / (n * sumXLogSquared - sumXLog * sumXLog);
            intercept = (sumY - slope * sumXLog) / n;
            baseValue = Mathf.Exp((float)intercept);
        }

        private static void CalculateExponentialFit(double[] xValues, double[] yValues, out double baseValue,
            out double multiplier)
        {
            int n = xValues.Length;

            double sumX = 0;
            double sumYLog = 0;
            double sumXY = 0;
            double sumXX = 0;

            for (int i = 0; i < n; i++)
            {
                sumX += xValues[i];
                double yLog = Mathf.Log((float)yValues[i]);
                sumYLog += yLog;
                sumXY += xValues[i] * yLog;
                sumXX += xValues[i] * xValues[i];
            }

            double slope = (n * sumXY - sumX * sumYLog) / (n * sumXX - sumX * sumX);
            double intercept = (sumYLog - slope * sumX) / n;
            baseValue = Mathf.Exp((float)intercept);
            multiplier = slope;
        }

        private static void CalculatePowerCurveFit(double[] xValues, double[] yValues, out double baseValue,
            out double multiplier)
        {
            int n = xValues.Length;

            double sumXLog = 0;
            double sumYLog = 0;
            double sumXYLog = 0;
            double sumXLogSquared = 0;

            for (int i = 0; i < n; i++)
            {
                double xLog = Mathf.Log((float)xValues[i]);
                double yLog = Mathf.Log((float)yValues[i]);
                sumXLog += xLog;
                sumYLog += yLog;
                sumXYLog += xLog * yLog;
                sumXLogSquared += xLog * xLog;
            }

            double slope = (n * sumXYLog - sumXLog * sumYLog) / (n * sumXLogSquared - sumXLog * sumXLog);
            double intercept = (sumYLog - slope * sumXLog) / n;
            baseValue = Mathf.Exp((float)intercept);
            multiplier = slope;
        }

        private static double CalculateError(double[] xValues, double[] yValues, double a, double b)
        {
            int n = xValues.Length;
            double sumErrorSquared = 0;

            for (int i = 0; i < n; i++)
            {
                double predictedValue = a + b * xValues[i];
                double error = predictedValue - yValues[i];
                sumErrorSquared += error * error;
            }

            return sumErrorSquared;
        }
    }
}