using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public struct WeightedValue<T>
    {
        public float weight;
        public T value;

        public WeightedValue(float valueWeight, T weightedValue)
        {
            weight = valueWeight;
            value = weightedValue;
        }
    }
}