﻿using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public static class RectTransformExtensions
    {
        public static void SetAndStretchToParentSize(this RectTransform _mRect, RectTransform _parent)
        {
            _mRect.anchoredPosition = _parent.position;
            _mRect.anchorMin = new Vector2(1, 0);
            _mRect.anchorMax = new Vector2(0, 1);
            _mRect.pivot = new Vector2(0.5f, 0.5f);
            _mRect.sizeDelta = _parent.rect.size;
            _mRect.transform.SetParent(_parent);
        }

        public static void AutoStretchToParent(this RectTransform rectTransform, Vector2 minOffset, Vector2 maxOffset)
        {
            rectTransform.anchorMin = new Vector2(0, 0);
            rectTransform.anchorMax = new Vector2(1, 1);
            rectTransform.offsetMin = minOffset;
            rectTransform.offsetMax = -maxOffset;
        }
    }
}