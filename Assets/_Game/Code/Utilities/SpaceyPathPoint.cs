﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public struct SpaceyPathPoint
{
    public Vector3 position, handlePosition, facingDirection;

    public SpaceyPathPoint(Vector3 pos, Vector3 handle, Vector3 facing)
    {
        position = pos;
        handlePosition = handle;
        facingDirection = facing;
    }
}

//public class Path : MonoBehaviour
//{
//    private PathPoint pointA, pointB;

//    public 

//    // Start is called before the first frame update
//    void Start()
//    {
        
//    }

//    // Update is called once per frame
//    void Update()
//    {
        
//    }
//}
