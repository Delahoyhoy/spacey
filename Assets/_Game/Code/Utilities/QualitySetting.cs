﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    [Serializable]
    public class QualitySetting
    {
        [SerializeField]
        private int level;

        public int TargetLevel => level;

        [SerializeField]
        private UnityEvent onSetLevel;

        public void SetLevel(int newLevel)
        {
            if (level == newLevel)
                onSetLevel?.Invoke();
        }
    }
}