﻿using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public static class InterpolationHelper
    {
        public static double Lerp(double a, double b, float t)
        {
            return a + (b - a) * Mathf.Clamp01(t);
        }
    }
}