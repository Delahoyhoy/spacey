﻿using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class IndexedObjectSwapper : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] objects;

        public void SetActiveObject(int index)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                objects[i].SetActive(i == index);
            }
        }
    }
}