using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class ToggleObject : MonoBehaviour
    {
        [SerializeField]
        private GameObject activeObject, inactiveObject;

        public void SetActive(bool active)
        {
            activeObject.SetActive(active);
            inactiveObject.SetActive(!active);
        }
    }
}
