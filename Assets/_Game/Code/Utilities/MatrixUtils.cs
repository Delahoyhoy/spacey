﻿namespace Core.Gameplay.Utilities
{
    public static class MatrixUtils
    {
        // Solves the linear system of equations Ax = b using LU decomposition
        public static void SolveLinearSystem(double[,] A, double[,] LU, double[] b, double[] x)
        {
            int n = A.GetLength(0);

            // Forward substitution (Ly = b)
            double[] y = new double[n];
            for (int i = 0; i < n; i++)
            {
                double sum = b[i];
                for (int j = 0; j < i; j++)
                {
                    sum -= LU[i, j] * y[j];
                }
                y[i] = sum / LU[i, i];
            }

            // Backward substitution (Ux = y)
            for (int i = n - 1; i >= 0; i--)
            {
                double sum = y[i];
                for (int j = i + 1; j < n; j++)
                {
                    sum -= LU[i, j] * x[j];
                }
                x[i] = sum / LU[i, i];
            }
        }
    }
}