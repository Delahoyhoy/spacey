using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Core.Gameplay.Utilities
{
    public class QuickTextReplacer : MonoBehaviour
    {
        [SerializeField]
        private List<Font> testFonts = new List<Font>();

        public Font targetFont;

        public Color colourToSet = Color.white;

        [ContextMenu("Replace Fonts")]
        public void ReplaceFonts()
        {
#if UNITY_EDITOR
            Text[] texts = GetComponentsInChildren<Text>(true);
            Undo.RecordObjects(texts, "ReplaceText");
            foreach (Text text in texts)
            {
                if (!testFonts.Contains(text.font))
                    continue;
                text.font = targetFont;
                text.color = colourToSet;
                EditorUtility.SetDirty(text);
            }

            Undo.RecordObjects(texts, "ReplacedText");
#endif
        }
    }
}
