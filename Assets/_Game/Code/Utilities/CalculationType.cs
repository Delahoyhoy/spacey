﻿namespace Core.Gameplay.Utilities
{
    public enum CalculationType
    {
        Power,
        Exponential,
        Linear,
        Logarithmic,
    }
}