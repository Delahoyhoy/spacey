using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CustomFormatter : MonoBehaviour
{
    [SerializeField] [Multiline]
    private string format = "{0}";
    
    [SerializeField]
    private UnityEvent<string> onSetWithFormat;

    public void SetWithFormat(string text)
    {
        onSetWithFormat?.Invoke(string.Format(format, text));
    }

    public void SetWithNumberFormat(double value)
    {
        onSetWithFormat?.Invoke(string.Format(format, CurrencyController.FormatToString(value)));
    }
    
    public void SetWithIntFormat(int value)
    {
        onSetWithFormat?.Invoke(string.Format(format, CurrencyController.FormatToString(value)));
    }
}
