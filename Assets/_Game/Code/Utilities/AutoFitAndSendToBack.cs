using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Utilities;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class AutoFitAndSendToBack : MonoBehaviour
    {
        [SerializeField]
        private RectTransform rectTransform;

        [SerializeField]
        private Vector2 minOffset, maxOffset;

        private void Start()
        {
            rectTransform.AutoStretchToParent(minOffset, maxOffset);
            rectTransform.SetAsFirstSibling();
        }
    }
}
