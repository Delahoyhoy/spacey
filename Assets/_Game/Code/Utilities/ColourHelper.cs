﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;

public class ColourHelper : MonoBehaviour {

    [SerializeField]
    private Color commonColour, uncommonColour, rareColour, epicColour, legendaryColour;

    public static ColourHelper Instance => instance ??= ObjectFinder.FindObjectOfType<ColourHelper>();
    private static ColourHelper instance;

    public Color GetTierColour(Tier tier)
    {
        switch (tier)
        {
            case Tier.Common:
                return commonColour;
            case Tier.Uncommon:
                return uncommonColour;
            case Tier.Rare:
                return rareColour;
            case Tier.Epic:
                return epicColour;
            case Tier.Legendary:
                return legendaryColour;
            default:
                return commonColour;
        }
    }

    private void Awake()
    {
        instance = this;
    }
}
