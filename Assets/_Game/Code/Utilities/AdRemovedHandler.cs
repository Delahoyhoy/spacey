using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fumb.Adverts;
using UnityEngine.Events;

namespace Core.Gameplay.Utilities
{
    public class AdRemovedHandler : MonoBehaviour
    {
        [SerializeField]
        private AdType adType;

        private bool initialised;
        
        [SerializeField]
        private UnityEvent<bool> onSetAdsRemoved;
        
        private bool HasRemovedAds => AdHub.Instance.GetAdRemoved(adType);
        private bool AdsRemovedOrReady => HasRemovedAds || AdvertManager.IsAdLoaded(adType);
        
        private void Start()
        {
            onSetAdsRemoved?.Invoke(HasRemovedAds);
            AdHub.Instance.onSetAdRemoved += SetRemoveAdsStatus;
        }

        public void SetRemoveAdsStatus(AdType adType, bool removed)
        {
            if (adType == AdType.VideoReward)
                onSetAdsRemoved?.Invoke(removed);
        }
    }
}
