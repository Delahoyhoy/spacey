using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class DebugCurrencyAdder : MonoBehaviour
    {
        [SerializeField]
        
        private CurrencyType currencyType;
        
        [SerializeField]
        private double amountToAdd;

        public void AddCurrency()
        {
            CurrencyController.Instance.AddCurrency(amountToAdd, currencyType);
        }

        public void AddCurrency(float amount)
        {
            CurrencyController.Instance.AddCurrency(amount, currencyType);
        }
    }
}
