using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Utilities
{
    public class EnsureUpright : MonoBehaviour
    {
        private void Update()
        {
            transform.eulerAngles = Vector3.zero;
        }
    }
}
