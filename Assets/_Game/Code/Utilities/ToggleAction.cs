using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleAction : MonoBehaviour
{
    [SerializeField]
    private UnityEvent onSetActive, onSetInactive;
    
    public void SetActive(bool active)
    {
        if (active)
            onSetActive?.Invoke();
        else
            onSetInactive?.Invoke();
    }
}
