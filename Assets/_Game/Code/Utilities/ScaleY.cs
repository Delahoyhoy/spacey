﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

public class ScaleY : Editor
{
    [MenuItem("Fumb/Rescale Sprites")]
    public static void ShrinkSprites()
    {
        List<SpriteRenderer> sprites = ObjectFinder.FindAllObjectsOfType<SpriteRenderer>();
        for (int i = 0; i < sprites.Count; i++)
        {
            if (!sprites[i].enabled)
                continue;
            GameObject newGameObject = new GameObject(sprites[i].gameObject.name + "(scaled)");
            newGameObject.transform.parent = sprites[i].transform;
            newGameObject.AddComponent<SpriteRenderer>();
            SpriteRenderer spriteRenderer = newGameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprites[i].sprite;
            spriteRenderer.sharedMaterial = sprites[i].sharedMaterial;
            spriteRenderer.sortingLayerID = sprites[i].sortingLayerID;
            spriteRenderer.sortingOrder = sprites[i].sortingOrder;
            newGameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            newGameObject.transform.localPosition = Vector3.zero;
            newGameObject.transform.localEulerAngles = Vector3.zero;
            newGameObject.transform.SetAsFirstSibling();
            sprites[i].enabled = false;
        }
    }

    [MenuItem("Fumb/Select Button Children")]
    public static void SelectButtonChildren()
    {
        if (Selection.gameObjects.Length <= 0)
            return;
        GameObject[] currentSelection = Selection.gameObjects;
        List<GameObject> buttonObjects = new List<GameObject>();
        for (int i = 0; i < currentSelection.Length; i++)
        {
            List<UnityEngine.UI.Button> buttons = new List<UnityEngine.UI.Button>(currentSelection[i].GetComponentsInChildren<UnityEngine.UI.Button>(true));
            for (int j = 0; j < buttons.Count; j++)
            {
                if (!buttonObjects.Contains(buttons[j].gameObject))
                    buttonObjects.Add(buttons[j].gameObject);
            }
        }
        Selection.objects = buttonObjects.ToArray();
    }
}
#endif