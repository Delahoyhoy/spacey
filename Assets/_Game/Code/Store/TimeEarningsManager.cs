using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Store
{
    public class TimeEarningsManager : MonoBehaviour
    {
        private static TimeEarningsManager instance;

        private double currentEarningsPerSecond;
        public static double CurrentEarningsPerSecond => instance.currentEarningsPerSecond;

        public static Action<double> onUpdateEarningsPerSecond;

        private bool setup;

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            Setup();
        }

        public void Setup()
        {
            UpdatePerSecondRate();
            if (setup)
                return;
            GameManager.OnSecondUpdate += UpdatePerSecondRate;
            setup = true;
        }

        private void OnDestroy()
        {
            if (setup)
                GameManager.OnSecondUpdate -= UpdatePerSecondRate;
        }

        private void UpdatePerSecondRate()
        {
            currentEarningsPerSecond = GameManager.Instance.GetTotalGPS(true);
            onUpdateEarningsPerSecond?.Invoke(currentEarningsPerSecond);
        }
    }
}
