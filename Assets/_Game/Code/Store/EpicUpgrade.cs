using Fumb.Attribute;
using UnityEngine.Events;

namespace Core.Gameplay.Store
{
    [System.Serializable]
    public struct EpicUpgrade
    {
        public string id;

        public EpicUpgradeType upgradeType;
        
        public double gemCost;
        public string description;
        public bool formatDescription;
        [ConditionalField("formatDescription")]
        public string format;
        [ConditionalField("upgradeType", false, EpicUpgradeType.Income)]
        public bool displayCurrency;
        [ConditionalField("displayCurrency")]
        public string amount;
        [ConditionalField("displayCurrency")]
        public CurrencyType currencyType;

        [ConditionalField("upgradeType", true, EpicUpgradeType.Generic)]
        public int metaData;

        public UnityEvent onPurchase;

        public void Purchase()
        {
            onPurchase?.Invoke();
        }
    }
}