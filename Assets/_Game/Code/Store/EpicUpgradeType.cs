namespace Core.Gameplay.Store
{
    public enum EpicUpgradeType
    {
        Generic,
        Income,
        Chest,
    }
}