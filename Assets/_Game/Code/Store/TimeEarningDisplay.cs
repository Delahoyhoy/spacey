using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Store
{
    public class TimeEarningDisplay : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<string> onSetEarnings;

        [SerializeField]
        private string format = "+{0}";

        [SerializeField]
        private float totalSeconds;

        private bool subscribed;

        private void Start()
        {
            SetEarnings(TimeEarningsManager.CurrentEarningsPerSecond);
            if (subscribed)
                return;
            TimeEarningsManager.onUpdateEarningsPerSecond += SetEarnings;
            subscribed = true;
        }

        private void OnDestroy()
        {
            if (subscribed)
                TimeEarningsManager.onUpdateEarningsPerSecond -= SetEarnings;
        }

        public void SetEarnings(double perSecond)
        {
            onSetEarnings?.Invoke(string.Format(format, CurrencyController.FormatToString(perSecond * totalSeconds)));
        }
    }
}
