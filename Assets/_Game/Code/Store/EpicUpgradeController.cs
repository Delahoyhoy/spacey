﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Core.Gameplay.Augments;
using Core.Gameplay.Earnings;
using Core.Gameplay.Save;
using Fumb.Save;
using Random = UnityEngine.Random;

//using Firebase.Analytics;

namespace Core.Gameplay.Store
{
    public class EpicUpgradeController : MonoBehaviour, ISaveLoad, IReturnToGamePrompt
    {
        [ManagedSaveValue("EpicUpgradeControllerHasClaimed")]
        private SaveValueBool saveHasPromptedStarter;

        public EpicUpgrade[] epicUpgrades;

        private Dictionary<string, EpicUpgrade> epicDictionary = new Dictionary<string, EpicUpgrade>();

        [SerializeField]
        private GameObject currencyObject, starterNotification, starterPack, megaPack, subPanel;

        [SerializeField]
        private UIScreen popup;

        [SerializeField]
        private Text popupDescription, currencyText, costText, starterCountdown;

        private DateTime starterPackEnd;

        [SerializeField]
        private Image currencyIcon;

        [SerializeField]
        private Sprite goldIcon, gemIcon;

        [SerializeField]
        private Button purchaseButton;

        [SerializeField]
        private GameObject getMoreButton;

        [SerializeField]
        private ScrollRect content;

        [SerializeField]
        private float boxPos = 0.75625f, permaPos = 0.51875f, gemPos = 0.2375f;

        [SerializeField]
        private LuckyBoxAugmentsInfo augmentsInfo;

        [SerializeField]
        private UnityEvent<string> onSetRemainingStarterTime;

        [SerializeField]
        private UnityEvent onPromptLastChance;

        private bool starterAvailable = true;

        private string selectedID;

        private string FormatDescription(string description, string format)
        {
            string result = ParseInput(format);
            return string.Format(description, result);
        }

        public bool CheckShouldPrompt()
        {
            if (starterPackEnd > UnbiasedTime.Instance.Now())
                return false;
            if (!saveHasPromptedStarter.IsPopulated)
                return false;
            if (saveHasPromptedStarter.Value)
                return false;

            onPromptLastChance?.Invoke();
            
            saveHasPromptedStarter.Value = true;
            return true;
        }

        public void OpenOnInstant()
        {
            content.verticalNormalizedPosition = 1f;
        }

        public void OpenOnBoxes()
        {
            content.verticalNormalizedPosition = boxPos;
        }

        public void OpenOnGems()
        {
            content.verticalNormalizedPosition = gemPos;
        }

        private string FormatCountdown(TimeSpan timeSpan)
        {
            if (timeSpan.Hours >= 1)
                return $"{timeSpan.Hours}h {timeSpan.Minutes}m";
            return $"{timeSpan.Minutes}m {timeSpan.Seconds}s";
        }
        
        private string FormatCountdownLong(TimeSpan timeSpan)
        {
            return $"{timeSpan.Hours}h {timeSpan.Minutes}m {timeSpan.Seconds}s";
        }

        private IEnumerator RunCountdown()
        {
            while (starterAvailable)
            {
                if (UnbiasedTime.Instance.Now() <= starterPackEnd)
                {
                    TimeSpan remainingTime = starterPackEnd - UnbiasedTime.Instance.Now();
                    starterCountdown.text = FormatCountdown(remainingTime);
                    onSetRemainingStarterTime?.Invoke(FormatCountdownLong(remainingTime));
                    yield return new WaitForSeconds(1f);
                }
                else
                {
                    starterAvailable = false;
                    starterNotification.SetActive(false);
                    starterPack.SetActive(false);
                    subPanel.SetActive(true);
                    megaPack.SetActive(true);
                }
            }
        }

        private static string ParseInput(string format)
        {
            string result = format;
            if (result.Contains("hrs"))
            {
                string hours = result.TrimEnd(new char[] {'h', 'r', 's'});
                if (int.TryParse(hours, out int totalHours))
                {
                    result = CurrencyController.FormatToString(
                        GameManager.Instance.GetGoldEarned(totalHours * 60 * 60, true));
                }

                return result;
            }

            double value = 0;
            if (double.TryParse(format, out value))
            {
                return CurrencyController.FormatToString(value);
            }

            return format;
        }

        public void Select(string id)
        {
            if (!epicDictionary.TryGetValue(id, out EpicUpgrade upgrade)) 
                return;
            selectedID = id;
            AudioManager.Play("Click", 1f);
            //popupObject.SetActive(true);
            popup.Open();
            if (upgrade.formatDescription)
                popupDescription.text = FormatDescription(upgrade.description, upgrade.format);
            else
                popupDescription.text = upgrade.description;
            currencyObject.SetActive(false);
            augmentsInfo.gameObject.SetActive(false);
            switch (upgrade.upgradeType)
            {
                case EpicUpgradeType.Income:
                    currencyObject.SetActive(upgrade.displayCurrency);
                    if (upgrade.displayCurrency)
                    {
                        currencyText.text = ParseInput(upgrade.amount);
                        currencyIcon.sprite = upgrade.currencyType switch
                        {
                            CurrencyType.HardCurrency => gemIcon,
                            _ => goldIcon
                        };
                    }
                    break;
                case EpicUpgradeType.Chest:
                    if (LuckyBoxManager.Instance.TryGetBoxTemplate((Tier) upgrade.metaData,
                        out LuckyBoxTemplate template))
                    {
                        augmentsInfo.gameObject.SetActive(true);
                        augmentsInfo.Setup(template);
                    }
                    break;
            }

            costText.text = CurrencyController.FormatToString(upgrade.gemCost);
            bool canAfford = CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) >= upgrade.gemCost;
            purchaseButton.interactable = canAfford;
            getMoreButton.SetActive(!canAfford);
        }

        private void SetupStarterPack(DateTime endTime)
        {
            if (UnbiasedTime.Instance.Now() >= endTime)
            {
                starterAvailable = false;
                starterNotification.SetActive(false);
                starterPack.SetActive(false);
                subPanel.SetActive(true);
                megaPack.SetActive(true);
            }
            else
            {
                starterAvailable = true;
                starterNotification.SetActive(true);
                starterPack.SetActive(true);
                subPanel.SetActive(false);
                megaPack.SetActive(false);
                StartCoroutine(RunCountdown());
            }
        }

        public void AddGold(float hours)
        {
            CoinBurster.Instance.NewBurst(CurrencyType.SoftCurrency, currencyIcon.transform, Random.Range(5, 10),
                GameManager.Instance.GetGoldEarned(hours * 60 * 60, true));
        }

        public void Purchase()
        {
            if (epicDictionary.ContainsKey(selectedID))
            {
                double gemCost = epicDictionary[selectedID].gemCost;
                if (CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) >= gemCost)
                {
                    AudioManager.Play("Click", 1f);
                    CurrencyController.Instance.AddCurrency(-gemCost, CurrencyType.HardCurrency, false, "premium_store", $"purchase_{selectedID}");
                    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
                    /*
                List<Firebase.Analytics.Parameter> analyticsParameters = new List<Firebase.Analytics.Parameter>();
                analyticsParameters.Add(new Firebase.Analytics.Parameter("PurchaseID", selectedID));
                analyticsParameters.Add(new Firebase.Analytics.Parameter("GemCost", gemCost));
                AnalyticsManager.Instance.TriggerEvent(AnalyticsEvent.Purchase, analyticsParameters);*/
                    epicDictionary[selectedID].Purchase();
                    //popupObject.SetActive(false);
                    popup.Close();
                }
            }
        }

        // Use this for initialization
        private void Awake()
        {
            for (int i = 0; i < epicUpgrades.Length; i++)
            {
                if (!epicDictionary.ContainsKey(epicUpgrades[i].id))
                    epicDictionary.Add(epicUpgrades[i].id, epicUpgrades[i]);
            }
        }

        public void ClaimStarterPack()
        {
            starterAvailable = false;
            starterNotification.SetActive(false);
            starterPack.SetActive(false);
            megaPack.SetActive(true);
            subPanel.SetActive(true);
            starterPackEnd = UnbiasedTime.Instance.Now().AddHours(-1);
            SetupStarterPack(starterPackEnd);
            saveHasPromptedStarter.Value = true;
        }

        private void Start()
        {
            //starterPackEnd = MergeManager.GetDateTime(SaveManager.GetString("starterEnd", ""), UnbiasedTime.Instance.Now().AddHours(24));
            //SaveManager.SetString("starterEnd", MergeManager.SaveDateTime(starterPackEnd));
            //Debug.Log(starterPackEnd);
            //SetupStarterPack(starterPackEnd);
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("starterPackEnd", JC_SaveManager.SaveDateTime(starterPackEnd));
            //throw new System.NotImplementedException();
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            starterPackEnd = JC_SaveManager.GetDateTime(loadedData.LoadData("starterPackEnd", ""),
                UnbiasedTime.Instance.Now().AddDays(1));
            SetupStarterPack(starterPackEnd);
            //throw new System.NotImplementedException();
        }

        public void LoadDefault()
        {
            starterPackEnd = UnbiasedTime.Instance.Now().AddDays(1);
            SetupStarterPack(starterPackEnd);
            saveHasPromptedStarter.Value = false;
            //throw new System.NotImplementedException();
        }
    }

}