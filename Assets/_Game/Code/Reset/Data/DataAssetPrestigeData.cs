﻿#if USE_FUMB_DATA
using Core.Gameplay.Reset;
using Fumb.Data;
using UnityEngine;

namespace Data {
    [CreateAssetMenu(fileName = "DataAssetPrestigeData", menuName = "Data/DataAsset/DataAssetPrestigeData", order = 10000)]
    public class DataAssetPrestigeData : DataAsset<PrestigeData> { 
    
        /// <summary>
        /// If you have multiple instance of the same type of DataAsset, use this override to differentiate them for runtime lookup.
        /// For example, your asset could contain an Enum Category Type, and return that value cast to string.
        /// </summary>
        public override string Variant => "";
    }
}
#endif