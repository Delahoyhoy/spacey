using System;
using UnityEngine;

namespace Core.Gameplay.Reset
{
    [Serializable]
    public class PrestigeData
    {
        [SerializeField]
        private float prestigeMinTime;

        public float PrestigeMinTime => prestigeMinTime;

        [SerializeField]
        private int prestigeMinReq;

        public int PrestigeMinReq => prestigeMinReq;

        [SerializeField]
        private float prestigeIncrease;

        public float PrestigeIncrease => prestigeIncrease;

        [SerializeField]
        private double prestigeDivide;

        public double PrestigeDivisor => prestigeDivide;

        [SerializeField]
        private double prestigeExponent;

        public double PrestigeExponent => prestigeExponent;

        [SerializeField]
        private double prestigeExchange;

        public double PrestigeExchange => prestigeExchange;

        [SerializeField]
        private int prestigeStarFragmentLevel;

        public int PrestigeStarFragmentLevel => prestigeStarFragmentLevel;

        public double GetPrestigeCurrency(double fromEarnings)
        {
            return Math.Pow(fromEarnings / prestigeDivide, prestigeExponent);
        }
    }
}
