﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Core.Gameplay.Research;
using Core.Gameplay.Sectors;
using Core.Gameplay.Ships;
using Core.Gameplay.StarFragments;
using Core.Gameplay.UserInterface;
using Core.Gameplay.Version;
using Core.Gameplay.VisualEffects;
using Data;
using Fumb.Data;
using Fumb.RemoteConfig;
using TMPro;

namespace Core.Gameplay.Reset
{
    public class ResetManager : MonoBehaviour
    {
        [SerializeField]
        private UIScreen switchSystemScreen, doublePrestigeScreen;

        [SerializeField]
        private Text currentPrestige, newPrestige, creditsText, introText;

        [SerializeField]
        [Multiline]
        private string available, stillWaiting;

        [SerializeField]
        private Button resetButton;

        [SerializeField]
        private UnityEvent onCompleteReset;
        
        [SerializeField]
        private UnityEvent onStartPrestige, onCompletePrestige;

        public Action<ResetType> preReset, beginReset, finishReset;

        [HideInInspector]
        public bool resetting = false;

        [SerializeField]
        private GameObject requirement, creditsDisplay, creditsGlow;

        [SerializeField]
        private SectorJump sectorJump;

        [SerializeField] [Multiline]
        private string bottomInfoFormat;

        [SerializeField]
        private string totalFormat = "{0} Dark Matter", gainedFormat = "Total Gained: +<sprite index=2>{0}";

        [SerializeField]
        private TextMeshProUGUI bottomInfoText, totalText, addedText;

        [SerializeField]
        private UnityEvent<int> onSetRequirement, onSetUnlockRequirement;

        private int PrestigeRequirement =>
            Mathf.RoundToInt(PrestigeSetup.PrestigeMinReq * (1 + PrestigeSetup.PrestigeIncrease * 
                                                                  (int)StatTracker.Instance.GetStat("timesPrestiged")));

        private RemoteInt doublePrestigeCost = new RemoteInt("doublePrestigeCost", 100);

        private bool DoublePrestigeAvailable => doublePrestigeCost > 0;

        private static ResetManager instance;

        public static ResetManager Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<ResetManager>();
                return instance;
            }
        }

        [SerializeField]
        private UIScreen systemConvert;

        [SerializeField]
        private GameObject characterObject;

        [SerializeField]
        private Animator characterAnim;

        [SerializeField]
        private BaseTypewriter typewriter;

        [SerializeField]
        [Multiline]
        private string displayText;

        [SerializeField]
        private UnityEvent<bool> onSetUnlocked;

        private bool dataInitialised;
        private PrestigeData prestigeData;

        public PrestigeData PrestigeSetup
        {
            get
            {
                InitialisePrestigeData();
                return prestigeData;
            }
        }

        private void OnDataUpdated()
        {
            prestigeData = DataAccessor.GetData<DataAssetPrestigeData>().Data[0];
            onSetRequirement?.Invoke(PrestigeRequirement);
            if (switchSystemScreen.IsOpen)
                SetupScreen();
        }
        
        private void InitialisePrestigeData()
        {
            if (dataInitialised)
                return;

            prestigeData = DataAccessor.GetData<DataAssetPrestigeData>().Data[0];
            DataAccessor.RemoteDataChangeDetectedAction += OnDataUpdated;
            dataInitialised = true;
        }

        private void Start()
        {
            InitialisePrestigeData();
            onSetRequirement?.Invoke(PrestigeRequirement);
        }

        public void HideCharacter()
        {
            if (characterAnim)
            {
                characterAnim.SetBool(MoveIn, false);
            }

            onCharacterComplete.Invoke();
            StartCoroutine(WaitDisable());
        }

        private void ShowCharacter()
        {
            characterObject.SetActive(true);
            if (characterAnim)
                characterAnim.SetBool(MoveIn, true);
            //characterHidden = false;
        }

        private IEnumerator WaitDisable()
        {
            yield return new WaitForSeconds(1f);
            characterObject.SetActive(false);
        }

        [SerializeField]
        private UnityEvent onTypewriterBegin, onTypewriterEnd, onCharacterComplete;

        private static readonly int MoveIn = Animator.StringToHash("MoveIn");

        private void ShowMessage()
        {
            ShowCharacter();
            if (typewriter)
                typewriter.PlayTypeWriter(displayText, () => onTypewriterBegin?.Invoke(), 
                    () =>  onTypewriterEnd?.Invoke(), true, 1f);
        }

        public void Prestige()
        {
            bool wasUsingNewSystem = GameVersionManager.CurrentVersion != GameVersion.Version1;
            double resetCountBefore = StatTracker.Instance.GetStat("timesPrestiged");
            // if (!wasUsingNewSystem)
            // {
            //     GameManager.Instance.UsingNewSystem = true;
            //     GameVersionManager.Instance.UpdateToLatestVersion();
            // }

            CurrencyController.Instance.ResetCurrency();
            ResearchManager.Instance.ResetResearch();
            ShipManager.Instance.ClearCurrentShips();
            GameManager.Instance.ResetScene(ResetType.Prestige);
            resetting = false;
            onCompletePrestige?.Invoke();
            onCompleteReset?.Invoke();
            finishReset?.Invoke(ResetType.Prestige);
            onSetRequirement?.Invoke(PrestigeRequirement);
            // if (resetCountBefore < 1)
            // {
            //     if (!wasUsingNewSystem)
            //         systemConvert.Open();
            //     else
            //         ShowMessage();
            // }

            Check();
        }

        public void RegisterPreReset()
        {
            preReset?.Invoke(ResetType.SectorJump);
        }
        
        public void ResetForSectorJump()
        {
            beginReset?.Invoke(ResetType.SectorJump);
            GameManager.Instance.ResetScene(ResetType.SectorJump);
            finishReset?.Invoke(ResetType.SectorJump);
        }

        private void OnEnable()
        {
            GameManager.OnSecondUpdate += Check;
        }

        private void OnDestroy()
        {
            GameManager.OnSecondUpdate -= Check;
            if (dataInitialised)
                DataAccessor.RemoteDataChangeDetectedAction -= OnDataUpdated;
        }

        public void FireOffPrestige()
        {
            if (CurrencyController.Instance.GetPrestigeToAdd() < PrestigeRequirement)
                return;
            BeginPrestige();
        }

        private void BeginPrestige()
        {
            switchSystemScreen.Close();
            onStartPrestige?.Invoke();
            resetting = true;
            beginReset?.Invoke(ResetType.Prestige);
            if (GameVersionManager.CurrentVersion < GameVersion.Version3)
                GameManager.Instance.BeginPrestige();
            else
            {
                void CompletePrestige()
                {
                    Prestige();
                    ClusterManager.Instance.ReturnToSecondCluster();
                    MiningStation.Instance.UpdateOutpostCount(0);
                }
                sectorJump.ExecutePrestigeJump(CompletePrestige);
            }
        }

        public void ForcePrestige()
        {
            BeginPrestige();
        }

        public void SetupScreen()
        {
            switchSystemScreen.Open();
            RefreshScreen();
        }

        public void RefreshScreen()
        {
            if (!switchSystemScreen.IsOpen)
                return;
            double currentPrestigeCurrency = CurrencyController.Instance.CurrentPrestige;
            currentPrestige.text = CurrencyController.FormatToString(currentPrestigeCurrency);
            double prestigeToAdd = CurrencyController.Instance.GetPrestigeToAdd();
            newPrestige.text = "+" + CurrencyController.FormatToString(prestigeToAdd);
            introText.text = prestigeToAdd >= PrestigeRequirement ? available : stillWaiting;
            resetButton.interactable = prestigeToAdd >= PrestigeRequirement;
            requirement.SetActive(prestigeToAdd < PrestigeRequirement);
            
            double newTotal = currentPrestigeCurrency + prestigeToAdd;
            
            totalText.text = string.Format(totalFormat, newTotal.FormatAsCurrency());
            addedText.text = string.Format(gainedFormat, prestigeToAdd.FormatAsCurrency());

            bottomInfoText.text = string.Format(bottomInfoFormat, newTotal.FormatAsCurrency());

            bool unlocked = GameVersionManager.CurrentVersion < GameVersion.Version3 ||
                            StatTracker.Instance.GetStat("timesPrestiged") > 0 ||
                            StarFragmentManager.Instance.Fragments >= PrestigeSetup.PrestigeStarFragmentLevel;
            onSetUnlocked?.Invoke(unlocked);
            if (!unlocked)
                onSetUnlockRequirement?.Invoke(PrestigeSetup.PrestigeStarFragmentLevel);
        }

        private void Check()
        {
            double toAdd = CurrencyController.Instance.GetPrestigeToAdd();
            creditsDisplay.SetActive(CurrencyController.Instance.CurrentPrestige > 0 || toAdd >= PrestigeRequirement);
            creditsText.text = CurrencyController.FormatToString(CurrencyController.Instance.CurrentPrestige);
            creditsGlow.SetActive(toAdd >= PrestigeRequirement);
        }
        
        private void Awake()
        {
            instance = this;
        }
    }
}
