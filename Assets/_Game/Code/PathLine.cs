﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PathLine : MonoBehaviour
{
    [SerializeField]
    private float speed = 0.25f, minZoom = 10f, maxZoom = 20f;

    private float totalLength = 1f, offset = 0;

    private LineRenderer lineRenderer;

    private MaterialPropertyBlock propertyBlock;

    private TempBezier startBezier, endBezier;

    public BezierPath GetPath() => new BezierPath(startBezier, endBezier);

    public void Setup(BezierPoint start, BezierPoint end)
    {
        propertyBlock = new MaterialPropertyBlock();
        lineRenderer = GetComponent<LineRenderer>();
        int pointCount = Mathf.CeilToInt(Vector3.Distance(start.Point, end.Point) * 3f);
        lineRenderer.positionCount = pointCount;
        Vector3[] points = new Vector3[pointCount];

        float distance = Vector3.Distance(start.Handle, end.Point) / 3f;
        startBezier = new TempBezier(start.Point, start.Point + (start.Handle - start.Point).normalized * distance);
        endBezier = new TempBezier(end.Point, end.Point + (end.Handle - end.Point).normalized * distance);
        
        totalLength = 0;
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = BezierCurve.InterpolatePos(endBezier, startBezier, (float)i / (points.Length - 1));
            points[i].z = 1f;
            if (i > 0)
            {
                totalLength += Vector3.Distance(points[i], points[i - 1]);
            }
        }
        propertyBlock.SetFloat("_TotalLength", totalLength);
        propertyBlock.SetFloat("_Offset", offset);
        lineRenderer.SetPropertyBlock(propertyBlock);
        lineRenderer.SetPositions(points);
        RefreshZoom(BasicCameraController.Instance.CurrentZoom);
    }

    public static float GetTotalDistance(BezierPoint start, BezierPoint end)
    {
        float distance = 0;
        int pointCount = Mathf.CeilToInt(Vector3.Distance(start.Point, end.Point) / 1.5f);
        Vector3[] points = new Vector3[pointCount];
        for (int i = 0; i < pointCount; i++)
        {
            points[i] = BezierCurve.InterpolatePos(end, start, (float)i / (points.Length - 1));
            points[i].z = 1f;
            if (i > 0)
            {
                distance += Vector3.Distance(points[i], points[i - 1]);
            }
        }
        return distance;
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    private void OnEnable()
    {
        GameManager.OnZoomChange += RefreshZoom;
    }
    
    private void OnDisable()
    {
        GameManager.OnZoomChange -= RefreshZoom;
    }


    private void OnDestroy()
    {
        GameManager.OnZoomChange -= RefreshZoom;
    }

    private void RefreshZoom(float zoom)
    {
        float opacity = Mathf.Clamp01((zoom - minZoom) / (maxZoom - minZoom));
        propertyBlock.SetFloat("_Opacity", opacity);
        lineRenderer.SetPropertyBlock(propertyBlock);
    }

    // Update is called once per frame
    void Update()
    {
        if (!lineRenderer || propertyBlock == null)
            return;
        offset += Time.deltaTime * speed;
        propertyBlock.SetFloat("_Offset", offset);
        lineRenderer.SetPropertyBlock(propertyBlock);
    }
}
