namespace Core.Gameplay.Earnings
{
    public enum OfflineEarningPreviewType
    {
        Total,
        Difference,
    }
}