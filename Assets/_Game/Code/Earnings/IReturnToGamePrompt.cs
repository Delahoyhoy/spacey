﻿namespace Core.Gameplay.Earnings
{
    public interface IReturnToGamePrompt
    {
        public bool CheckShouldPrompt();
    }
}