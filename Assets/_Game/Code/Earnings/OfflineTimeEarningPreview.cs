using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Core.Gameplay.Earnings
{
    public class OfflineTimeEarningPreview : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<string> onSetValue;

        [SerializeField]
        private string multiplierId;

        [SerializeField]
        private string format;

        [SerializeField]
        private OfflineEarningPreviewType previewType;

        private double Value => previewType switch
        {
            OfflineEarningPreviewType.Difference => OfflineTimeMultiplier.Instance.GetIncreaseWithMult(multiplierId),
            _ => OfflineTimeMultiplier.Instance.GetTotalHoursWithMult(multiplierId),
        };
        
        private void Start()
        {
            UpdateValue();
        }

        public void UpdateValue()
        {
            onSetValue?.Invoke(string.Format(format, Value));
        }
    }
}
