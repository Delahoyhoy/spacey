﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Outposts;
using Core.Gameplay.Reset;
using Core.Gameplay.Save;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using Core.Gameplay.VIP;
using Fumb.RemoteConfig;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

//using Firebase.Analytics;

namespace Core.Gameplay.Earnings
{
    public class TimeSinceAwayHandler : MonoBehaviour
    {

        [SerializeField]
        private UIScreen screen;

        [SerializeField]
        private Text displayText, earnedText, watchAdText;

        [SerializeField]
        [Multiline]
        private string adReadyText, adWatchedText, adUnavailableText, tripledText;

        private double amount;

        [SerializeField]
        private GameObject doubleText, claimButton, timerHolder;

        [SerializeField]
        private Button doubleButton, tripleButton;

        [SerializeField]
        private float openDelay = 5f;

        private int tripleCost = 15;

        private bool isOpening = false;

        [SerializeField]
        private Slider timeAwayIndicator;

        [SerializeField]
        private Text maxTimeAway, totalTimeAway;

        private double MaxOfflineSeconds => offlineMultHandler.MaxTotalSeconds;

        private double timeAway, totalEarnings;

        [SerializeField]
        private OfflineTimeMultiplier offlineMultHandler;

        [SerializeField]
        private RectTransform currencyDisp;

        private bool adWatched = false;

        public void OpenMenu(double earned, double totalSeconds)
        {
            if (earned <= 0 || !FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;

            double maxSeconds = MaxOfflineSeconds;
            double adjustedEarnings = earned * VIPManager.Instance.GetValue(VIPCategory.OfflineEarnings) / 100f;
            
            if (isOpening)
            {
                if (earned <= totalEarnings)
                    return;
                timeAway = totalSeconds;
                adWatched = false;
                totalEarnings = earned;
                if (totalSeconds > maxSeconds)
                    adjustedEarnings *= (maxSeconds / totalSeconds);
                amount = adjustedEarnings;
                
                earnedText.text = CurrencyController.FormatToString(amount);
                timeAwayIndicator.value = (float)Math.Min(1, timeAway / MaxOfflineSeconds);
                maxTimeAway.text = string.Format("MAX {0:#0.#}h", MaxOfflineSeconds / 3600);
                totalTimeAway.text = FormatTimeValue(timeAway);
                
                LayoutRebuilder.ForceRebuildLayoutImmediate(currencyDisp);
                return;
            }
            totalEarnings = earned;
            amount = adjustedEarnings;
            timeAway = totalSeconds;
            adWatched = false;
            StartCoroutine(WaitOpen());
        }

        public void CalculateEarnings(double seconds)
        {
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                return;
            if (GameVersionManager.CurrentVersion >= GameVersion.Version3)
            {
                SimulateTimeAway(seconds);
                return;
            }
            if (seconds < 120)
                return;
            double totalEarnings = seconds * GameManager.Instance.GetTotalGPS(true);
            OpenMenu(totalEarnings, seconds);
        }

        public void SimulateTimeAway(double seconds)
        {
            double clampedTime = seconds;
            if (clampedTime >= MaxOfflineSeconds)
                clampedTime = MaxOfflineSeconds;
            double vipLevelMult = VIPManager.Instance.GetValue(VIPCategory.OfflineEarnings) / 100d;
            double adjustedTime = vipLevelMult * clampedTime;
            if (seconds < 120 || isOpening || screen.IsOpen)
            {
                CurrencyController.Instance.AddCurrency(OutpostManager.Instance.SimulateOfflineEarnings(adjustedTime),
                    CurrencyType.SoftCurrency, false, "offline", "claim_offline");
                return;
            }

            double totalValue = OutpostManager.Instance.SimulateOfflineEarnings(adjustedTime);
            
            if (totalValue <= 0)
                return;
            
            totalEarnings = totalValue;
            amount = totalValue;
            timeAway = seconds;
            adWatched = false;
            StartCoroutine(WaitOpen());
        }

        public void KickOffReset()
        {
            amount = 0;
        }

        private IEnumerator WaitOpen()
        {
            isOpening = true;
            yield return new WaitForSeconds(openDelay);
            if (amount <= 0 || !FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) || ResetManager.Instance.resetting)
            {
                isOpening = false;
                yield break;
            }
            
            timerHolder.SetActive(true);

            double maxSeconds = MaxOfflineSeconds;
            if (timeAway > maxSeconds)
                amount *= (maxSeconds / timeAway);

            //if (!SoulGemMenu.resetting)
            //{
            screen.Open();
            adWatched = false;
            bool adReady = AdHub.Ready;
            bool canLoadAd = Application.internetReachability != NetworkReachability.NotReachable;
            displayText.text =
                canLoadAd ?
                    adReadyText :
                    adUnavailableText; //LocalisationManager.Instance.GetLocalisedString("return_normal");
            doubleText.SetActive(canLoadAd || adReady);
            earnedText.text = CurrencyController.FormatToString(amount);
            LayoutRebuilder.MarkLayoutForRebuild(currencyDisp);

            timeAwayIndicator.value = (float)Math.Min(1, timeAway / maxSeconds);
            maxTimeAway.text = string.Format("MAX {0:#0.#}h", maxSeconds / 3600);
            totalTimeAway.text = FormatTimeValue(timeAway);

            claimButton.SetActive(false);
            doubleButton.gameObject.SetActive(true);
            tripleButton.gameObject.SetActive(true);
            if (canLoadAd)
            {
                doubleButton.gameObject.SetActive(true);
                doubleButton.interactable = adReady;
                watchAdText.text = adReady ? "" : "LOADING...";
                if (!adReady)
                    StartCoroutine(WaitLoadAd());
            }
            else
            {
                doubleButton.gameObject.SetActive(adReady);
                doubleButton.interactable = adReady;
                watchAdText.text = "";
            }

            tripleButton.interactable = CurrencyController.Instance.CurrentDiamonds >= tripleCost;
            //}
        }

        private IEnumerator WaitLoadAd()
        {
            yield return new WaitForSeconds(1f);
            bool adReady = AdHub.Ready;
            if (adReady)
            {
                doubleButton.interactable = true;
                watchAdText.text = "";
            }
            else
            {
                if (screen.IsOpen)
                    StartCoroutine(WaitLoadAd());
            }
        }

        private static string FormatTimeValue(double totalSeconds)
        {
            if (totalSeconds < 3600)
                return $"{totalSeconds / 60:#0} Minutes";
            return $"{totalSeconds / 3600:#0.#} Hours"; 
        }

        public void TripleEarnings()
        {
            if (adWatched)
                return;
            if (CurrencyController.Instance.CurrentDiamonds < tripleCost)
                return;
            timerHolder.SetActive(false);
            AudioManager.Play("Milestone", 0.5f);
            CurrencyController.Instance.AddCurrency(-tripleCost, CurrencyType.HardCurrency, false, "offline_boost", "triple_offline");
            amount *= 3;

            adWatched = true;
            doubleText.SetActive(false);
            displayText.text = tripledText; // LocalisationManager.Instance.GetLocalisedString("return_doubled"); ;
            earnedText.text = CurrencyController.FormatToString(amount);

            claimButton.SetActive(true);
            doubleButton.gameObject.SetActive(false);
            tripleButton.gameObject.SetActive(false);
        }

        public void DoubleUp()
        {
            if (AdHub.Ready)
                AdHub.ShowAd("DoubleOffline", DoubleEarnings);
        }

        public void DoubleEarnings()
        {
            if (adWatched)
                return;
            timerHolder.SetActive(false);
            VIPManager.Instance.AddVIPPoints(20);
            amount *= 2f;
            adWatched = true;
            doubleText.SetActive(false);
            displayText.text = adWatchedText; // LocalisationManager.Instance.GetLocalisedString("return_doubled"); ;
            earnedText.text = CurrencyController.FormatToString(amount);

            claimButton.SetActive(true);
            doubleButton.gameObject.SetActive(false);
            tripleButton.gameObject.SetActive(false);
        }

        public void Collect()
        {
            isOpening = false;
            //screen.Close();
            //if (adWatched)
            CoinBurster.Instance.NewBurst(CurrencyType.SoftCurrency, currencyDisp, Random.Range(6, 10),
                amount); //CurrencyController.Instance.AddGoldWithEffect(amount);
            CurrencyController.Instance.CurrencyEvent(CurrencyType.SoftCurrency, amount, "offline", "claim_offline");
            // else
            //     CurrencyController.Instance.AddCurrency(amount, CurrencyType.Gold);
            List<IReturnToGamePrompt> returnToGamePrompts = ObjectFinder.FindAllObjectsOfType<IReturnToGamePrompt>();
            for (int i = 0; i < returnToGamePrompts.Count; i++)
            {
                if (returnToGamePrompts[i].CheckShouldPrompt())
                    return;
            }
        }

        private void OnEnable()
        {
            JC_SaveManager.OnReturnToGame += CalculateEarnings;
        }

        private void OnDestroy()
        {
            JC_SaveManager.OnReturnToGame -= CalculateEarnings;
        }
    }
}
