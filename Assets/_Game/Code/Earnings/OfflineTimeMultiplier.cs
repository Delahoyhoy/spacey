using System;
using System.Collections.Generic;
using Core.Gameplay.Save;
using Fumb.RemoteConfig;
using Fumb.Save;
using UnityEngine;

namespace Core.Gameplay.Earnings
{
    public class OfflineTimeMultiplier : MonoBehaviour, ISaveLoad
    {
        private static OfflineTimeMultiplier instance;
        public static OfflineTimeMultiplier Instance =>
            instance ??= ObjectFinder.FindObjectOfType<OfflineTimeMultiplier>();
        
        [SerializeField]
        private OfflineTimeOption[] options;

        private Dictionary<string, OfflineTimeOption> multiplierLookup =
            new Dictionary<string, OfflineTimeOption>();
        
        private Dictionary<string, List<string>> multiplierFlags = new Dictionary<string, List<string>>();
        
        private RemoteFloat maxHoursOffline = new RemoteFloat("maxOfflineHours", 4);

        [ManagedSaveValue("OfflineTimeMultLastMaximum")]
        private SaveValueDouble saveLastMaximumTime;

        public double MaxTotalSeconds => Math.Max(saveLastMaximumTime.Value, maxHoursOffline * DetermineMultiplier() * 3600f);

        private void Awake()
        {
            for (int i = 0; i < options.Length; i++)
            {
                multiplierLookup.Add(options[i].identifier, options[i]);
            }
        }

        public float DetermineMultiplier()
        {
            if (multiplierFlags.Count <= 0)
                return 1;
            float highestMult = 1;
            foreach (string option in multiplierFlags.Keys)
            {
                if (multiplierFlags[option].Count <= 0)
                    continue;
                if (!multiplierLookup.TryGetValue(option, out OfflineTimeOption multiplier))
                    continue;
                float mult = multiplier.Multiplier;
                if (mult > highestMult)
                    highestMult = mult;
            }

            return highestMult;
        }

        public void AddFlag(string multId, string flag)
        {
            if (!multiplierFlags.ContainsKey(multId))
                multiplierFlags.Add(multId, new List<string>());
            if (!multiplierFlags[multId].Contains(flag))
                multiplierFlags[multId].Add(flag);
        }

        public void RemoveFlag(string multId, string flag)
        {
            if (!multiplierFlags.ContainsKey(multId))
                return;
            if (multiplierFlags[multId].Contains(flag))
                multiplierFlags[multId].Remove(flag);
        }

        public double GetTotalHoursWithMult(string multId)
        {
            if (!multiplierLookup.TryGetValue(multId, out OfflineTimeOption multiplier))
                return maxHoursOffline;
            return maxHoursOffline * multiplier.Multiplier;
        }

        public double GetIncreaseWithMult(string multId)
        {
            return GetTotalHoursWithMult(multId) - maxHoursOffline;
        }

        public void EnableMult(string multId)
        {
            AddFlag(multId, multId);
        }

        public void DisableMult(string multId)
        {
            RemoveFlag(multId, multId);
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveLastMaximumTime.Value = MaxTotalSeconds;
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            
        }

        public void LoadDefault()
        {
            
        }
    }
}