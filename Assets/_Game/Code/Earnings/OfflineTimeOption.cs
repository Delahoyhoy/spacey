using System;
using Fumb.RemoteConfig;

namespace Core.Gameplay.Earnings
{
    [Serializable]
    public struct OfflineTimeOption
    {
        public string identifier;
        public RemoteFloat remoteValue;

        public float Multiplier => remoteValue;
    }
}