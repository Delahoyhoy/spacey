﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using Fumb.Data;
using Fumb.Serialization;
using UnityEditor;
using UnityEngine;

namespace Core.Gameplay.Dialogue
{
    [CreateAssetMenu(fileName = "DataAssetDialogueData", menuName = "Data/DataAsset/Dialogue/DataAssetDialogueData", order = 10000)]
    public class DataAssetDialogueData : DataAsset<DialogueData> {
        public static Action<DataAssetDialogueData> DataCreatedAction;

        /// <summary>
        /// If you have multiple instance of the same type of DataAsset, use this override to differentiate them for runtime lookup.
        /// For example, your asset could contain an Enum Category Type, and return that value cast to string.
        /// </summary>
        public override string Variant => "";

#if UNITY_EDITOR
        /// <summary>
        /// Extract all the language texts from the json and add them to the data as a lookup (LanguageCode, text)
        /// </summary>
        protected override void OnCreatedDataFromJson(string json) {
            base.OnCreatedDataFromJson(json);

            List<string> allSupportedLanguageCodes = new List<string>(new []{"en-US"});
            List<Dictionary<string, string>> allRawDataRows = Serializer.Deserialize<List<Dictionary<string, string>>>(json);
            int currentDataIndex = 0;
            foreach (Dictionary<string, string> dataRow in allRawDataRows) {
                Dictionary<string, string> languageTextLookup = new Dictionary<string, string>();
                foreach (string supportedLanguageCode in allSupportedLanguageCodes) {
                    string languageText;
                    if (!dataRow.TryGetValue(supportedLanguageCode, out languageText)) {
                        string errorMessage = "Could not find language: " + supportedLanguageCode + " in json. DataAsset: " + Variant;
                        Debug.LogError(errorMessage);
                        throw new Exception(errorMessage);
                    }
                    languageTextLookup.Add(supportedLanguageCode, languageText);
                }
                
                Data[currentDataIndex].SetLanguageToTextLookup(languageTextLookup, languageTextLookup["en-US"]);
                currentDataIndex++;
            }
        }
#endif
    }
}
