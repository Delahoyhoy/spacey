﻿using System;
using System.Collections.Generic;
using Fumb.Serialization;
using UnityEngine;

namespace Core.Gameplay.Dialogue
{
    [Serializable]
    public class DialogueData {
        
        [SerializeField]
        private string locID;
        
        [SerializeField]
        private string characterID;

        [SerializeField]
        private string defaultValue;
        
        [SerializeField]
        private SerializableDictionary<string, string> languageToTextLookup;

        public string LocId => locID;
        public string CharacterId => characterID;
        public string DefaultValue => defaultValue;
        public SerializableDictionary<string, string> LanguageToTextLookup => languageToTextLookup;

        public void SetLanguageToTextLookup(Dictionary<string, string> lookup, string defaultText)
        {
            defaultValue = defaultText;
            languageToTextLookup = SerializableDictionary<string, string>.FromDictionary(lookup);
        }
    }
}