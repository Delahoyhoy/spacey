﻿using System;
using System.Collections.Generic;
using Fumb.Data;
using UnityEngine;

namespace Core.Gameplay.Dialogue
{
    public class DialogueManager : MonoBehaviour
    {
        private static DialogueManager instance;
        public static DialogueManager Instance => instance ??= ObjectFinder.FindObjectOfType<DialogueManager>();
        
        [SerializeField]
        private DialogueCharacter[] characters;

        [SerializeField]
        private DialogueDisplay dialogueDisplay;
        
        private Dictionary<string, DialogueData> dialogueLookup = new Dictionary<string, DialogueData>();
        private Dictionary<string, DialogueCharacter> characterLookup = new Dictionary<string, DialogueCharacter>();

        private Queue<string> dialogueQueue = new Queue<string>();

        private DialogueBehaviour dialogueBehaviour;
        private Action onCompleteDialogue;

        private bool initialised;

        private void Awake()
        {
            instance = this;
            Initialise();
        }

        private void Initialise()
        {
            if (initialised)
                return;
            
            DialogueData[] data = DataAccessor.GetData<DataAssetDialogueData>().Data;
            for (int i = 0; i < data.Length; i++)
                dialogueLookup.Add(data[i].LocId, data[i]);
            
            for (int i = 0; i < characters.Length; i++)
                characterLookup.Add(characters[i].CharacterId, characters[i]);
            
            initialised = true;
        }

        public bool TryGetEntry(string locId, out string text, out DialogueCharacter character)
        {
            text = "";
            character = characters[0];

            if (!dialogueLookup.TryGetValue(locId, out DialogueData data))
                return false;

            text = data.DefaultValue;
            string charId = data.CharacterId;
            
            characterLookup.TryGetValue(charId, out character);
            return true;
        }

        private void SetCharacterDialogue(string text, DialogueCharacter character)
        {
            // Open the dialogue display
            dialogueDisplay.Display(text, character, dialogueBehaviour, dialogueQueue.Count);
        }

        private void CloseDialogue()
        {
            // Close the dialogue display
            dialogueDisplay.Close();
        }
        
        public void EnqueueDialogue(Action onComplete, DialogueBehaviour behaviour, params string[] dialogueEntries)
        {
            dialogueBehaviour = behaviour;
            onCompleteDialogue = onComplete;
            for (int i = 0; i < dialogueEntries.Length; i++)
                dialogueQueue.Enqueue(dialogueEntries[i]);
            NextDialogue();
        }

        public void NextDialogue()
        {
            if (dialogueQueue.Count <= 0)
            {
                onCompleteDialogue?.Invoke();
                if (dialogueBehaviour.HasFlag(DialogueBehaviour.CloseOnComplete))
                    CloseDialogue();
                return;
            }

            string nextId = dialogueQueue.Dequeue();
            if (TryGetEntry(nextId, out string text, out DialogueCharacter character))
                SetCharacterDialogue(text, character);
            else
            {
                Debug.LogError($"Failed to find dialogue with id: {nextId}");
                NextDialogue();
            }
        }
    }
}