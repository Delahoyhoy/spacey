﻿using System;
using Core.Gameplay.Audio;
using Core.Gameplay.UserInterface;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Dialogue
{
    public class DialogueDisplay : MonoBehaviour
    {
        [SerializeField]
        private BaseTypewriter typewriter;

        [SerializeField]
        private TextMeshProUGUI characterName;

        [SerializeField]
        private Image characterIcon;

        private string currentCharacterId;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private GameObject inputBlocker;

        [SerializeField]
        private GameObject continueButtonHolder;
        
        [SerializeField]
        private Button continueButton;

        [SerializeField]
        private Image fader;

        [SerializeField]
        private Color faderActiveColor = new Color(0, 0, 0, 0.1f), faderInactiveColor = Color.clear;

        private bool faderActive;

        private bool isOpen;
        private static readonly int SwitchChar = Animator.StringToHash("SwitchChars");

        private bool setup;
        
        private void Setup()
        {
            if (setup)
                return;
            //fader.CrossFadeAlpha(0, 0, true);

            void PressContinue()
            {
                Continue();
                AudioManager.Instance.PlayClip("Click", 0.5f);
            }
            
            continueButton.onClick.AddListener(PressContinue);
            setup = true;
        }

        public void Close()
        {
            if (!isOpen)
                return;
            gameObject.SetActive(false);
            isOpen = false;
        }
        
        private void Continue()
        {
            DialogueManager.Instance.NextDialogue();    
        }
        
        public void Display(string text, DialogueCharacter character, 
            DialogueBehaviour behaviour, int remainingEntries)
        {
            Setup();
            if (!isOpen)
            {
                fader.color = faderInactiveColor;
                gameObject.SetActive(true);
            }
            characterIcon.sprite = character.CharacterSprite;
            characterName.text = character.DisplayName;
            if (isOpen && character.CharacterId != currentCharacterId)
                animator.SetTrigger(SwitchChar);

            bool isLastEntry = remainingEntries <= 0;

            bool blockBackground = behaviour.HasFlag(DialogueBehaviour.BlockBackground);
            inputBlocker.SetActive(blockBackground);
            faderActive = blockBackground;
            
            continueButtonHolder.SetActive(!isLastEntry || behaviour.HasFlag(DialogueBehaviour.DoneButton));
            continueButton.interactable = false;
            //fader.CrossFadeAlpha(0, 0, true);

            void CompleteReveal()
            {
                continueButton.interactable = true;
                if (blockBackground && behaviour.HasFlag(DialogueBehaviour.UnblockOnFinish) && isLastEntry)
                {
                    inputBlocker.SetActive(false);
                    faderActive = false;
                }
                if (behaviour.HasFlag(DialogueBehaviour.ContinueOnComplete) && isLastEntry)
                    Continue();
            }
            
            currentCharacterId = character.CharacterId;
            typewriter.PlayTypeWriter(text, null, CompleteReveal);
            isOpen = true;
        }

        private void Update()
        {
            Color targetColor = faderActive ? faderActiveColor : faderInactiveColor;
            fader.color = Color.Lerp(fader.color, targetColor, Time.deltaTime * 4f);
        }
    }
}