using System;

namespace Core.Gameplay.Dialogue
{
    [Serializable, Flags]
    public enum DialogueBehaviour
    {
        None = 0,
        DoneButton = 1 << 1, // Show a continue button once dialogue has finished
        BlockBackground = 1 << 2, // Prevent input on UI whilst dialogue is shown
        UnblockOnFinish = 1 << 3, // Automatically uncover the UI once dialogue has finished
        ContinueOnComplete = 1 << 4, // Automatically run complete action once dialogue has ended 
        CloseOnComplete = 1 << 5, // Close the dialogue display when the current dialogue ends
    }
}
