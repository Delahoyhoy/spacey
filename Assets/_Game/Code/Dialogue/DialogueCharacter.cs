﻿using System;
using UnityEngine;

namespace Core.Gameplay.Dialogue
{
    [Serializable]
    public struct DialogueCharacter
    {
        [SerializeField]
        private string characterId;

        public string CharacterId => characterId;

        [SerializeField]
        private string displayName;

        public string DisplayName => displayName;

        [SerializeField]
        private Sprite characterSprite;

        public Sprite CharacterSprite => characterSprite;
    }
}