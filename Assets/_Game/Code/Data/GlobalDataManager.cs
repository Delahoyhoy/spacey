using System;
using System.Collections;
using System.Collections.Generic;
using Data;
using Fumb.Data;
using UnityEngine;

public class GlobalDataManager : MonoBehaviour
{
    private static GlobalDataManager instance;
    private static bool initialised;

    private GlobalData data;
    
    public static GlobalDataManager Instance
    {
        get
        {
            if (initialised)
                return instance;
            GameObject manager = new GameObject("GlobalDataManager");
            instance = manager.AddComponent<GlobalDataManager>();
            initialised = true;
            return instance;
        }
    }

    public GlobalData Data => data;

    private void Awake()
    {
        instance = this;
        initialised = true;
        DontDestroyOnLoad(gameObject);

        data = DataAccessor.GetData<DataAssetGlobalData>().Data[0];
    }
}
