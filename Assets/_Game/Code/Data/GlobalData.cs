using System;
using UnityEngine;

[Serializable]
public class GlobalData
{
    [SerializeField]
    private double defaultSC;

    public double DefaultGold => defaultSC;

    [SerializeField]
    private double defaultHC;

    public double DefaultDiamonds => defaultHC;

    [SerializeField]
    private int defaultStarFragments;

    public int DefaultStarFragments => defaultStarFragments;

    [SerializeField]
    private double minerBaseMultiplier;

    public double MinerBaseMultiplier => minerBaseMultiplier;
    
    [SerializeField]
    private double minerIncrementMultiplier;

    public double MinerIncrementMultiplier => minerIncrementMultiplier;

    [SerializeField]
    private double additionalMilestoneMultiplier;

    public double AdditionalMilestoneMultiplier => additionalMilestoneMultiplier;
}
