﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
//using Facebook.Unity;
//using Firebase;
//using Firebase.Analytics;

public enum JC_AnalyticsEvent
{
    Purchase,
    NewShipUnlock,
    Reset,
    OpenVault,
    LuckyBox,
    WatchAd,
    CompleteEvent,
}

public interface ITracked
{
    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
    //void OnTrackedEvent(AnalyticsEvent analyticsEvent, ref List<Parameter> parameters);//ref Dictionary<string, object> parameters);

}

public class JC_AnalyticsManager : MonoBehaviour {
    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
    //private FirebaseApp app;

    private bool firebaseReady = false;

    private List<ITracked> trackedObjects = new List<ITracked>();

    private static JC_AnalyticsManager instance;
    public static JC_AnalyticsManager Instance { get { if (!instance) instance = FindObjectOfType<JC_AnalyticsManager>(); return instance; } }

    private bool amIADeveloper = false;

    public void SetMeToBeDeveloper(){
        amIADeveloper = true;
        PlayerPrefs.SetInt("Developer", 1);
        PlayerPrefs.Save();
    }

	// Use this for initialization
	void Start () {

        if(PlayerPrefs.GetInt("Developer", 0) == 1){
            amIADeveloper = true;
        }

        trackedObjects = ObjectFinder.FindAllObjectsOfType<ITracked>();
        try
        {
            //TODO: Oscar big rework
            //FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            //{
            //    var dependencyStatus = task.Result;
            //    if (dependencyStatus == DependencyStatus.Available)
            //    {
            //        // Create and hold a reference to your FirebaseApp,
            //        // where app is a Firebase.FirebaseApp property of your application class.
            //        //   app = Firebase.FirebaseApp.DefaultInstance;
            //        app = FirebaseApp.DefaultInstance;
            //        // Set a flag here to indicate whether Firebase is ready to use by your app.
            //        firebaseReady = true;
            //    }
            //    else
            //    {
            //        Debug.LogError(System.String.Format(
            //          "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            //        // Firebase Unity SDK is not safe to use here.
            //    }
            //});
        }
        catch
        {
            Debug.Log("Failed to load Firebase");
        }
    }

    //public void TriggerEvent(AnalyticsEvent analyticsEvent)
    //{
    //    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
    //    //TriggerEvent(analyticsEvent, new List<Parameter>());//Dictionary<string, object>());
    //}

    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
    public void TriggerEvent(JC_AnalyticsEvent analyticsEvent)//, List<Parameter> parameters)
    {
        try
        {
            if (!firebaseReady || IAPManager.Instance.IsThisLikelyACheater() || amIADeveloper)
                return;
            //Dictionary<string, object> parameters = param;
            for (int i = 0; i < trackedObjects.Count; i++)
            {//TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
             //
               // trackedObjects[i].OnTrackedEvent(analyticsEvent, ref parameters);
            }
            //if (parameters.Count > 0)
            //{
            //    //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            //    //
            //    //FirebaseAnalytics.LogEvent(analyticsEvent.ToString(), parameters.ToArray());
                
            //}
        }
        catch
        {
            Debug.Log("Failed to send analytics data");
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
