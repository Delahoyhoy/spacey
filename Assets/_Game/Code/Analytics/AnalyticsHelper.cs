using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fumb.Analytics;

public static class AnalyticsHelper
{
    public static void CurrencySpend(CurrencyType currency, double value, bool currencyValueCanBeVeryLarge,
        params string[] vcCategories)
    {
        //CurrencySpend(GetCurrencyName(currency), GetCurrencyId(currency), value, currencyValueCanBeVeryLarge, vcCategories);
        CurrencyController.Instance.CurrencyEvent(currency, -value, vcCategories);
    }

    public static void CurrencyEarn(CurrencyType currency, double value, bool currencyValueCanBeVeryLarge,
        params string[] vcCategories)
    {
        //CurrencyEarn(GetCurrencyName(currency), GetCurrencyId(currency), value, currencyValueCanBeVeryLarge, vcCategories);
        CurrencyController.Instance.CurrencyEvent(currency, value, vcCategories);
    }

    public static void SpendPrestige(double value, bool currencyValueCanBeVeryLarge, params string[] vcCategories)
    {
        AnalyticsEventCurrencySpend eventCurrencySpend = new AnalyticsEventCurrencySpend("2", "GPUs", value, currencyValueCanBeVeryLarge, vcCategories);
        AnalyticsManager.SendAnalyticEvent(eventCurrencySpend);
    }

    private static void CurrencyEarn(string currencyName, int currencyId, double value,
        bool currencyValueCanBeVeryLarge, params string[] vcCategories)
    {
        AnalyticsEventCurrencyEarn eventCurrencyEarn = new AnalyticsEventCurrencyEarn(currencyId.ToString(), currencyName, value, currencyValueCanBeVeryLarge, vcCategories);
        AnalyticsManager.SendAnalyticEvent(eventCurrencyEarn);
    }
    
    private static void CurrencySpend(string currencyName, int currencyId, double value,
        bool currencyValueCanBeVeryLarge, params string[] vcCategories)
    {
        AnalyticsEventCurrencySpend eventCurrencySpend = new AnalyticsEventCurrencySpend(currencyId.ToString(), currencyName, value, currencyValueCanBeVeryLarge, vcCategories);
        AnalyticsManager.SendAnalyticEvent(eventCurrencySpend);
    }

    // Event currency handling
    private static int GetCurrencyId(CurrencyType currency)
    {
        return currency switch
        {
            CurrencyType.SoftCurrency => 0,
            CurrencyType.HardCurrency => 1,
            CurrencyType.Satoshis => 3,
            _ => 0,
        };
    }

    private static string GetCurrencyName(CurrencyType currency)
    {
        return currency switch
        {
            CurrencyType.SoftCurrency => "Gold",
            CurrencyType.HardCurrency => "Gems",
            CurrencyType.Satoshis => "Sats",
            _ => "undefined"
        };
    }
}
