﻿namespace Core.Gameplay.Tutorial
{
    public class FTUECountedValueListener : FTUEValueListener
    {
        public void SetValues(int current, int max)
        {
            UpdateValue(current, max);
        }
    }
}