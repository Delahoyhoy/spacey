﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Core.Gameplay.Tutorial
{
    public class FTUEFixedTargetValueListener : FTUEValueListener
    {
        [SerializeField]
        private double fixedTargetValue;

        protected override void OnInitialised()
        {
            SetValue(0);
        }

        protected void SetValue(double value)
        {
            UpdateValue(value, fixedTargetValue);
        }

        public void Increment(double by)
        {
            SetValue(currentValue + by);
        }
    }
}