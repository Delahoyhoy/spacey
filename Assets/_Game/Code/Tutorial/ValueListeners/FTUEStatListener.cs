﻿using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStatListener : FTUEFixedTargetValueListener
    {
        [SerializeField]
        private string statId;

        protected override void OnInitialised()
        {
            StatTracker.Instance.onUpdateStat += UpdateStatValue; 
            SetValue(StatTracker.Instance.GetStat(statId));
        }

        private void UpdateStatValue(string id, double value)
        {
            if (!id.Equals(statId))
                return;
            SetValue(value);
        }
    }
}