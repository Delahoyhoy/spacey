﻿using System;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEValueListener : MonoBehaviour
    {
        [SerializeField]
        private string displayFormat;

        public string DisplayFormat => displayFormat;
        
        private Action<bool> onCompleteAction;
        private Action<double, double> onUpdateProgress;

        private bool hasReceivedValue;

        protected double currentValue, targetValue;

        public void Initialise(Action<bool> onComplete, Action<double, double> onUpdate)
        {
            onCompleteAction = onComplete;
            onUpdateProgress = onUpdate;
            OnInitialised();
        }

        public void DisableListener()
        {
            onCompleteAction = null;
            onUpdateProgress = null;
        }

        protected virtual void OnInitialised()
        {
            if (hasReceivedValue && currentValue >= targetValue)
                onCompleteAction?.Invoke(true);
        }

        protected void UpdateValue(double current, double target)
        {
            hasReceivedValue = true;
            currentValue = current;
            targetValue = target;
            onUpdateProgress?.Invoke(currentValue, targetValue);
            if (currentValue >= targetValue)
                onCompleteAction?.Invoke(true);
        }
    }
}