﻿namespace Core.Gameplay.Tutorial
{
    public class FTUEEventTrackedListener : FTUEValueListener
    {
        public void UpdateProgress(double current, double total)
        {
            UpdateValue(current, total);
        }
    }
}