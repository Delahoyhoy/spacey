﻿using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUECurrencyListener : FTUEFixedTargetValueListener
    {
        [SerializeField]
        private CurrencyType currencyType;

        protected override void OnInitialised()
        {
            CurrencyController.Instance.onCurrencyUpdate += UpdateCurrency;
            SetValue(CurrencyController.Instance.GetCurrency(currencyType));
        }

        private void UpdateCurrency(CurrencyType currency, double value)
        {
            if (!currency.Equals(currencyType))
                return;
            SetValue(value);
        }
    }
}