using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Mothership;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEMothershipDepartmentCostListener : FTUEValueListener
    {
        [SerializeField]
        private MothershipDepartment targetDepartment;

        private double CurrentTarget => MothershipManager.Instance.GetDepartmentLevelUpCost(targetDepartment);
        
        protected override void OnInitialised()
        {
            CurrencyController.Instance.onCurrencyUpdate += UpdateCurrency;
            SetValue(CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency));
        }

        private void UpdateCurrency(CurrencyType currency, double value)
        {
            if (!currency.Equals(CurrencyType.SoftCurrency))
                return;
            SetValue(value);
        }

        private void SetValue(double value)
        {
            UpdateValue(value, CurrentTarget);
        }
    }
}
