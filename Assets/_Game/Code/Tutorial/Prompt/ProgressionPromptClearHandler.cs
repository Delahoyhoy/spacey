﻿using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class ProgressionPromptClearHandler : MonoBehaviour
    {
        public void ClearPrompt()
        {
            ProgressionPrompter.Instance.ClearPrompt();
        }
    }
}