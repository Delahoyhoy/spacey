﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Tutorial
{
    public class ProgressionPromptStateHandler : MonoBehaviour
    {
        [SerializeField]
        private ProgressionPromptState targetState;

        [SerializeField]
        private UnityEvent<bool> onStateMatch;

        private bool initialised;

        private void Start()
        {
            if (initialised)
                return;
            OnSetState(ProgressionPrompter.Instance.CurrentState);
            ProgressionPrompter.Instance.onStateChange += OnSetState;
            initialised = true;
        }
        
        private void OnSetState(ProgressionPromptState state)
        {
            onStateMatch?.Invoke(state == targetState);
        }
    }
}