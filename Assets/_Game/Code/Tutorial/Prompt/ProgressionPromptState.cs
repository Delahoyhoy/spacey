﻿namespace Core.Gameplay.Tutorial
{
    public enum ProgressionPromptState
    {
        NoPrompt,
        PromptScan,
        PromptJump,
        AwaitingScan,
        AwaitingJump
    }
}