using System;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Outposts;
using Core.Gameplay.Sectors;
using Core.Gameplay.Version;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class ProgressionPrompter : MonoBehaviour
    {
        private static ProgressionPrompter instance;
        public static ProgressionPrompter Instance => instance ??= ObjectFinder.FindObjectOfType<ProgressionPrompter>();
        
        [SerializeField]
        private AnchoredUI asteroidPointer;

        [SerializeField]
        private GameObject jumpButtonPrompt;

        [SerializeField]
        private float scanPromptDelay = 30f, jumpPromptDelay = 15f;

        private float ScanPromptDelay => scanPromptDelay * (1f + ClusterManager.Instance.ClusterIndex * 0.25f);
        private float JumpPromptDelay => jumpPromptDelay * (1f + ClusterManager.Instance.ClusterIndex * 0.25f);

        private float nextPromptTime;
        private ProgressionPromptState currentState;

        public Action<ProgressionPromptState> onStateChange;

        public ProgressionPromptState CurrentState
        {
            get => currentState;
            private set
            {
                currentState = GameVersionManager.CurrentVersion < GameVersion.Version3 || 
                               FTUEManager.Instance.AnyModuleRunning ? ProgressionPromptState.NoPrompt : value;
                asteroidPointer.gameObject.SetActive(currentState == ProgressionPromptState.PromptScan);
                if (currentState == ProgressionPromptState.PromptScan)
                {
                    if (GameManager.Instance.TryGetFirstUnscannedAsteroid(out Asteroid target))
                        asteroidPointer.Target = target.transform;
                }
                
                jumpButtonPrompt.gameObject.SetActive(currentState == ProgressionPromptState.PromptJump);
                onStateChange?.Invoke(currentState);
            } 
        }

        private bool AwaitingAnyPrompt => currentState == ProgressionPromptState.AwaitingJump ||
                                          currentState == ProgressionPromptState.AwaitingScan;

        private void Awake()
        {
            instance = this;
        }

        private ProgressionPromptState GetTargetState()
        {
            if (currentState != ProgressionPromptState.NoPrompt) 
                return currentState;
            if (SectorManager.Instance.SectorCompleted)
            {
                nextPromptTime = Time.time + JumpPromptDelay;
                return ProgressionPromptState.AwaitingJump;
            }

            if (GameManager.Instance.Scanning || GameManager.Instance.GetTotalUnscannedAsteroids() <= 0)
                return ProgressionPromptState.NoPrompt;
            nextPromptTime = Time.time + ScanPromptDelay;
            return ProgressionPromptState.AwaitingScan;

        }

        private void CheckSectorCompletion(int asteroidCompleted, int totalAsteroids)
        {
            bool completed = asteroidCompleted >= totalAsteroids;
            switch (currentState)
            {
                case ProgressionPromptState.AwaitingScan:
                case ProgressionPromptState.PromptScan:
                    return;
                case ProgressionPromptState.AwaitingJump:
                case ProgressionPromptState.PromptJump:
                    if (!completed)
                        CurrentState = ProgressionPromptState.NoPrompt;
                    return;
                case ProgressionPromptState.NoPrompt:
                    CurrentState = GetTargetState();
                    return;
            }
        }

        private void Start()
        {
            Asteroid.OnAnyStateChange += CheckAsteroidStateChange;
            AsteroidManager.Instance.onUpdateCompletion += CheckSectorCompletion;
            CurrentState = GetTargetState();
        }

        private void OnDestroy()
        {
            Asteroid.OnAnyStateChange -= CheckAsteroidStateChange;
        }

        private void CheckAsteroidStateChange(AsteroidState state)
        {
            switch (currentState)
            {
                case ProgressionPromptState.AwaitingJump:
                case ProgressionPromptState.PromptJump:
                    return;
                case ProgressionPromptState.AwaitingScan:
                case ProgressionPromptState.PromptScan:
                    if (!OutpostManager.Instance.AnyOutpostsAvailable ||
                        GameManager.Instance.GetTotalUnscannedAsteroids() <= 0)
                        CurrentState = ProgressionPromptState.NoPrompt;
                    return;
                case ProgressionPromptState.NoPrompt:
                    CurrentState = GetTargetState();
                    return;
            }
        }

        public void ClearPrompt()
        {
            currentState = ProgressionPromptState.NoPrompt;
            CurrentState = GetTargetState();
        }

        private void Update()
        {
            if (!AwaitingAnyPrompt)
                return;
            if (Time.time < nextPromptTime)
                return;
            if (CurrentState == ProgressionPromptState.AwaitingJump)
                CurrentState = ProgressionPromptState.PromptJump;
            else if (CurrentState == ProgressionPromptState.AwaitingScan)
                CurrentState = ProgressionPromptState.PromptScan;
        }
    }
}
