﻿using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStepCompleteTrigger : MonoBehaviour
    {
        [SerializeField]
        private FTUEModuleId moduleId;
        
        public void TriggerStep(string stepId)
        {
            FTUEManager.Instance.CompleteStep(moduleId, stepId);
        }
    }
}