﻿namespace Core.Gameplay.Tutorial
{
    public enum FTUEModuleId
    {
        Core,
        Augments,
        Prestige,
    }
}