﻿using System;
using System.Collections.Generic;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Tutorial
{
    public abstract class FTUEStepHandler : MonoBehaviour
    {
        [SerializeField]
        private FTUEModuleId module;
        
        [SerializeField]
        private List<string> stepIds = new List<string>();

        protected abstract bool HandleStepBegin { get; }

        protected abstract bool HandleStepComplete { get; }

        private bool initialised;

        private void Start()
        {
            if (initialised)
                return;
            if (!FTUEManager.Instance.TryGetModule(module, out FTUEModule ftueModule))
                return;
            if (HandleStepBegin)
            {
                if (ftueModule.TutorialRunning)
                    BeginStep(ftueModule.CurrentStepId);
                ftueModule.onBeginStep += BeginStep;
            }
            if (HandleStepComplete)
                ftueModule.onCompleteStep += CompleteStep;
            initialised = true;
        }

        private void BeginStep(string step)
        {
            if (!stepIds.Contains(step))
                return;
            Begin();
        }

        protected abstract void Begin();
        
        private void CompleteStep(string step)
        {
            if (!stepIds.Contains(step))
                return;
            Complete();
        }

        protected abstract void Complete();
    }
}