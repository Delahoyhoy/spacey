﻿using Core.Gameplay.CameraControl;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStepCameraFocus : FTUEStepHandler
    {
        [SerializeField]
        private Transform target;

        [SerializeField]
        private float zoomLevel = 8f;

        protected override bool HandleStepBegin => true;
        protected override bool HandleStepComplete => true;
        
        protected override void Begin()
        {
            BasicCameraController.Instance.LockOnTarget(target, zoomLevel);
        }

        protected override void Complete()
        {
            BasicCameraController.Instance.UnlockTarget();
        }
    }
}