﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Tutorial
{
    public abstract class FTUEStateDependentStepHandler : FTUEStepHandler
    {
        [SerializeField]
        private UnityEvent<bool> onSetStateValid;

        [SerializeField]
        private UnityEvent onComplete;
        
        protected override bool HandleStepBegin => true;
        protected override bool HandleStepComplete => true;

        protected abstract bool IsValid { get; }
        
        protected override void Begin()
        {
            onSetStateValid?.Invoke(IsValid);
        }

        protected override void Complete()
        {
            onComplete?.Invoke();
        }
    }
}