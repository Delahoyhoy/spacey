﻿using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStepObject : FTUEStepHandler
    {
        [SerializeField]
        private GameObject targetObject;

        protected override bool HandleStepBegin => true;
        protected override bool HandleStepComplete => true;

        protected override void Begin()
        {
            targetObject.SetActive(true);
        }

        protected override void Complete()
        {
            targetObject.SetActive(false);
        }
    }
}