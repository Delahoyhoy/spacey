﻿using Core.Gameplay.Sectors;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUESectorMarkerDependentStepHandler : FTUEStateDependentStepHandler
    {
        [SerializeField]
        private SectorMarker marker;

        [SerializeField]
        private string targetMarkerId;

        protected override bool IsValid => targetMarkerId.Equals(marker.MarkerId);
    }
}