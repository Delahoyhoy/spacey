﻿using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStepEvent : FTUEStepHandler
    {
        [SerializeField]
        protected bool handleStepBegin;

        protected override bool HandleStepBegin => handleStepBegin;

        [SerializeField] [ConditionalField("handleStepBegin")]
        private UnityEvent onStepBegin;
        
        [SerializeField]
        protected bool handleStepComplete;

        protected override bool HandleStepComplete => handleStepComplete;

        [SerializeField] [ConditionalField("handleStepComplete")]
        private UnityEvent onStepComplete;

        protected override void Begin()
        {
            onStepBegin?.Invoke();
        }

        protected override void Complete()
        {
            onStepComplete?.Invoke();
        }
    }
}