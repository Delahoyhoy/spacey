﻿using Core.Gameplay.CameraControl;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEAsteroidStateDependentCamera : FTUEAsteroidStateDependentStepHandler
    {
        [SerializeField]
        private Transform target;

        [SerializeField]
        private float zoomLevel = 8f;
        
        protected override void Begin()
        {
            if (IsValid)
                BasicCameraController.Instance.LockOnTarget(target, zoomLevel);
            base.Begin();
        }

        protected override void Complete()
        {
            BasicCameraController.Instance.UnlockTarget();
            base.Complete();
        }
    }
}