﻿using Core.Gameplay.Research;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEResearchSlotStateDependentHandler : FTUEStateDependentStepHandler
    {
        [SerializeField]
        private ResearchSlot targetSlot;

        [SerializeField]
        private ResearchType requiredType;

        protected override bool IsValid => targetSlot.ResearchType.Equals(requiredType);
    }
}