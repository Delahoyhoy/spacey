﻿using Core.Gameplay.Asteroids;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Tutorial
{
    public class FTUEAsteroidStateDependentStepHandler : FTUEStateDependentStepHandler
    {
        [SerializeField]
        private Asteroid targetAsteroid;

        [SerializeField]
        private bool checkAsteroidIndex;

        [SerializeField] [ConditionalField("checkAsteroidIndex")]
        private int targetIndex;

        [SerializeField]
        private bool checkAsteroidState;

        [SerializeField] [ConditionalField("checkAsteroidState")]
        private AsteroidState targetState;
        
        protected override bool HandleStepBegin => true;
        protected override bool HandleStepComplete => true;

        protected override bool IsValid => targetAsteroid && (!checkAsteroidIndex || targetAsteroid.Index == targetIndex) &&
                                     (!checkAsteroidState || targetAsteroid.State == targetState);
    }
}