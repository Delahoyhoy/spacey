﻿using Fumb.Attribute;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStepUIScreenLocker : FTUEStepHandler
    {
        [SerializeField]
        private UIScreen targetScreen;

        [SerializeField]
        private bool autoCloseOnComplete;

        [SerializeField] [ConditionalField("autoCloseOnComplete")]
        private bool closeInstant;
        
        protected override bool HandleStepBegin => true;
        protected override bool HandleStepComplete => true;

        protected override void Begin()
        {
            targetScreen.SetForcedOpen(true);
        }

        protected override void Complete()
        {
            targetScreen.SetForcedOpen(false);
            
            if (!autoCloseOnComplete)
                return;
            
            if (closeInstant)
                targetScreen.CloseInstant();
            else
                targetScreen.Close();
        }
    }
}