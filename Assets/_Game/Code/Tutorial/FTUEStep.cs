﻿using System;
using Fumb.Attribute;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStep : MonoBehaviour
    {
        [SerializeField]
        private string stepId;

        public string StepId => stepId;
        
        [SerializeField]
        private FTUEFeature activeFeatures;

        public FTUEFeature ActiveFeatures => activeFeatures;

        [SerializeField]
        private bool isSafeLoadState;

        public bool IsSafeState => isSafeLoadState;
        
        [SerializeField] [ConditionalField("isSafeLoadState", true)]
        private SafeState safeStateLoadType;

        public SafeState SafeStateLoadType => safeStateLoadType;

        [SerializeField]
        private bool isTracked;

        public bool IsTracked => isTracked;

        private Action<bool> onComplete;
        private bool completed;

        public virtual void Begin(Action begin, Action<bool> completeAction)
        {
            onComplete = completeAction;
            if (TryGetUIBehaviour(out FTUEUIBehaviourHandler behaviourHandler))
                behaviourHandler.HandleBehaviour(begin);
            else
                begin?.Invoke();
        }

        public bool GetIsTrackedValueStep(Action<double, double> updateProgressAction, out string displayFormat)
        {
            if (TryGetValueComponent(out FTUEValueListener valueListener))
            {
                valueListener.Initialise(Complete, updateProgressAction);
                displayFormat = valueListener.DisplayFormat;
                return true;
            }

            displayFormat = "";
            return false;
        }

        public void Complete(bool completedFromTracking)
        {
            if (completed)
                return;
            onComplete?.Invoke(completedFromTracking);
            onComplete = null;
            completed = true;
        }

        public bool TryGetValueComponent(out FTUEValueListener valueListener)
        {
            valueListener = GetComponent<FTUEValueListener>();
            return valueListener;
        }

        private bool TryGetUIBehaviour(out FTUEUIBehaviourHandler behaviourHandler)
        {
            behaviourHandler = GetComponent<FTUEUIBehaviourHandler>();
            return behaviourHandler;
        }
    }
}