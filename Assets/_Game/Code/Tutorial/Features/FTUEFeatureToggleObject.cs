﻿using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEFeatureToggleObject : FTUEFeatureHandler
    {
        [SerializeField]
        private GameObject activeObject, inactiveObject;

        protected override void SetActive(bool active)
        {
            activeObject.SetActive(active);
            inactiveObject.SetActive(!active);
        }
    }
}