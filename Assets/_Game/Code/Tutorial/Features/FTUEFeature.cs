﻿using System;

namespace Core.Gameplay.Tutorial
{
    [Serializable, Flags]
    public enum FTUEFeature
    {
        None = 0,
        CurrencyBar = 1 << 1,
        SectorInfo = 1 << 2,
        JumpButton = 1 << 3,
        StarFragments = 1 << 4,
        BottomBar = 1 << 5,
        Mothership = 1 << 6,
        Research = 1 << 7,
        Map = 1 << 8,
        RemainingUi = 1 << 9
    }
}