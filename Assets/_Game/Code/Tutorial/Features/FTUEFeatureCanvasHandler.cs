﻿using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEFeatureCanvasHandler : FTUEFeatureHandler
    {
        [SerializeField]
        private Canvas targetCanvas;

        protected override void SetActive(bool active)
        {
            targetCanvas.enabled = active;
        }
    }
}