﻿using System;
using Core.Gameplay.Utilities;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEFeatureHandler : MonoBehaviour
    {
        [SerializeField] [EnumSingleFlag(typeof(FTUEFeature))]
        private FTUEFeature feature;

        private bool initialised = false;

        private void Start()
        {
            if (initialised)
                return;
            SetActiveFeatures(FTUEManager.Instance.CurrentActiveFeatures);
            FTUEManager.Instance.onSetActiveFeatures += SetActiveFeatures;
            initialised = true;
        }

        public void SetActiveFeatures(FTUEFeature activeFeatures)
        {
            SetActive(activeFeatures.HasFlag(feature));
        }

        protected virtual void SetActive(bool active) { }
    }
}