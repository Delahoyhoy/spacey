﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Tutorial
{
    public class FTUEFeatureEventHandler : FTUEFeatureHandler
    {
        [SerializeField]
        private UnityEvent<bool> onSetActive;

        protected override void SetActive(bool active)
        {
            onSetActive?.Invoke(active);
        }
    }
}