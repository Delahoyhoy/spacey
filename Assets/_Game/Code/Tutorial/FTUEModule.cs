﻿using System;
using System.Collections.Generic;
using Fumb.Save;
using UnityEngine;
using Core.Gameplay.Utilities;
using Fumb.Analytics;

namespace Core.Gameplay.Tutorial
{
    public class FTUEModule : MonoBehaviour, ISaveValueVariant
    {
        [SerializeField]
        private FTUEModuleId moduleId;

        public FTUEModuleId ModuleId => moduleId;

        public string SaveValueVariant => moduleId.ToString();

        [ManagedSaveValue("FTUEModuleStarted")]
        private SaveValueBool saveStarted;

        public bool Started
        {
            get => saveStarted.Value;
            private set
            {
                saveStarted.Value = value;
                onStateChange?.Invoke(State);
            }
        }
        
        [ManagedSaveValue("FTUEModuleCompleted")]
        private SaveValueBool saveCompleted;

        public bool Completed
        {
            get => saveCompleted.Value;
            private set
            {
                if (saveCompleted.Value != value)
                {
                    saveCompleted.Value = value;
                    if (value)
                        onFinalComplete?.Invoke();
                }
                onSetActiveFeatures?.Invoke(FlagHelper.AllFlags<FTUEFeature>());
                onStateChange?.Invoke(State);
            }
        }

        [ManagedSaveValue("FTUEModuleCurrentStep")]
        private SaveValueInt saveCurrentStep;

        private int CurrentStep {
            get => saveCurrentStep.Value;
            set {
                saveCurrentStep.Value = value;
                if (value >= steps.Length)
                {
                    Completed = true;
                    return;
                }
                SetupForCurrentStep();
            }
        }

        public string CurrentStepId => steps[CurrentStep].StepId;

        public bool TutorialRunning => initialised && Started && !Completed;

        public FTUEState State
        {
            get
            {
                if (!Started)
                    return FTUEState.NotStarted;
                return Completed ? FTUEState.Completed : FTUEState.Running;
            }
        }

        // Tracked value progress updates 
        public Action<bool, string> onSetTrackedValue;
        public Action<double, double> onUpdateProgress;

        // Begin and complete events
        public Action<string> onBeginStep, onCompleteStep;
        public Action<bool, Action> onCompleteStepWithTrackedValue;
        
        public Action<FTUEFeature> onSetActiveFeatures;
        public Action<FTUEState> onStateChange;

        public Action onFinalComplete;

        private FTUEStep[] steps;

        private bool initialised;

        public void Initialise()
        {
            if (initialised)
                return;
            steps = GetComponentsInChildren<FTUEStep>(false);
            initialised = true;

            #region Debug list all tutorial steps to clipboard

            // DEBUG - List all tutorial steps
            // string toCopy = "";
            // for (int i = 0; i < steps.Length; i++)
            // {
            //     if (i > 0)
            //         toCopy += "\n";
            //     toCopy += steps[i].StepId;
            // }
            // TextEditor te = new TextEditor();
            // te.text = toCopy;
            // te.SelectAll();
            // te.Copy();

            #endregion

            #region Debug list tracked tutorial steps to clipboard

            // DEBUG - List all tracked tutorial steps
            // string toCopy = "";
            // int index = 0;
            // for (int i = 0; i < steps.Length; i++)
            // {
            //     if (!steps[i].IsTracked)
            //         continue;
            //     
            //     if (index > 0)
            //         toCopy += "\n";
            //     toCopy += steps[i].StepId;
            //     index++;
            // }
            // TextEditor te = new TextEditor();
            // te.text = toCopy;
            // te.SelectAll();
            // te.Copy();

            #endregion
        }

        private void SetupForCurrentStep()
        {
            FTUEStep step = steps[CurrentStep];

            Debug.Log($"Beginning step: {step.StepId}");
            
            void Begin()
            {
                bool valueTracked = step.GetIsTrackedValueStep(onUpdateProgress, out string displayFormat);
                
                onSetTrackedValue?.Invoke(valueTracked, displayFormat);
                onBeginStep?.Invoke(step.StepId);
                onSetActiveFeatures?.Invoke(step.ActiveFeatures);
                if (!step.IsTracked) 
                    return;
                AnalyticsEventTutorialStep stepEvent =
                    new AnalyticsEventTutorialStep(moduleId.ToString(), step.StepId, "", "begin");
                AnalyticsManager.SendAnalyticEvent(stepEvent);
            }
            
            step.Begin(Begin, CompleteCurrentStep);
        }

        private void CompleteCurrentStep(bool fromTracking)
        {
            FTUEStep step = steps[CurrentStep];
            void Complete()
            {
                onCompleteStep?.Invoke(step.StepId);
                
                if (step.IsTracked)
                {
                    AnalyticsEventTutorialStep stepEvent =
                        new AnalyticsEventTutorialStep(moduleId.ToString(), step.StepId, "", "complete");
                    AnalyticsManager.SendAnalyticEvent(stepEvent);
                }
                
                CurrentStep++;
            }

            bool isTracked = step.TryGetValueComponent(out FTUEValueListener listener);
            bool triggeredFromTracking = fromTracking && isTracked;
            onCompleteStepWithTrackedValue?.Invoke(triggeredFromTracking, Complete);
            if (isTracked)
                listener.DisableListener();
        }

        public void CompleteStep(string stepId)
        {
            Initialise();
            if (!TutorialRunning)
                return;
            FTUEStep step = steps[CurrentStep];
            if (!step.StepId.Equals(stepId))
                return;
            CompleteCurrentStep(false);
        }

        private int GetValidLoadState(int index)
        {
            List<int> allSafeStates = GetSafeStates();
            if (allSafeStates.Contains(index))
                return index;
            if (index < steps.Length && index > 0)
            {
                SafeState safety = steps[index].SafeStateLoadType;
                for (int i = 0; i < allSafeStates.Count; i++)
                {
                    if (safety == SafeState.Next && allSafeStates[i] >= index)
                        return allSafeStates[i];
                    if (safety == SafeState.Previous && allSafeStates[i] >= index && i > 0)
                        return allSafeStates[i - 1];
                }
            }
            return allSafeStates[allSafeStates.Count - 1];
        }
        
        private List<int> GetSafeStates()
        {
            List<int> safeStates = new List<int>();
            for (int i = 0; i < steps.Length; i++)
            {
                if (steps[i].IsSafeState)
                    safeStates.Add(i);
            }
            return safeStates;
        }

        public void StartTutorial()
        {
            Initialise();
            if (Started || Completed)
                return;
            Started = true;
            CurrentStep = 0;
        }

        public void ForceStart()
        {
            Completed = false;
            Started = true;
            CurrentStep = 2;
            onStateChange?.Invoke(State);
        }

        public void LoadTutorial(bool autoStart = false, bool markCompletedIfNotStarted = false, bool forceCompleted = false)
        {
            Initialise();
            if (forceCompleted)
            {
                Started = true;
                Completed = true;
                return;
            }
            if (!Started)
            {
                if (autoStart)
                    StartTutorial();
                else if (markCompletedIfNotStarted)
                {
                    Started = true;
                    Completed = true;
                }
            } 
            else if (!Completed)
                CurrentStep = GetValidLoadState(CurrentStep);
            onStateChange?.Invoke(State);
        }
    }
}