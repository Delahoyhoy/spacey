﻿namespace Core.Gameplay.Tutorial
{
    public enum FTUEState
    {
        NotStarted,
        Running,
        Completed,
    }
}