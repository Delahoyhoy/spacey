﻿using System;
using Fumb.General;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Tutorial
{
    public class FTUEValueProgressDisplay : MonoBehaviour
    {
        [SerializeField]
        private Text descriptionText, progressText;

        [SerializeField]
        private UnityEvent<float> onSetProgress;

        private double currentValue, targetValue;

        [SerializeField]
        private Animator progressDisplayAnim;

        [SerializeField]
        private string completionTrigger;
        
        [SerializeField]
        private float completeDelay;

        public void Setup(bool active, string descriptionFormat)
        {
            if (!active)
            {
                gameObject.SetActive(false);
                return;
            }
            gameObject.SetActive(true);
            descriptionText.text = string.Format(descriptionFormat, targetValue.FormatAsCurrency());
        }

        public void CompleteStep(Action onComplete)
        {
            void Complete()
            {
                onComplete?.Invoke();
            }
            progressDisplayAnim.SetTrigger(completionTrigger);
            SetTimer.StartTimer(completeDelay, Complete);
        }

        public void UpdateValue(double current, double target)
        {
            currentValue = current;
            targetValue = target;
            progressText.text = 
                $"{Math.Min(currentValue, targetValue).FormatAsCurrency()}/{targetValue.FormatAsCurrency()}";
            onSetProgress?.Invoke(Mathf.Clamp01((float)(currentValue / targetValue)));
        }
    }
}