﻿using System;
using Core.Gameplay.Dialogue;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEDialogueStep : FTUEStep
    {
        [SerializeField]
        private string[] dialogueSteps;

        [SerializeField]
        private DialogueBehaviour behaviour;

        public override void Begin(Action begin, Action<bool> completeAction)
        {
            void CompleteDialogue()
            {
                Debug.Log("Dialogue complete");
                //if (behaviour.HasFlag(DialogueBehaviour.ContinueOnComplete))
                completeAction?.Invoke(false);
            }
            
            DialogueManager.Instance.EnqueueDialogue(CompleteDialogue, behaviour, dialogueSteps);
            base.Begin(begin, completeAction);
        }
    }
}