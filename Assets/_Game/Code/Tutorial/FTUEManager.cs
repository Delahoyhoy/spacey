﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Save;
using Core.Gameplay.Utilities;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEManager : MonoBehaviour, ISaveLoad
    {
        private static FTUEManager instance;
        public static FTUEManager Instance => instance ??= ObjectFinder.FindObjectOfType<FTUEManager>();

        private Dictionary<FTUEModuleId, FTUEModule> moduleLookup = new Dictionary<FTUEModuleId, FTUEModule>();

        [SerializeField]
        private FTUEValueProgressDisplay valueProgressDisplay;

        private FTUEFeature currentActiveFeatures = FlagHelper.AllFlags<FTUEFeature>();
        public FTUEFeature CurrentActiveFeatures => currentActiveFeatures;

        public bool AnyModuleRunning
        {
            get
            {
                foreach (FTUEModule module in moduleLookup.Values)
                {
                    if (module.TutorialRunning)
                        return true;
                }
                return false;
            }
        }
        
        public Action<FTUEFeature> onSetActiveFeatures;

        private void Awake()
        {
            instance = this;
            
            // Get and initialise all modules
            FTUEModule[] modules = GetComponentsInChildren<FTUEModule>();
            for (int i = 0; i < modules.Length; i++)
            {
                FTUEModule module = modules[i];
                FTUEModuleId moduleId = module.ModuleId;
                if (!moduleLookup.ContainsKey(moduleId))
                    moduleLookup.Add(moduleId, module);
                
                // Subscribe all global events
                module.onCompleteStepWithTrackedValue += HandleStepCompleted;
                module.onSetTrackedValue += valueProgressDisplay.Setup;
                module.onUpdateProgress += valueProgressDisplay.UpdateValue;

                module.onSetActiveFeatures += UpdateActiveFeatures;
            }
        }

        public FTUEState GetState(FTUEModuleId moduleId)
        {
            if (moduleLookup.TryGetValue(moduleId, out FTUEModule module))
                return module.State;
            return FTUEState.NotStarted;
        }

        public bool GetIsModuleCompleted(FTUEModuleId moduleId)
        {
            if (moduleLookup.TryGetValue(moduleId, out FTUEModule module))
                return module.Completed;
            return false;
        }

        private void UpdateActiveFeatures(FTUEFeature activeFeatures)
        {
            currentActiveFeatures = activeFeatures;
            onSetActiveFeatures?.Invoke(activeFeatures);
        }

        private void HandleStepCompleted(bool isTracked, Action onComplete)
        {
            if (!isTracked)
            {
                onComplete?.Invoke();
                return;
            }
            valueProgressDisplay.CompleteStep(onComplete);
        }

        public bool TryGetModule(FTUEModuleId moduleId, out FTUEModule module)
        {
            return moduleLookup.TryGetValue(moduleId, out module);
        }

        public void CompleteStep(FTUEModuleId moduleId, string stepId)
        {
            if (TryGetModule(moduleId, out FTUEModule module))
                module.CompleteStep(stepId);
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            foreach (FTUEModule module in moduleLookup.Values)
            {
                module.LoadTutorial(false, module.ModuleId.Equals(FTUEModuleId.Core), 
                                           loadedData.LoadData("tutorialStage", 0) > 0);
            }
        }

        public void LoadDefault()
        {
            foreach (FTUEModule module in moduleLookup.Values)
            {
                module.LoadTutorial(module.ModuleId.Equals(FTUEModuleId.Core));
            }
        }
    }
}