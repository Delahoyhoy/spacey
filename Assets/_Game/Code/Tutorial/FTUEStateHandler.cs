﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Tutorial
{
    public class FTUEStateHandler : MonoBehaviour
    {
        [SerializeField]
        private FTUEModuleId moduleId;

        [SerializeField]
        private List<FTUEState> validStates = new List<FTUEState>();

        [SerializeField]
        private UnityEvent<bool> onStateMatch;

        private bool initialised;

        private void Start()
        {
            if (initialised)
                return;
            initialised = true;
            if (!FTUEManager.Instance.TryGetModule(moduleId, out FTUEModule module))
                return;
            SetState(module.State);
            module.onStateChange += SetState;
        }

        private void SetState(FTUEState state)
        {
            onStateMatch?.Invoke(validStates.Contains(state));
        }
    }
}