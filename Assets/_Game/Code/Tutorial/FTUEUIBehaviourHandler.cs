﻿using System;
using UnityEngine;

namespace Core.Gameplay.Tutorial
{
    public class FTUEUIBehaviourHandler : MonoBehaviour
    {
        [SerializeField]
        private FTUEUIBehaviour uiBehaviour;

        private Action heldAction;

        public void HandleBehaviour(Action onBegin)
        {
            switch (uiBehaviour)
            {
                case FTUEUIBehaviour.CloseAllUiOnBegin:
                    UIManager.Instance.ForceAllClosed();
                    onBegin?.Invoke();
                    return;
                case FTUEUIBehaviour.WaitCloseUi:
                    if (!UIManager.Instance.InMenu)
                    {
                        onBegin?.Invoke();
                        return;
                    }

                    heldAction = onBegin;
                    UIManager.Instance.onScreenClear += HandleUICleared;        
                    return;
                default:
                    onBegin?.Invoke();
                    return;
            }
        }

        private void HandleUICleared(bool clear)
        {
            if (!clear)
                return;
            heldAction?.Invoke();
            UIManager.Instance.onScreenClear -= HandleUICleared;
        }
    }
}