﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// code changed from Unity3D gradient effect for Unity3D 5.2
/// REF : http://pastebin.com/dJabCfWn
/// NEW CODE OBTAINED FROM: https://pastebin.com/LNcj881C
/// </summary>
public class AdvancedGradient : BaseMeshEffect
{
    [SerializeField]
    private UnityEngine.Gradient gradient;

    [SerializeField]
    private bool horizontal;

    public override void ModifyMesh(VertexHelper vh)
    {
        if (!IsActive()) { return; }

        if (!horizontal)
        {
            List<UIVertex> stream = new List<UIVertex>();
            vh.GetUIVertexStream(stream);

            //UIVertex tv; vh.PopulateUIVertex( ref tv, 0 );

            if (stream.Count <= 0)
                return;

            float bottomY = stream[0].position.y;
            float topY = stream[0].position.y;

            for (int i = 1; i < vh.currentVertCount; i++)
            {
                float y = stream[i].position.y;
                if (y > topY)
                {
                    topY = y;
                }
                else if (y < bottomY)
                {
                    bottomY = y;
                }
            }

            float uiElementHeight = topY - bottomY;
            UIVertex uiv = new UIVertex();
            for (int i = 0; i < vh.currentVertCount; i++)
            {
                vh.PopulateUIVertex(ref uiv, i);
                uiv.color = gradient.Evaluate((uiv.position.y - bottomY) / uiElementHeight);
                vh.SetUIVertex(uiv, i);
            }
        }
        else
        {
            List<UIVertex> stream = new List<UIVertex>();
            vh.GetUIVertexStream(stream);

            //UIVertex tv; vh.PopulateUIVertex( ref tv, 0 );

            if (stream.Count <= 0)
                return;

            float bottomX = stream[0].position.x;
            float topX = stream[0].position.x;

            for (int i = 1; i < vh.currentVertCount; i++)
            {
                float x = stream[i].position.x;
                if (x > topX)
                {
                    topX = x;
                }
                else if (x < bottomX)
                {
                    bottomX = x;
                }
            }

            float uiElementWidth = topX - bottomX;
            UIVertex uiv = new UIVertex();
            for (int i = 0; i < vh.currentVertCount; i++)
            {
                vh.PopulateUIVertex(ref uiv, i);
                uiv.color = gradient.Evaluate((uiv.position.x - bottomX) / uiElementWidth);
                vh.SetUIVertex(uiv, i);
            }
        }
    }
}