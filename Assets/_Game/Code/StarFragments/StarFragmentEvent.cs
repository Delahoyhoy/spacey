using UnityEngine;

namespace Core.Gameplay.StarFragments
{
    public class StarFragmentEvent : MonoBehaviour
    {
        private bool initialised;

        protected virtual void Start()
        {
            if (initialised)
                return;
            OnFragmentCountUpdated(StarFragmentManager.Instance.Fragments);
            StarFragmentManager.Instance.onSetFragmentCount += OnFragmentCountUpdated;
            initialised = true;
        }

        protected virtual void OnFragmentCountUpdated(int newCount) { }
    }
}