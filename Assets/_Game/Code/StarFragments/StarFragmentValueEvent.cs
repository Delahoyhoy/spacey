using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.StarFragments
{
    public class StarFragmentValueEvent : StarFragmentEvent
    {
        [SerializeField]
        private UnityEvent<int> onSetValue;

        protected override void OnFragmentCountUpdated(int newCount)
        {
            onSetValue?.Invoke(newCount);
        }
    }
}