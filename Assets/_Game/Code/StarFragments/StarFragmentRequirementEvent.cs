using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.StarFragments
{
    public class StarFragmentRequirementEvent : StarFragmentEvent
    {
        [SerializeField]
        private int minRequirement;

        [SerializeField]
        private UnityEvent<bool> onSetRequirementMet;

        protected override void OnFragmentCountUpdated(int newCount)
        {
            onSetRequirementMet?.Invoke(newCount >= minRequirement);
        }
    }
}