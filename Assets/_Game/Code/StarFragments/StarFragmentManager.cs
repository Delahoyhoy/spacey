using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Sectors;
using Fumb.Save;
using UnityEngine;

namespace Core.Gameplay.StarFragments
{
    public class StarFragmentManager : MonoBehaviour
    {
        private static StarFragmentManager instance;
        public static StarFragmentManager Instance => instance ??= ObjectFinder.FindObjectOfType<StarFragmentManager>();
        
        [ManagedSaveValue("StarFragmentManagerCurrentFragments")]
        private SaveValueInt saveCurrentFragments;

        [ManagedSaveValue("StarFragmentManagerFragmentsFromClusters")]
        private SaveValueListInt saveFragmentsObtained;

        public int Fragments
        {
            get => saveCurrentFragments.Value;
            set
            {
                saveCurrentFragments.Value = value;
                onSetFragmentCount?.Invoke(value);
            }
        }

        public Action<int> onSetFragmentCount;

        private void Awake()
        {
            instance = this;
        }

        public bool GetHasFragments(int requiredFragments)
        {
            return Fragments >= requiredFragments;
        }

        public int GetStarFragmentsClaimedInCluster(int cluster)
        {
            if (!saveFragmentsObtained.IsPopulated)
                return 0;
            if (saveFragmentsObtained.Value.Count <= cluster)
                return 0;
            return saveFragmentsObtained.Value[cluster];
        }

        public void RegisterFragments(int cluster, int fragmentsAdded)
        {
            if (!saveFragmentsObtained.IsPopulated)
                saveFragmentsObtained.Value = new List<int>();
            if (saveFragmentsObtained.Value.Count <= cluster)
            {
                while (saveFragmentsObtained.Value.Count <= cluster)
                    saveFragmentsObtained.Value.Add(0);
            }

            Fragments += fragmentsAdded;
            saveFragmentsObtained.Value[cluster] += fragmentsAdded;
        }

        public void AddStarFragments(int toAdd)
        {
            RegisterFragments(ClusterManager.Instance.ClusterIndex, toAdd);
        }

        public int GetFragmentsObtained(int cluster)
        {
            if (!saveFragmentsObtained.IsPopulated)
                return 0;
            if (saveFragmentsObtained.Value.Count <= cluster)
                return 0;
            return saveFragmentsObtained.Value[cluster];
        }
    }
}
