using Fumb.LocalNotifications;
using Fumb.General;
using UnityEngine;

/// <summary>
/// Will trigger a permission request for local notifications on the 3rd time the player returns to the app. Never asks
/// again after that
/// </summary>
public class LocalNotificationPermissionPrompter : BaseLocalNotificationPermissionPrompter {

    private const int RestartsBeforePrompt = 3;
    private const string TotalRestartsSaveKey = "LocalNotificationPermissionPrompterDefaultTotalRestartsSaveKey";
    private int TotalAppRestarts {
        get => PlayerPrefs.GetInt(TotalRestartsSaveKey);
        set => PlayerPrefs.SetInt(TotalRestartsSaveKey, value);
    }

    private bool hasPrompted;

    //Occurs after Awake() and before Start() of first scene
    public override void Initialize() {
        AppFocusMethodBroadcaster.OnUserRequestedAppFocusAction += OnApplicationPause;

        hasPrompted = TotalAppRestarts > RestartsBeforePrompt;
        CheckIfShouldPrompt();
    }

    private void OnApplicationPause(bool pause) {
        if (pause) {
            TotalAppRestarts++;
        } else {
            CheckIfShouldPrompt();
        }
    }

    private void CheckIfShouldPrompt() {
        if (hasPrompted) {
            return;
        }
        
        if (TotalAppRestarts == RestartsBeforePrompt) {
            hasPrompted = true;
            RequestDisplayLocalNotificationPermission();
        }
    }
}
