﻿using System;
using UnityEngine;

namespace Core.Gameplay.SpaceVisuals
{
    [Serializable]
    public class BackgroundOption
    {
        [SerializeField]
        private GameObject lightingOption;

        [SerializeField]
        private Material backgroundMaterial;

        public Material BackgroundMaterial => backgroundMaterial;
        
        public void SetActive(bool active)
        {
            lightingOption.gameObject.SetActive(active);
        }
    }
}