﻿using UnityEngine;

namespace Core.Utilities.Visuals
{
    public class RandomAutoSpin : AutoSpin
    {
        [SerializeField]
        private Vector3 minSpeed, maxSpeed;

        private void Start()
        {
            rotateRate = Vector3.Lerp(minSpeed, maxSpeed, Random.value);
        }
    }
}