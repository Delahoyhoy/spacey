using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Core.Gameplay.VisualEffects
{
    public class PostProcessingSimpleEffectHandler : MonoBehaviour
    {
        [SerializeField]
        private PostProcessProfile targetProfile;

        [SerializeField]
        private PostProcessEffectType effectType;

        [SerializeField]
        private float multiplier = 1f;

        private bool defaultEnabled;
        private float defaultValue;

        public void SetEffectEnabled(bool active)
        {
            switch (effectType)
            {
                case PostProcessEffectType.LensDistortion:
                    if (targetProfile.TryGetSettings(out LensDistortion lensDistortion))
                        lensDistortion.active = active;
                    break;
                case PostProcessEffectType.ChromaticAberration :
                    if (targetProfile.TryGetSettings(out ChromaticAberration chromaticAberration))
                        chromaticAberration.active = active;
                    break;
                case PostProcessEffectType.MotionBlur :
                    if (targetProfile.TryGetSettings(out MotionBlur motionBlur))
                        motionBlur.active = active;
                    break;
            }
        }

        public void SetIntensity(float intensity)
        {
            switch (effectType)
            {
                case PostProcessEffectType.LensDistortion:
                    if (targetProfile.TryGetSettings(out LensDistortion lensDistortion))
                        lensDistortion.intensity.value = intensity * multiplier;
                    break;
                case PostProcessEffectType.ChromaticAberration :
                    if (targetProfile.TryGetSettings(out ChromaticAberration chromaticAberration))
                        chromaticAberration.intensity.value = intensity * multiplier;
                    break;
            }
        }

        private void Start()
        {
            SetEffectEnabled(defaultEnabled);
            SetIntensity(defaultValue);
        }

        private void OnApplicationQuit()
        {
            SetEffectEnabled(defaultEnabled);
            SetIntensity(defaultValue);
        }
    }
}
