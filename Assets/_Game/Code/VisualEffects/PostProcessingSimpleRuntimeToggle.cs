using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Core.Gameplay.VisualEffects
{
    public class PostProcessingSimpleRuntimeToggle : MonoBehaviour
    {
        [SerializeField]
        private PostProcessEffectType effectType;

        [SerializeField]
        private PostProcessProfile targetProfile;
        
        private bool isActive;
        public bool active;

        private void Update()
        {
            if (active == isActive)
                return;
            isActive = active;
            switch (effectType)
            {
                case PostProcessEffectType.LensDistortion:
                    if (targetProfile.TryGetSettings(out LensDistortion lensDistortion))
                        lensDistortion.active = active;
                    break;
                case PostProcessEffectType.ChromaticAberration :
                    if (targetProfile.TryGetSettings(out ChromaticAberration chromaticAberration))
                        chromaticAberration.active = active;
                    break;
                case PostProcessEffectType.MotionBlur :
                    if (targetProfile.TryGetSettings(out MotionBlur motionBlur))
                        motionBlur.active = active;
                    break;
            }
        }
    }
}
