using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipTravelEffects : MonoBehaviour
{
    [Range(0, 1f)]
    public float speed;

    private bool active;

    private void SetShipEffectsActive(bool isActive)
    {
        active = isActive;
        GameManager.Instance.SetEngines(isActive);
        if (isActive)
        {
            CameraShake.Instance.StartShake();
            CameraShake.Instance.IntensityMult = speed;
            GameManager.Instance.SetEngineTrailLength(speed);
            return;
        }
        CameraShake.Instance.StopShaking();
    }

    public void ActivateTravel()
    {
        SetShipEffectsActive(true);
    }

    public void EndTravel()
    {
        SetShipEffectsActive(false);
    }

    private void Update()
    {
        if (!active)
            return;
        CameraShake.Instance.IntensityMult = speed;
        GameManager.Instance.SetEngineTrailLength(speed);
    }
}
