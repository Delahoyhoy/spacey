﻿using System;
using UnityEngine;

namespace Core.Gameplay.SpaceVisuals
{
    [Serializable]
    public class SpaceBackgroundSetup
    {
        [SerializeField]
        private float scale;

        public float Scale => scale;
        
        [SerializeField]
        private float angle;

        public float Angle => angle;

        [SerializeField]
        private float distance;

        public float Distance => distance;

        [SerializeField]
        private int displayIndex;

        public int DisplayIndex => displayIndex;

        public SpaceBackgroundSetup(float objectScale, float placementAngle, float placementDistance, int selectedIndex)
        {
            scale = objectScale;
            angle = placementAngle;
            distance = placementDistance;
            displayIndex = selectedIndex;
        }
    }
}