using System;
using UnityEngine;

namespace Core.Gameplay.SpaceVisuals
{
    [Serializable]
    public struct NebulaColourConfig
    {
        public Color colour1;
        public Color colour2;
    }
}