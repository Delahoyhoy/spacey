﻿using System.Collections.Generic;
using Core.Gameplay.SpaceVisuals;
using Fumb.Save;

namespace Fumb.Save
{
    public class SaveSpaceBackgroundSetup : SaveValue<List<SpaceBackgroundSetup>> { }
}