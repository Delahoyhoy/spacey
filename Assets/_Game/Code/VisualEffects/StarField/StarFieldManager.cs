using System;
using System.Collections.Generic;
using Core.Gameplay.Reset;
using UnityEngine;
using Fumb.Save;
using Core.Gameplay.Utilities;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Core.Gameplay.SpaceVisuals
{
    public class StarFieldManager : MonoBehaviour
    {
        private Vector4 fogRevealer = Vector4.zero;

        private MaterialPropertyBlock propertyBlock, fogPropertyBlock;

        [SerializeField]
        private Renderer spaceBackground, fogRenderer;

        [SerializeField]
        private NebulaColourConfig[] nebulaColours;

        [SerializeField]
        private UnityEvent<Color> onSetPrimaryNebulaColour;

        private bool hasMainCamera;
        private Camera mainCamera;
        
        private Camera MainCamera
        {
            get
            {
                if (hasMainCamera)
                    return mainCamera;
                mainCamera = Camera.main;
                if (mainCamera)
                    hasMainCamera = true;
                return mainCamera;
            }
        }

        #region SaveValues

        [ManagedSaveValue("StarFieldManagerUseCustomConfig")]
        private SaveValueBool saveUseCustomConfig;
        
        [ManagedSaveValue("StarFieldManagerNebulaColour1")]
        private SaveValueString saveNebulaColour1;
        
        [ManagedSaveValue("StarFieldManagerNebulaColour2")]
        private SaveValueString saveNebulaColour2;
        
        [ManagedSaveValue("StarFieldManagerNebulaIntensity")]
        private SaveValueFloat saveNebulaIntensity;
        
        [ManagedSaveValue("StarFieldManagerNebulaOffset")]
        private SaveValueVector2 saveNebulaOffset;

        #endregion
        
        [SerializeField]
        private float minNebulaIntensity = 0.5f, maxNebulaIntensity = 0.75f, maxOffsetMagnitude = 1000f;

        private Vector2 GetMagnitudeOffset()
        {
            return new Vector2(Random.Range(-maxOffsetMagnitude, maxOffsetMagnitude),
                Random.Range(-maxOffsetMagnitude, maxOffsetMagnitude));
        }

        private float GetNewIntensity()
        {
            return Random.Range(minNebulaIntensity, maxNebulaIntensity);
        }

        private void Setup()
        {
            saveUseCustomConfig.Value = true;
            
            NebulaColourConfig selectedColourConfig = nebulaColours[Random.Range(0, nebulaColours.Length)];
            propertyBlock.SetColor("_NebulaColour0", selectedColourConfig.colour1);
            saveNebulaColour1.Value = selectedColourConfig.colour1.GetHexValue();
            onSetPrimaryNebulaColour?.Invoke(selectedColourConfig.colour1);
            
            propertyBlock.SetColor("_NebulaColour1", selectedColourConfig.colour2);
            saveNebulaColour2.Value = selectedColourConfig.colour2.GetHexValue();

            Vector2 newOffset = GetMagnitudeOffset();
            propertyBlock.SetVector("_PrimaryOffset", newOffset);
            saveNebulaOffset.Value = newOffset;

            float intensity = GetNewIntensity();
            propertyBlock.SetFloat("_NebulaGlow", intensity);
            saveNebulaIntensity.Value = intensity;
        }

        private void HandleReset(ResetType resetType)
        {
            Setup();
        }

        private void Awake()
        {
            propertyBlock = new MaterialPropertyBlock();
            if (spaceBackground)
                spaceBackground.SetPropertyBlock(propertyBlock);
            fogPropertyBlock = new MaterialPropertyBlock();
        }

        private void Start()
        {
            ResetManager.Instance.finishReset += HandleReset;
            if (!saveUseCustomConfig.Value)
                return;
            
            // Load if modified values are used
            Color mainColour = ColourHexHelper.GetColourFromHex(saveNebulaColour1.Value);
            propertyBlock.SetColor("_NebulaColour0", mainColour);
            propertyBlock.SetColor("_NebulaColour1", ColourHexHelper.GetColourFromHex(saveNebulaColour2.Value));
            propertyBlock.SetVector("_PrimaryOffset", saveNebulaOffset.Value);
            propertyBlock.SetFloat("_NebulaGlow", saveNebulaIntensity.Value);
            onSetPrimaryNebulaColour?.Invoke(mainColour);
        }

        private void OnEnable()
        {
            GameManager.OnZoomChange += ZoomChange;
            GameManager.OnCameraMove += CameraMove;
        }

        private void OnDestroy()
        {
            GameManager.OnZoomChange -= ZoomChange;
            GameManager.OnCameraMove -= CameraMove;
        }

        void ZoomChange(float zoomLevel)
        {
            propertyBlock.SetFloat("_CameraSize", zoomLevel / 25f);
            spaceBackground.transform.localScale = Vector3.one * (zoomLevel * 2f);
            spaceBackground.SetPropertyBlock(propertyBlock);
            fogRenderer.transform.localScale = Vector3.one * (zoomLevel * 2f);
            float ratio = (float) Screen.width / Screen.height;
            Vector3 relativePosition = MainCamera.WorldToViewportPoint(GameManager.Instance.FogCentre);
            //relativePosition.x *= ratio;
            fogRevealer.x = (relativePosition.x * ratio) + ((1 - ratio) / 2f);
            fogRevealer.y = relativePosition.y;
            fogRevealer.z = 0;
            fogRevealer.w = (GameManager.Instance.CurrentRadius / zoomLevel) / 2;
            fogPropertyBlock.SetVector("_Revealer", fogRevealer);
            fogRenderer.SetPropertyBlock(fogPropertyBlock);
        }

        public void SetSpaceColour(Color colour)
        {
            propertyBlock.SetColor("_BaseColour", colour);
            spaceBackground.SetPropertyBlock(propertyBlock);
        }

        public void SetNebulaEffectOffset(float offset)
        {
            propertyBlock.SetFloat("_NebulaEffectOffset", offset);
            spaceBackground.SetPropertyBlock(propertyBlock);
        }

        private void CameraMove(Vector3 newPos)
        {
            propertyBlock.SetVector("_Position", newPos);
            spaceBackground.SetPropertyBlock(propertyBlock);
            float ratio = (float) Screen.width / Screen.height;
            Vector3 relativePosition = MainCamera.WorldToViewportPoint(GameManager.Instance.FogCentre);
            //relativePosition.x *= ratio;
            fogRevealer.x = (relativePosition.x * ratio) + ((1 - ratio) / 2f);
            fogRevealer.y = relativePosition.y;
            fogRevealer.z = 0;
            fogPropertyBlock.SetVector("_Revealer", fogRevealer);
            fogRenderer.SetPropertyBlock(fogPropertyBlock);
        }
    }
}
