using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.SpaceVisuals
{
    public class SpaceBackgroundObject : MonoBehaviour
    {
        [SerializeField]
        private Vector3 minScale, maxScale;

        [SerializeField]
        private float minDistance, maxDistance;

        [SerializeField]
        private MeshRenderer meshRenderer;

        [SerializeField]
        private Transform target;
        
        public SpaceBackgroundSetup Setup(int displayIndex, Material selectedMaterial)
        {
            // Setup values
            Vector3 scale = Vector3.Lerp(minScale, maxScale, Random.value);
            float distance = Random.Range(minDistance, maxDistance);
            float angle = Random.value * 360f;

            ApplySetup(scale, distance, angle, selectedMaterial);

            return new SpaceBackgroundSetup(scale.x, angle, distance, displayIndex);
        }

        private void ApplySetup(Vector3 scale, float distance, float angle, Material selectedMaterial)
        {
            // Apply setup
            meshRenderer.sharedMaterial = selectedMaterial;
            target.localScale = scale;

            float rads = Mathf.Deg2Rad * angle;
            float x = Mathf.Sin(rads) * distance;
            float y = Mathf.Cos(rads) * distance;
            float z = target.localPosition.z;
            
            Vector3 position = new Vector3(x, y, z);
            target.localPosition = position;
        }

        public void Load(SpaceBackgroundSetup data, Material selectedMaterial)
        {
            // Setup values
            Vector3 scale = data.Scale * Vector3.one;
            float distance = data.Distance;
            float angle = data.Angle;

            ApplySetup(scale, distance, angle, selectedMaterial);
        }
    }
}
