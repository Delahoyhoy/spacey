﻿using System;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using UnityEngine;

namespace Core.Gameplay.SpaceVisuals
{
    public class ParralaxBackgroundLayer : MonoBehaviour
    {
        [SerializeField] [Range(0, 1)]
        private float offsetMult;

        [SerializeField]
        private Transform[] objects;

        private List<Vector3> initialPositions = new List<Vector3>();
        private bool initialised;

        private void Initialise()
        {
            if (initialised)
                return;
            for (int i = 0; i < objects.Length; i++)
                initialPositions.Add(objects[i].localPosition);
            initialised = true;
        }
        
        public void SetPosition(Vector3 pos)
        {
            Vector3 adjustedPosition = pos;
            adjustedPosition.z = 0;
            adjustedPosition *= offsetMult;
            Initialise();
            for (int i = 0; i < objects.Length; i++)
                objects[i].localPosition = initialPositions[i] - adjustedPosition;
        }

        private void Start()
        {
            Initialise();
            SetPosition(BasicCameraController.Instance.CurrentPos);
            GameManager.OnCameraMove += SetPosition;
        }

        private void OnDestroy()
        {
            if (initialised)
                GameManager.OnCameraMove -= SetPosition;
        }
    }
}