﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Reset;
using Core.Gameplay.Save;
using Fumb.Save;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.SpaceVisuals
{
    public class SpaceBackgroundObjectController : MonoBehaviour, ISaveLoad
    {
        [SerializeField]
        private SpaceBackgroundObject[] backgroundObjects;

        [SerializeField]
        private Material[] materials;
        
        [ManagedSaveValue("SpaceBackgroundObjectControllerSetup")]
        private SaveSpaceBackgroundSetup currentSetup;

        private void Start()
        {
            ResetManager.Instance.finishReset += resetType => GenerateBackgroundSetup();
        }

        private void GenerateBackgroundSetup()
        {
            if (currentSetup.IsPopulated)
                currentSetup.Value.Clear();
            List<SpaceBackgroundSetup> newData = new List<SpaceBackgroundSetup>();
            List<int> availableMaterials = GetAvailableMaterials();
            
            for (int i = 0; i < backgroundObjects.Length; i++)
            {
                if (availableMaterials.Count <= 0)
                    availableMaterials = GetAvailableMaterials();
                int selectedIndex = Random.Range(0, availableMaterials.Count);
                int selectedMaterial = availableMaterials[selectedIndex];
                Material displayMaterial = materials[selectedMaterial];
                availableMaterials.RemoveAt(selectedIndex);

                SpaceBackgroundSetup setupData = backgroundObjects[i].Setup(selectedMaterial, displayMaterial);
                newData.Add(setupData);
            }

            currentSetup.Value = newData;
        }

        private List<int> GetAvailableMaterials()
        {
            List<int> availableMaterials = new List<int>();

            for (int i = 0; i < materials.Length; i++)
                availableMaterials.Add(i);
            
            return availableMaterials;
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            if (!currentSetup.IsPopulated)
            {
                GenerateBackgroundSetup();
                return;
            }
            for (int i = 0; i < backgroundObjects.Length; i++)
            {
                if (i >= currentSetup.Value.Count)
                    break;
                SpaceBackgroundSetup data = currentSetup.Value[i];
                Material selectedMaterial = materials[Mathf.Clamp(data.DisplayIndex, 0, materials.Length - 1)];
                backgroundObjects[i].Load(data, selectedMaterial);
            }
        }

        public void LoadDefault()
        {
            GenerateBackgroundSetup();
        }
    }
}