using System.Collections;
using System.Collections.Generic;
using Core.Utilities.Visuals;
using UnityEngine;

namespace Core.Gameplay.VisualEffects
{
    public class CustomBurster : MonoBehaviour
    {
        [SerializeField]
        private CurrencyType currencyType;

        [SerializeField]
        private int maxBurst;

        public void Burst(int amount)
        {
            int burstAmount = Mathf.Max(amount, maxBurst);
            CoinBurster.Instance.NewBurstFromWorld(currencyType, transform, burstAmount);
        }
    }

}