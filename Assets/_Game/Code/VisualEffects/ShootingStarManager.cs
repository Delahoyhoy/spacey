using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using UnityEngine;

public class ShootingStarManager : MonoBehaviour
{
    [SerializeField]
    private ShootingStar shootingStarPrefab;
    
    private Queue<ShootingStar> shootingStarPool = new Queue<ShootingStar>();

    [SerializeField]
    private Transform shootingStarHolder;

    [SerializeField]
    private float delayRate = 2f;

    private bool running;
    private float nextStar;

    private ShootingStar GetStar()
    {
        if (shootingStarPool.Count > 0)
            return shootingStarPool.Dequeue();
        return Instantiate(shootingStarPrefab, shootingStarHolder);
    }

    public void NewStar()
    {
        ShootingStar newStar = GetStar();

        void OnComplete()
        {
            newStar.gameObject.SetActive(false);
            shootingStarPool.Enqueue(newStar);
        }
        
        newStar.Setup(BasicCameraController.Instance.GetRandomWorldPosOnScreen(), OnComplete);
    }

    public void SetRunning(bool isRunning)
    {
        if (isRunning)
            nextStar = Time.time + delayRate;
        running = isRunning;
    }

    private void Update()
    {
        if (!running)
            return;
        if (Time.time < nextStar)
            return;
        nextStar = Time.time + delayRate;
        NewStar();
    }
}
