using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteAlways]
public class ShootingStar : MonoBehaviour
{
    [SerializeField]
    private LineRenderer lineRenderer;

    [SerializeField]
    private Transform frontObj, backObj;

    [SerializeField]
    private Vector3 minScale = new Vector3(0.8f, 0.8f, 0.8f), maxScale = new Vector3(1.2f, 1.2f, 1.2f);
    
    private Action onComplete;

    public void Setup(Vector3 position, Action completedAction)
    {
        transform.localScale = Vector3.Lerp(minScale, maxScale, Random.value);
        gameObject.SetActive(true);
        var objTransform = transform;
        position.z = objTransform.position.z;
        objTransform.position = position;
        onComplete = completedAction;
    }
    
    private void Update()
    {
        Vector3[] positions = new Vector3[lineRenderer.positionCount];
        for (int i = 0; i < positions.Length; i++)
        {
            float t = (float)i / (positions.Length - 1);
            positions[i] = Vector3.Lerp(frontObj.localPosition, backObj.localPosition, t);
        }

        lineRenderer.SetPositions(positions);
    }

    public void Complete()
    {
        onComplete?.Invoke();
    }
}
