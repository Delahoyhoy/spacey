using System;
using System.Collections;
using System.Collections.Generic;
using AssetKits.ParticleImage;
using UnityEngine;

namespace Core.Gameplay.VisualEffects
{
    public class CurrencyParticleEffectManager : MonoBehaviour
    {
        private static CurrencyParticleEffectManager instance;

        public static CurrencyParticleEffectManager Instance =>
            instance ??= ObjectFinder.FindObjectOfType<CurrencyParticleEffectManager>();
        
        [SerializeField]
        private CurrencyParticleEffect[] effectTemplates;

        private Dictionary<CurrencyType, CurrencyParticleEffect> particleLookup = new Dictionary<CurrencyType, CurrencyParticleEffect>();

        private void Awake()
        {
            instance = this;
            for (int i = 0; i < effectTemplates.Length; i++)
            {
                if (particleLookup.ContainsKey(effectTemplates[i].CurrencyType))
                    continue;
                particleLookup.Add(effectTemplates[i].CurrencyType, effectTemplates[i]);
            }
        }
    }
}
