using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coffee.UIEffects;

public class SpinneyShiny : MonoBehaviour
{
    [SerializeField]
    private UIShiny shinyComponent;

    private void Update()
    {
        shinyComponent.rotation = Mathf.Repeat(Time.time * 180f, 360f) - 180f;
    }
}
