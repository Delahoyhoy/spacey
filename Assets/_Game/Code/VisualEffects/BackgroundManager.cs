using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Sectors;
using Fumb.Save;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.SpaceVisuals
{
    public class BackgroundManager : MonoBehaviour
    {
        [ManagedSaveValue("BackgroundManagerCurrentSetup")]
        private SaveValueInt saveCurrentBackground;

        [SerializeField]
        private BackgroundOption[] allOptions;

        [SerializeField]
        private MeshRenderer meshRenderer;
        
        public void ChooseBackground()
        {
            int selectedOption = 0;
            int iteration = 0;
            do
            {
                selectedOption = Random.Range(0, allOptions.Length);
                iteration++;
            } while (selectedOption == saveCurrentBackground.Value && iteration < 100);

            saveCurrentBackground.Value = selectedOption;
            SetupBackground(selectedOption);
        }

        private void SetupBackground(int selectedOption)
        {
            for (int i = 0; i < allOptions.Length; i++)
                allOptions[i].SetActive(i == selectedOption);
            meshRenderer.sharedMaterial = allOptions[selectedOption].BackgroundMaterial;
        }
        
        private void Start()
        {
            if (!saveCurrentBackground.IsPopulated)
                ChooseBackground();
            else
                SetupBackground(saveCurrentBackground.Value);
            SectorManager.Instance.onBeginNewSector += ChooseBackground;
        }
    }
}
