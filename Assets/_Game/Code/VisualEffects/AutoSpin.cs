using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Utilities.Visuals
{
    public class AutoSpin : MonoBehaviour
    {
        [SerializeField]
        protected Vector3 rotateRate;
        
        private void Update()
        {
            transform.Rotate(rotateRate * Time.deltaTime, Space.Self);
        }
    }
}
