using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostEffectColouring : MonoBehaviour
{
    [SerializeField]
    private Gradient gradient;

    [SerializeField]
    private ParticleSystem particles;
    
    public void SetColour(Color colour)
    {
        gradient.bottomColor = colour;
        Color fadedColour = colour;
        fadedColour.a = 0;
        gradient.topColor = fadedColour;
        var main = particles.main;
        main.startColor = colour;
    }
}
