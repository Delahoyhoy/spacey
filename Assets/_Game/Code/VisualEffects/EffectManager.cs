﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct EffectHolder
{
    public string effectID;
    public ParticleSystem particleSystem;
}

public class EffectManager : MonoBehaviour
{
    [SerializeField]
    private EffectHolder[] allEffects;

    private Dictionary<string, ParticleSystem> effectDictionary = new Dictionary<string, ParticleSystem>();

    private static EffectManager instance;
    public static EffectManager Instance { get { if (!instance) instance = ObjectFinder.FindObjectOfType<EffectManager>(); return instance; } }

    public void EmitAt(string id, Vector3 position, int count)
    {
        if (!effectDictionary.ContainsKey(id))
            return;
        ParticleSystem selection = effectDictionary[id];
        selection.transform.position = position;
        selection.Emit(count);
    }

    public void EmitAt(string id, Vector3 position, Quaternion rotation, int count)
    {
        if (!effectDictionary.ContainsKey(id))
            return;
        ParticleSystem selection = effectDictionary[id];
        selection.transform.position = position;
        selection.transform.rotation = rotation;
        selection.Emit(count);
    }

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        for (int i = 0; i < allEffects.Length; i++)
            effectDictionary.Add(allEffects[i].effectID, allEffects[i].particleSystem);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
