using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.VisualEffects
{
    public class MaterialRandomiser : MonoBehaviour
    {
        [SerializeField]
        private Material[] possibleMaterials;

        [SerializeField]
        private MeshRenderer targetRenderer;

        public void RandomiseMaterial()
        {
            targetRenderer.sharedMaterial = possibleMaterials[Random.Range(0, possibleMaterials.Length)];
            // load pls
        }
    }
}
