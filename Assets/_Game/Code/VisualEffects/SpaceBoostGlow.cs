using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceBoostGlow : MonoBehaviour
{
    [SerializeField]
    private Color standardColour;
    
    [SerializeField]
    private Color boostLowColour, boostHighColour;

    [SerializeField]
    private float glowRate = 3f;

    [SerializeField]
    private Renderer targetRenderer;
    
    private bool boostActive;

    private MaterialPropertyBlock propertyBlock;
    private static readonly int BaseColour = Shader.PropertyToID("_BaseColour");

    public void SetBoostActive(bool active)
    {
        boostActive = active;
        if (!active)
            SetColour(standardColour);
    }

    private void Start()
    {
        SetColour(standardColour);
    }

    private void SetColour(Color colour)
    {
        //GameManager.Instance.SetSpaceColour(colour);
        // propertyBlock.SetColor(BaseColour, colour);
        // targetRenderer.SetPropertyBlock(propertyBlock);
    }

    private void Update()
    {
        if (!boostActive)
            return;
        float lerpPos = Mathf.PingPong(Time.time, glowRate) / glowRate;
        SetColour(Color.Lerp(boostLowColour, boostHighColour, lerpPos));
        //GameManager.Instance.SetNebulaEffectOffset(Time.time * .01f);
    }
}
