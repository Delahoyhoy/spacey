﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.VIP;
using UnityEngine;

[System.Serializable]
public struct BlastTemplate
{
    public CurrencyType currencyType;
    public Transform target;
    public Sprite sprite;
    public HighlightType space;
    public UnityEngine.Gradient gradient;
}

public class CoinBurster : MonoBehaviour
{
    [SerializeField]
    private float positionBounds;
    private float aspectRatio;

    [SerializeField]
    private BlastTemplate[] blastTemplates;

    private Dictionary<CurrencyType, BlastTemplate> templateDictionary = new Dictionary<CurrencyType, BlastTemplate>();

    private static CoinBurster instance;

    public static CoinBurster Instance { get { if (!instance) instance = ObjectFinder.FindObjectOfType<CoinBurster>(); return instance; } }

    [SerializeField]
    private Transform parentTransform;

    [SerializeField]
    private SpriteRenderer bursterPrefab;

    [SerializeField]
    private Queue<SpriteRenderer> bursterPool = new Queue<SpriteRenderer>();

    [SerializeField]
    private float minDist = 1f, maxDist = 2.5f, speed = 0.5f;

    [SerializeField]
    private int countToSpawn = 7;

    [SerializeField]
    private Transform testStart, testEnd;

    [SerializeField]
    private AnimationCurve curve;

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < blastTemplates.Length; i++)
        {
            if (!templateDictionary.ContainsKey(blastTemplates[i].currencyType))
                templateDictionary.Add(blastTemplates[i].currencyType, blastTemplates[i]);
        }
    }

    private SpriteRenderer GetNewBurster()
    {
        if (bursterPool.Count > 0)
        {
            SpriteRenderer newBuster = bursterPool.Dequeue();
            newBuster.gameObject.SetActive(true);
            return newBuster;
        }
        else
            return Instantiate(bursterPrefab, parentTransform);
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.U))
        //{
        //    NewBurst(CurrencyType.Gold, testStart, 5, 0);
        //}
    }

    public void AddGemsWithEffect(int gems)
    {
        NewBurst(CurrencyType.HardCurrency, testStart, Random.Range(3, 6), gems);
    }

    public void PurchaseGems(int gems)
    {
        double multiplier = VIPManager.Instance.GetValue(VIPCategory.Diamonds);
        double totalGems = System.Math.Round(gems * multiplier);
        NewBurst(CurrencyType.HardCurrency, testStart, Random.Range(5, 10), totalGems);
        CurrencyController.Instance.CurrencyEvent(CurrencyType.HardCurrency, totalGems, "iap", "hc_purchase");
    }

    public void AddGoldWithEffect(double toAdd)
    {
        NewBurst(CurrencyType.SoftCurrency, testStart, Random.Range(3, 6), toAdd);
        //Debug.Log("A " + toAdd);
    }

    public void NewBurst(CurrencyType currencyType, Transform from, int count, double toAdd = 0)
    {
        NewBurst(currencyType, from.position, count, toAdd);
        //Debug.Log("B " + toAdd);
    }

    public void NewBurstFromWorld(CurrencyType currencyType, Transform from, int count, double toAdd = 0)
    {
        if (!templateDictionary.ContainsKey(currencyType))
            return;
        BlastTemplate template = templateDictionary[currencyType];
        Vector3 pos = Camera.main.WorldToScreenPoint(from.position);
        StartCoroutine(Burst(pos, template.target, count, template.sprite, currencyType, template.gradient, toAdd, template.space));
        //Debug.Log("C " + toAdd);
    }

    public void NewBurst(CurrencyType currencyType, Vector3 from, int count, double toAdd = 0)
    {
        if (!templateDictionary.ContainsKey(currencyType))
        {
            Debug.LogError($"FAILED TO GET CURRENCY: {currencyType}");
            return;
        }
        BlastTemplate template = templateDictionary[currencyType];
        StartCoroutine(Burst(from, template.target, count, template.sprite, currencyType, template.gradient, toAdd, template.space));
    }

    IEnumerator Burst(Vector3 from, Transform to, int count, Sprite sprite, CurrencyType type, UnityEngine.Gradient colour, double toAdd = 0, HighlightType space = HighlightType.Screen)
    {
        //Debug.Log("D " + toAdd);
        BezierHolder[] startPoints = new BezierHolder[count], endPoints = new BezierHolder[count];
        SpriteRenderer[] bursters = new SpriteRenderer[count];
        float eachAngle = 360f / count;
        Vector3 startPosition = GetPositionFromScreen(from);
        Vector3 endPosition = space == HighlightType.Screen ? GetPositionFromScreen(to.position) : GetPositionFromWorld(to.position);
        AudioManager.Play("Burst", 0.33f);
        for (int i = 0; i < count; i++)
        {
            startPoints[i] = new BezierHolder(startPosition, eachAngle * i, Random.Range(minDist, maxDist));
            endPoints[i] = new BezierHolder(endPosition, (eachAngle * i) + Random.Range(20f, 60f), Random.Range(minDist, maxDist));
            SpriteRenderer burster = GetNewBurster();
            TrailRenderer trailRenderer = burster.GetComponent<TrailRenderer>();
            if (trailRenderer)
                trailRenderer.colorGradient = colour;
            burster.sprite = sprite;
            burster.transform.localPosition = startPosition;
            bursters[i] = burster;
        }

        for (float t = 0; t < 1f; t += Time.deltaTime * speed)
        {
            for (int i = 0; i < count; i++)
            {
                bursters[i].transform.localPosition = BezierCurve.InterpolatePos(startPoints[i], endPoints[i], curve.Evaluate(t));
            }
            yield return null;
        }
        
        for (int i = 0; i < count; i++)
        {
            bursters[i].gameObject.SetActive(false);
            bursterPool.Enqueue(bursters[i]);
        }
        if (toAdd > 0)
        {
            AudioManager.Play("Coin", 0.1f);
            CurrencyController.Instance.AddCurrency(toAdd, type);
            //Debug.Log("Adding " + toAdd);
        }
    }

    private Vector3 GetPositionFromWorld(Vector3 from)
    {
        return GetPositionFromScreen(Camera.main.WorldToScreenPoint(from));
    }

    private Vector3 GetPositionFromScreen(Vector3 from)
    {
        aspectRatio = (float)Screen.width / Screen.height;
        float normalisedX = from.x / Screen.width, normalisedY = from.y / Screen.height;
        float minX = (1 - aspectRatio) / 2f, maxX = 1 - minX;
        normalisedX = Mathf.Lerp(minX, maxX, normalisedX);
        return new Vector3(Mathf.Lerp(-positionBounds, positionBounds, normalisedX), Mathf.Lerp(-positionBounds, positionBounds, normalisedY), 1f);
    }
}
