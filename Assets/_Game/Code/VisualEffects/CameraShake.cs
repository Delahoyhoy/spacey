﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CameraShake : MonoBehaviour
{
    private static CameraShake instance;

    public static CameraShake Instance
    {
        get
        {
            if (instance)
                return instance;
            List<CameraShake> possibleInstances = ObjectFinder.FindAllObjectsOfType<CameraShake>();
            for (int i = 0; i < possibleInstances.Count; i++)
            {
                if (!possibleInstances[i].canBeInstance)
                    continue;
                instance = possibleInstances[i];
                break;
            }
            return instance;
        }
    }
    
    [SerializeField]
    private float shakeTime = 2f, shakeIntensity = 1f, shakeSpeed = 5f;

    [SerializeField]
    private float intensityMult = 1f;

    public float IntensityMult
    {
        get => intensityMult;
        set => intensityMult = value;
    }

    private bool shaking = false;

    [SerializeField]
    private Transform[] otherObjects;

    [SerializeField]
    private Camera camera;

    private Coroutine shakeRoutine;

    private Vector3[] originalPositions;

    public bool canBeInstance = true;

    private void Awake()
    {
        if (canBeInstance)
            instance = this;
    }

    public void StartShake()
    {
        if (shakeRoutine != null)
            StopCoroutine(shakeRoutine);
        shaking = true;
        StartCoroutine(ShakeLoop());
    }

    public void StopShaking()
    {
        shaking = false;
    }

    private IEnumerator ShakeLoop()
    {
        if (!camera)
            camera = Camera.main;
        Vector3 originalPos = transform.localPosition;
        Vector3[] originalPositions = new Vector3[otherObjects.Length];
        float t = 0, time;
        int i = 0;
        for (int j = 0; j < otherObjects.Length; j++)
        {
            originalPositions[j] = otherObjects[j].localPosition;
        }
        Vector3 newPos, fromPos;
        while (shaking)
        {
            fromPos = transform.localPosition;
            newPos = originalPos;
            newPos.x = Random.Range(0.1f, shakeIntensity) * (i % 2 == 0 ? -1 : 1);
            newPos.y = Random.Range(0.1f, shakeIntensity) * (i % 2 == 0 ? -1 : 1);
            time = Vector3.Distance(fromPos, newPos);
            for (float f = 0; f < time; f += Time.deltaTime * shakeSpeed)
            {
                transform.localPosition = Vector3.Lerp(fromPos, newPos, f) * IntensityMult;
                for (int j = 0; j < otherObjects.Length; j++)
                {
                    Vector3 pos = originalPositions[j] + (transform.localPosition * (camera.orthographicSize / 2f));
                    pos.z = 0;
                    otherObjects[j].localPosition = pos;
                }
                yield return null;
            }
            i++;
            t += time / shakeSpeed;
        }
        fromPos = transform.localPosition;
        newPos = originalPos;
        time = Vector3.Distance(fromPos, newPos);
        for (float f = 0; f < time; f += Time.deltaTime * shakeSpeed)
        {
            transform.localPosition = Vector3.Lerp(fromPos, originalPos, f) * IntensityMult;
            for (int j = 0; j < otherObjects.Length; j++)
            {
                Vector3 pos = originalPositions[j] + (transform.localPosition * (camera.orthographicSize / 2f));
                pos.z = 0;
                otherObjects[j].localPosition = pos;
            }
            yield return null;
        }
        transform.localPosition = originalPos;
        for (int j = 0; j < otherObjects.Length; j++)
        {
            //Vector3 pos = -transform.localPosition * (Screen.height / Camera.main.orthographicSize / 2f);
            //pos.z = 0;
            otherObjects[j].localPosition = originalPositions[j];
        }
    }

    IEnumerator Shake()
    {
        if (!camera)
            camera = Camera.main;
        shaking = true;
        float t = 0, time;
        int i = 0;
        Vector3 originalPos = transform.localPosition;
        Vector3[] originalPositions = new Vector3[otherObjects.Length];
        for (int j = 0; j < otherObjects.Length; j++)
        {
            originalPositions[j] = otherObjects[j].localPosition;
        }
        Vector3 newPos, fromPos;
        while (t < shakeTime)
        {
            fromPos = transform.localPosition;
            newPos = originalPos;
            newPos.x = Random.Range(0.1f, shakeIntensity) * (i % 2 == 0 ? -1 : 1);
            newPos.y = Random.Range(0.1f, shakeIntensity) * (i % 2 == 0 ? -1 : 1);
            time = Vector3.Distance(fromPos, newPos);
            for (float f = 0; f < time; f += Time.deltaTime * shakeSpeed)
            {
                transform.localPosition = Vector3.Lerp(fromPos, newPos, f) * IntensityMult;
                for (int j = 0; j < otherObjects.Length; j++)
                {
                    Vector3 pos = originalPositions[j] + (-transform.localPosition * (Screen.height / camera.orthographicSize / 2f));
                    pos.z = 0;
                    otherObjects[j].localPosition = pos;
                }
                yield return null;
            }
            i++;
            t += time / shakeSpeed;
        }
        fromPos = transform.localPosition;
        newPos = originalPos;
        time = Vector3.Distance(fromPos, newPos);
        for (float f = 0; f < time; f += Time.deltaTime * shakeSpeed)
        {
            transform.localPosition = Vector3.Lerp(fromPos, originalPos, f) * IntensityMult;
            for (int j = 0; j < otherObjects.Length; j++)
            {
                Vector3 pos = originalPositions[j] + (-transform.localPosition * (Screen.height / camera.orthographicSize / 2f));
                pos.z = 0;
                otherObjects[j].localPosition = pos;
            }
            yield return null;
        }
        transform.localPosition = originalPos;
        for (int j = 0; j < otherObjects.Length; j++)
        {
            //Vector3 pos = -transform.localPosition * (Screen.height / Camera.main.orthographicSize / 2f);
            //pos.z = 0;
            otherObjects[j].localPosition = originalPositions[j];
        }
        shaking = false;
    }
    

    public void ShakeCamera()
    {
        if (!shaking)
            StartCoroutine(Shake());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            ShakeCamera();
        }
    }
}
