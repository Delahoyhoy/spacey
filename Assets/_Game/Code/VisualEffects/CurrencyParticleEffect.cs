﻿using System;
using AssetKits.ParticleImage;
using UnityEngine;

namespace Core.Gameplay.VisualEffects
{
    [Serializable]
    public class CurrencyParticleEffect
    {
        [SerializeField]
        private CurrencyType currencyType;

        public CurrencyType CurrencyType => currencyType;

        [SerializeField]
        private ParticleImage particleSystem;

        public ParticleImage ParticleSystem => particleSystem;

        private RectTransform rectTransform;

        private bool initialised;

        public void Initialise()
        {
            if (initialised)
                return;
            rectTransform = (RectTransform)particleSystem.transform;
        }

        public void Emit(int count)
        {
            Initialise();
            // Just found out there is no Emit(int count) method... might revisit it later
        }
    }
}