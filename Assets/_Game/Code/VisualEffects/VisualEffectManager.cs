using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;

[ExecuteAlways]
public class VisualEffectManager : MonoBehaviour
{
    [SerializeField]
    private bool previewInEditMode;
    
    public bool running;

    private bool isRunning;

    [SerializeField]
    private bool cameraShakeEnabled;

    [ConditionalField("cameraShakeEnabled")]
    public float shakeIntensity;

    [SerializeField] [ConditionalField("cameraShakeEnabled")]
    private bool overrideShakeEffect;

    [SerializeField] [ConditionalField("overrideShakeEffect")]
    private UnityEvent<bool> onSetShakeActive;

    [SerializeField] [ConditionalField("overrideShakeEffect")]
    private UnityEvent<float> onUpdateShakeEffect;
    
    [SerializeField]
    private bool zoomOverrideEnabled;
    
    [ConditionalField("zoomOverrideEnabled")]
    public float zoomMultiplier;

    [SerializeField] [ConditionalField("zoomOverrideEnabled")]
    private bool overrideZoomEffect;

    [SerializeField] [ConditionalField("overrideZoomEffect")]
    private UnityEvent<float> onUpdateZoom;

    [SerializeField]
    private bool overridePostProcessing;

    [ConditionalField("overridePostProcessing")]
    public float postProcessMagnitude;

    [SerializeField] [ConditionalField("overridePostProcessing")]
    private UnityEvent<float> onUpdatePostProcessing;

    [SerializeField] [ConditionalField("overridePostProcessing")]
    private UnityEvent<bool> onSetPostProcessingActive;

    private void Update()
    {
        if (!previewInEditMode && !Application.isPlaying)
            return;
        if (running != isRunning)
        {
            isRunning = running;
            if (cameraShakeEnabled)
            {
                if (!overrideShakeEffect)
                {
                    if (running)
                        CameraShake.Instance.StartShake();
                    else
                        CameraShake.Instance.StopShaking();
                }
                else
                    onSetShakeActive?.Invoke(running);
            }

            if (zoomOverrideEnabled && !running)
            {
                if (overrideZoomEffect)
                    onUpdateZoom?.Invoke(1f);
                else
                    BasicCameraController.Instance.SetZoomMult(1f);
            }

            if (overridePostProcessing)
            {
                if (!running)
                    onUpdatePostProcessing?.Invoke(0);
                onSetPostProcessingActive?.Invoke(running);
            }
        }
        
        if (!running)
            return;
        if (cameraShakeEnabled)
        {
            if (overrideShakeEffect)
                onUpdateShakeEffect?.Invoke(shakeIntensity);
            else
                CameraShake.Instance.IntensityMult = shakeIntensity;
        }

        if (zoomOverrideEnabled)
        {
            if (overrideZoomEffect)
                onUpdateZoom?.Invoke(zoomMultiplier);
            else
                BasicCameraController.Instance.SetZoomMult(zoomMultiplier);
        }

        if (overridePostProcessing)
        {
            onUpdatePostProcessing?.Invoke(postProcessMagnitude);
        }
    }
}
