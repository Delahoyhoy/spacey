using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Core.Gameplay.VisualEffects
{
    [ExecuteAlways]
    public class MotionBlurPostProcessBehaviour : MonoBehaviour
    {
        [SerializeField]
        private PostProcessProfile targetProfile;
        
        private bool active;

        public bool effectActive;

        public float shutterAngle = 270;
        public int sampleCount = 10;

        public void SetEffectActive(bool isActive)
        {
            effectActive = active = isActive;
            if (targetProfile && targetProfile.TryGetSettings(out MotionBlur motionBlur))
                motionBlur.active = active;
        }

        private void Update()
        {
            if (!targetProfile)
                return;
            if (!targetProfile.TryGetSettings(out MotionBlur motionBlur))
                return;
            
            if (effectActive != active)
                SetEffectActive(effectActive);
            
            motionBlur.sampleCount.value = sampleCount;
            motionBlur.shutterAngle.value = shutterAngle;
        }
    }
}
