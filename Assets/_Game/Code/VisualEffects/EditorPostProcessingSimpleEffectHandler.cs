﻿using UnityEngine;

namespace Core.Gameplay.VisualEffects
{
    [ExecuteAlways]
    public class EditorPostProcessingSimpleEffectHandler : PostProcessingSimpleEffectHandler { }
}