﻿namespace Core.Gameplay.VisualEffects
{
    public enum PostProcessEffectType
    {
        LensDistortion,
        ChromaticAberration,
        MotionBlur,
    }
}