﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using UnityEngine;

public class GaussianTest : MonoBehaviour {

    //material that's applied when doing postprocessing
    [SerializeField]
    private Material postprocessMaterial, galaxyZoomMaterial;

    public float Blur { set { targetBlur = value; } }

    private float targetBlur = 0, currentBlur = 0;

    [SerializeField]
    [Range(0, 1f)]
    private float transitionAmount;

    public RenderTexture renderTexture;

    [SerializeField]
    private Camera secondaryCamera;

    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private AnimationCurve zoomCurve;

    [SerializeField]
    private LayerMask standardMask, secondaryMask;

    [SerializeField]
    private GameObject systemUI;

    public bool useBlur = true;

    private void Awake()
    {
        if (!postprocessMaterial)
            postprocessMaterial = new Material(Shader.Find("Custom/Gaussian"));
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        // if (transitionAmount > 0)
        // {
        //     galaxyZoomMaterial.SetFloat("_Amount", transitionAmount);
        //     if (renderTexture != null)
        //     {
        //         if (transitionAmount > 0)
        //         {
        //             galaxyZoomMaterial.SetTexture("_SecondaryMat", renderTexture);
        //             Graphics.Blit(source, destination, galaxyZoomMaterial);
        //         }
        //         //RenderTexture.ReleaseTemporary(renderTexture);
        //         //renderTexture.Release();
        //     }
        // }
        // else if (currentBlur > 0.001f && useBlur)
        // {
        //     postprocessMaterial.SetFloat("_BlurSize", currentBlur);
        //     var temporaryTexture = RenderTexture.GetTemporary(source.width / 2, source.height / 2);
        //     Graphics.Blit(source, temporaryTexture);
        //     var temporaryTexture2 = RenderTexture.GetTemporary(source.width / 2, source.height / 2);
        //     Graphics.Blit(temporaryTexture, temporaryTexture2, postprocessMaterial, 0);
        //     RenderTexture.ReleaseTemporary(temporaryTexture);
        //     Graphics.Blit(temporaryTexture2, destination, postprocessMaterial, 1);
        //     RenderTexture.ReleaseTemporary(temporaryTexture2);
        // }
        // else
        Graphics.Blit(source, destination);
    }

    public void SetTransitionAmount(float amount)
    {
        transitionAmount = zoomCurve.Evaluate(amount);
    }

    public void SetSecondaryCameraSize(float size)
    {
        secondaryCamera.orthographicSize = size;
    }

    public void OnStartTransition(CurrentView to)
    {
        switch (to)
        {
            case CurrentView.Galaxy:
                secondaryCamera.gameObject.SetActive(true);
                transitionAmount = 0f;
                systemUI.SetActive(false);
                //mainCamera.cullingMask = standardMask.value;
                break;
            case CurrentView.System:
                mainCamera.cullingMask = standardMask.value;
                secondaryCamera.gameObject.SetActive(true);
                transitionAmount = 1f;
                break;
        }
    }

    public void OnEndTransition(CurrentView to)
    {
        switch (to)
        {
            case CurrentView.Galaxy:
                transitionAmount = 0;
                mainCamera.cullingMask = secondaryMask.value;
                secondaryCamera.gameObject.SetActive(false);
                break;
            case CurrentView.System:
                transitionAmount = 0;
                secondaryCamera.gameObject.SetActive(false);
                systemUI.SetActive(true);
                GameManager.ChangeZoom(mainCamera.orthographicSize);
                break;
        }
    }

    private void Update()
    {
        currentBlur = Mathf.Lerp(currentBlur, targetBlur, Time.deltaTime * 4f);
    }
}
