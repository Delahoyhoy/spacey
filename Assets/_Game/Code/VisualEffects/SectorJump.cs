using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using Core.Gameplay.Reset;
using Game.Adverts;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

namespace Core.Gameplay.VisualEffects
{
    public class SectorJump : MonoBehaviour
    {
        [SerializeField]
        private Transform shipTarget;

        [SerializeField]
        private Animator warpAnim, uiEffect;

        [SerializeField]
        private CanvasGroup[] canvases;

        private Action jumpAction;
        
        [SerializeField]
        private UnityEvent jumpEvent, jumpComplete;

        private static readonly int Out = Animator.StringToHash("Out");
        private static readonly int FadeIn = Animator.StringToHash("FadeIn");
        private static readonly int In = Animator.StringToHash("In");
        
        private static readonly int WarpOut = Animator.StringToHash("WarpOut");
        private static readonly int WarpIn = Animator.StringToHash("WarpIn");

        [SerializeField]
        private PlayableDirector director, prestigeJump, wormholeJump;

        public void ExecuteJump(Action onJump)
        {
            uiEffect.gameObject.SetActive(true);
            BasicCameraController.Instance.LockOnTarget(shipTarget);
            jumpAction = onJump;
            director.Play();
            //StartCoroutine(JumpToNextSector(onJump));
            InterstitialManager.instance.AddOverrideFlag();
        }

        public void ExecuteClusterJump(Action onJump)
        {
            uiEffect.gameObject.SetActive(true);
            BasicCameraController.Instance.LockOnTarget(shipTarget);
            jumpAction = onJump;
            wormholeJump.Play();
            InterstitialManager.instance.AddOverrideFlag();
        }

        public void ExecutePrestigeJump(Action onJump)
        {
            uiEffect.gameObject.SetActive(true);
            BasicCameraController.Instance.LockOnTarget(shipTarget);
            jumpAction = onJump;
            prestigeJump.Play();
            InterstitialManager.instance.AddOverrideFlag();
        }

        private void SetCanvasGroupsFade(float fade)
        {
            for (int i = 0; i < canvases.Length; i++)
                canvases[i].alpha = fade;
        }

        private void SetCanvasGroupsInteractable(bool interactable)
        {
            for (int i = 0; i < canvases.Length; i++)
                canvases[i].interactable = interactable;
        }

        public void FadeCanvasGroupsOut()
        {
            StartCoroutine(FadeCanvases(2f, false));
        }
        
        public void FadeCanvasGroupsIn()
        {
            StartCoroutine(FadeCanvases(2f, true));
        }

        public void UnlockAll()
        {
            uiEffect.gameObject.SetActive(false);
            BasicCameraController.Instance.UnlockTarget();
            jumpComplete?.Invoke();
            InterstitialManager.instance.ClearOverrideFlag();
        }

        private IEnumerator FadeCanvases(float speed = 1f, bool fadeIn = false)
        {
            SetCanvasGroupsFade(fadeIn ? 0 : 1f);
            if (!fadeIn)
                SetCanvasGroupsInteractable(false);
            for (float f = 0; f < 1f; f += Time.deltaTime * speed)
            {
                float fadeValue = fadeIn ? f : 1f - f;
                SetCanvasGroupsFade(fadeValue);
                yield return null;
            }
            if (fadeIn)
                SetCanvasGroupsInteractable(true);
            SetCanvasGroupsFade(fadeIn ? 1f : 0);
        }
        
        public void HandleJump()
        {
            jumpAction?.Invoke();
            jumpEvent?.Invoke();
        }
    }
    
    
}
