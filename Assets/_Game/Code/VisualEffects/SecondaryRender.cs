﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryRender : MonoBehaviour {

    [SerializeField]
    private GaussianTest galaxyTransition;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (galaxyTransition)
        {
            if (galaxyTransition.renderTexture != null)
                RenderTexture.ReleaseTemporary(galaxyTransition.renderTexture);
            RenderTexture texture = RenderTexture.GetTemporary(source.width, source.height);
            Graphics.Blit(source, texture);
            galaxyTransition.renderTexture = texture;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
