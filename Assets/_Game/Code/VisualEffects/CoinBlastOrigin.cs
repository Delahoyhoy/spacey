﻿using UnityEngine;

namespace Core.Utilities.Visuals
{
    public class CoinBlastOrigin : MonoBehaviour
    {
        [SerializeField]
        private CurrencyType currencyType;
        
        [SerializeField]
        private HighlightType space;

        public void EmitBlast(int count)
        {
            if (space == HighlightType.World)
                CoinBurster.Instance.NewBurstFromWorld(currencyType, transform, count);
            else
                CoinBurster.Instance.NewBurst(currencyType, transform, count);
        }
    }
}