using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SimpleZoomManager : MonoBehaviour
{
    [SerializeField]
    private float standardZoom = 5f;

    [SerializeField]
    private Camera targetCamera;
    
    private float zoomMult;

    public void SetZoomMult(float newZoom)
    {
        if (!targetCamera)
            return;
        zoomMult = newZoom;
        targetCamera.orthographicSize = standardZoom * newZoom;
    }
}
