﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Save;
using Core.Gameplay.Tutorial;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackMenu : MonoBehaviour, ISaveLoad {

    [SerializeField]
    private UIScreen feedbackScreen, popupScreen, failDialogue, highPraise;

    [SerializeField]
    private GameObject sendingScreen;

    private string formAddress = "https://docs.google.com/forms/u/1/d/e/1FAIpQLScby9FJVGnIv7xeRbbAQXvQm581T5etkuGmTNuy3hN1d89sKA/formResponse";

    public Button submitButton;

    private bool toggleSelected = false;

    public Toggle[] allToggles = new Toggle[5];
    public ToggleGroup toggleGroup;
    public InputField commentBox;

    [SerializeField]
    private GameObject playStoreIcon, appleIcon;

    [SerializeField]
    private GameObject notification;

    private bool hasDoneFeedback = false;

    public Text gemCount;
    
    public string GetToggleLevel()
    {
        for (int i = 0; i < allToggles.Length; i++)
        {
            if (allToggles[i].isOn)
                return (i + 1).ToString();
        }
        return "N/A";
    }

    private int GetRating()
    {
        for (int i = 0; i < allToggles.Length; i++)
        {
            if (allToggles[i].isOn)
                return i;
        }
        return 0;
    }

    private void Check()
    {
        notification.SetActive(CurrencyController.Instance.TotalGoldEver >= 1e7 && FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) && !hasDoneFeedback);
    }

    public void RecalculateGems()
    {
        int gems = 0;
        if (toggleGroup.AnyTogglesOn())
            gems += 5;
        if (commentBox.text.Length > 5)
            gems += 15;
        gemCount.text = gems.ToString();
    }

    public string uuidSlot = "entry.582521438", ratingSlot = "entry.736867718", commentSlot = "entry.1303373033";

    IEnumerator Post()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            int gemsToAdd = 5;
            sendingScreen.SetActive(true);
            UIManager.Instance.SetBlockBack(true);
            WWWForm form = new WWWForm();
            form.AddField(uuidSlot, SystemInfo.deviceUniqueIdentifier);
            form.AddField(ratingSlot, GetToggleLevel());
            form.AddField(commentSlot, commentBox.text);
            if (!string.IsNullOrEmpty(commentBox.text))
            {
                gemsToAdd += 15;
            }
            byte[] rawData = form.data;
            WWW www = new WWW(formAddress, rawData);
            yield return www;
            if (!string.IsNullOrEmpty(www.error))
                Debug.LogError(www.error);
            if (CurrencyController.Instance)
                CoinBurster.Instance.AddGemsWithEffect(gemsToAdd);
            //InGameNotifications.Instance.ClearNotification("feedback");
            hasDoneFeedback = true;
            notification.SetActive(false);
            if (GetRating() < 3)
                OpenPopup();
            else
                OpenHighPraisePopup();
            sendingScreen.SetActive(false);
            UIManager.Instance.SetBlockBack(false);
        }
        else
            OpenErrorDialogue();
    }

    public void OpenStorePage()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=com.fumbgames.spacey");
#else
        Application.OpenURL("itms-apps://itunes.apple.com/app/id1455782143");
#endif
    }

    public void SetFeedbackCompleted()
    {
        hasDoneFeedback = true;
        notification.SetActive(false);
    }

    public void OnSelectRating()
    {
        if (AudioManager.Instance)
            AudioManager.Instance.PlayClip("Click", 1f);
        toggleSelected = toggleGroup.AnyTogglesOn();
        submitButton.interactable = true;
        RecalculateGems();
    }

    public void Submit()
    {
        if (AudioManager.Instance)
            AudioManager.Instance.PlayClip("Click", 1f);
        if (Application.internetReachability == NetworkReachability.NotReachable)
            OpenErrorDialogue();
        else
            StartCoroutine(Post());
    }

    public void OpenDiscord()
    {
        if (AudioManager.Instance)
            AudioManager.Instance.PlayClip("Click", 1f);
        Application.OpenURL("https://discord.gg/MBsrucJ");
    }

    public void OpenPopup()
    {
        popupScreen.Open();
    }

    public void OpenHighPraisePopup()
    {
#if UNITY_ANDROID
        playStoreIcon.SetActive(true);
        appleIcon.SetActive(false);
#else
        playStoreIcon.SetActive(false);
        appleIcon.SetActive(true);
#endif
        highPraise.Open();
    }

    public void OpenErrorDialogue()
    {
        failDialogue.Open();
    }

    public void CloseErrorDialogue()
    {
        failDialogue.Close();
    }

    public void OpenMainForm()
    {
        feedbackScreen.Open();
        //DisableToggles();
    }

    public void CloseWholeThing()
    {
        if (AudioManager.Instance)
            AudioManager.Instance.PlayClip("Click", 1f);
        feedbackScreen.Close();
        failDialogue.CloseInstant();
        popupScreen.CloseInstant();
        sendingScreen.SetActive(false);
        UIManager.Instance.SetBlockBack(false);
    }

    // Use this for initialization
    void Start () {
        toggleGroup.SetAllTogglesOff(false);
	}

    public void DisableToggles()
    {
        StartCoroutine(WaitToggles());
    }

    IEnumerator WaitToggles()
    {
        //yield return null;
        yield return null;
        toggleGroup.SetAllTogglesOff(false);
        submitButton.interactable = false;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Save(ref Dictionary<string, object> saveData)
    {
        saveData.Add("feedbackDone", hasDoneFeedback);
    }

    public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
    {
        hasDoneFeedback = loadedData.LoadData("feedbackDone", false);
    }

    public void LoadDefault()
    {
        hasDoneFeedback = false;
    }
}