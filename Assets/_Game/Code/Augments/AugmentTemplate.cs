using System;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    [Serializable]
    public struct AugmentTemplate
    {
        public AugmentEffect augmentEffect;
        public Sprite symbol;
        public float[] effectMagnitudes;
        public string name, descriptionFormat, localisedID;

        public float GetEffectAtTier(int tier)
        {
            if (tier < 0 || tier >= effectMagnitudes.Length)
                return 0;
            return effectMagnitudes[tier];
        }

        public string GetDescrptionAtTier(Tier tier)
        {
            float magnitude = GetEffectAtTier((int) tier);
            return string.Format(descriptionFormat, magnitude);
        }

        public string GetCustomDescription(float magnitude)
        {
            return string.Format(descriptionFormat, magnitude);
        }
    }
}