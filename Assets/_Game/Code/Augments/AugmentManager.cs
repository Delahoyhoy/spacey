﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Outposts;
using Core.Gameplay.Save;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    public class AugmentManager : MonoBehaviour, ISaveLoad
    {

        [SerializeField]
        private AugmentTemplate[] allAugments;

        [SerializeField]
        private AugmentTierImage[] backgrounds;

        private Dictionary<AugmentEffect, Dictionary<Tier, int>> inventoryMatrix =
            new Dictionary<AugmentEffect, Dictionary<Tier, int>>();

        private Dictionary<AugmentEffect, AugmentTemplate> augmentTemplates =
            new Dictionary<AugmentEffect, AugmentTemplate>();

        private Dictionary<Tier, Sprite> augmentBackgrounds = new Dictionary<Tier, Sprite>();

        public static AugmentManager Instance
        {
            get
            {
                if (!instance) instance = FindObjectOfType<AugmentManager>();
                return instance;
            }
        }

        private static AugmentManager instance;

        private string SaveAugmentCount(AugmentEffect effect, Tier tier, int count)
        {
            return (int) effect + "~" + (int) tier + "~" + count;
        }

        private void LoadCount(string data)
        {
            string[] splitData = data.Split('~');
            if (splitData.Length < 3)
                return;
            int effect = 0, tier = 0, count = 0;
            if (int.TryParse(splitData[0], out effect))
                if (int.TryParse(splitData[1], out tier))
                    if (int.TryParse(splitData[2], out count))
                    {
                        inventoryMatrix[(AugmentEffect) effect][(Tier) tier] = count;
                        //Debug.Log(((Tier)tier).ToString() + " " + ((AugmentEffect)effect) + ": " + count);
                    }
        }

        public Sprite GetSymbol(AugmentEffect augmentEffect)
        {
            return augmentTemplates[augmentEffect].symbol;
        }

        public float GetMagnitude(AugmentEffect effect, Tier tier)
        {
            return augmentTemplates[effect].GetEffectAtTier((int) tier);
        }

        private void SetupDictionary()
        {
            for (int i = 0; i < Enum.GetValues(typeof(AugmentEffect)).Length; i++)
            {
                AugmentEffect effect = (AugmentEffect) i;
                if (!inventoryMatrix.ContainsKey(effect))
                    inventoryMatrix.Add(effect, new Dictionary<Tier, int>());
                for (int j = 0; j < Enum.GetValues(typeof(Tier)).Length; j++)
                {
                    Tier tier = (Tier) j;
                    if (!inventoryMatrix[effect].ContainsKey(tier))
                        inventoryMatrix[effect].Add(tier, 0);
                }
            }
        }

        public bool GetCanCraft(AugmentEffect effect, Tier tier)
        {
            if (tier == Tier.Common)
                return CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) >= 25;
            return inventoryMatrix[effect][tier - 1] >= 4;
        }

        public string GetDescription(AugmentEffect effect, Tier tier)
        {
            return augmentTemplates[effect].GetDescrptionAtTier(tier);
        }

        public string GetDescription(AugmentEffect effect, float magnitude)
        {
            return augmentTemplates[effect].GetCustomDescription(magnitude);
        }

        public string GetFormattedName(AugmentEffect effect, Tier tier)
        {
            string tierName, effectName;
            switch (tier)
            {
                case (Tier.Common):
                    tierName = "COMMON"; //LocalisationManager.Instance.GetLocalisedString("w_common");
                    break;
                case (Tier.Uncommon):
                    tierName = "UNCOMMON"; //LocalisationManager.Instance.GetLocalisedString("w_uncommon");
                    break;
                case (Tier.Rare):
                    tierName = "RARE"; //LocalisationManager.Instance.GetLocalisedString("w_rare");
                    break;
                case (Tier.Epic):
                    tierName = "EPIC"; //LocalisationManager.Instance.GetLocalisedString("w_epic"); ;
                    break;
                case (Tier.Legendary):
                    tierName = "LEGENDARY"; //LocalisationManager.Instance.GetLocalisedString("w_legend"); ;
                    break;
                default:
                    tierName = "COMMON"; //LocalisationManager.Instance.GetLocalisedString("w_common"); ;
                    break;
            }

            effectName =
                augmentTemplates[effect]
                    .name; // LocalisationManager.Instance.GetLocalisedString(augmentTemplates[effect].name).ToUpper();
            return string.Format("{0} {1} AUGMENT", tierName, effectName);
        }

        public bool CraftAugment(AugmentEffect effect, Tier tier, bool instantSave = true)
        {
            if (tier == Tier.Common)
            {
                if (CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) >= 25)
                {
                    CurrencyController.Instance.AddCurrency(-25, CurrencyType.HardCurrency, false, "augments", "craft_augment");
                    inventoryMatrix[effect][tier]++;
                    return true;
                }

                return false;
            }

            if (inventoryMatrix[effect][tier - 1] < 4) 
                return false;
            inventoryMatrix[effect][tier - 1] -= 4;
            inventoryMatrix[effect][tier]++;
            return true;

        }

        public void AddAugment(AugmentEffect effect, Tier tier)
        {
            inventoryMatrix[effect][tier]++;
        }

        public void EquipAugment(Outpost outpost, AugmentEffect effect, Tier tier, int index = 0)
        {
            if (inventoryMatrix[effect][tier] > 0)
            {
                if (outpost.GetAugment(index) != null)
                {
                    Augment oldAugment = outpost.GetAugment(index);
                    inventoryMatrix[oldAugment.augmentEffect][oldAugment.augmentTier]++;
                }

                inventoryMatrix[effect][tier]--;
                outpost.SetAugment(index, new Augment(effect, tier));
                //if (MergeManager.showAugments)
                //    slot.RefreshAugments();
            }
        }

        public void RemoveAugment(Outpost outpost, int index = 0)
        {
            if (outpost.GetAugment(index) != null)
            {
                Augment oldAugment = outpost.GetAugment(index);
                inventoryMatrix[oldAugment.augmentEffect][oldAugment.augmentTier]++;
                outpost.RemoveAugment(index);
                //slot.DisableAugment();
                //outpost.RefreshAugments();
            }
        }

        public void ClearAugments(Outpost outpost)
        {
            for (int i = 0; i < outpost.GetAugments().Length; i++)
            {
                RemoveAugment(outpost, i);
            }
        }

        public int GetCount(AugmentEffect effect, Tier tier)
        {
            return inventoryMatrix[effect][tier];
        }

        public bool GetAnyCraftable()
        {
            for (int i = 0; i < Enum.GetValues(typeof(AugmentEffect)).Length; i++)
            {
                for (int j = 1; j < Enum.GetValues(typeof(Tier)).Length; j++)
                {
                    if (GetCanCraft((AugmentEffect) i, (Tier) j))
                        return true;
                }
            }

            return false;
        }

        public void CraftAll()
        {
            for (int i = 0; i < Enum.GetValues(typeof(AugmentEffect)).Length; i++)
            {
                for (int j = 1; j < Enum.GetValues(typeof(Tier)).Length; j++)
                {
                    while (GetCanCraft((AugmentEffect) i, (Tier) j))
                    {
                        CraftAugment((AugmentEffect) i, (Tier) j, false);
                    }
                }
            }
        }

        // Use this for initialization
        void Awake()
        {
            instance = this;
            for (int i = 0; i < allAugments.Length; i++)
            {
                if (!augmentTemplates.ContainsKey(allAugments[i].augmentEffect))
                    augmentTemplates.Add(allAugments[i].augmentEffect, allAugments[i]);
            }

            for (int i = 0; i < backgrounds.Length; i++)
                augmentBackgrounds[backgrounds[i].Tier] = backgrounds[i].Background;
            
            SetupDictionary();
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            string data = "";
            int index = 0;
            foreach (AugmentEffect key in inventoryMatrix.Keys)
            {
                foreach (Tier tier in inventoryMatrix[key].Keys)
                {
                    int count = inventoryMatrix[key][tier];
                    if (count > 0)
                    {
                        if (index > 0)
                            data += "#";
                        data += SaveAugmentCount(key, tier, count);
                        index++;
                    }
                }
            }

            saveData.Add("augmentInv", data);
            //throw new System.NotImplementedException();
        }

        public Sprite GetBackground(Tier tier)
        {
            if (augmentBackgrounds.TryGetValue(tier, out Sprite background))
                return background;
            return null;
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            string data = loadedData.LoadData("augmentInv", "");
            if (string.IsNullOrEmpty(data))
                return;
            string[] splitData = data.Split('#');
            for (int i = 0; i < splitData.Length; i++)
            {
                LoadCount(splitData[i]);
            }
        }

        public void LoadDefault()
        {
            
        }
    }

}