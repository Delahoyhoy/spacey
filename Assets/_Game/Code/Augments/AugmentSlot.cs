﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Augments
{
    public class AugmentSlot : AugmentDisplay
    {

        [SerializeField]
        private Text countText;

        private AugmentSelectionMenu menuRef;

        private AugmentEffect augmentEffect;
        private Tier augmentTier;

        public void Setup(AugmentEffect effect, Tier tier, AugmentSelectionMenu menu)
        {
            Setup(effect, tier);
            augmentEffect = effect;
            augmentTier = tier;
            menuRef = menu;
        }

        public void Select()
        {
            if (menuRef)
                menuRef.SelectAugment(augmentEffect, augmentTier);
        }

        public void SetCount()
        {
            int count = menuRef.augmentManager.GetCount(augmentEffect, augmentTier);
            countText.text = CurrencyController.CurrencyToString(count);
            //Color foregroundColor = foreground.color, backgroundColor = Color.white;
            //if (count > 0)
            //{
            //    foregroundColor.a = 1f;
            //    backgroundColor.a = 1f;
            //    outline.enabled = true;
            //}
            //else
            //{
            //    foregroundColor.a = .5f;
            //    backgroundColor.a = .5f;
            //    outline.enabled = false;
            //}
            //foreground.color = foregroundColor;
            //background.color = backgroundColor;
        }
    }

}