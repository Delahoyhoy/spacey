using System;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    [Serializable]
    public struct RewardRow
    {
        public GameObject rowObject;
        public RewardDisplay[] rewardSlots;
    }
}