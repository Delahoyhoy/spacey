﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Augments
{
    public class AugmentDisplay : MonoBehaviour
    {
        [SerializeField]
        private Image symbol, background;

        [SerializeField]
        private Outline outline;

        public void Setup(AugmentEffect effect, Tier tier)
        {
            symbol.sprite = AugmentManager.Instance.GetSymbol(effect);
            Color tierColour = ColourHelper.Instance.GetTierColour(tier);

            bool hasBackground = background;
            outline.effectColor = tierColour;
            if (hasBackground)
            {
                background.sprite = AugmentManager.Instance.GetBackground(tier);
                symbol.color = Color.white;
                return;
            }
            symbol.color = tierColour;
            
        }
    }
}
