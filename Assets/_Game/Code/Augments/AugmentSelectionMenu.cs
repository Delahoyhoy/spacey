﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Outposts;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Augments
{
    public class AugmentSelectionMenu : UIScreen
    {

        public AugmentManager augmentManager;

        [SerializeField]
        private AugmentList[] augmentLists;

        [SerializeField]
        private List<AugmentSlot> augmentSlots = new List<AugmentSlot>();

        [SerializeField]
        private AugmentSlot slotPrefab;

        [SerializeField]
        private GameObject gemIcon;

        [SerializeField]
        private UIScreen popup, craftPopup;

        [SerializeField]
        private AugmentDisplay popupDisplay, costDisplay;

        [SerializeField]
        private Text popupNameText, popupDescText, popupCostText, popupCountText;

        [SerializeField]
        private Button popupCraftButton, popupAssignButton, noneButton, moreButton, craftAll;

        [SerializeField]
        private OutpostMenu slotMenu;

        private Outpost currentSelection;
        private int currentSlot = 0;
        private bool fromSlot = false;

        private AugmentEffect selectedEffect;
        private Tier selectedTier;

        public void Refresh()
        {
            for (int i = 0; i < augmentSlots.Count; i++)
            {
                augmentSlots[i].SetCount();
            }

            craftAll.interactable = augmentManager.GetAnyCraftable();
        }

        private bool setup = false;

        private void Setup()
        {
            if (!setup)
            {
                for (int i = 0; i < augmentLists.Length; i++)
                {
                    for (int j = 0; j < Enum.GetValues(typeof(Tier)).Length; j++)
                    {
                        AugmentSlot newSlot = Instantiate(slotPrefab, augmentLists[i].transform);
                        newSlot.Setup(augmentLists[i].effect, (Tier) j, this);
                        newSlot.SetCount();
                        augmentSlots.Add(newSlot);
                    }
                }

                //for (int i = 0; i < augmentSlots.Length; i++)
                //{
                //    augmentSlots[i].SetupInPlace(this);
                //    augmentSlots[i].SetCount();
                //}
                craftAll.interactable = augmentManager.GetAnyCraftable();
                setup = true;
            }
            else
                Refresh();
        }

        public void AddAugment(AugmentEffect effect, Tier tier)
        {
            augmentManager.AddAugment(effect, tier);
            Refresh();
        }

        override public void Open()
        {
            base.Open();
            Setup();
            fromSlot = false;
            popupAssignButton.gameObject.SetActive(false);
            noneButton.gameObject.SetActive(false);
            moreButton.gameObject.SetActive(true);
        }

        public void Craft()
        {
            AudioManager.Play("Click", 1f);
            //if (selectedTier == Tier.Common)
            //{
            //    craftPopup.Open();
            //    return;
            //}
            if (augmentManager.CraftAugment(selectedEffect, selectedTier))
            {
                Refresh();
                RefreshPopup();
            }
        }

        public void CraftCommon()
        {
            if (augmentManager.CraftAugment(selectedEffect, Tier.Common))
            {
                Refresh();
                RefreshPopup();
            }
        }

        public void Assign()
        {
            if (currentSelection != null)
            {
                augmentManager.EquipAugment(currentSelection, selectedEffect, selectedTier, currentSlot);
                ClosePopup();
                Close();
                slotMenu.Refresh();
                //MergeManager.Instance.SaveAugments();
            }
        }

        public void EquipNone()
        {
            if (currentSelection != null)
            {
                augmentManager.RemoveAugment(currentSelection, currentSlot);
                Close();
                slotMenu.Refresh();
                //MergeManager.Instance.SaveAugments();
            }
        }

        public void Open(Outpost slot, int index = 0)
        {
            base.Open();
            currentSlot = index;
            Setup();
            fromSlot = true;
            popupAssignButton.gameObject.SetActive(true);
            noneButton.gameObject.SetActive(true);
            moreButton.gameObject.SetActive(false);
            currentSelection = slot;
        }

        public void RefreshPopup()
        {
            popupCountText.text = augmentManager.GetCount(selectedEffect, selectedTier).ToString();
            popupAssignButton.interactable = augmentManager.GetCount(selectedEffect, selectedTier) > 0;
            popupCraftButton.interactable = augmentManager.GetCanCraft(selectedEffect, selectedTier);
        }

        public void SelectAugment(AugmentEffect effect, Tier tier)
        {
            selectedEffect = effect;
            selectedTier = tier;
            //popupObject.SetActive(true);
            popup.Open();
            AudioManager.Play("Click", 1f);
            popupDisplay.Setup(effect, tier);
            if (tier == Tier.Common)
            {
                costDisplay.gameObject.SetActive(false);
                gemIcon.SetActive(true);
                popupCostText.text = "25";
            }
            else
            {
                costDisplay.gameObject.SetActive(true);
                costDisplay.Setup(effect, tier - 1);
                gemIcon.SetActive(false);
                popupCostText.text = "x4";
            }

            popupCountText.text = augmentManager.GetCount(effect, tier).ToString();
            popupAssignButton.interactable = augmentManager.GetCount(effect, tier) > 0;
            popupCraftButton.interactable = augmentManager.GetCanCraft(effect, tier);
            popupDescText.text = augmentManager.GetDescription(effect, tier);
            popupNameText.text = augmentManager.GetFormattedName(effect, tier);
            //popupNameText.color = ColourHelper.Instance.GetTierColour(tier);
        }

        public void ClosePopup()
        {
            //popupObject.SetActive(false);
            popup.Close();
            AudioManager.Play("Click", 1f);
        }
    }

}