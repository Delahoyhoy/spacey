using System;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Augments
{
    [Serializable]
    public struct LuckyBoxTemplate
    {
        public Tier tier;
        public Sprite sprite;
        public int[] guaranteed;

        public int[] format;
        public float[] probabilities; // 0 - Common, 1 - Uncommon, 2 - Rare, 3 - Epic, 4 - Legendary
        
        public int Count
        {
            get
            {
                int total = 0;
                for (int i = 0; i < format.Length; i++) 
                    total += format[i];
                return total;
            }
        }

        public int GetGuaranteed(Tier augmentTier)
        {
            int tierIndex = (int) augmentTier;
            if (tierIndex >= guaranteed.Length)
                return 0;
            return guaranteed[tierIndex];
        }

        public float GetProbability(Tier augmentTier)
        {
            int tierIndex = (int) augmentTier;
            if (tierIndex >= probabilities.Length)
                return 0;
            float totalProbability = 0;
            for (int i = 0; i < probabilities.Length; i++)
                totalProbability += probabilities[i];
            
            return (probabilities[tierIndex] / totalProbability) * 100;
        }
        
        public Tier[] GetTiers()
        {
            int totalCount = Count;
            List<Tier> tiers = new List<Tier>();

            for (int i = 0; i < guaranteed.Length; i++)
            {
                for (int j = 0; j < guaranteed[i]; j++)
                    tiers.Add((Tier)i);
                if (tiers.Count >= totalCount)
                    break;
            }

            for (int i = tiers.Count; i < totalCount; i++)
            {
                tiers.Add(GetRandomTier());
            }

            Tier[] jumbled = new Tier[tiers.Count];
            for (int i = 0; i < jumbled.Length; i++)
            {
                int selectedIndex = Random.Range(0, tiers.Count);
                jumbled[i] = tiers[selectedIndex];
                tiers.RemoveAt(selectedIndex);
            }

            return jumbled;
        }

        public Tier GetRandomTier()
        {
            float maxValue = 0;
            for (int i = 0; i < probabilities.Length; i++)
            {
                maxValue += probabilities[i];
            }

            float value = Random.value * maxValue;
            for (int i = 0; i < probabilities.Length; i++)
            {
                if (value <= probabilities[i])
                    return (Tier) i;
                value -= probabilities[i];
            }

            return Tier.Common; // Fallback - give 'em a common one
        }
    }
}