using System;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    [Serializable]
    public struct AugmentList
    {
        public AugmentEffect effect;
        public RectTransform transform;
    }
}