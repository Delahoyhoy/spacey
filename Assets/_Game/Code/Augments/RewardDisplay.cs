﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    public class RewardDisplay : AugmentDisplay
    {
        public bool canSelect = true;

        private AugmentEffect augmentEffect;
        private Tier augmentTier;

        private LuckyBoxManager controllerRef;

        [SerializeField]
        private Animator augmentAnimator;

        public void ScaleIn()
        {
            gameObject.SetActive(true);
            //augmentAnimator.Play("In");
        }

        public void Setup(AugmentEffect effect, Tier tier, LuckyBoxManager controller)
        {
            gameObject.SetActive(false);
            augmentEffect = effect;
            augmentTier = tier;
            Setup(effect, tier);
            controllerRef = controller;
        }

        public void Select()
        {
            if (controllerRef && canSelect)
                controllerRef.OpenPopup(augmentEffect, augmentTier);
        }
    }

}