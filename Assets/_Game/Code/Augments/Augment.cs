using System;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    [Serializable]
    public class Augment
    {
        public AugmentEffect augmentEffect;
        public Tier augmentTier;

        public Augment(AugmentEffect effect, Tier tier)
        {
            augmentEffect = effect;
            augmentTier = tier;
        }

        public Augment(string data)
        {
            string[] splitData = data.Split('~');
            if (int.TryParse(splitData[0], out int effect)) 
                augmentEffect = (AugmentEffect) effect;
            if (int.TryParse(splitData[1], out int tier))
                augmentTier = (Tier) tier;
        }

        public string SaveAugment()
        {
            return (int) augmentEffect + "~" + (int) augmentTier;
        }
    }
}