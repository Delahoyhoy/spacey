using System;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    [Serializable]
    public struct AugmentMenuSlot
    {
        public GameObject slotHolder;
        public AugmentDisplay display;
    }
}