﻿using System;
using Core.Gameplay.Asteroids;
using UnityEngine;

namespace Core.Gameplay.Augments
{
    [Serializable]
    public class AugmentTierImage
    {
        [SerializeField]
        private Tier tier;

        public Tier Tier => tier;
        
        [SerializeField]
        private Sprite background;

        public Sprite Background => background;
    }
}