﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Save;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

//using Firebase.Analytics;

namespace Core.Gameplay.Augments
{
    public class LuckyBoxManager : MonoBehaviour, ISaveLoad
    {

        public AugmentManager augmentManager;

        //[SerializeField]
        //private AugmentSelectionMenu augmentScreen;

        [SerializeField]
        private LuckyBoxTemplate[] boxTemplates;

        [SerializeField]
        private RewardRow[] rows;

        [SerializeField]
        private GameObject rowsDisplay, doneButton, tapToOpen;

        [SerializeField]
        private float revealDelay = 0.3f;

        private static LuckyBoxManager instance;

        public static LuckyBoxManager Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<LuckyBoxManager>();
                return instance;
            }
        }

        private Queue<Tier> boxQueue = new Queue<Tier>();

        private Dictionary<Tier, LuckyBoxTemplate> boxDictionary = new Dictionary<Tier, LuckyBoxTemplate>();

        // Popup stuff
        [SerializeField]
        private GameObject menuObject;

        [SerializeField]
        private UIScreen popup;

        [SerializeField]
        private AugmentDisplay popupDisplay;

        [SerializeField]
        private Text popupNameText, popupDescText;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private Image boxImage;

        private List<Augment> augments = new List<Augment>();

        private Tier currentTier;

        // Use this for initialization
        void Awake()
        {
            for (int i = 0; i < boxTemplates.Length; i++)
            {
                if (!boxDictionary.ContainsKey(boxTemplates[i].tier))
                    boxDictionary.Add(boxTemplates[i].tier, boxTemplates[i]);
            }

            instance = this;
//        LoadBoxQueue(SaveManager.GetString("boxQueue", ""));
        }

        public bool TryGetBoxTemplate(Tier boxTier, out LuckyBoxTemplate template)
        {
            return boxDictionary.TryGetValue(boxTier, out template);
        }

        private string SaveBoxQueue()
        {
            if (boxQueue.Count <= 0)
                return "";
            string data = "";
            Tier[] queueToArray = boxQueue.ToArray();
            for (int i = 0; i < queueToArray.Length; i++)
            {
                if (i > 0)
                    data += ",";
                data += ((int) queueToArray[i]).ToString();
            }

            return data;
        }

        public Sprite GetSprite(Tier tier)
        {
            if (boxDictionary.ContainsKey(tier))
            {
                return boxDictionary[tier].sprite;
            }

            return null;
        }

        private void LoadBoxQueue(string data)
        {
            if (string.IsNullOrEmpty(data))
                return;
            string[] splitData = data.Split(',');
            for (int i = 0; i < splitData.Length; i++)
            {
                int tierIndex = 0;
                if (int.TryParse(splitData[i], out tierIndex))
                {
                    Tier tier = (Tier) tierIndex;
                    GetAugments(tier);
                }
            }
        }

        public List<RewardDisplay> Setup(int[] format, List<Augment> augments)
        {
            int index = 0;
            List<RewardDisplay> slotsUsed = new List<RewardDisplay>();
            for (int i = 0; i < rows.Length; i++)
            {
                if (i < format.Length)
                {
                    rows[i].rowObject.SetActive(true);
                    for (int j = 0; j < rows[i].rewardSlots.Length; j++)
                    {
                        if (j < format[i])
                        {
                            if (index < augments.Count)
                            {
                                rows[i].rewardSlots[j].transform.parent.gameObject.SetActive(true);
                                rows[i].rewardSlots[j].Setup(augments[index].augmentEffect, augments[index].augmentTier,
                                    this); //, this);
                                slotsUsed.Add(rows[i].rewardSlots[j]);
                                index++;
                            }
                            else
                                rows[i].rewardSlots[j].transform.parent.gameObject.SetActive(false);
                        }
                        else
                            rows[i].rewardSlots[j].transform.parent.gameObject.SetActive(false);
                    }
                }
                else
                    rows[i].rowObject.SetActive(false);
            }

            return slotsUsed;
        }

        public void OpenMenu(Tier tier)
        {
            if (!AudioManager.Instance.Muted)
                AudioManager.Fade(1.5f);
            AudioManager.Play("Milestone", 0.5f);
            rowsDisplay.SetActive(false);
            //popupObject.SetActive(false);
            popup.CloseInstant();
            menuObject.SetActive(true);
            //menuObject.transform.SetAsLastSibling();
            UIManager.Instance.SetBlockClose(true);
            tapToOpen.SetActive(false);
            doneButton.SetActive(false);
            animator.Play("LuckyBoxIn");
            boxImage.sprite = boxDictionary[tier].sprite;
            currentTier = tier;
            augments = GetAugments(tier);
            StartCoroutine(WaitEnableTap());
        }

        public void PressDone()
        {
            if (boxQueue.Count <= 0)
                CloseMenu();
            else
            {
                OpenMenu(boxQueue.Dequeue());
                //SaveManager.SetString("boxQueue", SaveBoxQueue());
            }
        }

        public void Setup(string tierIndices)
        {
            string[] splitTiers = tierIndices.Split(',');
            for (int i = 0; i < splitTiers.Length; i++)
            {
                int index = 0;
                if (int.TryParse(splitTiers[i], out index))
                {
                    boxQueue.Enqueue((Tier) index);
                }
            }

            OpenMenu(boxQueue.Dequeue());
            //SaveManager.SetString("boxQueue", SaveBoxQueue());
        }

        public void OpenMenu(int tier)
        {
            OpenMenu((Tier) tier);
        }

        IEnumerator WaitEnableTap()
        {
            yield return new WaitForSeconds(1.5f);
            tapToOpen.SetActive(true);
        }

        public void OpenPopup(AugmentEffect effect, Tier tier)
        {
            //popupObject.SetActive(true);
            popup.Open();
            //AudioManager.Play("Click", 1f);
            popupDisplay.Setup(effect, tier); //, this, false);
            popupDescText.text = augmentManager.GetDescription(effect, tier);
            popupNameText.text = augmentManager.GetFormattedName(effect, tier);
            popupNameText.color = ColourHelper.Instance.GetTierColour(tier);
        }

        public void DEBUGOpenRandomBox()
        {
            Tier tier = (Tier) (Random.Range(0, Enum.GetValues(typeof(Tier)).Length));
            OpenMenu(tier);
        }

        public void TapOpen()
        {
            tapToOpen.SetActive(false);
            StartCoroutine(OpenBox());
            StatTracker.Instance.IncrementStat("luckyBoxes", 1);
        }

        public void OpenLuckyBox()
        {
            StartCoroutine(OpenBox());
        }

        private IEnumerator OpenBox()
        {
            AudioManager.Play("BoxOpen", 0.75f);
            animator.Play("LuckyBoxOpen");
            int[] format = boxDictionary[currentTier].format;
            //List<Augment> augments = GetAugments(currentTier);
            rowsDisplay.SetActive(true);
            List<RewardDisplay> slots = Setup(format, augments);
            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            /*List<Firebase.Analytics.Parameter> analyticsParameters = new List<Firebase.Analytics.Parameter>();
        analyticsParameters.Add(new Firebase.Analytics.Parameter("BoxTier", currentTier.ToString()));
        AnalyticsManager.Instance.TriggerEvent(AnalyticsEvent.LuckyBox, analyticsParameters);*/
            yield return new WaitForSeconds(0.75f);
            for (int i = 0; i < slots.Count; i++)
            {
                slots[i].ScaleIn();
                yield return new WaitForSeconds(revealDelay);
            }

            for (int i = 0; i < slots.Count; i++)
            {
                slots[i].canSelect = true;
            }

            doneButton.SetActive(true);
        }

        public void ClosePopup()
        {
            //AudioManager.Play("Click", 1f);
            //popupObject.SetActive(false);
            popup.Close();
        }

        public void CloseMenu()
        {
            AudioManager.Play("Close", 1f);
            menuObject.SetActive(false);
            UIManager.Instance.SetBlockClose(false);
        }

        private List<Augment> GetAugments(Tier tier)
        {
            int countToSpawn = boxDictionary[tier].Count;
            List<Augment> augments = new List<Augment>();
            Tier[] tiersToSpawn = boxDictionary[tier].GetTiers();
            for (int i = 0; i < tiersToSpawn.Length; i++)
            {
                Tier tierToSpawn = tiersToSpawn[i];
                Augment augment = new Augment((AugmentEffect) Random.Range(0, 
                        Enum.GetValues(typeof(AugmentEffect)).Length), tierToSpawn);
                augmentManager.AddAugment(augment.augmentEffect, augment.augmentTier);
                augments.Add(augment);
            }
            return augments;
        }

        public void Save(ref Dictionary<string, object> saveData)
        {
            saveData.Add("boxQueue", SaveBoxQueue());
        }

        public void Load(SaveDataWrapper loadedData, ref List<string> idsToRemove)
        {
            LoadBoxQueue(loadedData.LoadData("boxQueue", ""));
        }

        public void LoadDefault()
        {

        }
    }

}