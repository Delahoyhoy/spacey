using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Augments
{
    public class LuckyBoxAugmentTier : MonoBehaviour
    {
        [SerializeField]
        private Tier augmentTier;

        public Tier AugmentTier => augmentTier;
        
        [SerializeField]
        private Text chanceText, minCountText;

        [SerializeField]
        private Image background;

        [SerializeField]
        private GameObject minCountHolder;
        
        public void Setup(float probability, int minCount)
        {
            chanceText.text = $"{probability:##0}%";
            minCountHolder.SetActive(minCount > 0);
            if (minCount > 0)
                minCountText.text = $"{minCount}+";
            background.sprite = AugmentManager.Instance.GetBackground(augmentTier);
        }
    }
}
