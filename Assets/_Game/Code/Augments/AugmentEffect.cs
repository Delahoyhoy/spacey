namespace Core.Gameplay.Augments
{
    public enum AugmentEffect
    {
        MineSpeed,
        FlySpeed,
        Capacity,
        Efficiency,
    }
}