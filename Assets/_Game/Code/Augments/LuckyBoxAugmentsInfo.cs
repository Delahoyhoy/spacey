using System;
using Core.Gameplay.Asteroids;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Augments
{
    public class LuckyBoxAugmentsInfo : MonoBehaviour
    {
        [SerializeField]
        private bool autoSetup;

        [SerializeField] [ConditionalField("autoSetup")]
        private Tier luckyBoxTier;
        
        [SerializeField]
        private LuckyBoxAugmentTier[] augmentTiers;

        [SerializeField]
        private Text countReadout;

        [SerializeField]
        private string countFormat = "x{0}";

        public void Setup(LuckyBoxTemplate template)
        {
            countReadout.text = string.Format(countFormat, template.Count);
            for (int i = 0; i < augmentTiers.Length; i++)
            {
                Tier augmentTier = augmentTiers[i].AugmentTier;
                int guaranteed = template.GetGuaranteed(augmentTier);
                float probability = template.GetProbability(augmentTier);
                augmentTiers[i].Setup(probability, guaranteed);
            }
        }

        private void Start()
        {
            if (autoSetup && LuckyBoxManager.Instance.TryGetBoxTemplate(luckyBoxTier, out LuckyBoxTemplate boxTemplate))
                Setup(boxTemplate);
        }
    }
}