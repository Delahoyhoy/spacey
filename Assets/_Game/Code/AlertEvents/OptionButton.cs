using System;
using UnityEngine.UI;

namespace Core.Gameplay.Alerts
{
    [Serializable]
    public struct OptionButton
    {
        public Button button;
        public Text buttonText;

        public void SetActive(bool active)
        {
            button.gameObject.SetActive(active);
        }

        public bool IsActive => button.gameObject.activeSelf;
        
        public Graphic[] GetGraphics()
        {
            return new[] { button.targetGraphic, buttonText };
        }
    }
}