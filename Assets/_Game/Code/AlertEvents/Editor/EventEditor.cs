using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace Core.Gameplay.Alerts
{
    [CustomEditor(typeof(EventController))]
    public class EventEditor : Editor
    {
        public float progress = 0;

        public override void OnInspectorGUI()
        {
            EventController eventController = (EventController) target;
            if (GUILayout.Button("Test"))
            {
                //eventController.TestProbability();
                EditorUtility.DisplayProgressBar("Calculating", "Calculating probabilities - 0%", 0);
                eventController.CalculateFromEditor(UpdateProgressBar());
            }
            //if (GUILayout.Button("Fail"))
            //{
            //    Undo.RecordObject(eventController, "Added stage");
            //    eventController.AddEventStage(CreateInstance<EventFailStage>());
            //    EditorUtility.SetDirty(eventController);
            //}
            //if (GUILayout.Button("Reward"))
            //{
            //    Undo.RecordObject(eventController, "Added stage");
            //    eventController.AddEventStage(CreateInstance<EventRewardStage>());
            //    EditorUtility.SetDirty(eventController);
            //}
            //if (GUILayout.Button("Challenge"))
            //{
            //    Undo.RecordObject(eventController, "Added stage");
            //    eventController.AddEventStage(CreateInstance<EventChallengeState>());
            //    EditorUtility.SetDirty(eventController);
            //}
            //GUILayout.EndHorizontal();

            GUILayout.Label("Event Stages");
            EventStage[] stages = eventController.GetEventStages();
            EditorGUI.indentLevel++;
            string[] ids = eventController.GetIDs();
            if (stages != null)
            {
                try
                {
                    for (int i = 0; i < stages.Length; i++)
                    {
                        stages[i].OnDisplay(i, Delete, ids);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }

            EditorGUI.indentLevel--;

            GUILayout.Label("Add New Stage");
            //GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add New Stage"))
            {
                Undo.RecordObject(eventController, "Added stage");
                eventController.AddEventStage(new EventStage());
                EditorUtility.SetDirty(eventController);
            }

            base.OnInspectorGUI();
        }

        IEnumerator UpdateProgressBar()
        {
            EventController eventController = (EventController) target;
            EditorUtility.DisplayProgressBar("Calculating", "Calculating probabilities - 0%", 0);
            yield return null;
            while (eventController.progress < 1)
            {
                //Debug.Log("progress: " + Mathf.Round(eventController.progress * 100) + "%");
                EditorUtility.DisplayProgressBar("Calculating",
                    "Calculating probabilities - " + Mathf.Round(eventController.progress * 100) + "%",
                    ((EventController) target).progress);
                yield return null;
            }

            EditorUtility.ClearProgressBar();
        }

        public void Delete(int index)
        {
            EventController eventController = (EventController) target;
            Undo.RecordObject(target, "Deleting Event");
            eventController.DeleteStage(index);
        }
    }
}