﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Augments;
using Core.Gameplay.Outposts;
using Core.Gameplay.Reset;
using Core.Gameplay.Ships;
using Core.Gameplay.Tutorial;
using Core.Gameplay.UserInterface;
using Fumb.RemoteConfig;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Alerts
{
    public class EventController : MonoBehaviour
    {
        public int target = 8;

        //public int[] dice = { 4, 4, 4, 4 };

        [SerializeField]
        public float progress = 0;

        //public int fixedAddition = 0;

        private int[] shipSelection = {-1, -1, -1, -1, -1};

        [SerializeField]
        private EventStage[] eventStages;

        [SerializeField]
        private EventStage[] entryStages;

        private Dictionary<string, EventStage> events = new Dictionary<string, EventStage>();


        // All

        [SerializeField]
        private UIScreen alertScreen, shipSelectionScreen;

        [SerializeField]
        private Text titleText, bodyText;

        [SerializeField]
        private BaseTypewriter bodyWriter;

        [SerializeField]
        private RectTransform rootTransform;

        // Sections

        [SerializeField]
        private GameObject buttonSelection, shipSection, rewardSection, lossesSection, closeSection;

        [SerializeField]
        private OptionButton[] buttonOptions;

        // Ship Selection

        [SerializeField]
        private Image[] shipSelections, screenShipSelections;

        [SerializeField]
        private GameObject graphicHolder;
        
        [SerializeField]
        private Image eventGraphicDisplay;

        [SerializeField]
        private Text successChance, screenSuccessChance;

        [SerializeField]
        private Button deployButton, retreatButton;

        [SerializeField]
        private EventShipSelection shipSelectionPrefab;

        [SerializeField]
        private RectTransform shipSelectionParent;

        private List<EventShipSelection> eventShipSelections = new List<EventShipSelection>();

        // Rewards
        [SerializeField]
        private EventRewardSlot rewardDisplayPrefab;

        [SerializeField]
        private List<EventRewardSlot> activeRewards = new List<EventRewardSlot>();

        [SerializeField]
        private RectTransform rewardDisplayParent;

        // Loss

        [SerializeField]
        private EventShipsLostSlot[] shipsLost;

        [SerializeField]
        private Text salvagedText;

        private IEnumerator calculateCoroutine;

        private List<string> startStages = new List<string>();
        private List<EventReward> eventRewards = new List<EventReward>();

        private double salvage;

        private EventStage currentStage;
        private string currentID;

        private double currentEarningsPerSecond;

        [SerializeField]
        private GameObject alertButton;

        private float countdownToNext = 300;

        private string entry;

        private RemoteDouble softCurrencyRewardMult = new RemoteDouble("alertSCRewardMult", 0.1);

        private void Awake()
        {
            for (int i = 0; i < eventStages.Length; i++)
            {
                if (!events.ContainsKey(eventStages[i].stageID))
                    events.Add(eventStages[i].stageID, eventStages[i]);
                if (eventStages[i].stageType == EventStageType.Default && eventStages[i].isStartStage)
                    startStages.Add(eventStages[i].stageID);
            }
        }

        public void StartRandomEvent()
        {
            if (startStages.Count <= 0)
                return;
            double updatedEarnings = GameManager.Instance.GetTotalGPS(false);
            if (updatedEarnings > currentEarningsPerSecond)
                currentEarningsPerSecond = updatedEarnings;
            alertButton.SetActive(false);
            alertScreen.Open();

            entry = GetEntryStage(); //startStages[Random.Range(0, startStages.Count)];

            SetupStage(entry);
        }

        private string GetEntryStage()
        {
            bool shipsAvailable = CheckReadyToDeploy();
            string id = "";
            float bestValue = 0;
            for (int i = 0; i < startStages.Count; i++)
            {
                if (events[startStages[i]].requireShips && !shipsAvailable)
                    continue;
                float value = Random.value * events[startStages[i]].weight;
                if (value >= bestValue)
                {
                    id = startStages[i];
                    bestValue = value;
                }
            }

            return id;
        }

        private UnityEvent blankEvent = new UnityEvent();

        private void SetupStage(string stageID)
        {
            if (!events.TryGetValue(stageID, out EventStage stage))
                return;
            if (events[stageID].stageType == EventStageType.Branch)
            {
                SetupStage(RandomiseSelection(events[stageID].eventOptions[0].stageIDs));
                return;
            }

            if (stage.isStartStage)
            {
                bool hasGraphic = stage.GetDisplayGraphic(out Sprite graphic);
                graphicHolder.SetActive(hasGraphic);
                if (hasGraphic)
                    eventGraphicDisplay.sprite = graphic;
            }
            
            EnableGraphics();
            currentID = stageID;
            currentStage = events[stageID];
            titleText.text = currentStage.titleText;
            //bodyText.text = currentStage.bodyText;
            bodyWriter.PlayTypeWriter(currentStage.bodyText, () => blankEvent?.Invoke(),  EnableGraphics, false, -0.125f);
            EventStageType stageType = currentStage.stageType;
            buttonSelection.SetActive(stageType.Equals(EventStageType.Default));
            shipSection.SetActive(stageType.Equals(EventStageType.Challenge));
            rewardSection.SetActive(stageType.Equals(EventStageType.Win));
            lossesSection.SetActive(stageType.Equals(EventStageType.Lose));

            if (!stageType.Equals(EventStageType.Default))
            {
                for (int i =0;i < buttonOptions.Length; i++)
                    buttonOptions[i].SetActive(false);
            }
            
            switch (currentStage.stageType)
            {
                case EventStageType.Default:
                    closeSection.SetActive(false);
                    for (int i = 0; i < buttonOptions.Length; i++)
                    {
                        if (i < currentStage.eventOptions.Length)
                        {
                            buttonOptions[i].SetActive(true);
                            buttonOptions[i].buttonText.text = currentStage.eventOptions[i].optionName;
                        }
                        else
                        {
                            buttonOptions[i].SetActive(false);
                        }
                    }

                    break;
                case EventStageType.Challenge:
                    closeSection.SetActive(false);
                    target = Mathf.CeilToInt(GetMaxDeployable() * (Random.Range(0.25f, 0.5f)) * 1 +
                                             ((currentStage.challengeRating) - 1) * .333f);
                    shipSelection = new int[] {-1, -1, -1, -1, -1};
                    screenSuccessChance.text = successChance.text = "SELECT SHIPS TO DEPLOY";
                    RebuildShipSelection();
                    break;
                case EventStageType.Win:
                    closeSection.SetActive(true);
                    BuildRewards(currentStage.eventRewards.ToArray());
                    break;
                case EventStageType.Lose:
                    closeSection.SetActive(true);
                    Dictionary<int, int> losses = GetLosses();
                    BuildLostShips(losses);
                    salvage = CalculateSalvage(losses);
                    salvagedText.text = CurrencyController.FormatToString(salvage);
                    break;
                case EventStageType.Exit:
                    closeSection.SetActive(true);
                    break;
            }

            RebuildLayout();
            StartCoroutine(WaitRebuild());
            GetGraphics();
            DisableGraphics();
        }

        public void Close()
        {
            //TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
            /*
        List<Firebase.Analytics.Parameter> parameters = new List<Firebase.Analytics.Parameter>();
        parameters.Add(new Firebase.Analytics.Parameter("EventStart", entry));
        parameters.Add(new Firebase.Analytics.Parameter("EventEndState", currentStage.stageType.ToString()));
        AnalyticsManager.Instance.TriggerEvent(AnalyticsEvent.CompleteEvent, parameters);
        */
            if (currentStage != null)
            {
                if (currentStage.stageType == EventStageType.Win)
                {
                    Dictionary<RewardType, int> rewardCounts = Award();
                    // foreach (RewardType key in rewardCounts.Keys)
                    // {
                    //     parameters.Add(new Firebase.Analytics.Parameter(key.ToString() + "Rewarded", key == RewardType.Gold ? GameManager.Instance.GetGoldEarned(rewardCounts[key] * 60) : rewardCounts[key]));
                    // }
                }
                else if (currentStage.stageType == EventStageType.Lose)
                    if (salvage > 0)
                        CoinBurster.Instance.AddGoldWithEffect(salvage);
            }

            alertScreen.Close();
        }

        private Dictionary<RewardType, int> Award()
        {
            Dictionary<RewardType, int> totalRewards = new Dictionary<RewardType, int>();
            if (eventRewards.Count <= 0)
                return totalRewards;
            for (int i = 0; i < eventRewards.Count; i++)
            {
                if (!totalRewards.ContainsKey(eventRewards[i].rewardType))
                    totalRewards.Add(eventRewards[i].rewardType, 0);
                totalRewards[eventRewards[i].rewardType] += eventRewards[i].count;
                switch (eventRewards[i].rewardType)
                {
                    case RewardType.Augments:
                        for (int j = 0; j < eventRewards[i].count; j++)
                            AugmentManager.Instance.AddAugment(eventRewards[i].augmentEffect, eventRewards[i].tier);
                        break;
                    case RewardType.Gems:
                        CoinBurster.Instance.AddGemsWithEffect(eventRewards[i].count);
                        break;
                    case RewardType.LuckyBoxes:
                        if (eventRewards[i].count > 1)
                        {
                            int tierIndex = (int) (eventRewards[i].tier);
                            string data = tierIndex.ToString();
                            for (int j = 1; j < eventRewards[i].count; j++)
                            {
                                data += ",";
                                data += tierIndex.ToString();
                            }

                            LuckyBoxManager.Instance.Setup(data);
                        }
                        else
                            LuckyBoxManager.Instance.OpenMenu(eventRewards[i].tier);

                        break;
                    case RewardType.Gold:
                        CoinBurster.Instance.AddGoldWithEffect(currentEarningsPerSecond * softCurrencyRewardMult * (eventRewards[i].count * 60));
                        break;
                }
            }

            return totalRewards;
        }

        private void RebuildLayout()
        {
            LayoutRebuilder.MarkLayoutForRebuild(rootTransform);
        }

        public void SelectOption(int option)
        {
            if (currentStage == null)
                return;
            if (currentStage.stageType != EventStageType.Default || option >= currentStage.eventOptions.Length)
                return;
            EventOption selectedOption = currentStage.eventOptions[option];
            SetupStage(RandomiseSelection(selectedOption.stageIDs));
        }

        private string RandomiseSelection(string[] ids)
        {
            if (ids.Length == 1)
                return ids[0];
            string bestSoFar = ids[0];
            float bestValue = 0;
            for (int i = 0; i < ids.Length; i++)
            {
                if (events.ContainsKey(ids[i]))
                {
                    float value = Random.value * events[ids[i]].weight;
                    if (value > bestValue)
                    {
                        bestValue = value;
                        bestSoFar = ids[i];
                    }
                }
            }

            return bestSoFar;
        }

        private Dictionary<int, int> GetLosses()
        {
            int countDeployed = 0;
            List<int> deployed = new List<int>(), lost = new List<int>();
            for (int i = 0; i < shipSelection.Length; i++)
                if (shipSelection[i] >= 0)
                {
                    deployed.Add(i);
                    countDeployed++;
                }

            int countLost = Mathf.CeilToInt(countDeployed * Random.Range(0.25f, 0.5f));
            if (countDeployed <= 0)
                return new Dictionary<int, int>();
            for (int i = 0; i < countLost; i++)
            {
                if (deployed.Count <= 0)
                    break;
                int indexToRemove = Random.Range(0, deployed.Count);
                //Debug.Log("REMOVING SHIP AT INDEX " + deployed[indexToRemove]);
                //Debug.Log("REMOVING 1 SHIP " + shipSelection[deployed[indexToRemove]]);
                ShipManager.Instance.DestroyShip(shipSelection[deployed[indexToRemove]]);
                lost.Add(shipSelection[deployed[indexToRemove]]);
                deployed.Remove(indexToRemove);
            }

            Dictionary<int, int> shipLosses = new Dictionary<int, int>();
            for (int i = 0; i < lost.Count; i++)
            {
                if (!shipLosses.ContainsKey(lost[i]))
                    shipLosses.Add(lost[i], 0);
                shipLosses[lost[i]]++;
            }

            return shipLosses;
        }

        private void BuildLostShips(Dictionary<int, int> losses)
        {
            int index = 0;
            foreach (int key in losses.Keys)
            {
                shipsLost[index].mainObject.SetActive(true);
                shipsLost[index].shipImage.sprite = ShipManager.Instance.GetUISprite(key);
                shipsLost[index].countText.text = losses[key].ToString();
                index++;
                if (index >= shipsLost.Length)
                    break;
            }

            for (int i = index; i < shipsLost.Length; i++)
                shipsLost[i].mainObject.SetActive(false);
        }

        private int GetShipsDeployed()
        {
            int total = 0;
            for (int i = 0; i < shipSelection.Length; i++)
                if (shipSelection[i] >= 0)
                    total++;
            return total;
        }

        private void SetupButtonsForDeployment()
        {
            int shipsDeployed = GetShipsDeployed();
            if (shipsDeployed > 0)
            {
                retreatButton.gameObject.SetActive(false);
                deployButton.gameObject.SetActive(true);
            }
            else
            {
                retreatButton.gameObject.SetActive(true);
                deployButton.gameObject.SetActive(false);
            }
        }

        public void Retreat()
        {
            if (currentStage != null)
            {
                if (currentStage.stageType == EventStageType.Challenge)
                {
                    SetupStage(currentStage.exitStage);
                }
            }
        }

        public void Deploy()
        {
            if (currentStage != null)
            {
                if (currentStage.stageType == EventStageType.Challenge && GetShipsDeployed() > 0)
                {
                    if (AttemptChallenge())
                        SetupStage(currentStage.winStage);
                    else
                        SetupStage(currentStage.loseStage);
                }
            }
        }

        private bool CheckReadyToDeploy()
        {
            int totalAvailable = 0;
            for (int i = 0; i < ShipManager.Instance.TotalShipTemplates; i++)
            {
                totalAvailable += ShipManager.Instance.GetCurrentShipCount(i);
                if (totalAvailable >= 4)
                    return true;
            }

            return false;
        }

        private double CalculateSalvage(Dictionary<int, int> losses)
        {
            double totalSalvage = 0;
            foreach (int key in losses.Keys)
            {
                totalSalvage += ShipManager.Instance.GetSalvage(key, losses[key]);
            }

            return totalSalvage;
        }

        private void BuildRewards(EventReward[] rewards)
        {
            for (int i = 0; i < activeRewards.Count; i++)
                Destroy(activeRewards[i].gameObject);
            activeRewards.Clear();
            eventRewards.Clear();
            for (int i = 0; i < rewards.Length; i++)
            {
                EventReward reward = new EventReward(rewards[i]);
                EventRewardSlot newReward = Instantiate(rewardDisplayPrefab, rewardDisplayParent);
                switch (reward.rewardType)
                {
                    case RewardType.Augments:
                        AugmentEffect augmentEffect = reward.augmentEffect;
                        if (reward.randomiseAugmentEffect)
                            augmentEffect = (AugmentEffect) Random.Range(0,
                                Enum.GetValues(typeof(AugmentEffect)).Length);
                        newReward.Setup(reward.rewardType, reward.count, currentEarningsPerSecond * softCurrencyRewardMult, reward.tier, augmentEffect);
                        break;
                    case RewardType.LuckyBoxes:
                        newReward.Setup(reward.rewardType, reward.count, currentEarningsPerSecond * softCurrencyRewardMult, reward.tier);
                        break;
                    case RewardType.Gems:
                        newReward.Setup(reward.rewardType, reward.count, currentEarningsPerSecond * softCurrencyRewardMult);
                        break;
                    case RewardType.Gold:
                        newReward.Setup(reward.rewardType, reward.count, currentEarningsPerSecond * softCurrencyRewardMult);
                        break;
                }

                activeRewards.Add(newReward);
                eventRewards.Add(reward);
            }
        }

        private List<Graphic> graphics = new List<Graphic>();

        private void EnableGraphics()
        {
            for (int i = 0; i < graphics.Count; i++)
                if (graphics[i])
                    graphics[i].enabled = true;//CrossFadeAlpha(1f, 0, true);
        }

        private void DisableGraphics()
        {
            for (int i = 0; i < graphics.Count; i++)
                if (graphics[i])
                    graphics[i].enabled = false;//CrossFadeAlpha(0, 0, true);
        }

        private void GetGraphics()
        {
            graphics.Clear();
            // if (buttonSelection.activeSelf)
            //     graphics.AddRange(GetGraphics(buttonSelection));
            for (int i = 0; i < buttonOptions.Length; i++)
            {
                if (buttonOptions[i].IsActive)
                    graphics.AddRange(buttonOptions[i].GetGraphics());
            }
            if (shipSection.activeSelf)
                graphics.AddRange(GetGraphics(shipSection));
            if (rewardSection.activeSelf)
                graphics.AddRange(GetGraphics(rewardSection));
            if (lossesSection.activeSelf)
                graphics.AddRange(GetGraphics(lossesSection));
            if (closeSection.activeSelf)
                graphics.AddRange(GetGraphics(closeSection));
        }

        private Graphic[] GetGraphics(GameObject parent)
        {
            Graphic[] graphicsFromParent = parent.GetComponentsInChildren<Graphic>();
            return graphicsFromParent;
        }

        IEnumerator WaitRebuild()
        {
            yield return null;
            RebuildLayout();
            yield return null; // Double check
            RebuildLayout();
        }

        public string[] GetIDs()
        {
            List<string> ids = new List<string>();
            for (int i = 0; i < eventStages.Length; i++)
            {
                ids.Add(eventStages[i].stageID);
            }

            return ids.ToArray();
        }

        public EventStage[] GetEventStages()
        {
            return eventStages;
        }

        public void AddEventStage(EventStage eventStage)
        {
            List<EventStage> temp = new List<EventStage>(eventStages);
            temp.Add(eventStage);
            eventStages = temp.ToArray();
        }

        private int GetMaxDeployable()
        {
            int total = 0;
            int shipTemplates = ShipManager.Instance.TotalShipTemplates;
            int[] shipCounts = new int[shipTemplates], combatEffects = new int[shipTemplates];
            for (int i = 0; i < shipTemplates; i++)
            {
                shipCounts[i] = ShipManager.Instance.GetCurrentShipCount(i);
                combatEffects[i] = ShipManager.Instance.GetCombatEffect(i);
            }

            for (int i = 0; i < 5; i++)
            {
                int maxIndex = 0, maxValue = -1;
                for (int j = 0; j < shipTemplates; j++)
                {
                    if (shipCounts[j] > 0)
                    {
                        if (combatEffects[j] > maxValue)
                        {
                            maxIndex = j;
                            maxValue = combatEffects[j];
                        }
                    }
                }

                if (maxValue < 0)
                    break;
                total += maxValue;
                shipCounts[maxIndex]--;
            }

            return total;
        }

        // Start is called before the first frame update
        private void Start()
        {
            alertButton.SetActive(false);
            // TESTING
            countdownToNext = Random.Range(45f, 120f);
            //countdownToNext = Random.Range(300f, 600f);

            // string data = "";
            // int index = 0;
            // foreach (EventStage stage in events.Values)
            // {
            //     if (index > 0)
            //         data += "\n";
            //     string bodyText = stage.bodyText;
            //     if (!string.IsNullOrEmpty(bodyText))
            //         bodyText = bodyText.Replace('\n', ' ');
            //     switch (stage.stageType)
            //     {
            //         case EventStageType.Default:
            //             data += stage.stageID;
            //             data += "\t" + stage.titleText;
            //             data += "\t" + bodyText;
            //             for (int i = 0; i < stage.eventOptions.Length; i++)
            //                 data += "\t" + stage.eventOptions[i].optionName;
            //             break;
            //         case EventStageType.Challenge:
            //             continue;
            //         case EventStageType.Win:
            //             data += stage.stageID;
            //             data += "\t" + stage.titleText;
            //             data += "\t" + bodyText;
            //             break;
            //         case EventStageType.Lose:
            //             data += stage.stageID;
            //             data += "\t" + stage.titleText;
            //             data += "\t" + bodyText;
            //             break;
            //         case EventStageType.Exit:
            //             data += stage.stageID;
            //             data += "\t" + stage.titleText;
            //             data += "\t" + bodyText;
            //             break;
            //         case EventStageType.Branch:
            //             continue;
            //     }
            //     index++;
            // }
            //
            // TextEditor te = new TextEditor();
            // te.text = data;
            // te.SelectAll();
            // te.Copy();
        }

        public void TestProbability()
        {

            //Debug.Log(string.Format("Probability of {0} beating {1} is " + (GetProbability(dice, target) * 100) + "%", FormatDice(dice), target));
        }

        int targetSlot = 0;

        public void SelectShip(int slot)
        {
            targetSlot = slot;
            //for (int i = 0; i < ShipManager.Instance.TotalShips; i++)
            //{
            //    if (i >= eventShipSelections.Count)
            //    {
            //        EventShipSelection newSelection = Instantiate(shipSelectionPrefab, shipSelectionParent);
            //        newSelection.Setup(i, SelectShipForSlot, shipSelection);
            //        eventShipSelections.Add(newSelection);
            //    }
            //    else
            //    {
            //        eventShipSelections[i].Setup(i, SelectShipForSlot, shipSelection);
            //    }
            //}
            SetupShipSelection();
            bodyWriter.Stop();
            shipSelectionScreen.Open();
        }

        private void SetupShipSelection()
        {
            for (int i = 0; i < ShipManager.Instance.TotalShips; i++)
            {
                if (i >= eventShipSelections.Count)
                {
                    EventShipSelection newSelection = Instantiate(shipSelectionPrefab, shipSelectionParent);
                    newSelection.Setup(i, SelectNextShip, RemoveShip, shipSelection, ShipsFull);
                    eventShipSelections.Add(newSelection);
                }
                else
                {
                    eventShipSelections[i].Setup(i, SelectNextShip, RemoveShip, shipSelection, ShipsFull);
                }
            }
        }

        private bool ShipsFull
        {
            get
            {
                for (int i = 0; i < shipSelection.Length; i++)
                    if (shipSelection[i] < 0)
                        return false;
                return true;
            }
        }

        private int GetNextFreeSlot()
        {
            for (int i = 0; i < shipSelection.Length; i++)
                if (shipSelection[i] < 0)
                    return i;
            return 0;
        }

        private void SelectNextShip(int shipIndex)
        {
            AudioManager.Play("Click", 0.75f);
            shipSelection[GetNextFreeSlot()] = shipIndex;
            RebuildShipSelection();
            CalculateCurrentProbability();
        }

        private void RemoveShip(int shipIndex)
        {
            AudioManager.Play("Click", 0.75f);
            List<int> shipList = new List<int>(shipSelection);
            if (shipList.Contains(shipIndex))
            {
                shipList.RemoveAt(shipList.LastIndexOf(shipIndex));
                shipList.Add(-1);
            }

            shipSelection = shipList.ToArray();
            RebuildShipSelection();
            CalculateCurrentProbability();
        }

        public void RemoveShipAtIndex(int index)
        {
            AudioManager.Play("Click", 0.75f);
            List<int> shipList = new List<int>(shipSelection);
            if (shipList.Count > index)
            {
                shipList.RemoveAt(index);
                shipList.Add(-1);
            }

            shipSelection = shipList.ToArray();
            RebuildShipSelection();
            CalculateCurrentProbability();
        }

        private int[] GetDice()
        {
            List<int> diceList = new List<int>();
            for (int i = 0; i < shipSelection.Length; i++)
            {
                if (shipSelection[i] > 0)
                    diceList.Add(ShipManager.Instance.GetCombatEffect(shipSelection[i]));
            }

            return diceList.ToArray();
        }

        public void DeleteStage(int stage)
        {
            List<EventStage> stages = new List<EventStage>(eventStages);
            if (stage >= eventStages.Length)
                return;
            Debug.Log("Deleting " + stage);
            stages.RemoveAt(stage);
            eventStages = stages.ToArray();
        }

        //public string FormatDice(int[] dice)
        //{
        //    Dictionary<int, int> diceCounts = new Dictionary<int, int>();
        //    for (int i = 0; i < dice.Length; i++)
        //    {
        //        if (!diceCounts.ContainsKey(dice[i]))
        //            diceCounts.Add(dice[i], 0);
        //        diceCounts[dice[i]]++;
        //    }
        //    List<string> outputs = new List<string>();
        //    foreach (int die in diceCounts.Keys)
        //    {
        //        outputs.Add(string.Format("{0}d{1}", diceCounts[die], die));
        //    }
        //    string output = "";
        //    for (int i = 0; i < outputs.Count; i++)
        //    {
        //        if (i > 0)
        //            output += " + ";
        //        output += outputs[i];
        //    }
        //    if (fixedAddition > 0)
        //        output += " + " + fixedAddition;
        //    return output;
        //}

        public void CalculateFromEditor(IEnumerator progressBar)
        {
            IEnumerator coroutine = Calculate(new int[] {4, 4, 4, 4});
            while (coroutine.MoveNext())
            {
                progressBar.MoveNext();
            }

            while (progressBar.MoveNext()) ;
            Debug.Log("DONE!");
        }

        public void SelectShipForSlot(int ship)
        {
            shipSelection[targetSlot] = ship;
            RebuildShipSelection();
            CalculateCurrentProbability();
            shipSelectionScreen.Close();
        }

        private void RebuildShipSelection()
        {
            for (int i = 0; i < shipSelections.Length; i++)
            {
                shipSelections[i].gameObject.SetActive(true);
                screenShipSelections[i].gameObject.SetActive(true);
                if (shipSelection[i] >= 0)
                {
                    // shipSelections[i].gameObject.SetActive(true);
                    screenShipSelections[i].sprite =
                        shipSelections[i].sprite = ShipManager.Instance.GetUISprite(shipSelection[i]);
                    screenShipSelections[i].color = shipSelections[i].color = Color.white;
                }
                else
                {
                    screenShipSelections[i].sprite = shipSelections[i].sprite = ShipManager.Instance.GetUISprite(0);
                    screenShipSelections[i].color = shipSelections[i].color = new Color(0, 0, 0, 0.33f);
                    //shipSelections[i].gameObject.SetActive(false);
                }
            }

            SetupShipSelection();
        }

        private bool AttemptChallenge()
        {
            List<int> dice = new List<int>();
            for (int i = 0; i < shipSelection.Length; i++)
            {
                if (shipSelection[i] >= 0)
                {
                    dice.Add(ShipManager.Instance.GetCombatEffect(shipSelection[i]));
                }
            }

            int total = 0;
            for (int i = 0; i < dice.Count; i++)
            {
                total += Random.Range(0, dice[i]) + 1;
            }

            return total >= target;
        }

        IEnumerator Calculate(int[] dice)
        {
            string diceReadout = "";
            for (int i = 0; i < dice.Length; i++)
            {
                if (i > 0)
                    diceReadout += ",";
                diceReadout += dice[i];
            }

            Debug.Log("Dice = " + diceReadout + " Target = " + target);
            float interval = (1f / Application.targetFrameRate);
            screenSuccessChance.text = successChance.text = "CALCULATING PROBABILITY...";
            int total = 0, countGreaterOrEqual = 0;
            List<int[]> rollableTable = GenerateRollableTable(dice);
            int[] indexing = new int[dice.Length];
            bool complete = false;
            int totalToDo = 1;
            for (int i = 0; i < rollableTable.Count; i++)
                totalToDo *= rollableTable[i].Length;
            DateTime lastUpdate = DateTime.Now;
            progress = 0;
            int minValue = dice.Length, maxValue = 0;
            for (int i = 0; i < dice.Length; i++)
                maxValue += dice[i];
            if (maxValue < target)
            {
                progress = 1;
                //Debug.Log(string.Format("Probability of {0} beating {1} is 0%", FormatDice(dice), target));
                screenSuccessChance.text = successChance.text = string.Format("PROBABILITY OF VICTORY: {0}%", 0);
                yield break;
            }

            if (target <= minValue)
            {
                progress = 1;
                screenSuccessChance.text = screenSuccessChance.text =
                    successChance.text = string.Format("PROBABILITY OF VICTORY: {0}%", 100);
                //Debug.Log(string.Format("Probability of {0} beating {1} is 100%", FormatDice(dice), target));
                yield break;
            }

            //Debug.Log("STARTED with " + totalToDo + " to do");
            while (!complete)
            {
                int value = 0; //  fixedAddition;
                for (int i = 0; i < dice.Length; i++)
                    value += rollableTable[i][indexing[i]];
                total++;
                if (value >= target)
                    countGreaterOrEqual++;
                complete = IncrementIndexer(ref indexing, dice);
                progress = (float) total / totalToDo;
                if ((DateTime.Now - lastUpdate).TotalSeconds > interval)
                {
                    lastUpdate = DateTime.Now;

                    yield return null;
                }
            }

            screenSuccessChance.text = successChance.text = string.Format("PROBABILITY OF VICTORY: {0}%",
                Mathf.Round(((float) countGreaterOrEqual / total) * 100));
            //Debug.Log(string.Format("Testing probability of {0} beating {1} is " + (((float)(target - minValue) / (maxValue - minValue)) * 100) + "%", FormatDice(dice), target));
            //EditorUtility.ClearProgressBar();
            progress = 1;
            //Debug.Log(string.Format("Probability of {0} beating {1} is " + (((float)countGreaterOrEqual / total) * 100) + "%", FormatDice(dice), target));
        }

        private void CalculateCurrentProbability()
        {
            List<int> dice = new List<int>();
            for (int i = 0; i < shipSelection.Length; i++)
            {
                if (shipSelection[i] >= 0)
                {
                    dice.Add(ShipManager.Instance.GetCombatEffect(shipSelection[i]));
                }
            }

            if (calculateCoroutine != null)
                StopCoroutine(calculateCoroutine);
            calculateCoroutine = Calculate(dice.ToArray());
            StartCoroutine(calculateCoroutine);
        }

        private float GetProbability(int[] dice, int target)
        {
            int total = 0, countGreaterOrEqual = 0;
            List<int[]> rollableTable = GenerateRollableTable(dice);
            int[] indexing = new int[dice.Length];
            bool complete = false;
            while (!complete)
            {
                int value = 0;
                for (int i = 0; i < dice.Length; i++)
                    value += rollableTable[i][indexing[i]];
                total++;
                if (value >= target)
                    countGreaterOrEqual++;
                complete = IncrementIndexer(ref indexing, dice);
            }

            //Debug.Log("Total Combinations = " + total + ", Successful Combinations = " + countGreaterOrEqual);
            return (float) countGreaterOrEqual / total;
        }

        public bool IncrementIndexer(ref int[] index, int[] dice)
        {
            for (int i = 0; i < index.Length; i++)
            {
                index[i]++;
                if (index[i] >= dice[i])
                {
                    index[i] = 0;
                    if (i >= index.Length - 1)
                        return true;
                }
                else
                    return false;
            }

            return false;
        }

        public List<int[]> GenerateRollableTable(int[] dice)
        {
            List<int[]> output = new List<int[]>();
            for (int i = 0; i < dice.Length; i++)
            {
                int[] newArray = new int[dice[i]];
                for (int j = 0; j < newArray.Length; j++)
                    newArray[j] = j + 1;
                output.Add(newArray);
            }

            return output;
        }

        public void ClearAlert()
        {
            countdownToNext = Random.Range(120f, 180f);
            alertButton.SetActive(false);
        }

        // Update is called once per frame
        private void Update()
        {
            if (!alertScreen.IsOpen && FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) && !alertButton.activeSelf &&
                !ResetManager.Instance.resetting && OutpostManager.Instance.GetOutpostCount(true) > 0)
            {
                countdownToNext -= Time.deltaTime;
                if (countdownToNext <= 0)
                {
                    countdownToNext = Random.Range(120f, 180f);
                    currentEarningsPerSecond = GameManager.Instance.GetTotalGPS(false); 
                    if (currentEarningsPerSecond <= 0)
                        return;
                    alertButton.SetActive(true);
                    AudioManager.Play("Radar");
                }
            }

            // if (Input.GetKeyDown(KeyCode.I))
            // {
            //     StartRandomEvent();
            // }
        }
    }

}