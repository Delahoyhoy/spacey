using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Gameplay.Alerts
{
    [Serializable]
    public class EventOption
    {
        public string optionName;
        public string[] stageIDs;

        [HideInInspector]
        [SerializeField]
        private bool show;

#if UNITY_EDITOR
        public virtual void OnGUI(int index, string[] ids)
        {
            show = EditorGUILayout.Foldout(show, string.IsNullOrEmpty(optionName) ? "UNTITLED" : optionName);

            if (show)
            {
                optionName = EditorGUILayout.TextField("Name", optionName);
                EditorGUILayout.Separator();
                List<string> idList = new List<string>(ids);
                idList.Insert(0, "--PLEASE SELECT--");
                if (stageIDs != null)
                {
                    for (int i = 0; i < stageIDs.Length; i++)
                    {
                        EditorGUILayout.BeginHorizontal();
                        GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Stage #" + i);
                        if (idList.Contains(stageIDs[i]))
                        {
                            int idIndex = idList.IndexOf(stageIDs[i]);
                            int newIndex = EditorGUILayout.Popup(idIndex, idList.ToArray());
                            if (idIndex != newIndex)
                            {
                                stageIDs[i] = idList[newIndex];
                            }
                        }
                        else
                        {
                            int idIndex = EditorGUILayout.Popup(0, idList.ToArray());
                            stageIDs[i] = idList[idIndex];
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                    //stageIDs[i] = EditorGUILayout.TextField("Stage #" + i, stageIDs[i]);
                }

                if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Add Stage"))
                    AddStageID();
            }
        }

        private void AddStageID()
        {
            if (stageIDs == null)
            {
                stageIDs = new string[] {""};
                return;
            }

            List<string> temp = new List<string>(stageIDs);
            temp.Add("");
            stageIDs = temp.ToArray();
        }
#endif
    }
}