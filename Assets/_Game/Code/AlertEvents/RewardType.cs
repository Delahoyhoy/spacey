namespace Core.Gameplay.Alerts
{
    public enum RewardType
    {
        Gems,
        Augments,
        LuckyBoxes,
        Gold,
    }
}