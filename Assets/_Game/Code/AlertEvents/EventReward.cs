using System;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Augments;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Core.Gameplay.Alerts
{
    [Serializable]
    public class EventReward
    {
        public EventReward(RewardType type, int totalCount, Tier itemTier = Tier.Common,
            AugmentEffect effect = AugmentEffect.Capacity)
        {
            rewardType = type;
            count = totalCount;
            tier = itemTier;
            augmentEffect = effect;
        }

        public EventReward()
        {
            rewardType = RewardType.Gems;
            count = 10;
            tier = Tier.Common;
            augmentEffect = AugmentEffect.Capacity;
        }

        public EventReward(EventReward template)
        {
            rewardType = template.rewardType;
            count = template.count;
            tier = template.tier;
            if (template.randomiseAugmentEffect)
                augmentEffect = (AugmentEffect) Random.Range(0, Enum.GetValues(typeof(AugmentEffect)).Length);
            else
                augmentEffect = template.augmentEffect;
            randomiseAugmentEffect = false;
        }

        public RewardType rewardType;
        public int count;
        public Tier tier;
        public AugmentEffect augmentEffect;
        public bool randomiseAugmentEffect;

        [HideInInspector]
        [SerializeField]
        private bool show;
#if UNITY_EDITOR
        public void OnGUI(int index, UnityAction<int> deleteCallback)
        {
            show = EditorGUILayout.Foldout(show, "REWARD #" + index);

            if (show)
            {
                int indentLevel = EditorGUI.indentLevel;
                EditorGUI.indentLevel = indentLevel + 8;
                if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "DELETE"))
                    deleteCallback(index);
                EditorGUI.indentLevel = indentLevel;
                rewardType =
                    (RewardType) EditorGUILayout.Popup((int) rewardType, Enum.GetNames(typeof(RewardType)));
                count = EditorGUILayout.IntField("Count", count);
                switch (rewardType)
                {
                    case RewardType.Augments:
                        randomiseAugmentEffect = EditorGUILayout.Toggle("Randomise Effect", randomiseAugmentEffect);
                        if (!randomiseAugmentEffect)
                            augmentEffect = (AugmentEffect) EditorGUILayout.Popup((int) augmentEffect,
                                Enum.GetNames(typeof(AugmentEffect)));
                        tier = (Tier) EditorGUILayout.Popup((int) tier, Enum.GetNames(typeof(Tier)));
                        break;
                    case RewardType.LuckyBoxes:
                        tier = (Tier) EditorGUILayout.Popup((int) tier, Enum.GetNames(typeof(Tier)));
                        break;
                }
            }
        }
#endif
    }
}