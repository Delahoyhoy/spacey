namespace Core.Gameplay.Alerts
{
    public enum EventStageType
    {
        Default,
        Challenge,
        Win,
        Lose,
        Exit,
        Branch,
    }
}