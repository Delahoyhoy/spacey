using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Core.Gameplay.Alerts
{
    [Serializable]
    public class EventStage
    {
        public string stageID;
        public float weight;
        public string titleText, bodyText;

        [HideInInspector] [SerializeField]
        protected bool show;

        public EventOption[] eventOptions;
        public string winStage, loseStage, exitStage;
        public int challengeRating;

        public bool isStartStage, requireShips;

        public EventStageType stageType;

        public List<EventReward> eventRewards = new List<EventReward>();

        [SerializeField]
        private bool hasDisplayGraphic;

        [SerializeField]
        private Sprite displayGraphic;

        public bool GetDisplayGraphic(out Sprite graphic)
        {
            graphic = displayGraphic;
            return isStartStage && hasDisplayGraphic;
        }

# if UNITY_EDITOR
        public virtual void OnDisplay(int index, UnityAction<int> deleteCallback, string[] stageIDs)
        {
            show = EditorGUILayout.Foldout(show,
                (string.IsNullOrEmpty(stageID) ? "UNTITLED" : stageID) + "  --  " + GetStageType());

            if (show)
            {
                int indentLevel = EditorGUI.indentLevel;
                EditorGUI.indentLevel = indentLevel + 15;
                if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "DELETE"))
                    deleteCallback(index);
                EditorGUI.indentLevel = indentLevel;
                if (stageType == EventStageType.Default)
                {
                    EditorGUILayout.BeginHorizontal();
                    GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), GetStageType());
                    isStartStage = EditorGUILayout.Toggle("Event Start", isStartStage);
                    EditorGUILayout.EndHorizontal();
                    if (isStartStage)
                        requireShips = EditorGUILayout.Toggle("Require Ships", requireShips);
                }
                else
                    GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), GetStageType());

                GuiLine();
                EditorGUILayout.BeginHorizontal();
                GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Select Type");
                stageType = (EventStageType) EditorGUILayout.Popup((int) stageType,
                    Enum.GetNames(typeof(EventStageType)));
                EditorGUILayout.EndHorizontal();
                stageID = EditorGUILayout.TextField("Stage ID", stageID);
                weight = EditorGUILayout.FloatField("Weight", weight);
                titleText = EditorGUILayout.TextField("Stage Title", titleText);
                GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Body Text");
                bodyText = EditorGUILayout.TextArea(bodyText);
                GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Button Options");
                GuiLine();

                switch (stageType)
                {
                    case EventStageType.Default:
                        if (eventOptions != null)
                        {
                            EditorGUI.indentLevel++;
                            for (int i = 0; i < eventOptions.Length; i++)
                            {
                                eventOptions[i].OnGUI(i, stageIDs);
                            }

                            EditorGUI.indentLevel--;
                        }

                        if (isStartStage)
                        {
                            hasDisplayGraphic = GUI.Toggle(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()),
                                hasDisplayGraphic, "Has Display Graphic");
                            if (hasDisplayGraphic)
                                displayGraphic = EditorGUILayout.ObjectField(displayGraphic, typeof(Sprite)) as Sprite;
                            
                        }

                        if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Add Option"))
                            AddOption();
                        break;
                    case EventStageType.Challenge:
                        GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Challenge Rating");
                        challengeRating = EditorGUILayout.IntSlider(challengeRating, 1, 4);
                        List<string> idList = new List<string>(stageIDs);
                        idList.Insert(0, "--PLEASE SELECT--");
                        winStage = idList[SelectID("WIN", winStage, idList)];
                        loseStage = idList[SelectID("LOSE", loseStage, idList)];
                        exitStage = idList[SelectID("EXIT", exitStage, idList)];
                        break;
                    case EventStageType.Win:
                        if (eventRewards != null)
                        {
                            for (int i = 0; i < eventRewards.Count; i++)
                            {
                                eventRewards[i].OnGUI(i, DeleteReward);
                            }
                        }

                        if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Add Reward"))
                            eventRewards.Add(new EventReward());
                        break;
                    case EventStageType.Branch:
                        if (eventOptions != null)
                        {
                            EditorGUI.indentLevel++;
                            for (int i = 0; i < eventOptions.Length; i++)
                            {
                                eventOptions[i].OnGUI(i, stageIDs);
                            }

                            EditorGUI.indentLevel--;
                        }

                        if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Add Option"))
                            AddOption();
                        break;
                }

                GuiLine();
            }
        }

        public void DeleteReward(int index)
        {
            if (index < eventRewards.Count)
                eventRewards.RemoveAt(index);
        }

        private int SelectID(string name, string currentValue, List<string> idList)
        {
            EditorGUILayout.BeginHorizontal();
            GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), name);
            int index = 0;
            if (idList.Contains(currentValue))
            {
                int idIndex = idList.IndexOf(currentValue);
                index = EditorGUILayout.Popup(idIndex, idList.ToArray());
                //if (idIndex != newIndex)
                //{
                //    currentValue = idList[newIndex];
                //}
            }
            else
            {
                index = EditorGUILayout.Popup(0, idList.ToArray());
                //currentValue = idList[idIndex];
            }

            EditorGUILayout.EndHorizontal();
            return index;
        }

        public static void GuiLine(int i_height = 1)
        {
            Rect rect = EditorGUI.IndentedRect(EditorGUILayout.GetControlRect(false, i_height));
            rect.height = i_height;
            EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
        }


        public void AddOption()
        {
            List<EventOption> temp;
            if (eventOptions == null)
            {
                temp = new List<EventOption>();
                temp.Add(new EventOption());
                eventOptions = temp.ToArray();
                return;
            }

            temp = new List<EventOption>(eventOptions);
            temp.Add(new EventOption());
            eventOptions = temp.ToArray();
        }

        protected string GetStageType()
        {
            switch (stageType)
            {
                case EventStageType.Default:
                    if (isStartStage)
                        return "ENTRY";
                    return "STANDARD";
                case EventStageType.Challenge:
                    return "CHALLENGE";
                case EventStageType.Win:
                    return "WIN";
                case EventStageType.Lose:
                    return "LOSE";
                case EventStageType.Exit:
                    return "EXIT";
                case EventStageType.Branch:
                    return "BRANCH";
            }

            return "STANDARD";
        }
#endif
    }

    #region Depricated Events

    //[System.Serializable]
    //public class EventFailStage : EventStage
    //{
    //#if UNITY_EDITOR

    //    protected override string GetStageType()
    //    {
    //        return "FAIL";
    //    }

    //#endif
    //}

    //[System.Serializable]
    //public class EventRewardStage : EventStage
    //{
    //    public EventReward[] eventRewards;

    //#if UNITY_EDITOR

    //    public override void OnDisplay(int index, UnityAction<int> deleteCallback, string[] stageIDs)
    //    {
    //        base.OnDisplay(index, deleteCallback, stageIDs);
    //    }

    //    protected override string GetStageType()
    //    {
    //        return "REWARD";
    //    }

    //#endif
    //}

    //[System.Serializable]
    //public class EventChallengeState : EventStage
    //{
    //    public int eventChallenge;
    //    public int maxShips = 5;

    //#if UNITY_EDITOR

    //    public override void OnDisplay(int index, UnityAction<int> deleteCallback, string[] stageIDs)
    //    {
    //        base.OnDisplay(index, deleteCallback, stageIDs);
    //        if (show)
    //        {
    //            GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Challengs Options");
    //            GuiLine();
    //            GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Max Ships");
    //            maxShips = EditorGUILayout.IntSlider(maxShips, 1, 5);
    //            GUI.Label(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Challenge Level");
    //            eventChallenge = EditorGUILayout.IntSlider(eventChallenge, 1, 4);
    //        }
    //    }

    //    protected override string GetStageType()
    //    {
    //        return "CHALLENGE";
    //    }

    //#endif
    //}

    //public enum EventStageType
    //{
    //    Standard,
    //    Challenge,
    //    Fail,
    //    Reward,
    //}

    #endregion
}