﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Ships;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


namespace Core.Gameplay.Alerts
{
    public class EventShipSelection : MonoBehaviour
    {
        private int shipIndex;

        [SerializeField]
        private Image shipImage;

        [SerializeField]
        private Text shipName, shipCount, shipCE;

        [SerializeField]
        private GameObject lockedPanel;

        [SerializeField]
        private Button button, addButton, removeButton;

        private bool setup = false;

        private UnityAction<int> selectCallback, removeCallback;

        public void Setup(int index, UnityAction<int> onSelect, UnityAction<int> onRemove, int[] currentSelection,
            bool full = false)
        {
            shipIndex = index;
            Ship ship = ShipManager.Instance.GetShip(index);
            if (!setup)
            {
                shipImage.sprite = ShipManager.Instance.GetUISprite(index);
                shipName.text = ship.shipName;
            }

            int currentCount = 0;
            for (int i = 0; i < currentSelection.Length; i++)
            {
                if (currentSelection[i] == index)
                    currentCount++;
            }

            removeButton.interactable = currentCount > 0;
            int shipsAvailable = ShipManager.Instance.GetCurrentShipCount(index) - currentCount;
            addButton.interactable = !full && shipsAvailable > 0;
            shipCount.text = CurrencyController.FormatToString(shipsAvailable);
            shipCE.text = "COMBAT EFFECT: " +
                          CurrencyController.FormatToString(ShipManager.Instance.GetCombatEffect(index));
            lockedPanel.SetActive(ship.shipLevel < 0);
            button.interactable = ship.shipLevel >= 0 && ship.currentCount - currentCount > 0;
            selectCallback = onSelect;
            removeCallback = onRemove;
        }

        public void Select()
        {
            if (selectCallback != null)
                selectCallback(shipIndex);
        }

        public void Add()
        {
            if (selectCallback != null)
                selectCallback(shipIndex);
        }

        public void Remove()
        {
            if (removeCallback != null)
                removeCallback(shipIndex);
        }
    }

}