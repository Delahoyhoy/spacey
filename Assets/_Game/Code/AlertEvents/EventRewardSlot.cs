﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Augments;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Alerts
{
    public class EventRewardSlot : MonoBehaviour
    {
        [SerializeField]
        private AugmentDisplay augment;

        [SerializeField]
        private Sprite gemSprite, goldSprite;

        [SerializeField]
        private Image icon;

        [SerializeField]
        private Text countText;

        public void Setup(RewardType type, int count, double earningsPerSecond, Tier quality = Tier.Common,
            AugmentEffect effect = AugmentEffect.MineSpeed)
        {
            switch (type)
            {
                case RewardType.Augments:
                    augment.gameObject.SetActive(true);
                    icon.gameObject.SetActive(false);
                    augment.Setup(effect, quality);
                    countText.text = CurrencyController.FormatToString(count);
                    break;
                case RewardType.Gems:
                    augment.gameObject.SetActive(false);
                    icon.gameObject.SetActive(true);
                    icon.sprite = gemSprite;
                    countText.text = CurrencyController.FormatToString(count);
                    break;
                case RewardType.LuckyBoxes:
                    augment.gameObject.SetActive(false);
                    icon.gameObject.SetActive(true);
                    icon.sprite = LuckyBoxManager.Instance.GetSprite(quality);
                    countText.text = CurrencyController.FormatToString(count);
                    break;
                case RewardType.Gold:
                    augment.gameObject.SetActive(false);
                    icon.gameObject.SetActive(true);
                    icon.sprite = goldSprite;
                    countText.text = CurrencyController.FormatToString(earningsPerSecond * (count * 60));
                    break;
            }

        }
    }

}