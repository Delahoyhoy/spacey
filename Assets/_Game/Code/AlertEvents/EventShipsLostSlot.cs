using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Alerts
{
    [Serializable]
    public struct EventShipsLostSlot
    {
        public GameObject mainObject;
        public Image shipImage;
        public Text countText;
    }
}