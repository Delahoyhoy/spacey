using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Outposts;
using Core.Gameplay.Sectors;
using Fumb.Save;
//using Fumb.Reviews;
using UnityEngine;
using UnityEngine.Events;

public class SimpleFeedbackManager : MonoBehaviour
{
    [ManagedSaveValue("SimpleFeedbackManagerHasAskedForReview")]
    private SaveValueBool saveHasAskedForReview;

    [SerializeField]
    private int minOutpostCount = 6;

    [SerializeField]
    private int minCluster = 3;

    [SerializeField]
    private GameObject askPanel, positivePanel, negativePanel;

    [SerializeField]
    private UIScreen requestScreen;

    [SerializeField]
    private UnityEvent onDisplayReview;

    private bool enqueuedToAwait;

    private void Start()
    {
        OutpostManager.Instance.onBuiltOutpost += CheckShouldAskForReview;
    }

    public void CheckShouldAskForReview()
    {
        if (saveHasAskedForReview.Value)
            return;
        if (StatTracker.Instance.GetStat("outpostsBuilt") < minOutpostCount && 
            ClusterManager.Instance.HighestCluster < minCluster)
            return;
        if (!UIManager.Instance.InMenu)
        {
            PopupRequest(true);
            return;
        }
        if (enqueuedToAwait)
            return;

        UIManager.Instance.onScreenClear += PopupRequest;
        enqueuedToAwait = true;
    }

    private void PopupRequest(bool screenClear)
    {
        if (!screenClear)
            return;
        if (enqueuedToAwait)
            UIManager.Instance.onScreenClear -= PopupRequest;    
        askPanel.SetActive(true);
        positivePanel.SetActive(false);
        negativePanel.SetActive(false);
        
        requestScreen.Open();

        saveHasAskedForReview.Value = true;
    }

    public void PressPositive()
    {
        AudioManager.Play("Click", 1f);
        askPanel.SetActive(false);
        positivePanel.SetActive(true);
    }

    public void PressNegative()
    {
        AudioManager.Play("Click", 1f);
        askPanel.SetActive(false);
        negativePanel.SetActive(true);
    }

    public void GoToReview()
    {
        //ReviewsManager.OpenReviewFlow(result => { requestScreen.Close(); });
        onDisplayReview?.Invoke();
    }
}
