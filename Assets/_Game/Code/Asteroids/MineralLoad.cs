using System;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class MineralLoad
    {
        public double maxCapacity = 5, commonMinerals, uncommonMinerals, rareMinerals,
            epicMinerals, legendaryMinerals;

        public Action<float> setFillAmount;
        public Action<bool> setFull;

        public void SetCapacity(double capacity)
        {
            maxCapacity = capacity;
            //Debug.Log($"SETTING CAPACITY TO {capacity}");
        }

        public MineralLoad(double cap = 0)
        {
            maxCapacity = cap;
            Reset();
        }

        public MineralLoad(double cap, Action<float> onSetFillAmount, Action<bool> onSetFull) : this(cap)
        {
            setFull = onSetFull;
            setFillAmount = onSetFillAmount;
        }

        public MineralLoad(MineralLoad load)
        {
            maxCapacity = load.maxCapacity;
            commonMinerals = load.commonMinerals;
            uncommonMinerals = load.uncommonMinerals;
            rareMinerals = load.rareMinerals;
            epicMinerals = load.epicMinerals;
            legendaryMinerals = load.legendaryMinerals;
        }

        public MineralLoad Duplicate()
        {
            return new MineralLoad(this);
        }

        public double TotalValue =>
            commonMinerals * TierHelper.GetValue(Tier.Common) +
            uncommonMinerals * TierHelper.GetValue(Tier.Uncommon) +
            rareMinerals * TierHelper.GetValue(Tier.Rare) +
            epicMinerals * TierHelper.GetValue(Tier.Epic) +
            legendaryMinerals * TierHelper.GetValue(Tier.Legendary);

        public void Reset()
        {
            commonMinerals = 0;
            uncommonMinerals = 0;
            rareMinerals = 0;
            epicMinerals = 0;
            legendaryMinerals = 0;
            
            setFull?.Invoke(false);
            setFillAmount?.Invoke(0);
        }

        public void AddMinerals(double[] toAdd)
        {
            AddMinerals(toAdd[0], toAdd[1], toAdd[2], toAdd[3], toAdd[4]);
            setFull?.Invoke(Full);
            setFillAmount?.Invoke((float)(TotalMinerals / maxCapacity));
        }

        public double[] GetPercentOfCapacity(double pct)
        {
            if (pct > 1)
                pct = 1;
            double[] result = new double[5];
            double[] composition = DetermineComposition();
            double multiplier = maxCapacity * pct;
            result[0] = composition[0] * multiplier;
            result[1] = composition[1] * multiplier;
            result[2] = composition[2] * multiplier;
            result[3] = composition[3] * multiplier;
            result[4] = composition[4] * multiplier;

            return result;
        }

        public double[] GetLoad()
        {
            return new double[] {commonMinerals, uncommonMinerals, rareMinerals, epicMinerals, legendaryMinerals};
        }

        public void Remove(double[] amounts)
        {
            commonMinerals -= amounts[0];
            if (commonMinerals < 0)
                commonMinerals = 0;
            uncommonMinerals -= amounts[1];
            if (uncommonMinerals < 0)
                uncommonMinerals = 0;
            rareMinerals -= amounts[2];
            if (rareMinerals < 0)
                rareMinerals = 0;
            epicMinerals -= amounts[3];
            if (epicMinerals < 0)
                epicMinerals = 0;
            legendaryMinerals -= amounts[4];
            if (legendaryMinerals < 0)
                legendaryMinerals = 0;

        }

        public double[] DetermineComposition()
        {
            double totalMinerals = TotalMinerals;
            if (totalMinerals <= 0)
                return new double[] {0, 0, 0, 0, 0};
            return new double[]
            {
                commonMinerals / totalMinerals, uncommonMinerals / totalMinerals, rareMinerals / totalMinerals,
                epicMinerals / totalMinerals, legendaryMinerals / totalMinerals
            };
        }

        public float[] GetFloatComposition()
        {
            double totalMinerals = TotalMinerals;
            if (totalMinerals <= 0)
                return new float[] {0, 0, 0, 0, 0};
            return new float[]
            {
                (float) (commonMinerals / maxCapacity), (float) (uncommonMinerals / maxCapacity),
                (float) (rareMinerals / maxCapacity), (float) (epicMinerals / maxCapacity),
                (float) (legendaryMinerals / maxCapacity)
            };
        }

        public void AddMinerals(double common, double uncommon, double rare, double epic, double legend)
        {
            if (Full)
                return;
            double overflow = 0;
            legendaryMinerals += legend;
            if (Overflow(out overflow))
            {
                legendaryMinerals -= overflow;
                return;
            }

            epicMinerals += epic;
            if (Overflow(out overflow))
            {
                epicMinerals -= overflow;
                return;
            }

            rareMinerals += rare;
            if (Overflow(out overflow))
            {
                rareMinerals -= overflow;
                return;
            }

            uncommonMinerals += uncommon;
            if (Overflow(out overflow))
            {
                uncommonMinerals -= overflow;
                return;
            }

            commonMinerals += common;
            if (Overflow(out overflow))
            {
                commonMinerals -= overflow;
                return;
            }
        }

        public bool Overflow(out double by)
        {
            if (Full)
            {
                by = TotalMinerals - maxCapacity;
                return true;
            }

            by = 0;
            return false;
        }

        public bool Full => TotalMinerals >= maxCapacity;

        public bool Empty => TotalMinerals <= 0;

        public double TotalMinerals => commonMinerals + uncommonMinerals + rareMinerals + epicMinerals + legendaryMinerals;
    }
}