﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using Core.Gameplay.Tutorial;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public class Debris : MonoBehaviour
    {
        private double goldYield = 5d, diamondYield = 0d;

        private Vector3 movementDir;

        [SerializeField]
        private Transform visuals, shine;

        [SerializeField]
        private float driftSpeed = 1f;

        private float rotationSpeed = 30f;

        private float lifetime = 30f;
        Vector3 shineMin = new Vector3(-1.5f, -1.5f, 0), shineMax = new Vector3(1.5f, 1.5f, 0);

        [SerializeField]
        private string trailID = "brownTrail", burstID = "brownRock";

        public void Claim()
        {
            Vector3 position = transform.position;
            Vector3 screenPos = Camera.main.WorldToScreenPoint(position);
            if (goldYield > 0)
                CoinBurster.Instance.NewBurst(CurrencyType.SoftCurrency, screenPos, Random.Range(5, 8), goldYield);
            if (diamondYield > 0)
                CoinBurster.Instance.NewBurst(CurrencyType.Vault, screenPos, (int) diamondYield, diamondYield);
            //CurrencyController.Instance.SpawnReward(goldYield, CurrencyType.Gold, position);
            AudioManager.Play("Milestone", 0.5f);
            EffectManager.Instance.EmitAt("sparkleBurst", position, Random.Range(10, 15));
            //TutorialManager.Instance.Trigger("debris");
            Destroy(gameObject);
        }

        public void DestroyDebris()
        {
            Destroy(gameObject);
        }

        public void Setup(double gold, double gems, Vector3 position, Vector3 direction)
        {
            goldYield = gold;
            diamondYield = gems;
            transform.position = position;
            movementDir = direction;
        }

        // Start is called before the first frame update
        private void Start()
        {
            driftSpeed = Random.Range(0.75f, 1.25f);
            lifetime = FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) ? Random.Range(20f, 60f) : 10f;
            rotationSpeed = Random.Range(20f, 90f) * (Random.value > 0.5f ? 1f : -1f);
            StartCoroutine(AutoDestruct());
            StartCoroutine(ShineLoop());
        }

        private IEnumerator AutoDestruct()
        {
            yield return new WaitForSeconds(lifetime);
            EffectManager.Instance.EmitAt(burstID, transform.position, 10);
            //TutorialManager.Instance.Trigger("debris");
            Destroy(gameObject);
        }

        private IEnumerator ShineLoop()
        {
            while (true)
            {
                yield return new WaitForSeconds(3f);
                for (float t = 0; t < 1f; t += Time.deltaTime)
                {
                    shine.localPosition = Vector3.Lerp(shineMin, shineMax, t);
                    yield return null;
                }
            }
        }

        private void OnEnable()
        {
            DebrisManager.OnDestroyDebris += DestroyDebris;
        }

        private void OnDestroy()
        {
            DebrisManager.OnDestroyDebris -= DestroyDebris;
        }

        private float emissionCooldown = 0;

        // Update is called once per frame
        private void Update()
        {
            if (emissionCooldown > 0)
                emissionCooldown -= Time.deltaTime;
            if (emissionCooldown <= 0)
            {
                emissionCooldown = 0.2f;
                EffectManager.Instance.EmitAt(trailID, transform.position, 1);
            }

            visuals.Rotate(0, 0, rotationSpeed * Time.deltaTime);
            transform.Translate(movementDir * (Time.deltaTime * driftSpeed));
        }
    }

}