﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.Audio;
using UnityEngine;
using UnityEngine.UI;
using Fumb.RemoteConfig;
using Fumb.Save;

namespace Core.Gameplay.Asteroids
{
    public class AsteroidMenu : MonoBehaviour
    {
        [SerializeField]
        private AsteroidSlot slotPrefab;

        [SerializeField]
        private RectTransform listParent;

        [SerializeField]
        private List<AsteroidSlot> slots = new List<AsteroidSlot>();

        [SerializeField]
        private Text sortModeText;

        [SerializeField]
        private Text totalUnscannedCount, quickScanCost;

        [SerializeField]
        private Button scanNextButton, quickScanButton;

        [SerializeField]
        private GameObject scanOptions, getMoreGemsButton;

        private float countdown = 1f;

        [ManagedSaveValue("AsteroidMenuSortMode")]
        private SaveValueInt saveSortMode;

        private RemoteInt quickScanCostPerAsteroid = new RemoteInt("quickScanCost", 10);

        private AsteroidSortMode currentSortMode = AsteroidSortMode.Distance;

        private void Setup()
        {
            bool addedAny = false;
            for (int i = 0; i < AsteroidManager.Instance.AsteroidCount; i++)
            {
                if (i < slots.Count)
                    slots[i].Refresh();
                else
                {
                    AsteroidSlot newSlot = Instantiate(slotPrefab, listParent);
                    newSlot.Setup(i);
                    slots.Add(newSlot);
                    addedAny = true;
                }
            }

            if (addedAny)
                SortList();

            int totalScannable = GameManager.Instance.GetTotalUnscannedAsteroids();
            bool scanOptionsAvailable = totalScannable > 0 && quickScanCostPerAsteroid > 0;
            scanOptions.SetActive(scanOptionsAvailable);
            if (!scanOptionsAvailable)
                return;
            double gemCost = quickScanCostPerAsteroid * totalScannable;
            bool canAfford = CurrencyController.Instance.CurrentDiamonds >= gemCost;
            quickScanButton.interactable = canAfford;
            getMoreGemsButton.SetActive(!canAfford);
            scanNextButton.interactable = !GameManager.Instance.Scanning;
            quickScanCost.text = CurrencyController.FormatToString(gemCost);
            totalUnscannedCount.text = $"x{totalScannable}";
        }

        public void QuickScan()
        {
            int totalScannable = GameManager.Instance.GetTotalUnscannedAsteroids();
            if (totalScannable <= 0)
                return;
            double gemCost = quickScanCostPerAsteroid * totalScannable;
            bool canAfford = CurrencyController.Instance.CurrentDiamonds >= gemCost;
            if (!canAfford)
                return;
            CurrencyController.Instance.AddCurrency(-gemCost, CurrencyType.HardCurrency, false, "scanner", "quick_scan");
            AudioManager.Play("Milestone", 1f);
            GameManager.Instance.QuickScanAsteroids();
            Setup();
        }

        public void ScanNext()
        {
            GameManager.Instance.ScanFirstUnscannedAsteroid();
            Setup();
        }

        private void SortList()
        {
            if (slots.Count <= 0)
                return;
            List<AsteroidSlot> sortedSlots = new List<AsteroidSlot>(slots.ToArray());
            sortedSlots = Sort(sortedSlots, currentSortMode);
            for (int i = 0; i < sortedSlots.Count; i++)
            {
                sortedSlots[i].transform.SetAsLastSibling();
            }
        }

        public void ClearMenu()
        {
            for (int i = 0; i < slots.Count; i++)
            {
                Destroy(slots[i].gameObject);
            }

            slots.Clear();
        }

        private void OnAsteroidStateChange(AsteroidState asteroidState)
        {
            SortList();
        }

        private List<AsteroidSlot> Sort(List<AsteroidSlot> asteroidSlots, AsteroidSortMode sortMode)
        {
            List<double> metric = new List<double>();
            List<AsteroidSlot> sortedList = new List<AsteroidSlot>();
            switch (sortMode)
            {
                case AsteroidSortMode.Distance:
                    for (int i = 0; i < asteroidSlots.Count; i++)
                    {
                        metric.Add(Vector3.Distance(
                            GameManager.Instance.GetAsteroid(asteroidSlots[i].Index).transform.position,
                            GameManager.Instance.FogCentre));
                    }

                    while (asteroidSlots.Count > 0)
                    {
                        double min = double.MaxValue;
                        int minIndex = 0;
                        for (int i = 0; i < metric.Count; i++)
                        {
                            if (metric[i] < min)
                            {
                                min = metric[i];
                                minIndex = i;
                            }
                        }

                        sortedList.Add(asteroidSlots[minIndex]);
                        asteroidSlots.RemoveAt(minIndex);
                        metric.RemoveAt(minIndex);
                    }

                    break;
                case AsteroidSortMode.Yield:
                    for (int i = 0; i < asteroidSlots.Count; i++)
                    {
                        metric.Add(GetYield(GameManager.Instance.GetAsteroid(asteroidSlots[i].Index)));
                    }

                    while (asteroidSlots.Count > 0)
                    {
                        double max = double.MinValue;
                        int maxIndex = 0;
                        for (int i = 0; i < metric.Count; i++)
                        {
                            if (metric[i] > max)
                            {
                                max = metric[i];
                                maxIndex = i;
                            }
                        }

                        sortedList.Add(asteroidSlots[maxIndex]);
                        asteroidSlots.RemoveAt(maxIndex);
                        metric.RemoveAt(maxIndex);
                    }

                    break;
                case AsteroidSortMode.Status:
                    List<List<AsteroidSlot>> statusMetrics = new List<List<AsteroidSlot>>();
                    for (int i = 0; i < 5; i++)
                        statusMetrics.Add(new List<AsteroidSlot>());
                    for (int i = 0; i < asteroidSlots.Count; i++)
                    {
                        int status = asteroidSlots[i].GetStatusMetric();
                        statusMetrics[status].Add(asteroidSlots[i]);
                    }

                    for (int i = 4; i >= 0; i--)
                    {
                        if (statusMetrics[i].Count > 0)
                        {
                            sortedList.AddRange(Sort(statusMetrics[i], AsteroidSortMode.Yield));
                        }
                    }

                    break;
            }

            return sortedList;
        }

        private void OnEnable()
        {
            Asteroid.OnAnyStateChange += OnAsteroidStateChange;
        }

        private void OnDestroy()
        {
            Asteroid.OnAnyStateChange -= OnAsteroidStateChange;
        }

        private static double GetYield(Asteroid asteroid)
        {
            if (asteroid.State == AsteroidState.Unscanned || asteroid.State == AsteroidState.Scanning)
                return 0;
            double[] yields = asteroid.GetResourceYields();
            double total = 0;
            for (int i = 0; i < yields.Length; i++)
            {
                total += yields[i] * TierHelper.GetValue((Tier) i);
            }

            return total;
        }

        public void SetSortMode(AsteroidSortMode mode)
        {
            currentSortMode = mode;
            sortModeText.text = GetSortModeName(currentSortMode);
            SortList();
        }

        public void CycleSortMode()
        {
            AudioManager.Play("Click", 0.75f);
            if ((int) currentSortMode >= Enum.GetValues(typeof(AsteroidSortMode)).Length - 1)
                currentSortMode = 0;
            else
                currentSortMode++;
            saveSortMode.Value = (int) currentSortMode;
            SetSortMode(currentSortMode);
        }

        private static string GetSortModeName(AsteroidSortMode mode)
        {
            return mode switch
            {
                AsteroidSortMode.Distance => "DISTANCE",
                AsteroidSortMode.Status => "STATUS",
                AsteroidSortMode.Yield => "YIELD",
                _ => "DEFAULT"
            };
        }

        private void Start()
        {
            SetSortMode((AsteroidSortMode) saveSortMode.Value);
            sortModeText.text = GetSortModeName(currentSortMode);
            Setup();
        }

        private void Update()
        {
            countdown -= Time.deltaTime;
            if (countdown > 0)
                return;
            Setup();
            countdown += 1f;
        }
    }

}