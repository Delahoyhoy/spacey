﻿namespace Core.Gameplay.Asteroids
{
    public enum ChunkSize
    {
        chunk_xsmall = 1,
        chunk_small = 2,
        chunk_medium = 3,
        chunk_large = 4,
        chunk_xlarge = 5,
    }
}