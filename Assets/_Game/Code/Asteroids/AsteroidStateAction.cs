﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class AsteroidStateAction
    {
        [SerializeField]
        private AsteroidState state;

        public AsteroidState State => state;

        [SerializeField]
        private UnityEvent action;

        public UnityEvent Action => action;
    }
}