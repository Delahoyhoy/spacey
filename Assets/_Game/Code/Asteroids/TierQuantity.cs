namespace Core.Gameplay.Asteroids
{
    public struct TierQuantity
    {
        private Tier mineralTier;
        public Tier MineralTier => mineralTier;

        private double amount;
        public double Amount => amount;

        public TierQuantity(Tier tier, double quantity)
        {
            mineralTier = tier;
            amount = quantity;
        }
    }
}