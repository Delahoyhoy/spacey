using System;
using System.Collections.Generic;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class AsteroidData
    {
        public int currentLayer;
        public List<AsteroidLayer> asteroidLayers;
        
        public ResourceChunkReward chunkReward;

        public bool HasReward => chunkReward != null;

        public AsteroidData(int current, List<AsteroidLayer> layers, ResourceChunkReward reward = null)
        {
            currentLayer = current;
            asteroidLayers = layers;
            chunkReward = reward;
        }
    }
}
