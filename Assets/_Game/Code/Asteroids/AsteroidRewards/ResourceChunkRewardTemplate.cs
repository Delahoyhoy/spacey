using System;
using Fumb.Attribute;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class ResourceChunkRewardTemplate
    {
        public ResourceChunkRewardType rewardType;
        public int magnitude;

        [SerializeField]
        private string titleText;

        public string TitleText => titleText;

        [SerializeField]
        private Sprite rewardIcon;

        public Sprite RewardIcon => rewardIcon;

        [SerializeField]
        private string magnitudeFormat = "{0}";

        public string MagnitudeFormat => magnitudeFormat;

        [SerializeField]
        private ResourceChunkRewardEnhancement rewardEnhancement;

        [SerializeField] [ConditionalField("rewardEnhancement", false, ResourceChunkRewardEnhancement.HardCurrency)]
        private int enhancementCost;

        [SerializeField] [ConditionalField("rewardEnhancement", true, ResourceChunkRewardEnhancement.None)]
        private float enhancementMagnitude;
        
        public UnityEvent<int> onConferReward;

        [SerializeField]
        private ResourceChunkRewardDisplay displayPrefab;

        public ResourceChunkRewardDisplay GetRewardDisplay(Transform parentObject)
        {
            ResourceChunkRewardDisplay display = Object.Instantiate(displayPrefab, parentObject);
            
            var displayTransform = display.transform;
            displayTransform.localPosition = Vector3.zero;
            displayTransform.localScale = Vector3.one;

            return display;
        }

        public void ConferReward(int value)
        {
            onConferReward?.Invoke(value);
        }

        public ResourceChunkRewardEnhancement GetEnhancementOption(out float enhanceMult, out int cost)
        {
            enhanceMult = enhancementMagnitude;
            cost = enhancementCost;
            return rewardEnhancement;
        }
    }
}