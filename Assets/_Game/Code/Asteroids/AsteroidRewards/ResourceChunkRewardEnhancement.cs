﻿namespace Core.Gameplay.Asteroids
{
    public enum ResourceChunkRewardEnhancement
    {
        None,
        RewardedVideo,
        HardCurrency,
    }
}