namespace Core.Gameplay.Asteroids
{
    public enum ResourceChunkRewardType
    {
        StarFragment,
        HardCurrency,
        Augments,
        Crew
    }
}