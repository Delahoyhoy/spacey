﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Asteroids
{
    public class ResourceChunkRewardPopup : MonoBehaviour
    {
        [SerializeField]
        private UIScreen uiScreen;

        [SerializeField]
        private Text titleText, valueDisplay, hardCurrencyCost;
        
        [SerializeField]
        private AdButton adButton;

        [SerializeField]
        private Image rewardIcon;

        [SerializeField]
        private Button hardCurrencyButton;

        [SerializeField]
        private string headerFormat = "{0} Found";

        private Action<float> claimAction;
        private bool claimed;

        public void Setup(ResourceChunkReward reward, Action onClaim)
        {
            if (!ResourceChunkRewardManager.Instance.TryGetRewardTemplate(reward.rewardType, 
                    out ResourceChunkRewardTemplate template))
                return;
            claimed = false;
            uiScreen.Open();
            rewardIcon.sprite = ResourceChunkRewardManager.Instance.GetIcon(reward.rewardType);
            titleText.text = string.Format(headerFormat, template.TitleText);
            valueDisplay.text = string.Format(template.MagnitudeFormat, reward.rewardMagnitude);

            void Claim(float multiplier)
            {
                if (claimed)
                    return;
                ResourceChunkRewardManager.Instance.ConferReward(reward.rewardType, 
                    Mathf.RoundToInt(reward.rewardMagnitude * multiplier));
                onClaim?.Invoke();
                uiScreen.Close();
                claimed = true;
            }

            claimAction = Claim;

            ResourceChunkRewardEnhancement enhancementType =
                template.GetEnhancementOption(out float enhanceMult, out int cost);
            
            hardCurrencyButton.gameObject.SetActive(enhancementType == ResourceChunkRewardEnhancement.HardCurrency);
            adButton.gameObject.SetActive(enhancementType == ResourceChunkRewardEnhancement.RewardedVideo);

            switch (enhancementType)
            {
                case ResourceChunkRewardEnhancement.HardCurrency:
                    void Purchase()
                    {
                        if (!CurrencyController.Instance.SpendCurrency(cost, CurrencyType.HardCurrency, "chunk_rewards", $"reward_{reward.rewardType}"))
                            return;
                        Claim(enhanceMult);
                    }
                    hardCurrencyCost.text = CurrencyController.FormatToString(cost);
                    hardCurrencyButton.interactable =
                        CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) >= cost;
                    hardCurrencyButton.onClick.RemoveAllListeners();
                    hardCurrencyButton.onClick.AddListener(Purchase);
                    break;
                
                case ResourceChunkRewardEnhancement.RewardedVideo:
                    adButton.AdCompleteAction.RemoveAllListeners();
                    adButton.AdCompleteAction.AddListener(() => Claim(enhanceMult));
                    break;
            }
        }

        public void ClaimFree()
        {
            claimAction?.Invoke(1f);
        }
    }
}