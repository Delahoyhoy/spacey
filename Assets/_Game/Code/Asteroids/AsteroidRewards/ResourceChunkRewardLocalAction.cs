using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Asteroids
{
    public class ResourceChunkRewardLocalAction : MonoBehaviour
    {
        [SerializeField]
        private ResourceChunkRewardType rewardType;
        
        [SerializeField]
        private UnityEvent<int> onConferReward;

        private bool initialised = false;
        
        private void Start()
        {
            if (initialised)
                return;
            ResourceChunkRewardManager.Instance.onConferReward += RewardConferred;
            initialised = true;
        }

        public void RewardConferred(ResourceChunkRewardType type, int amount)
        {
            if (rewardType.Equals(type))
                onConferReward?.Invoke(amount);
        }
    }
}