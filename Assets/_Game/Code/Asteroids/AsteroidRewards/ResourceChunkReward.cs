using System;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class ResourceChunkReward
    {
        public ResourceChunkRewardType rewardType;
        public int rewardMagnitude;
        
        public ResourceChunkReward(ResourceChunkRewardType type, int magnitude)
        {
            rewardType = type;
            rewardMagnitude = magnitude;
        }
    }
}