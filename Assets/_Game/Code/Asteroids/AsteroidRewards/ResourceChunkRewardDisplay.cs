﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Asteroids
{
    public class ResourceChunkRewardDisplay : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent onRevealReward, onClaim;

        private Action tapAction;
        
        public bool claimed;
        
        public void Display(Action claimAction)
        {
            gameObject.SetActive(true);
            onRevealReward?.Invoke();
            tapAction = () => claimAction?.Invoke();
        }

        public void CompleteClaim()
        {
            gameObject.SetActive(false);
            onClaim?.Invoke();
            claimed = true;
        }

        public void ClaimReward()
        {
            if (claimed)
                return;
            tapAction?.Invoke();
        }
    }
}