using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Asteroids
{
    public class ResourceChunkRewardVisual : MonoBehaviour
    {
        [SerializeField]
        private Image icon;

        private Action onTap;

        public void Setup(ResourceChunkRewardType reward, Action tapAction)
        {
            SetActive(true);
            onTap = tapAction;
            icon.sprite = ResourceChunkRewardManager.Instance.GetIcon(reward);
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void Tap()
        {
            onTap?.Invoke();
        }
    }
}
