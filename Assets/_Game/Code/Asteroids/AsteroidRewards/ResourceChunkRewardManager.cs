using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public class ResourceChunkRewardManager : MonoBehaviour
    {
        private static ResourceChunkRewardManager instance;

        public static ResourceChunkRewardManager Instance =>
            instance ??= ObjectFinder.FindObjectOfType<ResourceChunkRewardManager>();

        [SerializeField]
        private ResourceChunkRewardPopup popup;
        
        [SerializeField]
        private ResourceChunkRewardTemplate[] rewardTemplates;

        private Dictionary<ResourceChunkRewardType, ResourceChunkRewardTemplate> rewardsLookup =
            new Dictionary<ResourceChunkRewardType, ResourceChunkRewardTemplate>();

        public Action<ResourceChunkRewardType, int> onConferReward;

        private void Awake()
        {
            instance = this;
            for (int i = 0; i < rewardTemplates.Length; i++)
            {
                if (!rewardsLookup.ContainsKey(rewardTemplates[i].rewardType))
                    rewardsLookup.Add(rewardTemplates[i].rewardType, rewardTemplates[i]);
            }
        }

        public Sprite GetIcon(ResourceChunkRewardType rewardType)
        {
            if (rewardsLookup.TryGetValue(rewardType, out ResourceChunkRewardTemplate rewardTemplate))
                return rewardTemplate.RewardIcon;
            return null;
        }

        public bool TryGetRewardDisplay(ResourceChunkRewardType rewardType, Transform target, out ResourceChunkRewardDisplay newReward)
        {
            newReward = null;
            if (!TryGetRewardTemplate(rewardType, out ResourceChunkRewardTemplate rewardTemplate))
                return false;

            newReward = rewardTemplate.GetRewardDisplay(target);
            return newReward;
        }

        public void DisplayReward(ResourceChunkReward reward, Action onClaim)
        {
            popup.Setup(reward, onClaim);
        }

        public bool TryGetRewardTemplate(ResourceChunkRewardType rewardType, out ResourceChunkRewardTemplate rewardTemplate)
        {
            return rewardsLookup.TryGetValue(rewardType, out rewardTemplate);
        }

        public void ConferReward(ResourceChunkRewardType rewardType, int magnitude)
        {
            if (!rewardsLookup.TryGetValue(rewardType, out ResourceChunkRewardTemplate rewardTemplate))
                return;
            rewardTemplate.ConferReward(magnitude);
            onConferReward?.Invoke(rewardType, magnitude);
        }
    }
}