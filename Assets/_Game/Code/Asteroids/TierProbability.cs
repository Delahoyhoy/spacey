using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public struct TierProbability
    {
        public Tier tier;
        public GameObject panel;
        public Text percentChance;

        public void Setup(double probability)
        {
            double newProbability = Math.Floor(probability * 100);
            if (newProbability > 0)
            {
                panel.SetActive(true);
                percentChance.text = $"{newProbability}% {TierHelper.GetName(tier)}";
            }
            else
                panel.SetActive(false);
        }
    }
}