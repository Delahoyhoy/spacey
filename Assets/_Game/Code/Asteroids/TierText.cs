using System;
using UnityEngine.UI;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public struct TierText
    {
        public Text text;
        public LayoutElement layoutElement;

        public void Setup(double amount)
        {
            text.text = Math.Floor(amount * 100) + "%";
            layoutElement.flexibleWidth = (float) amount * 100f;
            if (amount <= 0)
                text.gameObject.SetActive(false);
        }
    }
}