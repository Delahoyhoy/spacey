namespace Core.Gameplay.Asteroids
{
    public enum AsteroidSortMode
    {
        Distance,
        Yield,
        Status,
    }
}