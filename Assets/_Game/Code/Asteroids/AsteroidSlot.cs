﻿using System;
using Core.Gameplay.Outposts;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Asteroids
{
    public class AsteroidSlot : MonoBehaviour
    {
        [SerializeField]
        private Text nameText, statusText, buildCost;

        private int asteroidIndex;

        public int Index
        {
            get { return asteroidIndex; }
        }

        private Asteroid asteroid;

        [SerializeField]
        private Image[] tierScales = new Image[5];

        [SerializeField]
        private TierText[] tierTexts = new TierText[5];

        [SerializeField]
        private GameObject target, contentIndicator;

        [SerializeField]
        private Button scanButton, buildButton, mineButton, skipScanButton, openButton;

        [SerializeField]
        private Text scanCost, scanPercent, distanceText;

        [SerializeField]
        private Image asteroidImage;

        [SerializeField]
        private RectTransform numbers;

        private string GetStatus()
        {
            string statusMessage = "UNKNOWN", colour = "red";
            if (!asteroid) 
                return string.Format("STATUS: <color={1}>>>{0}<<</color>", statusMessage, colour);
            switch (asteroid.State)
            {
                case AsteroidState.Unscanned:
                    statusMessage = "UNSCANNED";
                    colour = "red";
                    break;
                case AsteroidState.Scanned:
                    statusMessage = "SCANNED";
                    colour = "green";
                    break;
                case AsteroidState.Scanning:
                    statusMessage = "SCANNING";
                    colour = "orange";
                    break;
                case AsteroidState.OutpostBuilt:
                    Outpost outpost = OutpostManager.Instance.GetOutpostFromAsteroid(asteroidIndex);
                    if (outpost && outpost.GetCurrentlyMining())
                    {
                        statusMessage = "MINING";
                        colour = "blue";
                    }
                    else
                    {
                        statusMessage = "READY TO MINE";
                        colour = "purple";
                    }
                    break;
            }

            return string.Format("STATUS: <color={1}>>>{0}<<</color>", statusMessage, colour);
        }

        public int GetStatusMetric()
        {
            if (asteroid)
                switch (asteroid.State)
                {
                    case AsteroidState.Unscanned:
                        return 0;
                    case AsteroidState.Scanned:
                        return 2;
                    case AsteroidState.Scanning:
                        return 1;
                    case AsteroidState.OutpostBuilt:
                        Outpost outpost = OutpostManager.Instance.GetOutpostFromAsteroid(asteroidIndex);
                        if (outpost && outpost.GetCurrentlyMining())
                            return 4;
                        return 3;
                }

            return 0;
        }

        private void SetupButtons()
        {
            AsteroidState currentState = AsteroidState.Unscanned;
            if (asteroid)
                currentState = asteroid.State;
            switch (currentState)
            {
                case AsteroidState.Unscanned:
                    mineButton.gameObject.SetActive(false);
                    scanButton.gameObject.SetActive(true);
                    buildButton.gameObject.SetActive(false);
                    contentIndicator.SetActive(false);
                    skipScanButton.gameObject.SetActive(false);
                    openButton.gameObject.SetActive(false);
                    scanPercent.gameObject.SetActive(false);
                    break;
                case AsteroidState.Scanning:
                    mineButton.gameObject.SetActive(false);
                    scanButton.gameObject.SetActive(false);
                    buildButton.gameObject.SetActive(false);
                    contentIndicator.SetActive(false);
                    skipScanButton.gameObject.SetActive(true);
                    openButton.gameObject.SetActive(false);
                    double costToScan = ScannerSweep.Instance.HurryCost;
                    skipScanButton.interactable = CurrencyController.Instance.CurrentDiamonds >= costToScan;
                    scanCost.text = CurrencyController.FormatToString(costToScan);
                    scanPercent.gameObject.SetActive(true);
                    scanPercent.text = "SCANNING: " +
                                       Mathf.RoundToInt(ScannerSweep.Instance.percentComplete * 100).ToString() + "%";
                    break;
                case AsteroidState.Scanned:
                    mineButton.gameObject.SetActive(false); //(MiningStation.Instance.CurrentTarget != asteroidIndex);
                    scanButton.gameObject.SetActive(false);
                    buildButton.gameObject.SetActive(true);
                    buildCost.text = CurrencyController.FormatToString(OutpostManager.Instance.NextOutpostCost);
                    buildButton.interactable = CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency) >=
                                               OutpostManager.Instance.NextOutpostCost;
                    contentIndicator.SetActive(true);
                    skipScanButton.gameObject.SetActive(false);
                    openButton.gameObject.SetActive(false);
                    scanPercent.gameObject.SetActive(false);
                    break;
                case AsteroidState.OutpostBuilt:
                    mineButton.gameObject.SetActive(false); //(MiningStation.Instance.CurrentTarget != asteroidIndex);
                    scanButton.gameObject.SetActive(false);
                    buildButton.gameObject.SetActive(false);
                    contentIndicator.SetActive(true);
                    skipScanButton.gameObject.SetActive(false);
                    openButton.gameObject.SetActive(true);
                    scanPercent.gameObject.SetActive(false);
                    break;
            }
        }

        public void HurryScan()
        {
            ScannerSweep.Instance.Hurry();
            //CurrencyController.Instance.AddCurrency(-costToScan, CurrencyType.Diamonds);
        }

        public void OpenOutpost()
        {
            Outpost outpost = OutpostManager.Instance.GetOutpostFromAsteroid(asteroidIndex);
            OutpostManager.Instance.OpenOutpostMenu(outpost);
        }

        public void Setup(int index)
        {
            asteroidIndex = index;
            asteroid = GameManager.Instance.GetAsteroid(asteroidIndex);
            if (asteroid)
            {
                asteroidImage.sprite = asteroid.AsteroidSprite;
                asteroidImage.transform.localEulerAngles = asteroid.Rotation;
                asteroidImage.transform.localScale =
                    new Vector3(asteroid.ScaleFactor / 1.5f, asteroid.ScaleFactor / 1.5f, 1f);
                distanceText.text = "DISTANCE: " +
                                    (Mathf.Round(Vector3.Distance(asteroid.transform.position,
                                        GameManager.Instance.FogCentre) * 10) / 10).ToString() + "m";
                nameText.text = asteroid.AsteroidName;
            }

            statusText.text = GetStatus();
            SetupTiers();
            SetupButtons();
            target.SetActive(false); //(MiningStation.Instance.CurrentTarget == asteroidIndex);
        }

        public void Refresh()
        {
            statusText.text = GetStatus();
            SetupButtons();
            target.SetActive(false); //(MiningStation.Instance.CurrentTarget == asteroidIndex);
        }

        private void SetupTiers()
        {
            double[] tiers = {1, 0, 0, 0, 0};
            if (asteroid)
                tiers = asteroid.GetResourceYields();
            double total = 0;
            for (int i = 0; i < tiers.Length; i++)
            {
                total += tiers[i];
                tierScales[i].fillAmount = (float) total;
                tierTexts[i].Setup(tiers[i]);
            }

            LayoutRebuilder.MarkLayoutForRebuild(numbers);
        }

        public void PressScan()
        {
            if (asteroid)
                GameManager.Instance.ScanAsteroid(asteroid);
        }

        public void MineAsteroid()
        {
            MiningStation.Instance.SetTarget(asteroidIndex);
        }

        public void BuildOutpost()
        {
            OutpostManager.Instance.BuildOutpost(asteroidIndex);
            // if (OutpostManager.Instance.BuildOutpost(asteroidIndex) &&
            //     AsteroidUnlockSats.Instance.CheckShouldUnlock(out int reward))
            // {
            //     CoinBurster.Instance.NewBurst(CurrencyType.Satoshis, asteroidImage.transform.position, reward, reward);
            //     AnalyticsHelper.CurrencyEarn(CurrencyType.Satoshis, reward, true, "BuildOutpost");
            // }
        }
    }

}