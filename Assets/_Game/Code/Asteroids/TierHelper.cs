using System;
using System.Collections.Generic;
using Core.Gameplay.Version;
using Data;
using Fumb;
using Fumb.Data;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public static class TierHelper
    {
        private static bool initialised;

        private static Dictionary<Tier, MineralData> tierLookup;

        [RuntimeInitializeOnLoadMethod]
        public static void Initialise()
        {
            SceneLoader.LoadedFirstValidSceneAction += OnFirstSceneLoaded;
        }

        private static void OnFirstSceneLoaded()
        {
            tierLookup = new Dictionary<Tier, MineralData>();
            MineralData[] data = DataAccessor.GetData<DataAssetMineralData>().Data;
            for (int i = 0; i < data.Length; i++)
                tierLookup.Add((Tier)i, data[i]);
            initialised = true;
        }

        public static string GetName(Tier tier)
        {
            switch (tier)
            {
                case Tier.Common:
                    return "COMMON";
                case Tier.Uncommon:
                    return "UNCOMMON";
                case Tier.Rare:
                    return "RARE";
                case Tier.Epic:
                    return "EPIC";
                case Tier.Legendary:
                    return "LEGENDARY";
            }
            
            return "COMMON";
        }

        public static double GetToughness(Tier tier)
        {
            if (initialised && tierLookup.TryGetValue(tier, out MineralData data))
                return data.Toughness;
            return Math.Pow(2, (int)tier);
        }

        public static double GetValue(Tier tier)
        {
            switch (GameVersionManager.CurrentVersion)
            {
                case GameVersion.Version3:
                    if (initialised && tierLookup.TryGetValue(tier, out MineralData data))
                        return data.Value;
                    return 1;
                case GameVersion.Version2:
                    return tier switch
                    {
                        Tier.Common => 1,
                        Tier.Uncommon => 5,
                        Tier.Rare => 25,
                        Tier.Epic => 125,
                        Tier.Legendary => 625,
                        _ => 1
                    };
                case GameVersion.Version1:
                    return tier switch
                    {
                        Tier.Common => 1,
                        Tier.Uncommon => 4,
                        Tier.Rare => 16,
                        Tier.Epic => 64,
                        Tier.Legendary => 256,
                        _ => 1
                    };
            }

            return 1;
        }
    }
}