﻿using System;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class ChunkData
    {
        [SerializeField]
        private ChunkSize chunkTypeID;

        public ChunkSize ChunkSize => chunkTypeID;

        [SerializeField]
        private int layers;
        
        public int Layers => layers;

        [SerializeField]
        private float scanTime;

        public float ScanTime => scanTime;
    }
}