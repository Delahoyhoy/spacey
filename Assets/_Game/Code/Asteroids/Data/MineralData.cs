using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class MineralData
    {
        [SerializeField]
        private string rarityID;

        [SerializeField]
        private double valueModifier;

        public double Value => valueModifier;

        [SerializeField]
        private double toughnessModifier;

        public double Toughness => toughnessModifier;
    }
}