﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public class ScannerManager : MonoBehaviour
    {
        [SerializeField]
        private ScannerSweep scannerPrefab;

        [SerializeField]
        private Transform scannerHolder;

        private List<ScannerSweep> activeScanners = new List<ScannerSweep>();

        private Queue<ScannerSweep> scannerPool = new Queue<ScannerSweep>();

        public const int PremiumScannerCount = 2;
        public const int TotalBaseScanners = 3;
        
        private int baseScanners;
        public int BaseScanners => baseScanners;
        
        private int premiumScanners;
        public int PremiumScanners => premiumScanners;  

        private int TotalScanners => baseScanners + premiumScanners;

        public bool AnyScannersAvailable => scannerPool.Count > 0;

        public int TotalAvailable => scannerPool.Count;

        public double HurryAllCost
        {
            get
            {
                double total = 0;
                for (int i = 0; i < activeScanners.Count; i++)
                    total += activeScanners[i].HurryCost;
                return total;
            }
        }

        public void ClearScanners()
        {
            while (activeScanners.Count > 0)
            {
                Destroy(activeScanners[0].gameObject);
                activeScanners.RemoveAt(0);
            }
            while (scannerPool.Count > 0)
                Destroy(scannerPool.Dequeue().gameObject);
        }

        public void UpdateScanners(int scanners, bool premiumActive)
        {
            baseScanners = scanners;
            premiumScanners = premiumActive ? PremiumScannerCount : 0;
            UpdateTotal(TotalScanners);
        }

        public void UpdateTotal(int newTotal)
        {
            int currentTotal = activeScanners.Count + scannerPool.Count;
            if (newTotal <= currentTotal)
                return;
            int toAdd = newTotal - currentTotal;

            for (int i = 0; i < toAdd; i++)
            {
                ScannerSweep newScanner = Instantiate(scannerPrefab, scannerHolder);
                scannerPool.Enqueue(newScanner);
            }
        }

        public void ScanComplete(ScannerSweep scanner)
        {
            if (activeScanners.Contains(scanner))
                activeScanners.Remove(scanner);
            if (!scannerPool.Contains(scanner))
                scannerPool.Enqueue(scanner);
        }

        public bool TryScan(Asteroid target)
        {
            if (!AnyScannersAvailable)
                return false;
            ScannerSweep scanner = scannerPool.Dequeue();
            activeScanners.Add(scanner);
            scanner.ScanAsteroid(target);
            return true;
        }

        public void HurryAll()
        {
            if (CurrencyController.Instance.GetCurrency(CurrencyType.HardCurrency) < HurryAllCost)
                return;
            for (int i = 0; i < activeScanners.Count; i++)
                activeScanners[i].TryHurry();
        }
    }
}