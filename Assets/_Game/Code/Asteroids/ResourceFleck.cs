using System;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public struct ResourceFleck
    {
        public SpriteRenderer displaySprite;
        public float percentile;

        public void SetColour(Color colour)
        {
            displaySprite.color = colour;
        }

        public void SetActive(bool active)
        {
            displaySprite.enabled = active;
        }
    }
}