﻿using System;
using System.Collections.Generic;
using Core.Gameplay.Version;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class AsteroidVersionPrefabs
    {
        [SerializeField]
        private List<GameVersion> validVersions;

        [SerializeField]
        private Asteroid[] prefabs;

        public bool GetValid(GameVersion version, out Asteroid[] asteroidPrefabs)
        {
            asteroidPrefabs = prefabs;
            return validVersions.Contains(version);
        }
    }
}