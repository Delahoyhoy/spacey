﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Core.Gameplay.Audio;
using Core.Gameplay.CameraControl;
using Core.Gameplay.Economy;
using Core.Gameplay.Outposts;
using Core.Gameplay.Save;
using Core.Gameplay.Sectors;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Asteroids
{
    public class Asteroid : MonoBehaviour
    {
        [SerializeField]
        private double[] resourceYields = new double[5];

        private int currentLayer;

        public int CurrentLayer
        {
            get => currentLayer;
            set
            {
                currentLayer = value;
                SetActiveLayer(value);
            }
        }

        private double[] ResourceYields => GetResourceYields();

        [SerializeField]
        private float minMiningDist = 0.75f,
            maxMiningDist = 1.125f,
            minLaserDist = 0.5f,
            maxLaserDist = 0.25f,
            rotationalRandom = 5;

        private const int maxShips = 12;

        private List<float> angles = new List<float>();

        [SerializeField]
        private AsteroidState state = AsteroidState.Unscanned;

        public AsteroidState State
        {
            get => state;
            set
            {
                state = value;
                OnStateChange?.Invoke(State);
                OnAnyStateChange?.Invoke(State);
                outpostPreview.SetActive(state == AsteroidState.Scanned && 
                                         GameVersionManager.CurrentVersion >= GameVersion.Version3);
                if (stateActionsLookup.TryGetValue(state, out UnityEvent action))
                    action?.Invoke();
                for (int i = 0; i < layers.Length; i++)
                    layers[i].SetFlecksActive(state > AsteroidState.Scanning);
            }
        }

        public Transform outpostAnchor, miningStationAnchor;

        private List<AsteroidLayer> asteroidLayers = new List<AsteroidLayer>();

        public int TotalLayers => asteroidLayers.Count;

        public delegate void AsteroidStateChange(AsteroidState newState);

        public event AsteroidStateChange OnStateChange;
        public static event AsteroidStateChange OnAnyStateChange;

        [SerializeField]
        private Transform asteroidObject;

        [SerializeField]
        private string rockID = "brownRock";

        public string RockID => rockID;

        [SerializeField]
        private int spriteIndex;

        private int index = 0;

        public int Index => index;

        [SerializeField]
        private int debrisIndex = 0;

        public int DebrisIndex => debrisIndex;

        [SerializeField]
        private int prefabIndex = 0;

        [SerializeField]
        private float scaleFactor = 1f;

        public float ScaleFactor => Mathf.Clamp(scaleFactor, 1f, 2f);

        private float debrisCooldown = 0;

        public bool DebrisReady => debrisCooldown <= 0;

        private string asteroidName;

        public string AsteroidName => asteroidName;

        public Vector3 Rotation => asteroidObject.localEulerAngles;

        [SerializeField]
        private Sprite asteroidSprite;

        public Sprite AsteroidSprite => asteroidSprite;

        public Action layerCompleteAction, chunkCompleteAction, claimRewardAction;

        public UnityEvent onCompleteLayer, onCompleteAsteroid;

        public Transform tutorialAnchor;

        [SerializeField]
        private AudioSource asteroidAudio;

        [SerializeField]
        private AudioClip debrisSpawn;

        [SerializeField]
        private ResourceChunkReward completionReward;

        [SerializeField]
        private ResourceLayerDisplay[] layers;

        [SerializeField]
        private OutpostPreview outpostPreview;

        [SerializeField]
        private AsteroidStateAction[] stateActions;

        private Dictionary<AsteroidState, UnityEvent> stateActionsLookup = new Dictionary<AsteroidState, UnityEvent>();

        private ResourceChunkRewardLocalAction[] LocalRewardActions => 
            GetComponentsInChildren<ResourceChunkRewardLocalAction>();

        public Action<float, double> onUpdateAmountMined;

        public bool HasReward => completionReward != null && completionReward.rewardMagnitude > 0;
        public ResourceChunkReward CompletionReward => completionReward;

        private ModifierEffect debrisCooldownModifier = new ModifierEffect(SectorModifier.DebrisRate);

        private float DebrisCooldownMult
        {
            get
            {
                float value = debrisCooldownModifier.Value;
                if (value <= 0)
                    return 1f;
                return 1f / value;
            }
        }

        public double GetApproxTimeRemaining(double mineSpeed, double efficiency)
        {
            double totalSeconds = 0;
            for (int i = currentLayer; i < asteroidLayers.Count; i++)
            {
                totalSeconds += asteroidLayers[i].GetSecondsToComplete(mineSpeed, efficiency);
            }

            return totalSeconds;
        }

        public float AsteroidProgress
        {
            get
            {
                if (asteroidLayers.Count <= 0)
                    return 1;
                if (State == AsteroidState.Finished || CurrentLayer >= asteroidLayers.Count)
                    return 1;
                double mined = 0, total = 0;
                for (int i = 0; i < asteroidLayers.Count; i++)
                {
                    total += asteroidLayers[i].TotalMass;
                    if (i > CurrentLayer)
                        continue;
                    mined += i < CurrentLayer ? asteroidLayers[i].TotalMass : asteroidLayers[i].MassMined;
                }

                if (total <= 0)
                    return 1;
                return (float)(mined / total);
            }
        }

        public void DeployOutpost()
        {
            OutpostManager.Instance.OpenDeployMenu(this);
        }

        public void InitialSetup()
        {
            asteroidObject.localEulerAngles = new Vector3(0, 0, Random.Range(0, 360f));
            scaleFactor = Random.Range(1f, 1.125f);
            asteroidObject.localScale = new Vector3(scaleFactor, scaleFactor, 1f);
            State = AsteroidState.Unscanned;
        }
        
        public void SetupData(AsteroidData data, int asteroidIndex)
        {
            index = asteroidIndex;
            asteroidLayers = data.asteroidLayers;
            currentLayer = data.currentLayer;
            completionReward = data.chunkReward;
            SetActiveLayer(currentLayer);
            bool alreadyCompleted = State.Equals(AsteroidState.Finished);
            if (currentLayer >= layers.Length || TotalMined >= TotalMass)
                State = AsteroidState.Finished;
            
            if (state.Equals(AsteroidState.Finished))
                CompleteAsteroid(alreadyCompleted);
        }

        public void SetupWithNoData()
        {
            if (GameVersionManager.CurrentVersion < GameVersion.Version3)
                return;
            State = AsteroidState.Finished;
            CompleteAsteroid(true);
        }

        public void SetActiveLayer(int layer)
        {
            int activeLayer = asteroidLayers.Count - layer - 1;
            for (int i = 0; i < layers.Length; i++)
            {
                bool match = activeLayer == i;
                layers[i].SetActive(match);
                if (match)
                    layers[i].Setup(asteroidLayers[i]);
            }
        }

        public double TotalMass
        {
            get
            {
                if (asteroidLayers.Count <= 0)
                    return 1;
                
                double total = 0;
                for (int i = 0; i < asteroidLayers.Count; i++)
                    total += asteroidLayers[i].TotalMass;
                
                if (total <= 0)
                    return 1;
                
                return total;
            }
        }

        private double TotalMined
        {
            get
            {
                if (asteroidLayers.Count <= 0)
                    return 1;
                if (State == AsteroidState.Finished || CurrentLayer > asteroidLayers.Count)
                    return TotalMass;
                double mined = 0;
                for (int i = 0; i < asteroidLayers.Count; i++)
                {
                    if (i > CurrentLayer)
                        continue;
                    mined += i < CurrentLayer ? asteroidLayers[i].TotalMass : asteroidLayers[i].MassMined;
                }
                return mined;
            }
        }

        public AsteroidData SaveAsteroidData()
        {
            AsteroidData saveData = new AsteroidData(CurrentLayer, asteroidLayers, completionReward);
            return saveData;
        }

        public string Save()
        {
            string data = JC_SaveManager.SaveVector3(transform.position) + "#"
                                                                         + SaveYield() + "#" + spriteIndex.ToString() + "#" + ((int)State) + "#" +
                                                                         index.ToString() + "#" + asteroidName + "#" + asteroidObject.localEulerAngles.z + "#" +
                                                                         prefabIndex.ToString() + "#" + ScaleFactor.ToString("0.###", CultureInfo.InvariantCulture);

            return data;
        }

        public void SpawnDebris()
        {
            if (asteroidAudio && debrisSpawn)
                asteroidAudio.PlayOneShot(debrisSpawn);
        }

        private void Awake()
        {
            for (int i = 0; i < stateActions.Length; i++)
            {
                if (!stateActionsLookup.ContainsKey(stateActions[i].State))
                    stateActionsLookup.Add(stateActions[i].State, stateActions[i].Action);
            }
        }

        public void SetMining(bool mining)
        {
            //if (asteroidAudio)
            //{
            //    if (mining && !asteroidAudio.isPlaying)
            //        asteroidAudio.Play();
            //    else if (!mining && asteroidAudio.isPlaying)
            //        asteroidAudio.Stop();
            //}
        }

        public void Load(string data, int asteroidIndex = 0)
        {
            if (string.IsNullOrEmpty(data))
                return;
            string[] splitData = data.Split('#');
            transform.position = JC_SaveManager.LoadVector3(splitData[0]);
            ParseYieldData(splitData[1]);
            int.TryParse(splitData[2], out spriteIndex);
            int stateIndex = 0;
            if (int.TryParse(splitData[3], out stateIndex))
            {
                AsteroidState newState = (AsteroidState) stateIndex;
                State = newState == AsteroidState.Scanning ? AsteroidState.Scanned : newState;
                if (State == AsteroidState.OutpostBuilt)
                    State = AsteroidState.Scanned;
            }

            //int.TryParse(splitData[4], out index);
            index = asteroidIndex;
            if (splitData.Length >= 6)
            {
                asteroidName = splitData[5];
            }
            else
                asteroidName = AsteroidNameHelper.GetName();

            AsteroidNameHelper.AddName(asteroidName);
            if (splitData.Length >= 7)
            {
                float angle = 0;
                if (!float.TryParse(splitData[6], out angle))
                    angle = Random.Range(0, 360f);
                asteroidObject.localEulerAngles = new Vector3(0, 0, angle);
            }
            else
                asteroidObject.localEulerAngles = new Vector3(0, 0, Random.Range(0, 360f));

            scaleFactor = Random.Range(1.375f, 1.625f);
            if (splitData.Length >= 9)
            {
                float.TryParse(splitData[8], out scaleFactor);
            }

            asteroidObject.localScale = new Vector3(ScaleFactor, ScaleFactor, 1f);
        }

        public void Reposition(Vector3 newPos)
        {
            transform.position = newPos;
        }

        private string SaveYield()
        {
            string data = "";
            for (int i = 0; i < resourceYields.Length; i++)
            {
                if (i > 0)
                    data += ",";
                data += resourceYields[i].ToString();
            }

            return data;
        }

        private void ParseYieldData(string data)
        {
            string[] splitData = data.Split(',');
            for (int i = 0; i < splitData.Length; i++)
            {
                if (i < resourceYields.Length)
                {
                    double.TryParse(splitData[i], out resourceYields[i]);
                }
            }
        }

        public void CompleteAsteroid(bool alreadyFinished)
        {
            onCompleteAsteroid?.Invoke();
            //if (((int)StatTracker.Instance.IncrementStat("asteroidsCompleted", 1) % )
            if (!alreadyFinished)
            {
                StatTracker.Instance.IncrementStat("asteroidsCompleted", 1);
                if (AsteroidUnlockSats.Instance.CheckShouldUnlock(out int count))
                {
                    CoinBurster.Instance.NewBurstFromWorld(CurrencyType.Satoshis, transform, count, count);
                    CurrencyController.Instance.CurrencyEvent(CurrencyType.Satoshis, count, "asteroid",
                        "complete_asteroid");
                }
            }

            if (!HasReward || !ResourceChunkRewardManager.Instance.TryGetRewardDisplay(completionReward.rewardType, transform, 
                    out ResourceChunkRewardDisplay rewardDisplay))
                return;

            void TapReward()
            {
                DisplayReward();
                BasicCameraController.Instance.Pan(rewardDisplay.transform);
            }
            
            rewardDisplay.Display(TapReward);
            claimRewardAction += rewardDisplay.CompleteClaim;
        }

        public void DisplayReward()
        {
            ResourceChunkRewardManager.Instance.DisplayReward(completionReward, ClaimReward);
        }

        public void ClaimReward()
        {
            claimRewardAction?.Invoke();
            completionReward = null;
        }

        public void SetupAsteroidName()
        {
            asteroidName = AsteroidNameHelper.GetName();
            AsteroidNameHelper.AddName(asteroidName);
        }

        public void SetupResourceYields(double[] template, int asteroidIndex)
        {
            index = asteroidIndex;
            double total = 0;
            double[] result = new double[template.Length];
            for (int i = 0; i < result.Length; i++)
            {
                double value = template[i] * Random.value;
                result[i] = value;
                total += value;
            }

            if (total == 0)
            {
                result[0] = 1;
                total = 1;
            }

            double newTotal = 0;
            for (int i = 0; i < result.Length; i++)
            {
                double value = Math.Floor((result[i] / total) * 100) / 100d;
                result[i] = value;
                newTotal += value;
            }

            double remainder = 1d - newTotal;
            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] > 0)
                {
                    resourceYields[i] = result[i] + remainder;
                    remainder = 0;
                }
                else
                    resourceYields[i] = 0;
            }

            InitialSetup();
            // asteroidObject.localEulerAngles = new Vector3(0, 0, Random.Range(0, 360f));
            // scaleFactor = Random.Range(1.375f, 1.625f);
            // asteroidObject.localScale = new Vector3(scaleFactor, scaleFactor, 1f);
        }

        private void OnDrawGizmosSelected()
        {
            Vector3[] innerShip = GetPoints(minMiningDist),
                outerShip = GetPoints(maxMiningDist),
                innerLaser = GetPoints(minLaserDist),
                outerLaser = GetPoints(maxLaserDist);
            Gizmos.color = Color.green;
            for (int i = 1; i < innerShip.Length; i++)
                Gizmos.DrawLine(transform.position + innerShip[i - 1], transform.position + innerShip[i]);
            for (int i = 1; i < outerShip.Length; i++)
                Gizmos.DrawLine(transform.position + outerShip[i - 1], transform.position + outerShip[i]);
            Gizmos.color = Color.red;
            for (int i = 1; i < innerLaser.Length; i++)
                Gizmos.DrawLine(transform.position + innerLaser[i - 1], transform.position + innerLaser[i]);
            for (int i = 1; i < outerLaser.Length; i++)
                Gizmos.DrawLine(transform.position + outerLaser[i - 1], transform.position + outerLaser[i]);
        }

        //private void OnDrawGizmos()
        //{
        //    Vector3[] minDist = GetPoints(12f);
        //    Gizmos.color = Color.blue;
        //    for (int i = 1; i < minDist.Length; i++)
        //        Gizmos.DrawLine(transform.position + minDist[i - 1], transform.position + minDist[i]);
        //}

        private Vector3[] GetPoints(float radius)
        {
            List<Vector3> points = new List<Vector3>();
            for (float f = 0; f < Mathf.PI * 2; f += Mathf.PI / 12f)
            {
                Vector3 pos = new Vector3(Mathf.Sin(f), Mathf.Cos(f), 0) * radius;
                points.Add(pos);
            }

            points.Add(new Vector3(Mathf.Sin(0), Mathf.Cos(0), 0) * radius);
            return points.ToArray();
        }

        public double[] GetResourceYields()
        {
            if (GameVersionManager.CurrentVersion < GameVersion.Version3 || asteroidLayers.Count <= 0)
                return resourceYields;
            if (CurrentLayer >= asteroidLayers.Count)
                return new double[] {0, 0, 0, 0, 0};
            return asteroidLayers[CurrentLayer].MineralDistribution;
        }

        public float GetMiningAngle()
        {
            if (angles.Count <= 0)
            {
                for (int i = 0; i < maxShips; i++)
                {
                    angles.Add((((float) i / maxShips) * 360f) + Random.Range(-rotationalRandom, rotationalRandom));
                }
            }

            int selectedIndex = Random.Range(0, angles.Count);
            float selection = angles[selectedIndex];
            angles.RemoveAt(selectedIndex);
            return selection * Mathf.Deg2Rad;
        }

        private int GetMinAvailable()
        {
            double[] resources = ResourceYields;

            for (int i = 0; i < Enum.GetValues(typeof(Tier)).Length; i++)
            {
                if (resources[i] > 0)
                    return i;
            }

            return Enum.GetValues(typeof(Tier)).Length;
        }

        // Old distribution
        private static int[] efficiencySubtract = {0, 2, 6, 16, 32};
        private static double[] efficiencyCoefficients = {1d, 1.5d, 2d, 2.5d, 3d};
        
        // New distribution
        private static double[] efficiencyPower = {0.8, 1, 1.25, 1.5625, 1.953125};
        private static double[] efficiencyMult = {1d, 0.5d, 0.25d, 0.125d, 0.0625d};

        public static double[] GetEfficiencyManifest(double efficency, int minimumTier = 0)
        {
            double[] manifest = new double[5];
            if (efficency <= 0)
                efficency = 1;
            for (int i = 0; i < manifest.Length; i++)
            {
                if (i < minimumTier)
                {
                    manifest[i] = 0;
                    continue;
                }

                // double temp = efficiencyCoefficients[i - minimumTier] *
                //               (efficency - efficiencySubtract[i - minimumTier]);
                double temp = Math.Pow(efficiencyMult[i - minimumTier] * efficency, efficiencyPower[i - minimumTier]);
                manifest[i] = temp > 0 ? temp * temp : 0;
            }
            return manifest;
        }

        public double MineAmount(double tons, double efficiency)
        {
            double[] efficiencyManifest = GetEfficiencyManifest(efficiency, GetMinAvailable());
            double[] result = new double[5];
            double total = 0;
            for (int i = 0; i < result.Length; i++)
            {
                double resource = ResourceYields[i] * efficiencyManifest[i];
                result[i] = resource;
                total += resource;
            }

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (result[i] / total) * tons;
            }

            double finalResult = 0;
            for (int i = 0; i < result.Length; i++)
            {
                finalResult += result[i] * TierHelper.GetValue((Tier) i);
            }

            return finalResult;
        }

        public void Mine(double speed, ref MineralLoad load, double efficiency)
        {
            double[] result = new double[5];
            double[] efficiencyManifest = GetEfficiencyManifest(efficiency, GetMinAvailable());
            double total = 0;
            for (int i = 0; i < result.Length; i++)
            {
                double resource = ResourceYields[i] * efficiencyManifest[i];
                result[i] = resource;
                total += resource;
            }

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (result[i] / total) * speed;
            }

            load.AddMinerals(result);
        }

        public bool Mine(double amount, double efficiency, ref MineralLoad load)
        {
            if (CurrentLayer >= asteroidLayers.Count)
                return true;
            double remainder = amount;
            double[] mined = new double[5];
            while (asteroidLayers[CurrentLayer].Mine(remainder, efficiency, ref mined))
            {
                layerCompleteAction?.Invoke();
                onCompleteLayer?.Invoke();
                CurrentLayer++;
                if (CurrentLayer >= asteroidLayers.Count)
                    break;
            }

            load.AddMinerals(mined);

            UpdateAmountMined();
            
            bool finished = CurrentLayer >= asteroidLayers.Count;

            if (!finished) 
                return false;
            chunkCompleteAction?.Invoke();
            
            CompleteAsteroid(false);
            
            State = AsteroidState.Finished;

            return true;
        }

        public void UpdateAmountMined()
        {
            double total = TotalMass, amountMined = TotalMined;
            onUpdateAmountMined?.Invoke(1f - (float)(amountMined / total), total - amountMined);
        }

        public double GetGoldEarnedPerSecond(double speed, double efficiency)
        {
            if (GameVersionManager.CurrentVersion >= GameVersion.Version3)
            {
                if (State == AsteroidState.Finished || asteroidLayers.Count <= 0)
                    return 0;
                return asteroidLayers[CurrentLayer].GetMinedPerSecond(speed, efficiency);
            }
            
            double[] result = new double[5];
            double[] efficiencyManifest = GetEfficiencyManifest(efficiency, GetMinAvailable());
            double total = 0;
            for (int i = 0; i < result.Length; i++)
            {
                double resource = ResourceYields[i] * efficiencyManifest[i];
                result[i] = resource;
                total += resource;
            }

            double miningDur = 0.5d * speed;
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (result[i] / total) * miningDur;
            }

            return (result[0] * TierHelper.GetValue(Tier.Common)) +
                   (result[1] * TierHelper.GetValue(Tier.Uncommon)) +
                   (result[2] * TierHelper.GetValue(Tier.Rare)) +
                   (result[3] * TierHelper.GetValue(Tier.Epic)) +
                   (result[4] * TierHelper.GetValue(Tier.Legendary));

        }

        public void SetState(AsteroidState newState)
        {
            State = newState;
        }

        public Vector2 GetMiningDistance()
        {
            float miningDist = Random.Range(minMiningDist, maxMiningDist);
            float laserLength = miningDist - Random.Range(minLaserDist, maxLaserDist);
            return new Vector2(miningDist, laserLength) * (ScaleFactor / 1.5f);
        }

        public Vector3 GetRandomLaserPos()
        {
            float angle = Random.Range(0, 360f) * Mathf.Deg2Rad;
            float distance = Random.Range(minLaserDist, maxLaserDist) * ScaleFactor;
            float x = Mathf.Sin(angle) * distance;
            float y = Mathf.Cos(angle) * distance;
            Vector3 pos = transform.position + (new Vector3(x, y, 0));
            pos.z = 0;
            return pos;
        }

        // Start is called before the first frame update
        private void Start()
        {
            debrisCooldown = Random.Range(15f, 60f);
            if (asteroidAudio)
                asteroidAudio.mute = AudioManager.Instance.Muted;
        }

        private void SetMuted(bool muted)
        {
            if (asteroidAudio)
                asteroidAudio.mute = muted;
        }

        private void OnEnable()
        {
            AudioManager.OnSetMute += SetMuted;
        }

        private void OnDestroy()
        {
            AudioManager.OnSetMute -= SetMuted;
        }

        // Update is called once per frame
        private void Update()
        {
            if (debrisCooldown > 0)
                debrisCooldown -= Time.deltaTime;
        }

        public void ResetCooldown()
        {
            debrisCooldown = Random.Range(30f, 150f) * DebrisCooldownMult;
        }

        public void OpenAsteroidInfo()
        {
            AsteroidManager.Instance.OpenMenu(this);
        }
    }
}