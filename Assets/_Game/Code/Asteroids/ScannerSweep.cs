﻿using System;
using System.Collections;
using Core.Gameplay.Audio;
using Core.Gameplay.Reset;
using Core.Gameplay.Save;
using Core.Gameplay.Tutorial;
using Core.Gameplay.Version;
using Fumb.RemoteConfig;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Asteroids
{
    public class ScannerSweep : MonoBehaviour
    {
        [SerializeField]
        private Transform scannerDrone;

        public bool scanning;

        public Action<bool> onScanActive;
        public Action<ScannerSweep> onScanComplete;

        [SerializeField]
        private UnityEvent onCompleteScan;

        [SerializeField]
        private Image scanProgress;

        [SerializeField]
        private Button hurryButton;

        [SerializeField]
        private Text hurryText, progressText;

        [SerializeField]
        private float timePerSweep = 2.5f;

        private float TimePerSweep => timePerSweep / (scanSpeedMult * bonusSpeedMult);

        // [SerializeField]
        // private float secondsPerHc = 5f;.

        private float scanSpeedMult = 1f;
        private float bonusSpeedMult = 1f;

        [SerializeField]
        private RemoteFloat secondsPerHc = new RemoteFloat("scannerSecondsPerHcCost", 30f);

        [SerializeField]
        private Transform scannerGraphic;
        
        private int sweep;
        
        private bool setup;

        private static ScannerSweep instance;

        public static ScannerSweep Instance
        {
            get
            {
                if (!instance) instance = ObjectFinder.FindObjectOfType<ScannerSweep>();
                return instance;
            }
        }

        private void SetBonusMult(double multiplier)
        {
            bonusSpeedMult = (float)multiplier;
        }

        public void ScanAsteroid(Asteroid asteroid, Action<ScannerSweep> completeAction = null)
        {
            if (scanning)
                return;
            transform.position = asteroid.transform.position;
            AudioManager.Play("Scan", 0.25f);
            StartCoroutine(Scan(asteroid));
            onScanComplete = completeAction;
        }

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            ResetManager.Instance.beginReset += resetType =>
            {
                if (scanning)
                    skipped = true;
            };
            if (setup)
                return;
            SetBonusMult(GameManager.Instance.BonusMultiplier);
            GameManager.OnSetBonusActive += SetBonusMult;
            setup = true;
        }

        private void OnEnable()
        {
            JC_SaveManager.OnReturnToGame += HandleReturn;
        }

        private void OnDestroy()
        {
            JC_SaveManager.OnReturnToGame -= HandleReturn;
            if (setup)
                GameManager.OnSetBonusActive -= SetBonusMult;
        }
        
        private void HandleReturn(double seconds)
        {
            sweep += (int)(seconds / TimePerSweep);
        }

        public void SetScanSpeedMult(float speed)
        {
            scanSpeedMult = speed;
        }

        private int hurryCost = 0;
        private bool skipped = false;

        public int HurryCost => hurryCost;

        public float percentComplete = 0;

        private IEnumerator Scan(Asteroid asteroid)
        {
            hurryButton.gameObject.SetActive(FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core));
            skipped = false;
            scanning = true;
            asteroid.SetState(AsteroidState.Scanning);
            percentComplete = 0;
            //TutorialManager.OnTrigger("scan");
            onScanActive?.Invoke(true);
            scannerDrone.gameObject.SetActive(true);
            scannerDrone.transform.localScale = Vector3.zero;
            scanProgress.gameObject.SetActive(true);
            scanProgress.fillAmount = 0;
            progressText.text = "";
            GameManager.Instance.RefreshCamera();
            for (float t = 0; t < 1f; t += Time.deltaTime * 3f)
            {
                scannerDrone.localScale = Vector3.one * (t * 2f);
                yield return null;
            }

            int sweepsToDo =
                (2 * Mathf.CeilToInt(Vector3.Distance(asteroid.transform.position, GameManager.Instance.FogCentre) /
                                     8)) + 2;
            
            if (GameVersionManager.CurrentVersion >= GameVersion.Version3)
            {
                sweepsToDo = GetScanSweepsForLayers(asteroid.TotalLayers);
            }
            
            if (!FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core))
                sweepsToDo = Mathf.RoundToInt(5f / TimePerSweep);

            scannerDrone.transform.localScale = Vector3.one * 2f;
            for (sweep = 0; sweep < sweepsToDo; sweep++)
            {
                //hurryCost = sweepsToDo - i;
                hurryCost = Mathf.CeilToInt((sweepsToDo * TimePerSweep) / secondsPerHc);
                hurryButton.interactable = hurryCost <= CurrencyController.Instance.CurrentDiamonds;
                hurryText.text = CurrencyController.FormatToString(hurryCost);
                float targetAngle = Random.Range(0, 2 * Mathf.PI);
                Vector3 position = new Vector3(Mathf.Sin(targetAngle), Mathf.Cos(targetAngle), 0) * 3f;
                for (float f = 0; f < TimePerSweep; f += Time.deltaTime)
                {
                    if (sweep > sweepsToDo)
                    {
                        skipped = true;
                        break;
                    }
                    scannerDrone.localPosition = Vector3.Lerp(scannerDrone.localPosition, position, Time.deltaTime);
                    float angle = (Mathf.Atan2(-scannerDrone.localPosition.y, -scannerDrone.localPosition.x) *
                                   Mathf.Rad2Deg) - 90f;
                    scannerDrone.localEulerAngles = new Vector3(0, 0, angle);
                    percentComplete = ((sweep * TimePerSweep) + f) / (sweepsToDo * TimePerSweep);
                    scanProgress.fillAmount = percentComplete;
                    progressText.text = $"{percentComplete * 100:##0}%";
                    if (skipped)
                        break;
                    yield return null;
                }

                if (skipped)
                    break;
            }

            scanProgress.fillAmount = 1;
            progressText.text = "100%";
            hurryButton.gameObject.SetActive(false);
            for (float t = 0; t < 1f; t += Time.deltaTime * 3f)
            {
                scannerDrone.transform.localScale = Vector3.one * ((1 - t) * 2f);
                yield return null;
            }

            scanProgress.gameObject.SetActive(false);
            scannerDrone.transform.localScale = Vector3.zero;
            scannerDrone.gameObject.SetActive(false);
            asteroid.SetState(AsteroidState.Scanned);
            //TutorialManager.OnTrigger("scanning");
            onScanActive?.Invoke(false);
            onCompleteScan?.Invoke();
            onScanComplete?.Invoke(this);
            scanning = false;
        }
        
        private int GetScanSweepsForLayers(int layers)
        {
            AsteroidManager.Instance.TryGetScanTime((ChunkSize)layers, out float time);
            return Mathf.RoundToInt(time / TimePerSweep);
        }

        public int GetCostToFullyScan(int layers)
        {
            int sweeps = GetScanSweepsForLayers(layers);
            return Mathf.CeilToInt((sweeps * TimePerSweep) / secondsPerHc);
        }

        public void Skip()
        {
            skipped = true;
        }

        public void Hurry()
        {
            TryHurry();
        }

        public bool TryHurry()
        {
            if (!CurrencyController.Instance.SpendCurrency(hurryCost, CurrencyType.HardCurrency, "scanner", "hurry_scan"))
                return false;
            skipped = true;
            return true;
        }

        private void Update()
        {
            scannerGraphic.transform.localScale = Vector3.one * scannerDrone.localPosition.magnitude;
        }
    }

}