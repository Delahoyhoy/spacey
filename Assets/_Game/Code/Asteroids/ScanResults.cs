﻿using System;
using Core.Gameplay.Audio;
using Core.Gameplay.Outposts;
using Core.Gameplay.Version;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Gameplay.Asteroids
{
    public class ScanResults : AnchoredUI
    {
        private Asteroid selectedAsteroid;

        [SerializeField]
        private TierProbability[] tierProbabilities = new TierProbability[5];

        [SerializeField]
        private Button scanButton, buildButton, mineButton;

        [SerializeField]
        private GameObject compositionPanel, deployButton;

        [SerializeField]
        private Text buildCostText;
        
        [SerializeField]
        private Slider asteroidProgress;

        [SerializeField]
        private Text asteroidMassRemaining, timeRemaining;

        [SerializeField]
        private UnityEvent<bool> onSetOutpostsAvailable;

        public void Setup(Asteroid asteroid)
        {
            selectedAsteroid = asteroid;
            Target = asteroid.transform;
            asteroid.OnStateChange += SetState;
            asteroid.onUpdateAmountMined += UpdateRemainingMass;
            asteroid.UpdateAmountMined();
            double[] resourceYields = asteroid.GetResourceYields();
            for (int i = 0; i < tierProbabilities.Length; i++)
            {
                tierProbabilities[i].Setup(resourceYields[i]);
            }

            SetState(selectedAsteroid.State);
        }

        private void CheckTarget(int target)
        {
            mineButton.gameObject.SetActive(false);
            // if (selectedAsteroid.Index == target || selectedAsteroid.State == AsteroidState.Unscanned ||
            //     selectedAsteroid.State == AsteroidState.Scanning)
            // {
            //     //Debug.Log(selectedAsteroid.Index + ", " + (selectedAsteroid.Index == target) + ", " + selectedAsteroid.state);
            //     mineButton.gameObject.SetActive(false);
            // }
            // else
            //     mineButton.gameObject.SetActive(false);
        }

        public void ScanAsteroid()
        {
            AudioManager.Play("Click");
            GameManager.Instance.ScanAsteroid(selectedAsteroid);
        }

        public void MineAsteroid()
        {
            //TutorialManager.OnTrigger("targetAsteroid");
            MiningStation.Instance.SetTarget(selectedAsteroid.Index);
        }

        public void SelectDeploy()
        {
            OutpostManager.Instance.OpenDeployMenu(selectedAsteroid);
        }

        private void SetState(AsteroidState state)
        {
            switch (state)
            {
                case AsteroidState.Unscanned:
                    compositionPanel.SetActive(false);
                    asteroidProgress.gameObject.SetActive(false);
                    deployButton.SetActive(false);
                    break;
                case AsteroidState.Scanned:
                    compositionPanel.SetActive(GameVersionManager.CurrentVersion < GameVersion.Version3);
                    mineButton.gameObject
                        .SetActive(false); //(MiningStation.Instance.CurrentTarget != selectedAsteroid.Index);
                    asteroidProgress.gameObject.SetActive(false);
                    SetOutpostAvailable(OutpostManager.Instance.AnyOutpostsAvailable);
                    deployButton.SetActive(GameVersionManager.CurrentVersion >= GameVersion.Version3);
                    break;
                case AsteroidState.OutpostBuilt:
                    compositionPanel.SetActive(false);
                    mineButton.gameObject
                        .SetActive(false); //(MiningStation.Instance.CurrentTarget != selectedAsteroid.Index);
                    asteroidProgress.gameObject.SetActive(GameVersionManager.CurrentVersion >= GameVersion.Version3);
                    deployButton.SetActive(false);
                    break;
                case AsteroidState.Scanning:
                    compositionPanel.SetActive(false);
                    asteroidProgress.gameObject.SetActive(false);
                    deployButton.SetActive(false);
                    break;
                case AsteroidState.Finished:
                    compositionPanel.SetActive(false);
                    asteroidProgress.gameObject.SetActive(false);
                    deployButton.SetActive(false);
                    break;
            }
            scanButton.gameObject.SetActive(GameVersionManager.CurrentVersion < GameVersion.Version3 &&
                                            state == AsteroidState.Unscanned);
            timeRemaining.gameObject.SetActive(state == AsteroidState.OutpostBuilt);
            mineButton.gameObject.SetActive(false);
        }

        public void Build()
        {
            OutpostManager.Instance.BuildOutpost(selectedAsteroid.Index);
            // if (!OutpostManager.Instance.BuildOutpost(selectedAsteroid.Index) ||
            //     !AsteroidUnlockSats.Instance.CheckShouldUnlock(out int reward)) 
            //     return;
            // CoinBurster.Instance.NewBurstFromWorld(CurrencyType.Satoshis, selectedAsteroid.transform, reward,
            //     reward);
            // AnalyticsHelper.CurrencyEarn(CurrencyType.Satoshis, reward, true, "BuildOutpost");
        }

        public void RefreshCost(double cost, double currency)
        {
            buildCostText.text = CurrencyController.FormatToString(cost);
            buildButton.interactable = currency >= cost;
        }

        private void SetButton(bool active)
        {
            scanButton.interactable = !active;
        }

        public void SetBuildButtonAvailable(bool available)
        {
            buildButton.gameObject.SetActive(available);
        }

        private void OnEnable()
        {
            ScannerSweep.Instance.onScanActive += SetButton;
            GameManager.OnCameraMove += SetPosition;
            GameManager.OnZoomChange += SetScale;
            GameManager.OnRefreshOutpostCost += RefreshCost;
            OutpostManager.Instance.onSetOutpostAvailable += SetOutpostAvailable;
            MiningStation.OnTargetAsteroid += CheckTarget;
        }

        private void SetOutpostAvailable(bool available)
        {
            onSetOutpostsAvailable?.Invoke(available);
        }

        private void OnDestroy()
        {
            if (ScannerSweep.Instance)
                ScannerSweep.Instance.onScanActive -= SetButton;
            GameManager.OnCameraMove -= SetPosition;
            GameManager.OnZoomChange -= SetScale;
            GameManager.OnRefreshOutpostCost -= RefreshCost;
            if (selectedAsteroid)
                selectedAsteroid.OnStateChange -= SetState;
            MiningStation.OnTargetAsteroid -= CheckTarget;
        }
        
        public void UpdateRemainingMass(float percentage, double totalRemaining)
        {
            asteroidProgress.value = percentage;
            asteroidMassRemaining.text = $"{CurrencyController.FormatToString(totalRemaining)} tons";
        }

        public static string FormatRemainingTime(TimeSpan timeSpan)
        {
            if (timeSpan.TotalDays >= 1)
                return $"{timeSpan.Days}d {timeSpan.Hours}h";
            if (timeSpan.TotalHours >= 1)
                return $"{timeSpan.Hours}h {timeSpan.Minutes}m";
            if (timeSpan.TotalMinutes > 1)
                return $"{timeSpan.Minutes + 1}m";
            return $"<1m";
        }
        
        private void Update()
        {
            if (!selectedAsteroid || selectedAsteroid.State != AsteroidState.OutpostBuilt)
                return;
            if (!OutpostManager.Instance.GetOutpostMiningAsteroid(selectedAsteroid.Index, out Outpost outpost))
                return;
            timeRemaining.text = FormatRemainingTime(outpost.GetTimeRemainingOnTarget());
        }
    }

}