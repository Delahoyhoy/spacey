using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public class AsteroidSetupData
    {
        private Vector3 position;
        public Vector3 Position => position;
        
        private AsteroidData asteroidData;
        public AsteroidData Data => asteroidData;

        public AsteroidSetupData(Vector3 pos, AsteroidData data)
        {
            position = pos;
            asteroidData = data;
        }
    }
}