using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class AsteroidLayer
    {
        [SerializeField]
        private double totalMass;
        [SerializeField]
        private double remainingMass;

        public double[] mineralDistribution;

        public double TotalMass => totalMass;
        public double[] MineralDistribution => mineralDistribution;
        public double MassMined => totalMass - remainingMass;
        public float TotalRemainingPercent => (float) (remainingMass / totalMass);
        
        private double toughness;
        private bool initialised;

        public Tier GetTierAtPercentile(float percentile)
        {
            if (mineralDistribution == null)
                return Tier.Common;
            List<TierQuantity> sortedDistribution = new List<TierQuantity>();

            for (int i = 0; i < mineralDistribution.Length; i++)
            {
                sortedDistribution.Add(new TierQuantity((Tier)i, mineralDistribution[i]));
            }

            sortedDistribution = sortedDistribution.OrderBy(tierQuantity => tierQuantity.Amount).ToList();

            double remainder = percentile;
            for (int i = 0; i < sortedDistribution.Count; i++)
            {
                if (remainder <= sortedDistribution[i].Amount)
                    return sortedDistribution[i].MineralTier;
                remainder -= sortedDistribution[i].Amount;
            }

            return Tier.Common;
        }

        public Tier GetHighestDistribution()
        {
            int highestTier = 0;
            double highestDistribution = 0;
            for (int i = 0; i < mineralDistribution.Length; i++)
            {
                if (mineralDistribution[i] < highestDistribution)
                    continue;
                highestTier = i;
                highestDistribution = mineralDistribution[i];
            }

            return (Tier)highestTier;
        }

        public AsteroidLayer(double total, double remaining, double[] minerals)
        {
            totalMass = total;
            remainingMass = double.IsNaN(remaining) ? 0 : remaining;

            double totalMinerals = 0;

            for (int i = 0; i < minerals.Length; i++)
                totalMinerals += minerals[i];

            mineralDistribution = new double[minerals.Length];
            for (int i = 0; i < mineralDistribution.Length; i++)
            {
                mineralDistribution[i] = minerals[i] / totalMinerals;
                toughness += mineralDistribution[i] * Math.Pow(2, i);
            }

            initialised = true;
        }

        public double GetSecondsToComplete(double totalMineSpeed, double efficiency)
        {
            SetupToughness();
            double adjustedSpeed = totalMineSpeed;
            if (toughness > 0)
                adjustedSpeed *= Clamp01((1 + efficiency / 100) / toughness);
            if (adjustedSpeed <= 0)
                return 0;
            if (double.IsNaN(remainingMass))
                remainingMass = 0;
            return remainingMass / adjustedSpeed;
        }

        private void SetupToughness()
        {
            if (initialised)
                return;
            toughness = 0;
            for (int i = 0; i < mineralDistribution.Length; i++)
                toughness += mineralDistribution[i] * Math.Pow(2, i);
            initialised = true;
        }
        
        public AsteroidLayer(double total, double[] minerals) : this(total, total, minerals) {}

        /// <summary>
        /// Mine the asteroid layer for a set amount
        /// </summary>
        /// <param name="amount">Total mass to mine</param>
        /// <param name="efficiency">Efficiency rating of the mining ship (combats resource toughness)</param>
        /// <param name="mined">Resource tiers acquired</param>
        /// <returns>Whether the amount mined finishes the resource layer</returns>
        public bool Mine(double amount, double efficiency, ref double[] mined)
        {
            SetupToughness();
            double toughnessMult = Clamp01((1 + efficiency / 100) / toughness);
            if (toughness > 0)
                amount *= toughnessMult;
            bool depleted = remainingMass <= amount;
            if (depleted)
            {
                amount = remainingMass;
                remainingMass = 0;
            }
            else
                remainingMass -= amount;
            if (double.IsNaN(remainingMass))
                remainingMass = 0;
            //mined = new double[mineralDistribution.Length];
            for (int i = 0; i < mined.Length; i++)
                mined[i] = amount * mineralDistribution[i];
            return depleted;
        }

        public double GetMinedPerSecond(double amount, double efficiency)
        {
            SetupToughness();
            double toughnessMult = Clamp01((1 + efficiency / 100) / toughness);
            if (toughness > 0)
                amount *= toughnessMult;
            double total = 0;
            for (int i = 0; i < mineralDistribution.Length; i++)
            {
                total += (mineralDistribution[i] * amount) * TierHelper.GetValue((Tier)i);
            }

            return total;
        }

        private static double Clamp01(double value)
        {
            if (value < 0)
                return 0;
            if (value > 1)
                return 1;
            return value;
        }
    }
}