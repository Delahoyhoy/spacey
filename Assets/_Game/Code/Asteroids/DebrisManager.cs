﻿using System.Collections;
using System.Collections.Generic;
using Core.Gameplay.CameraControl;
using Core.Gameplay.VIP;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public class DebrisManager : MonoBehaviour
    {

        [SerializeField]
        private Debris[] debrisPrefabs;

        [SerializeField]
        private Transform debrisParent;

        private static DebrisManager instance;

        public static DebrisManager Instance => instance ??= ObjectFinder.FindObjectOfType<DebrisManager>();

        public bool ForceDebris { get; private set; }

        public delegate void DestroyDebris();

        public static event DestroyDebris OnDestroyDebris;

        private void Awake()
        {
            instance = this;
        }

        public void SetForceDebris(bool forceDebris)
        {
            ForceDebris = forceDebris;
        }

        public void CreateDebris(double diamonds, Vector3 position, int index)
        {
            //TutorialManager.Instance.Trigger("waitDebris");
            Debris newDebris = Instantiate(debrisPrefabs[index], debrisParent);
            Vector3 direction = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0);
            if (direction.magnitude == 0)
                direction = Vector3.right;
            newDebris.Setup(GameManager.Instance.GetGoldEarned(VIPManager.Instance.GetValue(VIPCategory.Debris) * 60d, false),
                diamonds, position, direction.normalized);
        }

        public void FocusOnDebris()
        {
            Debris debris = ObjectFinder.FindObjectOfType<Debris>();
            if (debris)
                BasicCameraController.Instance.LockOnTarget(debris.transform);
            //else
                //TutorialManager.Instance.Trigger("debris");
        }

        public void ClearExistingDebris()
        {
            OnDestroyDebris?.Invoke();
        }
    }
}
