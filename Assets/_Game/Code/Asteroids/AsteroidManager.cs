using System;
using System.Collections.Generic;
using Core.Gameplay.Version;
using Data;
using Fumb.Data;
using Fumb.Save;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Asteroids
{
    public class AsteroidManager : MonoBehaviour
    {
        private static AsteroidManager instance;
        public static AsteroidManager Instance => instance ??= ObjectFinder.FindObjectOfType<AsteroidManager>();

        [SerializeField]
        private AsteroidVersionPrefabs[] versionPrefabs;

        [SerializeField]
        private Asteroid[] asteroidPrefabs;

        [SerializeField]
        private ScannerScreen scannerScreen;

        [SerializeField]
        private int[] sweepsPerLayerCount = { 3, 8, 15, 30, 60 };

        private bool prefabsInitialised;

        public Asteroid[] AsteroidPrefabs
        {
            get
            {
                if (prefabsInitialised)
                    return asteroidPrefabs;
                GameVersion version = GameVersionManager.CurrentVersion;
                for (int i = 0; i < versionPrefabs.Length; i++)
                {
                    if (!versionPrefabs[i].GetValid(version, out Asteroid[] prefabs))
                        continue;
                    asteroidPrefabs = prefabs;
                    prefabsInitialised = true;
                    break;
                }
                return asteroidPrefabs;
            }
        }

        [SerializeField]
        private AsteroidInfoDisplay asteroidMenu;

        [SerializeField]
        private List<Asteroid> asteroids = new List<Asteroid>();
        
        public Action<float> onUpdateProgress;
        public Action<int, int> onUpdateCompletion;
        public Action<double, double> onUpdateMassProgress;

        [SerializeField]
        private UnityEvent<int, int> updateCompletion;

        [SerializeField]
        private UnityEvent<double, double> updateMassProgress;
        private double TotalMass
        {
            get
            {
                double total = 0;
                for (int i = 0; i < asteroids.Count; i++)
                    total += asteroids[i].TotalMass;
                return total;
            }
        }

        [ManagedSaveValue("AsteroidManagerAsteroidData")]
        private SaveAsteroidData saveAsteroidData;
        
        public List<Asteroid> Asteroids
        {
            get => asteroids;
            set => asteroids = value;
        }

        public int AsteroidCount => asteroids.Count;

        public int CompletedAsteroids
        {
            get
            {
                int total = 0;
                for (int i = 0; i < asteroids.Count; i++)
                    if (asteroids[i].State == AsteroidState.Finished)
                        total++;
                
                return total;
            }
        }

        public float LevelProgress
        {
            get
            {
                float total = 0, max = AsteroidCount;
                for (int i = 0; i < asteroids.Count; i++)
                    total += asteroids[i].AsteroidProgress;
                
                return total / max;
            }
        }

        private Dictionary<ChunkSize, ChunkData> chunkDataLookup = new Dictionary<ChunkSize, ChunkData>();

        private bool initialised;
        
        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            onUpdateCompletion += (current, total) => updateCompletion?.Invoke(current, total);
            onUpdateMassProgress += (current, total) => updateMassProgress?.Invoke(current, total);
        }

        private void Initialise()
        {
            if (initialised)
                return;

            ChunkData[] data = DataAccessor.GetData<DataAssetChunkData>().Data;
            for (int i = 0; i < data.Length; i++)
            {
                if (!chunkDataLookup.ContainsKey(data[i].ChunkSize))
                    chunkDataLookup.Add(data[i].ChunkSize, data[i]);
            }
            
            initialised = true;
        }

        public void OpenMenu(Asteroid asteroid)
        {
            if (asteroid.State < AsteroidState.Scanned)
            {
                if (asteroid.State == AsteroidState.Unscanned)
                    scannerScreen.OpenOnTarget(asteroid);
                return;
            }
            if (asteroid.State != AsteroidState.Finished)
                asteroidMenu.OpenOnAsteroid(asteroid);
        }

        public void ResetAsteroids()
        {
            while (asteroids.Count > 0)
            {
                Destroy(asteroids[0].gameObject);
                asteroids.RemoveAt(0);
            }
        }

        public int GetSweepsForLayerCount(int layerCount)
        {
            return sweepsPerLayerCount[Mathf.Clamp(layerCount, 0, sweepsPerLayerCount.Length - 1)];
        }

        public void RegisterVersionUpdated()
        {
            prefabsInitialised = false;
        }

        public List<AsteroidData> LoadAsteroidData()
        {
            if (GameVersionManager.CurrentVersion < GameVersion.Version3)
                return new List<AsteroidData>();
            if (!saveAsteroidData.IsPopulated)
                return new List<AsteroidData>();
            List<AsteroidData> asteroidData = saveAsteroidData.Value;
            // for (int i = 0; i < asteroidData.Count; i++)
            // {
            //     if (i >= asteroids.Count)
            //         break;
            //     asteroids[i].SetupData(asteroidData[i]);
            // }
            return asteroidData;
        }

        public bool TryGetScanTime(ChunkSize chunkSize, out float time)
        {
            Initialise();
            if (chunkDataLookup.TryGetValue(chunkSize, out ChunkData data))
            {
                time = data.ScanTime;
                return true;
            }

            time = 2f;
            return false;
        }

        private void SaveAsteroidData()
        {
            if (GameVersionManager.CurrentVersion < GameVersion.Version3)
                return;
            List<AsteroidData> asteroidData = new List<AsteroidData>();
            for (int i = 0; i < asteroids.Count; i++)
            {
                asteroidData.Add(asteroids[i].SaveAsteroidData());
            }

            saveAsteroidData.Value = asteroidData;
        }
        
        public string SaveAsteroids()
        {
            SaveAsteroidData();
            string data = "";
            for (int i = 0; i < asteroids.Count; i++)
            {
                if (i > 0)
                    data += "|";
                data += asteroids[i].Save();
            }
            return data;
        }

        private void Update()
        {
            UpdateValues();
        }

        public void ClearAllUnclaimedRewards()
        {
            for (int i = 0; i < asteroids.Count; i++)
            {
                if (!asteroids[i].HasReward)
                    continue;
                ResourceChunkReward reward = asteroids[i].CompletionReward;
                ResourceChunkRewardManager.Instance.ConferReward(reward.rewardType, 
                    Mathf.RoundToInt(reward.rewardMagnitude));
            }
        }

        private void UpdateValues()
        {
            float levelProgress = LevelProgress;
            onUpdateProgress?.Invoke(levelProgress);
            onUpdateCompletion?.Invoke(CompletedAsteroids, AsteroidCount);
            double totalMass = TotalMass;
            onUpdateMassProgress?.Invoke(totalMass * levelProgress, totalMass);
        }
    }
}