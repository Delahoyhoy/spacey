namespace Core.Gameplay.Asteroids
{
    public enum AsteroidState
    {
        Unscanned,
        Scanning,
        Scanned,
        OutpostBuilt,
        Finished,
    }
}