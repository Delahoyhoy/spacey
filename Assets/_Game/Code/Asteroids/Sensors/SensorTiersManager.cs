using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fumb.Save;

namespace Core.Gameplay.Asteroids
{
    public class SensorTiersManager : MonoBehaviour
    {
        private static SensorTiersManager instance;
        public static SensorTiersManager Instance => instance ??= ObjectFinder.FindObjectOfType<SensorTiersManager>();
        
        [SerializeField]
        private SensorTierVersion[] sensorTierVersions;

        private bool overrideSensorTiers;
        private SensorTier[] currentSensorTiers;

        private int CurrentVersion
        {
            get
            {
                if (!saveCurrentVersion.IsPopulated)
                    saveCurrentVersion.Value = sensorTierVersions.Length - 1;
                return saveCurrentVersion.Value;
            }
        }

        public SensorTier[] SensorTiers => overrideSensorTiers ? currentSensorTiers : sensorTierVersions[CurrentVersion].tiers;
        
        [ManagedSaveValue("SensorTiersManagerCurrentVersion")]
        private SaveValueInt saveCurrentVersion;

        public void UpdateToLatestVersion()
        {
            saveCurrentVersion.Value = sensorTierVersions.Length - 1;
        }

        public void InitialiseVersionFromLegacy(bool oldestSystem)
        {
            if (saveCurrentVersion.IsPopulated)
                return;
            saveCurrentVersion.Value = oldestSystem ? 0 : 1;
        }

        public void InitialiseCustomTiers(SensorTier[] tiers)
        {
            overrideSensorTiers = true;
            currentSensorTiers = tiers;
        }
        
    }
}
