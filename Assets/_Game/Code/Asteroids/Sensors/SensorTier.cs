using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public struct SensorTier
    {
        public int asteroidCount;
        public int fogRange;
        public float minDist, maxDist;

        public double[] mineralTemplate;
        public Transform[] fixedPositions;

        public static Vector3 GetPositionFromAngle(float angle, float dist)
        {
            float x = Mathf.Sin(angle) * dist;
            float y = Mathf.Cos(angle) * dist;
            return new Vector3(x, y, 0);
        }

        public List<Vector3> GetAsteroidPositions(List<Vector3> existingPositions)
        {
            return GetAsteroidPositions(asteroidCount, existingPositions);
        }

        public List<Vector3> GetAsteroidPositions(int toGenerate, List<Vector3> existingPositions)
        {
            int innerCount = toGenerate > 3 ? toGenerate / 2 : toGenerate;
            List<Vector3> positions = new List<Vector3>();
            float eachAngle = 360f / innerCount;
            float startAngle = Random.Range(0, eachAngle);
            for (int i = 0; i < toGenerate; i++)
            {
                if (i == innerCount)
                {
                    eachAngle = 360f / (toGenerate - innerCount);
                    startAngle = Random.Range(0, eachAngle);
                }

                //Debug.Log((i >= innerCount ? "Outer: " : "Inner: ") + dist);
                Vector3 position = Vector3.zero, bestPosition = Vector3.zero;
                float bestDistance = 0, distance = 0;
                int iterations = 0;
                if (i < fixedPositions.Length)
                {
                    position = fixedPositions[i].position;
                }
                else
                {
                    do
                    {
                        if (iterations > 25)
                        {
                            position = bestPosition;
                            break;
                        }

                        if (distance > bestDistance)
                        {
                            bestPosition = position;
                            bestDistance = distance;
                        }

                        float angle = (startAngle + (i * eachAngle) + Random.Range(0, eachAngle / 2f)) * Mathf.Deg2Rad;
                        float dist = 0;
                        if (i >= innerCount)
                            dist = Random.Range(minDist + ((maxDist - minDist) / 2) + 2, maxDist);
                        else
                            dist = Random.Range(minDist, maxDist - ((maxDist - minDist) / 2) - 2);
                        position = GetPositionFromAngle(angle, dist);
                        iterations++;
                    } while (CheckTooClose(position, existingPositions, 10f, out distance));
                }

                positions.Add(position);
                existingPositions.Add(position);
            }

            return positions;
        }

        private bool CheckTooClose(Vector3 position, List<Vector3> positions, float distanceThreshold,
            out float distance)
        {
            float lowestDistance = float.MaxValue;
            for (int i = 0; i < positions.Count; i++)
            {
                float distanceToPos = Vector3.Distance(position, positions[i]);
                if (distanceToPos < lowestDistance)
                    lowestDistance = distanceToPos;
            }

            distance = lowestDistance;
            if (lowestDistance < distanceThreshold)
                return true;
            return false;
        }
    }
}