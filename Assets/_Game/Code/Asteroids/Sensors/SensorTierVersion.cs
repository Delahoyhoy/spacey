using System;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    [Serializable]
    public class SensorTierVersion
    {
        public SensorTier[] tiers;
    }
}