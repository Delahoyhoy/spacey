using System.Collections.Generic;
using Fumb.Save;

namespace Core.Gameplay.Asteroids
{
    public class SaveAsteroidData : SaveValue<List<AsteroidData>> { }
}