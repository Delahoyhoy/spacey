using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public static class AsteroidNameHelper
    {
        private static bool initialised;
        private static List<string> usedIDs;

        private static void Init()
        {
            if (initialised) 
                return;
            usedIDs = new List<string>();
            initialised = true;
        }

        private static char[] halfMonths =
            {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                'V', 'W', 'X', 'Y'
            },
            secondLetter =
            {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                'V', 'W', 'X', 'Y', 'Z'
            },
            numerator =
            {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z'
            },
            year = {'N', 'O', 'P', 'Q'};

        private static string GenerateString()
        {
            Init();
            return string.Format("{0}{1}{2}{3}{4}{5}{6}",
                GetRandomChar(year),
                GetRandomDigit(),
                GetRandomDigit(),
                GetRandomChar(halfMonths),
                GetRandomChar(numerator),
                GetRandomDigit(),
                GetRandomChar(secondLetter));
        }

        public static void AddName(string name)
        {
            Init();
            if (!usedIDs.Contains(name))
                usedIDs.Add(name);
        }

        private static bool ValidateString(string input)
        {
            return !usedIDs.Contains(input);
        }

        public static string GetName()
        {
            string name = "";
            do
            {
                name = GenerateString();
            } while (!ValidateString(name));

            return name;
        }

        private static char GetRandomChar(char[] array)
        {
            return array[Random.Range(0, array.Length)];
        }

        private static string GetRandomDigit()
        {
            return Random.Range(0, 10).ToString();
        }
    }
}