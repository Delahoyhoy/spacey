using Core.Gameplay.CameraControl;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Gameplay.Asteroids
{
    public class AsteroidInfoDisplay : UIScreen
    {
        [SerializeField]
        private TierProbability[] tierProbabilities = new TierProbability[5];

        [SerializeField]
        private AnchoredUI anchor;

        [SerializeField]
        private Text asteroidName;

        public void OpenOnAsteroid(Asteroid asteroid)
        {
            asteroidName.text = asteroid.AsteroidName;
            BasicCameraController.Instance.Pan(asteroid.transform);
            anchor.Target = asteroid.transform;
            double[] resourceYields = asteroid.GetResourceYields();
            for (int i = 0; i < tierProbabilities.Length; i++)
            {
                tierProbabilities[i].Setup(resourceYields[i]);
            }
            base.Open();
        }

    }
}