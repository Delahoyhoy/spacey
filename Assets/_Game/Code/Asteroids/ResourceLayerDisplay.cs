using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Gameplay.Asteroids
{
    public class ResourceLayerDisplay : MonoBehaviour
    {
        [SerializeField]
        private ResourceFleck[] resourceFlecks;

        [SerializeField]
        private SpriteRenderer mostCommonResource;

        public void Setup(AsteroidLayer layerData)
        {
            for (int i = 0; i < resourceFlecks.Length; i++)
            {
                Tier tier = layerData.GetTierAtPercentile(resourceFlecks[i].percentile);
                Color colour = ColourHelper.Instance.GetTierColour(tier);
                resourceFlecks[i].SetColour(colour);
            }
            
            Tier mostCommon = layerData.GetHighestDistribution();
            mostCommonResource.color = ColourHelper.Instance.GetTierColour(mostCommon);
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void SetFlecksActive(bool active)
        {
            mostCommonResource.enabled = active;
            for (int i = 0; i < resourceFlecks.Length; i++)
                resourceFlecks[i].SetActive(active);
        }
    }
}
