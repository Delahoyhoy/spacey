﻿using System;
using Core.Gameplay.Audio;
using Core.Gameplay.CameraControl;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Gameplay.Asteroids
{
    public class ScannerScreen : MonoBehaviour
    {
        [SerializeField]
        private UIScreen screen;

        [SerializeField]
        private Asteroid currentTarget;

        [SerializeField]
        private AnchoredUI anchor;

        [SerializeField]
        private UnityEvent<bool> onSetScanActive;

        [SerializeField]
        private UnityEvent<double> onSetHurryCost, onSetQuickScanCost;

        private bool scanInProgress;
        private bool setup;
        private double instantScanCost;

        private void Start()
        {
            if (setup)
                return;

            void SetScanActive(bool active)
            {
                scanInProgress = active;
                onSetScanActive?.Invoke(scanInProgress);
                if (active)
                    onSetHurryCost?.Invoke(ScannerSweep.Instance.HurryCost + instantScanCost);
            }
            ScannerSweep.Instance.onScanActive += SetScanActive;
            SetScanActive(ScannerSweep.Instance.scanning);
            setup = true;
        }

        public void OpenOnTarget(Asteroid asteroid)
        {
            currentTarget = asteroid;
            Transform asteroidTransform = asteroid.transform;
            anchor.Target = asteroidTransform;
            BasicCameraController.Instance.LockOnTarget(asteroidTransform);
            UIManager.Instance.AddOverlayBlockFlags(1);
            screen.Open();
            Refresh();
        }

        public void Refresh()
        {
            if (!screen.IsOpen)
                return;
            if (!currentTarget)
                return;
            instantScanCost = ScannerSweep.Instance.GetCostToFullyScan(currentTarget.TotalLayers);
            onSetQuickScanCost?.Invoke(instantScanCost);
            onSetHurryCost?.Invoke(ScannerSweep.Instance.HurryCost + instantScanCost);
        }

        public void HurryScan()
        {
            if (ScannerSweep.Instance.TryHurry() && 
                CurrencyController.Instance.SpendCurrency(instantScanCost, CurrencyType.HardCurrency, 
                    "scanner", "hurry_scan"))
            {
                screen.Close();
                AudioManager.Play("Radar", 0.5f);
                currentTarget.SetState(AsteroidState.Scanned);
            }
        }

        public void QuickScan()
        {
            if (!CurrencyController.Instance.SpendCurrency(instantScanCost, CurrencyType.HardCurrency, "scanner",
                    "quick_scan"))
                return;
            screen.Close();
            AudioManager.Play("Radar", 0.5f);
            currentTarget.SetState(AsteroidState.Scanned);
        }

        public void HandleClose()
        {
            BasicCameraController.Instance.UnlockTarget();
            UIManager.Instance.RemoveOverlayBlockFlags(1);
        }

        public void Scan()
        {
            screen.Close();
            GameManager.Instance.ScanAsteroid(currentTarget);
        }

        private void Update()
        {
            if (scanInProgress)
                onSetHurryCost?.Invoke(ScannerSweep.Instance.HurryCost + instantScanCost);
        }
    }
}