namespace Core.Gameplay.Asteroids
{
    public enum Tier
    {
        Common,
        Uncommon,
        Rare,
        Epic,
        Legendary,
    }
}