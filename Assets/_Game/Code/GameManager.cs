﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Gameplay;
using Core.Gameplay.Asteroids;
using Core.Gameplay.Audio;
using Core.Gameplay.Augments;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Core.Gameplay.Outposts;
using Core.Gameplay.Research;
using Core.Gameplay.Boosts;
using Core.Gameplay.CameraControl;
using Core.Gameplay.Reset;
using Core.Gameplay.Sectors;
using Core.Gameplay.Save;
using Core.Gameplay.Ships;
using Core.Gameplay.Tutorial;
using Core.Gameplay.UserInterface;
using Core.Gameplay.Version;
using Fumb.Save;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

//TODO: OSCAR: PLUGIN REWORK REMOVE COMMENT AFTER
// using Facebook.Unity;

public class GameManager : MonoBehaviour, ISaveLoad
{
    public List<Asteroid> Asteroids
    {
        get => AsteroidManager.Instance.Asteroids;
        set => AsteroidManager.Instance.Asteroids = value;
    }
    private Asteroid[] AsteroidPrefabs => AsteroidManager.Instance.AsteroidPrefabs;

    public static bool useScientific = false;

    [SerializeField]
    private Transform fogCentre;

    [SerializeField]
    private SensorTiersManager sensorTiersManager;

    [SerializeField]
    private ScannerSweep scanner;

    [SerializeField]
    private ScanResults asteroidUIPrefab;

    private List<ScanResults> asteroidMenus = new List<ScanResults>();

    [SerializeField]
    private Transform asteroidUIParent;

    public Transform AsteroidUIParent => asteroidUIParent;

    private double cargoCapacity = 25f, cargoSpeed = 2f;

    public double CargoCapacity => cargoCapacity;
    public double CargoSpeed => cargoSpeed;

    public static Action<double> OnCargoCapacityChange;
    public static Action<float> OnCargoSpeedChange;

    private static GameManager instance;
    public static GameManager Instance { get { if (!instance) instance = ObjectFinder.FindObjectOfType<GameManager>(); return instance; } }
    
    public static Action<Vector3> OnCameraMove;
    public static Action<float> OnZoomChange;
    
    public static event Action<double, double> OnRefreshOutpostCost;
    
    public static Action OnSecondUpdate;

    private bool useOldSensorTiers = false;

    private SensorTier[] SensorTiers => sensorTiersManager.SensorTiers; //{ get { if (useOldSensorTiers) return sensorTiers; return newSensorTiers; } }

    private double globalMult = 1d, boostMultiplier = 1d;
    public double GlobalMultiplier => globalMult * CurrencyController.Instance.PrestigeMultiplier;

    public double BonusMultiplier => boostMultiplier;

    public double IncomeMultiplier => GlobalMultiplier;

    [SerializeField]
    private float currentRadius = 20f, targetRadius = 20f;

    public float CurrentRadius => currentRadius;

    public float FogRadius => targetRadius;
    public Vector3 FogCentre => fogCentre.position;

    [SerializeField]
    private BezierPoint topBay, bottomBay;

    [SerializeField]
    private BezierPoint[] dockingBays;

    [SerializeField]
    private GaussianTest shaderStuff;
    private int quality = 2;
    [SerializeField]
    private Slider qualitySlider;

    public int Quality => quality;
    
    public Action<int> onSetQuality;

    [SerializeField]
    private UIScreen shopMenu;
    [SerializeField]
    private MultitabWindow shopWindow;

    [SerializeField]
    private GameObject devWindow;

    [SerializeField]
    private Text earningText;

    private bool newSystem = false;

    [ManagedSaveValue("GameManagerHighestShipUnlocked")]
    private SaveValueInt saveHighestUnlocked;

    private int highestUnlocked = -1;

    public int HighestUnlocked
    {
        get => saveHighestUnlocked.IsPopulated ? saveHighestUnlocked.Value : -1;
        set => saveHighestUnlocked.Value = value;
    }

    public bool UsingNewSystem
    {
        get { return newSystem; }
        set
        {
            newSystem = value;
            if (value)
                onSelectNew.Invoke();
            else
                onSelectOld.Invoke();
        }
    }

    [SerializeField]
    private UnityEvent onSelectNew, onSelectOld;

    [SerializeField]
    private NewShipScreen newShipScreen;

    public double globalMineSpeedMult = 1f, cargoShipSpeedMult = 1f, efficiencyMult = 1f;

    public static event Action<double> OnSetBonusActive;

    public void UnlockShip(int ship)
    {
        if (GameVersionManager.CurrentVersion == GameVersion.Version1)
            return;
        if (ship > HighestUnlocked)
        {
            if (newShipScreen)
                newShipScreen.Setup(ship);
            HighestUnlocked = ship;
        }
    }

    public BezierPoint GetDockingBay(float yPosition)
    {
        return yPosition > -5.88 ? topBay : bottomBay;
    }

    public BezierPoint GetDockingBay(Vector3 pos)
    {
        if (GameVersionManager.CurrentVersion < GameVersion.Version3)
            return GetMainShipBay(pos);
        
        BezierPoint bestBay = dockingBays[0];
        float bestHeuristic = -1;

        for (int i = 0; i < dockingBays.Length; i++)
        {
            float heuristic = dockingBays[i].GetAngleHeuristic(pos);
            if (heuristic <= bestHeuristic)
                continue;
            bestBay = dockingBays[i];
            bestHeuristic = heuristic;
        }

        return bestBay;

    }

    public int GetNewAsteroidsAtLevel(int level)
    {
        return SensorTiers[Mathf.Min(level, SensorTiers.Length - 1)].asteroidCount;
    }

    public void OpenOnShop()
    {
        if (BasicCameraController.Instance.InSystemView && FTUEManager.Instance.GetIsModuleCompleted(FTUEModuleId.Core) && !UIManager.Instance.InMenu && !ResetManager.Instance.resetting)
        {
            shopMenu.Open();
            shopWindow.SelectTab(1);
        }
    }

    public double GetTotalEarnings(bool includeMinIfNotMining)
    {
        return OutpostManager.Instance.GetTotalEarnings(includeMinIfNotMining);
    }

    public void SetDoubleEvent(bool active)
    {
        boostMultiplier = active ? 2d : 1d;
    }

    private void EnableDevMode()
    {
        //NotificationDisplay.Instance.Display("DEV MODE ENABLED");
    }

    private void OnEnable()
    {
        ResearchManager.OnResearchLoaded += LoadResearch;
        ResearchManager.OnResearchUpgrade += UpgradeResearch;
    }

    public void ResetScene(ResetType resetType)
    {
        if (resetType == ResetType.Prestige)
            OutpostManager.Instance.OnReset();
        else
            OutpostManager.Instance.SetOutpostsInactive();
        AsteroidManager.Instance.ResetAsteroids();
        while (asteroidMenus.Count > 0)
        {
            Destroy(asteroidMenus[0].gameObject);
            asteroidMenus.RemoveAt(0);
        }
        useOldSensorTiers = false;

        if (GameVersionManager.CurrentVersion < GameVersion.Version3)
            SetupAsteroids(0);
    }

    public void ClearScene()
    {
        OutpostManager.Instance.OnReset();
        OutpostManager.Instance.ClearPool();
        AsteroidManager.Instance.ResetAsteroids();
        while (asteroidMenus.Count > 0)
        {
            Destroy(asteroidMenus[0].gameObject);
            asteroidMenus.RemoveAt(0);
        }
    }

    public void SetQuality(float newQuality)
    {
        int currentQuality = quality;
        quality = (int)newQuality;
        switch (quality)
        {
            case 0:
                shaderStuff.useBlur = false;
                Application.targetFrameRate = 30;
                break;
            case 1:
                shaderStuff.useBlur = false;
                Application.targetFrameRate = 45;
                break;
            case 2:
                shaderStuff.useBlur = true;
                Application.targetFrameRate = 60;
                break;
        }
        onSetQuality?.Invoke(quality);
    }

    public void FixMoney()
    {
        if (double.IsNaN(CurrencyController.Instance.CurrentGold) || double.IsInfinity(CurrencyController.Instance.CurrentGold))
        {
            CurrencyController.Instance.ResetMoney();
            AddHoursOfGold(8);
            NotificationDisplay.Instance.Display("SORRY ABOUT THAT!\nHOPEFULLY THIS SHOULD FIX IT");
        }
    }

    private IEnumerator ExpandCamera(float from, float to)
    {
        targetRadius = to;
        for (float f = 0; f < 1f; f += Time.deltaTime * 0.2f)
        {
            currentRadius = Mathf.Lerp(from, to, f);
            RefreshCamera();
            yield return null;
        }
        currentRadius = to;
        RefreshCamera();
    }

    private void OnDestroy()
    {
        ResearchManager.OnResearchLoaded -= LoadResearch;
        ResearchManager.OnResearchUpgrade -= UpgradeResearch;
    }

    public void SetBuildButtons(bool active)
    {
        for (int i = 0; i < asteroidMenus.Count; i++)
        {
            asteroidMenus[i].SetBuildButtonAvailable(active);
        }
    }

    public double GetTotalGPS(bool includeMinIfNotMining)
    {
        return OutpostManager.Instance.GetTotalEarnings(includeMinIfNotMining);
    }

    public double GetGoldEarned(double seconds, bool includeMinIfNotMining)
    {
        return GetTotalGPS(includeMinIfNotMining) * seconds;
    }

    public double GetBaseEarnings(double seconds)
    {
        return OutpostManager.Instance.GetBaseEarningsPerSecond() * seconds;
    }

    public void EnableBoost(BoostType type, double magnitude)
    {
        switch (type)
        {
            case BoostType.Tap:
                MiningStation.Instance.SetMultiplier(magnitude);
                break;
            case BoostType.MineSpeed:
                globalMineSpeedMult = magnitude;
                break;
            case BoostType.CargoSpeed:
                cargoShipSpeedMult = magnitude;
                break;
            case BoostType.Efficiency:
                efficiencyMult = 1d + (magnitude / 100);
                break;
        }
    }

    public void DisableBoost(BoostType type)
    {
        switch (type)
        {
            case BoostType.Tap:
                MiningStation.Instance.SetMultiplier(1d);
                break;
            case BoostType.MineSpeed:
                globalMineSpeedMult = 1d;
                break;
            case BoostType.CargoSpeed:
                cargoShipSpeedMult = 1d;
                break;
            case BoostType.Efficiency:
                efficiencyMult = 1d;
                break;
        }
    }

    private void UpgradeResearch(ResearchType researchType, double magnitude, int level)
    {
        switch (researchType)
        {
            case ResearchType.SensorRange:
                //currentRadius = sensorTiers[level].fogRange;
                SetupAsteroids(level);
                StartCoroutine(ExpandCamera(currentRadius, SensorTiers[level].fogRange));
                //RefreshCamera();
                break;
            case ResearchType.CargoCap:
                cargoCapacity = magnitude;
                if (OnCargoCapacityChange != null)
                    OnCargoCapacityChange(cargoCapacity);
                break;
            case ResearchType.CargoSpeed:
                cargoSpeed = magnitude;
                if (OnCargoSpeedChange != null)
                    OnCargoSpeedChange((float)cargoSpeed);
                break;
            case ResearchType.TapSpeed:
                MiningStation.Instance.SetMiningSpeed(magnitude);
                break;
            case ResearchType.TapEfficiency:
                MiningStation.Instance.SetMaxEfficiency(magnitude);
                break;
        }
    }

    private void LoadResearch(ResearchType researchType, double magnitude, int level)
    {
        switch (researchType)
        {
            case ResearchType.SensorRange:
                targetRadius = currentRadius = SensorTiers[level].fogRange;
                RefreshCamera();
                break;
            case ResearchType.CargoCap:
                cargoCapacity = magnitude;
                if (OnCargoCapacityChange != null)
                    OnCargoCapacityChange(cargoCapacity);
                break;
            case ResearchType.CargoSpeed:
                cargoSpeed = magnitude;
                if (OnCargoSpeedChange != null)
                    OnCargoSpeedChange((float)cargoSpeed);
                break;
            case ResearchType.TapSpeed:
                MiningStation.Instance.SetMiningSpeed(magnitude);
                break;
            case ResearchType.TapEfficiency:
                MiningStation.Instance.SetMaxEfficiency(magnitude);
                break;
        }
    }

    public double GetTotalMinedPerSecond(bool includeMinIfNotMining)
    {
        return OutpostManager.Instance.GetTotalEarnings(includeMinIfNotMining);
    }

    private bool boostActive = false;

    public void SetBonusActive(bool active)
    {
        boostMultiplier = active ? 2d : 1d;
        boostActive = active;
        if (active)
            StartCoroutine(BoostLoop());
        OnSetBonusActive?.Invoke(boostMultiplier);
    }

    private IEnumerator BoostLoop()
    {
        while (boostActive)
        {
            List<Outpost> outposts = OutpostManager.Instance.Outposts;
            for (int i = 0; i < outposts.Count; i++)
            {
                if (!outposts[i].HasTarget)
                    continue;
                Asteroid target = GetAsteroid(outposts[i].AsteroidIndex);
                if (!target || target.State == AsteroidState.Finished)
                    continue;
                EffectManager.Instance.EmitAt("sparkleBurst", target.transform.position, 1);
            }
            yield return new WaitForSeconds(Random.Range(0.33f, 0.66f));
        }
    }

    private void Awake()
    {
        instance = this;
        StartCoroutine(UpdateEverySecond());
        //DontDestroyDataHolder.Instance.GetIsAround(); // Don't think this is needed anymore...
    }

    public void SetupAsteroids(int sensorTier)
    {
        if (sensorTier >= SensorTiers.Length)
            return;
        List<Vector3> positions = SensorTiers[sensorTier].GetAsteroidPositions(AsteroidPositions);
        for (int i = 0; i < positions.Count; i++)
        {
            Vector3 position = fogCentre.position + positions[i];
            Asteroid newAsteroid = Instantiate(AsteroidPrefabs[Random.Range(0, AsteroidPrefabs.Length)], position, Quaternion.identity);
            newAsteroid.SetupAsteroidName();
            newAsteroid.SetupResourceYields(SensorTiers[sensorTier].mineralTemplate, Asteroids.Count);
            ScanResults asteroidUI = Instantiate(asteroidUIPrefab, asteroidUIParent);
            asteroidUI.Setup(newAsteroid);
            asteroidMenus.Add(asteroidUI);
            Asteroids.Add(newAsteroid);
        }
    }

    public void InitialiseAllAsteroids()
    {
        // for (int i = 0; i < SensorTiers.Length; i++)
        //     SetupAsteroids(i);
        // targetRadius = currentRadius = SensorTiers[SensorTiers.Length - 1].fogRange;
        // RefreshCamera();
    }

    public void InitialiseAsteroidsFromCurrentLevel()
    {
        SectorTemplate template = SectorManager.Instance.CurrentSectorTemplate;
        List<AsteroidSetupData> data = template.GenerateAsteroidData();
        for (int i = 0; i < data.Count; i++)
        {
            Vector3 position = fogCentre.position + data[i].Position;
            Asteroid newAsteroid = Instantiate(AsteroidPrefabs[Random.Range(0, AsteroidPrefabs.Length)], position, Quaternion.identity);
            newAsteroid.SetupAsteroidName();
            //newAsteroid.SetupResourceYields(SensorTiers[sensorTier].mineralTemplate, asteroids.Count);
            newAsteroid.SetupData(data[i].Data, i);
            newAsteroid.InitialSetup();
            ScanResults asteroidUI = Instantiate(asteroidUIPrefab, asteroidUIParent);
            asteroidUI.Setup(newAsteroid);
            asteroidMenus.Add(asteroidUI);
            Asteroids.Add(newAsteroid);
        }
        SetupFogRadius();
        RefreshCamera();
    }

    private List<Vector3> AsteroidPositions
    {
        get
        {
            List<Vector3> positions = new List<Vector3>();
            for (int i = 0; i < Asteroids.Count; i++)
            {
                positions.Add(Asteroids[i].transform.position);
            }
            return positions;
        }
    }

    private void SetupEarningText()
    {
        double earnings = GetTotalEarnings(false) * 60;
        if (earnings <= 0)
            earningText.text = "";
        else
            earningText.text = string.Format("{0}/minute", CurrencyController.FormatToString(earnings));
    }

    private IEnumerator UpdateEverySecond()
    {
        yield return null;
        yield return null;
        while (true)
        {
            SetupEarningText();
            OnRefreshOutpostCost?.Invoke(OutpostManager.Instance.NextOutpostCost, 
                CurrencyController.Instance.GetCurrency(CurrencyType.SoftCurrency));
            OnSecondUpdate?.Invoke();
            yield return new WaitForSeconds(1f);
        }
    }

    public void RefreshCamera()
    {
        ChangeZoom(Camera.main.orthographicSize);
    }

    public BezierPoint GetMainShipBay(Vector3 position)
    {
        return position.y > -5.88 ? topBay : bottomBay;
    }

    public void AddHoursOfGold(float toAdd)
    {
        double goldToAdd = GetGoldEarned(toAdd * 60 * 60, true);
        CoinBurster.Instance.AddGoldWithEffect(goldToAdd);
        //Debug.Log(goldToAdd);
    }

    private int shipsRetreated = 0;

    [SerializeField]
    private Transform shipFocalPoint, mainShip;

    private void OnShipRetreated(bool trackedShip)
    {
        shipsRetreated++;
        if (trackedShip)
        {
            BasicCameraController.Instance.LockOnTarget(shipFocalPoint, 8f);
        }
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    [SerializeField]
    private LineRenderer mainEngine, engine2, engine3;

    [SerializeField]
    private AnimationCurve shipExitSpeed, cameraFollowSpeed;

    private IEnumerator RetreatAllShips(Action onComplete)
    {
        int totalToRetreat = OutpostManager.Instance.RetreatAllShips(OnShipRetreated);
        // for (int i = 0; i < outposts.Count; i++)
        // {
        //     totalToRetreat += outposts[i].RetreatShips(OnShipRetreated);
        // }
        Vector3 shipStart = mainShip.position;
        shipFocalPoint.position = shipStart;
        shipsRetreated = 0;
        if (totalToRetreat <= 0)
            BasicCameraController.Instance.LockOnTarget(shipFocalPoint, 8f);
        else
        {
            BasicCameraController.Instance.LockOntoNearestShip();
            while (shipsRetreated < totalToRetreat)
            {
                yield return null;
            }
        }
        Vector3 exitVector = mainShip.up;
        StartCoroutine(WaitFade());
        SetEngines(true);
        CameraShake.Instance.StartShake();
        CameraShake.Instance.IntensityMult = 0;
        for (float t = 0; t < 8f; t += Time.deltaTime)
        {
            float f = t / 8f;
            float shipSpeed = shipExitSpeed.Evaluate(f);
            mainShip.Translate(exitVector * (shipSpeed * Time.deltaTime * 20f), Space.World);
            shipFocalPoint.Translate(exitVector * (cameraFollowSpeed.Evaluate(f) * Time.deltaTime * 20f), Space.World);
            CameraShake.Instance.IntensityMult = shipSpeed;
            SetEngineTrailLength(shipSpeed);
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);
        CameraShake.Instance.StopShaking();
        mainShip.transform.position = shipStart;
        shipFocalPoint.transform.position = shipStart;
        SetEngines(false);
        yield return new WaitForSeconds(0.25f);
        overlay.CrossFadeAlpha(1f, 0f, true);
        BasicCameraController.Instance.ResetCameraToPos(shipStart);
        BasicCameraController.Instance.UnlockTarget();
        onComplete?.Invoke();
    }

    public void SetEngineTrailLength(float shipSpeed)
    {
        mainEngine.SetPosition(1, Vector3.down * shipSpeed);
        Vector3 subEngineLength = Vector3.down * (shipSpeed * 0.5f);
        engine2.SetPosition(1, subEngineLength);
        engine3.SetPosition(1, subEngineLength);
    }

    public void SetEngines(bool active)
    {
        mainEngine.gameObject.SetActive(active);
        engine2.gameObject.SetActive(active);
        engine3.gameObject.SetActive(active);
    }

    [SerializeField]
    private Image overlay;

    private IEnumerator WaitFade()
    {
        overlay.gameObject.SetActive(true);
        overlay.CrossFadeAlpha(0, 0, true);
        yield return new WaitForSeconds(7f);
        overlay.CrossFadeAlpha(1f, 1f, true);
    }

    private IEnumerator WaitHideFade()
    {
        yield return new WaitForSeconds(1f);
        overlay.gameObject.SetActive(false);
    }

    public void BeginPrestige()
    {
        void Complete()
        {
            sensorTiersManager.UpdateToLatestVersion();
            ResetManager.Instance.Prestige();
            MiningStation.Instance.UpdateOutpostCount(0);
            StartCoroutine(WaitHideFade());
            overlay.CrossFadeAlpha(0, 1f, false);
        }
        
        StartCoroutine(RetreatAllShips(Complete));
    }

    public void PrestigeToUpdate(Action onComplete)
    {
        void Complete()
        {
            onComplete?.Invoke();
            ResetManager.Instance.Prestige();
            StartCoroutine(WaitHideFade());
            overlay.CrossFadeAlpha(0, 1f, false);
            InitialiseAsteroidsFromCurrentLevel();
        }

        StartCoroutine(RetreatAllShips(Complete));
    }

    private IEnumerator CheckFramerate()
    {
        int threshold = 20, required = 60;
        float secondsPerFrame = 1f / threshold;
        int countUnderThreshold = 0;
        while (true)
        {
            if (Time.deltaTime > secondsPerFrame) // Whoops, got that the wrong way around
                countUnderThreshold++;
            else
                countUnderThreshold = 0;

            if (quality > 0 && countUnderThreshold >= required)
            {
                SetQuality(quality - 1);
                qualitySlider.SetValueWithoutNotify(quality);
                countUnderThreshold = 0;
            }
            
            yield return null;
        }
    }

    public void ScanAsteroid(Asteroid asteroid)
    {
        if (scanner)
            scanner.ScanAsteroid(asteroid);
    }

    public bool Scanning => scanner && scanner.scanning;

    public Asteroid GetScannedAsteroid()
    {
        for (int i = 0; i < Asteroids.Count; i++)
        {
            if (Asteroids[i].State == AsteroidState.Scanned)
            {
                return Asteroids[i];
            }
        }
        return null;
    }

    public int GetTotalUnscannedAsteroids()
    {
        int total = 0;
        for (int i = 0; i < Asteroids.Count; i++)
        {
            if (Asteroids[i].State == AsteroidState.Unscanned || Asteroids[i].State == AsteroidState.Scanning)
                total++;
        }
        return total;
    }

    public void ScanFirstUnscannedAsteroid()
    {
        if (Scanning)
            return;
        for (int i = 0; i < Asteroids.Count; i++)
        {
            if (Asteroids[i].State != AsteroidState.Unscanned)
                continue;
            ScanAsteroid(Asteroids[i]);
            break;
        }
    }

    public bool TryGetFirstUnscannedAsteroid(out Asteroid asteroid)
    {
        asteroid = null;
        for (int i = 0; i < Asteroids.Count; i++)
        {
            if (Asteroids[i].State != AsteroidState.Unscanned)
                continue;
            asteroid = Asteroids[i];
            return true;
        }

        return false;
    }

    public void QuickScanAsteroids()
    {
        if (scanner.scanning)
            scanner.Skip();
        for (int i = 0; i < Asteroids.Count; i++)
        {
            if (Asteroids[i].State != AsteroidState.Unscanned)
                continue;
            Asteroids[i].SetState(AsteroidState.Scanned);
        }
    }

    public Asteroid GetBuiltAsteroid()
    {
        for (int i = 0; i < Asteroids.Count; i++)
        {
            if (Asteroids[i].State == AsteroidState.OutpostBuilt)
            {
                return Asteroids[i];
            }
        }
        return null;
    }

    public Asteroid GetAsteroid(int index)
    {
        int selectedIndex = Mathf.Clamp(index, 0, Asteroids.Count - 1);
        return Asteroids[selectedIndex];
    }

    public static void MoveCamera(Vector3 newPos)
    {
        OnCameraMove?.Invoke(newPos);
    }

    public static void ChangeZoom(float newZoom)
    {
        OnZoomChange?.Invoke(newZoom);
    }

    private void LoadAsteroids(string data)
    {
        string[] splitData = data.Split('|');
        List<AsteroidData> asteroidData = AsteroidManager.Instance.LoadAsteroidData();
        for (int i = 0; i < splitData.Length; i++)
        {
            int prefabIndex = Random.Range(0, AsteroidPrefabs.Length);
            string[] asteroid = splitData[i].Split('#');
            if (asteroid.Length >= 8)
                int.TryParse(asteroid[7], out prefabIndex);
            Asteroid newAsteroid = Instantiate(AsteroidPrefabs[prefabIndex]);
            newAsteroid.Load(splitData[i], i);
            if (i < asteroidData.Count)
                newAsteroid.SetupData(asteroidData[i], i);
            else
                newAsteroid.SetupWithNoData();
            ScanResults asteroidUI = Instantiate(asteroidUIPrefab, asteroidUIParent);
            asteroidUI.Setup(newAsteroid);
            asteroidMenus.Add(asteroidUI);
            Asteroids.Add(newAsteroid);
        }
        
        // if (GameVersionManager.CurrentVersion < GameVersion.Version3)
        //     return;
        //
        // SetupFogRadius();
    }

    private void SetupFogRadius()
    {
        int totalAsteroids = 0;
        for (int i = 0; i < SensorTiers.Length; i++)
        {
            if (Asteroids.Count <= totalAsteroids + SensorTiers[i].asteroidCount)
            {
                targetRadius = currentRadius = SensorTiers[i].fogRange;// * 1.2f;
                return;
            }
            totalAsteroids += SensorTiers[i].asteroidCount;
        }
        targetRadius = currentRadius = SensorTiers[SensorTiers.Length - 1].fogRange;
    }

    public void CheckLoadSuccess()
    {
        Vector3 centre = fogCentre.position;
        float maxDistance = FogRadius;
        for (int i = 0; i < Asteroids.Count; i++)
        {
            if (Vector3.Distance(centre, Asteroids[i].transform.position) <= maxDistance) 
                continue;
            Debug.LogWarning("Asteroid range exceeded -- Redistributing!");
            RedistributeAsteroids();
            break;
        }
    }

    public void RedistributeAsteroids()
    {
        Vector3 centre = fogCentre.position;
        float maxDistance = targetRadius - 10f;
        float minDistance = SensorTiers[0].minDist;
        int totalAsteroids = Asteroids.Count;

        int sensorTier = 0;
        Queue<Vector3> availablePositions = new Queue<Vector3>();
        List<Vector3> usedPositions = new List<Vector3>();
        
        
        for (int i = 0; i < totalAsteroids; i++)
        {
            if (availablePositions.Count <= 0)
            {
                List<Vector3> newPositions = SensorTiers[sensorTier].GetAsteroidPositions(usedPositions);
                for (int j = 0; j < newPositions.Count; j++)
                    availablePositions.Enqueue(newPositions[j]);
                sensorTier++;
            }
            // float angle = (80f * i + Random.Range(-20f, 20f)) * Mathf.Deg2Rad;
            // float distance = Mathf.Lerp(minDistance, maxDistance, (float)i / totalAsteroids);
            Vector3 pos = availablePositions.Dequeue();
            usedPositions.Add(pos);
            Asteroids[i].Reposition(centre + pos);
        }
        OutpostManager.Instance.RefreshPositions();
    }

    // Start is called before the first frame update
    private void Start()
    {
        //Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        StartCoroutine(CheckFramerate());
    }

    public void SetDevMode(bool enabled)
    {
        //DevMode = enabled;
    }

    // Update is called once per frame
    private void Update()
    {
#if UNITY_EDITOR || FUMB_DEVELOPMENT_BUILD
        if (Input.touchCount >= 4 || Input.GetKey(KeyCode.Space))
        {
            if (devWindow.activeSelf)
                return;
            AudioManager.Play("Click", 0.5f);
            devWindow.SetActive(true);
        }
#endif
    }

    public void Save(ref Dictionary<string, object> saveData)
    {
        saveData.Add("useNewSystem", UsingNewSystem);
        saveData.Add("w0asteroids", AsteroidManager.Instance.SaveAsteroids());
        //if (outposts.Count > 0)
        saveData.Add("w0outposts", OutpostManager.Instance.SaveOutposts());
        
        if (OutpostManager.Instance.SavePremiumOutposts(out string premiumData))
            saveData.Add("premiumOutposts", premiumData);
        
        saveData.Add("useOldSensors", useOldSensorTiers);
        //saveData.Add("w0mineTarget", MiningStation.Instance.CurrentTarget);
        saveData.Add("quality", quality);
        saveData.Add("w0highestUnlocked", HighestUnlocked);
    }

    public void SetUseOldSensors(bool value)
    {
        useOldSensorTiers = value;
    }

    public void Load(SaveDataWrapper loadedData, ref List<string> ids)
    {
        UsingNewSystem = loadedData.LoadData("useNewSystem", false);
        if (!saveHighestUnlocked.IsPopulated)
            HighestUnlocked = loadedData.LoadData("w0highestUnlocked", -1);
        GameVersionManager.Instance.InitialiseVersion(UsingNewSystem);
        useOldSensorTiers = loadedData.LoadData("useOldSensors", true);
        sensorTiersManager.InitialiseVersionFromLegacy(useOldSensorTiers);
        if (loadedData.HasTag("w0asteroids"))
            LoadAsteroids(loadedData.LoadData("w0asteroids", ""));
        if (loadedData.HasTag("w0outposts"))
            OutpostManager.Instance.LoadOutposts(loadedData.LoadData("w0outposts", ""), false);
        if (loadedData.HasTag("premiumOutposts"))
            OutpostManager.Instance.LoadOutposts(loadedData.LoadData("premiumOutposts", ""), true);
        quality = loadedData.LoadData("quality", 2);
        qualitySlider.SetValueWithoutNotify(quality);
        SetQuality(quality);
        if (GameVersionManager.CurrentVersion < GameVersion.Version3)
            return;
        //targetRadius = currentRadius = SensorTiers[SensorTiers.Length - 1].fogRange;
        SetupFogRadius();
        RefreshCamera();
        ClusterManager.Instance.InitialiseInCurrentSector();

        if (GameVersionManager.CurrentVersion >= GameVersion.Version3 && !FTUEManager.Instance.AnyModuleRunning && 
            TryGetFirstUnscannedAsteroid(out Asteroid firstAsteroid))
            BasicCameraController.Instance.Pan(firstAsteroid.transform);
        
        //MiningStation.Instance.SetTarget(loadedData.LoadData("w0mineTarget", -1));
        //MiningStation.Instance.UpdateOutpostCount(outposts.Count);
    }

    public void LoadDefault()
    {
        GameVersionManager.Instance.UpdateToLatestVersion();
        HighestUnlocked = -1;
        useOldSensorTiers = false;
        sensorTiersManager.UpdateToLatestVersion();
        quality = 2;
        qualitySlider.SetValueWithoutNotify(quality);
        SetQuality(quality);
        
        if (GameVersionManager.CurrentVersion < GameVersion.Version3)
            SetupAsteroids(0);
        else
            ClusterManager.Instance.StartAtFirstCluster();
        
        OutpostManager.Instance.LoadDefault();
        MiningStation.Instance.SetTarget(-1);
    }
}
